﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimStartAndDestroyToTime : MonoBehaviour
{
    // Start is called before the first frame update
    public OptimizationLogic.Effects effects;

    [SerializeField] private Animator anim;
    [SerializeField] private AnimationClip[] AnimName;

    [SerializeField] private SpriteRenderer Sprite;

    private int RandomAnim = 0;
    private float TimeCurrentAnim = 0f;
    private float TimeAnim = 0f;

    [SerializeField] private bool IsOptimization;
    void Start()
    {

    }

    public void Go()
    {
        if (anim == null)
            anim = GetComponent<Animator>();

        if (Sprite == null)
            Sprite = GetComponent<SpriteRenderer>();

        if (AnimName.Length > 0)
        {
            RandomAnim = Random.Range(0, AnimName.Length);
            anim.Play(AnimName[RandomAnim].name);
            TimeCurrentAnim = 0f;
        }

        if (IsOptimization)
        {
            anim.enabled = false;
            Sprite.color = new Color(255f, 255f, 255f, 0f);
        }

        TimeAnim = anim.GetCurrentAnimatorStateInfo(0).length / anim.GetCurrentAnimatorStateInfo(0).speed;

        if (!IsInvoking(nameof(Destroy)))
            Invoke(nameof(Destroy), TimeAnim);
    }

    void Update()
    {
        if(gameObject.activeInHierarchy)
            TimeCurrentAnim += Time.deltaTime;
    }

    void Destroy()
    {
        //if(gameObject)
        //    Destroy(gameObject);

        if (gameObject)
            gameObject.SetActive(false);
    }

    void OnBecameVisible()
    {
        if (gameObject && IsOptimization)
        {
            anim.enabled = true;

            if (AnimName.Length > 0)
            {
                if (TimeCurrentAnim < TimeAnim)
                    anim.Play(AnimName[RandomAnim].name, 0, TimeCurrentAnim / TimeAnim);
            }

            Sprite.color = new Color(255f, 255f, 255f, 255f);
        }
    }

    void OnBecameInvisible()
    {
        if (gameObject && IsOptimization)
        {
            anim.enabled = false;
            Sprite.color = new Color(255f, 255f, 255f, 0f);
        }
    }

    public void CancelAnim()
    {
        if (!gameObject.activeInHierarchy)
            return;

        if (IsInvoking(nameof(Destroy)))
            CancelInvoke(nameof(Destroy));

        gameObject.SetActive(false);
    }
}

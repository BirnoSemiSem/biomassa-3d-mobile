﻿using LightReflectiveMirror;
using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerInfo
{
    public string Email;
    public string NickName;
}

public class SpecificalOptions : MonoBehaviour
{
    public static SpecificalOptions specificalOptions;

    public ServerOptions ServerOptions;
    //Network
    public Net NetManager;
    public LightReflectiveMirrorTransport _LightTransport;

    //Scenes
    [Scene]
    public string MainMenu;
    [Scene]
    public string Game;

    public bool IsHost = false;
    public bool IsOnlyClient = false;

    public bool IsChangeScene = false;

    //Audio
    public float AudioEffectVolume = 0.0f;
    public float AudioMusicVolume = 0.0f;

    public PlayerInfo PlayerInfo;

    // Start is called before the first frame update
    void Awake()
    {
        specificalOptions = this;

        DontDestroyOnLoad(gameObject);

        //В РЕЛИЗНОЙ ВЕРСИИ ВЕРНУТЬ!!!

        if (_LightTransport == null)
            _LightTransport = (LightReflectiveMirrorTransport)Transport.activeTransport;

        if (NetManager == null)
            NetManager = _LightTransport.GetComponent<Net>();

        PlayerInfo = new PlayerInfo();

        if (PlayerPrefs.HasKey("AudioEffectVolume") && PlayerPrefs.HasKey("AudioMusicVolume"))
        {
            AudioEffectVolume = PlayerPrefs.GetFloat("AudioEffectVolume");
            AudioMusicVolume = PlayerPrefs.GetFloat("AudioMusicVolume");
        }
        else
            SetPlayerPrefsAudio();

        ServerOptions.ServerOptionsStart();
    }

    void OnApplicationQuit()
    {
        if(_LightTransport != null)
            _LightTransport.DisconnectFromRelay();
    }


    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void SetPlayerPrefsAudio()
    {
        AudioEffectVolume = 0.6f;
        AudioMusicVolume = 0.6f;

        PlayerPrefs.SetFloat("AudioEffectVolume", AudioEffectVolume);
        PlayerPrefs.SetFloat("AudioMusicVolume", AudioMusicVolume);

        PlayerPrefs.Save();
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (SceneManager.GetActiveScene().path == MainMenu)
        {
            IsHost = false;
            IsOnlyClient = false;

            ServerOptions.ServerID = null;
            ServerOptions.SelectMap = null;

            ServerOptions.SelectMode = null;
            ServerOptions.GameMode = Biohazard.GameModeSetup.Random;
            ServerOptions.MapName = null;
            ServerOptions.CurrentMap = null;

            ServerOptions.TimeLeftMinut = 0;
            ServerOptions.TimeLeftSeconds = 0;

            ServerOptions.CountBots = 0;

            ServerOptions.IsChangeMap = false;
        }
        else
        {
            NetManager.GameScene();
            ServerOptions.LoadingMap();

            if (IsOnlyClient)
                NetManager.OnClientConnect();
            else if (IsHost)
                NetworkManager.singleton.StartHost();
        }

        IsChangeScene = false;
    }
}

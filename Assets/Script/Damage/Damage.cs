﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class Damage : NetworkBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Biohazard Biohazard;

    [SerializeField] private GameObject BloodOnDeath;
    [SerializeField] private GameObject BloodOnFloor;
    [SerializeField] private GameObject BloodOnPain;

    private bool isDamage = false;
    private bool isKill = false;

    //Delegate
    static public UnityAction<GameObject, GameObject, int> EventTakeDamage;
    static public UnityAction<GameObject, int> EventDeathPlayer;

    public void TakeDamege(GameObject victim, GameObject attacker, int damage)
    {
        if (!isServer)
            return;

        isDamage = false;
        isKill = false;

        if (victim.CompareTag("Player"))
        {
            Player _victim = victim.GetComponent<Player>();

            if (attacker.CompareTag("Player"))
            {
                Player _attacker = attacker.GetComponent<Player>();

                if (Biohazard.IsModBiohazardStart)
                {
                    switch (_attacker.PlayerTeam)
                    {
                        case Player.Team.Zombie:
                        {
                            if (_victim.PlayerTeam == Player.Team.Human)
                            {
                                if (Biohazard.HumanCount > 1)
                                {
                                    if (_victim.Armor > 0 && Biohazard.Mode == Biohazard.BiohazardMode.Classic)
                                        _victim.Armor -= damage;
                                    else
                                    {
                                        Biohazard.RpcInfection(victim);
                                        StartCoroutine(Biohazard.InfectionPlayer(victim));
                                    }
                                }
                                else
                                {
                                    if(_victim.Armor > 0)
                                        _victim.Armor -= damage;
                                    else
                                        _victim.Health -= damage;
                                }

                                isDamage = true;
                                StartCoroutine(ParticalBlood(victim));
                            }

                            break;
                        }
                        case Player.Team.Human:
                        {
                            if (_victim.PlayerTeam == Player.Team.Zombie)
                            {
                                _victim.Health -= damage;

                                isDamage = true;
                                StartCoroutine(ParticalBlood(victim));
                            }

                            break;
                        }
                    }
                }
            }

            if (_victim.Health <= 0 && _victim.IsAlive)
            {
                isKill = true;

                RpcTakeDamage(_victim, isDamage, isKill);
                StartCoroutine(DeathPlayer(victim, _victim.GetPlayerID));
            }
            else
                RpcTakeDamage(_victim, isDamage, isKill);
        }

        if (GameControl.IsGame)
            EventTakeDamage?.Invoke(victim, attacker, damage);
    }

    public IEnumerator ParticalBlood(GameObject Player)
    {
        if (isServerOnly)
            yield break;

        if (Random.Range(0, 11) > 7)
        {
            //GameObject _BloodOnFloor = Instantiate(BloodOnFloor, Player.transform.position + new Vector3(Random.Range(-0.05f, 0.05f), Random.Range(-0.1f, 0.1f), 0), Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360))));
            GameObject _BloodOnFloor = OptimizationLogic.OptCode.GetPoolObject(OptimizationLogic.Effects.BloodFloor);
            _BloodOnFloor.transform.position = Player.transform.position + new Vector3(Random.Range(-0.05f, 0.05f), Random.Range(-0.1f, 0.1f), 0);
            _BloodOnFloor.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360)));
            _BloodOnFloor.SetActive(true);
            _BloodOnFloor.GetComponent<AnimStartAndDestroyToTime>().Go();
        }

        if (Random.Range(0, 11) > 7)
        {
            //GameObject _BloodOnPain = Instantiate(BloodOnPain, Player.transform.position + new Vector3(Random.Range(-0.05f, 0.05f), Random.Range(-0.1f, 0.1f), 0), Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360))));
            GameObject _BloodOnPain = OptimizationLogic.OptCode.GetPoolObject(OptimizationLogic.Effects.BloodPain);
            _BloodOnPain.transform.position = Player.transform.position + new Vector3(Random.Range(-0.05f, 0.05f), Random.Range(-0.1f, 0.1f), 0);
            _BloodOnPain.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360)));
            _BloodOnPain.SetActive(true);
            _BloodOnPain.GetComponent<AnimStartAndDestroyToTime>().Go();
        }
        yield return null;
    }

    public IEnumerator DeathPlayer(GameObject Player, int id)
    {
        Player _player = Player.GetComponent<Player>();

        _player.IsAlive = false;
        _player.IsRegeneration = false;
        _player.IsPainShock = false;

        //_player.WeaponSprite.sprite = null;

        Player.transform.Find("Feet").gameObject.SetActive(false);
        Player.transform.Find("Weapon").gameObject.SetActive(false);
        Player.transform.Find("FirePoint").gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
        Player.GetComponent<Collider2D>().enabled = false;

        _player.Pain = null;
        _player.Regeneration = null;
        StopAllCoroutines();

        if (_player.Bot && isServer)
        {
            _player.Bot.BotStop();
            _player.Bot.navMeshAgent.enabled = false;
        }

        switch (_player.PlayerTeam)
        {
            case global::Player.Team.Zombie: { if(!System.String.IsNullOrEmpty(_player.ZombieClass.Name)) _player.Animator.Play(_player.ZombieClass.Name + "-" + global::Player.AnimationPlayer.Death.ToString()); break; }
            case global::Player.Team.Human: { if (!System.String.IsNullOrEmpty(_player.HumanClass.Name)) _player.Animator.Play(_player.HumanClass.Name + "-" + global::Player.AnimationPlayer.Death.ToString()); break; }
        }

        if (!isServerOnly)
        {
            //GameObject _BloodOnDeath = Instantiate(BloodOnDeath, Player.transform.position, Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360))));
            GameObject _BloodOnDeath = OptimizationLogic.OptCode.GetPoolObject(OptimizationLogic.Effects.Death);
            _BloodOnDeath.transform.position = Player.transform.position;
            _BloodOnDeath.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360)));
            _BloodOnDeath.SetActive(true);
            _BloodOnDeath.GetComponent<AnimStartAndDestroyToTime>().Go();
        }

        EventDeathPlayer?.Invoke(Player, _player.GetPlayerID);

        yield return null;
    }

    //[ClientRpc]
    //void RpcParticalBlood(GameObject Player)
    //{
    //    if (isServer)
    //        return;

    //    StartCoroutine(ParticalBlood(Player));
    //}

    //[ClientRpc]
    //void RpcDeathPlayer(GameObject Player, int id)
    //{
    //    if (isServer)
    //        return;

    //    StartCoroutine(DeathPlayer(Player, Player.GetInstanceID()));
    //}

    [ClientRpc]
    void RpcTakeDamage(Player victim, bool isDamage, bool isKill)
    {
        if (isServer)
            return;

        if (!victim)
            return;

        if(isDamage)
            StartCoroutine(ParticalBlood(victim.gameObject));

        if (isKill)
            StartCoroutine(DeathPlayer(victim.gameObject, victim.GetPlayerID));
    }
}

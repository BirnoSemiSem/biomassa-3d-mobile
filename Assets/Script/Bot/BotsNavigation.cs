﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BotsNavigation : NetworkBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameObject[] Walk;
    [SerializeField] private List<SheltersAndLooksOfBunker> Bunkers;

    [SerializeField] private BotControl BotController;

    [SerializeField] private Vector3 TargetPoint = Vector3.zero;
    [SerializeField] private Vector3 LookAtPlayer = Vector3.zero;

    [SerializeField] private int IsBotWalkPoint = -1;

    [SerializeField] private int IsBotBunkerPoint = -1;
    [SerializeField] private int IsBotShelterPoint = -1;

    [SerializeField] private int SuccessRateZombieWalk = 50;

    [SerializeField] private float RadiusPlayerNoFollow = 3f;
    [SerializeField] private float RadiusStopDistans = 0.5f;
    [SerializeField] private float RadiusDangerEsape = 0.75f;

    [SerializeField] private float MinRandomBoxZone = 0.1f;
    [SerializeField] private float MaxRandomBoxZone = 2f;

    [SerializeField] private float CurrentLeftTimeLookAtPlayer = 0;
    [SerializeField] private float MinTimerLookAtPlayer = 2f;
    [SerializeField] private float MaxTimerLookAtPlayer = 10f;

    [SerializeField] private float MinDelayLogicActions = 0.25f;
    [SerializeField] private float MaxDelayLogicActions = 3f;

    [SerializeField] private float MinTimerChangePosition = 15f;
    [SerializeField] private float MaxTimerChangePosition = 30f;

    private bool IsNavigation = false;

    [SerializeField] private bool IsDebug = false;
    private Vector3 len;
    void Start()
    {
        SettingsNavigation();
    }

    void OnEnable()
    {
        GameControl.EventRoundStart += RoundStart;
        Biohazard.EventBiohazardStart += GameStart;
        Biohazard.EventInfectionPlayer += Infection;
    }

    void OnDisable()
    {
        GameControl.EventRoundStart -= RoundStart;
        Biohazard.EventBiohazardStart -= GameStart;
        Biohazard.EventInfectionPlayer -= Infection;
    }

    public void RoundStart()
    {
        if (!isServer)
            return;

        if (Bunkers.Count > 0)
        {
            IsBotBunkerPoint = Random.Range(0, Bunkers.Count);
            IsBotShelterPoint = Random.Range(0, Bunkers[IsBotBunkerPoint].Shelters.Length);
        }
    }

    public void GameStart()
    {
        if (!isServer)
            return;

        if(IsBotBunkerPoint != -1)
        {
            for (int i = 0; i < Bunkers.Count; i++)
            {
                if (Vector2.Distance(transform.position, Bunkers[i].transform.position) < Vector2.Distance(transform.position, Bunkers[IsBotBunkerPoint].transform.position))
                {
                    IsBotBunkerPoint = i;
                    IsBotShelterPoint = Random.Range(0, Bunkers[IsBotBunkerPoint].Shelters.Length);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isServer)
            return;

        SettingsNavigation();

        if (BotController.PlayerControl.IsAlive)
        {
            if(IsDebug)
                Debug.DrawLine(len, len + new Vector3(0, 0.1f, 0), Color.red);

            switch(BotController.PlayerControl.PlayerTeam)
            {
                case global::Player.Team.Zombie:
                {
                    BotWalkZombie();

                    BotLookZombie();

                    BotFollowPlayerZombie();

                    if(BotController.IsBotInBunker)
                        BotController.IsBotInBunker = false;

                    if(BotController.IsBotRunToShelterPositionForBunker)
                        BotController.IsBotRunToShelterPositionForBunker = false;

                    break;
                }
                case global::Player.Team.Human:
                {
                    BotWalkHuman();

                    BotRunToShelter();

                    BotPatroling();

                    BotFollowPlayerHuman();

                    BotDangerEscape();

                    break;
                }
            }

            if (BotController.Bot == BotControl.BotStatus.IsStoping)
            {
                StopAllCoroutines();
                CancelInvoke(nameof(ChangePosition));
                BotController.BotStop();

                CurrentLeftTimeLookAtPlayer = 0;
                TargetPoint = Vector3.zero;
                LookAtPlayer = Vector3.zero;
            }
        }
    }

    void SettingsNavigation()
    {
        if (!IsNavigation)
        {
            Bunkers.Clear();
            Walk = GameObject.FindGameObjectsWithTag("MapWalk");

            GameObject[] Bunker = GameObject.FindGameObjectsWithTag("MapBunker");

            for (int i = 0; i < Bunker.Length; i++)
                Bunkers.Add(Bunker[i].GetComponent<SheltersAndLooksOfBunker>());

            IsNavigation = true;
        }
    }

    void BotWalkZombie()
    {
        if ((BotController.Bot == BotControl.BotStatus.IsRunning) || (BotController.Bot == BotControl.BotStatus.IsRunningAndIsAttack) || (BotController.Bot == BotControl.BotStatus.IsRunningShelter) || (BotController.Bot == BotControl.BotStatus.IsRunningShelterAndIsAttack))
        {
            if (BotController.Bot == BotControl.BotStatus.IsRunning || BotController.Bot == BotControl.BotStatus.IsRunningShelter)
            {
                if (!BotController.IsBotWalk)
                {
                    if (Random.Range(1, 101) <= SuccessRateZombieWalk)
                    {
                        IsBotWalkPoint = Random.Range(0, Walk.Length);
                        TargetPoint = RandomBoxPoint(Walk[IsBotWalkPoint].transform.position, Random.Range(MinRandomBoxZone, MaxRandomBoxZone), Random.Range(MinRandomBoxZone, MaxRandomBoxZone));
                    }
                    else
                    {
                        IsBotBunkerPoint = Random.Range(0, Bunkers.Count);
                        IsBotShelterPoint = Random.Range(0, Bunkers[IsBotBunkerPoint].Shelters.Length);

                        if(Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].Zone != Vector2.zero)
                            TargetPoint = RandomBoxPoint(Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].transform.position, Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].Zone.x, Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].Zone.y);
                        else
                            TargetPoint = RandomBoxPoint(Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].transform.position, Random.Range(MinRandomBoxZone, MaxRandomBoxZone), Random.Range(MinRandomBoxZone, MaxRandomBoxZone));
                    }

                    BotController.IsBotWalk = true;
                    BotController.LookTarget(Vector3.zero, BotController.GetLookSpeedNormal);
                }
                else if (BotController.IsBotWalk && TargetPoint != Vector3.zero && Vector2.Distance(transform.position, TargetPoint) <= Random.Range(0f, RadiusStopDistans))
                {
                    StartCoroutine(boolBack("IsBotWalk"));
                    TargetPoint = Vector3.zero;
                    BotController.BotWalkStop();
                }

                if (BotController.IsBotWalk)
                    BotController.BotWalk(TargetPoint);
            }
            else if ((BotController.Bot == BotControl.BotStatus.IsRunningAndIsAttack || BotController.Bot == BotControl.BotStatus.IsRunningShelterAndIsAttack) && BotController.TargetAim)
            {
                BotController.IsBotWalk = false;
                BotController.BotWalk(BotController.TargetAim.transform.position);
            }
            else
                BotController.ChangeBotStatus();
        }
        else
            BotController.IsBotWalk = false;
    }

    void BotLookZombie()
    {
        if (BotController.BotLook != BotControl.LookingType.None && !BotController.IsBotInDanger)
        {
            if (BotController.IsBotInBunker || BotController.BotLook == BotControl.LookingType.AtBunker)
                BotController.LookTarget(Bunkers[IsBotBunkerPoint].Looks[IsBotShelterPoint].transform.position, BotController.GetLookSpeedNormal);
            else if (BotController.BotLook == BotControl.LookingType.AtPlayer)
            {
                CurrentLeftTimeLookAtPlayer -= Time.deltaTime;
                if (CurrentLeftTimeLookAtPlayer <= 0)
                {
                    CurrentLeftTimeLookAtPlayer = Random.Range(MinTimerLookAtPlayer, MaxTimerLookAtPlayer);
                    LookAtPlayer = Vector3.zero;
                }

                if (LookAtPlayer == Vector3.zero)
                {
                    LookAtPlayer = GameControl.Players[Random.Range(0, GameControl.Players.Length)].transform.position;
                    BotController.LookTarget(LookAtPlayer, BotController.GetLookSpeedNormal);
                }
                else
                    BotController.LookTarget(LookAtPlayer, BotController.GetLookSpeedNormal);
            }
            else if (BotController.BotLook == BotControl.LookingType.AtRandomAngle)
            {
                BotController.LookTarget(Vector3.zero, BotController.GetLookSpeedNormal);
            }
        }
    }

    void BotFollowPlayerZombie()
    {
        if (BotController.Bot == BotControl.BotStatus.IsFollowPlayer || BotController.Bot == BotControl.BotStatus.IsFollowPlayerAndIsAttack)
        {
            if (Biohazard.BotLeaderZombie != null && Biohazard.BotLeaderZombie.GetComponent<Player>().IsAlive)
            {
                if (!BotController.IsBotFollowPlayer && Vector2.Distance(TargetPoint, Biohazard.BotLeaderZombie.transform.position) > RadiusPlayerNoFollow)
                {
                    TargetPoint = RandomBoxPoint(Biohazard.BotLeaderZombie.transform.position, Random.Range(MinRandomBoxZone, MaxRandomBoxZone), Random.Range(MinRandomBoxZone, MaxRandomBoxZone));
                    BotController.IsBotFollowPlayer = true;
                }
                else if (BotController.IsBotFollowPlayer && TargetPoint != Vector3.zero && Vector2.Distance(transform.position, TargetPoint) <= RadiusPlayerNoFollow)
                {
                    StartCoroutine(boolBack("IsBotFollowPlayer"));
                    TargetPoint = Vector3.zero;
                    BotController.BotWalkStop();
                }
            }
            else
            {
                if (GameControl.Players.Length > 0)
                {
                    int countTry = GameControl.Players.Length;
                    int RandomIndex = 0;
                    Player TargetLeaderHumanPlayer;

                    while (!Biohazard.BotLeaderZombie && countTry > 0)
                    {
                        RandomIndex = Random.Range(0, GameControl.Players.Length);

                        TargetLeaderHumanPlayer = GameControl.Players[RandomIndex].GetComponent<Player>();

                        if (TargetLeaderHumanPlayer && TargetLeaderHumanPlayer.IsAlive && TargetLeaderHumanPlayer.PlayerTeam == BotController.PlayerControl.PlayerTeam)
                        {
                            Biohazard.BotLeaderZombie = GameControl.Players[RandomIndex];
                            BotController.IsBotFollowPlayer = true;
                            break;
                        }

                        countTry--;
                    }
                }

                if (!BotController.IsBotFollowPlayer)
                    BotController.ChangeBotStatus();
            }

            if (BotController.IsBotFollowPlayer)
                BotController.BotWalk(TargetPoint);
        }
        else
            BotController.IsBotFollowPlayer = false;
    }

    void BotWalkHuman()
    {
        if ((BotController.Bot == BotControl.BotStatus.IsRunning) || (BotController.Bot == BotControl.BotStatus.IsRunningAndIsAttack) && !BotController.IsBotInDangerEscape)
        {
            if (!BotController.IsBotWalk)
            {
                IsBotWalkPoint = Random.Range(0, Walk.Length);
                TargetPoint = RandomBoxPoint(Walk[IsBotWalkPoint].transform.position, Random.Range(MinRandomBoxZone, MaxRandomBoxZone), Random.Range(MinRandomBoxZone, MaxRandomBoxZone));
                BotController.IsBotWalk = true;
                BotController.IsBotInBunker = false;

                if (!BotController.IsBotInDanger)
                    BotController.LookTarget(Vector3.zero, BotController.GetLookSpeedNormal);
            }
            else if (BotController.IsBotWalk && TargetPoint != Vector3.zero && Vector2.Distance(transform.position, TargetPoint) <= Random.Range(0f, RadiusStopDistans))
            {
                StartCoroutine(boolBack("IsBotWalk"));
                TargetPoint = Vector3.zero;
                BotController.BotWalkStop();
            }

            if (BotController.IsBotWalk)
                BotController.BotWalk(TargetPoint);

        }
        else
            BotController.IsBotWalk = false;
    }

    void BotRunToShelter()
    {
        if (((BotController.Bot == BotControl.BotStatus.IsRunningShelter) || (BotController.Bot == BotControl.BotStatus.IsRunningShelterAndIsAttack)) && !BotController.IsBotInDangerEscape)
        {
            if (!BotController.IsBotRunToShelterPositionForBunker)
            {
                ChangePosition();
            }
            else if (BotController.IsBotRunToShelterPositionForBunker && TargetPoint != Vector3.zero && Vector2.Distance(transform.position, TargetPoint) <= Random.Range(0f, RadiusStopDistans))
            {
                StartCoroutine(boolBack("IsBotRunToShelterPositionForBunker"));
                TargetPoint = Vector3.zero;
                BotController.BotWalkStop();
            }

            if (Vector2.Distance(transform.position, Bunkers[IsBotBunkerPoint].transform.position) <= Bunkers[IsBotBunkerPoint].RadiusBunker)
                BotController.IsBotInBunker = true;
            else
                BotController.IsBotInBunker = false;

            if (BotController.IsBotRunToShelterPositionForBunker && BotController.IsBotInDanger && !BotController.IsBotInBunker && BotController.TargetAim && Vector2.Distance(BotController.TargetAim.transform.position, Bunkers[IsBotBunkerPoint].transform.position) <
                Vector2.Distance(transform.position, Bunkers[IsBotBunkerPoint].transform.position))
            {
                IsBotBunkerPoint = Random.Range(0, Bunkers.Count);
                IsBotShelterPoint = Random.Range(0, Bunkers[IsBotBunkerPoint].Shelters.Length);

                if (Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].Zone != Vector2.zero)
                    TargetPoint = RandomBoxPoint(Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].transform.position, Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].Zone.x, Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].Zone.y);
                else
                    TargetPoint = RandomBoxPoint(Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].transform.position, Random.Range(MinRandomBoxZone, MaxRandomBoxZone), Random.Range(MinRandomBoxZone, MaxRandomBoxZone));

                BotController.IsBotRunToShelterPositionForBunker = false;
            }

            if (BotController.IsBotRunToShelterPositionForBunker && !IsInvoking(nameof(ChangePosition)))
                BotController.BotWalk(TargetPoint);
        }
        else
            BotController.IsBotRunToShelterPositionForBunker = false;
    }

    void BotPatroling()
    {
        if ((BotController.Bot == BotControl.BotStatus.IsPatrolling || BotController.BotLook != BotControl.LookingType.None) && !BotController.IsBotInDanger)
        {
            if (BotController.IsBotInBunker || BotController.BotLook == BotControl.LookingType.AtBunker)
            {
                if (!BotController.IsBotInDanger)
                    BotController.LookTarget(Bunkers[IsBotBunkerPoint].Looks[IsBotShelterPoint].transform.position, BotController.GetLookSpeedNormal);

                if (BotController.IsBotInBunker)
                    BotController.BotWalkStop();
            }
            else if (BotController.BotLook == BotControl.LookingType.AtPlayer)
            {
                CurrentLeftTimeLookAtPlayer -= Time.deltaTime;
                if (CurrentLeftTimeLookAtPlayer <= 0)
                {
                    CurrentLeftTimeLookAtPlayer = Random.Range(MinTimerLookAtPlayer, MaxTimerLookAtPlayer);
                    LookAtPlayer = Vector3.zero;
                }

                if (LookAtPlayer == Vector3.zero)
                {
                    LookAtPlayer = GameControl.Players[Random.Range(0, GameControl.Players.Length)].transform.position;

                    if (!BotController.IsBotInDanger)
                        BotController.LookTarget(LookAtPlayer, BotController.GetLookSpeedNormal);
                }
                else if (!BotController.IsBotInDanger)
                    BotController.LookTarget(LookAtPlayer, BotController.GetLookSpeedNormal);
            }
            else if (BotController.BotLook == BotControl.LookingType.AtRandomAngle && !BotController.IsBotInDanger)
            {
                BotController.LookTarget(Vector3.zero, BotController.GetLookSpeedNormal);
            }
        }
    }

    void BotFollowPlayerHuman()
    {
        if ((BotController.Bot == BotControl.BotStatus.IsFollowPlayer || BotController.Bot == BotControl.BotStatus.IsFollowPlayerAndIsAttack) && !BotController.IsBotInBunker && !BotController.IsBotInDangerEscape)
        {
            if (Biohazard.BotLeaderHuman != null && Biohazard.BotLeaderHuman.GetComponent<Player>().IsAlive && Vector2.Distance(transform.position, Bunkers[IsBotBunkerPoint].transform.position) > Vector2.Distance(transform.position, Biohazard.BotLeaderHuman.transform.position))
            {
                if (!BotController.IsBotFollowPlayer && Vector2.Distance(TargetPoint, Biohazard.BotLeaderHuman.transform.position) > RadiusPlayerNoFollow)
                {
                    TargetPoint = RandomBoxPoint(Biohazard.BotLeaderHuman.transform.position, Random.Range(MinRandomBoxZone, MaxRandomBoxZone), Random.Range(MinRandomBoxZone, MaxRandomBoxZone));
                    BotController.IsBotFollowPlayer = true;
                }
                else if ((Vector2.Distance(transform.position, TargetPoint) <= RadiusPlayerNoFollow && BotController.IsBotFollowPlayer && TargetPoint != Vector3.zero))
                {
                    StartCoroutine(boolBack("IsBotFollowPlayer"));
                    TargetPoint = Vector3.zero;
                    BotController.BotWalkStop();
                }
            }
            else
            {
                if (GameControl.Players.Length > 0)
                {
                    int countTry = GameControl.Players.Length;
                    int RandomIndex = 0;
                    Player TargetLeaderHumanPlayer;

                    while (!Biohazard.BotLeaderHuman && countTry > 0)
                    {
                        RandomIndex = Random.Range(0, GameControl.Players.Length);

                        TargetLeaderHumanPlayer = GameControl.Players[RandomIndex].GetComponent<Player>();

                        if (TargetLeaderHumanPlayer && TargetLeaderHumanPlayer.IsAlive && TargetLeaderHumanPlayer.PlayerTeam == BotController.PlayerControl.PlayerTeam)
                        {
                            Biohazard.BotLeaderHuman = GameControl.Players[RandomIndex];
                            BotController.IsBotFollowPlayer = true;
                            break;
                        }

                        countTry--;
                    }
                }

                if (!BotController.IsBotFollowPlayer)
                    BotController.ChangeBotStatus();
            }

            if (BotController.IsBotFollowPlayer)
                BotController.BotWalk(TargetPoint);
        }
        else
            BotController.IsBotFollowPlayer = false;
    }

    void BotDangerEscape()
    {
        if (BotController.IsBotInDanger && BotController.TargetAim)
        {
            Vector2 difference = transform.position - transform.up;

            if (!BotController.IsBotInDangerEscape && Vector2.Distance(transform.position, BotController.TargetAim.transform.position) <= RadiusDangerEsape)
            {
                TargetPoint = RandomBoxPoint(difference, Random.Range(MinRandomBoxZone, MaxRandomBoxZone), Random.Range(MinRandomBoxZone, MaxRandomBoxZone));

                while (TargetPoint != Vector3.zero && Vector2.Distance(TargetPoint, transform.position) >= Vector2.Distance(TargetPoint, BotController.TargetAim.transform.position))
                {
                    TargetPoint = RandomBoxPoint(difference, Random.Range(MinRandomBoxZone, MaxRandomBoxZone), Random.Range(MinRandomBoxZone, MaxRandomBoxZone));

                    if (TargetPoint == Vector3.zero)
                        return;
                }

                BotController.IsBotInDangerEscape = true;
            }
            else if (BotController.IsBotInDangerEscape)
            {
                if (TargetPoint != Vector3.zero && Vector2.Distance(TargetPoint, transform.position) >= Vector2.Distance(TargetPoint, BotController.TargetAim.transform.position))
                {
                    TargetPoint = RandomBoxPoint(difference, Random.Range(MinRandomBoxZone, MaxRandomBoxZone), Random.Range(MinRandomBoxZone, MaxRandomBoxZone));

                    while (TargetPoint != Vector3.zero && Vector2.Distance(TargetPoint, transform.position) >= Vector2.Distance(TargetPoint, BotController.TargetAim.transform.position))
                    {
                        TargetPoint = RandomBoxPoint(difference, Random.Range(MinRandomBoxZone, MaxRandomBoxZone), Random.Range(MinRandomBoxZone, MaxRandomBoxZone));

                        if (TargetPoint == Vector3.zero)
                            return;
                    }
                }
                else if (Vector2.Distance(TargetPoint, transform.position) <= Random.Range(0f, RadiusStopDistans))
                {
                    BotController.IsBotInDangerEscape = false;
                    TargetPoint = Vector3.zero;
                }
            }

            if (BotController.IsBotInDangerEscape)
                BotController.BotWalk(TargetPoint);
        }
        else
            BotController.IsBotInDangerEscape = false;
    }

    void Infection(GameObject Player, int id)
    {
        if (!isServer)
            return;

        if (id == BotController.PlayerControl.GetPlayerID)
        {
            StopAllCoroutines();
            CancelInvoke(nameof(ChangePosition));
            BotController.BotStop();

            CurrentLeftTimeLookAtPlayer = 0;
            TargetPoint = Vector3.zero;
            LookAtPlayer = Vector3.zero;

            if (BotController.Bot != BotControl.BotStatus.IsStoping)
                BotController.Bot = BotControl.BotStatus.IsRunning;
        }
    }

    IEnumerator boolBack(string parametr)
    {
        yield return new WaitForSeconds(Random.Range(MinDelayLogicActions, MaxDelayLogicActions));

        if (BotController.Bot != BotControl.BotStatus.IsStoping)
        {
            switch (parametr)
            {
                case "IsBotWalk":
                {
                    if (BotController.IsBotWalk)
                        BotController.IsBotWalk = !BotController.IsBotWalk;

                    break;
                }
                case "IsBotRunToShelterPositionForBunker":
                {
                    if (BotController.IsBotRunToShelterPositionForBunker)
                    {
                        BotController.IsBotRunToShelterPositionForBunker = !BotController.IsBotRunToShelterPositionForBunker;
                        BotController.Bot = BotControl.BotStatus.IsPatrolling;
                        BotController.BotWalkStop();

                        if (!IsInvoking(nameof(ChangePosition)))
                            Invoke(nameof(ChangePosition), Random.Range(MinTimerChangePosition, MaxTimerChangePosition));
                    }
                    break;
                }
                case "IsBotFollowPlayer":
                {
                    if (BotController.IsBotFollowPlayer)
                        BotController.IsBotFollowPlayer = !BotController.IsBotFollowPlayer;
                    break;
                }
            }
        }
    }

    void ChangePosition()
    {
        if (BotController.PlayerControl.IsAlive && BotController.PlayerControl.PlayerTeam == Player.Team.Human && BotController.Bot != BotControl.BotStatus.IsStoping && !BotController.IsBotInDangerEscape)
        {
            BotController.Bot = BotControl.BotStatus.IsRunningShelter;
            IsBotShelterPoint = Random.Range(0, Bunkers[IsBotBunkerPoint].Shelters.Length);

            if (Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].Zone != Vector2.zero)
                TargetPoint = RandomBoxPoint(Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].transform.position, Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].Zone.x, Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].Zone.y);
            else
                TargetPoint = RandomBoxPoint(Bunkers[IsBotBunkerPoint].Shelters[IsBotShelterPoint].transform.position, Random.Range(MinRandomBoxZone, MaxRandomBoxZone), Random.Range(MinRandomBoxZone, MaxRandomBoxZone));

            BotController.IsBotRunToShelterPositionForBunker = true;
            BotController.BotLook = BotControl.LookingType.None;
        }
    }

    private Vector3 RandomBoxPoint(Vector3 Point, float TempX, float TempY)
    {
        NavMeshPath path = new NavMeshPath();

        float RandomX = 0f;
        float RandomY = 0f;

        double CountTry = System.Math.Pow((double)(TempX * TempY), 2) * 1000;

        Vector3 temp = Vector3.zero;

        while (path.status == NavMeshPathStatus.PathInvalid && CountTry > 0)
        {
            RandomX = Random.Range(0, 10) > 5 ? -Random.Range(0, TempX) : Random.Range(0, TempX);
            RandomY = Random.Range(0, 10) > 5 ? -Random.Range(0, TempY) : Random.Range(0, TempY);

            temp = new Vector3(Point.x + RandomX,
                Point.y + RandomY);

            BotController.navMeshAgent.CalculatePath(temp, path);

            CountTry--;
        }

        if (CountTry <= 0)
            temp = Vector3.zero;

        len = temp;

        return temp;
    }

    //Vector3 RandomBoxPoint(Vector3 Point, float MinR, float MaxR)
    //{
    //    Vector3 temp = new Vector3(Point.x + (Random.Range(1, 10) > 5 ? (-1 * Random.Range(MinR, MaxR)) : Random.Range(MinR, MaxR)),
    //        Point.y + (Random.Range(1, 10) > 5 ? (-1 * Random.Range(MinR, MaxR)) : Random.Range(MinR, MaxR)), 0f);
    //    len = temp;
    //    return temp;
    //}
}

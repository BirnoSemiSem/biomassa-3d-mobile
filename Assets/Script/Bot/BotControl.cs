﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BotControl : NetworkBehaviour
{
    // Start is called before the first frame update

    public enum BotStatus
    {
        IsRunning,
        IsRunningAndIsAttack,
        IsRunningShelter,
        IsRunningShelterAndIsAttack,
        IsFollowPlayer,
        IsFollowPlayerAndIsAttack,
        IsPatrolling,
        IsStoping
    };

    public enum LookingType
    {
        AtPlayer,
        AtBunker,
        AtRandomAngle,
        None
    };

    private float AngleLooking;

    public BotStatus Bot;
    public LookingType BotLook;

    public bool IsBotWalk = false;
    public bool IsBotFollowPlayer = false;
    public bool IsBotRunToShelterPositionForBunker = false;
    public bool IsBotInBunker = false;
    public bool IsBotInDanger = false;
    public bool IsBotInDangerEscape = false;

    public bool IsBotHunt = false;
    public bool IsBotFollow = false;

    public NavMeshAgent navMeshAgent;

    [SyncVar] public Vector2 VelocityMove;
    public Player TargetAim;

    public Rigidbody2D rb;
    [SerializeField] private Animator FeetAnim;
    [SerializeField] private SpriteRenderer FeetSpr;

    public Player PlayerControl;
    public Transform FirePointTransform;

    private Coroutine CorBotDanger;

    [Header("Options Bot")]

    #region [Options for Bot Status]
    [SerializeField] private int SuccessRateOtherStatus = 25;
    [SerializeField] private int SuccessRateLogicGroupActivated = 30;
    public int GetSuccessRateOtherStatus => SuccessRateOtherStatus;
    public int GetSuccessRateLogicGroupActivated => SuccessRateLogicGroupActivated;

    [Space]

    [SerializeField] private float MinTimerChangeBotStatusBeforeStartEvent = 0.1f;
    [SerializeField] private float MaxTimerChangeBotStatusBeforeStartEvent = 15f;
    public float GetMinTimerChangeBotStatusBeforeStartEvent => MinTimerChangeBotStatusBeforeStartEvent;
    public float GetMaxTimerChangeBotStatusBeforeStartEvent => MaxTimerChangeBotStatusBeforeStartEvent;
    [SerializeField] private float MinTimerChangeBotStatusAfterStartEvent = 15f;
    [SerializeField] private float MaxTimerChangeBotStatusAfterStartEvent = 90f;
    public float GetMinTimerChangeBotStatusAfterStartEvent => MinTimerChangeBotStatusAfterStartEvent;
    public float GetMaxTimerChangeBotStatusAfterStartEvent => MaxTimerChangeBotStatusAfterStartEvent;
    #endregion

    [Space]

    #region [Options Detect/Attack]
    [SerializeField] private float MinTimerDelayDetect = 0.5f;
    [SerializeField] private float MaxTimerDelayDetect = 1.0f;
    [SerializeField] private float RadiusPlayerDetect = 2f;
    [SerializeField] private float RadiusKnifeAttack = 0.3f;
    public float GetMinTimerDelayDetect => MinTimerDelayDetect;
    public float GetMaxTimerDelayDetect => MaxTimerDelayDetect;
    public float GetRadiusPlayerDetect => RadiusPlayerDetect;
    public float GetRadiusKnifeAttack => RadiusKnifeAttack;
    #endregion

    [Space]

    #region [Options Weapon actions]
    [SerializeField] private int SuccessRateChangeWeapon = 50;
    public int GetSuccessRateChangeWeapon => SuccessRateChangeWeapon;

    [Space]

    private float CurrentLeftTimeChangeWeapon = 0;
    [SerializeField] private float MinTimerChangeWeapon = 5f;
    [SerializeField] private float MaxTimerChangeWeapon = 11f;
    public float GetMinTimerChangeWeapon => MinTimerChangeWeapon;
    public float GetMaxTimerChangeWeapon => MaxTimerChangeWeapon;

    [Space]

    private float CurrentLeftTimeWeaponForReload = 0;
    [SerializeField] private float MinTimerWeaponForReload = 3f;
    [SerializeField] private float MaxTimerWeaponForReload = 6f;
    public float GetMinTimerWeaponForReload => MinTimerWeaponForReload;
    public float GetMaxTimerWeaponForReload => MaxTimerWeaponForReload;
    #endregion

    [Space]

    #region [Options Look]
    private float CurrentLeftTimeLookStatus = 0;
    [SerializeField] private float MinTimerLookStatus = 5f;
    [SerializeField] private float MaxTimerLookStatus = 16f;
    public float GetMinTimerLookStatus => MinTimerLookStatus;
    public float GetMaxTimerLookStatus => MaxTimerLookStatus;

    [Space]

    [SerializeField] private float LookSpeedNormal = 2f;
    [SerializeField] private float LookSpeedDangerHuman = 10f;
    [SerializeField] private float LookSpeedDangerZombie = 5f;
    public float GetLookSpeedNormal => LookSpeedNormal;
    public float GetLookSpeedDangerHuman => LookSpeedDangerHuman;
    public float GetLookSpeedDangerZombie => LookSpeedDangerZombie;
    #endregion

    [Space]

    [SerializeField] private WeaponControl WeaponControl;
    [SerializeField] private GameObject FirePoint;

    private bool IsVisible;
    [SerializeField] private bool IsOptimization;

    public bool IsDebug = false;

    void Start()
    {
        if (navMeshAgent == null)
            navMeshAgent = GetComponent<NavMeshAgent>();

        if (rb == null)
            rb = GetComponent<Rigidbody2D>();

        if (WeaponControl == null)
            WeaponControl = GetComponent<WeaponControl>();

        if (isServer)
        {
            Bot = BotStatus.IsStoping;
            BotLook = LookingType.None;
            LookTarget(Vector3.zero, LookSpeedNormal);
        }

        CurrentLeftTimeWeaponForReload = UnityEngine.Random.Range(MinTimerWeaponForReload, MaxTimerWeaponForReload);
        CurrentLeftTimeChangeWeapon = UnityEngine.Random.Range(MinTimerChangeWeapon, MaxTimerChangeWeapon);
        CurrentLeftTimeLookStatus = UnityEngine.Random.Range(MinTimerLookStatus, MaxTimerLookStatus);

        CorBotDanger = null;

        navMeshAgent.updateRotation = false;
        navMeshAgent.updateUpAxis = false;
    }

    void OnEnable()
    {
        GameControl.EventPreStartRound += NewRound;
        GameControl.EventSpawnPlayer += Spawn;
        GameControl.EventRoundStart += StartRound;
        GameControl.EventRoundEnd += EndRound;
        Biohazard.EventBiohazardStart += GameStart;
    }

    void OnDisable()
    {
        GameControl.EventPreStartRound -= NewRound;
        GameControl.EventSpawnPlayer -= Spawn;
        GameControl.EventRoundStart -= StartRound;
        GameControl.EventRoundEnd -= EndRound;
        Biohazard.EventBiohazardStart -= GameStart;
    }

    public void NewRound()
    {
        if (!isServer)
            return;

        if (!IsDebug)
        {
            Bot = BotStatus.IsStoping;
            BotStop();
        }
    }

    public void Spawn(GameObject Player, int id)
    {
        if (id != PlayerControl.GetPlayerID)
            return;

        if(GameControl.IsGame)
        {
            if(Biohazard.IsModBiohazardStart)
            {
                if (PlayerControl.PlayerTeam == global::Player.Team.Zombie)
                    Bot = BotStatus.IsRunning;
            }
        }

        CurrentLeftTimeWeaponForReload = UnityEngine.Random.Range(MinTimerWeaponForReload, MaxTimerWeaponForReload);
        CurrentLeftTimeChangeWeapon = UnityEngine.Random.Range(MinTimerChangeWeapon, MaxTimerChangeWeapon);
        CurrentLeftTimeLookStatus = UnityEngine.Random.Range(MinTimerLookStatus, MaxTimerLookStatus);

        if (CorBotDanger != null)
        {
            StopCoroutine(CorBotDanger);
            CorBotDanger = null;
        }
    }

    public void StartRound()
    {
        if (!isServer)
            return;

        if (!IsDebug)
        {
            if (PlayerControl.IsAlive)
            {
                if (UnityEngine.Random.Range(1, 101) <= SuccessRateOtherStatus)
                {
                    Bot = BotStatus.IsRunningShelter;
                }
                else
                {
                    Bot = BotStatus.IsRunning;
                    Invoke(nameof(ChangeBotStatus), UnityEngine.Random.Range(MinTimerChangeBotStatusBeforeStartEvent, MaxTimerChangeBotStatusBeforeStartEvent));
                }
            }
        }
    }

    public void EndRound()
    {
        if (!isServer)
            return;

        if (!IsDebug)
        {
            BotStop();
            Bot = BotStatus.IsRunning;
        }
    }

    public void GameStart()
    {
        if (!isServer)
            return;

        if (PlayerControl.IsAlive && Bot != BotStatus.IsStoping)
        {
            if (UnityEngine.Random.Range(1, 101) <= SuccessRateOtherStatus)
                Invoke(nameof(ChangeBotStatus), UnityEngine.Random.Range(MinTimerChangeBotStatusBeforeStartEvent, MaxTimerChangeBotStatusBeforeStartEvent));
            else
                Bot = BotStatus.IsRunningShelter;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerControl.IsAlive)
        {
            if(isServer)
                VelocityMove = navMeshAgent.velocity;

            switch (PlayerControl.PlayerTeam)
            {
                case Player.Team.Zombie: { navMeshAgent.speed = PlayerControl.ZombieClass.Speed * (PlayerControl.IsPainShock ? PlayerControl.ZombieClass.PainShock : 1f); break; }
                case Player.Team.Human: { navMeshAgent.speed = PlayerControl.HumanClass.Speed * (PlayerControl.IsPainShock ? PlayerControl.HumanClass.PainShock : 1f); break; }
            }

            if (isServer)
            {
                if (!Biohazard.IsBotHuntForZombies || !Biohazard.IsBotHuntForHumans)
                    if (!IsInvoking(nameof(ChangeBotStatus)))
                        Invoke(nameof(ChangeBotStatus), UnityEngine.Random.Range(MinTimerChangeBotStatusAfterStartEvent, MaxTimerChangeBotStatusAfterStartEvent));

                if (!IsBotInDanger)
                {
                    if (Biohazard.IsBotHuntForZombies && Bot != BotStatus.IsStoping && !IsBotHunt)
                    {
                        if (PlayerControl.PlayerTeam == Player.Team.Human)
                            Bot = BotStatus.IsRunning;
                        IsBotHunt = true;
                    }
                    else if (!Biohazard.IsBotHuntForZombies && Bot != BotStatus.IsStoping && IsBotHunt)
                    {
                        if (PlayerControl.PlayerTeam == Player.Team.Human)
                            Bot = BotStatus.IsRunningShelter;
                        IsBotHunt = false;
                    }

                    if (Biohazard.IsBotGroupZombie && PlayerControl.PlayerTeam == Player.Team.Zombie && !IsBotFollow)
                    {
                        Bot = BotStatus.IsFollowPlayer;
                        IsBotFollow = true;
                    }
                    else if (!Biohazard.IsBotGroupZombie && PlayerControl.PlayerTeam == Player.Team.Zombie && IsBotFollow)
                    {
                        ChangeBotStatus();
                        IsBotFollow = false;
                    }

                    if (Biohazard.IsBotGroupHuman && PlayerControl.PlayerTeam == Player.Team.Human && !IsBotFollow)
                    {
                        Bot = BotStatus.IsFollowPlayer;
                        IsBotFollow = true;
                    }
                    else if (!Biohazard.IsBotGroupHuman && PlayerControl.PlayerTeam == Player.Team.Human && IsBotFollow)
                    {
                        ChangeBotStatus();
                        IsBotFollow = false;
                    }
                }

                if (IsBotInDanger && TargetAim)
                {
                    switch (PlayerControl.PlayerTeam)
                    {
                        case Player.Team.Zombie:
                        {
                            if (Vector2.Distance(transform.position, TargetAim.transform.position) <= RadiusKnifeAttack)
                                WeaponControl.ClickButtonFireEnter();
                            else
                                WeaponControl.ClickButtonFireExit();
                            break;
                        }
                        case Player.Team.Human:
                        {
                            if (PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip > 0)
                                WeaponControl.ClickButtonFireEnter();
                            else
                            {
                                if (WeaponControl.IsFire)
                                {
                                    WeaponControl.ClickButtonFireExit();

                                    if (!WeaponControl.IsDraw && UnityEngine.Random.Range(1, 101) < SuccessRateChangeWeapon)
                                        WeaponControl.ClickChangeWeapon(UnityEngine.Random.Range(0, 2));
                                }

                                if(!WeaponControl.IsDraw && !WeaponControl.IsReload && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo > 0)
                                    WeaponControl.ClickButtonReloadEnter();
                            }

                            if (CurrentLeftTimeWeaponForReload < MinTimerWeaponForReload)
                                CurrentLeftTimeWeaponForReload = UnityEngine.Random.Range(MinTimerWeaponForReload, MaxTimerWeaponForReload);

                            if (CurrentLeftTimeChangeWeapon < MinTimerChangeWeapon)
                                CurrentLeftTimeChangeWeapon = UnityEngine.Random.Range(MinTimerChangeWeapon, MaxTimerChangeWeapon);

                            break;
                        }
                    }
                }
                else
                {
                    if (WeaponControl.IsFire)
                        WeaponControl.ClickButtonFireExit();

                    if (!WeaponControl.IsReload && !WeaponControl.IsDraw)
                    {
                        if (CurrentLeftTimeWeaponForReload >= 0)
                            CurrentLeftTimeWeaponForReload -= Time.deltaTime;

                        if (CurrentLeftTimeChangeWeapon >= 0)
                            CurrentLeftTimeChangeWeapon -= Time.deltaTime;

                        if (CurrentLeftTimeWeaponForReload < 0 && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip < PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxClip && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo > 0)
                        {
                            WeaponControl.ClickButtonReloadEnter();
                            CurrentLeftTimeWeaponForReload = UnityEngine.Random.Range(MinTimerWeaponForReload, MaxTimerWeaponForReload);
                        }

                        if (CurrentLeftTimeChangeWeapon < 0)
                        {
                            if (UnityEngine.Random.Range(1, 101) < SuccessRateChangeWeapon)
                                WeaponControl.ClickChangeWeapon(UnityEngine.Random.Range(0, 2));

                            CurrentLeftTimeChangeWeapon = UnityEngine.Random.Range(MinTimerChangeWeapon, MaxTimerChangeWeapon);
                        }
                    }
                }
            }

            AnimWalk();

            if(isServer)
            {
                if (CorBotDanger == null)
                    CorBotDanger = StartCoroutine(BotDanger());

                if (TargetAim &&
                    PlayerControl.PlayerTeam != TargetAim.PlayerTeam &&
                    TargetAim.IsAlive)
                {
                    switch (PlayerControl.PlayerTeam)
                    {
                        case Player.Team.Zombie:
                        {
                            LookTarget(TargetAim.transform.position, LookSpeedDangerZombie);
                            break;
                        }
                        case Player.Team.Human:
                        {
                            LookTarget(TargetAim.transform.position, LookSpeedDangerHuman);
                            break;
                        }
                    }
                }
                else
                    TargetAim = null;
            }

            if(IsDebug)
                Debug.DrawLine(transform.position, FirePointTransform.position, Color.green);
        }
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawLine(transform.position, FirePointTransform.position);
    //}

    public void AnimWalk()
    {
        if (VelocityMove != Vector2.zero && Bot != BotStatus.IsStoping)
        {
            if (!FeetAnim.enabled && IsVisible)
                FeetAnim.enabled = true;

            if (Mathf.RoundToInt(FeetAnim.gameObject.transform.eulerAngles.z) != Mathf.RoundToInt(-Mathf.Atan2(VelocityMove.x, VelocityMove.y) * Mathf.Rad2Deg) && IsVisible)
                FeetAnim.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -Mathf.Atan2(VelocityMove.x, VelocityMove.y) * Mathf.Rad2Deg));

            if (Bot == BotStatus.IsRunning || Bot == BotStatus.IsFollowPlayer || Bot == BotStatus.IsPatrolling || Bot == BotStatus.IsRunningShelter)
            {
                if (BotLook == LookingType.None)
                    LookTarget((Vector3)VelocityMove + transform.position, LookSpeedNormal);
            }
        }
        else if (FeetAnim.enabled)
            FeetAnim.enabled = false;
    }

    public void LookTarget(Vector3 TargetPosition, float speed)
    {
        if (!isServer)
            return;

        if (Bot != BotStatus.IsStoping)
        {
            if (TargetPosition != Vector3.zero || BotLook == LookingType.AtRandomAngle)
            {
                if (BotLook != LookingType.AtRandomAngle || IsBotInDanger)
                {
                    Vector2 Look = TargetPosition - transform.position;
                    AngleLooking = Mathf.Atan2(Look.x, Look.y) * Mathf.Rad2Deg;
                    AngleLooking = -AngleLooking;
                    //Debug.Log("Test AngleLooking: " + AngleLooking);
                }
                else if (Mathf.RoundToInt(transform.eulerAngles.z) == Mathf.RoundToInt(AngleLooking))
                    AngleLooking = UnityEngine.Random.Range(0f, 360.0f);

                if(Mathf.RoundToInt(transform.eulerAngles.z) != Mathf.RoundToInt(AngleLooking))
                    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 0, AngleLooking)), speed * Time.deltaTime);
            }

            if (CurrentLeftTimeLookStatus >= 0)
                CurrentLeftTimeLookStatus -= Time.deltaTime;

            if (CurrentLeftTimeLookStatus < 0)
            {
                if (!IsBotInDanger)
                    BotLook = (LookingType)UnityEngine.Random.Range(0, Enum.GetNames(typeof(LookingType)).Length);

                CurrentLeftTimeLookStatus = UnityEngine.Random.Range(MinTimerLookStatus, MaxTimerLookStatus);

                if(BotLook == LookingType.AtRandomAngle)
                    AngleLooking = UnityEngine.Random.Range(0f, 360.0f);
            }
        }
    }

    public void ChangeBotStatus()
    {
        if (!isServer)
            return;

        if (!Biohazard.IsModBiohazardStart)
            return;

        if (PlayerControl.IsAlive && Bot != BotStatus.IsStoping)
        {
            switch(PlayerControl.PlayerTeam)
            {
                case Player.Team.Zombie:
                {
                    switch(UnityEngine.Random.Range(0,2))
                    {
                        case 0:
                        {
                            Bot = BotStatus.IsRunning;
                            break;
                        }
                        case 1:
                        {
                            if(gameObject != Biohazard.BotLeaderZombie)
                                Bot = BotStatus.IsFollowPlayer;
                            else
                                Bot = BotStatus.IsRunning;

                            if (!Biohazard.IsBotGroupZombie && UnityEngine.Random.Range(1, 101) < SuccessRateLogicGroupActivated)
                                Biohazard.IsBotGroupZombie = true;
                            break;
                        }
                    }
                    break;
                }
                case Player.Team.Human:
                {
                    switch (UnityEngine.Random.Range(0, 4))
                    {
                        case 0:
                        {
                            Bot = BotStatus.IsRunningShelter;
                            break;
                        }
                        case 1:
                        {
                            if (Biohazard.IsModBiohazardStart)
                            {
                                if (gameObject != Biohazard.BotLeaderHuman)
                                    Bot = BotStatus.IsFollowPlayer;
                                else
                                    Bot = BotStatus.IsRunningShelter;

                                if (!Biohazard.IsBotGroupHuman && UnityEngine.Random.Range(1, 101) < SuccessRateLogicGroupActivated)
                                    Biohazard.IsBotGroupHuman = true;
                            }
                            else
                                Bot = BotStatus.IsRunningShelter;
                            break;
                        }
                        case 2:
                        {
                            Bot = BotStatus.IsPatrolling;
                            break;
                        }
                        case 3:
                        {
                            if(!Biohazard.IsModBiohazardStart)
                                Bot = BotStatus.IsRunning;
                            else
                                Bot = BotStatus.IsRunningShelter;
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }

    public void BotWalk(Vector3 Target)
    {
        if (!navMeshAgent.isOnNavMesh)
            return;

        if(PlayerControl.IsAlive && Target != navMeshAgent.pathEndPosition)
        {
            navMeshAgent.SetDestination(Target);
        }
    }

    public void BotWalkStop()
    {
        if (!navMeshAgent.isOnNavMesh)
            return;

        if (PlayerControl.IsAlive)
        {
            navMeshAgent.ResetPath();
        }
    }

    public void BotStop()
    {
        if (!navMeshAgent.isOnNavMesh)
            return;

        if (PlayerControl.IsAlive)
        {
            IsBotWalk = false;
            IsBotInBunker = false;
            IsBotFollowPlayer = false;
            IsBotFollow = false;
            IsBotRunToShelterPositionForBunker = false;
            IsBotInDanger = false;
            IsBotInDangerEscape = false;

            TargetAim = null;

            CurrentLeftTimeWeaponForReload = UnityEngine.Random.Range(MinTimerWeaponForReload, MaxTimerWeaponForReload);
            CurrentLeftTimeChangeWeapon = UnityEngine.Random.Range(MinTimerChangeWeapon, MaxTimerChangeWeapon);
            CurrentLeftTimeLookStatus = UnityEngine.Random.Range(MinTimerLookStatus, MaxTimerLookStatus);

            if (CorBotDanger != null)
            {
                StopCoroutine(CorBotDanger);
                CorBotDanger = null;
            }

            BotWalkStop();
            BotLook = LookingType.None;

            navMeshAgent.isStopped = false;
        }
    }

    private IEnumerator BotDanger()
    {
        if(TargetAim)
            yield return new WaitForSeconds(UnityEngine.Random.Range(MinTimerDelayDetect, MaxTimerDelayDetect));
        else
            yield return new WaitForSeconds(0.1f);

        TargetAim = null;
        IsBotInDanger = false;

        GameObject TempTargetEnemy = null;

        if (GameControl.Players.Length > 0)
        {
            Player _playerTarget;
            for (int i = 0; i < GameControl.Players.Length; i++)
            {
                if (!GameControl.Players[i])
                {
                    CorBotDanger = null;
                    yield break;
                }

                _playerTarget = GameControl.Players[i].GetComponent<Player>();

                if (_playerTarget != null && _playerTarget.IsAlive &&
                    _playerTarget.PlayerTeam != PlayerControl.PlayerTeam &&
                    Vector2.Distance(transform.position, GameControl.Players[i].transform.position) <= RadiusPlayerDetect)
                {
                    Vector2 TargetLocation = GameControl.Players[i].transform.position - transform.position;

                    RaycastHit2D[] hitRay = Physics2D.RaycastAll(transform.position, TargetLocation.normalized, RadiusPlayerDetect);
                    Player _HitTarget;

                    for(int id = 0; id < hitRay.Length; id++)
                    {
                        _HitTarget = hitRay[id].collider.GetComponent<Player>();

                        if(_HitTarget && _HitTarget.tag == "Player")
                        {
                            if(GameControl.Players[i] == _HitTarget.gameObject &&
                                _HitTarget.GetPlayerID != PlayerControl.GetPlayerID &&
                                PlayerControl.PlayerTeam != _HitTarget.PlayerTeam &&
                                _HitTarget.IsAlive)
                            {
                                if (!TempTargetEnemy)
                                    TempTargetEnemy = _HitTarget.gameObject;
                                else if (Vector2.Distance(transform.position, TempTargetEnemy.transform.position) > Vector2.Distance(transform.position, _HitTarget.transform.position))
                                    TempTargetEnemy = _HitTarget.gameObject;
                            }
                        }
                        else
                            break;
                    }
                }
            }
        }

        if (TempTargetEnemy)
        {
            IsBotInDanger = true;
            TargetAim = TempTargetEnemy.GetComponent<Player>();

            switch (PlayerControl.PlayerTeam)
            {
                case Player.Team.Zombie:
                {
                    if (Bot != BotControl.BotStatus.IsStoping)
                        Bot = BotControl.BotStatus.IsRunningAndIsAttack;

                    break;
                }
                case Player.Team.Human:
                {
                    if (Bot != BotControl.BotStatus.IsStoping)
                    {
                        switch (Bot)
                        {
                            case BotControl.BotStatus.IsRunning: { Bot = BotControl.BotStatus.IsRunningAndIsAttack; break; }
                            case BotControl.BotStatus.IsRunningShelter: { Bot = BotControl.BotStatus.IsRunningShelterAndIsAttack; break; }
                            case BotControl.BotStatus.IsFollowPlayer: { Bot = BotControl.BotStatus.IsFollowPlayerAndIsAttack; break; }
                        }
                    }

                    break;
                }
            }
        }

        if(IsBotInDangerEscape && !TargetAim)
            IsBotInDangerEscape = false;

        CorBotDanger = null;
    }

    void OnBecameVisible()
    {
        if (gameObject && FeetAnim && IsOptimization)
        {
            FeetAnim.enabled = true;
            FeetSpr.color = new Color(255f, 255f, 255f, 255f);
            IsVisible = true;
        }
    }

    void OnBecameInvisible()
    {
        if (gameObject && FeetAnim && IsOptimization)
        {
            FeetAnim.enabled = false;
            FeetSpr.color = new Color(255f, 255f, 255f, 0f);
            IsVisible = false;
        }
    }
}

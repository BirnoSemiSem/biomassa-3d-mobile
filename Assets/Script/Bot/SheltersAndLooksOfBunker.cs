﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheltersAndLooksOfBunker : MonoBehaviour
{
    public GameObject Bunker;
    public ShelterZone[] Shelters;
    public GameObject[] Looks;

    public float RadiusBunker = 0f;

    [SerializeField] private bool IsDebugMod = false;

    private void OnDrawGizmos()
    {
        if (!IsDebugMod)
            return;

        Gizmos.color = Color.grey;

        Gizmos.DrawWireSphere(transform.position, RadiusBunker);
    }
}
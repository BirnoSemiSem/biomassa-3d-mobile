﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameControl : NetworkBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private TextMeshProUGUI RoundText;

    [SerializeField] private Vector3[] SpawnsPoint;
    [SerializeField] private Quaternion[] SpawnsRotation;

    [SerializeField] private GameObject[] ClassicPrimaryWeapon;
    [SerializeField] private GameObject[] ClassicSecondWeapon;
    [SerializeField] private GameObject[] ClassicGrenadeWeapon;
    [SerializeField] private GameObject[] ClassicKnifeWeapon;

    [SerializeField] private GameObject ClassicZombieKnife;

    [SerializeField] private GameObject[] NewPrimaryWeapon;
    [SerializeField] private GameObject[] NewSecondWeapon;
    [SerializeField] private GameObject[] NewGrenadeWeapon;
    [SerializeField] private GameObject[] NewKnifeWeapon;

    [SerializeField] private GameObject NewZombieKnife;

    private GameObject[] PrimaryWeaponParametrs;
    private GameObject[] SecondWeaponParametrs;
    private GameObject[] GrenadeWeaponParametrs;
    private GameObject[] KnifeWeaponParametrs;

    private GameObject ZombieKnifeParametrs;

    [SerializeField] private float TimeMinet = 5f;
    [SyncVar] [SerializeField] private float RoundTime;
    [SerializeField] private int FreezeTime= 5;
    [SerializeField] private float RoundTimeEnd = 0;
    [SerializeField] private int TimeEndRound = 3;

    static public bool IsNewRound = false;
    static public bool IsFreeze = false;
    static public bool IsStartRound = false;
    static public bool IsEndRound = false;
    static public bool IsGame = false;
    static public bool IsForcedEndRound = false;

    static public bool IsInfinityAmmo = true;
    static public bool IsRespawnInGame = true;

    private int TextTimeMinut;
    private int TextTimeSecond;

    [SerializeField] private GameObject Bot;
    [SerializeField] private GameObject BotParent;

    [SerializeField] private GameObject PlayerParent;

    static public List<GameObject> Bots;

    [SerializeField] private int BotQuota = 0;

    static public GameObject[] Players;

    //Delegate
    static public UnityAction EventPreStartRound;
    static public UnityAction EventRoundStart;
    static public UnityAction<GameObject, int> EventSpawnPlayer;
    static public UnityAction EventRoundEnd;

    void Start()
    {
        Application.targetFrameRate = 120;

        Initialization();
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        Initialization();
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        Initialization();
    }

    void Initialization()
    {
        GameObject[] Spawns = GameObject.FindGameObjectsWithTag("Spawn");

        SpawnsPoint = new Vector3[Spawns.Length];
        SpawnsRotation = new Quaternion[Spawns.Length];

        for (int i = 0; i < Spawns.Length; i++)
        {
            SpawnsPoint[i] = Spawns[i].transform.position;
            SpawnsRotation[i] = Spawns[i].transform.rotation;
        }

        BotQuota = SpecificalOptions.specificalOptions.ServerOptions.CountBots;
        Bots = new List<GameObject>();

        RoundTime = 0;

        IsNewRound = false;
        IsFreeze = false;
        IsStartRound = false;
        IsEndRound = false;
        IsForcedEndRound = true;
        IsGame = false;
    }

    void OnEnable()
    {
        Player.EventsPlayerStart += WeaponUpdate;

        Damage.EventDeathPlayer += PlayerDeath;
    }

    void OnDisable()
    {
        Player.EventsPlayerStart -= WeaponUpdate;

        Damage.EventDeathPlayer -= PlayerDeath;
    }

    // Update is called once per frame
    void Update()
    {
        Players = GameObject.FindGameObjectsWithTag("Player");

        if (isServer)
        {
            if (RoundTime <= 0 && RoundTimeEnd <= 0 && IsEndRound)
            {
                RoundTime = FreezeTime;
                NewRound();
            }
            else
            if ((IsNewRound && RoundTime <= 0) || IsForcedEndRound)
            {
                if (IsForcedEndRound)
                {
                    RoundTimeEnd = TimeEndRound;
                    RoundTime = 0f;
                    GameEnd();
                }
                else
                if (IsFreeze)
                {
                    RoundTime = 60f * TimeMinet;
                    GameStart();
                }
                else
                if (IsGame)
                {
                    RoundTimeEnd = TimeEndRound;
                    GameEnd();
                }
            }
            else
            {
                if (IsEndRound && RoundTimeEnd >= 0)
                    RoundTimeEnd -= Time.deltaTime;
                else
                    RoundTime -= Time.deltaTime;
            }
        }

        TextTimeMinut = (int)(RoundTime / 60);
        TextTimeSecond = (int)(RoundTime % 60);
        RoundText.text = $"{(TextTimeMinut > 9 ? "" : "0")}{TextTimeMinut}:{(TextTimeSecond > 9 ? "" : "0")}{TextTimeSecond}";
    }

    public void NewRound()
    {
        if (isServer)
            RpcNewRound();

        IsNewRound = true;
        IsFreeze = true;
        IsEndRound = false;

        IsRespawnInGame = true;

        EventPreStartRound?.Invoke();

        UpdateWeapon();

        if (isServer)
        {
            if (Bots.Count < BotQuota)
            {
                int NewCountBots = BotQuota - Bots.Count;
                for (int i = 0; i < NewCountBots; i++)
                {
                    GameObject NewBot = Instantiate(Bot);
                    NetworkServer.Spawn(NewBot);
                    NewBot.transform.parent = BotParent.transform;

                    int SpawnIndex = UnityEngine.Random.Range(0, SpawnsPoint.Length);

                    NewBot.GetComponent<NavMeshAgent>().nextPosition = SpawnsPoint[SpawnIndex];
                    NewBot.transform.position = SpawnsPoint[SpawnIndex];

                    NewBot.GetComponent<NavMeshAgent>().enabled = false;
                    NewBot.GetComponent<NavMeshAgent>().enabled = true;

                    NewBot.GetComponent<BotControl>().Bot = BotControl.BotStatus.IsStoping;

                    Bots.Add(NewBot);

                    RpcCreateBot(NewBot, SpawnIndex);
                }
            }
            else if(Bots.Count > BotQuota)
            {
                int countDestroy = Bots.Count - BotQuota;

                for (int i = 0; i < countDestroy; i++)
                {
                    NetworkServer.Destroy(Bots[i]);
                    Bots.Remove(Bots[i]);

                    RpcDestroyBot(Bots[i]);
                }
            }
        }

        Players = GameObject.FindGameObjectsWithTag("Player");

        if (Players != null)
        {
            for (int i = 0; i < Players.Length; i++)
            {
                if (Players[i].GetComponent<Player>().PlayerTeam != global::Player.Team.Spectator)
                {
                    if (isServer)
                    {
                        Players[i].GetComponent<Player>().RandomSpawn = UnityEngine.Random.Range(0, SpawnsPoint.Length);

                        Players[i].GetComponent<Player>().RandomWeaponPrimary = UnityEngine.Random.Range(0, PrimaryWeaponParametrs.Length);
                        Players[i].GetComponent<Player>().RandomWeaponSecond = UnityEngine.Random.Range(0, SecondWeaponParametrs.Length);
                        Players[i].GetComponent<Player>().RandomWeaponGrenade = UnityEngine.Random.Range(0, GrenadeWeaponParametrs.Length);
                    }

                    StartCoroutine(RespawnPlayer(Players[i], 0.1f, Player.Team.Human));
                }
            }
        }
    }

    public void GameStart()
    {
        if (isServer)
            RpcGameStart();

        IsStartRound = true;
        IsFreeze = false;
        IsGame = true;

        EventRoundStart?.Invoke();
    }

    public void GameEnd()
    {
        if(isServer)
            RpcGameEnd();

        StopAllCoroutines();

        IsEndRound = true;
        IsStartRound = false;
        IsGame = false;
        IsNewRound = false;
        IsFreeze = false;
        IsForcedEndRound = false;

        IsRespawnInGame = false;

        EventRoundEnd?.Invoke();
    }

    public void PlayerDeath(GameObject Player, int id)
    {
        if (!isServer)
            return;

        if (!IsRespawnInGame)
            return;

        if (IsGame && Biohazard.IsModBiohazardStart)
        {
            NetworkIdentity opponentIdentity = Player.GetComponent<NetworkIdentity>();

            if (opponentIdentity.connectionToClient != null)
                TargetRespawn(opponentIdentity.connectionToClient, Player);

            StartCoroutine(RespawnPlayer(Player, 5f, global::Player.Team.Zombie));
        }
    }

    public IEnumerator RespawnPlayer(GameObject Player, float Timer, Player.Team team)
    {
        yield return new WaitForSeconds(Timer);

        if (Player == null)
            yield break;

        Player _player = Player.GetComponent<Player>();

        if (_player.PlayerTeam == global::Player.Team.Spectator)
            yield break;

        if (Player.transform.parent != PlayerParent.transform && !_player.Bot)
            Player.transform.parent = PlayerParent.transform;

        //if (isServer)
        //{
            if(_player.Bot && isServer)
                _player.Bot.navMeshAgent.enabled = false;

            Player.transform.position = SpawnsPoint[_player.RandomSpawn];
            Player.transform.rotation = SpawnsRotation[_player.RandomSpawn];
        //}

        _player.PlayerTeam = team;

        _player.IsAlive = true;
        _player.IsPainShock = false;
        _player.IsRegeneration = false;
        _player.Health = 100;
        _player.Armor = 0;
        _player.ActiveWeapon = 0;

        _player.Pain = null;
        _player.Regeneration = null;

        //Player.transform.Find("Feet").gameObject.SetActive(true);
        //Player.transform.Find("Weapon").gameObject.SetActive(true);
        //Player.transform.Find("PointTarget").gameObject.SetActive(true);

        Player.GetComponent<Collider>().enabled = true;

        if (_player.Bot && isServer)
            _player.Bot.navMeshAgent.enabled = true;

        _player.Animator.runtimeAnimatorController = _player.HumanClass.AnimatorController;
        _player.Animator.avatar = _player.HumanClass.AnimatorAvatar;

        _player.Model.sharedMesh = _player.HumanClass.Model;

        _player.Model.materials = _player.HumanClass.ModelMaterial;

        if (!_player.Model.enabled)
            _player.Model.enabled = true;

        //_player.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 255f);

        //switch (_player.PlayerTeam)
        //{
        //    case global::Player.Team.Zombie: { if (!String.IsNullOrEmpty(_player.ZombieClass.Name)) _player.Animator.Play(_player.ZombieClass.Name + "-" + global::Player.AnimationPlayer.Idle.ToString()); break; }
        //    case global::Player.Team.Human: { if (!String.IsNullOrEmpty(_player.HumanClass.Name)) _player.Animator.Play(_player.HumanClass.Name + "-" + global::Player.AnimationPlayer.Idle.ToString()); break; }
        //}

        //if (_player.PlayerTeam == global::Player.Team.Human)
        //{
        //    _player.Weapons = null;
        //    _player.Weapons = new Player.WeaponPlayer[3];

        //    SetWeapon(_player, 0, PrimaryWeaponParametrs[_player.RandomWeaponPrimary].GetComponent<WeaponSystem>());
        //    SetWeapon(_player, 1, SecondWeaponParametrs[_player.RandomWeaponSecond].GetComponent<WeaponSystem>());
        //    //SetWeapon(_player, 2, GrenadeWeapon[_player.RandomWeaponGrenade].GetComponent<WeaponSystem>());
        //}

        EventSpawnPlayer?.Invoke(Player, _player.GetPlayerID);

        yield return null;
    }

    void SetWeapon(Player player, int slot, WeaponSystem weapon)
    {
        player.Weapons[slot].PositionSpr = weapon.PositionSpr;

        player.Weapons[slot].Weapon = weapon.Weapon;

        player.Weapons[slot].NameWeapon = weapon.NameWeapon;

        player.Weapons[slot].MaxClip = weapon.MaxClip;
        player.Weapons[slot].Clip = weapon.Clip;
        player.Weapons[slot].MaxAmmo = weapon.MaxAmmo;
        player.Weapons[slot].Ammo = weapon.Ammo;

        player.Weapons[slot].MaxRecoil = weapon.MaxRecoil;

        player.Weapons[slot].FireSpeed = weapon.FireSpeed;
        player.Weapons[slot].SpeedBullet = weapon.SpeedBullet;
        player.Weapons[slot].ReloadTime = weapon.ReloadTime;

        player.Weapons[slot].MinDmg = weapon.MinDmg;
        player.Weapons[slot].MaxDmg = weapon.MaxDmg;
        player.Weapons[slot].HSDmg = weapon.HSDmg;

        player.Weapons[slot].Knockback = weapon.Knockback;

        player.Weapons[slot].WeaponSpr = weapon.WeaponSpr;

        player.Weapons[slot].FireClip = weapon.FireClip;
        player.Weapons[slot].DrawClip = weapon.DrawClip;
        player.Weapons[slot].ReloadClips = weapon.ReloadClips;

        player.Weapons[slot].KnifeMissClips = weapon.KnifeMissClips;
        player.Weapons[slot].KnifeStrikeClips = weapon.KnifeStrikeClips;

        if (slot == player.ActiveWeapon)
        {
            player.FireClip = weapon.FireClip;
            player.DrawClip = weapon.DrawClip;
            player.ReloadClips = weapon.ReloadClips;

            player.KnifeMissClips = weapon.KnifeMissClips;
            player.KnifeStrikeClips = weapon.KnifeStrikeClips;

            //player.WeaponTransform.localPosition = player.Weapons[slot].PositionSpr;
            //player.WeaponSprite.sprite = player.Weapons[slot].WeaponSpr;

            if (player.PlayerTeam == Player.Team.Human)
            {
                switch (player.ActiveWeapon)
                {
                    case 0: { if (!String.IsNullOrEmpty(player.HumanClass.Name)) player.Animator.Play(player.HumanClass.Name + "-" + global::Player.AnimationPlayer.IdleMachineGun.ToString()); break; }
                    case 1: { if (!String.IsNullOrEmpty(player.HumanClass.Name)) player.Animator.Play(player.HumanClass.Name + "-" + global::Player.AnimationPlayer.IdlePistolGun.ToString()); break; }
                    case 2: { if (!String.IsNullOrEmpty(player.HumanClass.Name)) player.Animator.Play(player.HumanClass.Name + "-" + global::Player.AnimationPlayer.IdleGrenade.ToString()); break; }
                }
            }
        }
    }

    public void WeaponUpdate(GameObject PlayerObject)
    {
        Player _player = PlayerObject.GetComponent<Player>();

        if (!_player.IsAlive)
            return;

        switch(_player.PlayerTeam)
        {
            case Player.Team.Zombie:
            {
                _player.Weapons = null;
                _player.Weapons = new Player.WeaponPlayer[1];

                //_player.WeaponSprite.sprite = null;

                if(ZombieKnifeParametrs != null)
                    SetWeapon(_player, 0, ZombieKnifeParametrs.GetComponent<WeaponSystem>());

                break;
            }
            case Player.Team.Human:
            {
                if(PrimaryWeaponParametrs != null)
                    SetWeapon(_player, 0, PrimaryWeaponParametrs[_player.RandomWeaponPrimary].GetComponent<WeaponSystem>());

                if(SecondWeaponParametrs != null)
                    SetWeapon(_player, 1, SecondWeaponParametrs[_player.RandomWeaponSecond].GetComponent<WeaponSystem>());

                //SetWeapon(_player, 2, GrenadeWeapon[_player.RandomWeaponGrenade].GetComponent<WeaponSystem>());

                break;
            }
        }
    }

    public void UpdateWeapon()
    {
        switch(Biohazard.Mode)
        {
            case Biohazard.BiohazardMode.Classic:
            {
                PrimaryWeaponParametrs = ClassicPrimaryWeapon;
                SecondWeaponParametrs = ClassicSecondWeapon;
                GrenadeWeaponParametrs = ClassicGrenadeWeapon;
                KnifeWeaponParametrs = ClassicKnifeWeapon;

                ZombieKnifeParametrs = ClassicZombieKnife;

                break;
            }
            case Biohazard.BiohazardMode.New:
            {
                PrimaryWeaponParametrs = NewPrimaryWeapon;
                SecondWeaponParametrs = NewSecondWeapon;
                GrenadeWeaponParametrs = NewGrenadeWeapon;
                KnifeWeaponParametrs = NewKnifeWeapon;

                ZombieKnifeParametrs = NewZombieKnife;

                break;
            }
        }
    }

    public void ConnectedRespawn(GameObject PlayerObject)
    {
        if (isServer)
        {
            NetworkIdentity opponentIdentity = PlayerObject.GetComponent<NetworkIdentity>();

            if (opponentIdentity.connectionToClient != null)
                TargetConnectedRespawn(opponentIdentity.connectionToClient, PlayerObject, IsRespawnInGame);
        }

        Player _player = PlayerObject.GetComponent<Player>();

        if(isServer)
        {
            _player.RandomSpawn = UnityEngine.Random.Range(0, SpawnsPoint.Length);

            _player.RandomWeaponPrimary = UnityEngine.Random.Range(0, PrimaryWeaponParametrs.Length);
            _player.RandomWeaponSecond = UnityEngine.Random.Range(0, SecondWeaponParametrs.Length);
            _player.RandomWeaponGrenade = UnityEngine.Random.Range(0, GrenadeWeaponParametrs.Length);
        }

        if (_player.IsAlive)
            return;

        if (IsGame && Biohazard.IsModBiohazardStart)
        {
            if (IsRespawnInGame)
                StartCoroutine(RespawnPlayer(PlayerObject, 3f, Player.Team.Zombie));
        }
        else
            StartCoroutine(RespawnPlayer(PlayerObject, 3f, Player.Team.Human));
    }

    [ClientRpc]
    public void RpcNewRound()
    {
        if (isServer)
            return;

        NewRound();
    }

    [ClientRpc]
    void RpcCreateBot(GameObject Bot, int SpawnIndex)
    {
        if (isServer)
            return;

        Bot.transform.parent = BotParent.transform;
        Bot.transform.position = SpawnsPoint[SpawnIndex];

        Bot.GetComponent<NavMeshAgent>().enabled = false;

        Bots.Add(Bot);
    }

    [ClientRpc]
    void RpcDestroyBot(GameObject Bot)
    {
        Bots.Remove(Bot);
    }

    [ClientRpc]
    public void RpcGameStart()
    {
        if (isServer)
            return;

        GameStart();
    }

    [ClientRpc]
    public void RpcGameEnd()
    {
        if (isServer)
            return;

        GameEnd();
    }

    [TargetRpc]
    void TargetConnectedRespawn(NetworkConnection target, GameObject Player, bool IsRespawn)
    {
        if (isServer)
            return;

        IsRespawnInGame = IsRespawn;
        ConnectedRespawn(Player);
    }

    [TargetRpc]
    void TargetRespawn(NetworkConnection target, GameObject Player)
    {
        if (isServer)
            return;

        StartCoroutine(RespawnPlayer(Player, 5f, global::Player.Team.Zombie));
    }
}

//───────────────────────────────────────────────────────────────────────██────────────────────────────────────────────────────────────────────────────────────────────
//███─█───█──██─████─██────────────██─██─████─███─█──█─████─████──█───█─█──█────████─████─████─███─████─████─█───█─█───█─█──█─████─███──██─████────█──█─███────█────██─
//─█──█───█─██──█──█──██────────────███──█──█─█───█──█─█──█─█──██─█───█─█──█────█──█─█──█─█──█─█───█──█─█──█─██─██─██─██─█──█─█──█──█──██──█──█────█──█───█────█──█──██
//─█──███─█─█───████───█────███──────█───████─███─████─█──█─████──███─█─█─██────█──█─████─█──█─█───████─████─█─█─█─█─█─█─█─██─█─────█──█─────██────█─██─███────████───█
//─█──█─█─█─██───█─█──██────────────███──█────█───█──█─█──█─█──██─█─█─█─██─█────█──█─█────█──█─█───█────█──█─█───█─█───█─██─█─█──█──█──██──██──────██─█───█───────█──██
//─█──███─█──██──█─█─██────────────██─██─█────███─█──█─████─████──███─█─█──█────█──█─█────████─█───█────█──█─█───█─█───█─█──█─████──█───██─████────█──█─███───────█─██─

﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockBack : NetworkBehaviour
{
    // Start is called before the first frame update

    public enum KnockBackType
    {
        Force,
        Velocity,
        None
    };

    public KnockBackType knockBack;

    private Coroutine CorKnockBack;

    public void TakeKnockBack(float Duration, float Power, GameObject Victim, GameObject Attacker)
    {
        if (!Biohazard.IsModBiohazardStart)
            return;

        if (isServer)
        {
            NetworkIdentity opponentIdentity = Victim.GetComponent<NetworkIdentity>();

            if(opponentIdentity.connectionToClient != null)
                TargetTakeKnockBack(opponentIdentity.connectionToClient, Duration, Power, Victim, Attacker);
        }

        switch (knockBack)
        {
            case KnockBackType.Force:
            {
                CorKnockBack = StartCoroutine(KnockBackForce(Duration, Power, Victim, Attacker));
                break;
            }
            case KnockBackType.Velocity:
            {
                CorKnockBack = StartCoroutine(KnockBackVelocity(Duration, Power, Victim, Attacker));
                break;
            }
        }
    }
    public IEnumerator KnockBackForce(float Duration, float Power, GameObject Victim, GameObject Attacker)
    {
        float timer = 0;

        if (!Victim.gameObject.GetComponent<Player>().Bot)
            Duration *= 1.5f;

        while (timer < Duration && Victim.gameObject.GetComponent<Player>().IsAlive)
        {
            timer += Time.deltaTime;

            Vector2 difference = (Attacker.transform.position - Victim.transform.position).normalized * Power;
            Victim.gameObject.GetComponent<Rigidbody2D>().AddForce(-difference, ForceMode2D.Impulse);
            yield return null;
        }

        yield break;
    }

    public IEnumerator KnockBackVelocity(float Duration, float Power, GameObject Victim, GameObject Attacker)
    {
        float timer = 0;

        if (!Victim.gameObject.GetComponent<Player>().Bot)
            Duration *= 1.5f;

        while (timer < Duration && Victim.gameObject.GetComponent<Player>().IsAlive)
        {
            timer += Time.deltaTime;

            if (isServer && Victim.gameObject.GetComponent<BotControl>())
                Victim.gameObject.GetComponent<BotControl>().navMeshAgent.isStopped = true;

            Vector2 difference = (Attacker.transform.position - Victim.transform.position).normalized * Power;
            Victim.gameObject.GetComponent<Rigidbody2D>().velocity = -difference;
            yield return null;
        }

        if (isServer && Victim.gameObject.GetComponent<BotControl>() && Victim.gameObject.GetComponent<Player>().IsAlive)
            Victim.gameObject.GetComponent<BotControl>().navMeshAgent.isStopped = false;

        Victim.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        yield break;
    }

    [TargetRpc]
    void TargetTakeKnockBack(NetworkConnection target, float Duration, float Power, GameObject Victim, GameObject Attacker)
    {
        if (isServer)
            return;

        TakeKnockBack(Duration, Power, Victim, Attacker);
    }
}

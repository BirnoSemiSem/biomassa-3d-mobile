﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InfoPlayer : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI MoneyText;
    [SerializeField] private TextMeshProUGUI GoldMoney;

    string[] Ranks =
    {
        "Призывник",
        "Новичок",
        "Рекрут",
        "Рядовой",
        "Опытный Сержант",
        "Ефрейтор",
        "Младший Сержант",
        "Сержант",
        "Мастер Сержант",
        "Старшина",
        "Главный Старшина",
        "Капитан",
        "Джаггернаут",
        "Наемник",
        "Охотник",
        "Берсерк",
        "Руководитель отряда",
        "Кандидат BioMassa",
        "BioHumanGenetics",
        "Ч.З.С." // член закрытого сообщества
    };

    //Прохождение обучения - 1000
    //Убийство - 50
    //Заражение - 10

    int[] Exp =
    {
        1000,       //Призывник -> Новичок
        1500,       //Новичок -> Рекрут
        2250,       //Рекрут -> Рядовой
        3500,       // Рядовой -> Опытный Сержант
        4000,       // Опытный Сержант -> Ефрейтор
        5000,      // Ефрейтор -> Младший Сержант

        7500,      // Младший Сержант -> Сержант
        9500,      // Сержант -> Мастер Сержант
        12000,      // Мастер Сержант -> Старшина
        15000,      // Старшина -> Главный Старшина
        20000,      // Главный Старшина -> Капитан

        30000,      // Капитан -> Джаггернаут
        37000,      // Джаггернаут -> Наемник
        52000,      // Наемник -> Охотник
        69000,      // Охотник -> Берсерк
        90000,      // Берсерк -> Руководитель отряда

        120000,     // Руководитель отряда -> Кандидат BioMassa
        150000,     // Кандидат BioMassa -> BioHumanGenetics
        200000,     // BioHumanGenetics -> Ч.З.С.
    };

    static public int TempMoney;
    static public int TempGoldMoney;

    static public string RankName;
    static public int RankIndex;
    static public int RankExp;



}

﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class Player : NetworkBehaviour
{
    public enum Team
    {
        Zombie,
        Human,
        Spectator
    }

    public enum AnimationPlayer
    {
        Idle,
        IdleMachineGun,
        IdlePistolGun,
        IdleGrenade,
        IdleKnife,
        KnifeAttack,
        Death
    }

    public struct WeaponPlayer
    {
        public WeaponSystem.TypeWeapon Weapon;

        public Vector2 PositionSpr;

        public string NameWeapon;

        public int MaxClip;
        public int Clip;
        public int MaxAmmo;
        public int Ammo;

        public float MaxRecoil;

        public float FireSpeed;
        public float SpeedBullet;
        public float ReloadTime;

        public int MinDmg;
        public int MaxDmg;
        public int HSDmg;

        public float Knockback;

        public Sprite WeaponSpr;

        public AudioClip FireClip;
        public AudioClip DrawClip;
        public AudioClip[] ReloadClips;

        public AudioClip[] KnifeMissClips;
        public AudioClip[] KnifeStrikeClips;
    }

    public struct ZombieClassPlayer
    {
        public string Name;
        public int Health;
        public float Speed;
        public float KnockBack;
        public int RegerationHP;
        public float RegerationSpeed;
        public float PainShock;
        public float PainShockSpeed;
    }

    public struct HumanClassPlayer
    {
        public Mesh Model;

        public Material[] ModelMaterial;

        public RuntimeAnimatorController AnimatorController;
        public Avatar AnimatorAvatar;

        public string Name;
        public int Health;
        public int Armor;
        public float Speed;
        public float PainShock;
        public float PainShockSpeed;
    }

    //Unique id
    private int PlayerID;
    public int GetPlayerID => PlayerID;

    //NickName Client
    public string Nick;

    //Random specificity
    [SyncVar] public int RandomSpawn;

    [SyncVar] public int RandomWeaponPrimary;
    [SyncVar] public int RandomWeaponSecond;
    [SyncVar] public int RandomWeaponGrenade;

    //Player properties
    [SyncVar] public Team PlayerTeam;

    [SyncVar] public int Health;
    [SyncVar] public int Armor;

    [SyncVar] public bool IsAlive = false;
    public bool IsFirstZombie = false;

    public Coroutine Pain;
    public Coroutine Regeneration;

    public bool IsPainShock = false;
    public bool IsRegeneration = false;

    //Player Animator
    public Animator Animator;

    public Collider Collider;

    //Class Select
    [SyncVar] public int ZombieSelect;
    [SyncVar] public int HumanSelect;

    ////Body Player Sprite
    //public SpriteRenderer PlayerSprite;

    ////Weapon Visual
    //public Transform WeaponTransform;
    //public SpriteRenderer WeaponSprite;

    public SkinnedMeshRenderer Model;

    //Audio Player
    public AudioClip[] FeetClips;
    public AudioClip[] DeathClips;
    public AudioClip[] PainClips;
    public AudioClip[] InfectionClips;

    public AudioClip FireClip;
    public AudioClip DrawClip;
    public AudioClip[] ReloadClips;

    public AudioClip[] KnifeMissClips;
    public AudioClip[] KnifeStrikeClips;

    //Weapon
    public WeaponControl weaponControl;
    [SyncVar] public int ActiveWeapon = 0;
    public WeaponPlayer[] Weapons = new WeaponPlayer[3];

    //Class Parametrs
    public ZombieClassPlayer ZombieClass;
    public HumanClassPlayer HumanClass;

    //Anti Bug 
    private bool FeetObject = false;
    private bool WeaponObject = false;
    private bool WeaponSpriteObject = false;
    private bool ColliderObject = false;
    private bool NavmeshObject = false;

    //Parent Object
    [SerializeField] private GameObject PlayerGroup;
    [SerializeField] private GameObject BotGroup;

    //Bot
    public BotControl Bot;

    //Delegate
    static public UnityAction<GameObject> EventsPlayerStart;

    void Start()
    {
        if (isServer)
        {
            PlayerTeam = Team.Human;
            ZombieSelect = 0;
            HumanSelect = 0;
        }

        PlayerID = gameObject.GetInstanceID();

        //if (!isServer && isLocalPlayer)
        //{
        //    Nick = System.Text.Encoding.UTF8.GetString(Net.PlayerInfo.NickName);
        //    CmdChangeNickName(Nick);
        //}

        BotGroup = GameObject.FindGameObjectWithTag("BotGroup");
        PlayerGroup = GameObject.FindGameObjectWithTag("PlayerGroup");

        if (GetComponent<BotControl>())
        {
            Bot = GetComponent<BotControl>();

            if (!isServer)
                Bot.navMeshAgent.enabled = false;

            gameObject.transform.SetParent(BotGroup.transform);
        }
        else
        {
            Bot = null;
            gameObject.transform.SetParent(PlayerGroup.transform);
        }

        FeetObject = false;
        WeaponObject = false;
        WeaponSpriteObject = false;
        ColliderObject = false;
        NavmeshObject = false;

        EventsPlayerStart?.Invoke(gameObject);
    }

    void OnEnable()
    {
        GameControl.EventPreStartRound += NewRound;
        GameControl.EventSpawnPlayer += Spawn;
        Damage.EventTakeDamage += TakeDamege;
        Damage.EventDeathPlayer += Death;
    }

    void OnDisable()
    {
        GameControl.EventPreStartRound -= NewRound;
        GameControl.EventSpawnPlayer -= Spawn;
        Damage.EventTakeDamage -= TakeDamege;
        Damage.EventDeathPlayer -= Death;
    }

    void Update()
    {
        if (IsAlive)
        {
            if(isServer)
            {
                if (!NavmeshObject && Bot)
                {
                    Bot.navMeshAgent.enabled = true;
                    NavmeshObject = true;
                }
            }

            //if (!FeetObject)
            //{
            //    gameObject.transform.Find("Feet").gameObject.SetActive(true);
            //    FeetObject = true;
            //}

            //if (!WeaponObject)
            //{
            //    gameObject.transform.Find("Weapon").gameObject.SetActive(true);
            //    WeaponObject = true;
            //}

            //if (!WeaponSpriteObject && PlayerTeam == Team.Human)
            //{
            //    WeaponSprite.sprite = Weapons[ActiveWeapon].WeaponSpr;
            //    WeaponSpriteObject = true;
            //}

            //if (!ColliderObject)
            //{
            //    gameObject.GetComponent<Collider>().enabled = true;
            //    ColliderObject = true;
            //}

            //if (PlayerTeam == Team.Zombie)
            //{
            //    if (!System.String.IsNullOrEmpty(ZombieClass.Name) && (Animator.GetCurrentAnimatorClipInfo(0).Length <= 0 || !Animator.GetCurrentAnimatorClipInfo(0)[0].clip.name.Contains(ZombieClass.Name)))
            //        Animator.Play(ZombieClass.Name + "-" + AnimationPlayer.Idle.ToString());
            //}
            //else if (PlayerTeam == Team.Human)
            //{
            //    AnimationPlayer animationPlayer = (AnimationPlayer)ActiveWeapon + 1;
            //    if (!System.String.IsNullOrEmpty(HumanClass.Name) && !Animator.GetCurrentAnimatorStateInfo(0).IsName(HumanClass.Name + "-" + animationPlayer.ToString()))
            //        Animator.Play(HumanClass.Name + "-" + animationPlayer.ToString());
            //}
        }
    }

    public void NewRound()
    {

    }

    public void Spawn(GameObject Player, int id)
    {

    }

    public void TakeDamege(GameObject victim, GameObject attacker, int damage)
    {
        if (!victim.GetComponent<Player>()) // Проверка что именно ДАМАГ получил ИГРОК
            return;

        if (isServer)
        {
            //RpcTakeDamage(victim, attacker, damage);

            if (victim.GetInstanceID() == PlayerID)
            {
                if (Pain != null)
                    StopCoroutine(Pain);
                Pain = StartCoroutine(PainShockEnd());

                if (PlayerTeam == Team.Zombie)
                {
                    if (Regeneration != null)
                        StopCoroutine(Regeneration);
                    Regeneration = StartCoroutine(RegenerationHP());
                }
            }
        }
    }

    public void Death(GameObject Player, int id)
    {
        if (!Player.GetComponent<Player>()) // Проверка что именно УМЕР ИГРОК
            return;

        if (id == PlayerID)
        {
            StopAllCoroutines();
            Pain = null;
            Regeneration = null;

            FeetObject = false;
            WeaponObject = false;
            WeaponSpriteObject = false;
            ColliderObject = false;
            NavmeshObject = false;
        }
    }

    public IEnumerator PainShockEnd()
    {
        IsPainShock = true;
        switch (PlayerTeam)
        {
            case Team.Zombie: { yield return new WaitForSeconds(ZombieClass.PainShockSpeed); break; }
            case Team.Human: { yield return new WaitForSeconds(HumanClass.PainShockSpeed); break; }
        }

        if (IsAlive)
        {
            IsPainShock = false;
        }

        Pain = null;
    }

    public IEnumerator RegenerationHP()
    {
        yield return new WaitForSeconds(2f);

        IsRegeneration = true;

        while (IsAlive && PlayerTeam == Team.Zombie && IsRegeneration && Health < ZombieClass.Health)
        {
            if (Health + ZombieClass.RegerationHP < ZombieClass.Health)
                Health += ZombieClass.RegerationHP;
            else
                Health = ZombieClass.Health;
            yield return new WaitForSeconds(ZombieClass.RegerationSpeed);
        }

        IsRegeneration = false;

        Regeneration = null;
    }

    //[ClientRpc]
    //void RpcTakeDamage(GameObject victim, GameObject attacker, int damage)
    //{
    //    if (isServer)
    //        return;

    //    if (victim.GetInstanceID() == PlayerID)
    //    {
    //        if (Pain != null)
    //            StopCoroutine(Pain);
    //        Pain = StartCoroutine(PainShockEnd());

    //        if (PlayerTeam == Team.Zombie)
    //        {
    //            if (Regeneration != null)
    //                StopCoroutine(Regeneration);
    //            Regeneration = StartCoroutine(RegenerationHP());
    //        }
    //    }
    //}

    //[ClientRpc]
    //void RpcDeath(GameObject Player, int id)
    //{
    //    if (isServer)
    //        return;

    //    StopAllCoroutines();
    //    Pain = null;
    //    Regeneration = null;

    //    FeetObject = false;
    //    WeaponObject = false;
    //    WeaponSpriteObject = false;
    //    ColliderObject = false;
    //    NavmeshObject = false;
    //}

    //[Command]
    //void CmdTakeDamage(GameObject victim, GameObject attacker, int damage)
    //{
    //    if (!isServer)
    //        return;

    //    if (victim.GetInstanceID() == PlayerID)
    //    {
    //        if (Pain != null)
    //            StopCoroutine(Pain);
    //        Pain = StartCoroutine(PainShockEnd());

    //        if (PlayerTeam == Team.Zombie)
    //        {
    //            if (Regeneration != null)
    //                StopCoroutine(Regeneration);
    //            Regeneration = StartCoroutine(RegenerationHP());
    //        }
    //    }
    //}

    //[Command]
    //void CmdDeath(GameObject Player, int id)
    //{
    //    if (!isServer)
    //        return;

    //    if (id == PlayerID)
    //    {
    //        StopAllCoroutines();
    //        Pain = null;
    //        Regeneration = null;

    //        FeetObject = false;
    //        WeaponObject = false;
    //        WeaponSpriteObject = false;
    //        ColliderObject = false;
    //        NavmeshObject = false;
    //    }
    //}

    [Command]
    public void CmdChangeNickName(string NewNickName)
    {
        if (!isServer)
            return;

        Nick = NewNickName;
    }
}

//█──█────████─███─████────███─█───█────█──█─████─███─█───█─████──████─███─█───█─█───────█──█─████──███──████─█───█─███─────██──█────█──█─███────█────██─
//█──█────█──█──█──█──█─────█──█───█────█──█─█──█───█─█───█─█──██─█──█─█───█───█─█───────█─█──█──█──█─█──█──█─██─██───█────██──██────█──█───█────█──█──██
//█─██──────██──█──█──█─────█──███─█────████─████─███─███─█─████──████─███─█─█─█─████────██───█──█──█─█──█──█─█─█─█─███────█────█────█─██─███────████───█
//██─█────█──█──█──█──█─────█──█─█─█────█──█─█──█───█─█─█─█─█──██─█──█─█───█─█─█─█──█────█─█──█──█─█████─█──█─█───█────────██───█────██─█───█───────█──██
//█──█────████──█──████─────█──███─█────█──█─█──█─███─███─█─████──█──█─███─█████─████────█──█─████─█───█─████─█───█─█───────██──█────█──█─███───────█─██─

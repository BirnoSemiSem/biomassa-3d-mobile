﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayer : MonoBehaviour
{
    public GameObject Target; // положение персонажа
    public IndicatorPlayer TargetIndicator; // Индикатор

    public float dumping = 2f; // сглаживание камеры. скольжение
    public float distance = 2f;

    [Space]

    [SerializeField]
    float limitleft;
    [SerializeField]
    float limitright;
    [SerializeField]
    float limitbottom;
    [SerializeField]
    float limitupper;

    public bool IsLimit = false;
    public bool DebugCamera = false;

    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!Target)
        {
            if (GameControl.Players != null && GameControl.Players.Length > 0)
                for (int i = 0; i < GameControl.Players.Length; i++)
                    if (GameControl.Players[i] != null && GameControl.Players[i].GetComponent<Player>().isLocalPlayer)
                        Target = GameControl.Players[i];
        }

        if (Target)
        {
            //if (!TargetIndicator)
            //    TargetIndicator = Target.transform.Find("PointTarget").GetComponent<IndicatorPlayer>();

            Vector3 target = Target.transform.position + (Target.transform.forward * distance);

            Vector3 currentPosition = Vector3.Lerp(transform.position, new Vector3(target.x, transform.position.y, target.z), dumping * Time.deltaTime);
            transform.position = currentPosition;
        }

        if (IsLimit)
        {
            transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, limitleft, limitright),
            Mathf.Clamp(transform.position.y, limitbottom, limitupper),
            transform.position.z);
        }
    }

    public void ChangePlayerTarget(GameObject CurrentTarget)
    {
        if (Target == CurrentTarget)
            return;

        TargetIndicator.ChangeIndicator(IndicatorPlayer.Indicator.none);

        Target = CurrentTarget;
        TargetIndicator = Target.transform.Find("PointTarget").GetComponent<IndicatorPlayer>();

        TargetIndicator.ChangeIndicator(IndicatorPlayer.Indicator.LocalPlayer);
    }

    private void OnDrawGizmos()
    {
        if (!DebugCamera || !IsLimit)
            return;

        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector2(limitleft, limitupper), new Vector2(limitright, limitupper));
        Gizmos.DrawLine(new Vector2(limitleft, limitupper), new Vector2(limitleft, limitbottom));
        Gizmos.DrawLine(new Vector2(limitleft, limitbottom), new Vector2(limitright, limitbottom));
        Gizmos.DrawLine(new Vector2(limitright, limitupper), new Vector2(limitright, limitbottom));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorPlayer : MonoBehaviour
{
    public enum Indicator
    {
        LocalPlayer,
        Player,
        Bot,
        none
    }

    public SpriteRenderer PointTarget; // положение персонажа
    public Player PlayerProperties; // положение персонажа

    [SerializeField] private Sprite RedIndicator; // Индиактор локального игрока
    [SerializeField] private Sprite GreenIndicator; // Индиактор игрока
    [SerializeField] private Sprite BotIndicator; // Индиактор Бота

    private bool IsVisible;
    [SerializeField] private bool IsOptimization;

    // Update is called once per frame
    void Update()
    {
        if (IsVisible)
        {
            if (PlayerProperties.IsAlive && PointTarget.sprite == null)
            {
                if (PlayerProperties.isLocalPlayer)
                    PointTarget.sprite = RedIndicator;
                else if (!PlayerProperties.Bot)
                    PointTarget.sprite = GreenIndicator;
                else
                    PointTarget.sprite = BotIndicator;

            }
            else if (!PlayerProperties.IsAlive && PointTarget.sprite != null)
                PointTarget.sprite = null;
        }
    }

    public void ChangeIndicator(Indicator Target)
    {
        switch(Target)
        {
            case Indicator.LocalPlayer:
            {
                PointTarget.sprite = RedIndicator;
                break;
            }
            case Indicator.Player:
            {
                PointTarget.sprite = GreenIndicator;
                break;
            }
            case Indicator.Bot:
            {
                PointTarget.sprite = BotIndicator;
                break;
            }
            case Indicator.none:
            {
                PointTarget.sprite = null;
                break;
            }
        }
    }

    void OnBecameVisible()
    {
        if (gameObject && IsOptimization)
        {
            PointTarget.color = new Color(255f, 255f, 255f, 255f);
            IsVisible = true;
        }
    }

    void OnBecameInvisible()
    {
        if (gameObject && IsOptimization)
        {
            PointTarget.color = new Color(255f, 255f, 255f, 0f);
            IsVisible = false;
        }
    }
}

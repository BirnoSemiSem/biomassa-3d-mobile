﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class UI : NetworkBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Biohazard Biohazard;

    [SerializeField] private TextMeshProUGUI TextCountZombie;
    [SerializeField] private TextMeshProUGUI TextCountHuman;

    // Player
    [SerializeField] private GameObject Player;
    [SerializeField] private ControlController Control;


    //Human
    [SerializeField] private Image HumanHealthBar;
    [SerializeField] private Image HumanArmorBar;
    [SerializeField] private TextMeshProUGUI HumanTextHealth;
    [SerializeField] private TextMeshProUGUI HumanTextArmor;

    //Zombie
    [SerializeField] private Image ZombieHealthBar;
    //[SerializeField] private Image ZombieArmorBar;
    [SerializeField] private TextMeshProUGUI ZombieTextHealth;
    //[SerializeField] private TextMeshProUGUI ZombieTextArmor;

    [SerializeField] private GameObject HumanInfoStats;
    [SerializeField] private GameObject ZombieInfoStats;

    [SerializeField] private TextMeshProUGUI TextNameWeapon;
    [SerializeField] private TextMeshProUGUI TextClipWeapon;
    [SerializeField] private TextMeshProUGUI TextAmmoWeapon;

    [SerializeField] private Text TextMoney;

    [SerializeField] private ButtonUI[] buttonUIs;
    [SerializeField] private ProgressBarPlayer PrograssBarPlayer;

    [SerializeField] private GameObject PlayerStats;

    [SerializeField] private Joystic JoyStick;

    [SerializeField] private GameObject DeadScreen;
    [SerializeField] private GameObject ButtonsAlive;
    [SerializeField] private GameObject ButtonsDied;
    [SerializeField] private GameObject ButtonsChat;
    [SerializeField] private GameObject ButtonsMenu;

    void Start()
    {

    }

    void OnEnable()
    {
        GameControl.EventPreStartRound += NewRound;

        GameControl.EventSpawnPlayer += PlayerSpawn;
        Biohazard.EventInfectionPlayer += PlayerInfection;
    }

    void OnDisable()
    {
        GameControl.EventPreStartRound -= NewRound;

        GameControl.EventSpawnPlayer -= PlayerSpawn;
        Biohazard.EventInfectionPlayer -= PlayerInfection;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Player)
        {
            if (GameControl.Players != null)
            {
                for (int i = 0; i < GameControl.Players.Length; i++)
                {
                    if (GameControl.Players[i] && GameControl.Players[i].GetComponent<Player>().isLocalPlayer)
                    {
                        Player = GameControl.Players[i];
                        Control.PlayerControlLocal = Player.GetComponent<Player>();
                        Control.controllerPlayer = Player.GetComponent<ControllerPlayer>();

                        if (!Player.GetComponent<WeaponControl>().ProgressBar)
                        {
                            Player.GetComponent<WeaponControl>().ProgressBar = PrograssBarPlayer;
                        }

                        if (Control.Control == ControlController.Controller.Android)
                        {
                            for (int id = 0; id < buttonUIs.Length; id++)
                            {
                                buttonUIs[id].Player = Player;
                                buttonUIs[id].WeaponControl = Player.GetComponent<WeaponControl>();
                            }
                        }
                    }
                }
            }
        }

        //Boti.Clear();
        //GameObject[] Bots = GameObject.FindGameObjectsWithTag("Player");

        //for (int i = 0; i < Bots.Length; i++)
        //{
        //    if (Bots[i].name.Contains("BOT"))
        //        Boti.Add(Bots[i]);
        //}

        //if (BotQuota < Boti.Count)
        //    BotQuota = Boti.Count;

        //TextBotCount.text = $"BotQuota: {BotQuota} | Bot Count: {Boti.Count}";

        TextCountZombie.text = Biohazard.ZombieCount.ToString();
        TextCountHuman.text = Biohazard.HumanCount.ToString();

        if (Player)
        {
            if((!ButtonsChat.activeInHierarchy || !ButtonsMenu.activeInHierarchy) && Control.Control == ControlController.Controller.Android)
            {
                ButtonsChat.SetActive(true);
                ButtonsMenu.SetActive(true);
            }
            else if (Control.PlayerControlLocal && Control.PlayerControlLocal.IsAlive)
            {
                if (!ButtonsAlive.activeInHierarchy && Control.Control == ControlController.Controller.Android)
                {
                    JoyStick.Reset();
                    ButtonsAlive.SetActive(true);

                    ButtonsDied.SetActive(false);
                }

                if (DeadScreen.activeInHierarchy)
                    DeadScreen.SetActive(false);

                if (!PlayerStats.activeInHierarchy)
                    PlayerStats.SetActive(true);

                Hud(Control.PlayerControlLocal);
            }
            else if(Control.PlayerControlLocal && !Control.PlayerControlLocal.IsAlive && Control.PlayerControlSpec == null)
            {
                Hud(Control.PlayerControlLocal);

                if (Control.PlayerControlLocal.PlayerTeam != global::Player.Team.Spectator)
                    Invoke(nameof(SpectrMode), 3f);
                else
                    SpectrMode();

                if(!DeadScreen.activeInHierarchy)
                    DeadScreen.SetActive(true);

                if (!ButtonsDied.activeInHierarchy && Control.Control == ControlController.Controller.Android)
                {
                    ButtonsDied.SetActive(true);

                    JoyStick.Reset();
                    ButtonsAlive.SetActive(false);
                }
            }
            else if (Control.PlayerControlSpec && Control.PlayerControlSpec.IsAlive)
            {
                if (!ButtonsDied.activeInHierarchy && Control.Control == ControlController.Controller.Android)
                {
                    ButtonsDied.SetActive(true);

                    JoyStick.Reset();
                    ButtonsAlive.SetActive(false);
                }

                if(Control.PlayerControlSpec.PlayerTeam == global::Player.Team.Human && !HumanInfoStats.activeInHierarchy)
                {
                    HumanInfoStats.SetActive(true);
                    ZombieInfoStats.SetActive(false);
                }
                else if(Control.PlayerControlSpec.PlayerTeam == global::Player.Team.Zombie && !ZombieInfoStats.activeInHierarchy)
                {
                    HumanInfoStats.SetActive(false);
                    ZombieInfoStats.SetActive(true);
                }

                if (!PlayerStats.activeInHierarchy)
                    PlayerStats.SetActive(true);

                Hud(Control.PlayerControlSpec);
            }
            else if(Control.PlayerControlSpec && !Control.PlayerControlSpec.IsAlive)
            {
                Hud(Control.PlayerControlSpec);

                Invoke(nameof(SpectrMode), 3f);

                if (!DeadScreen.activeInHierarchy)
                    DeadScreen.SetActive(true);

                if (!ButtonsDied.activeInHierarchy && Control.Control == ControlController.Controller.Android)
                {
                    ButtonsDied.SetActive(true);

                    JoyStick.Reset();
                    ButtonsAlive.SetActive(false);
                }
            }
            else
            {
                PlayerStats.SetActive(false);

                if (Control.Control == ControlController.Controller.Android)
                {
                    ButtonsDied.SetActive(false);

                    JoyStick.Reset();
                    ButtonsAlive.SetActive(false);
                }
            }
        }

        if(Control.Control != ControlController.Controller.Android)
        {
            if(ButtonsDied.activeInHierarchy)
                ButtonsDied.SetActive(false);

            if (ButtonsAlive.activeInHierarchy)
            {
                JoyStick.Reset();
                ButtonsAlive.SetActive(false);
            }
        }
    }

    public void NewRound()
    {

    }

    void Hud(Player PlayerControl)
    {
        switch (PlayerControl.PlayerTeam)
        {
            case global::Player.Team.Zombie:
            {
                    ZombieTextHealth.text = $"{(PlayerControl.Health > 0 ? PlayerControl.Health : 0)}";

                if (PlayerControl.ZombieClass.Health > 0f)
                        ZombieHealthBar.fillAmount = PlayerControl.Health > 0f ? (float)PlayerControl.Health / (float)PlayerControl.ZombieClass.Health : 0f;
                else
                        ZombieHealthBar.fillAmount = (float)PlayerControl.Health / 100f;
                //ArmorBar.fillAmount = 0f;
                break;
            }
            case global::Player.Team.Human:
            {
                HumanTextHealth.text = $"{(PlayerControl.Health > 0 ? PlayerControl.Health : 0)}";
                HumanTextArmor.text = $"{(PlayerControl.Armor > 0 ? PlayerControl.Armor : 0)}";

                if (PlayerControl.HumanClass.Health > 0f)
                    HumanHealthBar.fillAmount = PlayerControl.Health > 0f ? (float)(PlayerControl.Health / (float)PlayerControl.HumanClass.Health) : 0f;
                else
                    HumanHealthBar.fillAmount = (float)PlayerControl.Health / 100f;
                if (PlayerControl.HumanClass.Armor > 0f)
                    HumanArmorBar.fillAmount = PlayerControl.Armor > 0f ? (float)(PlayerControl.Armor / (float)PlayerControl.HumanClass.Armor) : 0f;
                else
                    HumanArmorBar.fillAmount = (float)PlayerControl.Armor / 100f;
                break;
            }
        }

        if (PlayerControl.IsAlive && PlayerControl.PlayerTeam == global::Player.Team.Human)
        {
            TextNameWeapon.text = PlayerControl.Weapons[PlayerControl.ActiveWeapon].NameWeapon;
            TextClipWeapon.text = PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip.ToString();
            TextAmmoWeapon.text = PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo.ToString();
        }
    }

    void SpectrMode()
    {
        CancelInvoke(nameof(SpectrMode));
        Control.SpectrMode();
    }

    public void PlayerSpawn(GameObject Player, int id)
    {
        if (!Control.PlayerControlLocal)
            return;

        if (id == Control.PlayerControlLocal.GetPlayerID)
        {
            if (Control.PlayerControlLocal.IsAlive)
            {
                if (Control.PlayerControlLocal.PlayerTeam == global::Player.Team.Human)
                {
                    HumanInfoStats.SetActive(true);
                    ZombieInfoStats.SetActive(false);
                }
                else if(Control.PlayerControlLocal.PlayerTeam == global::Player.Team.Zombie)
                {
                    HumanInfoStats.SetActive(false);
                    ZombieInfoStats.SetActive(true);
                }
            }

            DeadScreen.SetActive(false);

            Control.CameraLocalPlayer.ChangePlayerTarget(Player);

            CancelInvoke(nameof(SpectrMode));
            Control.IndexSpec = -1;
            Control.PlayerControlSpec = null;
        }
    }

    void PlayerInfection(GameObject Player, int id)
    {
        if (!Control.PlayerControlLocal)
            return;

        if (id == Control.PlayerControlLocal.GetPlayerID)
        {
            if (Control.PlayerControlLocal.IsAlive)
            {
                HumanInfoStats.SetActive(false);
                ZombieInfoStats.SetActive(true);
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarPlayer : MonoBehaviour
{
    public Slider LeftBar;
    public Slider RightBar;

    public TMP_Text TextProgressBar;
}

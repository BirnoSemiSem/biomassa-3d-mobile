﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Joystic : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    // Start is called before the first frame update

    [SerializeField] private GameObject marker;

    [SerializeField] private int Radios;
    public int CoffMaxRadios = 5;

    private Vector2 TargetVector;
    private Vector2 PoinerPosition;

    public ControllerPlayer controllerPlayer;

    private bool IsMoveZone = false;

    private bool IsPointerDown = false;

    void Start()
    {
        marker.transform.position = transform.position;
        Radios = (int)gameObject.GetComponent<RectTransform>().rect.width / 2;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        IsPointerDown = true;
        PoinerPosition = eventData.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        IsPointerDown = true;
        PoinerPosition = eventData.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        IsPointerDown = false;
        PoinerPosition = Vector2.zero;

        marker.transform.position = transform.position;

        if(controllerPlayer)
            controllerPlayer.ChangeTargetMove(new Vector2(0, 0));

        IsMoveZone = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!controllerPlayer)
        {
            if (GameControl.Players != null)
            {
                for (int i = 0; i < GameControl.Players.Length; i++)
                    if (GameControl.Players[i].GetComponent<Player>().isLocalPlayer)
                        controllerPlayer = GameControl.Players[i].GetComponent<ControllerPlayer>();
            }
        }

        if (IsPointerDown && controllerPlayer)
        {
            TargetVector = PoinerPosition - (Vector2)transform.position;

            if (TargetVector.magnitude < Radios * CoffMaxRadios || IsMoveZone)
            {
                if (TargetVector.magnitude < Radios * CoffMaxRadios)
                {
                    if (TargetVector.magnitude < Radios)
                        marker.transform.position = PoinerPosition;
                    else
                        marker.transform.position = transform.position + ((Vector3)PoinerPosition - transform.position).normalized * Radios;
                    controllerPlayer.ChangeTargetMove(TargetVector);

                    IsMoveZone = true;
                }
                else
                {
                    marker.transform.position = transform.position;
                    controllerPlayer.ChangeTargetMove(new Vector2(0, 0));
                    IsMoveZone = false;

                    IsPointerDown = false;
                    PoinerPosition = Vector2.zero;
                }
            }
            else
            {
                marker.transform.position = transform.position;
                controllerPlayer.ChangeTargetMove(new Vector2(0, 0));
                IsMoveZone = false;

                IsPointerDown = false;
                PoinerPosition = Vector2.zero;
            }
        }
    }

    public void Reset()
    {
        IsPointerDown = false;
        PoinerPosition = Vector2.zero;

        marker.transform.position = transform.position;

        if (controllerPlayer)
            controllerPlayer.ChangeTargetMove(new Vector2(0, 0));

        IsMoveZone = false;
    }
}

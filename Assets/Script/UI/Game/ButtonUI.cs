﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    enum ButtonFun
    {
        Fire,
        Reload,
        Slot1,
        Slot2,
        Slot3,
        Shop
    }

    [SerializeField] private ButtonFun buttonFun;

    public GameObject Player;
    public WeaponControl WeaponControl;

    public void OnPointerDown(PointerEventData data)
    {
        if (WeaponControl)
        {
            switch (buttonFun)
            {
                case ButtonFun.Fire:
                {
                    WeaponControl.ClickButtonFireEnter();
                    break;
                }
                case ButtonFun.Reload:
                {
                    WeaponControl.ClickButtonReloadEnter();
                    break;
                }
                case ButtonFun.Slot1:
                {
                    WeaponControl.ClickChangeWeapon(0);
                    break;
                }
                case ButtonFun.Slot2:
                {
                    WeaponControl.ClickChangeWeapon(1);
                    break;
                }
                case ButtonFun.Slot3:
                {
                    WeaponControl.ClickChangeWeapon(2);
                    break;
                }
            }
        }
    }

    public void OnPointerUp(PointerEventData data)
    {
        if (WeaponControl)
        {
            switch (buttonFun)
            {
                case ButtonFun.Fire:
                {
                    WeaponControl.ClickButtonFireExit();
                    break;
                }
            }
        }
    }
}

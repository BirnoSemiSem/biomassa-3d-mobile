﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CreateServer : MonoBehaviour
{
    public TMP_InputField IServerName;

    public TMP_InputField ICountBots;
    public TMP_InputField ICountPlayers;

    public Slider SCountBots;
    public Slider SCountPlayers;

    public Scrollbar MapsScrollBar;
    public Transform MapsContent;
    float MapsScrollPos = 0;
    int MapsIndex = 0;

    public Scrollbar ModeScrollBar;
    public Transform ModeContent;
    float ModeScrollPos = 0;
    int ModeIndex = 0;

    float[] PositionAllContent;

    public GameObject PrefabHostGameTextForScroll;

    Regex rgx = new Regex("[^0-9]");
    // Start is called before the first frame update
    void OnEnable()
    {
        IServerName.text = "Zombie Server";
        ICountBots.text = SCountBots.value.ToString();
        ICountPlayers.text = SCountPlayers.value.ToString();

        if (MapsContent.childCount <= 0 && ModeContent.childCount <= 0)
        {
            if (SpecificalOptions.specificalOptions.ServerOptions.MapControllers.Count > 0)
            {
                for (int i = 0; i < SpecificalOptions.specificalOptions.ServerOptions.MapControllers.Count; i++)
                {
                    GameObject TextForScrool = Instantiate(PrefabHostGameTextForScroll) as GameObject;
                    TextForScrool.transform.SetParent(MapsContent);
                    TextForScrool.transform.position = MapsContent.position;

                    TextForScrool.GetComponent<TMP_Text>().text = SpecificalOptions.specificalOptions.ServerOptions.MapControllers[i].MapName;
                }
            }
            else
            {
                GameObject TextForScrool = Instantiate(PrefabHostGameTextForScroll) as GameObject;
                TextForScrool.transform.SetParent(MapsContent);
                TextForScrool.transform.position = MapsContent.position;
            }

            if (SpecificalOptions.specificalOptions.ServerOptions.Mode.Length > 0)
            {
                for (int i = 0; i < SpecificalOptions.specificalOptions.ServerOptions.Mode.Length; i++)
                {
                    GameObject TextForScrool = Instantiate(PrefabHostGameTextForScroll) as GameObject;
                    TextForScrool.transform.SetParent(ModeContent);
                    TextForScrool.transform.position = ModeContent.position;

                    TextForScrool.GetComponent<TMP_Text>().text = SpecificalOptions.specificalOptions.ServerOptions.Mode[i];
                }
            }
            else
            {
                GameObject TextForScrool = Instantiate(PrefabHostGameTextForScroll) as GameObject;
                TextForScrool.transform.SetParent(ModeContent);
                TextForScrool.transform.position = ModeContent.position;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        ScrollView(MapsContent, MapsScrollBar, ref MapsScrollPos, ref MapsIndex);
        ScrollView(ModeContent, ModeScrollBar, ref ModeScrollPos, ref ModeIndex);

        SpecificalOptions.specificalOptions.ServerOptions.SelectMap = SpecificalOptions.specificalOptions.ServerOptions.MapControllers[MapsIndex];
        SpecificalOptions.specificalOptions.ServerOptions.SelectMode = SpecificalOptions.specificalOptions.ServerOptions.Mode[ModeIndex];
    }

    void ScrollView(Transform Content, Scrollbar scrollbar, ref float ScrollPosition, ref int index)
    {
        PositionAllContent = new float[Content.childCount];
        float distance = 1f / (PositionAllContent.Length - 1f);

        for (int i = 0; i < PositionAllContent.Length; i++)
            PositionAllContent[i] = distance * i;

        if (Input.GetMouseButton(0))
        {
            ScrollPosition = scrollbar.GetComponent<Scrollbar>().value;
        }
        else
        {
            for (int i = 0; i < PositionAllContent.Length; i++)
            {
                if (ScrollPosition < PositionAllContent[i] + (distance / 2) && ScrollPosition > PositionAllContent[i] - (distance / 2))
                {
                    index = i;
                    scrollbar.GetComponent<Scrollbar>().value = Mathf.Lerp(scrollbar.GetComponent<Scrollbar>().value, PositionAllContent[i], 0.1f);
                }
            }
        }
    }

    public void ClickLeftMapsScroll()
    {
        float distance = 1f / (MapsContent.childCount - 1f);
        MapsScrollPos -= distance;

        if (MapsScrollPos < 0)
            MapsScrollPos = 0;

        ScrollView(MapsContent, MapsScrollBar, ref MapsScrollPos, ref MapsIndex);
    }

    public void ClickRightMapsScroll()
    {
        float distance = 1f / (MapsContent.childCount - 1f);
        MapsScrollPos += distance;

        if (MapsScrollPos > MapsContent.childCount)
            MapsScrollPos = MapsContent.childCount;

        ScrollView(MapsContent, MapsScrollBar, ref MapsScrollPos, ref MapsIndex);
    }

    public void ClickLeftModeScroll()
    {
        float distance = 1f / (ModeContent.childCount - 1f);
        ModeScrollPos -= distance;


        if (ModeScrollPos < 0)
            ModeScrollPos = 0;

        ScrollView(ModeContent, ModeScrollBar, ref ModeScrollPos, ref ModeIndex);
    }

    public void ClickRightModeScroll()
    {
        float distance = 1f / (ModeContent.childCount - 1f);
        ModeScrollPos += distance;

        if (ModeScrollPos > ModeContent.childCount)
            ModeScrollPos = ModeContent.childCount;

        ScrollView(ModeContent, ModeScrollBar, ref ModeScrollPos, ref ModeIndex);
    }

    public void StartHostGame()
    {
        SpecificalOptions.specificalOptions._LightTransport.serverName = IServerName.text;
        SpecificalOptions.specificalOptions._LightTransport.maxServerPlayers = int.Parse(ICountPlayers.text);
        SpecificalOptions.specificalOptions._LightTransport.extraServerData = $"{SpecificalOptions.specificalOptions.ServerOptions.SelectMap.MapName} | {SpecificalOptions.specificalOptions.ServerOptions.SelectMode}";

        SpecificalOptions.specificalOptions.ServerOptions.CountBots = int.Parse(ICountBots.text);
    }

    public void ChangeValueCountsBot(float value)
    {
        ICountBots.text = value.ToString();
    }

    public void ChangeValueCountsPlayers(float value)
    {
        ICountPlayers.text = value.ToString();
    }

    public void EditInputCountBots()
    {
        ICountBots.text = rgx.Replace(ICountBots.text, "");

        CheckCount(ICountBots, SCountBots);
    }

    public void EditInputCountPlayers()
    {
        ICountPlayers.text = rgx.Replace(ICountPlayers.text, "");

        CheckCount(ICountPlayers, SCountPlayers);
    }

    void CheckCount(TMP_InputField _input, Slider _slider)
    {
        int Check = int.Parse(_input.text);

        if (Check < _slider.minValue)
            Check = (int)_slider.minValue;
        else if(Check > _slider.maxValue)
            Check = (int)_slider.maxValue;

        _input.text = Check.ToString();
        _slider.value = Check;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ServerInformation : MonoBehaviour
{
    public Button CurrentButtonServerSlot;

    public TextMeshProUGUI ServerNameText;
    public TextMeshProUGUI CountPlayersText;
    public TextMeshProUGUI MapNameText;
    public TextMeshProUGUI ModNameText;
    public TextMeshProUGUI PingText;

    public string ServerID;
    public string MapName;

    public bool IsSelectedButton = false;

    [SerializeField] private Color Normal;
    [SerializeField] private Color Selected;
    public Color GetColorNormal => Normal;
    public Color GetColorSelected => Selected;
}

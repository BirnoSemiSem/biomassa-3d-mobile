﻿using LightReflectiveMirror;
using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ServerBrowser : MonoBehaviour
{
    [SerializeField] private MainMenuControl MenuControl;

    [SerializeField] private Transform ScrollParent;
    [SerializeField] private GameObject ServerSlotPrefab;

    private List<ServerInformation> ButtonList = new List<ServerInformation>();

    private Button SelectButton;
    // Start is called before the first frame update
    void OnEnable()
    {
        if (SpecificalOptions.specificalOptions._LightTransport != null)
        {
            SpecificalOptions.specificalOptions._LightTransport.serverListUpdated.AddListener(ServerListUpdate);
            SpecificalOptions.specificalOptions._LightTransport.RequestServerList();
        }
    }

    void OnDisable()
    {
        if (SpecificalOptions.specificalOptions._LightTransport != null)
            SpecificalOptions.specificalOptions._LightTransport.serverListUpdated.RemoveListener(ServerListUpdate);

        foreach (Transform t in ScrollParent)
            Destroy(t.gameObject);
    }

    public void RefreshServerList()
    {
        SpecificalOptions.specificalOptions._LightTransport.RequestServerList();
    }

    public void ServerListUpdate()
    {
        foreach (Transform t in ScrollParent)
            Destroy(t.gameObject);

        ButtonList.Clear();
        SelectButton = null;
        SpecificalOptions.specificalOptions.ServerOptions.ServerID = null;

        for (int i = 0; i < SpecificalOptions.specificalOptions._LightTransport.relayServerList.Count; i++)
        {
            GameObject ServerSlot = Instantiate(ServerSlotPrefab, ScrollParent);
            ServerInformation serverInformation = ServerSlot.GetComponent<ServerInformation>();

            serverInformation.ServerNameText.text = SpecificalOptions.specificalOptions._LightTransport.relayServerList[i].serverName;
            serverInformation.CountPlayersText.text = $"{SpecificalOptions.specificalOptions._LightTransport.relayServerList[i].currentPlayers}/{SpecificalOptions.specificalOptions._LightTransport.relayServerList[i].maxPlayers}";

            string[] ServerData = SpecificalOptions.specificalOptions._LightTransport.relayServerList[i].serverData.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

            serverInformation.MapNameText.text = $"{ServerData[0]}";
            serverInformation.ModNameText.text = $"{ServerData[1]}";

            serverInformation.MapName = ServerData[0].Replace(" ", "");

            serverInformation.PingText.text = null;
            StartCoroutine(StartPing(serverInformation, SpecificalOptions.specificalOptions._LightTransport.relayServerList[i].relayInfo.address));

            serverInformation.ServerID = SpecificalOptions.specificalOptions._LightTransport.relayServerList[i].serverId;

            Button ButtonServerSlot = ServerSlot.GetComponent<Button>();

            ButtonList.Add(serverInformation);

            ButtonServerSlot.onClick.AddListener(delegate{ SwicthSelectServer(ButtonServerSlot, serverInformation.ServerID, serverInformation.MapName); });
        }
    }

    public void SwicthSelectServer(Button _BtnSelect, string _serverID, string MapName)
    {
        SelectButton = _BtnSelect;
        SpecificalOptions.specificalOptions.ServerOptions.ServerID = _serverID;

        for(int i = 0; i < SpecificalOptions.specificalOptions.ServerOptions.MapControllers.Count; i++)
        {
            if(SpecificalOptions.specificalOptions.ServerOptions.MapControllers[i].MapName == MapName)
            {
                SpecificalOptions.specificalOptions.ServerOptions.SelectMap = SpecificalOptions.specificalOptions.ServerOptions.MapControllers[i];
            }
        }

        for(int i = 0; i < ButtonList.Count; i++)
        {
            var ColorBlock = ButtonList[i].CurrentButtonServerSlot.colors;

            if (ButtonList[i].CurrentButtonServerSlot == SelectButton)
            {
                ColorBlock.normalColor = ButtonList[i].GetColorSelected;
                ButtonList[i].CurrentButtonServerSlot.colors = ColorBlock;

                ButtonList[i].IsSelectedButton = true;
            }
            else
            {
                ColorBlock.normalColor = ButtonList[i].GetColorNormal;
                ButtonList[i].CurrentButtonServerSlot.colors = ColorBlock;

                ButtonList[i].IsSelectedButton = false;
            }
        }
    }

    public void ConnectToServer()
    {
        if(SelectButton != null && SelectButton.GetComponent<ServerInformation>().IsSelectedButton)
        {
            Debug.Log("Connecting...");

            NetworkManager.singleton.networkAddress = SpecificalOptions.specificalOptions.ServerOptions.ServerID;
            MenuControl.GameStartConnect();
        }
    }

    IEnumerator StartPing(ServerInformation serverInformation, string ip)
    {
        WaitForSeconds f = new WaitForSeconds(0.05f);
        Ping p = new Ping(ip);
        while (p.isDone == false)
        {
            yield return f;
        }
        serverInformation.PingText.text = p.time.ToString();
    }
}

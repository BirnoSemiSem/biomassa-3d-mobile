using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] private MainMenuControl MainMenuControl;

    [SerializeField] private List<Button> ButtonList = new List<Button>();
    public Button SelectButton;

    private Sprite NormalSpr;
    private Sprite SelectSpr;

    [SerializeField] private GameObject Slider;
    [SerializeField] private PalleteWindow palleteWindow;

    private void Start()
    {
        if (SelectButton != null)
        {
            NormalSpr = SelectButton.image.sprite;
            SelectSpr = SelectButton.spriteState.selectedSprite;

            SelectButton.image.sprite = SelectSpr;
        }
    }

    public void ClickBack(Button _button)
    {
        if (_button != SelectButton)
            MainMenuControl.ClickBack();
    }

    public void ClickButtonGroupSelect(Button _button)
    {
        if (Slider.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Out"))
            return;

        if (SelectButton != null)
        {
            SelectButton.image.sprite = NormalSpr;
        }

        SelectButton = _button;

        NormalSpr = SelectButton.image.sprite;
        SelectSpr = SelectButton.spriteState.selectedSprite;

        _button.image.sprite = SelectSpr;

        MainMenuControl.ButtonPlayAnimation(_button);
    }

    public void SliderTrigger(bool flags)
    {
        if(!flags)
        {
            Slider.GetComponent<Animator>().Play("Out");
            Invoke(nameof(OutEndAnimation), Slider.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
        }
        else
            Slider.SetActive(flags);
    }

    public void ClickChangePallet(GameObject pallet)
    {
        if (Slider.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Out"))
            return;

        StartCoroutine(StartChangePallet(pallet));
    }

    void OutEndAnimation()
    {
        Slider.SetActive(false);
    }

    IEnumerator StartChangePallet(GameObject pallet)
    {
        Slider.GetComponent<Animator>().Play("Out");
        yield return new WaitForSeconds(Slider.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
        palleteWindow.ActivePallete(pallet);
        Slider.GetComponent<Animator>().Play("In");
    }
}

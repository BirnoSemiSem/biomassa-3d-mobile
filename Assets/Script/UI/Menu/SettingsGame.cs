﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingsGame : MonoBehaviour
{
    [SerializeField] private Slider SliderMusic;
    [SerializeField] private Slider SliderEffect;

    [SerializeField] private TextMeshProUGUI TextMusicVol;
    [SerializeField] private TextMeshProUGUI TextEffectVol;

    [SerializeField] private Image SprMusic;
    [SerializeField] private Image SprEffect;

    [SerializeField] private Sprite SprMusicVolOn;
    [SerializeField] private Sprite SprEffectVolOn;

    [SerializeField] private Sprite SprMusicVolOff;
    [SerializeField] private Sprite SprEffectVolOff;

    // Start is called before the first frame update
    void Start()
    {
        SliderEffect.value = SpecificalOptions.specificalOptions.AudioEffectVolume;
        SliderMusic.value = SpecificalOptions.specificalOptions.AudioMusicVolume;

        TextUpdate();
    }

    public void ChangeLanguageForEng()
    {
        LangsList.SetLanguage(1, true);
    }

    public void ChangeLanguageForRus()
    {
        LangsList.SetLanguage(0, true);
    }

    public void ChangeMusicVolume(float vol)
    {
        PlayerPrefs.SetFloat("AudioMusicVolume", vol);
        SpecificalOptions.specificalOptions.AudioMusicVolume = vol;

        TextUpdate();
    }

    public void ClickSwithMusicVol()
    {
        if (SliderMusic.value > 0)
        {
            SliderMusic.value = 0;
            PlayerPrefs.SetFloat("AudioMusicVolume", 0);
            SpecificalOptions.specificalOptions.AudioMusicVolume = SliderMusic.value;
        }
        else
        {
            SliderMusic.value = 0.6f;
            PlayerPrefs.SetFloat("AudioMusicVolume", 0.6f);
            SpecificalOptions.specificalOptions.AudioMusicVolume = SliderMusic.value;
        }

        TextUpdate();
    }

    public void ChangeEffectVolume(float vol)
    {
        PlayerPrefs.SetFloat("AudioEffectVolume", vol);
        SpecificalOptions.specificalOptions.AudioEffectVolume = vol;

        TextUpdate();
    }

    public void ClickSwithEffectVol()
    {
        if (SliderEffect.value > 0)
        {
            SliderEffect.value = 0;
            PlayerPrefs.SetFloat("AudioEffectVolume", 0);
            SpecificalOptions.specificalOptions.AudioEffectVolume = SliderEffect.value;
        }
        else
        {
            SliderEffect.value = 0.6f;
            PlayerPrefs.SetFloat("AudioEffectVolume", 0.6f);
            SpecificalOptions.specificalOptions.AudioEffectVolume = SliderEffect.value;
        }

        TextUpdate();
    }

    void TextUpdate()
    {
        if(TextMusicVol)
            TextMusicVol.text = $"{(int)(SliderMusic.value * 100)}";

        if(TextEffectVol)
            TextEffectVol.text = $"{(int)(SliderEffect.value * 100)}";

        if (SliderMusic.value <= 0 && SprMusic.sprite != SprMusicVolOff)
            SprMusic.sprite = SprMusicVolOff;
        else if (SliderMusic.value > 0 && SprMusic.sprite != SprMusicVolOn)
            SprMusic.sprite = SprMusicVolOn;

        if (SliderEffect.value <= 0 && SprEffect.sprite != SprEffectVolOff)
            SprEffect.sprite = SprEffectVolOff;
        else if (SliderEffect.value > 0 && SprEffect.sprite != SprEffectVolOn)
            SprEffect.sprite = SprEffectVolOn;
    }

    public void Save()
    {
        PlayerPrefs.Save();
    }
}

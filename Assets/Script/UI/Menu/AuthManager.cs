﻿using Firebase;
using Firebase.Auth;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AuthManager : MonoBehaviour
{
    public DependencyStatus dependencyStatus;
    public FirebaseAuth auth;
    public FirebaseUser User;

    // Start is called before the first frame update
    [SerializeField] private GameObject LogInWindow;
    [SerializeField] private GameObject SignInWindow;
    [SerializeField] private GameObject ResetInWindow;

    private Coroutine CorAuthError;

    [SerializeField] private GameObject AuthErrorWidnow;
    [SerializeField] private TMP_Text AuthErrorText;

    [SerializeField] private TMP_InputField EmailLogIn, PasswordLogIn;
    [SerializeField] private TMP_InputField EmailSignIn, NickNameSignIn, PasswordSignIn;
    [SerializeField] private TMP_InputField EmailReset;

    [SerializeField] private TMP_InputField SettingsNickName;
    [SerializeField] private TMP_InputField SettingsPassword, SettingsPasswordReplay;
    [SerializeField] private TMP_InputField SettingsEmail;

    [SerializeField] private MainMenuControl MenuControl;
    [SerializeField] private GameObject AuthenticationUserWindow;

    [SerializeField] private bool IsVrificationEmail = false;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Awake()
    {
        //Check that all of the necessary dependencies for Firebase are present on the system
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                //If they are avalible Initialize Firebase
                InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        //Set the authentication instance object
        auth = FirebaseAuth.DefaultInstance;

        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    private void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != User)
        {
            bool signedin = User != auth.CurrentUser && auth.CurrentUser != null;

            if (!signedin && User != null)
            {
                Debug.Log("Signed Out");
            }

            User = auth.CurrentUser;

            if (signedin)
                Debug.Log($"Signed In: {User.DisplayName}");
        }
    }

    public void SingIn()
    {
        EmailSignIn.text = "";
        PasswordSignIn.text = "";
        NickNameSignIn.text = "";

        MenuControl.LoadingWindow.LoadingWindowClear();

        AuthenticationUserWindow.SetActive(true);

        LogInWindow.SetActive(false);
        SignInWindow.SetActive(true);
        ResetInWindow.SetActive(false);
    }

    public void LogIn()
    {
        EmailLogIn.text = "";
        PasswordLogIn.text = "";

        MenuControl.LoadingWindow.LoadingWindowClear();

        AuthenticationUserWindow.SetActive(true);

        LogInWindow.SetActive(true);
        SignInWindow.SetActive(false);
        ResetInWindow.SetActive(false);
    }

    public void ResetIn()
    {
        EmailReset.text = "";

        MenuControl.LoadingWindow.LoadingWindowClear();

        AuthenticationUserWindow.SetActive(true);

        LogInWindow.SetActive(false);
        SignInWindow.SetActive(false);
        ResetInWindow.SetActive(true);
    }

    public void SignUp()
    {
        MenuControl.LoadingWindow.LoadingWindowUp(LangsList.GetWord("Connecting"));
        StartCoroutine(Register(EmailSignIn.text, PasswordSignIn.text, NickNameSignIn.text));
    }

    public void LogUp()
    {
        MenuControl.LoadingWindow.LoadingWindowUp(LangsList.GetWord("Connecting"));
        StartCoroutine(Login(EmailLogIn.text, PasswordLogIn.text));
    }

    public void ResetUp()
    {
        StartCoroutine(SendEmailResetPassword(EmailReset.text));
    }

    public void ChangeProfilPlayer()
    {
        if(SettingsNickName.text == User.DisplayName)
        {
            MenuControl.DialogWindow.ShortDialog(LangsList.GetWord("ErrorChangeNick"));
            return;
        }

        StartCoroutine(UpdateProfile(SettingsNickName.text, null));
    }
    public void ChangePassword()
    {
        if(SettingsPassword.text != SettingsPasswordReplay.text)
        {
            MenuControl.DialogWindow.ShortDialog(LangsList.GetWord("ErrorChangePassword"));
            return;
        }

        StartCoroutine(UpdatePassword(SettingsPassword.text));
    }

    public void ChangeEmail()
    {
        if (SettingsEmail.text == User.Email)
        {
            MenuControl.DialogWindow.ShortDialog(LangsList.GetWord("ErrorChangeEmail"));
            return;
        }

        StartCoroutine(UpdateEmail(SettingsEmail.text));
    }

    public void LogUpAutomatically()
    {
        StartCoroutine(CheckAutologin());
    }

    public void SignOut()
    {
        StartCoroutine(SignOutUser());
    }

    private IEnumerator CheckAutologin()
    {
        yield return new WaitForEndOfFrame();

        if (User != null)
        {
            var reloaduserTask = User.ReloadAsync();
            yield return new WaitUntil(predicate: () => reloaduserTask.IsCompleted);

            AutoLogin();
        }
        else
        {
            LogIn();
        }
    }

    public void AutoLogin()
    {
        if(User != null)
        {
            if (IsVrificationEmail)
            {
                if (User.IsEmailVerified)
                {
                    SpecificalOptions.specificalOptions.PlayerInfo.Email = User.Email;
                    SpecificalOptions.specificalOptions.PlayerInfo.NickName = User.DisplayName;

                    SettingsNickName.text = User.DisplayName;
                    SettingsEmail.text = User.Email;

                    MenuControl.LoadingWindow.LoadingWindowClear();

                    MainMenuStart();
                }
                else
                {
                    StartCoroutine(SendVerificationEmail());
                    LogIn();
                }
            }
            else
            {
                SpecificalOptions.specificalOptions.PlayerInfo.Email = User.Email;
                SpecificalOptions.specificalOptions.PlayerInfo.NickName = User.DisplayName;

                SettingsNickName.text = User.DisplayName;
                SettingsEmail.text = User.Email;

                MenuControl.LoadingWindow.LoadingWindowClear();

                MainMenuStart();
            }
        }
        else
        {
            LogIn();
        }
    }

    void MainMenuStart()
    {
        AuthenticationUserWindow.SetActive(false);

        LogInWindow.SetActive(false);
        SignInWindow.SetActive(false);
    }

    private IEnumerator Login(string _email, string _password)
    {
        //Call the Firebase auth signin function passing the email and password
        var LoginTask = auth.SignInWithEmailAndPasswordAsync(_email, _password);
        //Wait until the task completes
        yield return new WaitUntil(predicate: () => LoginTask.IsCompleted);

        if (LoginTask.Exception != null)
        {
            //If there are errors handle them
            Debug.LogWarning(message: $"Failed to register task with {LoginTask.Exception}");

            string error = GetErrorMessage(LoginTask.Exception.GetBaseException());

            MenuControl.LoadingWindow.LoadingWindowClear();

            if (!LogInWindow.activeInHierarchy)
                LogInWindow.SetActive(true);

            if(CorAuthError == null)
                CorAuthError = StartCoroutine(AuthErrorActive($"{LangsList.GetWord("LoginFailed")} " + error));
            else
            {
                StopCoroutine(CorAuthError);
                CorAuthError = StartCoroutine(ChangeAuthErrorActive($"{LangsList.GetWord("LoginFailed")} " + error));
            }
            //MenuControl.DialogWindow.VerificationEmail("Login Failed! " + error);
        }
        else
        {
            //User is now logged in
            //Now get the result
            User = LoginTask.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", User.DisplayName, User.Email);

            if (IsVrificationEmail)
            {
                if (User.IsEmailVerified)
                {
                    SpecificalOptions.specificalOptions.PlayerInfo.Email = User.Email;
                    SpecificalOptions.specificalOptions.PlayerInfo.NickName = User.DisplayName;

                    SettingsNickName.text = User.DisplayName;
                    SettingsEmail.text = User.Email;

                    MenuControl.LoadingWindow.LoadingWindowClear();

                    MainMenuStart();
                }
                else
                {
                    LogIn();
                    StartCoroutine(SendVerificationEmail());
                }
            }
            else
            {
                SpecificalOptions.specificalOptions.PlayerInfo.Email = User.Email;
                SpecificalOptions.specificalOptions.PlayerInfo.NickName = User.DisplayName;

                SettingsNickName.text = User.DisplayName;
                SettingsEmail.text = User.Email;

                MenuControl.LoadingWindow.LoadingWindowClear();

                MainMenuStart();
            }
        }
    }

    private IEnumerator Register(string _email, string _password, string _username)
    {
        if (_username == "")
        {
            //If the username field is blank show a warning

            MenuControl.LoadingWindow.LoadingWindowClear();

            if (!SignInWindow.activeInHierarchy)
                SignInWindow.SetActive(true);

            if (CorAuthError == null)
                CorAuthError = StartCoroutine(AuthErrorActive(LangsList.GetWord("MissingUsername")));
            else
            {
                StopCoroutine(CorAuthError);
                CorAuthError = StartCoroutine(ChangeAuthErrorActive(LangsList.GetWord("MissingUsername")));
            }

            //MenuControl.DialogWindow.VerificationEmail("Missing Username");
        }
        else
        {
            //Call the Firebase auth signin function passing the email and password
            var RegisterTask = auth.CreateUserWithEmailAndPasswordAsync(_email, _password);
            //Wait until the task completes
            yield return new WaitUntil(predicate: () => RegisterTask.IsCompleted);

            if (RegisterTask.Exception != null)
            {
                //If there are errors handle them
                Debug.LogWarning(message: $"Failed to register task with {RegisterTask.Exception}");

                string error = GetErrorMessage(RegisterTask.Exception.GetBaseException());

                MenuControl.LoadingWindow.LoadingWindowClear();

                if (!SignInWindow.activeInHierarchy)
                    SignInWindow.SetActive(true);

                if (CorAuthError == null)
                    CorAuthError = StartCoroutine(AuthErrorActive($"{LangsList.GetWord("RegisterFailed")} " + error));
                else
                {
                    StopCoroutine(CorAuthError);
                    CorAuthError = StartCoroutine(ChangeAuthErrorActive($"{LangsList.GetWord("RegisterFailed")} " + error));
                }

                //MenuControl.DialogWindow.VerificationEmail("Register Failed!" + error);
            }
            else
            {
                //User has now been created
                //Now get the result
                User = RegisterTask.Result;

                if (User != null)
                {
                    //Create a user profile and set the username
                    UserProfile profile = new UserProfile { DisplayName = _username };

                    //Call the Firebase auth update user profile function passing the profile with the username
                    var ProfileTask = User.UpdateUserProfileAsync(profile);
                    //Wait until the task completes
                    yield return new WaitUntil(predicate: () => ProfileTask.IsCompleted);

                    if (ProfileTask.Exception != null)
                    {
                        //If there are errors handle them
                        Debug.LogWarning(message: $"Failed to register task with {ProfileTask.Exception}");
                        FirebaseException firebaseEx = ProfileTask.Exception.GetBaseException() as FirebaseException;
                        AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

                        MenuControl.LoadingWindow.LoadingWindowClear();

                        if (!SignInWindow.activeInHierarchy)
                            SignInWindow.SetActive(true);

                        if (CorAuthError == null)
                            CorAuthError = StartCoroutine(AuthErrorActive(LangsList.GetWord("UsernameSetFailed")));
                        else
                        {
                            StopCoroutine(CorAuthError);
                            CorAuthError = StartCoroutine(ChangeAuthErrorActive(LangsList.GetWord("UsernameSetFailed")));
                        }

                        //MenuControl.DialogWindow.VerificationEmail("Username Set Failed!");
                    }
                    else
                    {
                        //Username is now set
                        //Now return to login screen

                        if (IsVrificationEmail)
                        {
                            LogIn();
                            StartCoroutine(SendVerificationEmail());
                        }
                        else
                            StartCoroutine(Login(_email, _password));
                    }
                }
            }
        }
    }

    private IEnumerator SignOutUser()
    {
        MenuControl.LoadingWindow.LoadingWindowUp(LangsList.GetWord("Loading"));
        auth.SignOut();
        SceneManager.LoadSceneAsync(SpecificalOptions.specificalOptions.MainMenu);
        yield return null;
    }

    private IEnumerator SendVerificationEmail()
    {
        if (User != null)
        {
            var EmailVerificationTask = User.SendEmailVerificationAsync();
            yield return new WaitUntil(predicate: () => EmailVerificationTask.IsCompleted);

            if (EmailVerificationTask.Exception != null)
            {
                string error = GetErrorMessage(EmailVerificationTask.Exception.GetBaseException());

                MenuControl.DialogWindow.ShortDialog($"{LangsList.GetWord("EmailAuthenticationError")} " + error);
            }
            else
            {
                MenuControl.DialogWindow.ShortDialog(LangsList.GetWord("VereficationEmail"));

                Debug.Log("SendEmailVerificationAsync Good!");
            }
        }
    }

    private IEnumerator SendEmailResetPassword(string _email)
    {
        if (User != null)
        {
            var EmailResetPasswordTask = auth.SendPasswordResetEmailAsync(_email);
            yield return new WaitUntil(predicate: () => EmailResetPasswordTask.IsCompleted);

            if (EmailResetPasswordTask.Exception != null)
            {
                string error = GetErrorMessage(EmailResetPasswordTask.Exception.GetBaseException());

                MenuControl.DialogWindow.ShortDialog($"{LangsList.GetWord("RecoverPasswordError")} " + error);
            }
            else
            {
                MenuControl.DialogWindow.ShortDialog(LangsList.GetWord("RecoverPassword"));

                Debug.Log("SendEmailResetPassword Good!");
            }
        }
    }

    private IEnumerator UpdateProfile(string _nickname, string _UrlImage)
    {
        if (User != null)
        {
            Firebase.Auth.UserProfile profile = new Firebase.Auth.UserProfile
            {
                DisplayName = _nickname,
                //PhotoUrl = new System.Uri(_UrlImage),
            };

            var UpdateEmailTask = User.UpdateUserProfileAsync(profile);
            yield return new WaitUntil(predicate: () => UpdateEmailTask.IsCompleted);

            if (UpdateEmailTask.Exception != null)
            {
                string error = GetErrorMessage(UpdateEmailTask.Exception.GetBaseException());

                MenuControl.DialogWindow.ShortDialog($"{LangsList.GetWord("UpdateProfileError")} " + error);
            }
            else
            {
                SpecificalOptions.specificalOptions.PlayerInfo.NickName = _nickname;

                MenuControl.DialogWindow.ShortDialog(LangsList.GetWord("ProfileUpdate"));

                Debug.Log("UpdateProfile Good!");
            }
        }
    }

    private IEnumerator UpdateEmail(string _email)
    {
        if (User != null)
        {
            var UpdateEmailTask = User.UpdateEmailAsync(_email);
            yield return new WaitUntil(predicate: () => UpdateEmailTask.IsCompleted);

            if (UpdateEmailTask.Exception != null)
            {
                string error = GetErrorMessage(UpdateEmailTask.Exception.GetBaseException());

                if(error == "RECONNECT")
                    MenuControl.DialogWindow.ShortDialog(LangsList.GetWord("EmailSecurityMeasures"));
                else
                    MenuControl.DialogWindow.ShortDialog($"{LangsList.GetWord("UpdateEmailError")} " + error);
            }
            else
            {
                SpecificalOptions.specificalOptions.PlayerInfo.Email = _email;

                MenuControl.DialogWindow.ShortDialog(LangsList.GetWord("EmailChanged"));

                Debug.Log("UpdateEmail Good!");
            }
        }
    }

    private IEnumerator UpdatePassword(string _password)
    {
        if (User != null)
        {
            var UpdatePasswordTask = User.UpdatePasswordAsync(_password);
            yield return new WaitUntil(predicate: () => UpdatePasswordTask.IsCompleted);

            if (UpdatePasswordTask.Exception != null)
            {
                string error = GetErrorMessage(UpdatePasswordTask.Exception.GetBaseException());

                if (error == "RECONNECT")
                    MenuControl.DialogWindow.ShortDialog(LangsList.GetWord("PasswordSecurityMeasures"));
                else
                    MenuControl.DialogWindow.ShortDialog($"{LangsList.GetWord("UpdatePasswordError")} " + error);
            }
            else
            {
                MenuControl.DialogWindow.ShortDialog(LangsList.GetWord("PasswordChanged"));

                Debug.Log("UpdatePassword Good!");
            }
        }
    }

    private IEnumerator AuthErrorActive(string text)
    {
        AuthErrorWidnow.GetComponent<Animator>().Play("Out");

        AuthErrorWidnow.GetComponent<AudioSource>().Play();

        AuthErrorText.text = text;
        yield return new WaitForSeconds(10f);

        AuthErrorWidnow.GetComponent<Animator>().Play("In");

        CorAuthError = null;
    }

    private IEnumerator ChangeAuthErrorActive(string text)
    {
        AuthErrorWidnow.GetComponent<Animator>().Play("In");

        yield return new WaitForSeconds(AuthErrorWidnow.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);

        AuthErrorWidnow.GetComponent<Animator>().Play("Out");

        AuthErrorWidnow.GetComponent<AudioSource>().Play();

        AuthErrorText.text = text;
        yield return new WaitForSeconds(10f);

        AuthErrorWidnow.GetComponent<Animator>().Play("In");

        CorAuthError = null;
    }

    private string GetErrorMessage(Exception exception)
    {
        Debug.Log(exception.ToString());
        Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
        if (firebaseEx != null)
        {
            var errorCode = (AuthError)firebaseEx.ErrorCode;
            return GetErrorMessage(errorCode);
        }

        return exception.ToString();
    }

    private string GetErrorMessage(AuthError errorCode)
    {
        var message = "";
        switch (errorCode)
        {
            case AuthError.AccountExistsWithDifferentCredentials:
                message = LangsList.GetWord("AccountAlready");
                break;
            case AuthError.MissingPassword:
                message = LangsList.GetWord("PasswordRequired");
                break;
            case AuthError.WeakPassword:
                message = LangsList.GetWord("UnreliablePassword");
                break;
            case AuthError.WrongPassword:
                message = LangsList.GetWord("PasswordWrong");
                break;
            case AuthError.EmailAlreadyInUse:
                message = LangsList.GetWord("EmailAlready");
                break;
            case AuthError.InvalidEmail:
                message = LangsList.GetWord("IncorrectEmail");
                break;
            case AuthError.MissingEmail:
                message = LangsList.GetWord("EmailFieldNull");
                break;
            case AuthError.Cancelled:
                message = LangsList.GetWord("EmailChecksCancelled");
                break;
            case AuthError.TooManyRequests:
                message = LangsList.GetWord("TooManyRequests");
                break;
            case AuthError.UserNotFound:
                message = LangsList.GetWord("UserDoesNotExist");
                break;
            case AuthError.RequiresRecentLogin:
                message = "RECONNECT";
                break;
            default:
                message = errorCode.ToString();
                break;
        }

        return message;
    }
}

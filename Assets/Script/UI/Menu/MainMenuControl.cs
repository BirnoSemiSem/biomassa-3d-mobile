﻿using LightReflectiveMirror;
using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuControl : MonoBehaviour
{
    [SerializeField] private GameObject MirrorNet;
    private Coroutine CorNetwork;

    [SerializeField] private AudioSource AudioMenu;

    [SerializeField] private AuthManager AuthManager;
    public bool IsFirstLogin = false;

    //Menu
    [SerializeField] private GameObject SliderServerBrowser;
    [SerializeField] private GameObject ConnectGame;
    [SerializeField] private GameObject HostGame;

    [SerializeField] private GameObject SettingsMenu;
    [SerializeField] private GameObject SettingsAdvenceMenu;
    [SerializeField] private GameObject SettingsKeyControlMenu;

    [SerializeField] private GameObject WelcomToTheGame;

    //Elements Window
    [SerializeField] private List<GameObject> Windows;

    //Window
    public LoadingMenu LoadingWindow;
    public ErrorMenu ErrorWindow;
    public DialogMenu DialogWindow;

    //Text
    [SerializeField] private TMP_Text OnlineCountPlayers;
    [SerializeField] private TMP_Text CurrnetTime;

    void Start()
    {
        if (SpecificalOptions.specificalOptions != null)
            Destroy(MirrorNet);

        CorNetwork = StartCoroutine(ConnectingToLRM());

        if (SpecificalOptions.specificalOptions != null && SpecificalOptions.specificalOptions._LightTransport != null)
        {
            SpecificalOptions.specificalOptions._LightTransport.serverListUpdated.AddListener(UpdateCountPlayers);
            SpecificalOptions.specificalOptions._LightTransport.RequestServerList();
        }

        StartCoroutine(CountOnlinePlayers());
    }

    void OnEnable()
    {
        if (SpecificalOptions.specificalOptions != null && SpecificalOptions.specificalOptions._LightTransport != null)
        {
            SpecificalOptions.specificalOptions._LightTransport.serverListUpdated.AddListener(UpdateCountPlayers);
            SpecificalOptions.specificalOptions._LightTransport.RequestServerList();
        }
    }

    void OnDisable()
    {
        if (SpecificalOptions.specificalOptions._LightTransport != null)
            SpecificalOptions.specificalOptions._LightTransport.serverListUpdated.RemoveListener(UpdateCountPlayers);
    }

    public void GameStartConnect()
    {
        StartCoroutine(ConnectingToServer());
        //MainMenu.SetActive(false);
    }

    public void GameStartHost()
    {
        StartCoroutine(HostToServer());
        //MainMenu.SetActive(false);
    }

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    if (MainMenu.activeInHierarchy)
        //        DialogWindow.EscToGameExit();
        //    else
        //        DelWindows(Windows.Count-1);
        //}

        //if(MainMenu.activeInHierarchy && !WelcomToTheGame.activeInHierarchy && IsFirstLogin)
        //{
        //    AddWindows(WelcomToTheGame, false);

        //    IsFirstLogin = false;
        //}

        if (!SpecificalOptions.specificalOptions._LightTransport.Available() && CorNetwork == null)
            CorNetwork = StartCoroutine(ConnectingToLRM());

        if (AudioMenu && AudioMenu.volume != SpecificalOptions.specificalOptions.AudioMusicVolume)
            AudioMenu.volume = SpecificalOptions.specificalOptions.AudioMusicVolume;

        CurrnetTime.text = System.DateTime.Now.ToShortTimeString();
    }

    void UpdateCountPlayers()
    {
        int CountPlayers = 0;

        for (int i = 0; i < SpecificalOptions.specificalOptions._LightTransport.relayServerList.Count; i++)
            CountPlayers += SpecificalOptions.specificalOptions._LightTransport.relayServerList[i].currentPlayers;

        OnlineCountPlayers.text = CountPlayers.ToString();
    }

    public void ClickBack()
    {
        DelWindows(Windows.Count-1);
    }

    public void ClickCLoseAllWindow()
    {
        for(int i = 0; i < Windows.Count; i++)
        {
            DelWindows(i);
        }
    }

    public void ClickAddWindow(GameObject Window)
    {
        if ((Window.GetComponent<Animator>() && Window.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Out")) || Window.activeInHierarchy)
            return;

        AddWindows(Window);
    }

    public void ClickOpenTelegram()
    {
        Application.OpenURL("https://t.me/biomassamobile");
    }

    public void ClickOpenVK()
    {
        Application.OpenURL("https://vk.com/biomassamobile");
    }

    public void ClickOpenDiscordServer()
    {
        Application.OpenURL("https://discord.gg/v2HgTAPaxF");
    }

    void AddWindows(GameObject Window)
    {
        Windows.Add(Window);
        Window.SetActive(true);
    }

    void DelWindows(int index)
    {
        if(Windows.Count > 0)
        {
            if(Windows[index] != null)
            {
                if (Windows[index].GetComponent<Animator>())
                {
                    if (!Windows[index].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Out"))
                    {
                        Windows[index].GetComponent<Animator>().Play("Out");
                        Invoke(nameof(OutEndAnimation), Windows[index].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
                    }
                }
                else
                {
                    Windows[index].SetActive(false);
                    Windows.RemoveAt(index);
                }
            }
        }
    }

    void OutEndAnimation()
    {
        for(int i = 0; i < Windows.Count; i++)
        {
            if(Windows[i].activeInHierarchy && Windows[i].GetComponent<Animator>() && Windows[i].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Out"))
            {
                Windows[i].SetActive(false);
                Windows.RemoveAt(i);
            }
        }
    }

    public void ButtonPlayAnimation(Button button)
    {
        button.GetComponent<Animation>().Play();
    }

    IEnumerator ConnectingToServer()
    {
        LoadingWindow.LoadingWindowUp(LangsList.GetWord("Connecting"));

        yield return new WaitForSeconds(1f);

        if (!SpecificalOptions.specificalOptions._LightTransport.Available())
        {
            ErrorWindow.ErrorWindowUp(LangsList.GetWord("ReasonNoConnection"));

            ErrorWindow.BtnContinuous.onClick.AddListener(ClickReConnectRelay);
            ErrorWindow.BtnContinuousText.text = LangsList.GetWord("Repeat");

            ErrorWindow.BtnExit.onClick.AddListener(ClickExitGame);
            ErrorWindow.BtnExitText.text = LangsList.GetWord("Exit");

            yield break;
        }

        LoadingWindow.LoadingWindowUp(LangsList.GetWord("Loading"));
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(SpecificalOptions.specificalOptions.Game);
        SpecificalOptions.specificalOptions.IsChangeScene = true;

        asyncOperation.allowSceneActivation = false;

        while (!asyncOperation.isDone)
        {
            if (asyncOperation.progress >= .9f)
                break;
            else
                yield return null;
        }

        NetworkManager.singleton.StartClient();
        LoadingWindow.LoadingWindowUp(LangsList.GetWord("Connecting"));

        int CountTimeOut = 0;

        while (!NetworkClient.isConnecting && CountTimeOut <= ((kcp2k.KcpTransport)SpecificalOptions.specificalOptions._LightTransport.clientToServerTransport).Timeout)
        {
            yield return new WaitForSeconds(1f);
            CountTimeOut +=1000;
        }

        if (CountTimeOut >= 5)
        {
            ErrorWindow.ErrorWindowUp(LangsList.GetWord("UnableConnectToServer"));

            ErrorWindow.BtnContinuous.onClick.AddListener(ClickReConnectServer);
            ErrorWindow.BtnContinuousText.text = LangsList.GetWord("Repeat");

            ErrorWindow.BtnExit.onClick.AddListener(ClickResetWindows);
            ErrorWindow.BtnExitText.text = LangsList.GetWord("Back");
            yield break;
        }

        SpecificalOptions.specificalOptions.IsOnlyClient = true;

        asyncOperation.allowSceneActivation = true;

        yield return null;
    }

    IEnumerator HostToServer()
    {
        LoadingWindow.LoadingWindowUp(LangsList.GetWord("Connecting"));

        yield return new WaitForSeconds(1f);

        if (!SpecificalOptions.specificalOptions._LightTransport.Available())
        {
            ErrorWindow.ErrorWindowUp(LangsList.GetWord("ReasonNoConnection"));

            ErrorWindow.BtnContinuous.onClick.AddListener(ClickReConnectRelay);
            ErrorWindow.BtnContinuousText.text = LangsList.GetWord("Repeat");

            ErrorWindow.BtnExit.onClick.AddListener(ClickExitGame);
            ErrorWindow.BtnExitText.text = LangsList.GetWord("Exit");

            yield break;
        }

        LoadingWindow.LoadingWindowUp(LangsList.GetWord("Loading"));
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(SpecificalOptions.specificalOptions.Game);
        SpecificalOptions.specificalOptions.IsChangeScene = true;

        asyncOperation.allowSceneActivation = false;

        while (!asyncOperation.isDone)
        {
            if (asyncOperation.progress >= .9f)
                break;
            else
                yield return null;
        }

        SpecificalOptions.specificalOptions.IsHost = true;

        asyncOperation.allowSceneActivation = true;

        yield return null;
    }

    IEnumerator ConnectingToLRM()
    {
        LoadingWindow.LoadingWindowUp(LangsList.GetWord("Connecting"));

        yield return new WaitForSeconds(1f);

        if (SpecificalOptions.specificalOptions._LightTransport == null)
        {
            ErrorWindow.ErrorWindowUp(LangsList.GetWord("CauseNoClientNetwork"));

            ErrorWindow.BtnContinuous.onClick.AddListener(ClickReConnectRelay);
            ErrorWindow.BtnContinuousText.text = LangsList.GetWord("Repeat");

            ErrorWindow.BtnExit.onClick.AddListener(ClickExitGame);
            ErrorWindow.BtnExitText.text = LangsList.GetWord("Exit");

            yield break;
        }

        int CountTimeOut = 0;

        while (!SpecificalOptions.specificalOptions._LightTransport.Available() && CountTimeOut <= ((kcp2k.KcpTransport)SpecificalOptions.specificalOptions._LightTransport.clientToServerTransport).Timeout)
        {
            yield return new WaitForSeconds(1f);
            CountTimeOut += 1000;
        }

        if (!SpecificalOptions.specificalOptions._LightTransport.Available())
        {
            ErrorWindow.ErrorWindowUp(LangsList.GetWord("ReasonNoConnection"));

            ErrorWindow.BtnContinuous.onClick.AddListener(ClickReConnectRelay);
            ErrorWindow.BtnContinuousText.text = LangsList.GetWord("Repeat");

            ErrorWindow.BtnExit.onClick.AddListener(ClickExitGame);
            ErrorWindow.BtnExitText.text = LangsList.GetWord("Exit");

            yield break;
        }

        AuthManager.LogUpAutomatically();

        CorNetwork = null;
    }

    IEnumerator CountOnlinePlayers()
    {
        while(true)
        {
            yield return new WaitForSeconds(10f);

            if(SpecificalOptions.specificalOptions._LightTransport.Available())
                SpecificalOptions.specificalOptions._LightTransport.RequestServerList();
        }
    }

    public void ClickReConnectRelay()
    {
        if (SpecificalOptions.specificalOptions._LightTransport == null)
            SpecificalOptions.specificalOptions._LightTransport = (LightReflectiveMirrorTransport)Transport.activeTransport;

        SpecificalOptions.specificalOptions._LightTransport.ConnectToRelay();

        ErrorWindow.ErrorWindowClear();
        CorNetwork = StartCoroutine(ConnectingToLRM());
    }

    public void ClickReConnectServer()
    {
        if (SpecificalOptions.specificalOptions._LightTransport == null)
            SpecificalOptions.specificalOptions._LightTransport = (LightReflectiveMirrorTransport)Transport.activeTransport;

        NetworkManager.singleton.networkAddress = SpecificalOptions.specificalOptions.ServerOptions.ServerID;
        NetworkManager.singleton.StartClient();

        ErrorWindow.ErrorWindowClear();
        CorNetwork = StartCoroutine(ConnectingToServer());
    }

    public void ClickExitGame()
    {
        DialogWindow.EscToGameExit();
    }

    public void ClickResetWindows()
    {
        ErrorWindow.ErrorWindowClear();
        LoadingWindow.LoadingWindowClear();

        SpecificalOptions.specificalOptions.IsChangeScene = false;
    }
}

﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DialogMenu : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private GameObject DialogWindow;

    [SerializeField] private TMP_Text Text;

    [SerializeField] private Button ButtonYes;
    [SerializeField] private TMP_Text ButtonYesText;

    [SerializeField] private Button ButtonNo;
    [SerializeField] private TMP_Text ButtonNoText;


    [SerializeField] private LoadingMenu Loading;

    public void EscToGameExit()
    {
        if (!DialogWindow.activeInHierarchy)
        {
            DialogWindow.SetActive(true);
            ExitDialog(LangsList.GetWord("QuitGame"));
        }
        else
            DialogWindow.SetActive(false);
    }

    public void EscToMainMenu()
    {
        if (!DialogWindow.activeInHierarchy)
        {
            DialogWindow.SetActive(true);
            ExitToMainMenu(LangsList.GetWord("BackMainMenu"));
        }
        else
            DialogWindow.SetActive(false);
    }

    public void ExitDialog(string str)
    {
        ButtonYes.gameObject.SetActive(true);
        ButtonNo.gameObject.SetActive(true);

        ButtonYesText.text = LangsList.GetWord("Yes");
        ButtonNoText.text = LangsList.GetWord("No");

        Text.text = str;

        ButtonYes.onClick.AddListener(ExitGame);
        ButtonNo.onClick.AddListener(DialogExitClose);
    }

    public void ExitToMainMenu(string str)
    {
        ButtonYes.gameObject.SetActive(true);
        ButtonNo.gameObject.SetActive(true);

        ButtonYesText.text = LangsList.GetWord("Yes");
        ButtonNoText.text = LangsList.GetWord("No");

        Text.text = str;

        ButtonYes.onClick.AddListener(ExitMainMenu);
        ButtonNo.onClick.AddListener(DialogExitClose);
    }

    public void ShortDialog(string str)
    {
        DialogWindow.SetActive(true);

        ButtonYes.gameObject.SetActive(false);
        ButtonNo.gameObject.SetActive(true);

        ButtonNoText.text = LangsList.GetWord("Ok");

        Text.text = str;

        ButtonNo.onClick.AddListener(DialogExitClose);
    }

    //Function Button

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ExitMainMenu()
    {
        DialogExitClose();

        SpecificalOptions.specificalOptions.IsChangeScene = true;

        if (SpecificalOptions.specificalOptions.IsHost)
            NetworkManager.singleton.StopHost();
        else if (SpecificalOptions.specificalOptions.IsOnlyClient)
            NetworkManager.singleton.StopClient();

        Loading.LoadingWindowUp(LangsList.GetWord("Loading"));
        SceneManager.LoadSceneAsync(SpecificalOptions.specificalOptions.MainMenu);
    }

    public void DialogExitClose()
    {
        DialogWindow.SetActive(false);
    }
}

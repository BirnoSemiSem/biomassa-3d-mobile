using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalleteWindow : MonoBehaviour
{
    [SerializeField] private List<GameObject> Pallets = new List<GameObject>();

    public void ActivePallete(GameObject pallet)
    {
        for(int i = 0; i < Pallets.Count; i++)
        {
            if (Pallets[i] == pallet)
                Pallets[i].SetActive(true);
            else
                Pallets[i].SetActive(false);
        }
    }
}

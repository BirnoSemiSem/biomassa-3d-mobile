﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlController : MonoBehaviour
{
    // Start is called before the first frame update

    public enum Controller
    {
        PC,
        Android
    };

    public Controller Control;

    public GameObject GameUI;

    public ControllerPlayer controllerPlayer;
    public CameraPlayer CameraLocalPlayer;

    public Player PlayerControlLocal;
    public Player PlayerControlSpec;

    public int IndexSpec = -1;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPauseMenu || Chat.IsChatActive)
        {
            controllerPlayer.ChangeTargetMove(Vector3.zero);

            if (PlayerControlLocal.weaponControl.IsFire)
                PlayerControlLocal.weaponControl.ClickButtonFireExit();

            return;
        }

        if(Control == Controller.PC && GameUI.activeInHierarchy)
        {
            if (PlayerControlLocal)
            {
                if (PlayerControlLocal.IsAlive)
                {
                    if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
                    {
                        float movengX = Input.GetAxis("Horizontal");
                        float movengY = Input.GetAxis("Vertical");

                        Vector3 movement = new Vector3(movengX > 0 || movengX < 0 ? movengX : 0, 0, movengY > 0 || movengY < 0 ? movengY : 0);

                        controllerPlayer.ChangeTargetMove(movement);
                    }
                    else
                        controllerPlayer.ChangeTargetMove(Vector3.zero);

                    if (Input.GetButton("Fire1"))
                        PlayerControlLocal.weaponControl.ClickButtonFireEnter();
                    else if(Input.GetKey(KeyCode.R))
                        PlayerControlLocal.weaponControl.ClickButtonReloadEnter();
                    else
                    {
                        if (PlayerControlLocal.weaponControl.IsFire)
                            PlayerControlLocal.weaponControl.ClickButtonFireExit();
                    }

                    if (Input.GetKey(KeyCode.Alpha1))
                        PlayerControlLocal.weaponControl.ClickChangeWeapon(0);
                    else if (Input.GetKey(KeyCode.Alpha2))
                        PlayerControlLocal.weaponControl.ClickChangeWeapon(1);
                }
                else if(!PlayerControlLocal.IsAlive)
                {
                    if (Input.GetButtonDown("Fire1") && IndexSpec != -1)
                        LeftButtonSpect();
                    else if (Input.GetButtonDown("Fire2") && IndexSpec != -1)
                        RightButtonSpect();
                }

                if (!PlayerControlLocal.IsAlive && PlayerControlSpec != null)
                {
                    if (PlayerControlLocal.transform.position != PlayerControlSpec.transform.position)
                        PlayerControlLocal.transform.position = PlayerControlSpec.transform.position;
                }
            }
        }
    }

    public void SpectrMode()
    {
        if (GameControl.Players == null || GameControl.Players.Length <= 0)
            return;

        SpectrLogic(true, 0);
    }

    public void LeftButtonSpect()
    {
        if (GameControl.Players == null || GameControl.Players.Length <= 0)
            return;

        SpectrLogic(false, -1);
    }

    public void RightButtonSpect()
    {
        if (GameControl.Players == null || GameControl.Players.Length <= 0)
            return;

        SpectrLogic(false, 1);
    }

    private void SpectrLogic(bool IsRandom, int DirectionForIndex)
    {
        if(IsRandom)
            IndexSpec = Random.Range(0, GameControl.Players.Length);
        else
            IndexSpec = IndexSpec + DirectionForIndex;

        if (IndexSpec >= GameControl.Players.Length)
            IndexSpec = 0;
        else if (IndexSpec <= -1)
            IndexSpec = GameControl.Players.Length - 1;

        if (GameControl.Players[IndexSpec])
            PlayerControlSpec = GameControl.Players[IndexSpec].GetComponent<Player>();

        int count = GameControl.Players.Length - 1;

        while (PlayerControlSpec && !PlayerControlSpec.IsAlive && count > 0)
        {
            if (IsRandom)
                IndexSpec = Random.Range(0, GameControl.Players.Length);
            else
                IndexSpec = IndexSpec + DirectionForIndex;

            if (IndexSpec >= GameControl.Players.Length)
                IndexSpec = 0;
            else if (IndexSpec <= -1)
                IndexSpec = GameControl.Players.Length - 1;

            if (GameControl.Players[IndexSpec])
                PlayerControlSpec = GameControl.Players[IndexSpec].GetComponent<Player>();

            count--;
        }

        PlayerControlLocal.transform.position = PlayerControlSpec.transform.position;
        CameraLocalPlayer.ChangePlayerTarget(PlayerControlSpec.gameObject);
    }
}

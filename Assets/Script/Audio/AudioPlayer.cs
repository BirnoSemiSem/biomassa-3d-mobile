﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    public enum AudioPlayerControlSound
    {
        Spawn,
        Death,
        Pain,
        Infection,

        Fire,
        ReloadStart,
        ReloadEnd,
        Draw
    };

    [SerializeField] private Player _Player;
    private int _PlayerID;

    [SerializeField] private ControllerPlayer _PlayerController;
    [SerializeField] private BotControl _BotController;

    [SerializeField] private WeaponControl _WeaponControl;

    [SerializeField] private AudioSource FeetAudio;
    [SerializeField] private AudioSource WeaponAudio;
    [SerializeField] private AudioSource PlayerAudio;

    private AudioClip[] FeetClips;
    private AudioClip[] DeathClips;
    private AudioClip[] PainClips;
    private AudioClip[] InfectionClips;

    private AudioClip FireClip;
    private AudioClip DrawClip;
    private AudioClip[] ReloadClips;

    private AudioClip[] KnifeMissClips;
    private AudioClip[] KnifeStrikeClips;

    [SerializeField] private AudioClip[] Event;

    private float CurrentFloatTime;
    private bool IsBot = false;
    // Start is called before the first frame update
    void Start()
    {
        if (!_PlayerController)
            IsBot = true;
        else
            IsBot = false;

        _PlayerID = _Player.GetPlayerID;

        SettingsAudioVolume();
    }

    void OnEnable()
    {
        GameControl.EventSpawnPlayer += SoundPlayerSpawn;
        Damage.EventTakeDamage += SoundPlayerPain;
        Damage.EventDeathPlayer += SoundPlayerDeath;
        Biohazard.EventInfectionPlayer += SoundPlayerInfection;

        _WeaponControl.EventPlayerAttackFireWeapon += SoundPlayerAttackFireWeapon;
        _WeaponControl.EventPlayerAttackFireKnife += SoundPlayerAttackFireKnife;
        _WeaponControl.EventPlayerStartSoundReloading += SoundPlayerStartReloading;
        _WeaponControl.EventPlayerEndSoundReloading += SoundPlayerEndReloading;
        _WeaponControl.EventPlayerStartDraw += SoundPlayerStartDraw;
    }

    void OnDisable()
    {
        GameControl.EventSpawnPlayer -= SoundPlayerSpawn;
        Damage.EventTakeDamage -= SoundPlayerPain;
        Damage.EventDeathPlayer -= SoundPlayerDeath;
        Biohazard.EventInfectionPlayer -= SoundPlayerInfection;

        _WeaponControl.EventPlayerAttackFireWeapon -= SoundPlayerAttackFireWeapon;
        _WeaponControl.EventPlayerAttackFireKnife -= SoundPlayerAttackFireKnife;
        _WeaponControl.EventPlayerStartSoundReloading -= SoundPlayerStartReloading;
        _WeaponControl.EventPlayerEndSoundReloading -= SoundPlayerEndReloading;
        _WeaponControl.EventPlayerStartDraw -= SoundPlayerStartDraw;
    }

    // Update is called once per frame
    void Update()
    {
        SettingsAudioVolume();

        if (!GameControl.IsFreeze && _Player.IsAlive)
            Feet();
    }

    void SettingsAudioVolume()
    {
        if (FeetAudio && FeetAudio.volume != SpecificalOptions.specificalOptions.AudioEffectVolume)
            FeetAudio.volume = SpecificalOptions.specificalOptions.AudioEffectVolume;

        if (WeaponAudio && WeaponAudio.volume != SpecificalOptions.specificalOptions.AudioEffectVolume)
            WeaponAudio.volume = SpecificalOptions.specificalOptions.AudioEffectVolume;

        if (PlayerAudio && PlayerAudio.volume != SpecificalOptions.specificalOptions.AudioEffectVolume)
            PlayerAudio.volume = SpecificalOptions.specificalOptions.AudioEffectVolume;
    }

    void Feet()
    {
        if (FeetClips != null && FeetClips.Length > 0)
        {
            CurrentFloatTime -= Time.deltaTime;

            if(CurrentFloatTime <= 0)
            {
                if((!IsBot && _PlayerController.TargetMove != Vector3.zero) || (IsBot && _BotController.VelocityMove != Vector2.zero))
                {
                    FeetAudio.clip = FeetClips[Random.Range(0, FeetClips.Length)];
                    FeetAudio.Play();
                    CurrentFloatTime = Random.Range(0.25f, 0.4f);
                }
            }
        }
    }

    void SoundPlayerSpawn(GameObject Player, int id)
    {
        if (id == _PlayerID)
        {
            StartCoroutine(SoundPlay(AudioPlayerControlSound.Spawn));
        }
    }

    void SoundPlayerPain(GameObject victim, GameObject attacker, int damage)
    {
        if (victim.GetInstanceID() == _PlayerID)
        {
            if (victim.GetComponent<Player>().Health > 0 && !PlayerAudio.isPlaying &&
                victim.GetComponent<Player>().PlayerTeam != attacker.GetComponent<Player>().PlayerTeam)
            {
                StartCoroutine(SoundPlay(AudioPlayerControlSound.Pain));
            }
        }
    }

    void SoundPlayerDeath(GameObject Player, int id)
    {
        if (id == _PlayerID)
        {
            StartCoroutine(SoundPlay(AudioPlayerControlSound.Death));
        }
    }

    void SoundPlayerInfection(GameObject Player, int id)
    {
        if (id == _PlayerID)
        {
            StartCoroutine(SoundPlay(AudioPlayerControlSound.Infection));
        }
    }

    void SoundPlayerAttackFireWeapon(GameObject Player, int id)
    {
        if (id == _PlayerID)
        {
            StartCoroutine(SoundPlay(AudioPlayerControlSound.Fire));
        }
    }

    void SoundPlayerAttackFireKnife(GameObject Player, int id, HitKnife Hit)
    {
        if (id == _PlayerID)
        {
            StartCoroutine(SoundPlayKnife(Hit));
        }
    }

    void SoundPlayerStartReloading(GameObject Player, int id)
    {
        if (id == _PlayerID)
        {
            StartCoroutine(SoundPlay(AudioPlayerControlSound.ReloadStart));
        }
    }

    void SoundPlayerEndReloading(GameObject Player, int id)
    {
        if (id == _PlayerID)
        {
            StartCoroutine(SoundPlay(AudioPlayerControlSound.ReloadEnd));
        }
    }

    void SoundPlayerStartDraw(GameObject Player, int id)
    {
        if (id == _PlayerID)
        {
            StartCoroutine(SoundPlay(AudioPlayerControlSound.Draw));
        }
    }

    IEnumerator SoundPlay(AudioPlayerControlSound controlSound)
    {
        if (_Player)
        {
            FeetClips = _Player.FeetClips;
            DeathClips = _Player.DeathClips;
            PainClips = _Player.PainClips;
            InfectionClips = _Player.InfectionClips;
            FireClip = _Player.FireClip;
            DrawClip = _Player.DrawClip;
            ReloadClips = _Player.ReloadClips;
        }

        switch (controlSound)
        {
            case AudioPlayerControlSound.Spawn:
            {
                if (Event != null && Event.Length > 0)
                {
                    PlayerAudio.clip = Event[Random.Range(0, Event.Length)];
                    PlayerAudio.Play();
                }
                break;
            }
            case AudioPlayerControlSound.Death:
            {
                if (DeathClips != null && DeathClips.Length > 0)
                {
                    PlayerAudio.clip = DeathClips[Random.Range(0, DeathClips.Length)];
                    PlayerAudio.Play();
                }
                break;
            }
            case AudioPlayerControlSound.Pain:
            {
                if (PainClips != null && PainClips.Length > 0)
                {
                    PlayerAudio.clip = PainClips[Random.Range(0, PainClips.Length)];
                    PlayerAudio.Play();
                }
                break;
            }
            case AudioPlayerControlSound.Infection:
            {
                if (InfectionClips != null && InfectionClips.Length > 0)
                {
                    PlayerAudio.clip = InfectionClips[Random.Range(0, InfectionClips.Length)];
                    PlayerAudio.Play();
                }
                break;
            }
            case AudioPlayerControlSound.Fire:
            {
                if (FireClip)
                {
                    WeaponAudio.clip = FireClip;
                    WeaponAudio.Play();
                }
                break;
            }
            case AudioPlayerControlSound.ReloadStart:
            {
                if (ReloadClips != null && ReloadClips.Length > 0)
                {
                    WeaponAudio.clip = ReloadClips[0];
                    WeaponAudio.Play();
                }
                break;
            }
            case AudioPlayerControlSound.ReloadEnd:
            {
                if (ReloadClips != null && ReloadClips.Length > 0)
                {
                    WeaponAudio.clip = ReloadClips[1];
                    WeaponAudio.Play();
                }
                break;
            }
            case AudioPlayerControlSound.Draw:
            {
                if(DrawClip)
                {
                    WeaponAudio.clip = DrawClip;
                    WeaponAudio.Play();
                }
                break;
            }
        }
        yield return null;
    }

    IEnumerator SoundPlayKnife(HitKnife Hit)
    {
        yield return new WaitForSeconds(0.1f);

        if (_Player)
        {
            KnifeMissClips = _Player.KnifeMissClips;
            KnifeStrikeClips = _Player.KnifeStrikeClips;
        }

        if(Hit.IsHit)
        {
            if (KnifeStrikeClips != null && KnifeStrikeClips.Length > 0)
            {
                WeaponAudio.clip = KnifeStrikeClips[Random.Range(0, KnifeStrikeClips.Length)];
                WeaponAudio.Play();
            }
        }
        else
        {
            if (KnifeMissClips != null && KnifeMissClips.Length > 0)
            {
                WeaponAudio.clip = KnifeMissClips[Random.Range(0, KnifeMissClips.Length)];
                WeaponAudio.Play();
            }
        }
        yield return null;
    }
}

﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSignal : NetworkBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private AudioSource AudioEvent;
    [SerializeField] private AudioSource AudioBackground;

    [SerializeField] private AudioClip[] BlackGroundMusicClips;
    [SerializeField] private AudioClip[] BioStartClips;
    [SerializeField] private AudioClip[] HumanWinClips;
    [SerializeField] private AudioClip[] ZombieWinClips;

    void Start()
    {
        Initialization();
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        Initialization();
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        Initialization();
    }

    void Initialization()
    {
        AudioEvent.Stop();
        AudioBackground.Stop();

        if (AudioEvent && AudioEvent.volume != SpecificalOptions.specificalOptions.AudioEffectVolume)
            AudioEvent.volume = SpecificalOptions.specificalOptions.AudioEffectVolume;

        if (AudioBackground && AudioBackground.volume != SpecificalOptions.specificalOptions.AudioMusicVolume)
            AudioBackground.volume = SpecificalOptions.specificalOptions.AudioMusicVolume;
    }

    void OnEnable()
    {
        GameControl.EventRoundStart += RoundStart;
        GameControl.EventRoundEnd += RoundEnd;
        Biohazard.EventBiohazardStart += MusicStart;
        Biohazard.EventHumanWin += HumanWin;
        Biohazard.EventZombieWin += ZombieWin;
    }

    void OnDisable()
    {
        GameControl.EventRoundStart -= RoundStart;
        GameControl.EventRoundEnd -= RoundEnd;
        Biohazard.EventBiohazardStart -= MusicStart;
        Biohazard.EventHumanWin -= HumanWin;
        Biohazard.EventZombieWin -= ZombieWin;
    }

    void Update()
    {
        if (isServerOnly)
            return;

        if (AudioEvent && AudioEvent.volume != SpecificalOptions.specificalOptions.AudioEffectVolume)
            AudioEvent.volume = SpecificalOptions.specificalOptions.AudioEffectVolume;

        if (GameControl.IsGame)
        {
            if (AudioBackground && AudioBackground.volume != SpecificalOptions.specificalOptions.AudioMusicVolume)
                AudioBackground.volume = SpecificalOptions.specificalOptions.AudioMusicVolume;
        }

        if (AudioBackground && !AudioBackground.isPlaying && Biohazard.IsModBiohazardStart)
            MusicStart();
    }

    void RoundStart()
    {
        if (isServerOnly)
            return;

        if (BioStartClips.Length > 0)
        {
            AudioEvent.clip = BioStartClips[Random.Range(0, BioStartClips.Length)];
            AudioEvent.Play();
        }
    }

    void RoundEnd()
    {
        if (isServerOnly)
            return;

        StartCoroutine(Fade(AudioBackground, 3f, 0.0f));
    }

    void MusicStart()
    {
        if (isServerOnly)
            return;

        if (BlackGroundMusicClips.Length > 0)
        {
            AudioBackground.volume = 0.0f;
            AudioBackground.clip = BlackGroundMusicClips[Random.Range(0, BlackGroundMusicClips.Length)];
            AudioBackground.Play();

            float temp = SpecificalOptions.specificalOptions.AudioMusicVolume;

            StartCoroutine(Fade(AudioBackground, 3f, temp));
        }
    }

    void HumanWin()
    {
        if (isServerOnly)
            return;

        if (HumanWinClips.Length > 0)
        {
            AudioEvent.clip = HumanWinClips[Random.Range(0, HumanWinClips.Length)];
            AudioEvent.Play();
        }
    }

    void ZombieWin()
    {
        if (isServerOnly)
            return;

        if (ZombieWinClips.Length > 0)
        {
            AudioEvent.clip = ZombieWinClips[Random.Range(0, ZombieWinClips.Length)];
            AudioEvent.Play();
        }
    }

    public static IEnumerator Fade(AudioSource audioSource, float duration, float targetVolume)
    {
        float currentTime = 0;
        float start = audioSource.volume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
            yield return null;
        }

        if (audioSource.volume == 0.0f)
            audioSource.Stop();

        yield break;
    }
}

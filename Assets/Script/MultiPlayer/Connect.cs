﻿using kcp2k;
using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connect : MonoBehaviour
{
    public enum BuildType
    {
        LOCAL_CLIENT,
        REMOTE_CLIENT,
        SERVER,
        none
    }

    public BuildType buildType;

    public string ipAddress = "";
    public ushort port = 0;

    public Net NetManager;
    public KcpTransport KcpTransport;

    [SerializeField] private LoadingMenu Loading;

    // Start is called before the first frame update
    void Start()
    {
        KcpTransport = NetManager.GetComponent<KcpTransport>();

        if (buildType == BuildType.SERVER)
            StartRemoteServer();
        else if (buildType == BuildType.REMOTE_CLIENT || buildType == BuildType.LOCAL_CLIENT)
            OnLoginUserButtonClick();
    }

    public void OnLoginUserButtonClick()
    {
        if (buildType == BuildType.REMOTE_CLIENT)
        {
            NetManager.networkAddress = ipAddress;
            KcpTransport.Port = port;

            NetManager.StartClient();

            Loading.LoadingWindowUp(null);
        }
        else if (buildType == BuildType.LOCAL_CLIENT)
        {
            if (!NetworkServer.active)
                NetManager.StartClient();
        }
    }

    private void StartRemoteServer()
    {
        NetManager.StartServer();
    }

    public void LoginRemoteUser()
    {
        Loading.LoadingWindowUp("Подключение");
        Debug.Log("[ClientStartUp].LoginRemoteUser");

        //if (Net.PlayerInfo == null)
        //    Net.PlayerInfo = DataSaver.loadData<PlayerInfo>("ProfilePlayer");

        //Debug.Log(Net.PlayerInfo.Email);
        //Debug.Log(Encrypt(System.Text.Encoding.UTF8.GetString(Net.PlayerInfo.Password)));

        //We need to login a user to get at PlayFab API's. 
        //var requst = new LoginWithEmailAddressRequest()
        //{
        //    Email = System.Text.Encoding.UTF8.GetString(Net.PlayerInfo.Email),
        //    Password = Encrypt(System.Text.Encoding.UTF8.GetString(Net.PlayerInfo.Password))
        //};

        //PlayFabClientAPI.LoginWithEmailAddress(requst, OnPlayFabLoginSuccess, OnLoginError);
    }

    string Encrypt(string pass)
    {
        System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] bs = System.Text.Encoding.UTF8.GetBytes(pass);
        bs = x.ComputeHash(bs);
        System.Text.StringBuilder s = new System.Text.StringBuilder();

        foreach (byte b in bs)
            s.Append(b.ToString("x2").ToLower());

        return s.ToString();
    }
}

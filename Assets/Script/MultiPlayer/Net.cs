﻿using kcp2k;
using LightReflectiveMirror;
using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Net : NetworkManager
{
    [SerializeField] private GameObject SpecialOptionPrefab;
    [SerializeField] private SpatialHashingInterestManagement spatialHashing;

    private GameObject GameController;

    public override void Start()
    {
        base.Start();

        if (SpecificalOptions.specificalOptions == null)
            Instantiate(SpecialOptionPrefab);

        GameScene();
    }

    public void GameScene()
    {
        if (SceneManager.GetActiveScene().path == SpecificalOptions.specificalOptions.Game)
        {
            if (spatialHashing == null)
            {
                spatialHashing = gameObject.AddComponent<SpatialHashingInterestManagement>();

                spatialHashing.visRange = 12;
                spatialHashing.rebuildInterval = 0.1f;
                spatialHashing.checkMethod = SpatialHashingInterestManagement.CheckMethod.XY_FOR_2D;
            }

            if (GameOptions.gameOptions.GameUI)
                GameOptions.gameOptions.GameUI.SetActive(false);

            if (GameOptions.gameOptions.MenuUI)
                GameOptions.gameOptions.MenuUI.SetActive(true);

            GameController = GameObject.FindGameObjectWithTag("Controller");

            if (SpecificalOptions.specificalOptions.IsOnlyClient)
            {
                Debug.Log("OnlyClient: GameScene->CheckConnectToServer");
                StartCoroutine(CheckConnectToServer());
            }
        }
    }

    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        GameObject player = Instantiate(playerPrefab);
        NetworkServer.AddPlayerForConnection(conn, player);

        StartCoroutine(RespawnPlayer(player));
    }

    public override void OnClientConnect()
    {
        base.OnClientConnect();

        spatialHashing.enabled = true;
        StartCoroutine(Synchronization());
    }

    public override void OnClientDisconnect()
    {
        base.OnClientDisconnect();

        if (!SpecificalOptions.specificalOptions.IsChangeScene && SceneManager.GetActiveScene().path == SpecificalOptions.specificalOptions.Game)
        {
            ErrorUI("¯\\_( ͡° ͜ʖ ͡°)_/¯");
        }

        spatialHashing.enabled = false;
    }

    public override void OnClientError(Exception exception)
    {
        base.OnClientError(exception);

        if (SceneManager.GetActiveScene().path == SpecificalOptions.specificalOptions.Game)
        {
            ErrorUI(exception.Message);
        }

        spatialHashing.enabled = false;
    }

    void ErrorUI(string error)
    {
        GameOptions.gameOptions.Loading.LoadingWindowClear();

        if(GameOptions.gameOptions.MenuUI && !GameOptions.gameOptions.MenuUI.activeInHierarchy)
        {
            GameOptions.gameOptions.MenuUI.SetActive(true);
            GameOptions.gameOptions.GameUI.SetActive(false);
        }

        GameOptions.gameOptions.Error.ErrorWindowUp($"Причина: {error}");

        GameOptions.gameOptions.Error.BtnContinuous.onClick.AddListener(ClickReConnectToServer);
        GameOptions.gameOptions.Error.BtnContinuousText.text = "Повторить";

        //GameOptions.gameOptions.Error.BtnExit.onClick.AddListener(ClickBackToMainMenu);
        GameOptions.gameOptions.Error.BtnExitText.text = "Главное меню";
    }

    //public override void OnStartServer()
    //{
    //    base.OnStartServer();

    //    NetworkServer.RegisterHandler(NetworkConnection, OnCreateCharacter);
    //}

    public override void OnStartServer()
    {
        base.OnStartServer();

        Invoke(nameof(ResetPlayerObjectForServer), 0.1f);
    }

    //void OnCreateCharacter(NetworkConnection conn)
    //{
    //    GameObject gameobject = Instantiate(playerPrefab);
    //    NetworkServer.AddPlayerForConnection(conn, gameobject);
    //}

    public override void OnServerDisconnect(NetworkConnectionToClient conn)
    {
        base.OnServerDisconnect(conn);
        NetworkServer.DestroyPlayerForConnection(conn);
    }

    public override void OnStopServer()
    {
        base.OnStopServer();
        NetworkServer.DisconnectAll();
        spatialHashing.enabled = false;
    }

    void ResetPlayerObjectForServer()
    {
        if (!NetworkServer.localClientActive)
        {
            if (GameOptions.gameOptions.Camera)
                GameOptions.gameOptions.Camera.SetActive(false);

            GetComponent<AudioListener>().enabled = true;

            GameOptions.gameOptions.MenuUI.SetActive(false);
            GameOptions.gameOptions.GameUI.SetActive(true);
        }

        spatialHashing.enabled = false;
        spatialHashing.enabled = true;
    }

    void ClickReConnectToServer()
    {
        if (SpecificalOptions.specificalOptions._LightTransport == null)
            SpecificalOptions.specificalOptions._LightTransport = (LightReflectiveMirrorTransport)Transport.activeTransport;

        singleton.networkAddress = SpecificalOptions.specificalOptions.ServerOptions.ServerID;
        singleton.StartClient();

        GameOptions.gameOptions.Error.ErrorWindowClear();
        StartCoroutine(CheckConnectToServer());
    }

    //void ClickBackToMainMenu()
    //{
    //    GameOptions.gameOptions.Dialog.EscToMainMenu();
    //}

    IEnumerator CheckConnectToServer()
    {
        GameOptions.gameOptions.Loading.LoadingWindowUp("Подключение");

        int CountTimeOut = 0;

        while (!NetworkClient.isConnecting && CountTimeOut < 5)
        {
            yield return new WaitForSeconds(2f);
            CountTimeOut++;
        }

        if (CountTimeOut >= 5)
        {
            GameOptions.gameOptions.Error.ErrorWindowUp("Причина: невозможно подключиться к серверу");

            GameOptions.gameOptions.Error.BtnContinuous.onClick.AddListener(ClickReConnectToServer);
            GameOptions.gameOptions.Error.BtnContinuousText.text = "Повторить";

            //GameOptions.gameOptions.Error.BtnExit.onClick.AddListener(ClickBackToMainMenu);
            GameOptions.gameOptions.Error.BtnExitText.text = "Главное меню";
            yield break;
        }

        yield return null;
    }

    IEnumerator Synchronization()
    {
        spatialHashing.enabled = false;

        yield return new WaitForSeconds(1f);

        spatialHashing.enabled = true;

        GameOptions.gameOptions.Error.ErrorWindowClear();
        GameOptions.gameOptions.Loading.LoadingWindowUp("Синхронизация");

        GameController = GameObject.FindGameObjectWithTag("Controller");

        GameController.GetComponent<Biohazard>().SwitchSettingsMode();

        GameObject[] Spawns = GameObject.FindGameObjectsWithTag("Spawn");

        yield return new WaitForSeconds(1f);

        GameController.GetComponent<GameControl>().UpdateWeapon();

        if (GameControl.Players != null)
        {
            for (int i = 0; i < GameControl.Players.Length; i++)
            {
                if (GameControl.Players[i].GetComponent<BotControl>())
                    GameControl.Players[i].transform.parent = GameOptions.gameOptions.ParentBot.transform;
                else
                    GameControl.Players[i].transform.parent = GameOptions.gameOptions.ParentPlayer.transform;

                if (GameControl.Players[i].GetComponent<Player>().isLocalPlayer)
                    GameControl.Players[i].transform.position = Spawns[UnityEngine.Random.Range(0, Spawns.Length)].transform.position;

                GameController.GetComponent<GameControl>().WeaponUpdate(GameControl.Players[i]);
                GameController.GetComponent<Biohazard>().UpdatePlayerProperties(GameControl.Players[i]);
            }
        }

        GameOptions.gameOptions.MenuUI.SetActive(false);
        GameOptions.gameOptions.GameUI.SetActive(true);

        yield return null;
    }

    IEnumerator RespawnPlayer(GameObject Player)
    {
        yield return new WaitForSeconds(5f);

        GameController.GetComponent<GameControl>().ConnectedRespawn(Player);

        yield return null;
    }
}

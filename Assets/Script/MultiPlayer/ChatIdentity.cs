﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatIdentity : NetworkBehaviour
{
    // Start is called before the first frame update
    public Chat Chat;

    void Start()
    {
        Chat = GameObject.FindWithTag("Chat").GetComponent<Chat>();
    }

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();

        if(!Chat)
            Chat = GameObject.FindWithTag("Chat").GetComponent<Chat>();

        Chat.CurrentPlayer = GetComponent<Player>();
        Chat.ChatIdentity = this;
        Chat.Go();
    }

    public void Say(string message)
    {
        if (isServer)
        {
            Chat.RpcSend(message);
            Send(message);
        }
        else
        {
            CmdSend(message);
        }
    }

    [Command]
    public void CmdSend(string message)
    {
        if (!isServer)
            return;

        Chat.RpcSend(message);
        Send(message);
    }

    public void NewGlobalMessage(string msg, Chat.Sender sender)
    {
        switch (sender)
        {
            case Chat.Sender.Server:
            {
                if (!isServer)
                    return;

                Chat.RpcSend(msg);
                Send(msg);
                break;
            }
            case Chat.Sender.Client:
            {
                if (isServer)
                    return;

                CmdSend(msg);
                break;
            }
        }
    }

    public void Send(string message)
    {
        if (!Chat)
            return;

        Message Mss = new Message()
        {
            Length = message.Length,
            Mss = message
        };

        if (Chat.Messages.Count <= 0)
        {
            Chat.Messages.Add(Mss);
            Chat.ChatText.text = message;
        }
        else
        {
            Chat.Messages.Add(Mss);

            int Summ = 0;

            for (int i = 0; i < Chat.Messages.Count; i++)
                Summ += Chat.Messages[i].Length;

            if (Summ >= Chat.LimitChatSymbol)
            {
                while (Summ >= Chat.LimitChatSymbol && Chat.Messages.Count > 0)
                {
                    Chat.Messages.RemoveAt(0);
                    Chat.ChatText.text = "";

                    Summ = 0;
                    for (int i = 0; i < Chat.Messages.Count; i++)
                    {
                        Summ += Chat.Messages[i].Length;

                        if (System.String.IsNullOrEmpty(Chat.ChatText.text))
                            Chat.ChatText.text = Chat.Messages[i].Mss;
                        else
                            Chat.ChatText.text += System.Environment.NewLine + Chat.Messages[i].Mss;
                    }
                }
            }
            else
                Chat.ChatText.text += System.Environment.NewLine + message;

            if (Chat.ChatText.textInfo.lineCount >= Chat.LimitLine)
            {
                Chat.Messages.RemoveAt(0);

                Chat.ChatText.text = "";

                for (int i = 0; i < Chat.Messages.Count; i++)
                {
                    if (System.String.IsNullOrEmpty(Chat.ChatText.text))
                        Chat.ChatText.text = Chat.Messages[i].Mss;
                    else
                        Chat.ChatText.text += System.Environment.NewLine + Chat.Messages[i].Mss;
                }
            }
        }
    }
}

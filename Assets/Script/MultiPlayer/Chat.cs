﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Message
{
    public int Length = 0;
    public string Mss;
}

public class Chat : NetworkBehaviour
{
    public static bool IsChatActive = false;

    public enum Sender
    {
        Server,
        Client
    }

    public ChatIdentity ChatIdentity;
    public Player CurrentPlayer;

    private string NickNamePlayer = "";
    public TextMeshProUGUI ChatText;
    
    public TMP_InputField ChatInput;
    public Button ChatButton;

    public List<Message> Messages = new List<Message>();

    public int LimitChatSymbol = 270;
    public int LimitLine = 10;

    // Start is called before the first frame update
    void Start()
    {
        IsChatActive = false;
    }

    public void Go()
    {
        ChatText.text = null;
        Messages = new List<Message>();

        if (SpecificalOptions.specificalOptions.PlayerInfo != null)
        {
            NickNamePlayer = SpecificalOptions.specificalOptions.PlayerInfo.NickName;

            if (!System.String.IsNullOrEmpty(NickNamePlayer))
            {
                CurrentPlayer.Nick = NickNamePlayer;

                Invoke(nameof(PlayerConnected), 0.1f);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!PauseMenu.IsPauseMenu)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && ChatInput.gameObject.activeInHierarchy)
                Invoke(nameof(SelectButtonOnChat), 0.1f);
            else if (Input.GetKeyDown(KeyCode.Y) && !IsChatActive)
                SelectButtonOnChat();
            else if (Input.GetKeyDown(KeyCode.Return) && IsChatActive)
                SelectButtonOnChat();
        }
    }

    public void SelectButtonOnChat()
    {
        if (SpecificalOptions.specificalOptions.PlayerInfo == null)
            return;

        if (!ChatInput.gameObject.activeInHierarchy)
        {
            ChatInput.gameObject.SetActive(true);
            ChatButton.gameObject.SetActive(true);

            ChatInput.text = "";
            ChatInput.OnSelect(null);
            IsChatActive = true;
        }
        else
        {
            if (!System.String.IsNullOrEmpty(ChatInput.text))
            {
                switch(CurrentPlayer.PlayerTeam)
                {
                    case Player.Team.Zombie:
                    {
                        ChatIdentity.Say($"<color=#c12e32>{NickNamePlayer}</color>:{ChatInput.text}");
                        break;
                    }
                    case Player.Team.Human:
                    {
                        ChatIdentity.Say($"<color=#3a66c7>{NickNamePlayer}</color>:{ChatInput.text}");
                        break;
                    }
                    case Player.Team.Spectator:
                    {
                        ChatIdentity.Say($"<color=#bebebe>{NickNamePlayer}</color>:{ChatInput.text}");
                        break;
                    }
                }
            }

            ChatInput.gameObject.SetActive(false);
            ChatButton.gameObject.SetActive(false);

            IsChatActive = false;
        }
    }

    void PlayerConnected()
    {
        if(ChatIdentity)
            ChatIdentity.NewGlobalMessage($"Подключился игрок с ником <color=#84d733>{NickNamePlayer}</color>", Sender.Client);
    }

    [ClientRpc]
    public void RpcSend(string message)
    {
        if (isServer)
            return;

        ChatIdentity.Send(message);
    }
}

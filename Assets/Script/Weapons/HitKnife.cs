﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitKnife : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Damage Damage;
    [SerializeField] private Player _player;

    [SerializeField] private Collider2D ColliderZoneHitKnife;

    public bool IsHit = false;
    void Start()
    {
        GameObject Controller = GameObject.FindGameObjectWithTag("Controller");

        Damage = Controller.GetComponent<Damage>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!ColliderZoneHitKnife.enabled)
            IsHit = false;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (_player.isServer && col.GetComponent<Player>() && _player.PlayerTeam == Player.Team.Zombie)
        {
            if(Damage)
                Damage.TakeDamege(col.gameObject, gameObject.transform.parent.gameObject, Random.Range(_player.Weapons[0].MinDmg, _player.Weapons[0].MaxDmg));
        }
        IsHit = true;
    }
}

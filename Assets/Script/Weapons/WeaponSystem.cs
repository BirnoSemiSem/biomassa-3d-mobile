﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSystem : NetworkBehaviour
{
    // Start is called before the first frame update
    public enum TypeWeapon
    {
        MachineGun,
        PistolGun,
        Grenade,
        Knife
    };

    public TypeWeapon Weapon;

    public Vector2 PositionSpr;

    public string NameWeapon;

    public int MaxClip;
    public int Clip;
    public int MaxAmmo;
    public int Ammo;

    public float MaxRecoil;

    public float FireSpeed;
    public float SpeedBullet;
    public float ReloadTime;

    public int MinDmg;
    public int MaxDmg;
    public int HSDmg;

    public float Knockback;

    public Sprite WeaponSpr;

    public AudioClip FireClip;
    public AudioClip DrawClip;
    public AudioClip[] ReloadClips;

    public AudioClip[] KnifeMissClips;
    public AudioClip[] KnifeStrikeClips;
}

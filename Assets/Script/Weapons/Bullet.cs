﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : NetworkBehaviour
{
    // Start is called before the first frame update
    public Rigidbody2D rb;

    [SerializeField] private GameObject Player;
    [SerializeField] private Player _player;
    [SerializeField] private Player.WeaponPlayer Weapon;

    [SerializeField] private Damage Damage;
    [SerializeField] private KnockBack KnockBack;

    [SerializeField] private SpriteRenderer Sprite;
    [SerializeField] private bool IsOptimization;

    [SerializeField] private bool GetServer = false;
    void Start()
    {
        if(IsOptimization)
            Sprite.color = new Color(255f, 255f, 255f, 0f);
    }

    void OnEnable()
    {
        GameControl.EventRoundEnd += InvisibleBullet;
    }

    void OnDisable()
    {
        GameControl.EventRoundEnd -= InvisibleBullet;
    }

    void OnBecameVisible()
    {
        if (gameObject && IsOptimization)
        {
            Sprite.color = new Color(255f, 255f, 255f, 255f);
        }
    }

    void OnBecameInvisible()
    {
        if (gameObject && IsOptimization)
        {
            Sprite.color = new Color(255f, 255f, 255f, 0f);
        }
    }

    public void Go(GameObject PlayerObject, bool Server)
    {
        GetServer = Server;

        if (GetServer)
        {
            if (!Damage || !KnockBack)
            {
                GameObject Controller = GameObject.FindWithTag("Controller");

                Damage = Controller.GetComponent<Damage>();
                KnockBack = Controller.GetComponent<KnockBack>();
            }
        }

        Player = PlayerObject;
        _player = PlayerObject.GetComponent<Player>();
        Weapon = _player.Weapons[_player.ActiveWeapon];

        rb.velocity = transform.up * Weapon.SpeedBullet;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (Player && Player != col.gameObject && gameObject.CompareTag("Bullet") != col.CompareTag("Bullet"))
        {
            if (col.gameObject.GetComponent<Player>())
            {
                if (_player.PlayerTeam == global::Player.Team.Human && _player.PlayerTeam != col.gameObject.GetComponent<Player>().PlayerTeam)
                {
                    if (GetServer)
                    {
                        int Dmg = 0;
                        if (Random.Range(0, 10) > 7)
                            Dmg = Weapon.HSDmg;
                        else
                            Dmg = Random.Range(Weapon.MinDmg, Weapon.MaxDmg);

                        Damage.TakeDamege(col.gameObject, Player, Dmg);
                        KnockBack.TakeKnockBack(0.1f, Weapon.Knockback, col.gameObject, Player);
                    }

                    InvisibleBullet();

                    //NetworkServer.Destroy(gameObject);

                    //if (isServer)
                    //    RpcDamage(col.gameObject, Player);
                    //else
                    //{
                    //    int Dmg = Random.Range(Weapon.MinDmg, Weapon.MaxDmg);
                    //    CmdDamage(col.gameObject, Player, Dmg);

                    //    Damage.RpcTakeDamege(col.gameObject, Player, Dmg);
                    //    KnockBack.TakeKnockBack(0.3f, 5f, col.gameObject, Player);
                    //}
                }
            }
            else if (!col.CompareTag("ClearGlass"))
            {
                InvisibleBullet();

                //NetworkServer.Destroy(gameObject);
            }
        }
    }

    void InvisibleBullet()
    {
        if (gameObject)
            gameObject.SetActive(false);

        Player = null;
        _player = null;
        Weapon = new Player.WeaponPlayer();

        GetServer = false;

        rb.velocity = Vector2.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeInHierarchy && (transform.position.x > 50 || transform.position.x < -50 || transform.position.y > 50 || transform.position.y < -50 || rb.velocity == Vector2.zero))
        {
            InvisibleBullet();

            //NetworkServer.Destroy(gameObject);
        }
    }
}

﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class WeaponControl : NetworkBehaviour
{
    // Start is called before the first frame update
    public Player PlayerControl;

    public ProgressBarPlayer ProgressBar;

    public Transform FirePoint;
    [SerializeField] private CapsuleCollider2D HitColiderKnifeAttack;
    [SerializeField] private HitKnife PlayerHitKnife;

    public GameObject Bullet;
    public GameObject MuzzleFlash;
    public AnimStartAndDestroyToTime AnimMuzzleFlash;

    public Coroutine CorPrograssFadeLeft;
    public Coroutine CorPrograssFadeRight;

    public Coroutine CorReload;
    public Coroutine CorChangeWeapon;

    [SyncVar] public bool IsFire = false;
    [SyncVar] public bool IsReload = false;
    [SyncVar] public bool IsDraw = false;

    float FireCurrentTime;
    float FireNextTime;

    //Bot
    private BotControl Bot;

    //Delegate
    public UnityAction<GameObject, int> EventPlayerAttackFireWeapon;
    public UnityAction<GameObject, int, HitKnife> EventPlayerAttackFireKnife;
    public UnityAction<GameObject, int> EventPlayerStartReloading;
    public UnityAction<GameObject, int> EventPlayerEndReloading;
    public UnityAction<GameObject, int> EventPlayerStartSoundReloading;
    public UnityAction<GameObject, int> EventPlayerEndSoundReloading;
    public UnityAction<GameObject, int> EventPlayerStartDraw;
    public UnityAction<GameObject, int> EventPlayerEndDraw;

    void Start()
    {
        FireNextTime = FireCurrentTime = 0;

        if (GetComponent<BotControl>())
            Bot = GetComponent<BotControl>();
        else
            Bot = null;
    }

    void OnEnable()
    {
        GameControl.EventSpawnPlayer += Reset;
        Biohazard.EventInfectionPlayer += Reset;
        Damage.EventDeathPlayer += Reset;
    }

    void OnDisable()
    {
        GameControl.EventSpawnPlayer -= Reset;
        Biohazard.EventInfectionPlayer -= Reset;
        Damage.EventDeathPlayer -= Reset;
    }

    //Update is called once per frame
    void Update()
    {
        //if (!isLocalPlayer && !Bot)
        //    return;

        //if (!isServer && Bot)
        //    return;

        //if (!isServer)
        //    return;

        if (PlayerControl.IsAlive)
        {
            if (PlayerControl.PlayerTeam == global::Player.Team.Human)
            {
                if (!GameControl.IsFreeze)
                {
                    FireCurrentTime += Time.deltaTime;

                    if (isServer)
                    {
                        if (IsFire && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip > 0 && FireCurrentTime >= FireNextTime && !IsReload && !IsDraw)
                        {
                            float Recoil = Random.Range(1, 10) > 5 ? (Random.Range(0f, PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxRecoil) * -1) : Random.Range(0f, PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxRecoil);
                            int Clip = PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip - 1;
                            float NextTime = FireCurrentTime + PlayerControl.Weapons[PlayerControl.ActiveWeapon].FireSpeed;

                            Shoot(Recoil);
                            RpcShotFire(Recoil, Clip, NextTime);

                            transform.rotation = Quaternion.Euler(new Vector3(0, 0, transform.eulerAngles.z + Recoil));

                            if (!isServerOnly)
                            {
                                //GameObject _muzzleflash = Instantiate(MuzzleFlash, FirePoint.position, Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil))) as GameObject;
                                //_muzzleflash.transform.SetParent(transform);

                                if (!MuzzleFlash.activeInHierarchy)
                                {
                                    MuzzleFlash.SetActive(true);
                                    AnimMuzzleFlash = MuzzleFlash.GetComponent<AnimStartAndDestroyToTime>();
                                    AnimMuzzleFlash.Go();
                                    MuzzleFlash.transform.position = FirePoint.position;
                                    MuzzleFlash.transform.rotation = Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil));
                                }
                            }

                            FireNextTime = FireCurrentTime + PlayerControl.Weapons[PlayerControl.ActiveWeapon].FireSpeed;
                            PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip--;

                            EventPlayerAttackFireWeapon?.Invoke(gameObject, PlayerControl.GetPlayerID);
                        }
                    }
                    //else
                    //{
                    //    if (IsFire && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip > 0 && FireCurrentTime >= FireNextTime && !IsReload && !IsDraw)
                    //    {
                    //        float Recoil = Random.Range(1, 10) > 5 ? (Random.Range(0f, PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxRecoil) * -1) : Random.Range(0f, PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxRecoil);

                    //        transform.rotation = Quaternion.Euler(new Vector3(0, 0, transform.eulerAngles.z + Recoil));

                    //        GameObject _muzzleflash = Instantiate(MuzzleFlash, FirePoint.position, Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil)));

                    //        _muzzleflash.transform.SetParent(transform);

                    //        FireNextTime = FireCurrentTime + PlayerControl.Weapons[PlayerControl.ActiveWeapon].FireSpeed;
                    //        PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip--;

                    //        EventPlayerAttackFireWeapon?.Invoke(gameObject, PlayerControl.GetPlayerID);
                    //    }
                    //}
                }

                if (isServer && !IsReload && !IsDraw && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip <= 0 && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo > 0)
                {
                    RpcStartReload();
                    CorReload = StartCoroutine(Reload());
                }
            }
            else if (PlayerControl.PlayerTeam == global::Player.Team.Zombie)
            {
                if (IsFire && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Weapon == WeaponSystem.TypeWeapon.Knife && !PlayerControl.Animator.GetBool("IsKnifeAttack") && !HitColiderKnifeAttack.enabled)
                    PlayerControl.Animator.SetBool("IsKnifeAttack", true);
                else if(!IsFire && PlayerControl.Animator.GetBool("IsKnifeAttack"))
                    PlayerControl.Animator.SetBool("IsKnifeAttack", false);
            }
        }
    }

    public void ClickButtonFireEnter()
    {
        if (!isServer && Bot)
            return;

        if (isServer)
        {
            //RpcStartFire();
            IsFire = true;
        }
        else
        {
            CmdFireEnter();
            IsFire = true;
        }
    }

    public void ClickButtonFireExit()
    {
        if (!isServer && Bot)
            return;

        if (isServer)
        {
            //RpcEndFire();
            IsFire = false;
        }
        else
        {
            CmdFireExit();
            IsFire = false;
        }
    }

    public void ClickButtonReloadEnter()
    {
        if (!isServer && Bot)
            return;

        if (!IsReload && !IsDraw && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip < PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxClip && PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo > 0)
        {
            if (isServer)
            {
                RpcStartReload();
                CorReload = StartCoroutine(Reload());
            }
            else
            {
                CmdStartReload();
                CorReload = StartCoroutine(Reload());
            }
        }
    }

    public void ClickChangeWeapon(int WeaponSelect)
    {
        if (PlayerControl.PlayerTeam != global::Player.Team.Human)
            return;

        if (WeaponSelect == PlayerControl.ActiveWeapon)
            return;

        if (isServer)
        {
            IsDraw = false;

            RpcStartChangeWeapon(WeaponSelect);
            PlayerControl.ActiveWeapon = WeaponSelect;

            if (CorChangeWeapon != null)
                StopCoroutine(CorChangeWeapon);

            CorChangeWeapon = StartCoroutine(ChangeWeapon());
        }
        else
        {
            IsDraw = false;

            CmdStartChangeWeapon(WeaponSelect);
            PlayerControl.ActiveWeapon = WeaponSelect;

            if (CorChangeWeapon != null)
                StopCoroutine(CorChangeWeapon);

            CorChangeWeapon = StartCoroutine(ChangeWeapon());
        }
    }

    public void KnifeAttack()
    {
        if (PlayerControl.IsAlive)
        {
            HitColiderKnifeAttack.enabled = !HitColiderKnifeAttack.enabled;
            HitColiderKnifeAttack.isTrigger = !HitColiderKnifeAttack.isTrigger;

            if (!HitColiderKnifeAttack.enabled)
            {
                PlayerControl.Animator.SetBool("IsKnifeAttack", HitColiderKnifeAttack.enabled);
            }
            else
            {
                EventPlayerAttackFireKnife?.Invoke(gameObject, PlayerControl.GetPlayerID, PlayerHitKnife);
            }
        }
    }

    void Shoot(float Recoil)
    {
        GameObject _bullet = OptimizationLogic.OptCode.GetPoolObject(OptimizationLogic.Effects.Bullet);
        _bullet.transform.position = FirePoint.position;
        _bullet.transform.rotation = Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil));
        _bullet.SetActive(true);
        _bullet.GetComponent<Bullet>().Go(gameObject, isServer);
    }

    void Reset(GameObject Player, int id)
    {
        if (id == PlayerControl.GetPlayerID)
        {
            if (ProgressBar && !Bot)
            {
                ProgressBar.gameObject.SetActive(false);
                ProgressBar.LeftBar.value = 0;
                ProgressBar.RightBar.value = 0;

                ProgressBar.TextProgressBar.text = null;

                if (CorPrograssFadeLeft != null)
                {
                    StopCoroutine(CorPrograssFadeLeft);
                    StopCoroutine(CorPrograssFadeRight);
                }
            }

            if(CorReload != null)
                StopCoroutine(CorReload);

            if(CorChangeWeapon != null)
                StopCoroutine(CorChangeWeapon);

            if(AnimMuzzleFlash)
                AnimMuzzleFlash.CancelAnim();

            PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo = PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxAmmo;
            PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip = PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxClip;

            FireNextTime = FireCurrentTime = 0;

            IsReload = false;
            IsFire = false;
            IsDraw = false;

            PlayerControl.Animator.SetBool("IsKnifeAttack", false);
            HitColiderKnifeAttack.enabled = false;
            HitColiderKnifeAttack.isTrigger = false;
        }
    }

    void SwicthWeapon()
    {
        if (!PlayerControl.IsAlive)
            return;

        PlayerControl.FireClip = PlayerControl.Weapons[PlayerControl.ActiveWeapon].FireClip;
        PlayerControl.DrawClip = PlayerControl.Weapons[PlayerControl.ActiveWeapon].DrawClip;
        PlayerControl.ReloadClips = PlayerControl.Weapons[PlayerControl.ActiveWeapon].ReloadClips;

        PlayerControl.KnifeMissClips = PlayerControl.Weapons[PlayerControl.ActiveWeapon].KnifeMissClips;
        PlayerControl.KnifeStrikeClips = PlayerControl.Weapons[PlayerControl.ActiveWeapon].KnifeStrikeClips;

        //PlayerControl.WeaponTransform.localPosition = PlayerControl.Weapons[PlayerControl.ActiveWeapon].PositionSpr;
        //PlayerControl.WeaponSprite.sprite = PlayerControl.Weapons[PlayerControl.ActiveWeapon].WeaponSpr;

        if (PlayerControl.PlayerTeam == global::Player.Team.Human)
        {
            switch (PlayerControl.ActiveWeapon)
            {
                case 0: { if (!System.String.IsNullOrEmpty(PlayerControl.HumanClass.Name)) PlayerControl.Animator.Play(PlayerControl.HumanClass.Name + "-" + global::Player.AnimationPlayer.IdleMachineGun.ToString()); break; }
                case 1: { if (!System.String.IsNullOrEmpty(PlayerControl.HumanClass.Name)) PlayerControl.Animator.Play(PlayerControl.HumanClass.Name + "-" + global::Player.AnimationPlayer.IdlePistolGun.ToString()); break; }
                case 2: { if (!System.String.IsNullOrEmpty(PlayerControl.HumanClass.Name)) PlayerControl.Animator.Play(PlayerControl.HumanClass.Name + "-" + global::Player.AnimationPlayer.IdleGrenade.ToString()); break; }
            }
        }
    }

    public IEnumerator Reload()
    {
        IsReload = true;
        if (ProgressBar && !Bot)
        {
            ProgressBar.gameObject.SetActive(true);
            ProgressBar.LeftBar.value = 0;
            ProgressBar.RightBar.value = 0;

            ProgressBar.TextProgressBar.text = "Перезарядка";

            if (CorPrograssFadeLeft != null)
            {
                StopCoroutine(CorPrograssFadeLeft);
                StopCoroutine(CorPrograssFadeRight);
            }

            CorPrograssFadeLeft = StartCoroutine(PrograssBarFade(ProgressBar.LeftBar, PlayerControl.Weapons[PlayerControl.ActiveWeapon].ReloadTime));
            CorPrograssFadeRight = StartCoroutine(PrograssBarFade(ProgressBar.RightBar, PlayerControl.Weapons[PlayerControl.ActiveWeapon].ReloadTime));
        }
        EventPlayerStartReloading?.Invoke(gameObject, PlayerControl.GetPlayerID);

        yield return new WaitForSeconds(0.2f);
        EventPlayerStartSoundReloading?.Invoke(gameObject, PlayerControl.GetPlayerID);
        yield return new WaitForSeconds(PlayerControl.Weapons[PlayerControl.ActiveWeapon].ReloadTime - 0.5f);
        EventPlayerEndSoundReloading?.Invoke(gameObject, PlayerControl.GetPlayerID);
        yield return new WaitForSeconds(0.3f);

        if (PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo >= PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxClip)
        {
            if (!GameControl.IsInfinityAmmo)
                PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo -= PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxClip - PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip;
            PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip = PlayerControl.Weapons[PlayerControl.ActiveWeapon].MaxClip;
        }
        else
        {
            PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip = PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo;
            if (!GameControl.IsInfinityAmmo)
                PlayerControl.Weapons[PlayerControl.ActiveWeapon].Ammo = 0;
        }

        if (ProgressBar)
            ProgressBar.gameObject.SetActive(false);

        IsReload = false;
        EventPlayerEndReloading?.Invoke(gameObject, PlayerControl.GetPlayerID);
    }

    public static IEnumerator PrograssBarFade(Slider prograss, float duration)
    {
        float currentTime = 0;
        float start = prograss.value;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            prograss.value = Mathf.Lerp(start, 1f, currentTime / duration);
            yield return null;
        }
        yield break;
    }

    public IEnumerator ChangeWeapon()
    {
        if (IsReload)
        {
            if(CorReload != null)
                StopCoroutine(CorReload);
            IsReload = false;
        }

        IsDraw = true;

        if (ProgressBar && !Bot)
        {
            ProgressBar.gameObject.SetActive(true);
            ProgressBar.LeftBar.value = 0;
            ProgressBar.RightBar.value = 0;

            ProgressBar.TextProgressBar.text = "Смена оружия";

            if (CorPrograssFadeLeft != null)
            {
                StopCoroutine(CorPrograssFadeLeft);
                StopCoroutine(CorPrograssFadeRight);
            }

            CorPrograssFadeLeft = StartCoroutine(PrograssBarFade(ProgressBar.LeftBar, 1f));
            CorPrograssFadeRight = StartCoroutine(PrograssBarFade(ProgressBar.RightBar, 1f));
        }

        SwicthWeapon();

        EventPlayerStartDraw?.Invoke(gameObject, PlayerControl.GetPlayerID);

        yield return new WaitForSeconds(1f);

        if (ProgressBar)
            ProgressBar.gameObject.SetActive(false);

        IsDraw = false;
        EventPlayerEndDraw?.Invoke(gameObject, PlayerControl.GetPlayerID);
    }

    #region [ClientRpc]
    //[ClientRpc]
    //void RpcStartFire()
    //{
    //    if (isServer)
    //        return;

    //    IsFire = true;
    //}

    //[ClientRpc]
    //void RpcEndFire()
    //{
    //    if (isServer)
    //        return;

    //    IsFire = false;
    //}

    [ClientRpc]
    void RpcStartReload()
    {
        if (isServer)
            return;

        CorReload = StartCoroutine(Reload());
    }

    [ClientRpc]
    void RpcStartChangeWeapon(int WeaponSelect)
    {
        if (isServer)
            return;

        PlayerControl.ActiveWeapon = WeaponSelect;

        if (CorChangeWeapon != null)
            StopCoroutine(CorChangeWeapon);

        CorChangeWeapon = StartCoroutine(ChangeWeapon());
    }

    [ClientRpc]
    void RpcShotFire(float Recoil, int Clip, float Time)
    {
        if (isServer)
            return;

        PlayerControl.transform.rotation = Quaternion.Euler(new Vector3(0, 0, PlayerControl.transform.eulerAngles.z + Recoil));

        //GameObject _muzzleflash = Instantiate(MuzzleFlash, FirePoint.position, Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil)));
        //_muzzleflash.transform.SetParent(PlayerControl.transform);

        if (!MuzzleFlash.activeInHierarchy)
        {
            MuzzleFlash.SetActive(true);

            AnimMuzzleFlash = MuzzleFlash.GetComponent<AnimStartAndDestroyToTime>();
            AnimMuzzleFlash.Go();

            MuzzleFlash.transform.position = FirePoint.position;
            MuzzleFlash.transform.rotation = Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil));
        }

        FireNextTime = Time;
        PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip = Clip;

        Shoot(Recoil);

        EventPlayerAttackFireWeapon?.Invoke(gameObject, PlayerControl.GetPlayerID);
    }

    //[ClientRpc]
    //void RpcStartKnifeAttack()
    //{
    //    if (isServer)
    //        return;

    //    PlayerControl.Animator.SetBool("IsKnifeAttack", true);
    //}

    //[ClientRpc]
    //void RpcEndKnifeAttack()
    //{
    //    if (isServer)
    //        return;

    //    PlayerControl.Animator.SetBool("IsKnifeAttack", false);
    //}

    //[ClientRpc]
    //void RpcKnifeAttack()
    //{
    //    if (isServer)
    //        return;

    //    if (PlayerControl.IsAlive)
    //    {
    //        HitColiderKnifeAttack.enabled = !HitColiderKnifeAttack.enabled;
    //        HitColiderKnifeAttack.isTrigger = !HitColiderKnifeAttack.isTrigger;

    //        if (!HitColiderKnifeAttack.enabled)
    //        {
    //            PlayerControl.Animator.SetBool("IsKnifeAttack", HitColiderKnifeAttack.enabled);
    //        }
    //        else
    //        {
    //            EventPlayerAttackFireKnife?.Invoke(Player, Player.GetInstanceID(), PlayerHitKnife);
    //        }
    //    }
    //}
    #endregion

    #region [Command]
    [Command]
    void CmdReset(GameObject Player, int id)
    {
        if (!isServer)
            return;

        Reset(Player, id);
    }

    [Command]
    void CmdFireEnter()
    {
        if (!isServer)
            return;

        IsFire = true;
    }

    [Command]
    void CmdFireExit()
    {
        if (!isServer)
            return;

        IsFire = false;
    }

    [Command]
    void CmdStartReload()
    {
        if (!isServer)
            return;

        CorReload = StartCoroutine(Reload());
    }

    [Command]
    void CmdStartChangeWeapon(int WeaponSelect)
    {
        if (!isServer)
            return;

        if (CorChangeWeapon != null)
            StopCoroutine(CorChangeWeapon);

        PlayerControl.ActiveWeapon = WeaponSelect;
        CorChangeWeapon = StartCoroutine(ChangeWeapon());
    }

    [Command]
    void CmdShotFire(float Recoil)
    {
        if (!isServer)
            return;

        transform.rotation = Quaternion.Euler(new Vector3(0, 0, transform.eulerAngles.z + Recoil));

        FireNextTime = FireCurrentTime + PlayerControl.Weapons[PlayerControl.ActiveWeapon].FireSpeed;
        PlayerControl.Weapons[PlayerControl.ActiveWeapon].Clip--;

        EventPlayerAttackFireWeapon?.Invoke(gameObject, PlayerControl.GetPlayerID);
    }

    [Command]
    void CmdStartKnifeAttack()
    {
        if (!isServer)
            return;

        PlayerControl.Animator.SetBool("IsKnifeAttack", true);
    }

    [Command]
    void CmdEndKnifeAttack()
    {
        if (!isServer)
            return;

        PlayerControl.Animator.SetBool("IsKnifeAttack", false);
    }

    [Command]
    void CmdKnifeAttack()
    {
        if (!isServer)
            return;

        if (PlayerControl.IsAlive)
        {
            HitColiderKnifeAttack.GetComponent<CapsuleCollider2D>().enabled = !HitColiderKnifeAttack.GetComponent<CapsuleCollider2D>().enabled;
            HitColiderKnifeAttack.GetComponent<CapsuleCollider2D>().isTrigger = !HitColiderKnifeAttack.GetComponent<CapsuleCollider2D>().isTrigger;

            if (!HitColiderKnifeAttack.GetComponent<CapsuleCollider2D>().enabled)
            {
                PlayerControl.Animator.SetBool("IsKnifeAttack", !HitColiderKnifeAttack.GetComponent<CapsuleCollider2D>().enabled);
            }
            else
            {
                EventPlayerAttackFireKnife?.Invoke(gameObject, PlayerControl.GetPlayerID, HitColiderKnifeAttack.GetComponent<HitKnife>());
            }
        }
    }

    //[Command]
    //void CmdShot(float Recoil)
    //{
    //    if (!isServer)
    //        return;

    //    GameObject _bulet = Instantiate(Bullet, FirePoint.position, Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil)));
    //    GameObject _muzzleflash = Instantiate(MuzzleFlash, FirePoint.position, Quaternion.Euler(new Vector3(0, 0, FirePoint.transform.eulerAngles.z + Recoil)));

    //    NetworkServer.Spawn(_bulet);
    //    NetworkServer.Spawn(_muzzleflash);

    //    _bulet.transform.SetParent(transform);
    //    _muzzleflash.transform.SetParent(transform);

    //    _bulet.GetComponent<Bullet>().Go();
    //}
    #endregion
}

﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtualFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtualFuncInvoker5
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};

// System.Action`2<UnityEngine.Color32,UnityEngine.Color32>
struct Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA;
// System.Action`2<Mirror.NetworkWriter,System.ArraySegment`1<System.Byte>>
struct Action_2_tE7DE89222EAD5CBE5FBA94711CB3C71222202DFC;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Boolean>>
struct Action_2_tFAFB5B1636A32CF85A452A078736260BB496C253;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Byte>>
struct Action_2_t104D6887DC3FA703E6D350257E4EE72A3EFCF3A3;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Char>>
struct Action_2_t1B2365362928779807EDAEE12F27010A7A8D39C6;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Color>>
struct Action_2_t1AB30C8E5E10864F469D7062D99016352CC3602F;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Color32>>
struct Action_2_t284D0A27595992EAC03994E6C1A1E82225441B67;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Decimal>>
struct Action_2_t3913958E2393CE5F44A661B2F0835A6FC4513CBE;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Double>>
struct Action_2_tD872DFCCB454D89FE5878BA1B3A03AA4FF2F7A72;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Guid>>
struct Action_2_t00EE65E013F88779EEEE98835E86BDF400A3BF36;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Int16>>
struct Action_2_tE28F1C5549DD431F6354E325F28C3FA842F94E57;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Int32>>
struct Action_2_tD938883EC204367D5A295A7CB2BB5D4D20A90029;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Int64>>
struct Action_2_tDF176CFB33B35A021E5DA20E9485F6A452F2C345;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Matrix4x4>>
struct Action_2_tB31DAF5F45A869010A4A3D1EA9EA5C56DCFAB947;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Plane>>
struct Action_2_t9BBCAC312FAACBDD9B08E82065F0C66CB92596DD;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Quaternion>>
struct Action_2_tFAE875C9728E772ACF44D3BCF5F2B94860FB1CDE;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Ray>>
struct Action_2_tBE61BBF1961F6628AEE63F058E11FEB22FB6A6ED;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Rect>>
struct Action_2_tD39051A11B2175464C227775F3F6FD44715C0767;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.SByte>>
struct Action_2_tEE379855D79D043D165FD05C3786F7B0ADB89B38;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Single>>
struct Action_2_tD01A9F5ED6BB951B86BE216B2B8DE2C17FFAE551;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.UInt16>>
struct Action_2_tDD6B83C4A6E2ECFC616A05B485C0D7E24A750E90;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.UInt32>>
struct Action_2_tC9DC1064C14B7AF886718CB747591EC132E1E301;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.UInt64>>
struct Action_2_t9E34BCADC1F9EF87E3BC9817BF9ABF3086286388;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector2>>
struct Action_2_t06CF1E09D2E968AE317C1B4066FE59281D0D7F35;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector2Int>>
struct Action_2_tF8B877CE67C7AF23C81D6C5C15EB3D2FE44321A0;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector3>>
struct Action_2_t3D709A9DD3DD60B2669C03CAF9A9607B98F0CC44;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector3Int>>
struct Action_2_tCF4F398AAC858F0B69CF49EA4DB6878F5E85A2FE;
// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector4>>
struct Action_2_t437C51FCE7D4E11D08E66AC6884ADD843A29305E;
// System.Action`2<Mirror.NetworkWriter,System.Byte[]>
struct Action_2_t6CB11000550B779DE6F0A9873A4FA47684E8A996;
// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchInfo[]>
struct Action_2_t77285E9313B73F391F0816DD5738EBF05F8774AD;
// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.PlayerInfo[]>
struct Action_2_t5199EDEE1233BFABE45CDA21E9230C8346645716;
// System.Action`2<Mirror.NetworkWriter,Mirror.AddPlayerMessage>
struct Action_2_t98800A090710915B3E861E237B1A9B6E9F29163F;
// System.Action`2<Mirror.NetworkWriter,System.Boolean>
struct Action_2_t771EB50B64E2F517312250DEC5230DC62C7E36FD;
// System.Action`2<Mirror.NetworkWriter,System.Byte>
struct Action_2_tDDEAD4B308FD0F5A9299458AF5A6B47CA5D3732F;
// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.CellValue>
struct Action_2_t117A07FAD93D252BC5E69F05373B4E286F1F8D56;
// System.Action`2<Mirror.NetworkWriter,Mirror.ChangeOwnerMessage>
struct Action_2_tA6272A2BBF53770CE719C794145D53E55F0DCC51;
// System.Action`2<Mirror.NetworkWriter,System.Char>
struct Action_2_t3C170C2BFCCAA591C6DA755A86E3FEDBB4428D90;
// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ClientMatchMessage>
struct Action_2_tCFCEE592405B35148F5E1AC506FEB4F8CB2F49B6;
// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ClientMatchOperation>
struct Action_2_tE631CB825D55269F80774B2F55C5E62192891CF8;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Color>
struct Action_2_t993E6168F76ADC356CC943B6992CDD55115F3057;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Color32>
struct Action_2_t27076E67A79536591E04C63FD1582DB7D564BA08;
// System.Action`2<Mirror.NetworkWriter,Mirror.CommandMessage>
struct Action_2_t8863334817D651A8E3B8E434C682CA5A76CE9498;
// System.Action`2<Mirror.NetworkWriter,System.Decimal>
struct Action_2_tC4556EDF997707D306A86E36FC8EE3751F9C687C;
// System.Action`2<Mirror.NetworkWriter,System.Double>
struct Action_2_tA70CFBAAF2BDFBA2BEE4003F4C2E6E5B12924D8E;
// System.Action`2<Mirror.NetworkWriter,Mirror.EntityStateMessage>
struct Action_2_t92D149BA0BB9E6C54B7036167F4A14DFF1B5BE08;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.GameObject>
struct Action_2_t7A7E74A9B38640A94D5CC807EC76F706FB6693C0;
// System.Action`2<Mirror.NetworkWriter,System.Guid>
struct Action_2_tCB8F678502AD5BFA52EE6F149351DAC3037677AF;
// System.Action`2<Mirror.NetworkWriter,System.Int16>
struct Action_2_tED2F1B32CB312EA2AFE3FF65E3EBD21ADF00A34C;
// System.Action`2<Mirror.NetworkWriter,System.Int32>
struct Action_2_t4D3AA2594C46CEF975DCD32BFF9A0FEE380568B3;
// System.Action`2<Mirror.NetworkWriter,System.Int64>
struct Action_2_tE79E56090404F1ED684677C80F34095664D0010D;
// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchInfo>
struct Action_2_t1D8E4389A2FC4D611519FC7C996BF96F763E5017;
// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchPlayerData>
struct Action_2_tB478A835A6DC624AEA7C01C383390CE10F4D2BF1;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Matrix4x4>
struct Action_2_tD12F3221DBF89BF11C6CB2855E9BDBD70D6A8AB9;
// System.Action`2<Mirror.NetworkWriter,Mirror.NetworkBehaviour>
struct Action_2_tC5312D54454D5411CB706A055837C90CBD0FEC12;
// System.Action`2<Mirror.NetworkWriter,Mirror.NetworkIdentity>
struct Action_2_t51E55DFCEC17AF21DB946B2527D3669CD8E09542;
// System.Action`2<Mirror.NetworkWriter,Mirror.NetworkPingMessage>
struct Action_2_t427A3AAB52F4338A54E05C0F9CCE238604B75463;
// System.Action`2<Mirror.NetworkWriter,Mirror.NetworkPongMessage>
struct Action_2_t19A08A9BB7D482718CD3474975A6559CA159FA9F;
// System.Action`2<Mirror.NetworkWriter,Mirror.NotReadyMessage>
struct Action_2_tE1C508C7DB184AC769DA763897EA5CE208D96BE9;
// System.Action`2<Mirror.NetworkWriter,Mirror.ObjectDestroyMessage>
struct Action_2_t4125FEC2DD5C797C8E273617CF2C3E2ABB98FA45;
// System.Action`2<Mirror.NetworkWriter,Mirror.ObjectHideMessage>
struct Action_2_tB5D2418D8D5BB1CA27E37232842C681B92ABDB9B;
// System.Action`2<Mirror.NetworkWriter,Mirror.ObjectSpawnFinishedMessage>
struct Action_2_t88B940D5126F22A0E314062C9CB9B8CF14430308;
// System.Action`2<Mirror.NetworkWriter,Mirror.ObjectSpawnStartedMessage>
struct Action_2_t932E36A723FFA4E592D80F6F8AE785A34669DA75;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Plane>
struct Action_2_tF808E86BBEE3D1817CE8A0C2E299320C9DD9D1BA;
// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.PlayerInfo>
struct Action_2_t15EAEA1713B2DE57B0F7BFED86F3CF5915138314;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Quaternion>
struct Action_2_tD28B743D21DCE5F5BE026041A86D0F58543D86C4;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Ray>
struct Action_2_t0726BDF29C00FDD4DA0F278B3AFC4E47F69A4CED;
// System.Action`2<Mirror.NetworkWriter,Mirror.ReadyMessage>
struct Action_2_t4CDBF06B555A45F0D103E46E4114CEEEFA34B691;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Rect>
struct Action_2_t776DBFE440EDCE827698E6B849C13676E750F733;
// System.Action`2<Mirror.NetworkWriter,Mirror.RpcMessage>
struct Action_2_tAF32C09EC861FEA50FE0977ED9C0DAD82BA3D637;
// System.Action`2<Mirror.NetworkWriter,System.SByte>
struct Action_2_tEE3DE27FC7A1D873CE799E7124798B9A979DE913;
// System.Action`2<Mirror.NetworkWriter,Mirror.SceneMessage>
struct Action_2_t4A43C94439BC4A524A4463B987E9326B340BB5D7;
// System.Action`2<Mirror.NetworkWriter,Mirror.SceneOperation>
struct Action_2_t102F2BE697BDA0818FA414975B57C948D9C2BBDF;
// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ServerMatchMessage>
struct Action_2_tAF93D04C699FAC16A658F8DA78DD252A93E952E8;
// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ServerMatchOperation>
struct Action_2_tEF11BC50B4B015730C0409DC35EAC7D6E03CEE11;
// System.Action`2<Mirror.NetworkWriter,System.Single>
struct Action_2_tE45BEC1FBF44C71FEAD78DC6323066FD51D7FE55;
// System.Action`2<Mirror.NetworkWriter,Mirror.SpawnMessage>
struct Action_2_t8CF38B31F52C4EF713F3D2BAC6B806B321ADC0A1;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Sprite>
struct Action_2_tA527CAC169A61C3874E9DEC5127904806AF2463C;
// System.Action`2<Mirror.NetworkWriter,System.String>
struct Action_2_t63FAAE13C5F96B474021BB5448158556C8EBB594;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Texture2D>
struct Action_2_tA50DCC93E783D93FEFF521E5255E78C184ED8E02;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Transform>
struct Action_2_tD87BC92995377D813FAEA7DFF07C8F114451FAAA;
// System.Action`2<Mirror.NetworkWriter,System.UInt16>
struct Action_2_tDC9B80B3DDEFFE6AD670786AA0C0A41F09DFE02D;
// System.Action`2<Mirror.NetworkWriter,System.UInt32>
struct Action_2_t0FDD9213EEB281B92425DB7083B3E28D691AA59F;
// System.Action`2<Mirror.NetworkWriter,System.UInt64>
struct Action_2_t7962FDDC98F8F0028DDCD9B0CADBA4C9C051E1C5;
// System.Action`2<Mirror.NetworkWriter,System.Uri>
struct Action_2_t92B2C6139E3081F85D498D28EED40F2C004C7B20;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector2>
struct Action_2_tCAF5159105AB227514D4319BDE35C44C14FB6C67;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector2Int>
struct Action_2_tE7875C2C92DB47F882FCBBE26BB848F3264D0B63;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector3>
struct Action_2_tD63675C2600BD13840BDC184B61A1282E2FA7667;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector3Int>
struct Action_2_tA9611CADC87D4AD3B0FB66AF118E5B269E6FA658;
// System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector4>
struct Action_2_t0708A1448BB2410AD3F0DF78AF427D6730760C7F;
// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage>
struct Action_2_tC9D1D64ED53FED0525E8EE017F8CAEBD3D9CF610;
// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage>
struct Action_2_t6B81023D4D427FA4343B3A8E4F4A88A145C6884F;
// System.Action`2<System.Object,System.ArraySegment`1<System.Byte>>
struct Action_2_tA0B9181EBC3DE1D849BF7E7F4A5AC0BFF3ADE4A0;
// System.Action`2<System.Object,System.Nullable`1<System.Boolean>>
struct Action_2_t19DDF6787EEF711A74EADD77E4E6F3D956E2C28D;
// System.Action`2<System.Object,System.Nullable`1<System.Byte>>
struct Action_2_t0EA2F4062B5A7AF23762E1CA13EE62E373B8063A;
// System.Action`2<System.Object,System.Nullable`1<System.Char>>
struct Action_2_t2BE39FF3B09EE50AA0545282A8EE5587BFFB2C5F;
// System.Action`2<System.Object,System.Nullable`1<UnityEngine.Color>>
struct Action_2_tCA122DBF107CF0C679F98E670FAADFE8FE1BEBA8;
// System.Action`2<System.Object,System.Nullable`1<UnityEngine.Color32>>
struct Action_2_t3B8EACCB52F36391CCC830BE3012A0973DBEB8C4;
// System.Action`2<System.Object,System.Nullable`1<System.Decimal>>
struct Action_2_tE1202DA57F63F53904EB9B7237892A05618A9DC0;
// System.Action`2<System.Object,System.Nullable`1<System.Double>>
struct Action_2_tE27D0AB32B79423AEFB88AABD75A141FB5EA0BED;
// System.Action`2<System.Object,System.Nullable`1<System.Guid>>
struct Action_2_t99CB75AA9FD19A7683885E5A2F24C3AAFCDDC278;
// System.Action`2<System.Object,System.Nullable`1<System.Int16>>
struct Action_2_t0720E8845D2D725B998A4D839E9CD687E9AA8A63;
// System.Action`2<System.Object,System.Nullable`1<System.Int32>>
struct Action_2_t8B8196A491839286593B1D6C22B7A0895C6C8551;
// System.Action`2<System.Object,System.Nullable`1<System.Int64>>
struct Action_2_t5F67668459C62E1686E6F70EC800F84023CA5ACD;
// System.Action`2<System.Object,System.Nullable`1<UnityEngine.Matrix4x4>>
struct Action_2_t7ECC231A0F21A8EAFF3CDDE28C0107EAAF418CC0;
// System.Action`2<System.Object,System.Nullable`1<UnityEngine.Plane>>
struct Action_2_t3C124597F3448FD87CDB4BCD38BCB06A4EAEDC72;
// System.Action`2<System.Object,System.Nullable`1<UnityEngine.Quaternion>>
struct Action_2_t8EE27287CA86F665416BCC3CEB75BFF21B6763B4;
// System.Action`2<System.Object,System.Nullable`1<UnityEngine.Ray>>
struct Action_2_tD35371E8ACD943C5D80DA61CE07E0B6AB8ECC097;
// System.Action`2<System.Object,System.Nullable`1<UnityEngine.Rect>>
struct Action_2_tF2E3AB37232E746CC65E72C46C18DFA82E69AA43;
// System.Action`2<System.Object,System.Nullable`1<System.SByte>>
struct Action_2_tD06D328EF21B755A8744E094F143390CB2FFBB4E;
// System.Action`2<System.Object,System.Nullable`1<System.Single>>
struct Action_2_t228E1A20705B669EA4EBD29D890F929E58CB0068;
// System.Action`2<System.Object,System.Nullable`1<System.UInt16>>
struct Action_2_tA6DEFB39C08E44C2B644C1FAC396492A4A6B8A21;
// System.Action`2<System.Object,System.Nullable`1<System.UInt32>>
struct Action_2_tC12C303F4598D6C6EC0D86E1FCEAE767AE57532B;
// System.Action`2<System.Object,System.Nullable`1<System.UInt64>>
struct Action_2_tE2135AF9B8B4237B445171FB2F8FF95B6A97B7AE;
// System.Action`2<System.Object,System.Nullable`1<UnityEngine.Vector2>>
struct Action_2_t4713D34F9988FCB698B7DA0A7D9B8A204C6CA2B2;
// System.Action`2<System.Object,System.Nullable`1<UnityEngine.Vector2Int>>
struct Action_2_tBB26490704E8CF5E6DB62C8034C2C634B511557D;
// System.Action`2<System.Object,System.Nullable`1<UnityEngine.Vector3>>
struct Action_2_tE22C5D126868AABF1A79F8592617AD154D66B8F0;
// System.Action`2<System.Object,System.Nullable`1<UnityEngine.Vector3Int>>
struct Action_2_t040A45BD91165322688FA8D225544BE87C45EA29;
// System.Action`2<System.Object,System.Nullable`1<UnityEngine.Vector4>>
struct Action_2_tEC96CFDA9D98801198609F46E307BF1C17C0372E;
// System.Action`2<System.Object,Mirror.AddPlayerMessage>
struct Action_2_tF27DDFA21F48F9D313ADDA7441B963FC9AA6057F;
// System.Action`2<System.Object,System.Boolean>
struct Action_2_t5BCD350E28ADACED656596CC308132ED74DA0915;
// System.Action`2<System.Object,System.Byte>
struct Action_2_tE894829A2C1154E5BF43E1E37F8D5586426273A0;
// System.Action`2<System.Object,System.ByteEnum>
struct Action_2_t41A214EE3EBB7463ACA8699A9C159476EADA910D;
// System.Action`2<System.Object,Mirror.ChangeOwnerMessage>
struct Action_2_t88B48FA22730395EF1E51A7A2972771D031A5C9D;
// System.Action`2<System.Object,System.Char>
struct Action_2_tFC1CD94ECC9BBC2047B3C781A1B83B2DF9693300;
// System.Action`2<System.Object,Mirror.Examples.MultipleMatch.ClientMatchMessage>
struct Action_2_tAD390F10177FE26729242610D47C9BAA921F5745;
// System.Action`2<System.Object,UnityEngine.Color>
struct Action_2_tA6D2280AF26A98A178E1D2455D1B9A83FADE20C8;
// System.Action`2<System.Object,UnityEngine.Color32>
struct Action_2_t1C11CECBE63666FF1C08415A1B683FEC522EE8E8;
// System.Action`2<System.Object,Mirror.CommandMessage>
struct Action_2_t4835364398852785232767ADC8361F049FEB46C0;
// System.Action`2<System.Object,System.Decimal>
struct Action_2_t469C223ACF2D27BB434BE7D9364B434942AFCF78;
// System.Action`2<System.Object,System.Double>
struct Action_2_t20E1B0E5A61C42E2C74B90596ED942D921D42DE1;
// System.Action`2<System.Object,Mirror.EntityStateMessage>
struct Action_2_t1EFB12E6CCF58902FAEAAC4C2EB883B71778DA47;
// System.Action`2<System.Object,System.Guid>
struct Action_2_t6A986B6DBED6BD578579C5BDA97931C92B339E4A;
// System.Action`2<System.Object,System.Int16>
struct Action_2_t914070E97ABAE8FBF5DB5B520982E5BE6F22FFA5;
// System.Action`2<System.Object,System.Int32>
struct Action_2_tAC461AE4F7B507965CE2E6A32853473F8C02CD75;
// System.Action`2<System.Object,System.Int64>
struct Action_2_tBAC40DD9F3FDB12141E0F973A3E6DBD9B65D5E35;
// System.Action`2<System.Object,Mirror.Examples.MultipleMatch.MatchInfo>
struct Action_2_t6F7E4F91AFFDBD3655646C203278FB18892BCA0C;
// System.Action`2<System.Object,Mirror.Examples.MultipleMatch.MatchPlayerData>
struct Action_2_tD8779192920625E7824209374872284249D11C5B;
// System.Action`2<System.Object,UnityEngine.Matrix4x4>
struct Action_2_t3C64BBA8D7C0CE1AFD1B68560D964965065531A6;
// System.Action`2<System.Object,Mirror.NetworkPingMessage>
struct Action_2_t12EDEA4C2BB1EA955C81CDF6F223F749C561408B;
// System.Action`2<System.Object,Mirror.NetworkPongMessage>
struct Action_2_tFB992070C2C2C01311FEA3098D84E5EFCF050153;
// System.Action`2<System.Object,Mirror.NotReadyMessage>
struct Action_2_t2920C412E842E4598BD848335A3EE80C70661D15;
// System.Action`2<System.Object,System.Object>
struct Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C;
// System.Action`2<System.Object,Mirror.ObjectDestroyMessage>
struct Action_2_t699BD3CC86B570FF349D8536FD855A26D3C10DCB;
// System.Action`2<System.Object,Mirror.ObjectHideMessage>
struct Action_2_t73491674AD3965BB99682243E91DC45F3FDDCBD8;
// System.Action`2<System.Object,Mirror.ObjectSpawnFinishedMessage>
struct Action_2_t7AFFCE926D1238FF71C1ED285049DABEA47E1CF9;
// System.Action`2<System.Object,Mirror.ObjectSpawnStartedMessage>
struct Action_2_t6650D678F3ECC975E373718FBBCF4C75B61A7BAF;
// System.Action`2<System.Object,UnityEngine.Plane>
struct Action_2_t8848423C02445C09CDE79D766D804D11639BA6BC;
// System.Action`2<System.Object,Mirror.Examples.MultipleMatch.PlayerInfo>
struct Action_2_t523EEFB49FBAF086F44C1B6AC796C385A1043214;
// System.Action`2<System.Object,UnityEngine.Quaternion>
struct Action_2_tDE5AC57A8E9A00B7BC0B0420FE43635FA84A2E8B;
// System.Action`2<System.Object,UnityEngine.Ray>
struct Action_2_tE16F1E549385589A493835919A9AD6123F85262C;
// System.Action`2<System.Object,Mirror.ReadyMessage>
struct Action_2_t0D65532CCC13FFF343A5DFD64A725437D3924032;
// System.Action`2<System.Object,UnityEngine.Rect>
struct Action_2_t5F545431D161B1E92A435ECB8F3DE7D923EF5CE0;
// System.Action`2<System.Object,Mirror.RpcMessage>
struct Action_2_t33F282E45E567B51FACA5C6DCED76A2BE1D80AED;
// System.Action`2<System.Object,System.SByte>
struct Action_2_t3392E551DFBD7852E59AA9CBE97E3FBB0752EB4A;
// System.Action`2<System.Object,Mirror.SceneMessage>
struct Action_2_tCE32AE3C7B7EE2A62D0DB6344F1D6B6C87B6EB36;
// System.Action`2<System.Object,Mirror.Examples.MultipleMatch.ServerMatchMessage>
struct Action_2_tE45C2C7B921B579F30A718DE9E11E71E974CC9DC;
// System.Action`2<System.Object,System.Single>
struct Action_2_t4A5313D1C1FEF099C0E5969104BDE957CD82CF22;
// System.Action`2<System.Object,Mirror.SpawnMessage>
struct Action_2_t195177EAA533C3206581DBAE7D103683E0E6B2B5;
// System.Action`2<System.Object,System.UInt16>
struct Action_2_t89BE0FE525BF3C6B2736E259A711D12E7BC72750;
// System.Action`2<System.Object,System.UInt16Enum>
struct Action_2_tDFD8BF49304CDB0AFD0BDE4FDF3BCA93F8278878;
// System.Action`2<System.Object,System.UInt32>
struct Action_2_tDA74CED516153CD7F42CADEB337C93F4AFDC8DDD;
// System.Action`2<System.Object,System.UInt64>
struct Action_2_tC2C04F74903D0BD2838A81020541B5DAF476227C;
// System.Action`2<System.Object,UnityEngine.Vector2>
struct Action_2_t15D6234343A6C232F6E9C1563A9666FD379F4A1B;
// System.Action`2<System.Object,UnityEngine.Vector2Int>
struct Action_2_t2A9111170E5F9F9711A910F2CD7B650F9A4D830D;
// System.Action`2<System.Object,UnityEngine.Vector3>
struct Action_2_t0F28FD6DF12DE3C0C0A3C8670D6FF563CB91D7DE;
// System.Action`2<System.Object,UnityEngine.Vector3Int>
struct Action_2_tA07091F44E5128C0BA44B35E278263CCB60A2D74;
// System.Action`2<System.Object,UnityEngine.Vector4>
struct Action_2_tA4CC875AD1B535E16B296C7E63CA2647BA2009C9;
// System.Action`2<System.Object,Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage>
struct Action_2_tF7E35EE8593504574A7421730DCFCEEE05B1DCEE;
// System.Action`2<System.Object,Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage>
struct Action_2_t4E383B0432304D07A4A7B2F37BC993DFB7E3450B;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Text.Encoding>
struct Dictionary_2_t87EDE08B2E48F793A22DE50D6B3CC2E7EBB2DB54;
// System.Collections.Generic.Dictionary`2<System.Int32,Mirror.NetworkConnectionToClient>
struct Dictionary_2_t27781EEAEE164B870331F779DBE0DED7F941F4D6;
// System.Collections.Generic.Dictionary`2<System.UInt64,Mirror.NetworkIdentity>
struct Dictionary_2_t55A938BB79E925B7A9B5D7F7C857728FC8864C14;
// System.Func`2<Mirror.NetworkReader,System.ArraySegment`1<System.Byte>>
struct Func_2_t0968F31AEE953CA42422B45BEE8A04FB272E086F;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Boolean>>
struct Func_2_t2CF9D64DD882AC30B207D3D6C3C4EFC846A40BC9;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Byte>>
struct Func_2_tC5D3F83B86462F66072C190B541019F5456F2DB9;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Char>>
struct Func_2_tBB870D080A3294867CDE63CF2AF4F2D4699C33B2;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Color>>
struct Func_2_tB85C8267AB8EBEC2E4C056F08CC1288F0C2055B7;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Color32>>
struct Func_2_t5B94AD2B8545A777AE2428975D92A19545B20228;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Decimal>>
struct Func_2_t2ED5FBB4104367F73E820E5864485F4EDD912E0F;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Double>>
struct Func_2_t3C87EC87405703EAD0A26AD3BD3A5B3615EB39E6;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Guid>>
struct Func_2_tDCE62FBEE709378CCBDB8A5F059E3D9EB17469CB;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Int16>>
struct Func_2_tA3EC1D81F5C9B991E5E551D9C0197BBCFA6ECFC8;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Int32>>
struct Func_2_tA8008B61C32E0E3A90F98593FFE956D148147DC5;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Int64>>
struct Func_2_t8F78F44B218E20FDA86CA793AEC9892B18B978F6;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Matrix4x4>>
struct Func_2_t4F5890FDEA23094C33678E54A33233DBBC85F64B;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Plane>>
struct Func_2_t93378D91D2D47434F10E8104D47A5A2363B48D08;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Quaternion>>
struct Func_2_t643EFCAEA913218C33F9E05144DAB3702712A18C;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Ray>>
struct Func_2_t4467783C00F409FFB8ED1FDCE87EF974AB372C4D;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Rect>>
struct Func_2_tCD175F70DBC690C8EF0231BF4AF1180C50BE25E0;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.SByte>>
struct Func_2_tFB31016B91F9F5C6B2CAC98095F7D025353DD033;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Single>>
struct Func_2_t2A9651B5311D10F2FDA41A113A50E18A22D93138;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.UInt16>>
struct Func_2_t4842934F57735E601CC8B995D6E040EAF256FBF4;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.UInt32>>
struct Func_2_tE53CE7EE59BDF79AA9E78FAB9A62695508883396;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.UInt64>>
struct Func_2_t4B1235C83470DD7C3286754A4CFC840FBFD35741;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector2>>
struct Func_2_t603A0F2238D55FD8325BC0816D55C2970606BB81;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector2Int>>
struct Func_2_tE2F89B2F7AD6F1B1ED5373840C57D1603C193D7D;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector3>>
struct Func_2_t58F06EBEDB50273CFFCE1BED6156FF00537E7BF6;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector3Int>>
struct Func_2_tB67E11F122FE23A54C3DC5758CF7ABCED89CA2B9;
// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector4>>
struct Func_2_t3EF4F45A92179F162ABDFA4C40C59CEAB4DF8C95;
// System.Func`2<Mirror.NetworkReader,System.Byte[]>
struct Func_2_t68753017D74F6BABDA2E69D18EC2FA64AC20B03F;
// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.MatchInfo[]>
struct Func_2_tEAEC0CEBCFCC9157E40E1A7FFFEB82D8A082B205;
// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.PlayerInfo[]>
struct Func_2_t57B5F9E47ED4C8F5611A672DF5C949F2D02972C8;
// System.Func`2<Mirror.NetworkReader,Mirror.AddPlayerMessage>
struct Func_2_tC36B775425D425FC2AF1D16C0CD70386004FFEA0;
// System.Func`2<Mirror.NetworkReader,System.Boolean>
struct Func_2_tE35FCB04208B127B021F241EA079464ACC551B7C;
// System.Func`2<Mirror.NetworkReader,System.Byte>
struct Func_2_t0BC506BED5AC85C37292910C8BDAD52A9F6C9986;
// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.CellValue>
struct Func_2_t22170DBE4CAFFA15631BB8B0CE681A822A3A160A;
// System.Func`2<Mirror.NetworkReader,Mirror.ChangeOwnerMessage>
struct Func_2_tAF26F2C22D87F8F669162B6542990070635D2A30;
// System.Func`2<Mirror.NetworkReader,System.Char>
struct Func_2_t26F6F63A0DA84EBAD15F6BCC374071D9F6C8B42C;
// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.ClientMatchMessage>
struct Func_2_t21336B204733D902134B7F0FB71D1D4D85655F8E;
// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.ClientMatchOperation>
struct Func_2_tFBF2D2CE91617E529AC8196465432685D724AEBE;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Color>
struct Func_2_t6DF06C835E4A0B6416DE67676C67EDBE1EBBFD78;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Color32>
struct Func_2_tA1D462828AADE4C7A521611177F1A49476CFE773;
// System.Func`2<Mirror.NetworkReader,Mirror.CommandMessage>
struct Func_2_tDB9FD64DF7D2CA31FFF8279F26075DFCF440D33C;
// System.Func`2<Mirror.NetworkReader,System.Decimal>
struct Func_2_tB643983D6CBD6220B483FEE44F3207125D7D3726;
// System.Func`2<Mirror.NetworkReader,System.Double>
struct Func_2_t589ED36A1F9FA112DA0EA54F4A46CBAEE886A07A;
// System.Func`2<Mirror.NetworkReader,Mirror.EntityStateMessage>
struct Func_2_tB637CE13E86654599FE7556001525095E552A933;
// System.Func`2<Mirror.NetworkReader,UnityEngine.GameObject>
struct Func_2_t1E82FEEB6D88844359FA37AD374ACC1AA39DDC4A;
// System.Func`2<Mirror.NetworkReader,System.Guid>
struct Func_2_tEC73485627298AE849634B626697EFC64D213D43;
// System.Func`2<Mirror.NetworkReader,System.Int16>
struct Func_2_tEE61CDD83F7808585CB68357D75DC2637FBA492F;
// System.Func`2<Mirror.NetworkReader,System.Int32>
struct Func_2_t3F9C9CE2DD4A455F6BF2ACA2CBC6A00343B798EA;
// System.Func`2<Mirror.NetworkReader,System.Int64>
struct Func_2_t0D88BB68D7BF644024EA7DFB9E7CDBA1EE7EF69C;
// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.MatchInfo>
struct Func_2_t0D870FCF01A0B3EEF980A0B74CCA07765A7552B3;
// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.MatchPlayerData>
struct Func_2_t747EDA19750AE43DE30476D128A90ACDBEAC48AF;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Matrix4x4>
struct Func_2_tAAC77BA3AEC5902F635E82ED90239D6BCD31F70A;
// System.Func`2<Mirror.NetworkReader,Mirror.NetworkBehaviour>
struct Func_2_t763657E22AECC9ED46856683B3045624CF6351E6;
// System.Func`2<Mirror.NetworkReader,Mirror.NetworkIdentity>
struct Func_2_t8D8F451E2C46B99D55F5781D36B4F981BE62BD6A;
// System.Func`2<Mirror.NetworkReader,Mirror.NetworkPingMessage>
struct Func_2_t3C7B620CCBA5E47EED93AC22DB224F015928D9BF;
// System.Func`2<Mirror.NetworkReader,Mirror.NetworkPongMessage>
struct Func_2_t57653632E080A4623E41FABED4B1B6707C4D1B44;
// System.Func`2<Mirror.NetworkReader,Mirror.NotReadyMessage>
struct Func_2_t9B160232C1403CEF10F27D1D1BE4368650EE22CD;
// System.Func`2<Mirror.NetworkReader,Mirror.ObjectDestroyMessage>
struct Func_2_t056E7181DAB40DF2A9D14BB4450C0DCC566784E6;
// System.Func`2<Mirror.NetworkReader,Mirror.ObjectHideMessage>
struct Func_2_t76221365B0738498867CB728129555B7A8617C15;
// System.Func`2<Mirror.NetworkReader,Mirror.ObjectSpawnFinishedMessage>
struct Func_2_tAE6F8E9F1706B23DD7BC49D738569171CF6E8C3B;
// System.Func`2<Mirror.NetworkReader,Mirror.ObjectSpawnStartedMessage>
struct Func_2_tBD4C61172F81BE2A292FAE166832A0324BB00B1B;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Plane>
struct Func_2_tF1646D6248B9372FDA9C49F858144441B68F98FD;
// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.PlayerInfo>
struct Func_2_t966D11E60D81CE62911BC8F172BA2326E5F49FE0;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Quaternion>
struct Func_2_tDF2B4816BD9F902CA19B4AFA6CA78C2A61867EA3;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Ray>
struct Func_2_tB124603C647DAC5696CA392A20B3EE6C213CCA26;
// System.Func`2<Mirror.NetworkReader,Mirror.ReadyMessage>
struct Func_2_t31743D3E7DDCFE0001801AD8A7459B2FE016E401;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Rect>
struct Func_2_t340EA86AEF734F68ACF5E9FF2A124F9ADDED52DE;
// System.Func`2<Mirror.NetworkReader,Mirror.RpcMessage>
struct Func_2_t5F7A34ACC37197D2AFFFFF3A63EE51E2B44D6607;
// System.Func`2<Mirror.NetworkReader,System.SByte>
struct Func_2_t3EE5A9E53DE82B19DAC86C3D30AC7BD98D305238;
// System.Func`2<Mirror.NetworkReader,Mirror.SceneMessage>
struct Func_2_t8C43EB67A953E2DA4EA0F861438FEF6C6F87360E;
// System.Func`2<Mirror.NetworkReader,Mirror.SceneOperation>
struct Func_2_tA736A0330DE6964C91228A379FAB411414BBBB31;
// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.ServerMatchMessage>
struct Func_2_t2EFEEFDAE61B1D665B02E8D247FA7A8AE1B3D3FF;
// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.ServerMatchOperation>
struct Func_2_t4A60C2EC0E8ECFA6E6872E11BFAD744A805D5FF8;
// System.Func`2<Mirror.NetworkReader,System.Single>
struct Func_2_tDA855C737BB7E9546B48245EC0F04DAA56A1CB03;
// System.Func`2<Mirror.NetworkReader,Mirror.SpawnMessage>
struct Func_2_t1FE35EF0C4BBDC7AFA697EBBB5B4D64547326DCC;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Sprite>
struct Func_2_tB35F19D43C3D74BFE4F16B609D5DAFBCCE477EC1;
// System.Func`2<Mirror.NetworkReader,System.String>
struct Func_2_tF047810C662C3A551DDB01290047E803F32DA440;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Texture2D>
struct Func_2_t58133EDD30520660CD4F542594E8D913BB704B55;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Transform>
struct Func_2_t8085A3B2562300C528C41159E557B58E555D6798;
// System.Func`2<Mirror.NetworkReader,System.UInt16>
struct Func_2_tD28C61E3A0A47F71DB0CDCB3B43C7F13D4F82235;
// System.Func`2<Mirror.NetworkReader,System.UInt32>
struct Func_2_t1FE4698618B38731C4E2D67BCC314778BA13ECC0;
// System.Func`2<Mirror.NetworkReader,System.UInt64>
struct Func_2_t86251E51D7B41B00F1CC89068612DB2EE6F5FDE0;
// System.Func`2<Mirror.NetworkReader,System.Uri>
struct Func_2_t2DBAA86C74F086DBA285C7B8ECE41C9F35E419D4;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Vector2>
struct Func_2_t9C4AED5DAF5E665602E3D752BDF50BAE7C2C5D0F;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Vector2Int>
struct Func_2_t1D54F05297BE0A4ACFBE3D87381C38332A2F7837;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Vector3>
struct Func_2_t9BD17C9037D00DFA5EFF1643A78C3E4300A31331;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Vector3Int>
struct Func_2_tFD61C39BCF897AD7E0777243349B4558DB639EC5;
// System.Func`2<Mirror.NetworkReader,UnityEngine.Vector4>
struct Func_2_tEA21835A50BEE04419A104B2F4D8CCE015122924;
// System.Func`2<Mirror.NetworkReader,Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage>
struct Func_2_t4CD599531AE1FA8CA3CF4057D910B500DBF99E45;
// System.Func`2<Mirror.NetworkReader,Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage>
struct Func_2_t5DF2F494B8936184EE2E2F51E135F530C09B76D0;
// System.Func`2<Mirror.NetworkReader,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar>
struct Func_2_t842043449CFBC0F4C127867ED6917B5566AC5576;
// System.Func`2<System.Object,System.ArraySegment`1<System.Byte>>
struct Func_2_t54336CDCB27B475E725C043FEF2FFE34F807E1E1;
// System.Func`2<System.Object,System.Nullable`1<System.Boolean>>
struct Func_2_t3720B07918CDF70232923BF3A8CEAD96E1B97B56;
// System.Func`2<System.Object,System.Nullable`1<System.Byte>>
struct Func_2_tDF175FA34A7EF4ED084CEC34DCCD4FFCE7187DAF;
// System.Func`2<System.Object,System.Nullable`1<System.Char>>
struct Func_2_t76784AE3FD6485217951AF0E4DF1EF027579C615;
// System.Func`2<System.Object,System.Nullable`1<UnityEngine.Color>>
struct Func_2_t277DE8FCC4D58D9C922F2E9E48C9836A9730E379;
// System.Func`2<System.Object,System.Nullable`1<UnityEngine.Color32>>
struct Func_2_t470D5B37E4893766336577FE720F58F7F69033BC;
// System.Func`2<System.Object,System.Nullable`1<System.Decimal>>
struct Func_2_t1EE7B88D4216B85C8665FB82E512E56E07A64752;
// System.Func`2<System.Object,System.Nullable`1<System.Double>>
struct Func_2_t1B81A262430D3552787048D67101A5DB5FB0FF1E;
// System.Func`2<System.Object,System.Nullable`1<System.Guid>>
struct Func_2_t0947A1DA2DA6CC217BCF51C2047F2A2F77FD5E63;
// System.Func`2<System.Object,System.Nullable`1<System.Int16>>
struct Func_2_t1F4D30F7C5A1C5A5E1C32C0EE2526402D0256042;
// System.Func`2<System.Object,System.Nullable`1<System.Int32>>
struct Func_2_t9BDD2E959989A0F342560C7A37F50108887A6F4F;
// System.Func`2<System.Object,System.Nullable`1<System.Int64>>
struct Func_2_t8B4541DCF3576ADB91352AD960AE14B97A5199A7;
// System.Func`2<System.Object,System.Nullable`1<UnityEngine.Matrix4x4>>
struct Func_2_tC1ADF3DC8C492897502820946F1DEAF640DEE4B9;
// System.Func`2<System.Object,System.Nullable`1<UnityEngine.Plane>>
struct Func_2_t39A050645526CE9F1FE48DA80C637A1094C096AA;
// System.Func`2<System.Object,System.Nullable`1<UnityEngine.Quaternion>>
struct Func_2_tB0FE6C770B3E7D10673472CEF7F7A404A313E064;
// System.Func`2<System.Object,System.Nullable`1<UnityEngine.Ray>>
struct Func_2_t0333326FB3B060B71D5CC50138A417CB745AC9CE;
// System.Func`2<System.Object,System.Nullable`1<UnityEngine.Rect>>
struct Func_2_tEBBD26D2A474EADEE27827872BBEE2114CC4ACFA;
// System.Func`2<System.Object,System.Nullable`1<System.SByte>>
struct Func_2_t2A3B1C6C9E76A988821205B024AB085553C52654;
// System.Func`2<System.Object,System.Nullable`1<System.Single>>
struct Func_2_t09C944E9E92CF4E666BD2A414C1050B0A3485DB0;
// System.Func`2<System.Object,System.Nullable`1<System.UInt16>>
struct Func_2_tE81BE34070BF1C8D4E290B73ECFAEE943B5BF0CD;
// System.Func`2<System.Object,System.Nullable`1<System.UInt32>>
struct Func_2_tBE16A3C03B9956B3EDC2954831E6D1887E0C527A;
// System.Func`2<System.Object,System.Nullable`1<System.UInt64>>
struct Func_2_t9344B2FAD5D0BA160A247C7EEB8D7E54CB6C15DA;
// System.Func`2<System.Object,System.Nullable`1<UnityEngine.Vector2>>
struct Func_2_t8427DA63AAE4E5A7888CBF062568A308A9879293;
// System.Func`2<System.Object,System.Nullable`1<UnityEngine.Vector2Int>>
struct Func_2_t761519B467969C404270B9B774F5FED211CC87D1;
// System.Func`2<System.Object,System.Nullable`1<UnityEngine.Vector3>>
struct Func_2_t282A2068D4E9F64E26AE2DD0625AC8F8D12A4C0D;
// System.Func`2<System.Object,System.Nullable`1<UnityEngine.Vector3Int>>
struct Func_2_t091A46A39B1CF6E1E553370FA483C3623915A30F;
// System.Func`2<System.Object,System.Nullable`1<UnityEngine.Vector4>>
struct Func_2_t2917E538BA6D9379979D6190F3F4A9BAF27F7CFC;
// System.Func`2<System.Object,Mirror.AddPlayerMessage>
struct Func_2_tC6F47BF3D64D54DC390DEA7C3BC733C6563E0D10;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_tE1F0D41563EE092E5E5540B061449FDE88F1DC00;
// System.Func`2<System.Object,System.Byte>
struct Func_2_t5C9D9EE08A80057DD8A6BF8F0E7483819FB6E341;
// System.Func`2<System.Object,System.ByteEnum>
struct Func_2_tFF45FB5FEDF57DF29D6702B86F41D1EB73B8BBE6;
// System.Func`2<System.Object,Mirror.ChangeOwnerMessage>
struct Func_2_t874CCA1FBE84B81386C19F66B459479242A53420;
// System.Func`2<System.Object,System.Char>
struct Func_2_tA8F6CB924B4548E42010325397F16C187C098225;
// System.Func`2<System.Object,Mirror.Examples.MultipleMatch.ClientMatchMessage>
struct Func_2_t54F672BBBEAC8D2709215BC414613CE56FB8D890;
// System.Func`2<System.Object,UnityEngine.Color>
struct Func_2_tB58BCE382BE21A0ECA2C8642716DD941FB670EDA;
// System.Func`2<System.Object,UnityEngine.Color32>
struct Func_2_tE51521503EC6E99EE2D7CC1F15A72876BFD3AB3C;
// System.Func`2<System.Object,Mirror.CommandMessage>
struct Func_2_tF62A213C212F79C67E8C43D5DD7C70750D6C6D9B;
// System.Func`2<System.Object,System.Decimal>
struct Func_2_t5D415239CB4CB3874B276935A09EF4D650EF3705;
// System.Func`2<System.Object,System.Double>
struct Func_2_t5D850B409400F6FC6B650829D4B758F5899212B1;
// System.Func`2<System.Object,Mirror.EntityStateMessage>
struct Func_2_t399D28FF903B89DDCF4673444BB3AED168ED1C15;
// System.Func`2<System.Object,System.Guid>
struct Func_2_tB966008A65EE3C580BEEAEA7E13ED7A153257838;
// System.Func`2<System.Object,System.Int16>
struct Func_2_tC43D5377992B28BE94D1A61A31D05D01B3153C90;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t9A0D493A82DCC47C9C819A3B045E02D9B5DDCE1B;
// System.Func`2<System.Object,System.Int64>
struct Func_2_t78D13C74B0F5A1DD640F8722F0AFB5F5144EAB97;
// System.Func`2<System.Object,Mirror.Examples.MultipleMatch.MatchInfo>
struct Func_2_t2BDCDF710598521A63032F1F4A7151427876B48D;
// System.Func`2<System.Object,Mirror.Examples.MultipleMatch.MatchPlayerData>
struct Func_2_t7D1179805331926FF931916FE392A6660AD491E6;
// System.Func`2<System.Object,UnityEngine.Matrix4x4>
struct Func_2_tBF1D7BFC66E867B07DAB51D1158B6014CDCFF80C;
// System.Func`2<System.Object,Mirror.NetworkPingMessage>
struct Func_2_t36712FEA5ABA9573E272B9F326AA2560F8F134B0;
// System.Func`2<System.Object,Mirror.NetworkPongMessage>
struct Func_2_tAB2BC2D7D8DCD30DD421EA2B596E29A851CA6974;
// System.Func`2<System.Object,Mirror.NotReadyMessage>
struct Func_2_t41C476CEDB33511DFC03F63715BB2AB6D98DAE7D;
// System.Func`2<System.Object,System.Object>
struct Func_2_tACBF5A1656250800CE861707354491F0611F6624;
// System.Func`2<System.Object,Mirror.ObjectDestroyMessage>
struct Func_2_tC86CB06FACA637370854ECEEE2717F60B64B34BA;
// System.Func`2<System.Object,Mirror.ObjectHideMessage>
struct Func_2_t461B9B3CD7F177FBFBC2987142C789614DB49B8B;
// System.Func`2<System.Object,Mirror.ObjectSpawnFinishedMessage>
struct Func_2_t5913B836B9B34B4A3D7F7ADD0F594DCD8782C3E8;
// System.Func`2<System.Object,Mirror.ObjectSpawnStartedMessage>
struct Func_2_t3104CBF8C4A12DD26E48A03D7FC94DA15B2856E0;
// System.Func`2<System.Object,UnityEngine.Plane>
struct Func_2_t71AA5F9303BD84786B5E76C59E2EC23602AE39DE;
// System.Func`2<System.Object,Mirror.Examples.MultipleMatch.PlayerInfo>
struct Func_2_t69DF1BAE52533FEFDCCA1294C8BEBCB863C644F8;
// System.Func`2<System.Object,UnityEngine.Quaternion>
struct Func_2_tF9A1676D5CC48AA93FA04FCF9B2FB5E3D6D8332E;
// System.Func`2<System.Object,UnityEngine.Ray>
struct Func_2_t040C6DBD8E79D890B4C62419CA5F6A904EA1C1DC;
// System.Func`2<System.Object,Mirror.ReadyMessage>
struct Func_2_tA458E8728DB879480AFF42AB43EDE5D5424A6AC1;
// System.Func`2<System.Object,UnityEngine.Rect>
struct Func_2_t69DD684ECAE49CD391AD3F6B556465178004A9BE;
// System.Func`2<System.Object,Mirror.RpcMessage>
struct Func_2_t19334C7D7D41A14D93AD26EC9D161D1A3255FDBA;
// System.Func`2<System.Object,System.SByte>
struct Func_2_tFA7E684C9AE1F9ED14B06D3CC6DBC2C2492486C4;
// System.Func`2<System.Object,Mirror.SceneMessage>
struct Func_2_tC1615AFA2FE69A46E605C59E2EABC1295CBCCE9C;
// System.Func`2<System.Object,Mirror.Examples.MultipleMatch.ServerMatchMessage>
struct Func_2_t47308A521FE0814186F73F78DB95A5C83DB2163F;
// System.Func`2<System.Object,System.Single>
struct Func_2_tB5C40A90702B6A6A2E315FD927EEFC9FB69F2B12;
// System.Func`2<System.Object,Mirror.SpawnMessage>
struct Func_2_tE41D048BD903F47B6358EAAAD01C1B223BEF9A1B;
// System.Func`2<System.Object,System.UInt16>
struct Func_2_t8F75D16C6A6CD98824844B9D931354AC0DBCB055;
// System.Func`2<System.Object,System.UInt16Enum>
struct Func_2_tE7F8E9A788049ABBD8E43380184C13BB07CE42C6;
// System.Func`2<System.Object,System.UInt32>
struct Func_2_tB86D019F1289E2D123C00796B373933613385952;
// System.Func`2<System.Object,System.UInt64>
struct Func_2_t0041BDC545AC23D00BA1439051E79D5351CF315C;
// System.Func`2<System.Object,UnityEngine.Vector2>
struct Func_2_t127163694D7C66D0F32B7F8F5BB2507F7516DEE4;
// System.Func`2<System.Object,UnityEngine.Vector2Int>
struct Func_2_t51AFC293A9EE45091BAFAE499602C1CAADD7DE04;
// System.Func`2<System.Object,UnityEngine.Vector3>
struct Func_2_t1F9887E0A0ADE496D09CAA16DBA7B19D5579727E;
// System.Func`2<System.Object,UnityEngine.Vector3Int>
struct Func_2_tFBB8AD85FFDEE93D72698FE0EC4BDE7FC96D5420;
// System.Func`2<System.Object,UnityEngine.Vector4>
struct Func_2_t01EEA5FE255AF6B9B90E3CFAE8B5571BC66E8302;
// System.Func`2<System.Object,Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage>
struct Func_2_tF380B94DC70C773BB37CB699FF1CDD8A617C3D1E;
// System.Func`2<System.Object,Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage>
struct Func_2_t558EB0477BD7C161CEBA3CD3689485898B6D0898;
// System.Func`2<System.Object,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar>
struct Func_2_tFE38F936D16387AF1B3CC164A91B33888267FEA5;
// System.Collections.Generic.List`1<Mirror.SyncObject>
struct List_1_t8ED884B12AC29C4F4BDFB975C0DB057D2C0519AA;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// Mirror.Examples.MultipleMatch.MatchInfo[]
struct MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572;
// Mirror.NetworkBehaviour[]
struct NetworkBehaviourU5BU5D_tB037699FB91FE996B291D1BD9E9941B2A6F53C98;
// Mirror.Examples.MultipleMatch.PlayerInfo[]
struct PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t52460FA30AE37F4F26ACB81055E58002262F19F2;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// System.Text.DecoderFallback
struct DecoderFallback_t7324102215E4ED41EC065C02EB501CB0BC23CD90;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// System.Text.EncoderFallback
struct EncoderFallback_tD2C40CE114AA9D8E1F7196608B2D088548015293;
// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095;
// System.IO.EndOfStreamException
struct EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Mirror.NetworkBehaviour
struct NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C;
// Mirror.NetworkConnection
struct NetworkConnection_t49880296B0FA972023F34582D7A41D7B63383E78;
// Mirror.NetworkConnectionToClient
struct NetworkConnectionToClient_t80F9FBDD786601CB93A63585D05BCAA1050C406A;
// Mirror.NetworkIdentity
struct NetworkIdentity_t5C06E7EE595FF674F722D11C1397B12518C007AC;
// Mirror.NetworkReader
struct NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1;
// Mirror.NetworkWriter
struct NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// Mirror.Examples.AdditiveLevels.RandomColor
struct RandomColor_t85537EF520483D7F0DD339CC9F802E1A53CC282F;
// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// System.Type
struct Type_t;
// System.Text.UTF8Encoding
struct UTF8Encoding_t90B56215A1B0B7ED5CDEA772E695F0DDAFBCD3BE;
// System.Uri
struct Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E;
// System.UriParser
struct UriParser_t920B0868286118827C08B08A15A9456AF6C19D81;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// Mirror.NetworkIdentity/ClientAuthorityCallback
struct ClientAuthorityCallback_tD9013EF9C1BA9FA8A240D80D87F90C1DED964CB0;
// System.Text.UTF8Encoding/UTF8EncodingSealed
struct UTF8EncodingSealed_tF97A34F40CABE9CE1C168967D60396F51C43DD36;
// System.Uri/UriInfo
struct UriInfo_t5F91F77A93545DDDA6BB24A609BAF5E232CC1A09;

IL2CPP_EXTERN_C RuntimeClass* Action_2_t00EE65E013F88779EEEE98835E86BDF400A3BF36_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t06CF1E09D2E968AE317C1B4066FE59281D0D7F35_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t0708A1448BB2410AD3F0DF78AF427D6730760C7F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t0726BDF29C00FDD4DA0F278B3AFC4E47F69A4CED_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t0FDD9213EEB281B92425DB7083B3E28D691AA59F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t102F2BE697BDA0818FA414975B57C948D9C2BBDF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t104D6887DC3FA703E6D350257E4EE72A3EFCF3A3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t117A07FAD93D252BC5E69F05373B4E286F1F8D56_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t15EAEA1713B2DE57B0F7BFED86F3CF5915138314_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t19A08A9BB7D482718CD3474975A6559CA159FA9F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t1AB30C8E5E10864F469D7062D99016352CC3602F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t1B2365362928779807EDAEE12F27010A7A8D39C6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t1D8E4389A2FC4D611519FC7C996BF96F763E5017_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t27076E67A79536591E04C63FD1582DB7D564BA08_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t284D0A27595992EAC03994E6C1A1E82225441B67_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t3913958E2393CE5F44A661B2F0835A6FC4513CBE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t3C170C2BFCCAA591C6DA755A86E3FEDBB4428D90_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t3D709A9DD3DD60B2669C03CAF9A9607B98F0CC44_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t4125FEC2DD5C797C8E273617CF2C3E2ABB98FA45_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t427A3AAB52F4338A54E05C0F9CCE238604B75463_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t437C51FCE7D4E11D08E66AC6884ADD843A29305E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t4A43C94439BC4A524A4463B987E9326B340BB5D7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t4CDBF06B555A45F0D103E46E4114CEEEFA34B691_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t4D3AA2594C46CEF975DCD32BFF9A0FEE380568B3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t5199EDEE1233BFABE45CDA21E9230C8346645716_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t51E55DFCEC17AF21DB946B2527D3669CD8E09542_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t63FAAE13C5F96B474021BB5448158556C8EBB594_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t6B81023D4D427FA4343B3A8E4F4A88A145C6884F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t6CB11000550B779DE6F0A9873A4FA47684E8A996_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t771EB50B64E2F517312250DEC5230DC62C7E36FD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t77285E9313B73F391F0816DD5738EBF05F8774AD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t776DBFE440EDCE827698E6B849C13676E750F733_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t7962FDDC98F8F0028DDCD9B0CADBA4C9C051E1C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t7A7E74A9B38640A94D5CC807EC76F706FB6693C0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t8863334817D651A8E3B8E434C682CA5A76CE9498_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t88B940D5126F22A0E314062C9CB9B8CF14430308_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t8CF38B31F52C4EF713F3D2BAC6B806B321ADC0A1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t92B2C6139E3081F85D498D28EED40F2C004C7B20_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t92D149BA0BB9E6C54B7036167F4A14DFF1B5BE08_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t932E36A723FFA4E592D80F6F8AE785A34669DA75_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t98800A090710915B3E861E237B1A9B6E9F29163F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t993E6168F76ADC356CC943B6992CDD55115F3057_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t9BBCAC312FAACBDD9B08E82065F0C66CB92596DD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t9E34BCADC1F9EF87E3BC9817BF9ABF3086286388_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tA50DCC93E783D93FEFF521E5255E78C184ED8E02_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tA527CAC169A61C3874E9DEC5127904806AF2463C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tA6272A2BBF53770CE719C794145D53E55F0DCC51_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tA70CFBAAF2BDFBA2BEE4003F4C2E6E5B12924D8E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tA9611CADC87D4AD3B0FB66AF118E5B269E6FA658_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tAF32C09EC861FEA50FE0977ED9C0DAD82BA3D637_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tAF93D04C699FAC16A658F8DA78DD252A93E952E8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tB31DAF5F45A869010A4A3D1EA9EA5C56DCFAB947_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tB478A835A6DC624AEA7C01C383390CE10F4D2BF1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tB5D2418D8D5BB1CA27E37232842C681B92ABDB9B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tBE61BBF1961F6628AEE63F058E11FEB22FB6A6ED_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tC4556EDF997707D306A86E36FC8EE3751F9C687C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tC5312D54454D5411CB706A055837C90CBD0FEC12_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tC9D1D64ED53FED0525E8EE017F8CAEBD3D9CF610_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tC9DC1064C14B7AF886718CB747591EC132E1E301_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tCAF5159105AB227514D4319BDE35C44C14FB6C67_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tCB8F678502AD5BFA52EE6F149351DAC3037677AF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tCF4F398AAC858F0B69CF49EA4DB6878F5E85A2FE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tCFCEE592405B35148F5E1AC506FEB4F8CB2F49B6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tD01A9F5ED6BB951B86BE216B2B8DE2C17FFAE551_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tD12F3221DBF89BF11C6CB2855E9BDBD70D6A8AB9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tD28B743D21DCE5F5BE026041A86D0F58543D86C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tD39051A11B2175464C227775F3F6FD44715C0767_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tD63675C2600BD13840BDC184B61A1282E2FA7667_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tD872DFCCB454D89FE5878BA1B3A03AA4FF2F7A72_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tD87BC92995377D813FAEA7DFF07C8F114451FAAA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tD938883EC204367D5A295A7CB2BB5D4D20A90029_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tDC9B80B3DDEFFE6AD670786AA0C0A41F09DFE02D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tDD6B83C4A6E2ECFC616A05B485C0D7E24A750E90_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tDDEAD4B308FD0F5A9299458AF5A6B47CA5D3732F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tDF176CFB33B35A021E5DA20E9485F6A452F2C345_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tE1C508C7DB184AC769DA763897EA5CE208D96BE9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tE28F1C5549DD431F6354E325F28C3FA842F94E57_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tE45BEC1FBF44C71FEAD78DC6323066FD51D7FE55_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tE631CB825D55269F80774B2F55C5E62192891CF8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tE7875C2C92DB47F882FCBBE26BB848F3264D0B63_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tE79E56090404F1ED684677C80F34095664D0010D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tE7DE89222EAD5CBE5FBA94711CB3C71222202DFC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tED2F1B32CB312EA2AFE3FF65E3EBD21ADF00A34C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tEE379855D79D043D165FD05C3786F7B0ADB89B38_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tEE3DE27FC7A1D873CE799E7124798B9A979DE913_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tEF11BC50B4B015730C0409DC35EAC7D6E03CEE11_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tF808E86BBEE3D1817CE8A0C2E299320C9DD9D1BA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tF8B877CE67C7AF23C81D6C5C15EB3D2FE44321A0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tFAE875C9728E772ACF44D3BCF5F2B94860FB1CDE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tFAFB5B1636A32CF85A452A078736260BB496C253_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t056E7181DAB40DF2A9D14BB4450C0DCC566784E6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t0968F31AEE953CA42422B45BEE8A04FB272E086F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t0BC506BED5AC85C37292910C8BDAD52A9F6C9986_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t0D870FCF01A0B3EEF980A0B74CCA07765A7552B3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t0D88BB68D7BF644024EA7DFB9E7CDBA1EE7EF69C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t1D54F05297BE0A4ACFBE3D87381C38332A2F7837_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t1E82FEEB6D88844359FA37AD374ACC1AA39DDC4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t1FE35EF0C4BBDC7AFA697EBBB5B4D64547326DCC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t1FE4698618B38731C4E2D67BCC314778BA13ECC0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t21336B204733D902134B7F0FB71D1D4D85655F8E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t22170DBE4CAFFA15631BB8B0CE681A822A3A160A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t26F6F63A0DA84EBAD15F6BCC374071D9F6C8B42C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t2A9651B5311D10F2FDA41A113A50E18A22D93138_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t2CF9D64DD882AC30B207D3D6C3C4EFC846A40BC9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t2DBAA86C74F086DBA285C7B8ECE41C9F35E419D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t2ED5FBB4104367F73E820E5864485F4EDD912E0F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t2EFEEFDAE61B1D665B02E8D247FA7A8AE1B3D3FF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t31743D3E7DDCFE0001801AD8A7459B2FE016E401_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t340EA86AEF734F68ACF5E9FF2A124F9ADDED52DE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t3C7B620CCBA5E47EED93AC22DB224F015928D9BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t3C87EC87405703EAD0A26AD3BD3A5B3615EB39E6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t3EE5A9E53DE82B19DAC86C3D30AC7BD98D305238_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t3EF4F45A92179F162ABDFA4C40C59CEAB4DF8C95_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t3F9C9CE2DD4A455F6BF2ACA2CBC6A00343B798EA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t4467783C00F409FFB8ED1FDCE87EF974AB372C4D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t4842934F57735E601CC8B995D6E040EAF256FBF4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t4A60C2EC0E8ECFA6E6872E11BFAD744A805D5FF8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t4B1235C83470DD7C3286754A4CFC840FBFD35741_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t4CD599531AE1FA8CA3CF4057D910B500DBF99E45_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t4F5890FDEA23094C33678E54A33233DBBC85F64B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t57653632E080A4623E41FABED4B1B6707C4D1B44_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t57B5F9E47ED4C8F5611A672DF5C949F2D02972C8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t58133EDD30520660CD4F542594E8D913BB704B55_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t589ED36A1F9FA112DA0EA54F4A46CBAEE886A07A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t58F06EBEDB50273CFFCE1BED6156FF00537E7BF6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t5B94AD2B8545A777AE2428975D92A19545B20228_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t5DF2F494B8936184EE2E2F51E135F530C09B76D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t5F7A34ACC37197D2AFFFFF3A63EE51E2B44D6607_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t603A0F2238D55FD8325BC0816D55C2970606BB81_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t643EFCAEA913218C33F9E05144DAB3702712A18C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t68753017D74F6BABDA2E69D18EC2FA64AC20B03F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t6DF06C835E4A0B6416DE67676C67EDBE1EBBFD78_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t747EDA19750AE43DE30476D128A90ACDBEAC48AF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t76221365B0738498867CB728129555B7A8617C15_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t763657E22AECC9ED46856683B3045624CF6351E6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t8085A3B2562300C528C41159E557B58E555D6798_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t842043449CFBC0F4C127867ED6917B5566AC5576_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t86251E51D7B41B00F1CC89068612DB2EE6F5FDE0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t8C43EB67A953E2DA4EA0F861438FEF6C6F87360E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t8D8F451E2C46B99D55F5781D36B4F981BE62BD6A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t8F78F44B218E20FDA86CA793AEC9892B18B978F6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t93378D91D2D47434F10E8104D47A5A2363B48D08_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t966D11E60D81CE62911BC8F172BA2326E5F49FE0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t9B160232C1403CEF10F27D1D1BE4368650EE22CD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t9BD17C9037D00DFA5EFF1643A78C3E4300A31331_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t9C4AED5DAF5E665602E3D752BDF50BAE7C2C5D0F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tA1D462828AADE4C7A521611177F1A49476CFE773_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tA3EC1D81F5C9B991E5E551D9C0197BBCFA6ECFC8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tA736A0330DE6964C91228A379FAB411414BBBB31_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tA8008B61C32E0E3A90F98593FFE956D148147DC5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tAAC77BA3AEC5902F635E82ED90239D6BCD31F70A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tAE6F8E9F1706B23DD7BC49D738569171CF6E8C3B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tAF26F2C22D87F8F669162B6542990070635D2A30_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tB124603C647DAC5696CA392A20B3EE6C213CCA26_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tB35F19D43C3D74BFE4F16B609D5DAFBCCE477EC1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tB637CE13E86654599FE7556001525095E552A933_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tB643983D6CBD6220B483FEE44F3207125D7D3726_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tB67E11F122FE23A54C3DC5758CF7ABCED89CA2B9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tB85C8267AB8EBEC2E4C056F08CC1288F0C2055B7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tBB870D080A3294867CDE63CF2AF4F2D4699C33B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tBD4C61172F81BE2A292FAE166832A0324BB00B1B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tC36B775425D425FC2AF1D16C0CD70386004FFEA0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tC5D3F83B86462F66072C190B541019F5456F2DB9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tCD175F70DBC690C8EF0231BF4AF1180C50BE25E0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tD28C61E3A0A47F71DB0CDCB3B43C7F13D4F82235_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tDA855C737BB7E9546B48245EC0F04DAA56A1CB03_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tDB9FD64DF7D2CA31FFF8279F26075DFCF440D33C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tDCE62FBEE709378CCBDB8A5F059E3D9EB17469CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tDF2B4816BD9F902CA19B4AFA6CA78C2A61867EA3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tE2F89B2F7AD6F1B1ED5373840C57D1603C193D7D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tE35FCB04208B127B021F241EA079464ACC551B7C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tE53CE7EE59BDF79AA9E78FAB9A62695508883396_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tEA21835A50BEE04419A104B2F4D8CCE015122924_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tEAEC0CEBCFCC9157E40E1A7FFFEB82D8A082B205_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tEC73485627298AE849634B626697EFC64D213D43_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tEE61CDD83F7808585CB68357D75DC2637FBA492F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tF047810C662C3A551DDB01290047E803F32DA440_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tF1646D6248B9372FDA9C49F858144441B68F98FD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tFB31016B91F9F5C6B2CAC98095F7D025353DD033_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tFBF2D2CE91617E529AC8196465432685D724AEBE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tFD61C39BCF897AD7E0777243349B4558DB639EC5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NetworkServer_t90298DAB739AB649EFA5EE04950D68A903D6E920_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t00DA01ACAF536E0AF78C6E8BC015679E9934DB88_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t0676D3BA8C95F6153F7DA06A84ECA06D85436585_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t0CB918AF0F66B31D68CF91A09F89A19D747B6B71_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t16EF44DD5D73A84B2F654B9671D58A442A179C99_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t198B364FF80EB504EC1C04BA6BDB4431FCAC173E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t1EC17F900AFBDD0AA0263D3A62B0630B495FE255_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t23067DF0051C0D5F767FB367AFEEE3C196DC518C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t259EB4559035BAE63FEDCB6F4C5B72D8CA00F6C7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t286A71B8B0F59FB5063D0E327C76169FAF8F45D6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t28D5381047606A72C4A5B7E019C0F77FBCEE1C15_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t2A59AD3448B11193AB518EAC5FFE24BA1ED83727_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t2C531D10760B34519D87218F56AE3866A9C6B924_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t2FDE27056CF1FB6DE71EBDBFE9EA615E76F9B215_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t33986420F424835F9DB93EEB52C71E46AB50C09F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t36FED30A7BD4508A497B3FDF00A93EFCBF2CC6C6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t39F933887B1FE66BE8404A5FAC7840D47C4CE2F2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t3E733A5CBD67C2A9860A6535DC31C53DC3294C05_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t4135AEE9236AC48C3B0D8C260D3A2DC89E212394_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t41E7ACDBED757B1830FB93B6F8CBF75F959CD886_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t429249FC13D808B0B1401B5B6BD0815F37C082B8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t436C9D757F7667ABD9FB2D630506ED1ED29652AA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t497575125F4FF1F1EEB32CB0C8FF12569655666B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t4AF495607AC4510AD0529F12D6A0BB0418C352A7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t4BEAA8B6A674DE41A4F74F8F9F7628DAEB444E28_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t4C51943F9874BC9563E868F14B951239A3817126_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t4EB488AA5A482EE20D6D1D3A5E90B63F672378C7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t4F335D70E11B569B7B2CFBE0E13B13D07B3BA4CF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t5072BFCCD801A3F27F8B16872FF18C70016F2CC0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t526D37680AAEFF5888DD8FAF5DE39A4018267FDB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t59A60ECBC527BDCD550C648C4F309C4E55AE4AA7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t5AAF2D6763D2C4497F18AC3DE6C04568FBA25219_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t5F1A5D57BC2BD2B0AB5EF928D722588060144152_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t637274CA1AF284DD25208101A16558A23B6E67B5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t65AE09028DBFFFB995B18BC7147A9A651D389384_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t6757FC52418E33974D53365FEF9C7406320DED1A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t6B6A1FCF933F958098FE25A100627E7241FEB588_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t6B88F366A149F8A58032F93153EF0A2DFC60E74F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t6FC28304AD44844C4A3BFD56F3139BA2B74B01FF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t7A28D876B4CE12F3E4C96FA0EDF6E7175F65C810_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t7D07B6E13CF9AA9927D4FD2385DB61464F9B5FE4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t80077764BF20B75B1B8B47491E64EF0F64D019DF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t822A5642F255AE3ACC0D29BCD12B45F8C7A6EBAE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t84A9F65298C551CBBA05DF543DBED0468412CCD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t85795B601B4F9B63136F7F928000E1996B634723_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t88B56AF4E9141686F1DE1F3D155CB3FBA34B913D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t894EA749E730DD156D39D5D3C89DD01B1840FC90_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t8CE8050417CA35F7B7ED704A952421B24A331DFB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t8CFA4E86EAB3C0AE3848A8BC147CB1A393CB3B50_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t8D947140B208E58AB2F0C97B38D53DDA61149A4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t92A37B251D88CED6738CA5720BFA048525E94646_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t936C97C09526A419E67C5F81F13C53B05B727833_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t942164B32720705E43130F8A5332C5B7AFF4FAE5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t94C4EBCF7B54695CBEF81ACC847D9D01B8D34339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t9773E186C54234048A015DDEBB899F90C35E96B0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t99038D5B7B0CB7EA48F6A69A3DA42DDF77732757_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t9999666C4A090A6C9AD43987D46A3326989F2E39_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t9C04509E1AAFBF9B8BE4514971DC0FAFCF576DCE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t9C76556BF9E8625EB258DEFF7A1656D33B4B236A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t9F020D67D02A1B6BF8EF51B03B6F298B7042AC26_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_t9F0D38236764D927F7809A9EF55BC5E04C385A80_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tA05950C10C6D79E26FD7C70ACCED03FCC01F5FC0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tA1A9ED8F30B3E4079D52171EBCE69E3D54C0E32A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tA2296DCE521FC65EB6FBA78472797DC918D9FE4E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tA296681D381BFB2393CD36BF8D1737E846F15634_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tA5BF854AFBD00B4F60644779FA6DEB2DC22F7F61_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tA9538DBB44C0A8B65EDF025A5D5FA611D152FA31_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tACF841391431A7BEF83CACCF4CAF12E6601B5A00_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tADD3DCC9F400A548DD461E9BE16C5ACCBCA80B8E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tAF89048D8EB67FD9887BFA7A88596629C237E4CA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tB18271683F45221F345D04D571881186F2B19615_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tB3752A50A44FC8C286392A80358AFF6F3CE44155_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tB498EAF4882CB62E4E9E943CFD2165144F1AD05B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tB503DFFAE8032B3381FEE0779316357D1CE28658_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tBE1B76CC86F1EDCE1B7A3203682971C129964518_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tC8C6D6D608FA323A6019F7C773782247361141D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tCB389B7DC3DE02BC9C35A7B57D2B65F26E3A7C60_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tCF89C1D29B73DD90DA2BB475B19671460984C68D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tDB685B03953454DEB76F09E00F64F95F5B14E21F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tDC04CC49F13C042AA2FC9CEA968B413D1B6F3F38_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tDC213F8037D359AA1C0EC410CE387A872B5F05EB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tDF34563EB909B8D900E05A73EAE294A1BE35E704_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tE473AB6B794D2436C26789A313B67896AB07A48D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tE674AC89FB334BAAB07744B62BEB917F65466F8B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tF19F6E8C291C912786327989D4FD0124DF26DB09_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tF38A27591F97AE59155FCEDBC831BE975EBD3842_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tF46320E57E205382BE23057439BFC8EA1F2457F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tF4BD915B07DF8D3A5EB55BCAAD572DEE9ECE8FB5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tF4E8B721D496EDF9F9FFF783C41F20BF05DE9F34_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tFBA7DC5EFAB4785CF16735020B26CD15760FF936_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tFD0BF8A2A5368E4951ED7917611F5A25E4D46A7B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Reader_1_tFFD0A7FB0A8C37CF4374C67CB0DF9F2448CA9784_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t02378C060D52C54C236D7DC3C612BD8B0B2153AE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t03D9486B5C228414D227AD0F4B1C32582EF57381_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t080683EBC9B7A30F6121E3179DCB7370B1E8F62A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t0BA0F75C0F12434151A45FAC15BD8A05CCF138C9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t0C51959312818298C3E0C1C9872771D904AD5062_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t110A7C2C82FDE2873D9B37CDBE36F7F5D9B42E86_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t1382F19A71CF32746849E9722B51E1B6EBC080DC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t190700FB2EFCFE964C8FDD64548F48100C8B992B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t1D6C09BFBA988FC86B2ABBD261ECF76022F5691B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t2190E1DFD8994854D9985EB6B653D18BBC814BBD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t21E999198EB9EDBDB5B29C76134D9493E50B5775_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t23D4BACC405E3FA13539AE7E93E94435ED73C740_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t243E336527949636CD7438A1DBBBDA88A829C4CC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t256EBB9B83C8B6F6F428369117FBBEC36C7E390C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t271DE44B9F6C5A28AB5ADF72AEDCAA3628BABB90_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t279D7AD1213DC4E446FDB2E12DD927F5544311B9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t2862DC0A99F1512C1E98B5CEEFEAB76FBB011055_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t28CA88055E5AD2A0F9B106076EF93C52105F504A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t28DF10100EA6DD2804A3081EC57C156DE5808B4F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t3A7BC19F51D39AA83BAC5C1DEC028E0AE21067DA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t3C56C64613D535D9C73CA3D587F334CD7CE30434_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t3D2702EC8F6235254020DB789887C072FDDFECAA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t3F4AFD2264190A12B59C26F438E4D9A0D5C6B8A0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t43CDE8B30140776D24809288A7A734939B0C8392_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t44DF034032ED1B1CEEAF5EAF3332EDE362ED947D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t4BDE3F806C4CC59448CAF1EE19A59A5CFEAD58F7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t4C15B820BC518F2CE261CF46CCD5C0763E35D445_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t4C3ABDA4E9C5685EE6B45F6ED3FACEE79F085657_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t4CC6F64E17DBF07E25DEBC8F9A2FC129D5E21F6B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t4D548019E9743B81B59DEF8D2478BEACFDC3CE09_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t4D7D68DB9331B49DDA144EDF2DEE5BCBCAC34F6F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t56257DBAFE2A2FE91E9247E46AC489415891F173_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t5B4A56E0557BF93D873BD64F49D7D7EB2FFAC769_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t5E74ADF66E1728C9F5589CB7743629820364AF71_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t5EA9A1C053345BA806F8721312945B9DF2E1FFA3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t633DC4AA7F39C88128E15553F3C582C26A375D17_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t6BDF566A850D915F8607498B2A74D90E6AD3DFE4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t6C6CC5FCDF55B79BC00FD028E0A44F85A45210CD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t72E49BCDFE4B2A53733511DC5BCDB5E02D0EBF54_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t76BD90233C405D75BFDE795DB7AADBF14659D95A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t79E192D8556A9F0A0CFF931EEF19A382A96A85B3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t7B4F1E8858C145418CFBD4E81EEB412568ABB75B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t7D22F5C863050135C73CAB5F962B1CB2C9DD592F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t7F01052FAB46FDA22D39911D07363E1D87E72BBA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t8390A5CF97A52E052D8612D70A087CFAF6A64170_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t83950ACDC982F33C59176AEBFE823CBB074A0BFC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t864220A64B0EDB912027564DF90AE901084A093D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t86A60D54965CEA1D0E31A7BF6611C9E4EA2A7B87_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t88385B356F11A0E27B1FA1BB321C08F75AE44C4D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t89600A4E62D36536DC444EAF231664989DD702F3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t918CD45EC571EF06C238F361375BF18469663559_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t93E7560AB93CC6616158EBA6F0ADF0F95A12807A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t95C4BC990ED47CA1945D53AA60D4F38C75743BAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_t978255B2785507C424403C582ECD6DC09003148E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tA5144EF44424ACEA981DF61078CF47C5A0E75FA4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tA63F5424DDE9B2587D7E34744054819D40E1EF86_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tA7C0E92EDDBAEE98892D2BFC9229C66F5D6C0D50_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tAB097320742DBC1A8B8DF7BFB4F762C902C20077_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tACC5A4F72D8EB16086B351E1784F11620EED13F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tADD2D185EF8CADFAA813221CFBDB1706512C378A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tAFAB4C7529C18C2FED4E4A6D66D55F4EB4CA10CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tAFDA6C5A95301BCFC4E49DC4B9E31CE5E13B51A3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tB00DD74436318ED1FFBD6AE43C142C83E0E441E7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tB611FFB23F108BACF0D008A5913B87BDA0B78161_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tBAC97F7921836787FC871388114C4389B83F337E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tBCB1D512EE5CFE7E2A2BA73BC72DA709A669FA52_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tC35EE38E41AE63A92291A26CB18FBF2D36A07896_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tC900E33B3D8F5D4C68DB3645A60C917BAC711321_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tCA15166526F2EA72615546B0168BD1A9B7067C0B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tCF7983600918F8F12E621BA83FD47D9F458B363A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tD572915EE39BA9844F8AB175F9C7BE4C23CA9B97_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tD6B98FC179BE9F9519958863FD7D3515B8FFC6A7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tD6CDFC673E86CE3DA4B9A902DFE96553C7C89B49_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tD745BB380F67A854010D62CF4FE8179D1D9A0FFF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tDB03F8AA9D2A369BF9554E72C2BAF71BECCC4D81_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tDC62F016728AC1B9962FAB7BA7DB68E48CECC6A9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tDCFFA2190DEB474AD0651E28A8D50F5AE9BA52E1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tDD7CE7C0539A0A26F84206A36C8A80400C6E622E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tE1FE9E7BA2D0C18CB94133B384971FAAD2EABAB7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tE36053BA0B28C83CF0A86F8398DABFC7F10A4B88_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tE837CDE21BF382361B1ED24E4C34DC6B7B9C65E0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tE871E366A3A0953860589F6AC9B876E03571DD8F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tEB9DBA2CB2FA886294119B2A97206EB710E97D34_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tECEB6A5A5CE5B7325CE0ED4C1A86ED28CC960D40_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tED1C9085F349458B13C36D062822203E85AF457B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tEF909D3F88F3F38B3C89136F9CEE84B8AF352E3F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tF790A3D1EF8C3E9D9DC8802687D0B51121BA2551_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tF86673563FB7B0CBA7838723701DABAE01570F38_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tF902FB09B54D9973B6DE4667D18385E6F351DE14_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Writer_1_tFB29484EAFF97FE2C50FDB48351489533752921F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral03E170E8209664E0E5EC4BFA0C00EC477E7A12B8;
IL2CPP_EXTERN_C String_t* _stringLiteral8958537A1C371340CA2DB0CDC27257F8CB3BC9D9;
IL2CPP_EXTERN_C String_t* _stringLiteral97DBD483FF6D25D8A2CF2D9700D08EB0CFDD00D4;
IL2CPP_EXTERN_C String_t* _stringLiteral9EEC476CC0C4E4E34F2F7CFB59773BB801BA4C9C;
IL2CPP_EXTERN_C String_t* _stringLiteralB7A997A8E0DBFC3D9382D423EC562F92A6E66F5B;
IL2CPP_EXTERN_C String_t* _stringLiteralC1B42A5ACC3B7F06923EA0BF4A5C01ED39F8C63B;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Resize_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_m82FEC5823560947D2B12C8D675AED2C190DF4F3F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInChildren_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m7CBAFA50AB995C9F53D6140718FCD31D7BEC7CC8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_m7C5BA3DA8EFBFCD37258B6FCA8B0DD3DAC4DC9B3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m1ED249926C5FFFF3711276C020ABE54B928BAAEE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_CommandMessage_m1EB0BBD20B46C4D33A437C5D4CF2D8EA43F18A8B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_EntityStateMessage_mFACC1BD75FFD70F53F10FD27A7EDB7AC60E2DB5D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_Examples_Chat_ChatAuthenticator_AuthRequestMessage_mA629A2F8CCBFF4C76544ED51BD2A35A5947315EF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_Examples_Chat_ChatAuthenticator_AuthResponseMessage_m43271020844960E04376F6C896CFE7D9AC33124D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_CellValue_m960AA8E4580568652E48F8810768B4DEA14B34F8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ClientMatchMessage_m588555F2CD175A2C40F8045755BF3F659CDB65B4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ClientMatchOperation_m967DCEAD7E36E480A8496E822CACE1F60AE9AFB9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchInfoU5BU5D_mE23CB91C4A5FF33675CF8A56B16BA006EC9047E9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchInfo_mB709C9D76C0446BF9C8C43CEBFCEC92A0D42551A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchPlayerData_m3268F6B9575F69594F5CEE743E0EA6FDF9ED56E3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_PlayerInfoU5BU5D_mDB000F31EDB9991937FE2C78FE0E3009EF2C1209_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_PlayerInfo_mAAF3F3CF61F837BA88391A854E75733E0F5014F6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ServerMatchMessage_m45838D1053B8C35602D526DA8A918170A84E882B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ServerMatchOperation_m0E0217A1EA28419B9541B868BB7E160A7F9876C8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m2AB11C6E07E94D6C3DB059650CCA9BD6D9838F5C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_mD30EC9CAE30F7761C7D307A8671F27E9D63BAC17_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m208EB5BA02CD99D1236C15A7B3E6C13E0935702B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_m1805A0712B400427B3DA28B6B83D2DB7BA47184C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_mEFA99EDE85679F1A1946C256D3829EC04CBF8A40_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_m9DCF582A7F2ACD58B46CE0B5006DC8DF925AAB07_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_m7236240B1388E3E63F1E45767148C047F2B86681_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_ReadyMessage_m39CE2D271EF006C1BF90E631FE38A0509A2A7174_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_RpcMessage_m4FC20C673517D44D58414A7AA047AF7D75A377C4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_SceneMessage_m5E357A51D411045B49C0B390BBBEEA981F754A43_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_SceneOperation_m74E8C7FCF4974E3652B0B512F68390584B1B3DEF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Read_Mirror_SpawnMessage_mEC8E19BCC23380500B3605B78866DDBEA4C904EE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_mEDEBEED5BBA0B7D7E68879616FC283492746A528_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m71C4764A857C326F63101E4181166D807AB065B0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_CommandMessage_mB652F843580DC0EB2C46997A80B317C449CEC38F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m68475F9DBE473336E6961AAF9477B31B544CEB61_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_Examples_Chat_ChatAuthenticator_AuthRequestMessage_m8266348F8A557605B0C42EC1EF5EA60DBA5E1C53_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_Examples_Chat_ChatAuthenticator_AuthResponseMessage_m12300CEF77BCC2BC22FE9F7B9385CE87D7B02E10_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_CellValue_mE850D02656D8C7B9181D58B92D4CA0583C063A1D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ClientMatchMessage_m9534DBB36A6BF3241779E9E5168D444DDF31E51C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ClientMatchOperation_m08496CD05975A28AB59D4513B9BA36209FD081C0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchInfoU5BU5D_m2A631F7A481D201FABBF213396D9D3C548AB54C0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchInfo_m154E3D80BB1984768FBF6C11EC8128FFF90EB743_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchPlayerData_m30BD65E22A872300D89020C18828BD180BA41FA7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_PlayerInfoU5BU5D_m258F7D9CB39C3B6D8C6B2BA7E8B1FE9491B38B50_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_PlayerInfo_m03B2F18306AC1FE8C7C933859A265C2B38C03700_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ServerMatchMessage_mF90A563F787035F17D0CD90966372FB8DEFF4A03_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ServerMatchOperation_m4ED27F7C3469C55F345EFBE642DC3F4DB0FB9B5C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_mFAEC5102CEF441E12C2E1A9A267D3A70C60C7B5C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_m79990113E28B9ADA181528EFC5D92C21AEC24CAF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_NotReadyMessage_mF6F4E00A0FB4198BA8291861F03B44A5EA9473E2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_m635CCE7CF2563B4550CB476F6AC5EEB18A6D939B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_m2233A20099B4B9AAD2F5232DC7B04C59A1581350_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_m8F8249A46B9C3061ED971ADF650AC9609E486619_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m2875D87F1F21B322D53603292FFE5BF8D604EC47_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_ReadyMessage_mBAE8C7940A984DEEA5331A23E0A01BB3A6B481CD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_RpcMessage_m690CA90FADE7D8E3E22236A59E88C3EA5D0F720B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_SceneMessage_m2415AA836382B52EAF54E4CBB6425300C99BEF86_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_SceneOperation_mA2C6453809A524632C4C882EE0133AAE2B596560_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedNetworkCode__Write_Mirror_SpawnMessage_m4D40E1220150E253BDECE7A9915957EDB4AFC499_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkBehaviour_GeneratedSyncVarDeserialize_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mCD3E867438BB843F76C76561EC9F138D8CC6694D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkBehaviour_GeneratedSyncVarSetter_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mD43D62C7BD24575258CA0CF86ABD19131A08BE2D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m3CE6D0D53E5BA355729F19428F259391B4A375B5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_mC8AFAB257D0DC0249D387758D4F204833AADE730_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadBoolNullable_m49866827FF66A52CA4E36AAED2D3ACC8766F8B38_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadByteNullable_mB622478495C2AE927128F9F196A47DCFEB666E4E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadBytesAndSize_mB707572AAF6CBDE9E6FAC190629882468EAFAD8E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadCharNullable_m728E4E8F336F06A0BDB8BDEE69842C707ED4540A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadChar_mCCA8829AD9CA54D8510AE4C3E3D1CA0F6F6E8966_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadColor32Nullable_m95A7EDB77042A0B8D6D00D2C96E9A530DEA6AF8C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadColor32_m0F0066C51CACC736B893D9F3C1D4324F87641BEF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadColorNullable_mDA6AADFE45C4CE1364429EACA43199CB319C9065_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadColor_mC5D200708B20F2ADC42224245960E2ED7E5DD27A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadDecimalNullable_m18D27D0176D98F043EC804A512EB8B55856229E9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadDecimal_m79DE6589996D493A3A95BAD98036B09FF9CB144E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadDoubleNullable_mE52DB83CB818F30F912FD40175B39731A2FBD33B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadDouble_m949A60A21C6EB3B9952A43355903F08B3A7E0EF9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadFloatNullable_m1EB56AA1F1CDB7981728CACF5941EB0B6B4275BD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadFloat_mF3D9834531FC09112A506971638FB9682A231D97_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadGameObject_m037E8EDDA39F95DA70EE3226939F677F9E3A2EBD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadGuidNullable_m884FD11E39BB14010073AB443D46779317340927_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadGuid_mCFFAB7379132286F7C9CC70EC291F8B28EA08B0E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadIntNullable_m9F68CD73D47D10DE2E1C6934DE14234E19D02E71_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadLongNullable_m747B938C128B0CAD7E22D0909E1AEE9DFDB54F67_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadLong_m67D408F9D8D9FB04A0101AAE2AB9B01120E34435_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadMatrix4x4Nullable_m508241752BEC24CCF4BD45230613444685553D06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadMatrix4x4_mBB21ACB1A8610F3813CE4A37DBF1608CA31A0E2C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadNetworkBehaviourSyncVar_mD3DCF91C73BB12C70E487EA4C4C85EAC62FE8A1A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadNetworkBehaviour_m6D724C97DE822B84C3FF75E80DA169D7C44E5E0B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadNetworkIdentity_mFDB6779F9A77F88F9760FD9902EFFDF3331E62AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadPlaneNullable_m252E55444808DDA4A5CEBCBE440E34728ECA5120_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadPlane_m2DA9573A8252F9B24A10E9E1AB448976D9963B96_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadQuaternionNullable_mE4E31E56C486837C0EC9C6047B276C9452D02C9D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadQuaternion_m135F5C523703C700E6A266DA9718E44D160BB567_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadRayNullable_mEE7ECB615AEFA818E73B366F681EA86595CA8F19_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadRay_mBE12F756FAAA9395B88F69C6A43F8576921AB20C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadRectNullable_m58AF30FAB6E523648BA18026AD02B220FBDDBC85_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadRect_mA4B7FDD8840C7E3A299614815C36EFB27232AB3C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadSByteNullable_mEAB105DEC52D7789AEE2A6E110B66A3C2EB8785E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadSByte_m1B3975CC87DD10621C8A369EA7D053AFE57E958B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadShortNullable_m6B7ED0AC2C951C2461A34D3DEB05E6055399B896_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadShort_m5FFC8A9D90AE04D1D0AD681F1D2C32564DBC8677_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadSprite_mA5B19DCF570BA845B63AA79858FCFDF27DEAE040_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadString_m5A56179D2C1CB509EC9C762F97BBD4133A5AD394_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadTexture2D_m5795D8D017B66A5ED4BDE243E306BD2B77A35EDC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadTransform_m56C2AB03C3891F0A72C1FC7153655E7AE4DCD6E8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadUIntNullable_m3D4906C1707F48E5439F4EA0E9DE4B8860AB2E73_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadULongNullable_mE853C2A40E3E7F9FD1BB49D5E16BCB9310B0752E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadULong_mAC6B83521EBA7FFEDFC72A6AAE1BF5D87221A5F5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadUShortNullable_mEDDEE70BF7A15DC1503C4BDF580F54A26C82DCA4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadUShort_mA98395DD1B1DA249096858B171B8BC23D95DF765_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadUri_m9CB721F84C66F0749E586B02C4CE8E472F266C06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadVector2IntNullable_m698F001AADBF901CE9571E3AA5687DFC1DD65701_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadVector2Int_mC9CEB6A103CD7C5DBCD8A944A57A59C0D1311F25_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadVector2Nullable_mCDCE58B581701AC12499A36355838E45F298C817_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadVector2_m673B821E39E194BA5E2B7E5F444D6CCD76812811_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadVector3IntNullable_mB88012F753982406CA6C49E8440318BFF784AF97_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadVector3Int_m59BAA3EBC52DB1635EA840D23B9D4A011E480E3F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadVector3Nullable_m17D39303F570FAB53014718C07327F9DAAD8DB18_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadVector3_mD35BF8B14DD5F75688AB9C360D138D1BAB432637_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadVector4Nullable_m1303DE93C2EB13F32622A8B868B17610B8C4AD09_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReaderExtensions_ReadVector4_m7870D12D4D86684F68719E7F040A33A085C2F1D4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReader_ReadBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mD424A6F72F431A64491144A4321E8915B2A9F179_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReader_ReadBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_m6DBD713FC553C165E92A9ABA7691638AA9C2E7BD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReader_ReadBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m9F3B82FB2EF977402985132FD7A7B3F531AC32C8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReader_ReadBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m4436F2F119D575AC7A0EA84CCD5C3CF655A3FFFC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReader_ReadBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mF27EFEBBC75EBD54AF10CE92FF64D5C780A97A69_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReader_ReadBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m5CB828366095AF0971BD2196AC15F944092E1E91_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReader_ReadBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m20406E9280C5EE4AA46F5E308A00B1CE728A70A3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReader_ReadBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m5B0E578FA24816AF3240FCA5BFF9353FBB4B958A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReader_ReadBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m17B9EA1CB3FF9F5A7936974AAD62B19383E2D69F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReader_ReadBytesSegment_mA17220D13799B7AA2EFF9B49C6F1F98B486A330E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkReader_ReadBytes_mA1E53425AAD4AD3038C9759F6971533248347130_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m0946D038BE696722FB3ED0523DEC8233C19FFAC2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_m348C67F42E07DE2F8A0295C5B388A6985FF81A0E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteBoolNullable_mFB1ADF7E798F7991680382003FA5584DA972EBBE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteByteNullable_mE34CDDB5354D7536941F1AB0DB0EFA4E3269C5A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteBytesAndSizeSegment_mD7FCDC44AB0313C3ED0AFBFEC77366B6947672AC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteBytesAndSize_m3A29964C9A4F7D85B49358324A238EADA7DE57BA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteBytesAndSize_mD5E8FA492EACCF5C68D0E76D84C20689CDAA0F27_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteCharNullable_m3D59A08FC508A0A1469115A766844A3DC3F1E420_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteChar_m79E8B11FA260E5C83FAAB385A039B9B73F4E15E7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteColor32Nullable_m3931F587C14E96A05B25E3446B6F7AE6D81115C0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteColor32_m123810E64991275156516FBB8AD2CFA67A7C3B7B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteColorNullable_m0B728B0EF504CC3FB0CE87FAD1505794AB82CDBC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteColor_m142E05754268CB4F297199994A61605D0FF1D9A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteDecimalNullable_mACB5EA0A0661A694C8521C3C245AC9EE3ECAE1DA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteDecimal_mFDD008D98CD77D9B4E63EF9AE0421FABAE70F483_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteDoubleNullable_m91EB95539CA35FAA383E01BEFA894A346A218ACA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteDouble_m0475F5FB9E1D69D60501C7158AD3431680BC1BDB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteFloatNullable_m0F2D06A7FA1A84F3F3C54537D6A3ABAB3206585F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteFloat_mA3AEF60E8288F55D5A3365AA0E4730AFFC231050_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteGameObject_m1B7DE5CB70EE416C894BA361CE421473734456AD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteGuidNullable_m19C9499197D2DEEB57A46D81FC9993D6EBFA14D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteGuid_mFF6C6A1BC90A9A7BBC9C179FA6FC25753689D3F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteIntNullable_mD476FA9C8F66E723E823733776EC840B4DCE6FAF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteLongNullable_mE9A8A93B2C853063398E244A3FA8342BB51D0C5C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteLong_m631751934892884B4E8B0FAF18BC616ADBAE1E90_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteMatrix4x4Nullable_mEBA4E2383B008575F3957933958627DEE30465D7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteMatrix4x4_m19A3B92281557AC6E231E3B5C663ACB8366CDFE2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteNetworkBehaviour_mDEE6FB11729AF7833D749E1C0573A559113E26A3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteNetworkIdentity_m670598EE39418EC82E5A35DD60EBDA69D7B8A74A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WritePlaneNullable_m1D7DE145477804CB9A5F078D51F2C31408A9CA2D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WritePlane_m5BF0BAF633E94AAE16D6D7E44B78E474E601077F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteQuaternionNullable_m7E68536A12BD33C4E0D063841FCB8B3319CE546A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteQuaternion_mFC2E046965F6BA1B694218D5E60E26807974ACBA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteRayNullable_m4C9EE3A8F4B5A24523EE0A02827A481F300A7C36_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteRay_mE3C68E64E43515730710198FF05734D077BDBEBA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteRectNullable_mD90246AB0237C3D9B3D669CEDFF1548D9DE26364_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteRect_m52D47BD93F73E06FB131C75A78127E3CC9073093_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteSByteNullable_m3330A77E2E4D2AC1B90BF53BC8150063BB6F8B30_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteSByte_m777D700EE0D8256617BE1128DE65C2DEBF674EB3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteShortNullable_mB84CEDD1AF6243DABA5C235B4013AD5A1801BE6B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteShort_m8593C0C47C9EADF1A65AA97BCBA9C15BF3739089_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteSprite_mC849B7B1044D0DC1989BC8F5A77DD93CDB7B0C82_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteTexture2D_mFA5FE217BF0E9D2F1CBF3A50C7A0B9C689B79782_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteTransform_m2F65EBB30598661EE20259C40E58691589593CD4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteUIntNullable_m5C618D0B2F565D4C20CACAE51E3D8A9AAA7EC3A4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteULongNullable_m5CD68A058B65F0F3B8729DEE05D43A94E222055E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteULong_mC0AE4801C58209EF02B73E3B353100B3AB95D28C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteUShortNullable_mE77F289B55D295E545826AAEA6CAAFCD26FA11A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteUShort_m4AEC8147034117F9EB131043089577CD2DB42DB4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteUri_mFB6E40C094D853A44F750835EF778B567546D775_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteVector2IntNullable_mDAC15DF3BA2A0FDA9705D0D35A3C4F486D2DDFA5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteVector2Int_m0099C36CFAF8015034E1CBC4CFCD7623543C758F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteVector2Nullable_mF82E294E5D5AB3D06DEA7404DAD4C9430D89C728_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteVector2_m5C9C94ECCE2643B670009D710BA8D6A2434F8BA5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteVector3IntNullable_m6EDE27130713A9C3A4012DCE58D53E488EE7E36A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteVector3Int_m2A6D52133117098B0C8A65520CBEFF8C4297B47B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteVector3Nullable_mD35B4E68313CFC87EF7B80823D9F0502C4D63E62_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteVector3_mB3C2B2F2D3C9874F883C12813FB20B9D5AABC882_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteVector4Nullable_m225B849A988CD2861387E8368F780E90E3D956CD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriterExtensions_WriteVector4_m710FEA287EE2C56C2C7DA468B394D23FE2424023_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriter_WriteBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mCDD7CA43B0B14857B8777A663BE02A0AA5917764_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriter_WriteBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mDB472FF359C94951E696CB3FD4F6016CFE6F82B7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriter_WriteBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m520D47927C9E53348DF0990757101E4408B4E4E8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriter_WriteBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m5745AC0FB4E1ABCB68691585D1B48F92DA99AEFB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriter_WriteBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mD95146DDEF099B2575801811770B7527E72A4969_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriter_WriteBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m7A05C5F5DD5D33FC126D2DAC895A096EC3402A14_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriter_WriteBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m3F8565D904BC20262924C7AF8BFBB3F7FE770535_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriter_WriteBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m869219CA464A4A9CCE03043BF274E8EBD19428AC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NetworkWriter_WriteBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m6552B19126A3F040F7E78F41CEB63CA85B0EF8BC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* RandomColor_SetColor_mBDDBD9EE5A4F48CE9121665E51B8DE6AC5E643DD_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41;
struct PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_marshaled_com;
struct PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_marshaled_pinvoke;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572;
struct PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mirror.Reader`1<System.ArraySegment`1<System.Byte>>
struct Reader_1_tDC213F8037D359AA1C0EC410CE387A872B5F05EB  : public RuntimeObject
{
};

struct Reader_1_tDC213F8037D359AA1C0EC410CE387A872B5F05EB_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t0968F31AEE953CA42422B45BEE8A04FB272E086F* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.Boolean>>
struct Reader_1_t36FED30A7BD4508A497B3FDF00A93EFCBF2CC6C6  : public RuntimeObject
{
};

struct Reader_1_t36FED30A7BD4508A497B3FDF00A93EFCBF2CC6C6_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t2CF9D64DD882AC30B207D3D6C3C4EFC846A40BC9* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.Byte>>
struct Reader_1_t4F335D70E11B569B7B2CFBE0E13B13D07B3BA4CF  : public RuntimeObject
{
};

struct Reader_1_t4F335D70E11B569B7B2CFBE0E13B13D07B3BA4CF_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tC5D3F83B86462F66072C190B541019F5456F2DB9* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.Char>>
struct Reader_1_tDB685B03953454DEB76F09E00F64F95F5B14E21F  : public RuntimeObject
{
};

struct Reader_1_tDB685B03953454DEB76F09E00F64F95F5B14E21F_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tBB870D080A3294867CDE63CF2AF4F2D4699C33B2* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<UnityEngine.Color>>
struct Reader_1_t5F1A5D57BC2BD2B0AB5EF928D722588060144152  : public RuntimeObject
{
};

struct Reader_1_t5F1A5D57BC2BD2B0AB5EF928D722588060144152_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tB85C8267AB8EBEC2E4C056F08CC1288F0C2055B7* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<UnityEngine.Color32>>
struct Reader_1_t4EB488AA5A482EE20D6D1D3A5E90B63F672378C7  : public RuntimeObject
{
};

struct Reader_1_t4EB488AA5A482EE20D6D1D3A5E90B63F672378C7_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t5B94AD2B8545A777AE2428975D92A19545B20228* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.Decimal>>
struct Reader_1_t9C04509E1AAFBF9B8BE4514971DC0FAFCF576DCE  : public RuntimeObject
{
};

struct Reader_1_t9C04509E1AAFBF9B8BE4514971DC0FAFCF576DCE_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t2ED5FBB4104367F73E820E5864485F4EDD912E0F* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.Double>>
struct Reader_1_tC8C6D6D608FA323A6019F7C773782247361141D0  : public RuntimeObject
{
};

struct Reader_1_tC8C6D6D608FA323A6019F7C773782247361141D0_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t3C87EC87405703EAD0A26AD3BD3A5B3615EB39E6* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.Guid>>
struct Reader_1_tB498EAF4882CB62E4E9E943CFD2165144F1AD05B  : public RuntimeObject
{
};

struct Reader_1_tB498EAF4882CB62E4E9E943CFD2165144F1AD05B_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tDCE62FBEE709378CCBDB8A5F059E3D9EB17469CB* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.Int16>>
struct Reader_1_tE473AB6B794D2436C26789A313B67896AB07A48D  : public RuntimeObject
{
};

struct Reader_1_tE473AB6B794D2436C26789A313B67896AB07A48D_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tA3EC1D81F5C9B991E5E551D9C0197BBCFA6ECFC8* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.Int32>>
struct Reader_1_tA2296DCE521FC65EB6FBA78472797DC918D9FE4E  : public RuntimeObject
{
};

struct Reader_1_tA2296DCE521FC65EB6FBA78472797DC918D9FE4E_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tA8008B61C32E0E3A90F98593FFE956D148147DC5* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.Int64>>
struct Reader_1_t16EF44DD5D73A84B2F654B9671D58A442A179C99  : public RuntimeObject
{
};

struct Reader_1_t16EF44DD5D73A84B2F654B9671D58A442A179C99_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t8F78F44B218E20FDA86CA793AEC9892B18B978F6* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<UnityEngine.Matrix4x4>>
struct Reader_1_t8CE8050417CA35F7B7ED704A952421B24A331DFB  : public RuntimeObject
{
};

struct Reader_1_t8CE8050417CA35F7B7ED704A952421B24A331DFB_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t4F5890FDEA23094C33678E54A33233DBBC85F64B* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<UnityEngine.Plane>>
struct Reader_1_t0CB918AF0F66B31D68CF91A09F89A19D747B6B71  : public RuntimeObject
{
};

struct Reader_1_t0CB918AF0F66B31D68CF91A09F89A19D747B6B71_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t93378D91D2D47434F10E8104D47A5A2363B48D08* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<UnityEngine.Quaternion>>
struct Reader_1_t41E7ACDBED757B1830FB93B6F8CBF75F959CD886  : public RuntimeObject
{
};

struct Reader_1_t41E7ACDBED757B1830FB93B6F8CBF75F959CD886_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t643EFCAEA913218C33F9E05144DAB3702712A18C* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<UnityEngine.Ray>>
struct Reader_1_t526D37680AAEFF5888DD8FAF5DE39A4018267FDB  : public RuntimeObject
{
};

struct Reader_1_t526D37680AAEFF5888DD8FAF5DE39A4018267FDB_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t4467783C00F409FFB8ED1FDCE87EF974AB372C4D* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<UnityEngine.Rect>>
struct Reader_1_t936C97C09526A419E67C5F81F13C53B05B727833  : public RuntimeObject
{
};

struct Reader_1_t936C97C09526A419E67C5F81F13C53B05B727833_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tCD175F70DBC690C8EF0231BF4AF1180C50BE25E0* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.SByte>>
struct Reader_1_t94C4EBCF7B54695CBEF81ACC847D9D01B8D34339  : public RuntimeObject
{
};

struct Reader_1_t94C4EBCF7B54695CBEF81ACC847D9D01B8D34339_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tFB31016B91F9F5C6B2CAC98095F7D025353DD033* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.Single>>
struct Reader_1_t0676D3BA8C95F6153F7DA06A84ECA06D85436585  : public RuntimeObject
{
};

struct Reader_1_t0676D3BA8C95F6153F7DA06A84ECA06D85436585_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t2A9651B5311D10F2FDA41A113A50E18A22D93138* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.UInt16>>
struct Reader_1_tADD3DCC9F400A548DD461E9BE16C5ACCBCA80B8E  : public RuntimeObject
{
};

struct Reader_1_tADD3DCC9F400A548DD461E9BE16C5ACCBCA80B8E_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t4842934F57735E601CC8B995D6E040EAF256FBF4* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.UInt32>>
struct Reader_1_t4C51943F9874BC9563E868F14B951239A3817126  : public RuntimeObject
{
};

struct Reader_1_t4C51943F9874BC9563E868F14B951239A3817126_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tE53CE7EE59BDF79AA9E78FAB9A62695508883396* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<System.UInt64>>
struct Reader_1_tFFD0A7FB0A8C37CF4374C67CB0DF9F2448CA9784  : public RuntimeObject
{
};

struct Reader_1_tFFD0A7FB0A8C37CF4374C67CB0DF9F2448CA9784_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t4B1235C83470DD7C3286754A4CFC840FBFD35741* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<UnityEngine.Vector2>>
struct Reader_1_tA9538DBB44C0A8B65EDF025A5D5FA611D152FA31  : public RuntimeObject
{
};

struct Reader_1_tA9538DBB44C0A8B65EDF025A5D5FA611D152FA31_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t603A0F2238D55FD8325BC0816D55C2970606BB81* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<UnityEngine.Vector2Int>>
struct Reader_1_t637274CA1AF284DD25208101A16558A23B6E67B5  : public RuntimeObject
{
};

struct Reader_1_t637274CA1AF284DD25208101A16558A23B6E67B5_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tE2F89B2F7AD6F1B1ED5373840C57D1603C193D7D* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<UnityEngine.Vector3>>
struct Reader_1_t59A60ECBC527BDCD550C648C4F309C4E55AE4AA7  : public RuntimeObject
{
};

struct Reader_1_t59A60ECBC527BDCD550C648C4F309C4E55AE4AA7_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t58F06EBEDB50273CFFCE1BED6156FF00537E7BF6* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<UnityEngine.Vector3Int>>
struct Reader_1_tAF89048D8EB67FD9887BFA7A88596629C237E4CA  : public RuntimeObject
{
};

struct Reader_1_tAF89048D8EB67FD9887BFA7A88596629C237E4CA_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tB67E11F122FE23A54C3DC5758CF7ABCED89CA2B9* ___read_0;
};

// Mirror.Reader`1<System.Nullable`1<UnityEngine.Vector4>>
struct Reader_1_t6757FC52418E33974D53365FEF9C7406320DED1A  : public RuntimeObject
{
};

struct Reader_1_t6757FC52418E33974D53365FEF9C7406320DED1A_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t3EF4F45A92179F162ABDFA4C40C59CEAB4DF8C95* ___read_0;
};

// Mirror.Reader`1<System.Byte[]>
struct Reader_1_tDF34563EB909B8D900E05A73EAE294A1BE35E704  : public RuntimeObject
{
};

struct Reader_1_tDF34563EB909B8D900E05A73EAE294A1BE35E704_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t68753017D74F6BABDA2E69D18EC2FA64AC20B03F* ___read_0;
};

// Mirror.Reader`1<Mirror.Examples.MultipleMatch.MatchInfo[]>
struct Reader_1_t9C76556BF9E8625EB258DEFF7A1656D33B4B236A  : public RuntimeObject
{
};

struct Reader_1_t9C76556BF9E8625EB258DEFF7A1656D33B4B236A_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tEAEC0CEBCFCC9157E40E1A7FFFEB82D8A082B205* ___read_0;
};

// Mirror.Reader`1<Mirror.Examples.MultipleMatch.PlayerInfo[]>
struct Reader_1_tB3752A50A44FC8C286392A80358AFF6F3CE44155  : public RuntimeObject
{
};

struct Reader_1_tB3752A50A44FC8C286392A80358AFF6F3CE44155_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t57B5F9E47ED4C8F5611A672DF5C949F2D02972C8* ___read_0;
};

// Mirror.Reader`1<Mirror.AddPlayerMessage>
struct Reader_1_t2FDE27056CF1FB6DE71EBDBFE9EA615E76F9B215  : public RuntimeObject
{
};

struct Reader_1_t2FDE27056CF1FB6DE71EBDBFE9EA615E76F9B215_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tC36B775425D425FC2AF1D16C0CD70386004FFEA0* ___read_0;
};

// Mirror.Reader`1<System.Boolean>
struct Reader_1_t942164B32720705E43130F8A5332C5B7AFF4FAE5  : public RuntimeObject
{
};

struct Reader_1_t942164B32720705E43130F8A5332C5B7AFF4FAE5_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tE35FCB04208B127B021F241EA079464ACC551B7C* ___read_0;
};

// Mirror.Reader`1<System.Byte>
struct Reader_1_t894EA749E730DD156D39D5D3C89DD01B1840FC90  : public RuntimeObject
{
};

struct Reader_1_t894EA749E730DD156D39D5D3C89DD01B1840FC90_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t0BC506BED5AC85C37292910C8BDAD52A9F6C9986* ___read_0;
};

// Mirror.Reader`1<Mirror.Examples.MultipleMatch.CellValue>
struct Reader_1_t85795B601B4F9B63136F7F928000E1996B634723  : public RuntimeObject
{
};

struct Reader_1_t85795B601B4F9B63136F7F928000E1996B634723_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t22170DBE4CAFFA15631BB8B0CE681A822A3A160A* ___read_0;
};

// Mirror.Reader`1<Mirror.ChangeOwnerMessage>
struct Reader_1_tB503DFFAE8032B3381FEE0779316357D1CE28658  : public RuntimeObject
{
};

struct Reader_1_tB503DFFAE8032B3381FEE0779316357D1CE28658_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tAF26F2C22D87F8F669162B6542990070635D2A30* ___read_0;
};

// Mirror.Reader`1<System.Char>
struct Reader_1_t8D947140B208E58AB2F0C97B38D53DDA61149A4A  : public RuntimeObject
{
};

struct Reader_1_t8D947140B208E58AB2F0C97B38D53DDA61149A4A_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t26F6F63A0DA84EBAD15F6BCC374071D9F6C8B42C* ___read_0;
};

// Mirror.Reader`1<Mirror.Examples.MultipleMatch.ClientMatchMessage>
struct Reader_1_t286A71B8B0F59FB5063D0E327C76169FAF8F45D6  : public RuntimeObject
{
};

struct Reader_1_t286A71B8B0F59FB5063D0E327C76169FAF8F45D6_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t21336B204733D902134B7F0FB71D1D4D85655F8E* ___read_0;
};

// Mirror.Reader`1<Mirror.Examples.MultipleMatch.ClientMatchOperation>
struct Reader_1_t23067DF0051C0D5F767FB367AFEEE3C196DC518C  : public RuntimeObject
{
};

struct Reader_1_t23067DF0051C0D5F767FB367AFEEE3C196DC518C_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tFBF2D2CE91617E529AC8196465432685D724AEBE* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Color>
struct Reader_1_tB18271683F45221F345D04D571881186F2B19615  : public RuntimeObject
{
};

struct Reader_1_tB18271683F45221F345D04D571881186F2B19615_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t6DF06C835E4A0B6416DE67676C67EDBE1EBBFD78* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Color32>
struct Reader_1_tA1A9ED8F30B3E4079D52171EBCE69E3D54C0E32A  : public RuntimeObject
{
};

struct Reader_1_tA1A9ED8F30B3E4079D52171EBCE69E3D54C0E32A_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tA1D462828AADE4C7A521611177F1A49476CFE773* ___read_0;
};

// Mirror.Reader`1<Mirror.CommandMessage>
struct Reader_1_t92A37B251D88CED6738CA5720BFA048525E94646  : public RuntimeObject
{
};

struct Reader_1_t92A37B251D88CED6738CA5720BFA048525E94646_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tDB9FD64DF7D2CA31FFF8279F26075DFCF440D33C* ___read_0;
};

// Mirror.Reader`1<System.Decimal>
struct Reader_1_tA05950C10C6D79E26FD7C70ACCED03FCC01F5FC0  : public RuntimeObject
{
};

struct Reader_1_tA05950C10C6D79E26FD7C70ACCED03FCC01F5FC0_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tB643983D6CBD6220B483FEE44F3207125D7D3726* ___read_0;
};

// Mirror.Reader`1<System.Double>
struct Reader_1_tF46320E57E205382BE23057439BFC8EA1F2457F0  : public RuntimeObject
{
};

struct Reader_1_tF46320E57E205382BE23057439BFC8EA1F2457F0_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t589ED36A1F9FA112DA0EA54F4A46CBAEE886A07A* ___read_0;
};

// Mirror.Reader`1<Mirror.EntityStateMessage>
struct Reader_1_tCF89C1D29B73DD90DA2BB475B19671460984C68D  : public RuntimeObject
{
};

struct Reader_1_tCF89C1D29B73DD90DA2BB475B19671460984C68D_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tB637CE13E86654599FE7556001525095E552A933* ___read_0;
};

// Mirror.Reader`1<UnityEngine.GameObject>
struct Reader_1_tE674AC89FB334BAAB07744B62BEB917F65466F8B  : public RuntimeObject
{
};

struct Reader_1_tE674AC89FB334BAAB07744B62BEB917F65466F8B_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t1E82FEEB6D88844359FA37AD374ACC1AA39DDC4A* ___read_0;
};

// Mirror.Reader`1<System.Guid>
struct Reader_1_t84A9F65298C551CBBA05DF543DBED0468412CCD3  : public RuntimeObject
{
};

struct Reader_1_t84A9F65298C551CBBA05DF543DBED0468412CCD3_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tEC73485627298AE849634B626697EFC64D213D43* ___read_0;
};

// Mirror.Reader`1<System.Int16>
struct Reader_1_t436C9D757F7667ABD9FB2D630506ED1ED29652AA  : public RuntimeObject
{
};

struct Reader_1_t436C9D757F7667ABD9FB2D630506ED1ED29652AA_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tEE61CDD83F7808585CB68357D75DC2637FBA492F* ___read_0;
};

// Mirror.Reader`1<System.Int32>
struct Reader_1_t259EB4559035BAE63FEDCB6F4C5B72D8CA00F6C7  : public RuntimeObject
{
};

struct Reader_1_t259EB4559035BAE63FEDCB6F4C5B72D8CA00F6C7_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t3F9C9CE2DD4A455F6BF2ACA2CBC6A00343B798EA* ___read_0;
};

// Mirror.Reader`1<System.Int64>
struct Reader_1_tF38A27591F97AE59155FCEDBC831BE975EBD3842  : public RuntimeObject
{
};

struct Reader_1_tF38A27591F97AE59155FCEDBC831BE975EBD3842_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t0D88BB68D7BF644024EA7DFB9E7CDBA1EE7EF69C* ___read_0;
};

// Mirror.Reader`1<Mirror.Examples.MultipleMatch.MatchInfo>
struct Reader_1_t9F020D67D02A1B6BF8EF51B03B6F298B7042AC26  : public RuntimeObject
{
};

struct Reader_1_t9F020D67D02A1B6BF8EF51B03B6F298B7042AC26_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t0D870FCF01A0B3EEF980A0B74CCA07765A7552B3* ___read_0;
};

// Mirror.Reader`1<Mirror.Examples.MultipleMatch.MatchPlayerData>
struct Reader_1_tFD0BF8A2A5368E4951ED7917611F5A25E4D46A7B  : public RuntimeObject
{
};

struct Reader_1_tFD0BF8A2A5368E4951ED7917611F5A25E4D46A7B_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t747EDA19750AE43DE30476D128A90ACDBEAC48AF* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Matrix4x4>
struct Reader_1_t4AF495607AC4510AD0529F12D6A0BB0418C352A7  : public RuntimeObject
{
};

struct Reader_1_t4AF495607AC4510AD0529F12D6A0BB0418C352A7_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tAAC77BA3AEC5902F635E82ED90239D6BCD31F70A* ___read_0;
};

// Mirror.Reader`1<Mirror.NetworkBehaviour>
struct Reader_1_t497575125F4FF1F1EEB32CB0C8FF12569655666B  : public RuntimeObject
{
};

struct Reader_1_t497575125F4FF1F1EEB32CB0C8FF12569655666B_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t763657E22AECC9ED46856683B3045624CF6351E6* ___read_0;
};

// Mirror.Reader`1<Mirror.NetworkIdentity>
struct Reader_1_t2C531D10760B34519D87218F56AE3866A9C6B924  : public RuntimeObject
{
};

struct Reader_1_t2C531D10760B34519D87218F56AE3866A9C6B924_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t8D8F451E2C46B99D55F5781D36B4F981BE62BD6A* ___read_0;
};

// Mirror.Reader`1<Mirror.NetworkPingMessage>
struct Reader_1_tF4E8B721D496EDF9F9FFF783C41F20BF05DE9F34  : public RuntimeObject
{
};

struct Reader_1_tF4E8B721D496EDF9F9FFF783C41F20BF05DE9F34_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t3C7B620CCBA5E47EED93AC22DB224F015928D9BF* ___read_0;
};

// Mirror.Reader`1<Mirror.NetworkPongMessage>
struct Reader_1_tDC04CC49F13C042AA2FC9CEA968B413D1B6F3F38  : public RuntimeObject
{
};

struct Reader_1_tDC04CC49F13C042AA2FC9CEA968B413D1B6F3F38_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t57653632E080A4623E41FABED4B1B6707C4D1B44* ___read_0;
};

// Mirror.Reader`1<Mirror.NotReadyMessage>
struct Reader_1_t4BEAA8B6A674DE41A4F74F8F9F7628DAEB444E28  : public RuntimeObject
{
};

struct Reader_1_t4BEAA8B6A674DE41A4F74F8F9F7628DAEB444E28_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t9B160232C1403CEF10F27D1D1BE4368650EE22CD* ___read_0;
};

// Mirror.Reader`1<Mirror.ObjectDestroyMessage>
struct Reader_1_tCB389B7DC3DE02BC9C35A7B57D2B65F26E3A7C60  : public RuntimeObject
{
};

struct Reader_1_tCB389B7DC3DE02BC9C35A7B57D2B65F26E3A7C60_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t056E7181DAB40DF2A9D14BB4450C0DCC566784E6* ___read_0;
};

// Mirror.Reader`1<Mirror.ObjectHideMessage>
struct Reader_1_t28D5381047606A72C4A5B7E019C0F77FBCEE1C15  : public RuntimeObject
{
};

struct Reader_1_t28D5381047606A72C4A5B7E019C0F77FBCEE1C15_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t76221365B0738498867CB728129555B7A8617C15* ___read_0;
};

// Mirror.Reader`1<Mirror.ObjectSpawnFinishedMessage>
struct Reader_1_t6B6A1FCF933F958098FE25A100627E7241FEB588  : public RuntimeObject
{
};

struct Reader_1_t6B6A1FCF933F958098FE25A100627E7241FEB588_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tAE6F8E9F1706B23DD7BC49D738569171CF6E8C3B* ___read_0;
};

// Mirror.Reader`1<Mirror.ObjectSpawnStartedMessage>
struct Reader_1_t5072BFCCD801A3F27F8B16872FF18C70016F2CC0  : public RuntimeObject
{
};

struct Reader_1_t5072BFCCD801A3F27F8B16872FF18C70016F2CC0_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tBD4C61172F81BE2A292FAE166832A0324BB00B1B* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Plane>
struct Reader_1_t9999666C4A090A6C9AD43987D46A3326989F2E39  : public RuntimeObject
{
};

struct Reader_1_t9999666C4A090A6C9AD43987D46A3326989F2E39_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tF1646D6248B9372FDA9C49F858144441B68F98FD* ___read_0;
};

// Mirror.Reader`1<Mirror.Examples.MultipleMatch.PlayerInfo>
struct Reader_1_t9F0D38236764D927F7809A9EF55BC5E04C385A80  : public RuntimeObject
{
};

struct Reader_1_t9F0D38236764D927F7809A9EF55BC5E04C385A80_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t966D11E60D81CE62911BC8F172BA2326E5F49FE0* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Quaternion>
struct Reader_1_tF4BD915B07DF8D3A5EB55BCAAD572DEE9ECE8FB5  : public RuntimeObject
{
};

struct Reader_1_tF4BD915B07DF8D3A5EB55BCAAD572DEE9ECE8FB5_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tDF2B4816BD9F902CA19B4AFA6CA78C2A61867EA3* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Ray>
struct Reader_1_t88B56AF4E9141686F1DE1F3D155CB3FBA34B913D  : public RuntimeObject
{
};

struct Reader_1_t88B56AF4E9141686F1DE1F3D155CB3FBA34B913D_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tB124603C647DAC5696CA392A20B3EE6C213CCA26* ___read_0;
};

// Mirror.Reader`1<Mirror.ReadyMessage>
struct Reader_1_t00DA01ACAF536E0AF78C6E8BC015679E9934DB88  : public RuntimeObject
{
};

struct Reader_1_t00DA01ACAF536E0AF78C6E8BC015679E9934DB88_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t31743D3E7DDCFE0001801AD8A7459B2FE016E401* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Rect>
struct Reader_1_t6B88F366A149F8A58032F93153EF0A2DFC60E74F  : public RuntimeObject
{
};

struct Reader_1_t6B88F366A149F8A58032F93153EF0A2DFC60E74F_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t340EA86AEF734F68ACF5E9FF2A124F9ADDED52DE* ___read_0;
};

// Mirror.Reader`1<Mirror.RpcMessage>
struct Reader_1_t33986420F424835F9DB93EEB52C71E46AB50C09F  : public RuntimeObject
{
};

struct Reader_1_t33986420F424835F9DB93EEB52C71E46AB50C09F_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t5F7A34ACC37197D2AFFFFF3A63EE51E2B44D6607* ___read_0;
};

// Mirror.Reader`1<System.SByte>
struct Reader_1_t9773E186C54234048A015DDEBB899F90C35E96B0  : public RuntimeObject
{
};

struct Reader_1_t9773E186C54234048A015DDEBB899F90C35E96B0_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t3EE5A9E53DE82B19DAC86C3D30AC7BD98D305238* ___read_0;
};

// Mirror.Reader`1<Mirror.SceneMessage>
struct Reader_1_t4135AEE9236AC48C3B0D8C260D3A2DC89E212394  : public RuntimeObject
{
};

struct Reader_1_t4135AEE9236AC48C3B0D8C260D3A2DC89E212394_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t8C43EB67A953E2DA4EA0F861438FEF6C6F87360E* ___read_0;
};

// Mirror.Reader`1<Mirror.SceneOperation>
struct Reader_1_t6FC28304AD44844C4A3BFD56F3139BA2B74B01FF  : public RuntimeObject
{
};

struct Reader_1_t6FC28304AD44844C4A3BFD56F3139BA2B74B01FF_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tA736A0330DE6964C91228A379FAB411414BBBB31* ___read_0;
};

// Mirror.Reader`1<Mirror.Examples.MultipleMatch.ServerMatchMessage>
struct Reader_1_tF19F6E8C291C912786327989D4FD0124DF26DB09  : public RuntimeObject
{
};

struct Reader_1_tF19F6E8C291C912786327989D4FD0124DF26DB09_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t2EFEEFDAE61B1D665B02E8D247FA7A8AE1B3D3FF* ___read_0;
};

// Mirror.Reader`1<Mirror.Examples.MultipleMatch.ServerMatchOperation>
struct Reader_1_t80077764BF20B75B1B8B47491E64EF0F64D019DF  : public RuntimeObject
{
};

struct Reader_1_t80077764BF20B75B1B8B47491E64EF0F64D019DF_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t4A60C2EC0E8ECFA6E6872E11BFAD744A805D5FF8* ___read_0;
};

// Mirror.Reader`1<System.Single>
struct Reader_1_t7D07B6E13CF9AA9927D4FD2385DB61464F9B5FE4  : public RuntimeObject
{
};

struct Reader_1_t7D07B6E13CF9AA9927D4FD2385DB61464F9B5FE4_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tDA855C737BB7E9546B48245EC0F04DAA56A1CB03* ___read_0;
};

// Mirror.Reader`1<Mirror.SpawnMessage>
struct Reader_1_tBE1B76CC86F1EDCE1B7A3203682971C129964518  : public RuntimeObject
{
};

struct Reader_1_tBE1B76CC86F1EDCE1B7A3203682971C129964518_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t1FE35EF0C4BBDC7AFA697EBBB5B4D64547326DCC* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Sprite>
struct Reader_1_t65AE09028DBFFFB995B18BC7147A9A651D389384  : public RuntimeObject
{
};

struct Reader_1_t65AE09028DBFFFB995B18BC7147A9A651D389384_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tB35F19D43C3D74BFE4F16B609D5DAFBCCE477EC1* ___read_0;
};

// Mirror.Reader`1<System.String>
struct Reader_1_t822A5642F255AE3ACC0D29BCD12B45F8C7A6EBAE  : public RuntimeObject
{
};

struct Reader_1_t822A5642F255AE3ACC0D29BCD12B45F8C7A6EBAE_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tF047810C662C3A551DDB01290047E803F32DA440* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Texture2D>
struct Reader_1_t8CFA4E86EAB3C0AE3848A8BC147CB1A393CB3B50  : public RuntimeObject
{
};

struct Reader_1_t8CFA4E86EAB3C0AE3848A8BC147CB1A393CB3B50_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t58133EDD30520660CD4F542594E8D913BB704B55* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Transform>
struct Reader_1_t5AAF2D6763D2C4497F18AC3DE6C04568FBA25219  : public RuntimeObject
{
};

struct Reader_1_t5AAF2D6763D2C4497F18AC3DE6C04568FBA25219_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t8085A3B2562300C528C41159E557B58E555D6798* ___read_0;
};

// Mirror.Reader`1<System.UInt16>
struct Reader_1_t198B364FF80EB504EC1C04BA6BDB4431FCAC173E  : public RuntimeObject
{
};

struct Reader_1_t198B364FF80EB504EC1C04BA6BDB4431FCAC173E_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tD28C61E3A0A47F71DB0CDCB3B43C7F13D4F82235* ___read_0;
};

// Mirror.Reader`1<System.UInt32>
struct Reader_1_t3E733A5CBD67C2A9860A6535DC31C53DC3294C05  : public RuntimeObject
{
};

struct Reader_1_t3E733A5CBD67C2A9860A6535DC31C53DC3294C05_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t1FE4698618B38731C4E2D67BCC314778BA13ECC0* ___read_0;
};

// Mirror.Reader`1<System.UInt64>
struct Reader_1_t99038D5B7B0CB7EA48F6A69A3DA42DDF77732757  : public RuntimeObject
{
};

struct Reader_1_t99038D5B7B0CB7EA48F6A69A3DA42DDF77732757_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t86251E51D7B41B00F1CC89068612DB2EE6F5FDE0* ___read_0;
};

// Mirror.Reader`1<System.Uri>
struct Reader_1_t1EC17F900AFBDD0AA0263D3A62B0630B495FE255  : public RuntimeObject
{
};

struct Reader_1_t1EC17F900AFBDD0AA0263D3A62B0630B495FE255_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t2DBAA86C74F086DBA285C7B8ECE41C9F35E419D4* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Vector2>
struct Reader_1_t7A28D876B4CE12F3E4C96FA0EDF6E7175F65C810  : public RuntimeObject
{
};

struct Reader_1_t7A28D876B4CE12F3E4C96FA0EDF6E7175F65C810_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t9C4AED5DAF5E665602E3D752BDF50BAE7C2C5D0F* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Vector2Int>
struct Reader_1_t39F933887B1FE66BE8404A5FAC7840D47C4CE2F2  : public RuntimeObject
{
};

struct Reader_1_t39F933887B1FE66BE8404A5FAC7840D47C4CE2F2_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t1D54F05297BE0A4ACFBE3D87381C38332A2F7837* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Vector3>
struct Reader_1_tA296681D381BFB2393CD36BF8D1737E846F15634  : public RuntimeObject
{
};

struct Reader_1_tA296681D381BFB2393CD36BF8D1737E846F15634_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t9BD17C9037D00DFA5EFF1643A78C3E4300A31331* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Vector3Int>
struct Reader_1_tACF841391431A7BEF83CACCF4CAF12E6601B5A00  : public RuntimeObject
{
};

struct Reader_1_tACF841391431A7BEF83CACCF4CAF12E6601B5A00_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tFD61C39BCF897AD7E0777243349B4558DB639EC5* ___read_0;
};

// Mirror.Reader`1<UnityEngine.Vector4>
struct Reader_1_tFBA7DC5EFAB4785CF16735020B26CD15760FF936  : public RuntimeObject
{
};

struct Reader_1_tFBA7DC5EFAB4785CF16735020B26CD15760FF936_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_tEA21835A50BEE04419A104B2F4D8CCE015122924* ___read_0;
};

// Mirror.Reader`1<Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage>
struct Reader_1_t429249FC13D808B0B1401B5B6BD0815F37C082B8  : public RuntimeObject
{
};

struct Reader_1_t429249FC13D808B0B1401B5B6BD0815F37C082B8_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t4CD599531AE1FA8CA3CF4057D910B500DBF99E45* ___read_0;
};

// Mirror.Reader`1<Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage>
struct Reader_1_t2A59AD3448B11193AB518EAC5FFE24BA1ED83727  : public RuntimeObject
{
};

struct Reader_1_t2A59AD3448B11193AB518EAC5FFE24BA1ED83727_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t5DF2F494B8936184EE2E2F51E135F530C09B76D0* ___read_0;
};

// Mirror.Reader`1<Mirror.NetworkBehaviour/NetworkBehaviourSyncVar>
struct Reader_1_tA5BF854AFBD00B4F60644779FA6DEB2DC22F7F61  : public RuntimeObject
{
};

struct Reader_1_tA5BF854AFBD00B4F60644779FA6DEB2DC22F7F61_StaticFields
{
	// System.Func`2<Mirror.NetworkReader,T> Mirror.Reader`1::read
	Func_2_t842043449CFBC0F4C127867ED6917B5566AC5576* ___read_0;
};

// Mirror.Writer`1<System.ArraySegment`1<System.Byte>>
struct Writer_1_t02378C060D52C54C236D7DC3C612BD8B0B2153AE  : public RuntimeObject
{
};

struct Writer_1_t02378C060D52C54C236D7DC3C612BD8B0B2153AE_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tE7DE89222EAD5CBE5FBA94711CB3C71222202DFC* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.Boolean>>
struct Writer_1_t1D6C09BFBA988FC86B2ABBD261ECF76022F5691B  : public RuntimeObject
{
};

struct Writer_1_t1D6C09BFBA988FC86B2ABBD261ECF76022F5691B_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tFAFB5B1636A32CF85A452A078736260BB496C253* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.Byte>>
struct Writer_1_t44DF034032ED1B1CEEAF5EAF3332EDE362ED947D  : public RuntimeObject
{
};

struct Writer_1_t44DF034032ED1B1CEEAF5EAF3332EDE362ED947D_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t104D6887DC3FA703E6D350257E4EE72A3EFCF3A3* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.Char>>
struct Writer_1_t4C15B820BC518F2CE261CF46CCD5C0763E35D445  : public RuntimeObject
{
};

struct Writer_1_t4C15B820BC518F2CE261CF46CCD5C0763E35D445_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t1B2365362928779807EDAEE12F27010A7A8D39C6* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<UnityEngine.Color>>
struct Writer_1_t86A60D54965CEA1D0E31A7BF6611C9E4EA2A7B87  : public RuntimeObject
{
};

struct Writer_1_t86A60D54965CEA1D0E31A7BF6611C9E4EA2A7B87_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t1AB30C8E5E10864F469D7062D99016352CC3602F* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<UnityEngine.Color32>>
struct Writer_1_t633DC4AA7F39C88128E15553F3C582C26A375D17  : public RuntimeObject
{
};

struct Writer_1_t633DC4AA7F39C88128E15553F3C582C26A375D17_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t284D0A27595992EAC03994E6C1A1E82225441B67* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.Decimal>>
struct Writer_1_tCF7983600918F8F12E621BA83FD47D9F458B363A  : public RuntimeObject
{
};

struct Writer_1_tCF7983600918F8F12E621BA83FD47D9F458B363A_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t3913958E2393CE5F44A661B2F0835A6FC4513CBE* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.Double>>
struct Writer_1_t4CC6F64E17DBF07E25DEBC8F9A2FC129D5E21F6B  : public RuntimeObject
{
};

struct Writer_1_t4CC6F64E17DBF07E25DEBC8F9A2FC129D5E21F6B_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tD872DFCCB454D89FE5878BA1B3A03AA4FF2F7A72* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.Guid>>
struct Writer_1_tCA15166526F2EA72615546B0168BD1A9B7067C0B  : public RuntimeObject
{
};

struct Writer_1_tCA15166526F2EA72615546B0168BD1A9B7067C0B_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t00EE65E013F88779EEEE98835E86BDF400A3BF36* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.Int16>>
struct Writer_1_tC35EE38E41AE63A92291A26CB18FBF2D36A07896  : public RuntimeObject
{
};

struct Writer_1_tC35EE38E41AE63A92291A26CB18FBF2D36A07896_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tE28F1C5549DD431F6354E325F28C3FA842F94E57* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.Int32>>
struct Writer_1_tED1C9085F349458B13C36D062822203E85AF457B  : public RuntimeObject
{
};

struct Writer_1_tED1C9085F349458B13C36D062822203E85AF457B_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tD938883EC204367D5A295A7CB2BB5D4D20A90029* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.Int64>>
struct Writer_1_t0BA0F75C0F12434151A45FAC15BD8A05CCF138C9  : public RuntimeObject
{
};

struct Writer_1_t0BA0F75C0F12434151A45FAC15BD8A05CCF138C9_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tDF176CFB33B35A021E5DA20E9485F6A452F2C345* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<UnityEngine.Matrix4x4>>
struct Writer_1_t7D22F5C863050135C73CAB5F962B1CB2C9DD592F  : public RuntimeObject
{
};

struct Writer_1_t7D22F5C863050135C73CAB5F962B1CB2C9DD592F_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tB31DAF5F45A869010A4A3D1EA9EA5C56DCFAB947* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<UnityEngine.Plane>>
struct Writer_1_tD745BB380F67A854010D62CF4FE8179D1D9A0FFF  : public RuntimeObject
{
};

struct Writer_1_tD745BB380F67A854010D62CF4FE8179D1D9A0FFF_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t9BBCAC312FAACBDD9B08E82065F0C66CB92596DD* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<UnityEngine.Quaternion>>
struct Writer_1_t23D4BACC405E3FA13539AE7E93E94435ED73C740  : public RuntimeObject
{
};

struct Writer_1_t23D4BACC405E3FA13539AE7E93E94435ED73C740_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tFAE875C9728E772ACF44D3BCF5F2B94860FB1CDE* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<UnityEngine.Ray>>
struct Writer_1_t3A7BC19F51D39AA83BAC5C1DEC028E0AE21067DA  : public RuntimeObject
{
};

struct Writer_1_t3A7BC19F51D39AA83BAC5C1DEC028E0AE21067DA_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tBE61BBF1961F6628AEE63F058E11FEB22FB6A6ED* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<UnityEngine.Rect>>
struct Writer_1_t918CD45EC571EF06C238F361375BF18469663559  : public RuntimeObject
{
};

struct Writer_1_t918CD45EC571EF06C238F361375BF18469663559_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tD39051A11B2175464C227775F3F6FD44715C0767* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.SByte>>
struct Writer_1_tACC5A4F72D8EB16086B351E1784F11620EED13F0  : public RuntimeObject
{
};

struct Writer_1_tACC5A4F72D8EB16086B351E1784F11620EED13F0_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tEE379855D79D043D165FD05C3786F7B0ADB89B38* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.Single>>
struct Writer_1_tAB097320742DBC1A8B8DF7BFB4F762C902C20077  : public RuntimeObject
{
};

struct Writer_1_tAB097320742DBC1A8B8DF7BFB4F762C902C20077_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tD01A9F5ED6BB951B86BE216B2B8DE2C17FFAE551* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.UInt16>>
struct Writer_1_tAFAB4C7529C18C2FED4E4A6D66D55F4EB4CA10CE  : public RuntimeObject
{
};

struct Writer_1_tAFAB4C7529C18C2FED4E4A6D66D55F4EB4CA10CE_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tDD6B83C4A6E2ECFC616A05B485C0D7E24A750E90* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.UInt32>>
struct Writer_1_tA63F5424DDE9B2587D7E34744054819D40E1EF86  : public RuntimeObject
{
};

struct Writer_1_tA63F5424DDE9B2587D7E34744054819D40E1EF86_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tC9DC1064C14B7AF886718CB747591EC132E1E301* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<System.UInt64>>
struct Writer_1_t243E336527949636CD7438A1DBBBDA88A829C4CC  : public RuntimeObject
{
};

struct Writer_1_t243E336527949636CD7438A1DBBBDA88A829C4CC_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t9E34BCADC1F9EF87E3BC9817BF9ABF3086286388* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<UnityEngine.Vector2>>
struct Writer_1_tF902FB09B54D9973B6DE4667D18385E6F351DE14  : public RuntimeObject
{
};

struct Writer_1_tF902FB09B54D9973B6DE4667D18385E6F351DE14_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t06CF1E09D2E968AE317C1B4066FE59281D0D7F35* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<UnityEngine.Vector2Int>>
struct Writer_1_tD572915EE39BA9844F8AB175F9C7BE4C23CA9B97  : public RuntimeObject
{
};

struct Writer_1_tD572915EE39BA9844F8AB175F9C7BE4C23CA9B97_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tF8B877CE67C7AF23C81D6C5C15EB3D2FE44321A0* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<UnityEngine.Vector3>>
struct Writer_1_tF790A3D1EF8C3E9D9DC8802687D0B51121BA2551  : public RuntimeObject
{
};

struct Writer_1_tF790A3D1EF8C3E9D9DC8802687D0B51121BA2551_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t3D709A9DD3DD60B2669C03CAF9A9607B98F0CC44* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<UnityEngine.Vector3Int>>
struct Writer_1_tECEB6A5A5CE5B7325CE0ED4C1A86ED28CC960D40  : public RuntimeObject
{
};

struct Writer_1_tECEB6A5A5CE5B7325CE0ED4C1A86ED28CC960D40_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tCF4F398AAC858F0B69CF49EA4DB6878F5E85A2FE* ___write_0;
};

// Mirror.Writer`1<System.Nullable`1<UnityEngine.Vector4>>
struct Writer_1_t271DE44B9F6C5A28AB5ADF72AEDCAA3628BABB90  : public RuntimeObject
{
};

struct Writer_1_t271DE44B9F6C5A28AB5ADF72AEDCAA3628BABB90_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t437C51FCE7D4E11D08E66AC6884ADD843A29305E* ___write_0;
};

// Mirror.Writer`1<System.Byte[]>
struct Writer_1_tA7C0E92EDDBAEE98892D2BFC9229C66F5D6C0D50  : public RuntimeObject
{
};

struct Writer_1_tA7C0E92EDDBAEE98892D2BFC9229C66F5D6C0D50_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t6CB11000550B779DE6F0A9873A4FA47684E8A996* ___write_0;
};

// Mirror.Writer`1<Mirror.Examples.MultipleMatch.MatchInfo[]>
struct Writer_1_t6BDF566A850D915F8607498B2A74D90E6AD3DFE4  : public RuntimeObject
{
};

struct Writer_1_t6BDF566A850D915F8607498B2A74D90E6AD3DFE4_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t77285E9313B73F391F0816DD5738EBF05F8774AD* ___write_0;
};

// Mirror.Writer`1<Mirror.Examples.MultipleMatch.PlayerInfo[]>
struct Writer_1_tDB03F8AA9D2A369BF9554E72C2BAF71BECCC4D81  : public RuntimeObject
{
};

struct Writer_1_tDB03F8AA9D2A369BF9554E72C2BAF71BECCC4D81_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t5199EDEE1233BFABE45CDA21E9230C8346645716* ___write_0;
};

// Mirror.Writer`1<Mirror.AddPlayerMessage>
struct Writer_1_tE36053BA0B28C83CF0A86F8398DABFC7F10A4B88  : public RuntimeObject
{
};

struct Writer_1_tE36053BA0B28C83CF0A86F8398DABFC7F10A4B88_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t98800A090710915B3E861E237B1A9B6E9F29163F* ___write_0;
};

// Mirror.Writer`1<System.Boolean>
struct Writer_1_t4D548019E9743B81B59DEF8D2478BEACFDC3CE09  : public RuntimeObject
{
};

struct Writer_1_t4D548019E9743B81B59DEF8D2478BEACFDC3CE09_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t771EB50B64E2F517312250DEC5230DC62C7E36FD* ___write_0;
};

// Mirror.Writer`1<System.Byte>
struct Writer_1_t6C6CC5FCDF55B79BC00FD028E0A44F85A45210CD  : public RuntimeObject
{
};

struct Writer_1_t6C6CC5FCDF55B79BC00FD028E0A44F85A45210CD_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tDDEAD4B308FD0F5A9299458AF5A6B47CA5D3732F* ___write_0;
};

// Mirror.Writer`1<Mirror.Examples.MultipleMatch.CellValue>
struct Writer_1_tADD2D185EF8CADFAA813221CFBDB1706512C378A  : public RuntimeObject
{
};

struct Writer_1_tADD2D185EF8CADFAA813221CFBDB1706512C378A_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t117A07FAD93D252BC5E69F05373B4E286F1F8D56* ___write_0;
};

// Mirror.Writer`1<Mirror.ChangeOwnerMessage>
struct Writer_1_t03D9486B5C228414D227AD0F4B1C32582EF57381  : public RuntimeObject
{
};

struct Writer_1_t03D9486B5C228414D227AD0F4B1C32582EF57381_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tA6272A2BBF53770CE719C794145D53E55F0DCC51* ___write_0;
};

// Mirror.Writer`1<System.Char>
struct Writer_1_t28DF10100EA6DD2804A3081EC57C156DE5808B4F  : public RuntimeObject
{
};

struct Writer_1_t28DF10100EA6DD2804A3081EC57C156DE5808B4F_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t3C170C2BFCCAA591C6DA755A86E3FEDBB4428D90* ___write_0;
};

// Mirror.Writer`1<Mirror.Examples.MultipleMatch.ClientMatchMessage>
struct Writer_1_tB611FFB23F108BACF0D008A5913B87BDA0B78161  : public RuntimeObject
{
};

struct Writer_1_tB611FFB23F108BACF0D008A5913B87BDA0B78161_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tCFCEE592405B35148F5E1AC506FEB4F8CB2F49B6* ___write_0;
};

// Mirror.Writer`1<Mirror.Examples.MultipleMatch.ClientMatchOperation>
struct Writer_1_t5B4A56E0557BF93D873BD64F49D7D7EB2FFAC769  : public RuntimeObject
{
};

struct Writer_1_t5B4A56E0557BF93D873BD64F49D7D7EB2FFAC769_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tE631CB825D55269F80774B2F55C5E62192891CF8* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Color>
struct Writer_1_tFB29484EAFF97FE2C50FDB48351489533752921F  : public RuntimeObject
{
};

struct Writer_1_tFB29484EAFF97FE2C50FDB48351489533752921F_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t993E6168F76ADC356CC943B6992CDD55115F3057* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Color32>
struct Writer_1_t110A7C2C82FDE2873D9B37CDBE36F7F5D9B42E86  : public RuntimeObject
{
};

struct Writer_1_t110A7C2C82FDE2873D9B37CDBE36F7F5D9B42E86_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t27076E67A79536591E04C63FD1582DB7D564BA08* ___write_0;
};

// Mirror.Writer`1<Mirror.CommandMessage>
struct Writer_1_tD6CDFC673E86CE3DA4B9A902DFE96553C7C89B49  : public RuntimeObject
{
};

struct Writer_1_tD6CDFC673E86CE3DA4B9A902DFE96553C7C89B49_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t8863334817D651A8E3B8E434C682CA5A76CE9498* ___write_0;
};

// Mirror.Writer`1<System.Decimal>
struct Writer_1_t8390A5CF97A52E052D8612D70A087CFAF6A64170  : public RuntimeObject
{
};

struct Writer_1_t8390A5CF97A52E052D8612D70A087CFAF6A64170_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tC4556EDF997707D306A86E36FC8EE3751F9C687C* ___write_0;
};

// Mirror.Writer`1<System.Double>
struct Writer_1_t4BDE3F806C4CC59448CAF1EE19A59A5CFEAD58F7  : public RuntimeObject
{
};

struct Writer_1_t4BDE3F806C4CC59448CAF1EE19A59A5CFEAD58F7_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tA70CFBAAF2BDFBA2BEE4003F4C2E6E5B12924D8E* ___write_0;
};

// Mirror.Writer`1<Mirror.EntityStateMessage>
struct Writer_1_t43CDE8B30140776D24809288A7A734939B0C8392  : public RuntimeObject
{
};

struct Writer_1_t43CDE8B30140776D24809288A7A734939B0C8392_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t92D149BA0BB9E6C54B7036167F4A14DFF1B5BE08* ___write_0;
};

// Mirror.Writer`1<UnityEngine.GameObject>
struct Writer_1_t89600A4E62D36536DC444EAF231664989DD702F3  : public RuntimeObject
{
};

struct Writer_1_t89600A4E62D36536DC444EAF231664989DD702F3_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t7A7E74A9B38640A94D5CC807EC76F706FB6693C0* ___write_0;
};

// Mirror.Writer`1<System.Guid>
struct Writer_1_t5EA9A1C053345BA806F8721312945B9DF2E1FFA3  : public RuntimeObject
{
};

struct Writer_1_t5EA9A1C053345BA806F8721312945B9DF2E1FFA3_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tCB8F678502AD5BFA52EE6F149351DAC3037677AF* ___write_0;
};

// Mirror.Writer`1<System.Int16>
struct Writer_1_tF86673563FB7B0CBA7838723701DABAE01570F38  : public RuntimeObject
{
};

struct Writer_1_tF86673563FB7B0CBA7838723701DABAE01570F38_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tED2F1B32CB312EA2AFE3FF65E3EBD21ADF00A34C* ___write_0;
};

// Mirror.Writer`1<System.Int32>
struct Writer_1_tAFDA6C5A95301BCFC4E49DC4B9E31CE5E13B51A3  : public RuntimeObject
{
};

struct Writer_1_tAFDA6C5A95301BCFC4E49DC4B9E31CE5E13B51A3_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t4D3AA2594C46CEF975DCD32BFF9A0FEE380568B3* ___write_0;
};

// Mirror.Writer`1<System.Int64>
struct Writer_1_t3D2702EC8F6235254020DB789887C072FDDFECAA  : public RuntimeObject
{
};

struct Writer_1_t3D2702EC8F6235254020DB789887C072FDDFECAA_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tE79E56090404F1ED684677C80F34095664D0010D* ___write_0;
};

// Mirror.Writer`1<Mirror.Examples.MultipleMatch.MatchInfo>
struct Writer_1_tE837CDE21BF382361B1ED24E4C34DC6B7B9C65E0  : public RuntimeObject
{
};

struct Writer_1_tE837CDE21BF382361B1ED24E4C34DC6B7B9C65E0_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t1D8E4389A2FC4D611519FC7C996BF96F763E5017* ___write_0;
};

// Mirror.Writer`1<Mirror.Examples.MultipleMatch.MatchPlayerData>
struct Writer_1_t76BD90233C405D75BFDE795DB7AADBF14659D95A  : public RuntimeObject
{
};

struct Writer_1_t76BD90233C405D75BFDE795DB7AADBF14659D95A_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tB478A835A6DC624AEA7C01C383390CE10F4D2BF1* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Matrix4x4>
struct Writer_1_t0C51959312818298C3E0C1C9872771D904AD5062  : public RuntimeObject
{
};

struct Writer_1_t0C51959312818298C3E0C1C9872771D904AD5062_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tD12F3221DBF89BF11C6CB2855E9BDBD70D6A8AB9* ___write_0;
};

// Mirror.Writer`1<Mirror.NetworkBehaviour>
struct Writer_1_t79E192D8556A9F0A0CFF931EEF19A382A96A85B3  : public RuntimeObject
{
};

struct Writer_1_t79E192D8556A9F0A0CFF931EEF19A382A96A85B3_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tC5312D54454D5411CB706A055837C90CBD0FEC12* ___write_0;
};

// Mirror.Writer`1<Mirror.NetworkIdentity>
struct Writer_1_tA5144EF44424ACEA981DF61078CF47C5A0E75FA4  : public RuntimeObject
{
};

struct Writer_1_tA5144EF44424ACEA981DF61078CF47C5A0E75FA4_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t51E55DFCEC17AF21DB946B2527D3669CD8E09542* ___write_0;
};

// Mirror.Writer`1<Mirror.NetworkPingMessage>
struct Writer_1_t88385B356F11A0E27B1FA1BB321C08F75AE44C4D  : public RuntimeObject
{
};

struct Writer_1_t88385B356F11A0E27B1FA1BB321C08F75AE44C4D_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t427A3AAB52F4338A54E05C0F9CCE238604B75463* ___write_0;
};

// Mirror.Writer`1<Mirror.NetworkPongMessage>
struct Writer_1_t279D7AD1213DC4E446FDB2E12DD927F5544311B9  : public RuntimeObject
{
};

struct Writer_1_t279D7AD1213DC4E446FDB2E12DD927F5544311B9_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t19A08A9BB7D482718CD3474975A6559CA159FA9F* ___write_0;
};

// Mirror.Writer`1<Mirror.NotReadyMessage>
struct Writer_1_tDD7CE7C0539A0A26F84206A36C8A80400C6E622E  : public RuntimeObject
{
};

struct Writer_1_tDD7CE7C0539A0A26F84206A36C8A80400C6E622E_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tE1C508C7DB184AC769DA763897EA5CE208D96BE9* ___write_0;
};

// Mirror.Writer`1<Mirror.ObjectDestroyMessage>
struct Writer_1_t864220A64B0EDB912027564DF90AE901084A093D  : public RuntimeObject
{
};

struct Writer_1_t864220A64B0EDB912027564DF90AE901084A093D_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t4125FEC2DD5C797C8E273617CF2C3E2ABB98FA45* ___write_0;
};

// Mirror.Writer`1<Mirror.ObjectHideMessage>
struct Writer_1_t190700FB2EFCFE964C8FDD64548F48100C8B992B  : public RuntimeObject
{
};

struct Writer_1_t190700FB2EFCFE964C8FDD64548F48100C8B992B_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tB5D2418D8D5BB1CA27E37232842C681B92ABDB9B* ___write_0;
};

// Mirror.Writer`1<Mirror.ObjectSpawnFinishedMessage>
struct Writer_1_tDC62F016728AC1B9962FAB7BA7DB68E48CECC6A9  : public RuntimeObject
{
};

struct Writer_1_tDC62F016728AC1B9962FAB7BA7DB68E48CECC6A9_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t88B940D5126F22A0E314062C9CB9B8CF14430308* ___write_0;
};

// Mirror.Writer`1<Mirror.ObjectSpawnStartedMessage>
struct Writer_1_t21E999198EB9EDBDB5B29C76134D9493E50B5775  : public RuntimeObject
{
};

struct Writer_1_t21E999198EB9EDBDB5B29C76134D9493E50B5775_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t932E36A723FFA4E592D80F6F8AE785A34669DA75* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Plane>
struct Writer_1_t978255B2785507C424403C582ECD6DC09003148E  : public RuntimeObject
{
};

struct Writer_1_t978255B2785507C424403C582ECD6DC09003148E_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tF808E86BBEE3D1817CE8A0C2E299320C9DD9D1BA* ___write_0;
};

// Mirror.Writer`1<Mirror.Examples.MultipleMatch.PlayerInfo>
struct Writer_1_t080683EBC9B7A30F6121E3179DCB7370B1E8F62A  : public RuntimeObject
{
};

struct Writer_1_t080683EBC9B7A30F6121E3179DCB7370B1E8F62A_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t15EAEA1713B2DE57B0F7BFED86F3CF5915138314* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Quaternion>
struct Writer_1_t5E74ADF66E1728C9F5589CB7743629820364AF71  : public RuntimeObject
{
};

struct Writer_1_t5E74ADF66E1728C9F5589CB7743629820364AF71_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tD28B743D21DCE5F5BE026041A86D0F58543D86C4* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Ray>
struct Writer_1_t83950ACDC982F33C59176AEBFE823CBB074A0BFC  : public RuntimeObject
{
};

struct Writer_1_t83950ACDC982F33C59176AEBFE823CBB074A0BFC_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t0726BDF29C00FDD4DA0F278B3AFC4E47F69A4CED* ___write_0;
};

// Mirror.Writer`1<Mirror.ReadyMessage>
struct Writer_1_t95C4BC990ED47CA1945D53AA60D4F38C75743BAB  : public RuntimeObject
{
};

struct Writer_1_t95C4BC990ED47CA1945D53AA60D4F38C75743BAB_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t4CDBF06B555A45F0D103E46E4114CEEEFA34B691* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Rect>
struct Writer_1_tBCB1D512EE5CFE7E2A2BA73BC72DA709A669FA52  : public RuntimeObject
{
};

struct Writer_1_tBCB1D512EE5CFE7E2A2BA73BC72DA709A669FA52_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t776DBFE440EDCE827698E6B849C13676E750F733* ___write_0;
};

// Mirror.Writer`1<Mirror.RpcMessage>
struct Writer_1_tDCFFA2190DEB474AD0651E28A8D50F5AE9BA52E1  : public RuntimeObject
{
};

struct Writer_1_tDCFFA2190DEB474AD0651E28A8D50F5AE9BA52E1_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tAF32C09EC861FEA50FE0977ED9C0DAD82BA3D637* ___write_0;
};

// Mirror.Writer`1<System.SByte>
struct Writer_1_t4C3ABDA4E9C5685EE6B45F6ED3FACEE79F085657  : public RuntimeObject
{
};

struct Writer_1_t4C3ABDA4E9C5685EE6B45F6ED3FACEE79F085657_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tEE3DE27FC7A1D873CE799E7124798B9A979DE913* ___write_0;
};

// Mirror.Writer`1<Mirror.SceneMessage>
struct Writer_1_t4D7D68DB9331B49DDA144EDF2DEE5BCBCAC34F6F  : public RuntimeObject
{
};

struct Writer_1_t4D7D68DB9331B49DDA144EDF2DEE5BCBCAC34F6F_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t4A43C94439BC4A524A4463B987E9326B340BB5D7* ___write_0;
};

// Mirror.Writer`1<Mirror.SceneOperation>
struct Writer_1_t56257DBAFE2A2FE91E9247E46AC489415891F173  : public RuntimeObject
{
};

struct Writer_1_t56257DBAFE2A2FE91E9247E46AC489415891F173_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t102F2BE697BDA0818FA414975B57C948D9C2BBDF* ___write_0;
};

// Mirror.Writer`1<Mirror.Examples.MultipleMatch.ServerMatchMessage>
struct Writer_1_t3F4AFD2264190A12B59C26F438E4D9A0D5C6B8A0  : public RuntimeObject
{
};

struct Writer_1_t3F4AFD2264190A12B59C26F438E4D9A0D5C6B8A0_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tAF93D04C699FAC16A658F8DA78DD252A93E952E8* ___write_0;
};

// Mirror.Writer`1<Mirror.Examples.MultipleMatch.ServerMatchOperation>
struct Writer_1_tB00DD74436318ED1FFBD6AE43C142C83E0E441E7  : public RuntimeObject
{
};

struct Writer_1_tB00DD74436318ED1FFBD6AE43C142C83E0E441E7_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tEF11BC50B4B015730C0409DC35EAC7D6E03CEE11* ___write_0;
};

// Mirror.Writer`1<System.Single>
struct Writer_1_t3C56C64613D535D9C73CA3D587F334CD7CE30434  : public RuntimeObject
{
};

struct Writer_1_t3C56C64613D535D9C73CA3D587F334CD7CE30434_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tE45BEC1FBF44C71FEAD78DC6323066FD51D7FE55* ___write_0;
};

// Mirror.Writer`1<Mirror.SpawnMessage>
struct Writer_1_t1382F19A71CF32746849E9722B51E1B6EBC080DC  : public RuntimeObject
{
};

struct Writer_1_t1382F19A71CF32746849E9722B51E1B6EBC080DC_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t8CF38B31F52C4EF713F3D2BAC6B806B321ADC0A1* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Sprite>
struct Writer_1_tEF909D3F88F3F38B3C89136F9CEE84B8AF352E3F  : public RuntimeObject
{
};

struct Writer_1_tEF909D3F88F3F38B3C89136F9CEE84B8AF352E3F_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tA527CAC169A61C3874E9DEC5127904806AF2463C* ___write_0;
};

// Mirror.Writer`1<System.String>
struct Writer_1_tC900E33B3D8F5D4C68DB3645A60C917BAC711321  : public RuntimeObject
{
};

struct Writer_1_tC900E33B3D8F5D4C68DB3645A60C917BAC711321_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t63FAAE13C5F96B474021BB5448158556C8EBB594* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Texture2D>
struct Writer_1_t2190E1DFD8994854D9985EB6B653D18BBC814BBD  : public RuntimeObject
{
};

struct Writer_1_t2190E1DFD8994854D9985EB6B653D18BBC814BBD_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tA50DCC93E783D93FEFF521E5255E78C184ED8E02* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Transform>
struct Writer_1_t28CA88055E5AD2A0F9B106076EF93C52105F504A  : public RuntimeObject
{
};

struct Writer_1_t28CA88055E5AD2A0F9B106076EF93C52105F504A_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tD87BC92995377D813FAEA7DFF07C8F114451FAAA* ___write_0;
};

// Mirror.Writer`1<System.UInt16>
struct Writer_1_tD6B98FC179BE9F9519958863FD7D3515B8FFC6A7  : public RuntimeObject
{
};

struct Writer_1_tD6B98FC179BE9F9519958863FD7D3515B8FFC6A7_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tDC9B80B3DDEFFE6AD670786AA0C0A41F09DFE02D* ___write_0;
};

// Mirror.Writer`1<System.UInt32>
struct Writer_1_tE1FE9E7BA2D0C18CB94133B384971FAAD2EABAB7  : public RuntimeObject
{
};

struct Writer_1_tE1FE9E7BA2D0C18CB94133B384971FAAD2EABAB7_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t0FDD9213EEB281B92425DB7083B3E28D691AA59F* ___write_0;
};

// Mirror.Writer`1<System.UInt64>
struct Writer_1_tE871E366A3A0953860589F6AC9B876E03571DD8F  : public RuntimeObject
{
};

struct Writer_1_tE871E366A3A0953860589F6AC9B876E03571DD8F_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t7962FDDC98F8F0028DDCD9B0CADBA4C9C051E1C5* ___write_0;
};

// Mirror.Writer`1<System.Uri>
struct Writer_1_t7B4F1E8858C145418CFBD4E81EEB412568ABB75B  : public RuntimeObject
{
};

struct Writer_1_t7B4F1E8858C145418CFBD4E81EEB412568ABB75B_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t92B2C6139E3081F85D498D28EED40F2C004C7B20* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Vector2>
struct Writer_1_tEB9DBA2CB2FA886294119B2A97206EB710E97D34  : public RuntimeObject
{
};

struct Writer_1_tEB9DBA2CB2FA886294119B2A97206EB710E97D34_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tCAF5159105AB227514D4319BDE35C44C14FB6C67* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Vector2Int>
struct Writer_1_t72E49BCDFE4B2A53733511DC5BCDB5E02D0EBF54  : public RuntimeObject
{
};

struct Writer_1_t72E49BCDFE4B2A53733511DC5BCDB5E02D0EBF54_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tE7875C2C92DB47F882FCBBE26BB848F3264D0B63* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Vector3>
struct Writer_1_t7F01052FAB46FDA22D39911D07363E1D87E72BBA  : public RuntimeObject
{
};

struct Writer_1_t7F01052FAB46FDA22D39911D07363E1D87E72BBA_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tD63675C2600BD13840BDC184B61A1282E2FA7667* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Vector3Int>
struct Writer_1_t2862DC0A99F1512C1E98B5CEEFEAB76FBB011055  : public RuntimeObject
{
};

struct Writer_1_t2862DC0A99F1512C1E98B5CEEFEAB76FBB011055_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tA9611CADC87D4AD3B0FB66AF118E5B269E6FA658* ___write_0;
};

// Mirror.Writer`1<UnityEngine.Vector4>
struct Writer_1_tBAC97F7921836787FC871388114C4389B83F337E  : public RuntimeObject
{
};

struct Writer_1_tBAC97F7921836787FC871388114C4389B83F337E_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t0708A1448BB2410AD3F0DF78AF427D6730760C7F* ___write_0;
};

// Mirror.Writer`1<Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage>
struct Writer_1_t256EBB9B83C8B6F6F428369117FBBEC36C7E390C  : public RuntimeObject
{
};

struct Writer_1_t256EBB9B83C8B6F6F428369117FBBEC36C7E390C_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_tC9D1D64ED53FED0525E8EE017F8CAEBD3D9CF610* ___write_0;
};

// Mirror.Writer`1<Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage>
struct Writer_1_t93E7560AB93CC6616158EBA6F0ADF0F95A12807A  : public RuntimeObject
{
};

struct Writer_1_t93E7560AB93CC6616158EBA6F0ADF0F95A12807A_StaticFields
{
	// System.Action`2<Mirror.NetworkWriter,T> Mirror.Writer`1::write
	Action_2_t6B81023D4D427FA4343B3A8E4F4A88A145C6884F* ___write_0;
};
struct Il2CppArrayBounds;

// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095  : public RuntimeObject
{
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t52460FA30AE37F4F26ACB81055E58002262F19F2* ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_tD2C40CE114AA9D8E1F7196608B2D088548015293* ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t7324102215E4ED41EC065C02EB501CB0BC23CD90* ___decoderFallback_14;
};

struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095_StaticFields
{
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___latin1Encoding_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Text.Encoding> modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Dictionary_2_t87EDE08B2E48F793A22DE50D6B3CC2E7EBB2DB54* ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject* ___s_InternalSyncObject_15;
};

// Mirror.GeneratedNetworkCode
struct GeneratedNetworkCode_t5888D0B34F923FDEF563A09B4637A6922EB2D8A0  : public RuntimeObject
{
};

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// Mirror.NetworkReaderExtensions
struct NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B  : public RuntimeObject
{
};

struct NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_StaticFields
{
	// System.Text.UTF8Encoding Mirror.NetworkReaderExtensions::encoding
	UTF8Encoding_t90B56215A1B0B7ED5CDEA772E695F0DDAFBCD3BE* ___encoding_0;
};

// Mirror.NetworkWriter
struct NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C  : public RuntimeObject
{
	// System.Byte[] Mirror.NetworkWriter::buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___buffer_1;
	// System.Int32 Mirror.NetworkWriter::Position
	int32_t ___Position_2;
};

// Mirror.NetworkWriterExtensions
struct NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD  : public RuntimeObject
{
};

struct NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_StaticFields
{
	// System.Text.UTF8Encoding Mirror.NetworkWriterExtensions::encoding
	UTF8Encoding_t90B56215A1B0B7ED5CDEA772E695F0DDAFBCD3BE* ___encoding_0;
	// System.Byte[] Mirror.NetworkWriterExtensions::stringBuffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___stringBuffer_1;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.Uri
struct Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E  : public RuntimeObject
{
	// System.String System.Uri::m_String
	String_t* ___m_String_16;
	// System.String System.Uri::m_originalUnicodeString
	String_t* ___m_originalUnicodeString_17;
	// System.UriParser System.Uri::m_Syntax
	UriParser_t920B0868286118827C08B08A15A9456AF6C19D81* ___m_Syntax_18;
	// System.String System.Uri::m_DnsSafeHost
	String_t* ___m_DnsSafeHost_19;
	// System.Uri/Flags System.Uri::m_Flags
	uint64_t ___m_Flags_20;
	// System.Uri/UriInfo System.Uri::m_Info
	UriInfo_t5F91F77A93545DDDA6BB24A609BAF5E232CC1A09* ___m_Info_21;
	// System.Boolean System.Uri::m_iriParsing
	bool ___m_iriParsing_22;
};

struct Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E_StaticFields
{
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_0;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_1;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_2;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_3;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_4;
	// System.String System.Uri::UriSchemeWs
	String_t* ___UriSchemeWs_5;
	// System.String System.Uri::UriSchemeWss
	String_t* ___UriSchemeWss_6;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_7;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_8;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_9;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_10;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_11;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitialized
	bool ___s_ConfigInitialized_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitializing
	bool ___s_ConfigInitializing_24;
	// System.UriIdnScope modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IdnScope
	int32_t ___s_IdnScope_25;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IriParsing
	bool ___s_IriParsing_26;
	// System.Boolean System.Uri::useDotNetRelativeOrAbsolute
	bool ___useDotNetRelativeOrAbsolute_27;
	// System.Boolean System.Uri::IsWindowsFileSystem
	bool ___IsWindowsFileSystem_29;
	// System.Object System.Uri::s_initLock
	RuntimeObject* ___s_initLock_30;
	// System.Char[] System.Uri::HexLowerChars
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___HexLowerChars_34;
	// System.Char[] System.Uri::_WSchars
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ____WSchars_35;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.ArraySegment`1<System.Byte>
struct ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 
{
	// T[] System.ArraySegment`1::_array
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____array_1;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_2;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_3;
};

struct ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093_StaticFields
{
	// System.ArraySegment`1<T> System.ArraySegment`1::<Empty>k__BackingField
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___U3CEmptyU3Ek__BackingField_0;
};

// System.Nullable`1<System.Boolean>
struct Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	bool ___value_1;
};

// System.Nullable`1<System.Byte>
struct Nullable_1_tEB6689CC9747A3600689077DCBF77B8E8B510505 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	uint8_t ___value_1;
};

// System.Nullable`1<System.Char>
struct Nullable_1_tD52F1D0FC7EBB336F119BE953E59F426766032C1 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Il2CppChar ___value_1;
};

// System.Nullable`1<System.Double>
struct Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	double ___value_1;
};

// System.Nullable`1<System.Int16>
struct Nullable_1_t57D99A484501B89DA27E67D6D9A89722D5A7DE2C 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	int16_t ___value_1;
};

// System.Nullable`1<System.Int32>
struct Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	int32_t ___value_1;
};

// System.Nullable`1<System.Int64>
struct Nullable_1_t365991B3904FDA7642A788423B28692FDC7CDB17 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	int64_t ___value_1;
};

// System.Nullable`1<System.SByte>
struct Nullable_1_tCF16C2638810B89EAA3EEFE6B35FC71B6AE96B2C 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	int8_t ___value_1;
};

// System.Nullable`1<System.Single>
struct Nullable_1_t3D746CBB6123D4569FF4DEA60BC4240F32C6FE75 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	float ___value_1;
};

// System.Nullable`1<System.UInt16>
struct Nullable_1_t70F850DEE49B62D1B877D3C32F9E0EC724ADC4D9 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	uint16_t ___value_1;
};

// System.Nullable`1<System.UInt32>
struct Nullable_1_tD043F01310E483091D0E9A5526C3425F13EF2099 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	uint32_t ___value_1;
};

// System.Nullable`1<System.UInt64>
struct Nullable_1_tF8BFF19FF240C9F0A45168187CD7106BAA146A99 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	uint64_t ___value_1;
};

// Mirror.AddPlayerMessage
struct AddPlayerMessage_t8B70DF20613DB8A7E5B2A4DAAF2674D0DFD2F8F6 
{
	union
	{
		struct
		{
		};
		uint8_t AddPlayerMessage_t8B70DF20613DB8A7E5B2A4DAAF2674D0DFD2F8F6__padding[1];
	};
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// Mirror.ChangeOwnerMessage
struct ChangeOwnerMessage_t6539B3468B62CDA56EBE80C50BA9CF93FEF8F55F 
{
	// System.UInt32 Mirror.ChangeOwnerMessage::netId
	uint32_t ___netId_0;
	// System.Boolean Mirror.ChangeOwnerMessage::isOwner
	bool ___isOwner_1;
	// System.Boolean Mirror.ChangeOwnerMessage::isLocalPlayer
	bool ___isLocalPlayer_2;
};
// Native definition for P/Invoke marshalling of Mirror.ChangeOwnerMessage
struct ChangeOwnerMessage_t6539B3468B62CDA56EBE80C50BA9CF93FEF8F55F_marshaled_pinvoke
{
	uint32_t ___netId_0;
	int32_t ___isOwner_1;
	int32_t ___isLocalPlayer_2;
};
// Native definition for COM marshalling of Mirror.ChangeOwnerMessage
struct ChangeOwnerMessage_t6539B3468B62CDA56EBE80C50BA9CF93FEF8F55F_marshaled_com
{
	uint32_t ___netId_0;
	int32_t ___isOwner_1;
	int32_t ___isLocalPlayer_2;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// UnityEngine.Color32
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};
};

// System.Decimal
struct Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 System.Decimal::flags
			int32_t ___flags_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___flags_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___hi_9_OffsetPadding[4];
			// System.Int32 System.Decimal::hi
			int32_t ___hi_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___hi_9_OffsetPadding_forAlignmentOnly[4];
			int32_t ___hi_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___lo_10_OffsetPadding[8];
			// System.Int32 System.Decimal::lo
			int32_t ___lo_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___lo_10_OffsetPadding_forAlignmentOnly[8];
			int32_t ___lo_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___mid_11_OffsetPadding[12];
			// System.Int32 System.Decimal::mid
			int32_t ___mid_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___mid_11_OffsetPadding_forAlignmentOnly[12];
			int32_t ___mid_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___ulomidLE_12_OffsetPadding[8];
			// System.UInt64 System.Decimal::ulomidLE
			uint64_t ___ulomidLE_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___ulomidLE_12_OffsetPadding_forAlignmentOnly[8];
			uint64_t ___ulomidLE_12_forAlignmentOnly;
		};
	};
};

struct Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F_StaticFields
{
	// System.Decimal System.Decimal::Zero
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___Zero_3;
	// System.Decimal System.Decimal::One
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___One_4;
	// System.Decimal System.Decimal::MinusOne
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___MinusOne_5;
	// System.Decimal System.Decimal::MaxValue
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___MaxValue_6;
	// System.Decimal System.Decimal::MinValue
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___MinValue_7;
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Guid
struct Guid_t 
{
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;
};

struct Guid_t_StaticFields
{
	// System.Guid System.Guid::Empty
	Guid_t ___Empty_0;
};

// System.Int16
struct Int16_tB8EF286A9C33492FA6E6D6E67320BE93E794A175 
{
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// Mirror.Examples.MultipleMatch.MatchPlayerData
struct MatchPlayerData_t8CBE176A0E06C3BDF615BAAF008FBD6E419977A9 
{
	// System.Int32 Mirror.Examples.MultipleMatch.MatchPlayerData::playerIndex
	int32_t ___playerIndex_0;
	// System.Int32 Mirror.Examples.MultipleMatch.MatchPlayerData::wins
	int32_t ___wins_1;
	// Mirror.Examples.MultipleMatch.CellValue Mirror.Examples.MultipleMatch.MatchPlayerData::currentScore
	uint16_t ___currentScore_2;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;
};

struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields
{
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___identityMatrix_17;
};

// Mirror.NetworkIdentitySerialization
struct NetworkIdentitySerialization_t95487F26F667AF97203016B8FF0359E6D31FB4C9 
{
	// System.Int32 Mirror.NetworkIdentitySerialization::tick
	int32_t ___tick_0;
	// Mirror.NetworkWriter Mirror.NetworkIdentitySerialization::ownerWriter
	NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___ownerWriter_1;
	// Mirror.NetworkWriter Mirror.NetworkIdentitySerialization::observersWriter
	NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___observersWriter_2;
};
// Native definition for P/Invoke marshalling of Mirror.NetworkIdentitySerialization
struct NetworkIdentitySerialization_t95487F26F667AF97203016B8FF0359E6D31FB4C9_marshaled_pinvoke
{
	int32_t ___tick_0;
	NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___ownerWriter_1;
	NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___observersWriter_2;
};
// Native definition for COM marshalling of Mirror.NetworkIdentitySerialization
struct NetworkIdentitySerialization_t95487F26F667AF97203016B8FF0359E6D31FB4C9_marshaled_com
{
	int32_t ___tick_0;
	NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___ownerWriter_1;
	NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___observersWriter_2;
};

// Mirror.NetworkPingMessage
struct NetworkPingMessage_t254AA1B47CDBC1136A16C49B6147AC5462C60B27 
{
	// System.Double Mirror.NetworkPingMessage::clientTime
	double ___clientTime_0;
};

// Mirror.NetworkPongMessage
struct NetworkPongMessage_tD0BD2C925B3E72156657A78E2D9AD09D3E3B4EC6 
{
	// System.Double Mirror.NetworkPongMessage::clientTime
	double ___clientTime_0;
	// System.Double Mirror.NetworkPongMessage::serverTime
	double ___serverTime_1;
};

// Mirror.NotReadyMessage
struct NotReadyMessage_tF34CF670A9AD115E0FDC1F7BCE4F75A4C9172036 
{
	union
	{
		struct
		{
		};
		uint8_t NotReadyMessage_tF34CF670A9AD115E0FDC1F7BCE4F75A4C9172036__padding[1];
	};
};

// Mirror.ObjectDestroyMessage
struct ObjectDestroyMessage_tF205F01F24B264A63044BA3FAC1E9B080DB068D2 
{
	// System.UInt32 Mirror.ObjectDestroyMessage::netId
	uint32_t ___netId_0;
};

// Mirror.ObjectHideMessage
struct ObjectHideMessage_t13A7D352E4B0A0D08A38BCAC3E454CDB59756F3C 
{
	// System.UInt32 Mirror.ObjectHideMessage::netId
	uint32_t ___netId_0;
};

// Mirror.ObjectSpawnFinishedMessage
struct ObjectSpawnFinishedMessage_t2E367844E4D843D90ED9E273573BF341B04658D8 
{
	union
	{
		struct
		{
		};
		uint8_t ObjectSpawnFinishedMessage_t2E367844E4D843D90ED9E273573BF341B04658D8__padding[1];
	};
};

// Mirror.ObjectSpawnStartedMessage
struct ObjectSpawnStartedMessage_tD4E40048359CFB70648E9DEEDF4DD292084FD774 
{
	union
	{
		struct
		{
		};
		uint8_t ObjectSpawnStartedMessage_tD4E40048359CFB70648E9DEEDF4DD292084FD774__padding[1];
	};
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// Mirror.ReadyMessage
struct ReadyMessage_t827D165B99D0F8834C4F35860876486AFB9867F8 
{
	union
	{
		struct
		{
		};
		uint8_t ReadyMessage_t827D165B99D0F8834C4F35860876486AFB9867F8__padding[1];
	};
};

// UnityEngine.Rect
struct Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D 
{
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;
};

// System.SByte
struct SByte_tFEFFEF5D2FEBF5207950AE6FAC150FC53B668DB5 
{
	// System.SByte System.SByte::m_value
	int8_t ___m_value_0;
};

// Mirror.SceneMessage
struct SceneMessage_t47621E5F4F96B23FBA41C9E4015DCDB9F0BE6C26 
{
	// System.String Mirror.SceneMessage::sceneName
	String_t* ___sceneName_0;
	// Mirror.SceneOperation Mirror.SceneMessage::sceneOperation
	uint8_t ___sceneOperation_1;
	// System.Boolean Mirror.SceneMessage::customHandling
	bool ___customHandling_2;
};
// Native definition for P/Invoke marshalling of Mirror.SceneMessage
struct SceneMessage_t47621E5F4F96B23FBA41C9E4015DCDB9F0BE6C26_marshaled_pinvoke
{
	char* ___sceneName_0;
	uint8_t ___sceneOperation_1;
	int32_t ___customHandling_2;
};
// Native definition for COM marshalling of Mirror.SceneMessage
struct SceneMessage_t47621E5F4F96B23FBA41C9E4015DCDB9F0BE6C26_marshaled_com
{
	Il2CppChar* ___sceneName_0;
	uint8_t ___sceneOperation_1;
	int32_t ___customHandling_2;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// System.UInt16
struct UInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455 
{
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;
};

// System.UInt32
struct UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B 
{
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;
};

// System.UInt64
struct UInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF 
{
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;
};

// System.Text.UTF8Encoding
struct UTF8Encoding_t90B56215A1B0B7ED5CDEA772E695F0DDAFBCD3BE  : public Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095
{
	// System.Boolean System.Text.UTF8Encoding::_emitUTF8Identifier
	bool ____emitUTF8Identifier_18;
	// System.Boolean System.Text.UTF8Encoding::_isThrowException
	bool ____isThrowException_19;
};

struct UTF8Encoding_t90B56215A1B0B7ED5CDEA772E695F0DDAFBCD3BE_StaticFields
{
	// System.Text.UTF8Encoding/UTF8EncodingSealed System.Text.UTF8Encoding::s_default
	UTF8EncodingSealed_tF97A34F40CABE9CE1C168967D60396F51C43DD36* ___s_default_16;
	// System.Byte[] System.Text.UTF8Encoding::s_preamble
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_preamble_17;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector2Int
struct Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A 
{
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;
};

struct Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A_StaticFields
{
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Right_7;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector3Int
struct Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 
{
	// System.Int32 UnityEngine.Vector3Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector3Int::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.Vector3Int::m_Z
	int32_t ___m_Z_2;
};

struct Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376_StaticFields
{
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Zero
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Zero_3;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_One
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_One_4;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Up
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Up_5;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Down
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Down_6;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Left
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Left_7;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Right
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Right_8;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Forward
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Forward_9;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Back
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Back_10;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage
struct AuthRequestMessage_t73733A4DC155F7E3523FC84D17B810F3EEFC7BE2 
{
	// System.String Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage::authUsername
	String_t* ___authUsername_0;
};
// Native definition for P/Invoke marshalling of Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage
struct AuthRequestMessage_t73733A4DC155F7E3523FC84D17B810F3EEFC7BE2_marshaled_pinvoke
{
	char* ___authUsername_0;
};
// Native definition for COM marshalling of Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage
struct AuthRequestMessage_t73733A4DC155F7E3523FC84D17B810F3EEFC7BE2_marshaled_com
{
	Il2CppChar* ___authUsername_0;
};

// Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage
struct AuthResponseMessage_t0A0B56A8B7E569EF74D718C068255179F3E04CE7 
{
	// System.Byte Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage::code
	uint8_t ___code_0;
	// System.String Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage::message
	String_t* ___message_1;
};
// Native definition for P/Invoke marshalling of Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage
struct AuthResponseMessage_t0A0B56A8B7E569EF74D718C068255179F3E04CE7_marshaled_pinvoke
{
	uint8_t ___code_0;
	char* ___message_1;
};
// Native definition for COM marshalling of Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage
struct AuthResponseMessage_t0A0B56A8B7E569EF74D718C068255179F3E04CE7_marshaled_com
{
	uint8_t ___code_0;
	Il2CppChar* ___message_1;
};

// Mirror.NetworkBehaviour/NetworkBehaviourSyncVar
struct NetworkBehaviourSyncVar_t4850A9C89711F9252CECEB784584066044CEA19E 
{
	// System.UInt32 Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::netId
	uint32_t ___netId_0;
	// System.Byte Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::componentIndex
	uint8_t ___componentIndex_1;
};

// System.Nullable`1<UnityEngine.Color>
struct Nullable_1_tEE83D90B507D40B6C58B5EEF5B9D44D377B44F11 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value_1;
};

// System.Nullable`1<UnityEngine.Color32>
struct Nullable_1_tCD9239CD3E3695D86323FAA5C5D577A535E6FF06 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value_1;
};

// System.Nullable`1<System.Decimal>
struct Nullable_1_t072551AA1AA8366A46F232F8180C34AA0CFFACBB 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___value_1;
};

// System.Nullable`1<System.Guid>
struct Nullable_1_t0ECB838EB0C9A81655750B26970F21CF9A83A5F7 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Guid_t ___value_1;
};

// System.Nullable`1<UnityEngine.Matrix4x4>
struct Nullable_1_t4DE2D2A1D2B73B95E813201AC08E1041435836AA 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___value_1;
};

// System.Nullable`1<UnityEngine.Quaternion>
struct Nullable_1_tC8106DB4DC621B5BCB8913A244640A1CEDF9DD25 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___value_1;
};

// System.Nullable`1<UnityEngine.Rect>
struct Nullable_1_t13F9968C978BAF968F02BA5B41ABB481321A5440 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___value_1;
};

// System.Nullable`1<UnityEngine.Vector2>
struct Nullable_1_tAC9037ECF4C188DFFE614617119CAC19A784F9FD 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value_1;
};

// System.Nullable`1<UnityEngine.Vector2Int>
struct Nullable_1_t6ABD491AB047CA3F2EF9F1D89346A6A339003E35 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___value_1;
};

// System.Nullable`1<UnityEngine.Vector3>
struct Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value_1;
};

// System.Nullable`1<UnityEngine.Vector3Int>
struct Nullable_1_tFB4A56FF9A8D4E35AF50EF50D7B137C9B7AD717B 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___value_1;
};

// System.Nullable`1<UnityEngine.Vector4>
struct Nullable_1_t5070FF5B4129C859CA3EFC7B94E615A818E11144 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___value_1;
};

// Mirror.Examples.MultipleMatch.ClientMatchMessage
struct ClientMatchMessage_tE0C6260F938E859985838F9C68E0C87107E97837 
{
	// Mirror.Examples.MultipleMatch.ClientMatchOperation Mirror.Examples.MultipleMatch.ClientMatchMessage::clientMatchOperation
	uint8_t ___clientMatchOperation_0;
	// System.Guid Mirror.Examples.MultipleMatch.ClientMatchMessage::matchId
	Guid_t ___matchId_1;
	// Mirror.Examples.MultipleMatch.MatchInfo[] Mirror.Examples.MultipleMatch.ClientMatchMessage::matchInfos
	MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* ___matchInfos_2;
	// Mirror.Examples.MultipleMatch.PlayerInfo[] Mirror.Examples.MultipleMatch.ClientMatchMessage::playerInfos
	PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* ___playerInfos_3;
};
// Native definition for P/Invoke marshalling of Mirror.Examples.MultipleMatch.ClientMatchMessage
struct ClientMatchMessage_tE0C6260F938E859985838F9C68E0C87107E97837_marshaled_pinvoke
{
	uint8_t ___clientMatchOperation_0;
	Guid_t ___matchId_1;
	MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41* ___matchInfos_2;
	PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_marshaled_pinvoke* ___playerInfos_3;
};
// Native definition for COM marshalling of Mirror.Examples.MultipleMatch.ClientMatchMessage
struct ClientMatchMessage_tE0C6260F938E859985838F9C68E0C87107E97837_marshaled_com
{
	uint8_t ___clientMatchOperation_0;
	Guid_t ___matchId_1;
	MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41* ___matchInfos_2;
	PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_marshaled_com* ___playerInfos_3;
};

// Mirror.CommandMessage
struct CommandMessage_t33C5D102BB2924A7CB43AC46B166A390E5EB893E 
{
	// System.UInt32 Mirror.CommandMessage::netId
	uint32_t ___netId_0;
	// System.Byte Mirror.CommandMessage::componentIndex
	uint8_t ___componentIndex_1;
	// System.Int32 Mirror.CommandMessage::functionHash
	int32_t ___functionHash_2;
	// System.ArraySegment`1<System.Byte> Mirror.CommandMessage::payload
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___payload_3;
};
// Native definition for P/Invoke marshalling of Mirror.CommandMessage
struct CommandMessage_t33C5D102BB2924A7CB43AC46B166A390E5EB893E_marshaled_pinvoke
{
	uint32_t ___netId_0;
	uint8_t ___componentIndex_1;
	int32_t ___functionHash_2;
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___payload_3;
};
// Native definition for COM marshalling of Mirror.CommandMessage
struct CommandMessage_t33C5D102BB2924A7CB43AC46B166A390E5EB893E_marshaled_com
{
	uint32_t ___netId_0;
	uint8_t ___componentIndex_1;
	int32_t ___functionHash_2;
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___payload_3;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// Mirror.EntityStateMessage
struct EntityStateMessage_t741DD321283AD1933F59FB67E67905E4DE9A744E 
{
	// System.UInt32 Mirror.EntityStateMessage::netId
	uint32_t ___netId_0;
	// System.ArraySegment`1<System.Byte> Mirror.EntityStateMessage::payload
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___payload_1;
};
// Native definition for P/Invoke marshalling of Mirror.EntityStateMessage
struct EntityStateMessage_t741DD321283AD1933F59FB67E67905E4DE9A744E_marshaled_pinvoke
{
	uint32_t ___netId_0;
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___payload_1;
};
// Native definition for COM marshalling of Mirror.EntityStateMessage
struct EntityStateMessage_t741DD321283AD1933F59FB67E67905E4DE9A744E_marshaled_com
{
	uint32_t ___netId_0;
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___payload_1;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// Mirror.Examples.MultipleMatch.MatchInfo
struct MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 
{
	// System.Guid Mirror.Examples.MultipleMatch.MatchInfo::matchId
	Guid_t ___matchId_0;
	// System.Byte Mirror.Examples.MultipleMatch.MatchInfo::players
	uint8_t ___players_1;
	// System.Byte Mirror.Examples.MultipleMatch.MatchInfo::maxPlayers
	uint8_t ___maxPlayers_2;
};

// Mirror.NetworkReader
struct NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1  : public RuntimeObject
{
	// System.ArraySegment`1<System.Byte> Mirror.NetworkReader::buffer
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___buffer_0;
	// System.Int32 Mirror.NetworkReader::Position
	int32_t ___Position_1;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Plane
struct Plane_tB7D8CC6F7AACF5F3AA483AF005C1102A8577BC0C 
{
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_1;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_2;
};

// Mirror.Examples.MultipleMatch.PlayerInfo
struct PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 
{
	// System.Int32 Mirror.Examples.MultipleMatch.PlayerInfo::playerIndex
	int32_t ___playerIndex_0;
	// System.Boolean Mirror.Examples.MultipleMatch.PlayerInfo::ready
	bool ___ready_1;
	// System.Guid Mirror.Examples.MultipleMatch.PlayerInfo::matchId
	Guid_t ___matchId_2;
};
// Native definition for P/Invoke marshalling of Mirror.Examples.MultipleMatch.PlayerInfo
struct PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_marshaled_pinvoke
{
	int32_t ___playerIndex_0;
	int32_t ___ready_1;
	Guid_t ___matchId_2;
};
// Native definition for COM marshalling of Mirror.Examples.MultipleMatch.PlayerInfo
struct PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_marshaled_com
{
	int32_t ___playerIndex_0;
	int32_t ___ready_1;
	Guid_t ___matchId_2;
};

// UnityEngine.Ray
struct Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 
{
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Direction_1;
};

// Mirror.RpcMessage
struct RpcMessage_tA24B1AA9AB9593C9491786C4AD6EBD8CD822D4AD 
{
	// System.UInt32 Mirror.RpcMessage::netId
	uint32_t ___netId_0;
	// System.Byte Mirror.RpcMessage::componentIndex
	uint8_t ___componentIndex_1;
	// System.Int32 Mirror.RpcMessage::functionHash
	int32_t ___functionHash_2;
	// System.ArraySegment`1<System.Byte> Mirror.RpcMessage::payload
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___payload_3;
};
// Native definition for P/Invoke marshalling of Mirror.RpcMessage
struct RpcMessage_tA24B1AA9AB9593C9491786C4AD6EBD8CD822D4AD_marshaled_pinvoke
{
	uint32_t ___netId_0;
	uint8_t ___componentIndex_1;
	int32_t ___functionHash_2;
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___payload_3;
};
// Native definition for COM marshalling of Mirror.RpcMessage
struct RpcMessage_tA24B1AA9AB9593C9491786C4AD6EBD8CD822D4AD_marshaled_com
{
	uint32_t ___netId_0;
	uint8_t ___componentIndex_1;
	int32_t ___functionHash_2;
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___payload_3;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// Mirror.Examples.MultipleMatch.ServerMatchMessage
struct ServerMatchMessage_tAA90E663A53A2A48E57057D0B00DA4251AC000B8 
{
	// Mirror.Examples.MultipleMatch.ServerMatchOperation Mirror.Examples.MultipleMatch.ServerMatchMessage::serverMatchOperation
	uint8_t ___serverMatchOperation_0;
	// System.Guid Mirror.Examples.MultipleMatch.ServerMatchMessage::matchId
	Guid_t ___matchId_1;
};

// Mirror.SpawnMessage
struct SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 
{
	// System.UInt32 Mirror.SpawnMessage::netId
	uint32_t ___netId_0;
	// System.Boolean Mirror.SpawnMessage::isLocalPlayer
	bool ___isLocalPlayer_1;
	// System.Boolean Mirror.SpawnMessage::isOwner
	bool ___isOwner_2;
	// System.UInt64 Mirror.SpawnMessage::sceneId
	uint64_t ___sceneId_3;
	// System.Guid Mirror.SpawnMessage::assetId
	Guid_t ___assetId_4;
	// UnityEngine.Vector3 Mirror.SpawnMessage::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_5;
	// UnityEngine.Quaternion Mirror.SpawnMessage::rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_6;
	// UnityEngine.Vector3 Mirror.SpawnMessage::scale
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_7;
	// System.ArraySegment`1<System.Byte> Mirror.SpawnMessage::payload
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___payload_8;
};
// Native definition for P/Invoke marshalling of Mirror.SpawnMessage
struct SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475_marshaled_pinvoke
{
	uint32_t ___netId_0;
	int32_t ___isLocalPlayer_1;
	int32_t ___isOwner_2;
	uint64_t ___sceneId_3;
	Guid_t ___assetId_4;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_5;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_6;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_7;
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___payload_8;
};
// Native definition for COM marshalling of Mirror.SpawnMessage
struct SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475_marshaled_com
{
	uint32_t ___netId_0;
	int32_t ___isLocalPlayer_1;
	int32_t ___isOwner_2;
	uint64_t ___sceneId_3;
	Guid_t ___assetId_4;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_5;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_6;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_7;
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___payload_8;
};

// System.Nullable`1<UnityEngine.Plane>
struct Nullable_1_t450D8A4ABE39BE03FC49D7D0B5C19CE450F012F8 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Plane_tB7D8CC6F7AACF5F3AA483AF005C1102A8577BC0C ___value_1;
};

// System.Nullable`1<UnityEngine.Ray>
struct Nullable_1_t4963285281B990B794CDEACB9C3CD3069B5FA768 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___value_1;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700_StaticFields
{
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// System.Action`2<UnityEngine.Color32,UnityEngine.Color32>
struct Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.ArraySegment`1<System.Byte>>
struct Action_2_tE7DE89222EAD5CBE5FBA94711CB3C71222202DFC  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Boolean>>
struct Action_2_tFAFB5B1636A32CF85A452A078736260BB496C253  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Byte>>
struct Action_2_t104D6887DC3FA703E6D350257E4EE72A3EFCF3A3  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Char>>
struct Action_2_t1B2365362928779807EDAEE12F27010A7A8D39C6  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Color>>
struct Action_2_t1AB30C8E5E10864F469D7062D99016352CC3602F  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Color32>>
struct Action_2_t284D0A27595992EAC03994E6C1A1E82225441B67  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Decimal>>
struct Action_2_t3913958E2393CE5F44A661B2F0835A6FC4513CBE  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Double>>
struct Action_2_tD872DFCCB454D89FE5878BA1B3A03AA4FF2F7A72  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Guid>>
struct Action_2_t00EE65E013F88779EEEE98835E86BDF400A3BF36  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Int16>>
struct Action_2_tE28F1C5549DD431F6354E325F28C3FA842F94E57  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Int32>>
struct Action_2_tD938883EC204367D5A295A7CB2BB5D4D20A90029  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Int64>>
struct Action_2_tDF176CFB33B35A021E5DA20E9485F6A452F2C345  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Matrix4x4>>
struct Action_2_tB31DAF5F45A869010A4A3D1EA9EA5C56DCFAB947  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Plane>>
struct Action_2_t9BBCAC312FAACBDD9B08E82065F0C66CB92596DD  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Quaternion>>
struct Action_2_tFAE875C9728E772ACF44D3BCF5F2B94860FB1CDE  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Ray>>
struct Action_2_tBE61BBF1961F6628AEE63F058E11FEB22FB6A6ED  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Rect>>
struct Action_2_tD39051A11B2175464C227775F3F6FD44715C0767  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.SByte>>
struct Action_2_tEE379855D79D043D165FD05C3786F7B0ADB89B38  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Single>>
struct Action_2_tD01A9F5ED6BB951B86BE216B2B8DE2C17FFAE551  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.UInt16>>
struct Action_2_tDD6B83C4A6E2ECFC616A05B485C0D7E24A750E90  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.UInt32>>
struct Action_2_tC9DC1064C14B7AF886718CB747591EC132E1E301  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.UInt64>>
struct Action_2_t9E34BCADC1F9EF87E3BC9817BF9ABF3086286388  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector2>>
struct Action_2_t06CF1E09D2E968AE317C1B4066FE59281D0D7F35  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector2Int>>
struct Action_2_tF8B877CE67C7AF23C81D6C5C15EB3D2FE44321A0  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector3>>
struct Action_2_t3D709A9DD3DD60B2669C03CAF9A9607B98F0CC44  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector3Int>>
struct Action_2_tCF4F398AAC858F0B69CF49EA4DB6878F5E85A2FE  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector4>>
struct Action_2_t437C51FCE7D4E11D08E66AC6884ADD843A29305E  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Byte[]>
struct Action_2_t6CB11000550B779DE6F0A9873A4FA47684E8A996  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchInfo[]>
struct Action_2_t77285E9313B73F391F0816DD5738EBF05F8774AD  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.PlayerInfo[]>
struct Action_2_t5199EDEE1233BFABE45CDA21E9230C8346645716  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.AddPlayerMessage>
struct Action_2_t98800A090710915B3E861E237B1A9B6E9F29163F  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Boolean>
struct Action_2_t771EB50B64E2F517312250DEC5230DC62C7E36FD  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Byte>
struct Action_2_tDDEAD4B308FD0F5A9299458AF5A6B47CA5D3732F  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.CellValue>
struct Action_2_t117A07FAD93D252BC5E69F05373B4E286F1F8D56  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.ChangeOwnerMessage>
struct Action_2_tA6272A2BBF53770CE719C794145D53E55F0DCC51  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Char>
struct Action_2_t3C170C2BFCCAA591C6DA755A86E3FEDBB4428D90  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ClientMatchMessage>
struct Action_2_tCFCEE592405B35148F5E1AC506FEB4F8CB2F49B6  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ClientMatchOperation>
struct Action_2_tE631CB825D55269F80774B2F55C5E62192891CF8  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Color>
struct Action_2_t993E6168F76ADC356CC943B6992CDD55115F3057  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Color32>
struct Action_2_t27076E67A79536591E04C63FD1582DB7D564BA08  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.CommandMessage>
struct Action_2_t8863334817D651A8E3B8E434C682CA5A76CE9498  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Decimal>
struct Action_2_tC4556EDF997707D306A86E36FC8EE3751F9C687C  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Double>
struct Action_2_tA70CFBAAF2BDFBA2BEE4003F4C2E6E5B12924D8E  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.EntityStateMessage>
struct Action_2_t92D149BA0BB9E6C54B7036167F4A14DFF1B5BE08  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.GameObject>
struct Action_2_t7A7E74A9B38640A94D5CC807EC76F706FB6693C0  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Guid>
struct Action_2_tCB8F678502AD5BFA52EE6F149351DAC3037677AF  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Int16>
struct Action_2_tED2F1B32CB312EA2AFE3FF65E3EBD21ADF00A34C  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Int32>
struct Action_2_t4D3AA2594C46CEF975DCD32BFF9A0FEE380568B3  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Int64>
struct Action_2_tE79E56090404F1ED684677C80F34095664D0010D  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchInfo>
struct Action_2_t1D8E4389A2FC4D611519FC7C996BF96F763E5017  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchPlayerData>
struct Action_2_tB478A835A6DC624AEA7C01C383390CE10F4D2BF1  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Matrix4x4>
struct Action_2_tD12F3221DBF89BF11C6CB2855E9BDBD70D6A8AB9  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.NetworkBehaviour>
struct Action_2_tC5312D54454D5411CB706A055837C90CBD0FEC12  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.NetworkIdentity>
struct Action_2_t51E55DFCEC17AF21DB946B2527D3669CD8E09542  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.NetworkPingMessage>
struct Action_2_t427A3AAB52F4338A54E05C0F9CCE238604B75463  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.NetworkPongMessage>
struct Action_2_t19A08A9BB7D482718CD3474975A6559CA159FA9F  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.NotReadyMessage>
struct Action_2_tE1C508C7DB184AC769DA763897EA5CE208D96BE9  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.ObjectDestroyMessage>
struct Action_2_t4125FEC2DD5C797C8E273617CF2C3E2ABB98FA45  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.ObjectHideMessage>
struct Action_2_tB5D2418D8D5BB1CA27E37232842C681B92ABDB9B  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.ObjectSpawnFinishedMessage>
struct Action_2_t88B940D5126F22A0E314062C9CB9B8CF14430308  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.ObjectSpawnStartedMessage>
struct Action_2_t932E36A723FFA4E592D80F6F8AE785A34669DA75  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Plane>
struct Action_2_tF808E86BBEE3D1817CE8A0C2E299320C9DD9D1BA  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.PlayerInfo>
struct Action_2_t15EAEA1713B2DE57B0F7BFED86F3CF5915138314  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Quaternion>
struct Action_2_tD28B743D21DCE5F5BE026041A86D0F58543D86C4  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Ray>
struct Action_2_t0726BDF29C00FDD4DA0F278B3AFC4E47F69A4CED  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.ReadyMessage>
struct Action_2_t4CDBF06B555A45F0D103E46E4114CEEEFA34B691  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Rect>
struct Action_2_t776DBFE440EDCE827698E6B849C13676E750F733  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.RpcMessage>
struct Action_2_tAF32C09EC861FEA50FE0977ED9C0DAD82BA3D637  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.SByte>
struct Action_2_tEE3DE27FC7A1D873CE799E7124798B9A979DE913  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.SceneMessage>
struct Action_2_t4A43C94439BC4A524A4463B987E9326B340BB5D7  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.SceneOperation>
struct Action_2_t102F2BE697BDA0818FA414975B57C948D9C2BBDF  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ServerMatchMessage>
struct Action_2_tAF93D04C699FAC16A658F8DA78DD252A93E952E8  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ServerMatchOperation>
struct Action_2_tEF11BC50B4B015730C0409DC35EAC7D6E03CEE11  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Single>
struct Action_2_tE45BEC1FBF44C71FEAD78DC6323066FD51D7FE55  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.SpawnMessage>
struct Action_2_t8CF38B31F52C4EF713F3D2BAC6B806B321ADC0A1  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Sprite>
struct Action_2_tA527CAC169A61C3874E9DEC5127904806AF2463C  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.String>
struct Action_2_t63FAAE13C5F96B474021BB5448158556C8EBB594  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Texture2D>
struct Action_2_tA50DCC93E783D93FEFF521E5255E78C184ED8E02  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Transform>
struct Action_2_tD87BC92995377D813FAEA7DFF07C8F114451FAAA  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.UInt16>
struct Action_2_tDC9B80B3DDEFFE6AD670786AA0C0A41F09DFE02D  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.UInt32>
struct Action_2_t0FDD9213EEB281B92425DB7083B3E28D691AA59F  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.UInt64>
struct Action_2_t7962FDDC98F8F0028DDCD9B0CADBA4C9C051E1C5  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,System.Uri>
struct Action_2_t92B2C6139E3081F85D498D28EED40F2C004C7B20  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector2>
struct Action_2_tCAF5159105AB227514D4319BDE35C44C14FB6C67  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector2Int>
struct Action_2_tE7875C2C92DB47F882FCBBE26BB848F3264D0B63  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector3>
struct Action_2_tD63675C2600BD13840BDC184B61A1282E2FA7667  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector3Int>
struct Action_2_tA9611CADC87D4AD3B0FB66AF118E5B269E6FA658  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector4>
struct Action_2_t0708A1448BB2410AD3F0DF78AF427D6730760C7F  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage>
struct Action_2_tC9D1D64ED53FED0525E8EE017F8CAEBD3D9CF610  : public MulticastDelegate_t
{
};

// System.Action`2<Mirror.NetworkWriter,Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage>
struct Action_2_t6B81023D4D427FA4343B3A8E4F4A88A145C6884F  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.ArraySegment`1<System.Byte>>
struct Func_2_t0968F31AEE953CA42422B45BEE8A04FB272E086F  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Boolean>>
struct Func_2_t2CF9D64DD882AC30B207D3D6C3C4EFC846A40BC9  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Byte>>
struct Func_2_tC5D3F83B86462F66072C190B541019F5456F2DB9  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Char>>
struct Func_2_tBB870D080A3294867CDE63CF2AF4F2D4699C33B2  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Color>>
struct Func_2_tB85C8267AB8EBEC2E4C056F08CC1288F0C2055B7  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Color32>>
struct Func_2_t5B94AD2B8545A777AE2428975D92A19545B20228  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Decimal>>
struct Func_2_t2ED5FBB4104367F73E820E5864485F4EDD912E0F  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Double>>
struct Func_2_t3C87EC87405703EAD0A26AD3BD3A5B3615EB39E6  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Guid>>
struct Func_2_tDCE62FBEE709378CCBDB8A5F059E3D9EB17469CB  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Int16>>
struct Func_2_tA3EC1D81F5C9B991E5E551D9C0197BBCFA6ECFC8  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Int32>>
struct Func_2_tA8008B61C32E0E3A90F98593FFE956D148147DC5  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Int64>>
struct Func_2_t8F78F44B218E20FDA86CA793AEC9892B18B978F6  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Matrix4x4>>
struct Func_2_t4F5890FDEA23094C33678E54A33233DBBC85F64B  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Plane>>
struct Func_2_t93378D91D2D47434F10E8104D47A5A2363B48D08  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Quaternion>>
struct Func_2_t643EFCAEA913218C33F9E05144DAB3702712A18C  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Ray>>
struct Func_2_t4467783C00F409FFB8ED1FDCE87EF974AB372C4D  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Rect>>
struct Func_2_tCD175F70DBC690C8EF0231BF4AF1180C50BE25E0  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.SByte>>
struct Func_2_tFB31016B91F9F5C6B2CAC98095F7D025353DD033  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Single>>
struct Func_2_t2A9651B5311D10F2FDA41A113A50E18A22D93138  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.UInt16>>
struct Func_2_t4842934F57735E601CC8B995D6E040EAF256FBF4  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.UInt32>>
struct Func_2_tE53CE7EE59BDF79AA9E78FAB9A62695508883396  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.UInt64>>
struct Func_2_t4B1235C83470DD7C3286754A4CFC840FBFD35741  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector2>>
struct Func_2_t603A0F2238D55FD8325BC0816D55C2970606BB81  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector2Int>>
struct Func_2_tE2F89B2F7AD6F1B1ED5373840C57D1603C193D7D  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector3>>
struct Func_2_t58F06EBEDB50273CFFCE1BED6156FF00537E7BF6  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector3Int>>
struct Func_2_tB67E11F122FE23A54C3DC5758CF7ABCED89CA2B9  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector4>>
struct Func_2_t3EF4F45A92179F162ABDFA4C40C59CEAB4DF8C95  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Byte[]>
struct Func_2_t68753017D74F6BABDA2E69D18EC2FA64AC20B03F  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.MatchInfo[]>
struct Func_2_tEAEC0CEBCFCC9157E40E1A7FFFEB82D8A082B205  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.PlayerInfo[]>
struct Func_2_t57B5F9E47ED4C8F5611A672DF5C949F2D02972C8  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.AddPlayerMessage>
struct Func_2_tC36B775425D425FC2AF1D16C0CD70386004FFEA0  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Boolean>
struct Func_2_tE35FCB04208B127B021F241EA079464ACC551B7C  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Byte>
struct Func_2_t0BC506BED5AC85C37292910C8BDAD52A9F6C9986  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.CellValue>
struct Func_2_t22170DBE4CAFFA15631BB8B0CE681A822A3A160A  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.ChangeOwnerMessage>
struct Func_2_tAF26F2C22D87F8F669162B6542990070635D2A30  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Char>
struct Func_2_t26F6F63A0DA84EBAD15F6BCC374071D9F6C8B42C  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.ClientMatchMessage>
struct Func_2_t21336B204733D902134B7F0FB71D1D4D85655F8E  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.ClientMatchOperation>
struct Func_2_tFBF2D2CE91617E529AC8196465432685D724AEBE  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Color>
struct Func_2_t6DF06C835E4A0B6416DE67676C67EDBE1EBBFD78  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Color32>
struct Func_2_tA1D462828AADE4C7A521611177F1A49476CFE773  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.CommandMessage>
struct Func_2_tDB9FD64DF7D2CA31FFF8279F26075DFCF440D33C  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Decimal>
struct Func_2_tB643983D6CBD6220B483FEE44F3207125D7D3726  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Double>
struct Func_2_t589ED36A1F9FA112DA0EA54F4A46CBAEE886A07A  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.EntityStateMessage>
struct Func_2_tB637CE13E86654599FE7556001525095E552A933  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.GameObject>
struct Func_2_t1E82FEEB6D88844359FA37AD374ACC1AA39DDC4A  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Guid>
struct Func_2_tEC73485627298AE849634B626697EFC64D213D43  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Int16>
struct Func_2_tEE61CDD83F7808585CB68357D75DC2637FBA492F  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Int32>
struct Func_2_t3F9C9CE2DD4A455F6BF2ACA2CBC6A00343B798EA  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Int64>
struct Func_2_t0D88BB68D7BF644024EA7DFB9E7CDBA1EE7EF69C  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.MatchInfo>
struct Func_2_t0D870FCF01A0B3EEF980A0B74CCA07765A7552B3  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.MatchPlayerData>
struct Func_2_t747EDA19750AE43DE30476D128A90ACDBEAC48AF  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Matrix4x4>
struct Func_2_tAAC77BA3AEC5902F635E82ED90239D6BCD31F70A  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.NetworkBehaviour>
struct Func_2_t763657E22AECC9ED46856683B3045624CF6351E6  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.NetworkIdentity>
struct Func_2_t8D8F451E2C46B99D55F5781D36B4F981BE62BD6A  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.NetworkPingMessage>
struct Func_2_t3C7B620CCBA5E47EED93AC22DB224F015928D9BF  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.NetworkPongMessage>
struct Func_2_t57653632E080A4623E41FABED4B1B6707C4D1B44  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.NotReadyMessage>
struct Func_2_t9B160232C1403CEF10F27D1D1BE4368650EE22CD  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.ObjectDestroyMessage>
struct Func_2_t056E7181DAB40DF2A9D14BB4450C0DCC566784E6  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.ObjectHideMessage>
struct Func_2_t76221365B0738498867CB728129555B7A8617C15  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.ObjectSpawnFinishedMessage>
struct Func_2_tAE6F8E9F1706B23DD7BC49D738569171CF6E8C3B  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.ObjectSpawnStartedMessage>
struct Func_2_tBD4C61172F81BE2A292FAE166832A0324BB00B1B  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Plane>
struct Func_2_tF1646D6248B9372FDA9C49F858144441B68F98FD  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.PlayerInfo>
struct Func_2_t966D11E60D81CE62911BC8F172BA2326E5F49FE0  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Quaternion>
struct Func_2_tDF2B4816BD9F902CA19B4AFA6CA78C2A61867EA3  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Ray>
struct Func_2_tB124603C647DAC5696CA392A20B3EE6C213CCA26  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.ReadyMessage>
struct Func_2_t31743D3E7DDCFE0001801AD8A7459B2FE016E401  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Rect>
struct Func_2_t340EA86AEF734F68ACF5E9FF2A124F9ADDED52DE  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.RpcMessage>
struct Func_2_t5F7A34ACC37197D2AFFFFF3A63EE51E2B44D6607  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.SByte>
struct Func_2_t3EE5A9E53DE82B19DAC86C3D30AC7BD98D305238  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.SceneMessage>
struct Func_2_t8C43EB67A953E2DA4EA0F861438FEF6C6F87360E  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.SceneOperation>
struct Func_2_tA736A0330DE6964C91228A379FAB411414BBBB31  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.ServerMatchMessage>
struct Func_2_t2EFEEFDAE61B1D665B02E8D247FA7A8AE1B3D3FF  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.ServerMatchOperation>
struct Func_2_t4A60C2EC0E8ECFA6E6872E11BFAD744A805D5FF8  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Single>
struct Func_2_tDA855C737BB7E9546B48245EC0F04DAA56A1CB03  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.SpawnMessage>
struct Func_2_t1FE35EF0C4BBDC7AFA697EBBB5B4D64547326DCC  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Sprite>
struct Func_2_tB35F19D43C3D74BFE4F16B609D5DAFBCCE477EC1  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.String>
struct Func_2_tF047810C662C3A551DDB01290047E803F32DA440  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Texture2D>
struct Func_2_t58133EDD30520660CD4F542594E8D913BB704B55  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Transform>
struct Func_2_t8085A3B2562300C528C41159E557B58E555D6798  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.UInt16>
struct Func_2_tD28C61E3A0A47F71DB0CDCB3B43C7F13D4F82235  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.UInt32>
struct Func_2_t1FE4698618B38731C4E2D67BCC314778BA13ECC0  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.UInt64>
struct Func_2_t86251E51D7B41B00F1CC89068612DB2EE6F5FDE0  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,System.Uri>
struct Func_2_t2DBAA86C74F086DBA285C7B8ECE41C9F35E419D4  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Vector2>
struct Func_2_t9C4AED5DAF5E665602E3D752BDF50BAE7C2C5D0F  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Vector2Int>
struct Func_2_t1D54F05297BE0A4ACFBE3D87381C38332A2F7837  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Vector3>
struct Func_2_t9BD17C9037D00DFA5EFF1643A78C3E4300A31331  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Vector3Int>
struct Func_2_tFD61C39BCF897AD7E0777243349B4558DB639EC5  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,UnityEngine.Vector4>
struct Func_2_tEA21835A50BEE04419A104B2F4D8CCE015122924  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage>
struct Func_2_t4CD599531AE1FA8CA3CF4057D910B500DBF99E45  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage>
struct Func_2_t5DF2F494B8936184EE2E2F51E135F530C09B76D0  : public MulticastDelegate_t
{
};

// System.Func`2<Mirror.NetworkReader,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar>
struct Func_2_t842043449CFBC0F4C127867ED6917B5566AC5576  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// System.IO.IOException
struct IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// System.IO.EndOfStreamException
struct EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028  : public IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// Mirror.NetworkBehaviour
struct NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Mirror.SyncMode Mirror.NetworkBehaviour::syncMode
	int32_t ___syncMode_4;
	// System.Single Mirror.NetworkBehaviour::syncInterval
	float ___syncInterval_5;
	// System.Double Mirror.NetworkBehaviour::lastSyncTime
	double ___lastSyncTime_6;
	// System.Collections.Generic.List`1<Mirror.SyncObject> Mirror.NetworkBehaviour::syncObjects
	List_1_t8ED884B12AC29C4F4BDFB975C0DB057D2C0519AA* ___syncObjects_7;
	// Mirror.NetworkIdentity Mirror.NetworkBehaviour::<netIdentity>k__BackingField
	NetworkIdentity_t5C06E7EE595FF674F722D11C1397B12518C007AC* ___U3CnetIdentityU3Ek__BackingField_8;
	// System.Int32 Mirror.NetworkBehaviour::<ComponentIndex>k__BackingField
	int32_t ___U3CComponentIndexU3Ek__BackingField_9;
	// System.UInt64 Mirror.NetworkBehaviour::<syncVarDirtyBits>k__BackingField
	uint64_t ___U3CsyncVarDirtyBitsU3Ek__BackingField_10;
	// System.UInt64 Mirror.NetworkBehaviour::syncObjectDirtyBits
	uint64_t ___syncObjectDirtyBits_11;
	// System.UInt64 Mirror.NetworkBehaviour::syncVarHookGuard
	uint64_t ___syncVarHookGuard_12;
};

// Mirror.NetworkIdentity
struct NetworkIdentity_t5C06E7EE595FF674F722D11C1397B12518C007AC  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean Mirror.NetworkIdentity::<isClient>k__BackingField
	bool ___U3CisClientU3Ek__BackingField_4;
	// System.Boolean Mirror.NetworkIdentity::<isServer>k__BackingField
	bool ___U3CisServerU3Ek__BackingField_5;
	// System.Boolean Mirror.NetworkIdentity::<isLocalPlayer>k__BackingField
	bool ___U3CisLocalPlayerU3Ek__BackingField_6;
	// System.Boolean Mirror.NetworkIdentity::<hasAuthority>k__BackingField
	bool ___U3ChasAuthorityU3Ek__BackingField_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,Mirror.NetworkConnectionToClient> Mirror.NetworkIdentity::observers
	Dictionary_2_t27781EEAEE164B870331F779DBE0DED7F941F4D6* ___observers_8;
	// System.UInt32 Mirror.NetworkIdentity::<netId>k__BackingField
	uint32_t ___U3CnetIdU3Ek__BackingField_9;
	// System.UInt64 Mirror.NetworkIdentity::sceneId
	uint64_t ___sceneId_10;
	// System.Boolean Mirror.NetworkIdentity::serverOnly
	bool ___serverOnly_11;
	// System.Boolean Mirror.NetworkIdentity::destroyCalled
	bool ___destroyCalled_12;
	// Mirror.NetworkConnection Mirror.NetworkIdentity::<connectionToServer>k__BackingField
	NetworkConnection_t49880296B0FA972023F34582D7A41D7B63383E78* ___U3CconnectionToServerU3Ek__BackingField_13;
	// Mirror.NetworkConnectionToClient Mirror.NetworkIdentity::_connectionToClient
	NetworkConnectionToClient_t80F9FBDD786601CB93A63585D05BCAA1050C406A* ____connectionToClient_14;
	// Mirror.NetworkBehaviour[] Mirror.NetworkIdentity::<NetworkBehaviours>k__BackingField
	NetworkBehaviourU5BU5D_tB037699FB91FE996B291D1BD9E9941B2A6F53C98* ___U3CNetworkBehavioursU3Ek__BackingField_15;
	// Mirror.Visibility Mirror.NetworkIdentity::visible
	int32_t ___visible_16;
	// Mirror.NetworkIdentitySerialization Mirror.NetworkIdentity::lastSerialization
	NetworkIdentitySerialization_t95487F26F667AF97203016B8FF0359E6D31FB4C9 ___lastSerialization_17;
	// System.String Mirror.NetworkIdentity::m_AssetId
	String_t* ___m_AssetId_18;
	// System.Boolean Mirror.NetworkIdentity::hasSpawned
	bool ___hasSpawned_22;
	// System.Boolean Mirror.NetworkIdentity::<SpawnedFromInstantiate>k__BackingField
	bool ___U3CSpawnedFromInstantiateU3Ek__BackingField_23;
	// System.Boolean Mirror.NetworkIdentity::clientStarted
	bool ___clientStarted_24;
	// System.Boolean Mirror.NetworkIdentity::hadAuthority
	bool ___hadAuthority_26;
};

struct NetworkIdentity_t5C06E7EE595FF674F722D11C1397B12518C007AC_StaticFields
{
	// System.Collections.Generic.Dictionary`2<System.UInt64,Mirror.NetworkIdentity> Mirror.NetworkIdentity::sceneIds
	Dictionary_2_t55A938BB79E925B7A9B5D7F7C857728FC8864C14* ___sceneIds_19;
	// System.UInt32 Mirror.NetworkIdentity::nextNetworkId
	uint32_t ___nextNetworkId_20;
	// Mirror.NetworkIdentity/ClientAuthorityCallback Mirror.NetworkIdentity::clientAuthorityCallback
	ClientAuthorityCallback_tD9013EF9C1BA9FA8A240D80D87F90C1DED964CB0* ___clientAuthorityCallback_21;
	// Mirror.NetworkIdentity Mirror.NetworkIdentity::previousLocalPlayer
	NetworkIdentity_t5C06E7EE595FF674F722D11C1397B12518C007AC* ___previousLocalPlayer_25;
};

// Mirror.Examples.AdditiveLevels.RandomColor
struct RandomColor_t85537EF520483D7F0DD339CC9F802E1A53CC282F  : public NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C
{
	// UnityEngine.Color32 Mirror.Examples.AdditiveLevels.RandomColor::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_13;
	// UnityEngine.Material Mirror.Examples.AdditiveLevels.RandomColor::cachedMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___cachedMaterial_14;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Mirror.Examples.MultipleMatch.MatchInfo[]
struct MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572  : public RuntimeArray
{
	ALIGN_FIELD (8) MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 m_Items[1];

	inline MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 value)
	{
		m_Items[index] = value;
	}
};
// Mirror.Examples.MultipleMatch.PlayerInfo[]
struct PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415  : public RuntimeArray
{
	ALIGN_FIELD (8) PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 m_Items[1];

	inline PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// T UnityEngine.Component::GetComponentInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponentInChildren_TisRuntimeObject_mE483A27E876DE8E4E6901D6814837F81D7C42F65_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void System.Action`2<UnityEngine.Color32,UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m7FD44B2A35F4476C6ECC24DA778FEB2EA6A0AAA3_gshared (Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkBehaviour::GeneratedSyncVarSetter<UnityEngine.Color32>(T,T&,System.UInt64,System.Action`2<T,T>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkBehaviour_GeneratedSyncVarSetter_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mD43D62C7BD24575258CA0CF86ABD19131A08BE2D_gshared_inline (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value0, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* ___field1, uint64_t ___dirtyBit2, Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* ___OnChanged3, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkBehaviour::GeneratedSyncVarDeserialize<UnityEngine.Color32>(T&,System.Action`2<T,T>,T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkBehaviour_GeneratedSyncVarDeserialize_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mCD3E867438BB843F76C76561EC9F138D8CC6694D_gshared_inline (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* ___field0, Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* ___OnChanged1, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value2, const RuntimeMethod* method) ;
// T[] Mirror.NetworkReaderExtensions::ReadArray<Mirror.Examples.MultipleMatch.MatchInfo>(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* NetworkReaderExtensions_ReadArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m3CE6D0D53E5BA355729F19428F259391B4A375B5_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// T[] Mirror.NetworkReaderExtensions::ReadArray<Mirror.Examples.MultipleMatch.PlayerInfo>(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* NetworkReaderExtensions_ReadArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_mC8AFAB257D0DC0249D387758D4F204833AADE730_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteArray<Mirror.Examples.MultipleMatch.MatchInfo>(Mirror.NetworkWriter,T[])
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m0946D038BE696722FB3ED0523DEC8233C19FFAC2_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* ___array1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteArray<Mirror.Examples.MultipleMatch.PlayerInfo>(Mirror.NetworkWriter,T[])
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_m348C67F42E07DE2F8A0295C5B388A6985FF81A0E_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* ___array1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Byte>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m275566C978A87120E590B2AEEC6673681F0D0FFE_gshared (Action_2_tE894829A2C1154E5BF43E1E37F8D5586426273A0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.Byte>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mC78C0FAA42EA1557B12A0C19CC155B84846FEBF2_gshared (Action_2_t0EA2F4062B5A7AF23762E1CA13EE62E373B8063A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.SByte>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m1ADBEEBD003AEB0A008BF4203A9867B8F0BA1278_gshared (Action_2_t3392E551DFBD7852E59AA9CBE97E3FBB0752EB4A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.SByte>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mD6E7276D2B77469DFF19FAF60E563F9643F0639C_gshared (Action_2_tD06D328EF21B755A8744E094F143390CB2FFBB4E* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Char>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m4DA3B4ABECA91C1BA3BBAE4B3C4C08A58D393A4C_gshared (Action_2_tFC1CD94ECC9BBC2047B3C781A1B83B2DF9693300* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.Char>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mFA30B851A9481A0E19837178DC6713749E102BD0_gshared (Action_2_t2BE39FF3B09EE50AA0545282A8EE5587BFFB2C5F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m460C72FDDE5FF8033C7BD19A07CF4E3F473F7414_gshared (Action_2_t5BCD350E28ADACED656596CC308132ED74DA0915* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.Boolean>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m9FF8E5ED12C77689F6AB3235479B7036224952D4_gshared (Action_2_t19DDF6787EEF711A74EADD77E4E6F3D956E2C28D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Int16>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mABB69762BF0C1BAF5F0A3FDD5270A47610965D90_gshared (Action_2_t914070E97ABAE8FBF5DB5B520982E5BE6F22FFA5* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.Int16>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mC89E476DD21F19A8C63C5560E25A2F8C36D5338D_gshared (Action_2_t0720E8845D2D725B998A4D839E9CD687E9AA8A63* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.UInt16>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m416BB4EA5264A588FF09D1E60465F186AFAAA7C6_gshared (Action_2_t89BE0FE525BF3C6B2736E259A711D12E7BC72750* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.UInt16>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m6B2F99B86ECAA36AFD23FD69EA8C5E1553ECEC6E_gshared (Action_2_tA6DEFB39C08E44C2B644C1FAC396492A4A6B8A21* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m07C6392BB276FFCCFD4E495842992EA26FA44882_gshared (Action_2_tAC461AE4F7B507965CE2E6A32853473F8C02CD75* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mC4BC8A0AA1C664D10759568DE71018E9D5C339E1_gshared (Action_2_t8B8196A491839286593B1D6C22B7A0895C6C8551* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.UInt32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mD30FDAFA53F9F14951015A305F3F2782418E646F_gshared (Action_2_tDA74CED516153CD7F42CADEB337C93F4AFDC8DDD* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.UInt32>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m30A0F17BA067B84E683B953EB6E436A247766346_gshared (Action_2_tC12C303F4598D6C6EC0D86E1FCEAE767AE57532B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Int64>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mB5B0280FB694D8743982749E1ED3472CD323F172_gshared (Action_2_tBAC40DD9F3FDB12141E0F973A3E6DBD9B65D5E35* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.Int64>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m3DCA8976EB904F61BC50F4D9A59E5805084B4E55_gshared (Action_2_t5F67668459C62E1686E6F70EC800F84023CA5ACD* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.UInt64>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m8939564F9B7F5BD0A2B996E75501A72B094FD03F_gshared (Action_2_tC2C04F74903D0BD2838A81020541B5DAF476227C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.UInt64>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m49093CC0F8BC0E50E97397A4D141B1F5F8140C6D_gshared (Action_2_tE2135AF9B8B4237B445171FB2F8FF95B6A97B7AE* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m600728814D64949FB42DEE273E5E146A5E015BE1_gshared (Action_2_t4A5313D1C1FEF099C0E5969104BDE957CD82CF22* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.Single>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mB59E849E9D35F11C7AAF53EF2AD3D152FF05A23B_gshared (Action_2_t228E1A20705B669EA4EBD29D890F929E58CB0068* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Double>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m00A3B34AD5FB6A28E99E8F2FFA8E5B8CB61DEF1D_gshared (Action_2_t20E1B0E5A61C42E2C74B90596ED942D921D42DE1* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.Double>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mEF3263AD558AED9B7592D24A5A23EB4B92B7551B_gshared (Action_2_tE27D0AB32B79423AEFB88AABD75A141FB5EA0BED* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Decimal>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m177E2A82A82BA0668971BDC4FC2C3DB599EEA81B_gshared (Action_2_t469C223ACF2D27BB434BE7D9364B434942AFCF78* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.Decimal>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mE6F19ED15FE74006B01225FE99514FF56D36FB31_gshared (Action_2_tE1202DA57F63F53904EB9B7237892A05618A9DC0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m6A0E7FE9DF9AE6C4BEE58611CB55F64FC3D79052_gshared (Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.ArraySegment`1<System.Byte>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m78A92E408AB0E0C2654D69E27C395066B6C2968D_gshared (Action_2_tA0B9181EBC3DE1D849BF7E7F4A5AC0BFF3ADE4A0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m0579D8063D09880CEDC4C9D1178E80DF3B0590A1_gshared (Action_2_t15D6234343A6C232F6E9C1563A9666FD379F4A1B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<UnityEngine.Vector2>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mD49A36F5FFAF197E90B5952E9633AE309843E177_gshared (Action_2_t4713D34F9988FCB698B7DA0A7D9B8A204C6CA2B2* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mE637AA364DF34EED2F99253A70BFCF870A94CAF1_gshared (Action_2_t0F28FD6DF12DE3C0C0A3C8670D6FF563CB91D7DE* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<UnityEngine.Vector3>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m682410A20D07B6655C32D6C559873A009A4FCF1A_gshared (Action_2_tE22C5D126868AABF1A79F8592617AD154D66B8F0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mE8E693B9421FE0BCF376B60270B6CE035B9C0445_gshared (Action_2_tA4CC875AD1B535E16B296C7E63CA2647BA2009C9* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<UnityEngine.Vector4>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mB7E93B9BC80454A09A170D043A427932004F2B3C_gshared (Action_2_tEC96CFDA9D98801198609F46E307BF1C17C0372E* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,UnityEngine.Vector2Int>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m5A3C95C6CCA5A6FD7D037F30DF49E19272EF37D9_gshared (Action_2_t2A9111170E5F9F9711A910F2CD7B650F9A4D830D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<UnityEngine.Vector2Int>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m0F92180E2494ECB80B8A42DC605736360E6DF5B2_gshared (Action_2_tBB26490704E8CF5E6DB62C8034C2C634B511557D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,UnityEngine.Vector3Int>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mC564AEE7A8E8FAE71A3742A10AEBEA03FF0629B5_gshared (Action_2_tA07091F44E5128C0BA44B35E278263CCB60A2D74* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<UnityEngine.Vector3Int>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m56BC792FCBD78BC65CC2EC09BA07651D3871899A_gshared (Action_2_t040A45BD91165322688FA8D225544BE87C45EA29* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mD0DE34B532C4AA36FD3458BF59883E7050DC2209_gshared (Action_2_tA6D2280AF26A98A178E1D2455D1B9A83FADE20C8* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<UnityEngine.Color>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mC191176DB6B0FCB0B872565F7544590AACBB9D0E_gshared (Action_2_tCA122DBF107CF0C679F98E670FAADFE8FE1BEBA8* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mBBC00E0314287F9C6628AB9F74EF98D3A22AF75B_gshared (Action_2_t1C11CECBE63666FF1C08415A1B683FEC522EE8E8* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<UnityEngine.Color32>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m3BB9E2D7B6FDB8E6AB5B314FEA0C3745639E9BC1_gshared (Action_2_t3B8EACCB52F36391CCC830BE3012A0973DBEB8C4* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mDB3CB076F607F5410E97DA2180E2152FAD3DEF6E_gshared (Action_2_tDE5AC57A8E9A00B7BC0B0420FE43635FA84A2E8B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<UnityEngine.Quaternion>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mFADA1C21D43DA1112959F405E82247EE225DBD58_gshared (Action_2_t8EE27287CA86F665416BCC3CEB75BFF21B6763B4* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mD7493C3D3AA2FBA6DF6765E0C5EBAA710C98DD3C_gshared (Action_2_t5F545431D161B1E92A435ECB8F3DE7D923EF5CE0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<UnityEngine.Rect>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m0AAD1089E22F4ACDD6DD2CA994FCD91DFB547158_gshared (Action_2_tF2E3AB37232E746CC65E72C46C18DFA82E69AA43* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,UnityEngine.Plane>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mAB8DD07A760641CA0BEB366A71F9F82E12B40FB0_gshared (Action_2_t8848423C02445C09CDE79D766D804D11639BA6BC* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<UnityEngine.Plane>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m06BEB5BDCFF73A407C621A5137C7D0D60A61802B_gshared (Action_2_t3C124597F3448FD87CDB4BCD38BCB06A4EAEDC72* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,UnityEngine.Ray>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m00F6527E708A7FF1670B7F31EE846A13BDD05D6E_gshared (Action_2_tE16F1E549385589A493835919A9AD6123F85262C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<UnityEngine.Ray>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mA6EADFE77C5E972BE22C1BDEE6495E014A2DFF9C_gshared (Action_2_tD35371E8ACD943C5D80DA61CE07E0B6AB8ECC097* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,UnityEngine.Matrix4x4>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m01F2C5B90622E1ED10BA953081D61D070F9DB80E_gshared (Action_2_t3C64BBA8D7C0CE1AFD1B68560D964965065531A6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<UnityEngine.Matrix4x4>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m6817D47D12FE915AEBAD895890F54952084011C9_gshared (Action_2_t7ECC231A0F21A8EAFF3CDDE28C0107EAAF418CC0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Guid>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m0D60CCCE5E1F6876A78A508FED68F61B2ECB30D5_gshared (Action_2_t6A986B6DBED6BD578579C5BDA97931C92B339E4A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Nullable`1<System.Guid>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m25956627FFC58E8B05165191D0CDA23D1BA56E29_gshared (Action_2_t99CB75AA9FD19A7683885E5A2F24C3AAFCDDC278* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.ReadyMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m9C548C503F9756E0E827125D82A5364ED1BC8A73_gshared (Action_2_t0D65532CCC13FFF343A5DFD64A725437D3924032* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.NotReadyMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m9B86B8FD5F2E498DED165C15CB29A97C0FA9D464_gshared (Action_2_t2920C412E842E4598BD848335A3EE80C70661D15* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.AddPlayerMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m07A76F94AB6D967EB721874EF48BE8D8E3BF8434_gshared (Action_2_tF27DDFA21F48F9D313ADDA7441B963FC9AA6057F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.SceneMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m0C5368CFA99831F9D7EC0ED13B1A3518F4635F41_gshared (Action_2_tCE32AE3C7B7EE2A62D0DB6344F1D6B6C87B6EB36* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.ByteEnum>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m7CD05CED2A073EFA5EAF5610F4FB3CD26D80764A_gshared (Action_2_t41A214EE3EBB7463ACA8699A9C159476EADA910D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.CommandMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m48030241C18FC9C0BB3511E4AE52C0EB2169B778_gshared (Action_2_t4835364398852785232767ADC8361F049FEB46C0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.RpcMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m964236F341659B98C47F8BE7C5BE04BA65B44B4B_gshared (Action_2_t33F282E45E567B51FACA5C6DCED76A2BE1D80AED* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.SpawnMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mCF284042D6F1941A23E600BAADABE17FDA6997A4_gshared (Action_2_t195177EAA533C3206581DBAE7D103683E0E6B2B5* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.ChangeOwnerMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m2708719FC5C3991F88B255F63984BD42F6702196_gshared (Action_2_t88B48FA22730395EF1E51A7A2972771D031A5C9D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.ObjectSpawnStartedMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m829FE0C79B606D936AA3AFAA429A18D49193602A_gshared (Action_2_t6650D678F3ECC975E373718FBBCF4C75B61A7BAF* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.ObjectSpawnFinishedMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mA37AA6E4AEF79ED86433B22C41A8BC1F6BF74B23_gshared (Action_2_t7AFFCE926D1238FF71C1ED285049DABEA47E1CF9* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.ObjectDestroyMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mFF568B5AAE8E90AF27020509FCD58D0A5E0ABFA1_gshared (Action_2_t699BD3CC86B570FF349D8536FD855A26D3C10DCB* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.ObjectHideMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m3D9A5AD9337E240C8E048907CF65B5E91A4A75E9_gshared (Action_2_t73491674AD3965BB99682243E91DC45F3FDDCBD8* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.EntityStateMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mBD3B0FE04055EEBE35C50814C47D34F11E482B59_gshared (Action_2_t1EFB12E6CCF58902FAEAAC4C2EB883B71778DA47* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.NetworkPingMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m5F38E72A4FA2703145CA00EE6C7FF9B6CE832FD7_gshared (Action_2_t12EDEA4C2BB1EA955C81CDF6F223F749C561408B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.NetworkPongMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mDD6145E1B7E7AC69B01AF066136A477EA4C23A3E_gshared (Action_2_tFB992070C2C2C01311FEA3098D84E5EFCF050153* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.Examples.MultipleMatch.ServerMatchMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mDE0AB6E21D582143046BB6B026DFD3EEF5691978_gshared (Action_2_tE45C2C7B921B579F30A718DE9E11E71E974CC9DC* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.Examples.MultipleMatch.ClientMatchMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m1F92BDB0C5659D8FB6E9FF31EC929F27CF47CC42_gshared (Action_2_tAD390F10177FE26729242610D47C9BAA921F5745* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.Examples.MultipleMatch.MatchInfo>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m409669ED144DA290F9240B006B94E552E38ADA68_gshared (Action_2_t6F7E4F91AFFDBD3655646C203278FB18892BCA0C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.Examples.MultipleMatch.PlayerInfo>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m3B5B056A9402FF85E8BF3F30F8F842F9DD99AFA8_gshared (Action_2_t523EEFB49FBAF086F44C1B6AC796C385A1043214* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m1CC50902B0A8B46B92F8A64E3E6CDA863670EAA6_gshared (Action_2_tF7E35EE8593504574A7421730DCFCEEE05B1DCEE* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m534AA06214BC73B888D0B106431D5F8657EFBADC_gshared (Action_2_t4E383B0432304D07A4A7B2F37BC993DFB7E3450B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,Mirror.Examples.MultipleMatch.MatchPlayerData>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m724CC6A662995A8BDE491E680A0B19F566DD3B2E_gshared (Action_2_tD8779192920625E7824209374872284249D11C5B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.UInt16Enum>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mA2463D0820BDAF6210F929F698261F87A1EFB5A3_gshared (Action_2_tDFD8BF49304CDB0AFD0BDE4FDF3BCA93F8278878* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Byte>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m826425E2D4DB5E667248C6A79A1972CA0A60EB77_gshared (Func_2_t5C9D9EE08A80057DD8A6BF8F0E7483819FB6E341* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.Byte>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mD6FFA3BD35C1822C8D3F0650F141A5DFE148E50A_gshared (Func_2_tDF175FA34A7EF4ED084CEC34DCCD4FFCE7187DAF* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.SByte>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m9994BCD4B83B79BC1DD67D7F694974215DCE8EB0_gshared (Func_2_tFA7E684C9AE1F9ED14B06D3CC6DBC2C2492486C4* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.SByte>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mB41C506907EB6ED4588FD3FB76C3067096E87552_gshared (Func_2_t2A3B1C6C9E76A988821205B024AB085553C52654* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Char>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mE0224E3E48B05DE6D6D3FCAB652B609EAE8DD543_gshared (Func_2_tA8F6CB924B4548E42010325397F16C187C098225* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.Char>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mE4C517298BFB1ACEAE4E479F45230037927D5369_gshared (Func_2_t76784AE3FD6485217951AF0E4DF1EF027579C615* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m13C0A7F33154D861E2A041B52E88461832DA1697_gshared (Func_2_tE1F0D41563EE092E5E5540B061449FDE88F1DC00* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.Boolean>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m720FC4F90AF22CC262CED04C6117FCC8058561A2_gshared (Func_2_t3720B07918CDF70232923BF3A8CEAD96E1B97B56* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Int16>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mC7A0EC33AC48580094225C83A288E8EF80034BFC_gshared (Func_2_tC43D5377992B28BE94D1A61A31D05D01B3153C90* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.Int16>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m5D68B8D9C998E5AE4CC3250A8DDDE79C9BD5EBCF_gshared (Func_2_t1F4D30F7C5A1C5A5E1C32C0EE2526402D0256042* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.UInt16>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m0FFAC302C951F47E7D69018BE8FDF52D098E712D_gshared (Func_2_t8F75D16C6A6CD98824844B9D931354AC0DBCB055* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.UInt16>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m5FC5AD57F45CF1B53B7B60C4729EA389C1567C5D_gshared (Func_2_tE81BE34070BF1C8D4E290B73ECFAEE943B5BF0CD* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mEB7603EDE6D79A62E5BD74A896F030D2C9F2A821_gshared (Func_2_t9A0D493A82DCC47C9C819A3B045E02D9B5DDCE1B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mB90F61A92C810F7BCB3288E608C7B6766FB5A120_gshared (Func_2_t9BDD2E959989A0F342560C7A37F50108887A6F4F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.UInt32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m2F35D9DF8C659B83CA70029ACBA44930CD42E90D_gshared (Func_2_tB86D019F1289E2D123C00796B373933613385952* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.UInt32>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mCE51F65339B9D55BA56037F23360F38A3B6113D3_gshared (Func_2_tBE16A3C03B9956B3EDC2954831E6D1887E0C527A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Int64>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mD54790BA1BEF5C7D8025676612EB0FF0A97A87F6_gshared (Func_2_t78D13C74B0F5A1DD640F8722F0AFB5F5144EAB97* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.Int64>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mA0019FEC08A5C2E72206C2A4FECA4C228923CEDC_gshared (Func_2_t8B4541DCF3576ADB91352AD960AE14B97A5199A7* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.UInt64>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m3579BBCEF5B00795682D166C40CBA47618B2D563_gshared (Func_2_t0041BDC545AC23D00BA1439051E79D5351CF315C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.UInt64>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m0D50C137716B8F564918D800779AB562683454C0_gshared (Func_2_t9344B2FAD5D0BA160A247C7EEB8D7E54CB6C15DA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m04EB988C3F849C1BE164FC6656C2281DF47EEAA7_gshared (Func_2_tB5C40A90702B6A6A2E315FD927EEFC9FB69F2B12* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.Single>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mDFA24B5D1C39CBE639807C5095115EECCAA39D40_gshared (Func_2_t09C944E9E92CF4E666BD2A414C1050B0A3485DB0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Double>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mC2F9460DBFF9A8659492AC19F4B9FCA63BFA48A8_gshared (Func_2_t5D850B409400F6FC6B650829D4B758F5899212B1* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.Double>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mD5C38515FA9E2063BB33617714D4F4A0F5CD4DD1_gshared (Func_2_t1B81A262430D3552787048D67101A5DB5FB0FF1E* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Decimal>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m2883355E0E294A4A3D740AFD1E2D13121EAE4A1A_gshared (Func_2_t5D415239CB4CB3874B276935A09EF4D650EF3705* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.Decimal>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m8B3991F7314BBA8B50BF1F8436068A737FD8C13A_gshared (Func_2_t1EE7B88D4216B85C8665FB82E512E56E07A64752* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared (Func_2_tACBF5A1656250800CE861707354491F0611F6624* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.ArraySegment`1<System.Byte>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mA8804B68FFF2F0831D450EDE4380821D8528B16A_gshared (Func_2_t54336CDCB27B475E725C043FEF2FFE34F807E1E1* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mB8352E429272D346070147A942612E0C937BD03B_gshared (Func_2_t127163694D7C66D0F32B7F8F5BB2507F7516DEE4* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<UnityEngine.Vector2>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m96218009E13DF8E3215C5B5155ECC9729B41F974_gshared (Func_2_t8427DA63AAE4E5A7888CBF062568A308A9879293* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m55582D0AF64DBB8F297B71E8B68F40E374CE1910_gshared (Func_2_t1F9887E0A0ADE496D09CAA16DBA7B19D5579727E* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<UnityEngine.Vector3>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mB25549B0BDE31FBD830182682B8B0F258EAEC5D1_gshared (Func_2_t282A2068D4E9F64E26AE2DD0625AC8F8D12A4C0D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mBE75211545C5D3C820BEA11B595AB466F6D45FE4_gshared (Func_2_t01EEA5FE255AF6B9B90E3CFAE8B5571BC66E8302* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<UnityEngine.Vector4>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m32598ED0A528EE5B56631DCA23606800BF1E3420_gshared (Func_2_t2917E538BA6D9379979D6190F3F4A9BAF27F7CFC* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,UnityEngine.Vector2Int>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m366F848FE673CED0F9AAC46B542B8A8A2C377A89_gshared (Func_2_t51AFC293A9EE45091BAFAE499602C1CAADD7DE04* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<UnityEngine.Vector2Int>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m8750059BE69D44747DE0E741B5C550033140C8B1_gshared (Func_2_t761519B467969C404270B9B774F5FED211CC87D1* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,UnityEngine.Vector3Int>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m93FE856D524A39884536A4D8FFFF9FA8BC7DB916_gshared (Func_2_tFBB8AD85FFDEE93D72698FE0EC4BDE7FC96D5420* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<UnityEngine.Vector3Int>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m82052C7AC435234E2794624952FCF4B56746186E_gshared (Func_2_t091A46A39B1CF6E1E553370FA483C3623915A30F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m851A616484C4EE142B945D4B08A222778A25F038_gshared (Func_2_tB58BCE382BE21A0ECA2C8642716DD941FB670EDA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<UnityEngine.Color>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m065EB34D4CD158D458CFAA74F156078986C6B896_gshared (Func_2_t277DE8FCC4D58D9C922F2E9E48C9836A9730E379* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m28742C1B87FB886C579BBD88B3EA716E47C56F65_gshared (Func_2_tE51521503EC6E99EE2D7CC1F15A72876BFD3AB3C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<UnityEngine.Color32>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m9F05B0A4DBFD5C15DD3C15D847A3710A24CD5C94_gshared (Func_2_t470D5B37E4893766336577FE720F58F7F69033BC* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mD8552D6390B3EC85469DBC405A43A3BE7C1F939A_gshared (Func_2_tF9A1676D5CC48AA93FA04FCF9B2FB5E3D6D8332E* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<UnityEngine.Quaternion>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m4B0A30A5BF099E6F10A3906F02D6920684BAE99D_gshared (Func_2_tB0FE6C770B3E7D10673472CEF7F7A404A313E064* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m864DC3F910DBD1416040134913DD4D5E40C52F35_gshared (Func_2_t69DD684ECAE49CD391AD3F6B556465178004A9BE* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<UnityEngine.Rect>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mC2282DC5B9FA03C78072AFB7949F1C61701E0A57_gshared (Func_2_tEBBD26D2A474EADEE27827872BBEE2114CC4ACFA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,UnityEngine.Plane>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m5775276C45B867CA3DB663B2D8C444204A8A6345_gshared (Func_2_t71AA5F9303BD84786B5E76C59E2EC23602AE39DE* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<UnityEngine.Plane>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m7C8A028547A62CFAD1F55AA9A62D16CD24A2A239_gshared (Func_2_t39A050645526CE9F1FE48DA80C637A1094C096AA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,UnityEngine.Ray>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mCF3F8A5B698C0456FE17CBDE86BF91173067D539_gshared (Func_2_t040C6DBD8E79D890B4C62419CA5F6A904EA1C1DC* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<UnityEngine.Ray>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mC189715B5F495CDD877F81F199A0810F51DCD9B2_gshared (Func_2_t0333326FB3B060B71D5CC50138A417CB745AC9CE* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,UnityEngine.Matrix4x4>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m09AECDE4994C9122308EC501F2BCFBDCEBF98CC3_gshared (Func_2_tBF1D7BFC66E867B07DAB51D1158B6014CDCFF80C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<UnityEngine.Matrix4x4>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mF02608D0427390919A28909F9DDEAB9AE3360C22_gshared (Func_2_tC1ADF3DC8C492897502820946F1DEAF640DEE4B9* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Guid>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mF555B385B4BDA7CAA449193CC6171BE7C0962947_gshared (Func_2_tB966008A65EE3C580BEEAEA7E13ED7A153257838* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Nullable`1<System.Guid>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m2198BB76DA1151D5468437793BDFC4B8677D1F23_gshared (Func_2_t0947A1DA2DA6CC217BCF51C2047F2A2F77FD5E63* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mCF76F13F01BE18A846A15ED3D38AFCFA0EC78FC2_gshared (Func_2_tFE38F936D16387AF1B3CC164A91B33888267FEA5* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.ReadyMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mCD2D383C2FB019239FE1DBEC7EF8FB424B5DBC0F_gshared (Func_2_tA458E8728DB879480AFF42AB43EDE5D5424A6AC1* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.NotReadyMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mB5DD68FD377D0B4AE63535B9DF23D7ECC424BF45_gshared (Func_2_t41C476CEDB33511DFC03F63715BB2AB6D98DAE7D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.AddPlayerMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mFAA6E18BE4D8A686CD184B5B399BE74743056209_gshared (Func_2_tC6F47BF3D64D54DC390DEA7C3BC733C6563E0D10* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.SceneMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m8631958585DE743935CBB3C962D7557967A03225_gshared (Func_2_tC1615AFA2FE69A46E605C59E2EABC1295CBCCE9C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.ByteEnum>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m7BE3802A697DD49E577E0B122FD3C96AF4AEB6D5_gshared (Func_2_tFF45FB5FEDF57DF29D6702B86F41D1EB73B8BBE6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.CommandMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mC66467E2F6FDE0762616CF893F4CCDA0CC7C0766_gshared (Func_2_tF62A213C212F79C67E8C43D5DD7C70750D6C6D9B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.RpcMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m9ED02C7210DB2224AA2286AA7CF4B26E3B728C03_gshared (Func_2_t19334C7D7D41A14D93AD26EC9D161D1A3255FDBA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.SpawnMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m032D89FC15227676D2D18D443F179CE229A3C232_gshared (Func_2_tE41D048BD903F47B6358EAAAD01C1B223BEF9A1B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.ChangeOwnerMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mC0FC7199340AB97EDAD69AE90C60DA8A9CE4CEB1_gshared (Func_2_t874CCA1FBE84B81386C19F66B459479242A53420* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.ObjectSpawnStartedMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m8301D01266BFB09C863C904500B7EB9D86A296F2_gshared (Func_2_t3104CBF8C4A12DD26E48A03D7FC94DA15B2856E0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.ObjectSpawnFinishedMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m974B92BFD44CB8869450E84BC229D289EA342ED7_gshared (Func_2_t5913B836B9B34B4A3D7F7ADD0F594DCD8782C3E8* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.ObjectDestroyMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m14645801EB1832479F0856137D68C7408E823C68_gshared (Func_2_tC86CB06FACA637370854ECEEE2717F60B64B34BA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.ObjectHideMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mF0634E1C586D4F090250925858969DD40AC91E39_gshared (Func_2_t461B9B3CD7F177FBFBC2987142C789614DB49B8B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.EntityStateMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mF31C8EFEAC54CF635E51D7E3FA4A9783B424F0FB_gshared (Func_2_t399D28FF903B89DDCF4673444BB3AED168ED1C15* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.NetworkPingMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m14EA83B893C7636539B4D80A6E1A07B9528B355F_gshared (Func_2_t36712FEA5ABA9573E272B9F326AA2560F8F134B0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.NetworkPongMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m217F5FB2F2FE3C57D9EB739FDC48257A21D5C1CD_gshared (Func_2_tAB2BC2D7D8DCD30DD421EA2B596E29A851CA6974* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.Examples.MultipleMatch.ServerMatchMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m9D9B0F568D33F486468BBDC03A08A6674923D9EB_gshared (Func_2_t47308A521FE0814186F73F78DB95A5C83DB2163F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.Examples.MultipleMatch.ClientMatchMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m3D431B743B14AFC5C39381BDFC9C3398FE1E824A_gshared (Func_2_t54F672BBBEAC8D2709215BC414613CE56FB8D890* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.Examples.MultipleMatch.MatchInfo>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m8E1D68853309ADD19D6ACC13BCA4C8A8B65E3839_gshared (Func_2_t2BDCDF710598521A63032F1F4A7151427876B48D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.Examples.MultipleMatch.PlayerInfo>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mB40E9E079583CE4F4DB26BF7778C6042B07B60D9_gshared (Func_2_t69DF1BAE52533FEFDCCA1294C8BEBCB863C644F8* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mD156B6DCCBE963380AAEC4144B32001BB9EF09EA_gshared (Func_2_tF380B94DC70C773BB37CB699FF1CDD8A617C3D1E* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m9B92330D0970FA0ED7A79279EF247B7F2735F7F4_gshared (Func_2_t558EB0477BD7C161CEBA3CD3689485898B6D0898* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,Mirror.Examples.MultipleMatch.MatchPlayerData>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mF886556BBCCC11410D64B900FFC8934FAE5C0094_gshared (Func_2_t7D1179805331926FF931916FE392A6660AD491E6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.UInt16Enum>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m310FEE70AC898B237CF249B0F77DE33197C37314_gshared (Func_2_tE7F8E9A788049ABBD8E43380184C13BB07CE42C6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBlittable<UnityEngine.Color32>(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mDB472FF359C94951E696CB3FD4F6016CFE6F82B7_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBlittable<System.UInt64>(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m869219CA464A4A9CCE03043BF274E8EBD19428AC_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, uint64_t ___value0, const RuntimeMethod* method) ;
// T Mirror.NetworkReader::ReadBlittable<UnityEngine.Color32>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B NetworkReader_ReadBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_m6DBD713FC553C165E92A9ABA7691638AA9C2E7BD_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) ;
// T Mirror.NetworkReader::ReadBlittable<System.UInt64>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint64_t NetworkReader_ReadBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m5B0E578FA24816AF3240FCA5BFF9353FBB4B958A_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) ;
// T[] System.ArraySegment`1<System.Byte>::get_Array()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_gshared_inline (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, const RuntimeMethod* method) ;
// System.Int32 System.ArraySegment`1<System.Byte>::get_Offset()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_gshared_inline (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, const RuntimeMethod* method) ;
// System.Int32 System.ArraySegment`1<System.Byte>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_gshared_inline (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, const RuntimeMethod* method) ;
// T Mirror.NetworkReader::ReadBlittable<System.Byte>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint8_t NetworkReader_ReadBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mD424A6F72F431A64491144A4321E8915B2A9F179_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBlittable<System.Byte>(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mCDD7CA43B0B14857B8777A663BE02A0AA5917764_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, uint8_t ___value0, const RuntimeMethod* method) ;
// T Mirror.NetworkReader::ReadBlittable<System.UInt32>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t NetworkReader_ReadBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m20406E9280C5EE4AA46F5E308A00B1CE728A70A3_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) ;
// T Mirror.NetworkReader::ReadBlittable<System.Int32>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t NetworkReader_ReadBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m4436F2F119D575AC7A0EA84CCD5C3CF655A3FFFC_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBlittable<System.UInt32>(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m3F8565D904BC20262924C7AF8BFBB3F7FE770535_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, uint32_t ___value0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBlittable<System.Int32>(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m5745AC0FB4E1ABCB68691585D1B48F92DA99AEFB_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, int32_t ___value0, const RuntimeMethod* method) ;
// T Mirror.NetworkReader::ReadBlittable<UnityEngine.Vector3>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 NetworkReader_ReadBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m17B9EA1CB3FF9F5A7936974AAD62B19383E2D69F_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) ;
// T Mirror.NetworkReader::ReadBlittable<UnityEngine.Quaternion>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 NetworkReader_ReadBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mF27EFEBBC75EBD54AF10CE92FF64D5C780A97A69_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBlittable<UnityEngine.Vector3>(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m6552B19126A3F040F7E78F41CEB63CA85B0EF8BC_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBlittable<UnityEngine.Quaternion>(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mD95146DDEF099B2575801811770B7527E72A4969_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___value0, const RuntimeMethod* method) ;
// T Mirror.NetworkReader::ReadBlittable<System.Double>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double NetworkReader_ReadBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m9F3B82FB2EF977402985132FD7A7B3F531AC32C8_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBlittable<System.Double>(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m520D47927C9E53348DF0990757101E4408B4E4E8_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, double ___value0, const RuntimeMethod* method) ;
// T Mirror.NetworkReader::ReadBlittable<System.UInt16>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint16_t NetworkReader_ReadBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m5CB828366095AF0971BD2196AC15F944092E1E91_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBlittable<System.UInt16>(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m7A05C5F5DD5D33FC126D2DAC895A096EC3402A14_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, uint16_t ___value0, const RuntimeMethod* method) ;
// System.Void System.ArraySegment`1<System.Byte>::.ctor(T[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20_gshared (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___array0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method) ;
// System.Void System.Array::Resize<System.Byte>(T[]&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Resize_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_m82FEC5823560947D2B12C8D675AED2C190DF4F3F_gshared (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031** ___array0, int32_t ___newSize1, const RuntimeMethod* method) ;

// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponentInChildren<UnityEngine.Renderer>()
inline Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* Component_GetComponentInChildren_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m7CBAFA50AB995C9F53D6140718FCD31D7BEC7CC8 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_mE483A27E876DE8E4E6901D6814837F81D7C42F65_gshared)(__this, method);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656 (Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* __this, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color32_op_Implicit_m203A634DBB77053C9400C68065CA29529103D172_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___c0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_color_m5C32DEBB215FF9EE35E7B575297D8C2F29CC2A2D (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkBehaviour::OnStartServer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkBehaviour_OnStartServer_m3419FB42E919414317B3F207B92C54C416787074 (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_black()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline (const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Color::op_Equality(UnityEngine.Color,UnityEngine.Color)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Color_op_Equality_m3A255F888F9300ABB36ED2BC0640CFFDAAEFED2F_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___lhs0, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___rhs1, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Random::ColorHSV(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Random_ColorHSV_mC4AE87DC3711E0B9BC42F07625345F9443A3AF3B (float ___hueMin0, float ___hueMax1, float ___saturationMin2, float ___saturationMax3, float ___valueMin4, float ___valueMax5, const RuntimeMethod* method) ;
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B Color32_op_Implicit_m7EFA0B83AD1AE15567E9BC2FA2B8E66D3BFE1395_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___c0, const RuntimeMethod* method) ;
// System.Void Mirror.Examples.AdditiveLevels.RandomColor::set_Networkcolor(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomColor_set_Networkcolor_mF1B49792161BF2F51CF5745D283D69E1EEF2CCC3 (RandomColor_t85537EF520483D7F0DD339CC9F802E1A53CC282F* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkBehaviour__ctor_m63A1588EA5424B45068E2E6F74CDF520BF889765 (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, const RuntimeMethod* method) ;
// System.Void System.Action`2<UnityEngine.Color32,UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m7FD44B2A35F4476C6ECC24DA778FEB2EA6A0AAA3 (Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m7FD44B2A35F4476C6ECC24DA778FEB2EA6A0AAA3_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Mirror.NetworkBehaviour::GeneratedSyncVarSetter<UnityEngine.Color32>(T,T&,System.UInt64,System.Action`2<T,T>)
inline void NetworkBehaviour_GeneratedSyncVarSetter_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mD43D62C7BD24575258CA0CF86ABD19131A08BE2D_inline (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value0, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* ___field1, uint64_t ___dirtyBit2, Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* ___OnChanged3, const RuntimeMethod* method)
{
	((  void (*) (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C*, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B*, uint64_t, Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA*, const RuntimeMethod*))NetworkBehaviour_GeneratedSyncVarSetter_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mD43D62C7BD24575258CA0CF86ABD19131A08BE2D_gshared_inline)(__this, ___value0, ___field1, ___dirtyBit2, ___OnChanged3, method);
}
// System.Boolean Mirror.NetworkBehaviour::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NetworkBehaviour_SerializeSyncVars_mF60B2EAAE4B8BBE7044C528F6B3D34CBEB09EA65 (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, bool ___initialState1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteColor32(Mirror.NetworkWriter,UnityEngine.Color32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteColor32_m123810E64991275156516FBB8AD2CFA67A7C3B7B_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value1, const RuntimeMethod* method) ;
// System.UInt64 Mirror.NetworkBehaviour::get_syncVarDirtyBits()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint64_t NetworkBehaviour_get_syncVarDirtyBits_m96E01E08568BF6521B3B69F8F8F36884CAABEEB4_inline (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteULong(Mirror.NetworkWriter,System.UInt64)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteULong_mC0AE4801C58209EF02B73E3B353100B3AB95D28C_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint64_t ___value1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkBehaviour::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkBehaviour_DeserializeSyncVars_m4EECC3AAB3CAAD098D0B6056C590BBD98AEAF68C (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, bool ___initialState1, const RuntimeMethod* method) ;
// UnityEngine.Color32 Mirror.NetworkReaderExtensions::ReadColor32(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B NetworkReaderExtensions_ReadColor32_m0F0066C51CACC736B893D9F3C1D4324F87641BEF_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkBehaviour::GeneratedSyncVarDeserialize<UnityEngine.Color32>(T&,System.Action`2<T,T>,T)
inline void NetworkBehaviour_GeneratedSyncVarDeserialize_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mCD3E867438BB843F76C76561EC9F138D8CC6694D_inline (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* ___field0, Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* ___OnChanged1, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value2, const RuntimeMethod* method)
{
	((  void (*) (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C*, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B*, Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA*, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B, const RuntimeMethod*))NetworkBehaviour_GeneratedSyncVarDeserialize_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mCD3E867438BB843F76C76561EC9F138D8CC6694D_gshared_inline)(__this, ___field0, ___OnChanged1, ___value2, method);
}
// System.UInt64 Mirror.NetworkReaderExtensions::ReadULong(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint64_t NetworkReaderExtensions_ReadULong_mAC6B83521EBA7FFEDFC72A6AAE1BF5D87221A5F5_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.String Mirror.NetworkReaderExtensions::ReadString(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* NetworkReaderExtensions_ReadString_m5A56179D2C1CB509EC9C762F97BBD4133A5AD394_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// Mirror.SceneOperation Mirror.GeneratedNetworkCode::_Read_Mirror.SceneOperation(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t GeneratedNetworkCode__Read_Mirror_SceneOperation_m74E8C7FCF4974E3652B0B512F68390584B1B3DEF (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.Boolean Mirror.NetworkReaderExtensions::ReadBool(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.Byte Mirror.NetworkReaderExtensions::ReadByte(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint8_t NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteString(Mirror.NetworkWriter,System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, String_t* ___value1, const RuntimeMethod* method) ;
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneOperation(Mirror.NetworkWriter,Mirror.SceneOperation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_SceneOperation_mA2C6453809A524632C4C882EE0133AAE2B596560 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint8_t ___value1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteBool(Mirror.NetworkWriter,System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, bool ___value1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteByte(Mirror.NetworkWriter,System.Byte)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint8_t ___value1, const RuntimeMethod* method) ;
// System.UInt32 Mirror.NetworkReaderExtensions::ReadUInt(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.Int32 Mirror.NetworkReaderExtensions::ReadInt(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.ArraySegment`1<System.Byte> Mirror.NetworkReaderExtensions::ReadBytesAndSizeSegment(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteUInt(Mirror.NetworkWriter,System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint32_t ___value1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteInt(Mirror.NetworkWriter,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, int32_t ___value1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteBytesAndSizeSegment(Mirror.NetworkWriter,System.ArraySegment`1<System.Byte>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteBytesAndSizeSegment_mD7FCDC44AB0313C3ED0AFBFEC77366B6947672AC_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___buffer1, const RuntimeMethod* method) ;
// System.Guid Mirror.NetworkReaderExtensions::ReadGuid(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Guid_t NetworkReaderExtensions_ReadGuid_mCFFAB7379132286F7C9CC70EC291F8B28EA08B0E_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 Mirror.NetworkReaderExtensions::ReadVector3(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 NetworkReaderExtensions_ReadVector3_mD35BF8B14DD5F75688AB9C360D138D1BAB432637_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// UnityEngine.Quaternion Mirror.NetworkReaderExtensions::ReadQuaternion(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 NetworkReaderExtensions_ReadQuaternion_m135F5C523703C700E6A266DA9718E44D160BB567_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteGuid(Mirror.NetworkWriter,System.Guid)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteGuid_mFF6C6A1BC90A9A7BBC9C179FA6FC25753689D3F9_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, Guid_t ___value1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteVector3(Mirror.NetworkWriter,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteVector3_mB3C2B2F2D3C9874F883C12813FB20B9D5AABC882_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteQuaternion(Mirror.NetworkWriter,UnityEngine.Quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteQuaternion_mFC2E046965F6BA1B694218D5E60E26807974ACBA_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___value1, const RuntimeMethod* method) ;
// System.Double Mirror.NetworkReaderExtensions::ReadDouble(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double NetworkReaderExtensions_ReadDouble_m949A60A21C6EB3B9952A43355903F08B3A7E0EF9_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteDouble(Mirror.NetworkWriter,System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteDouble_m0475F5FB9E1D69D60501C7158AD3431680BC1BDB_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, double ___value1, const RuntimeMethod* method) ;
// Mirror.Examples.MultipleMatch.ServerMatchOperation Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.ServerMatchOperation(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ServerMatchOperation_m0E0217A1EA28419B9541B868BB7E160A7F9876C8 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.ServerMatchOperation(Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ServerMatchOperation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ServerMatchOperation_m4ED27F7C3469C55F345EFBE642DC3F4DB0FB9B5C (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint8_t ___value1, const RuntimeMethod* method) ;
// Mirror.Examples.MultipleMatch.ClientMatchOperation Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.ClientMatchOperation(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ClientMatchOperation_m967DCEAD7E36E480A8496E822CACE1F60AE9AFB9 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// Mirror.Examples.MultipleMatch.MatchInfo[] Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.MatchInfo[](Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchInfoU5BU5D_mE23CB91C4A5FF33675CF8A56B16BA006EC9047E9 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// Mirror.Examples.MultipleMatch.PlayerInfo[] Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.PlayerInfo[](Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_PlayerInfoU5BU5D_mDB000F31EDB9991937FE2C78FE0E3009EF2C1209 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// T[] Mirror.NetworkReaderExtensions::ReadArray<Mirror.Examples.MultipleMatch.MatchInfo>(Mirror.NetworkReader)
inline MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* NetworkReaderExtensions_ReadArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m3CE6D0D53E5BA355729F19428F259391B4A375B5_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method)
{
	return ((  MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))NetworkReaderExtensions_ReadArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m3CE6D0D53E5BA355729F19428F259391B4A375B5_gshared_inline)(___reader0, method);
}
// T[] Mirror.NetworkReaderExtensions::ReadArray<Mirror.Examples.MultipleMatch.PlayerInfo>(Mirror.NetworkReader)
inline PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* NetworkReaderExtensions_ReadArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_mC8AFAB257D0DC0249D387758D4F204833AADE730_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method)
{
	return ((  PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))NetworkReaderExtensions_ReadArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_mC8AFAB257D0DC0249D387758D4F204833AADE730_gshared_inline)(___reader0, method);
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.ClientMatchOperation(Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ClientMatchOperation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ClientMatchOperation_m08496CD05975A28AB59D4513B9BA36209FD081C0 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint8_t ___value1, const RuntimeMethod* method) ;
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.MatchInfo[](Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchInfo[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchInfoU5BU5D_m2A631F7A481D201FABBF213396D9D3C548AB54C0 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* ___value1, const RuntimeMethod* method) ;
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.PlayerInfo[](Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.PlayerInfo[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_PlayerInfoU5BU5D_m258F7D9CB39C3B6D8C6B2BA7E8B1FE9491B38B50 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* ___value1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteArray<Mirror.Examples.MultipleMatch.MatchInfo>(Mirror.NetworkWriter,T[])
inline void NetworkWriterExtensions_WriteArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m0946D038BE696722FB3ED0523DEC8233C19FFAC2_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* ___array1, const RuntimeMethod* method)
{
	((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572*, const RuntimeMethod*))NetworkWriterExtensions_WriteArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m0946D038BE696722FB3ED0523DEC8233C19FFAC2_gshared_inline)(___writer0, ___array1, method);
}
// System.Void Mirror.NetworkWriterExtensions::WriteArray<Mirror.Examples.MultipleMatch.PlayerInfo>(Mirror.NetworkWriter,T[])
inline void NetworkWriterExtensions_WriteArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_m348C67F42E07DE2F8A0295C5B388A6985FF81A0E_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* ___array1, const RuntimeMethod* method)
{
	((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415*, const RuntimeMethod*))NetworkWriterExtensions_WriteArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_m348C67F42E07DE2F8A0295C5B388A6985FF81A0E_gshared_inline)(___writer0, ___array1, method);
}
// Mirror.Examples.MultipleMatch.CellValue Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.CellValue(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint16_t GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_CellValue_m960AA8E4580568652E48F8810768B4DEA14B34F8 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.UInt16 Mirror.NetworkReaderExtensions::ReadUShort(Mirror.NetworkReader)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint16_t NetworkReaderExtensions_ReadUShort_mA98395DD1B1DA249096858B171B8BC23D95DF765_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) ;
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.CellValue(Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.CellValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_CellValue_mE850D02656D8C7B9181D58B92D4CA0583C063A1D (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint16_t ___value1, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriterExtensions::WriteUShort(Mirror.NetworkWriter,System.UInt16)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteUShort_m4AEC8147034117F9EB131043089577CD2DB42DB4_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint16_t ___value1, const RuntimeMethod* method) ;
// System.Void System.Action`2<Mirror.NetworkWriter,System.Byte>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m450CD99F88341CB21DB0FC2C3DD7C89F4D5C0AD0 (Action_2_tDDEAD4B308FD0F5A9299458AF5A6B47CA5D3732F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tDDEAD4B308FD0F5A9299458AF5A6B47CA5D3732F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m275566C978A87120E590B2AEEC6673681F0D0FFE_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Byte>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m23AD0D146D008C29D60BC1BC7B02142C50AB71B0 (Action_2_t104D6887DC3FA703E6D350257E4EE72A3EFCF3A3* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t104D6887DC3FA703E6D350257E4EE72A3EFCF3A3*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mC78C0FAA42EA1557B12A0C19CC155B84846FEBF2_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.SByte>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m0E3455CF54826F6B843F72903D6B054DD7963DA1 (Action_2_tEE3DE27FC7A1D873CE799E7124798B9A979DE913* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tEE3DE27FC7A1D873CE799E7124798B9A979DE913*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m1ADBEEBD003AEB0A008BF4203A9867B8F0BA1278_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.SByte>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m8B23EE7AB9CB6E996119CD5572F20B2326E154A1 (Action_2_tEE379855D79D043D165FD05C3786F7B0ADB89B38* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tEE379855D79D043D165FD05C3786F7B0ADB89B38*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mD6E7276D2B77469DFF19FAF60E563F9643F0639C_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Char>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mE178A62D8B5C362FCD4233DC326FA2E5C6888D1F (Action_2_t3C170C2BFCCAA591C6DA755A86E3FEDBB4428D90* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t3C170C2BFCCAA591C6DA755A86E3FEDBB4428D90*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m4DA3B4ABECA91C1BA3BBAE4B3C4C08A58D393A4C_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Char>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m1FA0EF4E6C4ED10AE76DB38281127F9DA725A3CF (Action_2_t1B2365362928779807EDAEE12F27010A7A8D39C6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t1B2365362928779807EDAEE12F27010A7A8D39C6*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mFA30B851A9481A0E19837178DC6713749E102BD0_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m537376D2B2BD93BFE47E52FF9BA6D6E53F3A191B (Action_2_t771EB50B64E2F517312250DEC5230DC62C7E36FD* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t771EB50B64E2F517312250DEC5230DC62C7E36FD*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m460C72FDDE5FF8033C7BD19A07CF4E3F473F7414_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Boolean>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mFB41A794E4F6AB08FCCA7F54C3917A44EFEBAFC7 (Action_2_tFAFB5B1636A32CF85A452A078736260BB496C253* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tFAFB5B1636A32CF85A452A078736260BB496C253*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m9FF8E5ED12C77689F6AB3235479B7036224952D4_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Int16>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mA7FDA03514437E579E951B398A1F19A4C40E1E68 (Action_2_tED2F1B32CB312EA2AFE3FF65E3EBD21ADF00A34C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tED2F1B32CB312EA2AFE3FF65E3EBD21ADF00A34C*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mABB69762BF0C1BAF5F0A3FDD5270A47610965D90_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Int16>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m0789F991A271A02D8263C3865AAA4FAAEAE1C9B0 (Action_2_tE28F1C5549DD431F6354E325F28C3FA842F94E57* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tE28F1C5549DD431F6354E325F28C3FA842F94E57*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mC89E476DD21F19A8C63C5560E25A2F8C36D5338D_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.UInt16>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mC6EA866F606C983D483B73525E3B1CFB128B3591 (Action_2_tDC9B80B3DDEFFE6AD670786AA0C0A41F09DFE02D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tDC9B80B3DDEFFE6AD670786AA0C0A41F09DFE02D*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m416BB4EA5264A588FF09D1E60465F186AFAAA7C6_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.UInt16>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m1735274EB0760EAE8A299DB6E281C7A877B54973 (Action_2_tDD6B83C4A6E2ECFC616A05B485C0D7E24A750E90* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tDD6B83C4A6E2ECFC616A05B485C0D7E24A750E90*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6B2F99B86ECAA36AFD23FD69EA8C5E1553ECEC6E_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Int32>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mC03200ADD4C8D842C9DBD941F1461C6F78466418 (Action_2_t4D3AA2594C46CEF975DCD32BFF9A0FEE380568B3* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t4D3AA2594C46CEF975DCD32BFF9A0FEE380568B3*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m07C6392BB276FFCCFD4E495842992EA26FA44882_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Int32>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mDDEA3388C6E9EA1735B2AAE13C38B7B08D143D7B (Action_2_tD938883EC204367D5A295A7CB2BB5D4D20A90029* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tD938883EC204367D5A295A7CB2BB5D4D20A90029*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mC4BC8A0AA1C664D10759568DE71018E9D5C339E1_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.UInt32>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m626DDD1C02889779E51819503D673C1724B71941 (Action_2_t0FDD9213EEB281B92425DB7083B3E28D691AA59F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t0FDD9213EEB281B92425DB7083B3E28D691AA59F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mD30FDAFA53F9F14951015A305F3F2782418E646F_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.UInt32>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m91E102E6C4CFEE5995B6B0CDCFA19811D4555667 (Action_2_tC9DC1064C14B7AF886718CB747591EC132E1E301* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tC9DC1064C14B7AF886718CB747591EC132E1E301*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m30A0F17BA067B84E683B953EB6E436A247766346_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Int64>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m1F3C63E311D92D34187BF9C6A02BED00C2A321F2 (Action_2_tE79E56090404F1ED684677C80F34095664D0010D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tE79E56090404F1ED684677C80F34095664D0010D*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mB5B0280FB694D8743982749E1ED3472CD323F172_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Int64>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mA3CF307C9CE476D251662DDDA235CC085B1130AC (Action_2_tDF176CFB33B35A021E5DA20E9485F6A452F2C345* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tDF176CFB33B35A021E5DA20E9485F6A452F2C345*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m3DCA8976EB904F61BC50F4D9A59E5805084B4E55_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.UInt64>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mF5892B15904085FCE623E0895697F33916A3DEB4 (Action_2_t7962FDDC98F8F0028DDCD9B0CADBA4C9C051E1C5* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t7962FDDC98F8F0028DDCD9B0CADBA4C9C051E1C5*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m8939564F9B7F5BD0A2B996E75501A72B094FD03F_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.UInt64>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m8B9741EFE4CF735550B34447D80680135080B161 (Action_2_t9E34BCADC1F9EF87E3BC9817BF9ABF3086286388* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t9E34BCADC1F9EF87E3BC9817BF9ABF3086286388*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m49093CC0F8BC0E50E97397A4D141B1F5F8140C6D_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Single>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m0209EF27E442D70A0261C1B2591ACBDDB9F146CC (Action_2_tE45BEC1FBF44C71FEAD78DC6323066FD51D7FE55* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tE45BEC1FBF44C71FEAD78DC6323066FD51D7FE55*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m600728814D64949FB42DEE273E5E146A5E015BE1_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Single>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m7E7EDC9B9F57AFF56485B0903C188A3E4092A7F7 (Action_2_tD01A9F5ED6BB951B86BE216B2B8DE2C17FFAE551* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tD01A9F5ED6BB951B86BE216B2B8DE2C17FFAE551*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mB59E849E9D35F11C7AAF53EF2AD3D152FF05A23B_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Double>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mC7F3CEF4AD030B10080D5A785DA693E1B7B456A5 (Action_2_tA70CFBAAF2BDFBA2BEE4003F4C2E6E5B12924D8E* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tA70CFBAAF2BDFBA2BEE4003F4C2E6E5B12924D8E*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m00A3B34AD5FB6A28E99E8F2FFA8E5B8CB61DEF1D_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Double>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m10E6D1F2202B9181AA7954A3F49D5DC32D977528 (Action_2_tD872DFCCB454D89FE5878BA1B3A03AA4FF2F7A72* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tD872DFCCB454D89FE5878BA1B3A03AA4FF2F7A72*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mEF3263AD558AED9B7592D24A5A23EB4B92B7551B_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Decimal>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m2FE11FE651F5AE8150006365EBA478BD731C9B0E (Action_2_tC4556EDF997707D306A86E36FC8EE3751F9C687C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tC4556EDF997707D306A86E36FC8EE3751F9C687C*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m177E2A82A82BA0668971BDC4FC2C3DB599EEA81B_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Decimal>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m7528A1A23D14871E7C355BAC759391CCF83EA771 (Action_2_t3913958E2393CE5F44A661B2F0835A6FC4513CBE* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t3913958E2393CE5F44A661B2F0835A6FC4513CBE*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mE6F19ED15FE74006B01225FE99514FF56D36FB31_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.String>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m96D1FE2D89915F6DCFE7C522C131EC85218DF44A (Action_2_t63FAAE13C5F96B474021BB5448158556C8EBB594* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t63FAAE13C5F96B474021BB5448158556C8EBB594*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6A0E7FE9DF9AE6C4BEE58611CB55F64FC3D79052_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.ArraySegment`1<System.Byte>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m145A15B63C274B6AC19BC4C615E51AAADD02AB2B (Action_2_tE7DE89222EAD5CBE5FBA94711CB3C71222202DFC* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tE7DE89222EAD5CBE5FBA94711CB3C71222202DFC*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m78A92E408AB0E0C2654D69E27C395066B6C2968D_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Byte[]>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m8E435DEA2338A3D48843DAAA8D21C85A4D1D39B0 (Action_2_t6CB11000550B779DE6F0A9873A4FA47684E8A996* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t6CB11000550B779DE6F0A9873A4FA47684E8A996*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6A0E7FE9DF9AE6C4BEE58611CB55F64FC3D79052_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m51724E50D62540A2CF078C8B78E9209761EF2D06 (Action_2_tCAF5159105AB227514D4319BDE35C44C14FB6C67* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tCAF5159105AB227514D4319BDE35C44C14FB6C67*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m0579D8063D09880CEDC4C9D1178E80DF3B0590A1_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector2>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m5D26AEB049FB2E3AFFE418B1A76F0CE17ECE0D09 (Action_2_t06CF1E09D2E968AE317C1B4066FE59281D0D7F35* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t06CF1E09D2E968AE317C1B4066FE59281D0D7F35*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mD49A36F5FFAF197E90B5952E9633AE309843E177_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m2DBDE75E3894CE657FF776915BA6C959E49A01D0 (Action_2_tD63675C2600BD13840BDC184B61A1282E2FA7667* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tD63675C2600BD13840BDC184B61A1282E2FA7667*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mE637AA364DF34EED2F99253A70BFCF870A94CAF1_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector3>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mE65B7ABA1299F1C5EB05CD59D377D5C576209304 (Action_2_t3D709A9DD3DD60B2669C03CAF9A9607B98F0CC44* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t3D709A9DD3DD60B2669C03CAF9A9607B98F0CC44*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m682410A20D07B6655C32D6C559873A009A4FCF1A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m4E3B75D5192791920CF89272FB90E620B075BD16 (Action_2_t0708A1448BB2410AD3F0DF78AF427D6730760C7F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t0708A1448BB2410AD3F0DF78AF427D6730760C7F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mE8E693B9421FE0BCF376B60270B6CE035B9C0445_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector4>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mFE7DECE762FED2E02629F28B9966DE881014D291 (Action_2_t437C51FCE7D4E11D08E66AC6884ADD843A29305E* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t437C51FCE7D4E11D08E66AC6884ADD843A29305E*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mB7E93B9BC80454A09A170D043A427932004F2B3C_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector2Int>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m4D7A6253073C76DC89FE5C83A73CA70F505973B6 (Action_2_tE7875C2C92DB47F882FCBBE26BB848F3264D0B63* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tE7875C2C92DB47F882FCBBE26BB848F3264D0B63*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m5A3C95C6CCA5A6FD7D037F30DF49E19272EF37D9_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector2Int>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m60D5109A818B5495DE26C493F3BCBE7239FF2236 (Action_2_tF8B877CE67C7AF23C81D6C5C15EB3D2FE44321A0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tF8B877CE67C7AF23C81D6C5C15EB3D2FE44321A0*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m0F92180E2494ECB80B8A42DC605736360E6DF5B2_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Vector3Int>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m1F945F52AF5C891A1C89B8C3E7C50B54664E0EEF (Action_2_tA9611CADC87D4AD3B0FB66AF118E5B269E6FA658* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tA9611CADC87D4AD3B0FB66AF118E5B269E6FA658*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mC564AEE7A8E8FAE71A3742A10AEBEA03FF0629B5_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector3Int>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m3B8F1FBBC6BBD141F9E7AB0ED6696F97D28E0874 (Action_2_tCF4F398AAC858F0B69CF49EA4DB6878F5E85A2FE* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tCF4F398AAC858F0B69CF49EA4DB6878F5E85A2FE*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m56BC792FCBD78BC65CC2EC09BA07651D3871899A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mDA326A2B24806A313B762DAB862967D52B3C4263 (Action_2_t993E6168F76ADC356CC943B6992CDD55115F3057* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t993E6168F76ADC356CC943B6992CDD55115F3057*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mD0DE34B532C4AA36FD3458BF59883E7050DC2209_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Color>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m95A3ECC89DD934CE7F7704EE287FAD33456470B0 (Action_2_t1AB30C8E5E10864F469D7062D99016352CC3602F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t1AB30C8E5E10864F469D7062D99016352CC3602F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mC191176DB6B0FCB0B872565F7544590AACBB9D0E_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m02D1798F3FF6629BE5763B034D7DBCC3A170BBCE (Action_2_t27076E67A79536591E04C63FD1582DB7D564BA08* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t27076E67A79536591E04C63FD1582DB7D564BA08*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mBBC00E0314287F9C6628AB9F74EF98D3A22AF75B_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Color32>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m761EA55F9D98E81F418148D5EBBDA59F8E2A8014 (Action_2_t284D0A27595992EAC03994E6C1A1E82225441B67* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t284D0A27595992EAC03994E6C1A1E82225441B67*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m3BB9E2D7B6FDB8E6AB5B314FEA0C3745639E9BC1_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m49389BFEA959AD5452A6075D222159D8B2CEF0CD (Action_2_tD28B743D21DCE5F5BE026041A86D0F58543D86C4* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tD28B743D21DCE5F5BE026041A86D0F58543D86C4*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mDB3CB076F607F5410E97DA2180E2152FAD3DEF6E_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Quaternion>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m0B572B0211EAE72327BDCDCACE96224357E558AF (Action_2_tFAE875C9728E772ACF44D3BCF5F2B94860FB1CDE* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tFAE875C9728E772ACF44D3BCF5F2B94860FB1CDE*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mFADA1C21D43DA1112959F405E82247EE225DBD58_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mC5F661551C6AA2C25B713BF1DF016D4636BC3239 (Action_2_t776DBFE440EDCE827698E6B849C13676E750F733* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t776DBFE440EDCE827698E6B849C13676E750F733*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mD7493C3D3AA2FBA6DF6765E0C5EBAA710C98DD3C_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Rect>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mFBD125BE0060AF3338538D4175299B3F7EF6AA24 (Action_2_tD39051A11B2175464C227775F3F6FD44715C0767* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tD39051A11B2175464C227775F3F6FD44715C0767*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m0AAD1089E22F4ACDD6DD2CA994FCD91DFB547158_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Plane>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m3E58F4F05B8EF5DFABBC848F3A02B5706F268172 (Action_2_tF808E86BBEE3D1817CE8A0C2E299320C9DD9D1BA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tF808E86BBEE3D1817CE8A0C2E299320C9DD9D1BA*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mAB8DD07A760641CA0BEB366A71F9F82E12B40FB0_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Plane>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m062962270F6BC7BB56727545E83C74101673DC36 (Action_2_t9BBCAC312FAACBDD9B08E82065F0C66CB92596DD* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t9BBCAC312FAACBDD9B08E82065F0C66CB92596DD*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m06BEB5BDCFF73A407C621A5137C7D0D60A61802B_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Ray>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m02CD61DADA5CFB5AF7B6BE7A4308480F4C2D1B35 (Action_2_t0726BDF29C00FDD4DA0F278B3AFC4E47F69A4CED* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t0726BDF29C00FDD4DA0F278B3AFC4E47F69A4CED*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m00F6527E708A7FF1670B7F31EE846A13BDD05D6E_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Ray>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m87D4F54DD955944BF8C781C605896FF9C98B5738 (Action_2_tBE61BBF1961F6628AEE63F058E11FEB22FB6A6ED* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tBE61BBF1961F6628AEE63F058E11FEB22FB6A6ED*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mA6EADFE77C5E972BE22C1BDEE6495E014A2DFF9C_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Matrix4x4>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m7FFD1C135C73A31984A52AF176F0593816B0844F (Action_2_tD12F3221DBF89BF11C6CB2855E9BDBD70D6A8AB9* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tD12F3221DBF89BF11C6CB2855E9BDBD70D6A8AB9*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m01F2C5B90622E1ED10BA953081D61D070F9DB80E_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Matrix4x4>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m3B7D9DE1EBC4E44C33D0FA00791767AC00309665 (Action_2_tB31DAF5F45A869010A4A3D1EA9EA5C56DCFAB947* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tB31DAF5F45A869010A4A3D1EA9EA5C56DCFAB947*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6817D47D12FE915AEBAD895890F54952084011C9_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Guid>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m968C81385D6BB7EEFD019E41D96199BB6E3A367D (Action_2_tCB8F678502AD5BFA52EE6F149351DAC3037677AF* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tCB8F678502AD5BFA52EE6F149351DAC3037677AF*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m0D60CCCE5E1F6876A78A508FED68F61B2ECB30D5_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Nullable`1<System.Guid>>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mC9D631DEF0104B3FF747C0CDF9186A29A55FE010 (Action_2_t00EE65E013F88779EEEE98835E86BDF400A3BF36* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t00EE65E013F88779EEEE98835E86BDF400A3BF36*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m25956627FFC58E8B05165191D0CDA23D1BA56E29_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.NetworkIdentity>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m76B26FEADA52D5D5EDCCCA0D1CE4445461158472 (Action_2_t51E55DFCEC17AF21DB946B2527D3669CD8E09542* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t51E55DFCEC17AF21DB946B2527D3669CD8E09542*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6A0E7FE9DF9AE6C4BEE58611CB55F64FC3D79052_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.NetworkBehaviour>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m018A30909323728A244BB88F8FCE714ED2B35B92 (Action_2_tC5312D54454D5411CB706A055837C90CBD0FEC12* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tC5312D54454D5411CB706A055837C90CBD0FEC12*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6A0E7FE9DF9AE6C4BEE58611CB55F64FC3D79052_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m3B708985FBC8A1C1BFDF36AD6EF0594211A84245 (Action_2_tD87BC92995377D813FAEA7DFF07C8F114451FAAA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tD87BC92995377D813FAEA7DFF07C8F114451FAAA*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6A0E7FE9DF9AE6C4BEE58611CB55F64FC3D79052_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.GameObject>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mD03C913E03D4340B7C63BEC0771E1AC6B6A2A0BE (Action_2_t7A7E74A9B38640A94D5CC807EC76F706FB6693C0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t7A7E74A9B38640A94D5CC807EC76F706FB6693C0*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6A0E7FE9DF9AE6C4BEE58611CB55F64FC3D79052_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,System.Uri>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mBFE16439C5F13D2FA3150BF43F7B28F2A91B755B (Action_2_t92B2C6139E3081F85D498D28EED40F2C004C7B20* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t92B2C6139E3081F85D498D28EED40F2C004C7B20*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6A0E7FE9DF9AE6C4BEE58611CB55F64FC3D79052_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Texture2D>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mBF279502827A8972D70378B6D4888F0506FD21DE (Action_2_tA50DCC93E783D93FEFF521E5255E78C184ED8E02* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tA50DCC93E783D93FEFF521E5255E78C184ED8E02*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6A0E7FE9DF9AE6C4BEE58611CB55F64FC3D79052_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,UnityEngine.Sprite>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m19E6335D2500A4A6E09FD4371235F0B6100BF35C (Action_2_tA527CAC169A61C3874E9DEC5127904806AF2463C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tA527CAC169A61C3874E9DEC5127904806AF2463C*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6A0E7FE9DF9AE6C4BEE58611CB55F64FC3D79052_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.ReadyMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mC23A039B775A0DFE21EBC500F50E285F39530E53 (Action_2_t4CDBF06B555A45F0D103E46E4114CEEEFA34B691* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t4CDBF06B555A45F0D103E46E4114CEEEFA34B691*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m9C548C503F9756E0E827125D82A5364ED1BC8A73_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.NotReadyMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mF286C1D11E18BCB8031E6551BDCB4184178A9713 (Action_2_tE1C508C7DB184AC769DA763897EA5CE208D96BE9* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tE1C508C7DB184AC769DA763897EA5CE208D96BE9*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m9B86B8FD5F2E498DED165C15CB29A97C0FA9D464_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.AddPlayerMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mA2007694D53E8ED7315A5A24D888F3BA8C89AF64 (Action_2_t98800A090710915B3E861E237B1A9B6E9F29163F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t98800A090710915B3E861E237B1A9B6E9F29163F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m07A76F94AB6D967EB721874EF48BE8D8E3BF8434_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.SceneMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m87022428E316DF22BCAB52DC647624C6B8ED38E1 (Action_2_t4A43C94439BC4A524A4463B987E9326B340BB5D7* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t4A43C94439BC4A524A4463B987E9326B340BB5D7*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m0C5368CFA99831F9D7EC0ED13B1A3518F4635F41_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.SceneOperation>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mA38AE26C37DA086615388A426BB2E181EC97EC23 (Action_2_t102F2BE697BDA0818FA414975B57C948D9C2BBDF* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t102F2BE697BDA0818FA414975B57C948D9C2BBDF*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m7CD05CED2A073EFA5EAF5610F4FB3CD26D80764A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.CommandMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m233601E9FE7EA83CD29B2774BB3384D00AD2CA9A (Action_2_t8863334817D651A8E3B8E434C682CA5A76CE9498* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t8863334817D651A8E3B8E434C682CA5A76CE9498*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m48030241C18FC9C0BB3511E4AE52C0EB2169B778_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.RpcMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mEB7542DAF4E595B6B775A313F4653E33F8728247 (Action_2_tAF32C09EC861FEA50FE0977ED9C0DAD82BA3D637* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tAF32C09EC861FEA50FE0977ED9C0DAD82BA3D637*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m964236F341659B98C47F8BE7C5BE04BA65B44B4B_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.SpawnMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m5F1F50CC15F7E0541F768F03B29697D7B1D1C821 (Action_2_t8CF38B31F52C4EF713F3D2BAC6B806B321ADC0A1* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t8CF38B31F52C4EF713F3D2BAC6B806B321ADC0A1*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mCF284042D6F1941A23E600BAADABE17FDA6997A4_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.ChangeOwnerMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mF0E4B938B893D67DAD1A1A97D8BB9128DB588838 (Action_2_tA6272A2BBF53770CE719C794145D53E55F0DCC51* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tA6272A2BBF53770CE719C794145D53E55F0DCC51*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m2708719FC5C3991F88B255F63984BD42F6702196_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.ObjectSpawnStartedMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m1F9ADDDB8C090B5DBA2E23D2B86EABBFC1F1780B (Action_2_t932E36A723FFA4E592D80F6F8AE785A34669DA75* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t932E36A723FFA4E592D80F6F8AE785A34669DA75*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m829FE0C79B606D936AA3AFAA429A18D49193602A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.ObjectSpawnFinishedMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mC06148B8AB62C775FB10920B5579CA5A71EA16E0 (Action_2_t88B940D5126F22A0E314062C9CB9B8CF14430308* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t88B940D5126F22A0E314062C9CB9B8CF14430308*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mA37AA6E4AEF79ED86433B22C41A8BC1F6BF74B23_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.ObjectDestroyMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mC7C0FB7B89D872EF7E8D1A8DA8D0C39B1E664180 (Action_2_t4125FEC2DD5C797C8E273617CF2C3E2ABB98FA45* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t4125FEC2DD5C797C8E273617CF2C3E2ABB98FA45*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mFF568B5AAE8E90AF27020509FCD58D0A5E0ABFA1_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.ObjectHideMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m17850095E89A0ED92C274F9DD97E792B5249EA42 (Action_2_tB5D2418D8D5BB1CA27E37232842C681B92ABDB9B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tB5D2418D8D5BB1CA27E37232842C681B92ABDB9B*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m3D9A5AD9337E240C8E048907CF65B5E91A4A75E9_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.EntityStateMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m045FACF9055F4BFC210539F7561F4F190BAAFACC (Action_2_t92D149BA0BB9E6C54B7036167F4A14DFF1B5BE08* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t92D149BA0BB9E6C54B7036167F4A14DFF1B5BE08*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mBD3B0FE04055EEBE35C50814C47D34F11E482B59_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.NetworkPingMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m8E94D4420B59CF9ADA5F0454DC31ECA7750E462E (Action_2_t427A3AAB52F4338A54E05C0F9CCE238604B75463* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t427A3AAB52F4338A54E05C0F9CCE238604B75463*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m5F38E72A4FA2703145CA00EE6C7FF9B6CE832FD7_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.NetworkPongMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mADF37BEC66DF6AB901EC870BE27B3318E0FC01EC (Action_2_t19A08A9BB7D482718CD3474975A6559CA159FA9F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t19A08A9BB7D482718CD3474975A6559CA159FA9F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mDD6145E1B7E7AC69B01AF066136A477EA4C23A3E_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ServerMatchMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m262F49A58D1EDD285AE54334A93FF77B4D892FFE (Action_2_tAF93D04C699FAC16A658F8DA78DD252A93E952E8* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tAF93D04C699FAC16A658F8DA78DD252A93E952E8*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mDE0AB6E21D582143046BB6B026DFD3EEF5691978_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ServerMatchOperation>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mCE1240AAB1D925B326D8A3F7F8FD25B6AFBA9D29 (Action_2_tEF11BC50B4B015730C0409DC35EAC7D6E03CEE11* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tEF11BC50B4B015730C0409DC35EAC7D6E03CEE11*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m7CD05CED2A073EFA5EAF5610F4FB3CD26D80764A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ClientMatchMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m76DF3E8268347D234290E4A12EC5A91BACEF6F18 (Action_2_tCFCEE592405B35148F5E1AC506FEB4F8CB2F49B6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tCFCEE592405B35148F5E1AC506FEB4F8CB2F49B6*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m1F92BDB0C5659D8FB6E9FF31EC929F27CF47CC42_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ClientMatchOperation>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mDD710CDD6E3F504530D5CFFD1FFC8A25E2264BBE (Action_2_tE631CB825D55269F80774B2F55C5E62192891CF8* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tE631CB825D55269F80774B2F55C5E62192891CF8*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m7CD05CED2A073EFA5EAF5610F4FB3CD26D80764A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchInfo[]>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m6827C4A015D2EB0AD992BF402EE39B204304F29D (Action_2_t77285E9313B73F391F0816DD5738EBF05F8774AD* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t77285E9313B73F391F0816DD5738EBF05F8774AD*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6A0E7FE9DF9AE6C4BEE58611CB55F64FC3D79052_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchInfo>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m9402B782F534C705854D68C8257E6F68F153EAC7 (Action_2_t1D8E4389A2FC4D611519FC7C996BF96F763E5017* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t1D8E4389A2FC4D611519FC7C996BF96F763E5017*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m409669ED144DA290F9240B006B94E552E38ADA68_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.PlayerInfo[]>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m6BCDF8708D0F6E4FD910C30E564B2ADB417AE0B0 (Action_2_t5199EDEE1233BFABE45CDA21E9230C8346645716* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t5199EDEE1233BFABE45CDA21E9230C8346645716*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m6A0E7FE9DF9AE6C4BEE58611CB55F64FC3D79052_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.PlayerInfo>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mC80DE69A229698030304FD2BA3320438FA30FEAA (Action_2_t15EAEA1713B2DE57B0F7BFED86F3CF5915138314* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t15EAEA1713B2DE57B0F7BFED86F3CF5915138314*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m3B5B056A9402FF85E8BF3F30F8F842F9DD99AFA8_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_mB98661590A449BCEE8A7E3E99D8266BD99EBE5F4 (Action_2_tC9D1D64ED53FED0525E8EE017F8CAEBD3D9CF610* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tC9D1D64ED53FED0525E8EE017F8CAEBD3D9CF610*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m1CC50902B0A8B46B92F8A64E3E6CDA863670EAA6_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m2430D42BE1E29623D9B59695BC96B80DF7BA3CD1 (Action_2_t6B81023D4D427FA4343B3A8E4F4A88A145C6884F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t6B81023D4D427FA4343B3A8E4F4A88A145C6884F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m534AA06214BC73B888D0B106431D5F8657EFBADC_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchPlayerData>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m024345F091E97FDED57D66405C674043E1EBD6A6 (Action_2_tB478A835A6DC624AEA7C01C383390CE10F4D2BF1* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tB478A835A6DC624AEA7C01C383390CE10F4D2BF1*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_m724CC6A662995A8BDE491E680A0B19F566DD3B2E_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`2<Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.CellValue>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m56261BE7DB6718F58FD8A022A04A8AD1FDF095C4 (Action_2_t117A07FAD93D252BC5E69F05373B4E286F1F8D56* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t117A07FAD93D252BC5E69F05373B4E286F1F8D56*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_2__ctor_mA2463D0820BDAF6210F929F698261F87A1EFB5A3_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Byte>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m5070845CE83FAC65D5C1A17411A99898A16002D6 (Func_2_t0BC506BED5AC85C37292910C8BDAD52A9F6C9986* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t0BC506BED5AC85C37292910C8BDAD52A9F6C9986*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m826425E2D4DB5E667248C6A79A1972CA0A60EB77_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Byte>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m623460CD3E0ED5E1D2A78F2D45931C5D4B106446 (Func_2_tC5D3F83B86462F66072C190B541019F5456F2DB9* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tC5D3F83B86462F66072C190B541019F5456F2DB9*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mD6FFA3BD35C1822C8D3F0650F141A5DFE148E50A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.SByte>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m5591363477B7D64C33CBFEA8D4EB049A3B5C84D1 (Func_2_t3EE5A9E53DE82B19DAC86C3D30AC7BD98D305238* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t3EE5A9E53DE82B19DAC86C3D30AC7BD98D305238*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m9994BCD4B83B79BC1DD67D7F694974215DCE8EB0_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.SByte>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m063EDE6FCD17CDBD99B22C07C6DD4FC2B8A92DF1 (Func_2_tFB31016B91F9F5C6B2CAC98095F7D025353DD033* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tFB31016B91F9F5C6B2CAC98095F7D025353DD033*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mB41C506907EB6ED4588FD3FB76C3067096E87552_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Char>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m4BA6B0FD3F6DB1E9057511A3857A5B01026A7E1A (Func_2_t26F6F63A0DA84EBAD15F6BCC374071D9F6C8B42C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t26F6F63A0DA84EBAD15F6BCC374071D9F6C8B42C*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mE0224E3E48B05DE6D6D3FCAB652B609EAE8DD543_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Char>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m30FB8CA63098A957572E1D071C547AF43811FBC5 (Func_2_tBB870D080A3294867CDE63CF2AF4F2D4699C33B2* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tBB870D080A3294867CDE63CF2AF4F2D4699C33B2*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mE4C517298BFB1ACEAE4E479F45230037927D5369_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mC260EE2E3AADF08FECA8211082B9843A2049F86A (Func_2_tE35FCB04208B127B021F241EA079464ACC551B7C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tE35FCB04208B127B021F241EA079464ACC551B7C*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m13C0A7F33154D861E2A041B52E88461832DA1697_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Boolean>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mAA5ECC078690E371A556E1D41024A45A75322618 (Func_2_t2CF9D64DD882AC30B207D3D6C3C4EFC846A40BC9* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t2CF9D64DD882AC30B207D3D6C3C4EFC846A40BC9*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m720FC4F90AF22CC262CED04C6117FCC8058561A2_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Int16>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mB1D9D8504E593D505ADEAF64E72EB0B4C33D0A4A (Func_2_tEE61CDD83F7808585CB68357D75DC2637FBA492F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tEE61CDD83F7808585CB68357D75DC2637FBA492F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mC7A0EC33AC48580094225C83A288E8EF80034BFC_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Int16>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mBE9EDCAC9C310837BC45D0CF3BFF615DC4A9D163 (Func_2_tA3EC1D81F5C9B991E5E551D9C0197BBCFA6ECFC8* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tA3EC1D81F5C9B991E5E551D9C0197BBCFA6ECFC8*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m5D68B8D9C998E5AE4CC3250A8DDDE79C9BD5EBCF_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.UInt16>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mBA9972836E55A9428AE3C331A510B4ABB56BEBCA (Func_2_tD28C61E3A0A47F71DB0CDCB3B43C7F13D4F82235* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tD28C61E3A0A47F71DB0CDCB3B43C7F13D4F82235*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m0FFAC302C951F47E7D69018BE8FDF52D098E712D_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.UInt16>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m7BDA824E493FDB492F25EB39A3F521CF201ACC12 (Func_2_t4842934F57735E601CC8B995D6E040EAF256FBF4* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t4842934F57735E601CC8B995D6E040EAF256FBF4*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m5FC5AD57F45CF1B53B7B60C4729EA389C1567C5D_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Int32>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m6D793C4BB9EFC6D203C711F6DFCC319E4293DF80 (Func_2_t3F9C9CE2DD4A455F6BF2ACA2CBC6A00343B798EA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t3F9C9CE2DD4A455F6BF2ACA2CBC6A00343B798EA*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mEB7603EDE6D79A62E5BD74A896F030D2C9F2A821_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Int32>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mBDE37F18AF96D73F7BF1AE78624BBC669E47EE73 (Func_2_tA8008B61C32E0E3A90F98593FFE956D148147DC5* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tA8008B61C32E0E3A90F98593FFE956D148147DC5*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mB90F61A92C810F7BCB3288E608C7B6766FB5A120_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.UInt32>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m0F6B5856378B49521EC96E3CDC25AFB2072D2338 (Func_2_t1FE4698618B38731C4E2D67BCC314778BA13ECC0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t1FE4698618B38731C4E2D67BCC314778BA13ECC0*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m2F35D9DF8C659B83CA70029ACBA44930CD42E90D_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.UInt32>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mC30D04ED011FDBB56C538B5F51AD9850C112485B (Func_2_tE53CE7EE59BDF79AA9E78FAB9A62695508883396* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tE53CE7EE59BDF79AA9E78FAB9A62695508883396*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mCE51F65339B9D55BA56037F23360F38A3B6113D3_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Int64>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m11E9C1A471F4D9E73120EF4CAAAEA62C795BED5B (Func_2_t0D88BB68D7BF644024EA7DFB9E7CDBA1EE7EF69C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t0D88BB68D7BF644024EA7DFB9E7CDBA1EE7EF69C*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mD54790BA1BEF5C7D8025676612EB0FF0A97A87F6_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Int64>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m7D133FF807CD80F365E68C437CA8D0AB82D0AEB9 (Func_2_t8F78F44B218E20FDA86CA793AEC9892B18B978F6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t8F78F44B218E20FDA86CA793AEC9892B18B978F6*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mA0019FEC08A5C2E72206C2A4FECA4C228923CEDC_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.UInt64>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m3059ADDD45E7205D5C2639AE508CA8657796F711 (Func_2_t86251E51D7B41B00F1CC89068612DB2EE6F5FDE0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t86251E51D7B41B00F1CC89068612DB2EE6F5FDE0*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m3579BBCEF5B00795682D166C40CBA47618B2D563_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.UInt64>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mB28A3EB5FCA927A61BE69EA4BB8342654C70D6AB (Func_2_t4B1235C83470DD7C3286754A4CFC840FBFD35741* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t4B1235C83470DD7C3286754A4CFC840FBFD35741*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m0D50C137716B8F564918D800779AB562683454C0_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Single>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m5826E0D00074A31DA58583D4A1F1FFA110E9EED6 (Func_2_tDA855C737BB7E9546B48245EC0F04DAA56A1CB03* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tDA855C737BB7E9546B48245EC0F04DAA56A1CB03*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m04EB988C3F849C1BE164FC6656C2281DF47EEAA7_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Single>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mA5F35AB5F92DCFC66D9D99AC7287972743050602 (Func_2_t2A9651B5311D10F2FDA41A113A50E18A22D93138* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t2A9651B5311D10F2FDA41A113A50E18A22D93138*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mDFA24B5D1C39CBE639807C5095115EECCAA39D40_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Double>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m95A1D4FB759F59D21AE946C9FA4D83F32EDB1310 (Func_2_t589ED36A1F9FA112DA0EA54F4A46CBAEE886A07A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t589ED36A1F9FA112DA0EA54F4A46CBAEE886A07A*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mC2F9460DBFF9A8659492AC19F4B9FCA63BFA48A8_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Double>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m104798A3ACA5A83E5355D889A5E09A0E1BE047BC (Func_2_t3C87EC87405703EAD0A26AD3BD3A5B3615EB39E6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t3C87EC87405703EAD0A26AD3BD3A5B3615EB39E6*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mD5C38515FA9E2063BB33617714D4F4A0F5CD4DD1_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Decimal>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m4E78040095EF78EF445101B5D92C9741EC6F76F5 (Func_2_tB643983D6CBD6220B483FEE44F3207125D7D3726* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tB643983D6CBD6220B483FEE44F3207125D7D3726*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m2883355E0E294A4A3D740AFD1E2D13121EAE4A1A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Decimal>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m564CECA9ABF0AAB5D6346FBF31136AF3E515B7D7 (Func_2_t2ED5FBB4104367F73E820E5864485F4EDD912E0F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t2ED5FBB4104367F73E820E5864485F4EDD912E0F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m8B3991F7314BBA8B50BF1F8436068A737FD8C13A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.String>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mA7515F595687974D1E2D8CF2075226849F6ED4DE (Func_2_tF047810C662C3A551DDB01290047E803F32DA440* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tF047810C662C3A551DDB01290047E803F32DA440*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Byte[]>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m0D22158C554642C2D76B64BA604FFF9BE0826A76 (Func_2_t68753017D74F6BABDA2E69D18EC2FA64AC20B03F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t68753017D74F6BABDA2E69D18EC2FA64AC20B03F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.ArraySegment`1<System.Byte>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m82AC9C94C7FDC3C7453D4D1B4AF7C0A8F211231D (Func_2_t0968F31AEE953CA42422B45BEE8A04FB272E086F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t0968F31AEE953CA42422B45BEE8A04FB272E086F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mA8804B68FFF2F0831D450EDE4380821D8528B16A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mAE410B36A1308D178CFC6F7F427D01B5E52C98E9 (Func_2_t9C4AED5DAF5E665602E3D752BDF50BAE7C2C5D0F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t9C4AED5DAF5E665602E3D752BDF50BAE7C2C5D0F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mB8352E429272D346070147A942612E0C937BD03B_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector2>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m6EA96A3743D0E29565D31A08F3F6641E7224A5CB (Func_2_t603A0F2238D55FD8325BC0816D55C2970606BB81* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t603A0F2238D55FD8325BC0816D55C2970606BB81*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m96218009E13DF8E3215C5B5155ECC9729B41F974_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m2A154129F35F2D94A66B118368E22F485E2E1754 (Func_2_t9BD17C9037D00DFA5EFF1643A78C3E4300A31331* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t9BD17C9037D00DFA5EFF1643A78C3E4300A31331*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m55582D0AF64DBB8F297B71E8B68F40E374CE1910_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector3>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m8AFF539B9E7BEE8788570878F77A6B1EB9E56BFA (Func_2_t58F06EBEDB50273CFFCE1BED6156FF00537E7BF6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t58F06EBEDB50273CFFCE1BED6156FF00537E7BF6*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mB25549B0BDE31FBD830182682B8B0F258EAEC5D1_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m0CE18E4741D0AEEF77AAD040BB74509D63E4F135 (Func_2_tEA21835A50BEE04419A104B2F4D8CCE015122924* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tEA21835A50BEE04419A104B2F4D8CCE015122924*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mBE75211545C5D3C820BEA11B595AB466F6D45FE4_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector4>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mC4084E3AF33665FF7FE3011ABEC4B0600702DEB4 (Func_2_t3EF4F45A92179F162ABDFA4C40C59CEAB4DF8C95* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t3EF4F45A92179F162ABDFA4C40C59CEAB4DF8C95*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m32598ED0A528EE5B56631DCA23606800BF1E3420_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Vector2Int>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m6222FE4CA8B3758CAC98A1ADD440A9F8E7ACF8EC (Func_2_t1D54F05297BE0A4ACFBE3D87381C38332A2F7837* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t1D54F05297BE0A4ACFBE3D87381C38332A2F7837*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m366F848FE673CED0F9AAC46B542B8A8A2C377A89_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector2Int>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m94D28186D109C0CB497C56C95938494A13E99BD5 (Func_2_tE2F89B2F7AD6F1B1ED5373840C57D1603C193D7D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tE2F89B2F7AD6F1B1ED5373840C57D1603C193D7D*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m8750059BE69D44747DE0E741B5C550033140C8B1_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Vector3Int>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m217139F1BECEBC6BBBE9E9EF9EB72AFFCB2F048A (Func_2_tFD61C39BCF897AD7E0777243349B4558DB639EC5* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tFD61C39BCF897AD7E0777243349B4558DB639EC5*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m93FE856D524A39884536A4D8FFFF9FA8BC7DB916_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Vector3Int>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m731CC3B334459200D7A73025EFD53DD1F367904D (Func_2_tB67E11F122FE23A54C3DC5758CF7ABCED89CA2B9* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tB67E11F122FE23A54C3DC5758CF7ABCED89CA2B9*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m82052C7AC435234E2794624952FCF4B56746186E_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mAD4F798590659BE2E777E0ACE42340F428825FAD (Func_2_t6DF06C835E4A0B6416DE67676C67EDBE1EBBFD78* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t6DF06C835E4A0B6416DE67676C67EDBE1EBBFD78*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m851A616484C4EE142B945D4B08A222778A25F038_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Color>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mFDB7A6FE1249299DD1A26AD6EF239EA1914B6FFA (Func_2_tB85C8267AB8EBEC2E4C056F08CC1288F0C2055B7* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tB85C8267AB8EBEC2E4C056F08CC1288F0C2055B7*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m065EB34D4CD158D458CFAA74F156078986C6B896_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m39287045E5A0907A42D35A9EE4A4DF09881759EF (Func_2_tA1D462828AADE4C7A521611177F1A49476CFE773* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tA1D462828AADE4C7A521611177F1A49476CFE773*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m28742C1B87FB886C579BBD88B3EA716E47C56F65_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Color32>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mD0E32851A51CE9193117D36268D301E4A08AAEF9 (Func_2_t5B94AD2B8545A777AE2428975D92A19545B20228* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t5B94AD2B8545A777AE2428975D92A19545B20228*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m9F05B0A4DBFD5C15DD3C15D847A3710A24CD5C94_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m20AAA57716BA46CE5CE092302B9871E665C8B2DD (Func_2_tDF2B4816BD9F902CA19B4AFA6CA78C2A61867EA3* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tDF2B4816BD9F902CA19B4AFA6CA78C2A61867EA3*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mD8552D6390B3EC85469DBC405A43A3BE7C1F939A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Quaternion>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m381A9AE2DFC9AC217CE781A47C3BDEDE755C44C9 (Func_2_t643EFCAEA913218C33F9E05144DAB3702712A18C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t643EFCAEA913218C33F9E05144DAB3702712A18C*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m4B0A30A5BF099E6F10A3906F02D6920684BAE99D_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m4854C63996A46CD4AD4F9245B66996B869921F1F (Func_2_t340EA86AEF734F68ACF5E9FF2A124F9ADDED52DE* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t340EA86AEF734F68ACF5E9FF2A124F9ADDED52DE*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m864DC3F910DBD1416040134913DD4D5E40C52F35_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Rect>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mED004AEBC522BA1D0E91B10B3B29E75B0A323A81 (Func_2_tCD175F70DBC690C8EF0231BF4AF1180C50BE25E0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tCD175F70DBC690C8EF0231BF4AF1180C50BE25E0*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mC2282DC5B9FA03C78072AFB7949F1C61701E0A57_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Plane>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mF1D4E3B7C5E6B42EA0B6D65FB8C0019AAB1D0DE7 (Func_2_tF1646D6248B9372FDA9C49F858144441B68F98FD* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tF1646D6248B9372FDA9C49F858144441B68F98FD*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m5775276C45B867CA3DB663B2D8C444204A8A6345_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Plane>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m532BA2795A3C3E358A83BEF75171D6D43EF6AB61 (Func_2_t93378D91D2D47434F10E8104D47A5A2363B48D08* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t93378D91D2D47434F10E8104D47A5A2363B48D08*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7C8A028547A62CFAD1F55AA9A62D16CD24A2A239_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Ray>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m80F47435B96AA1788E79B6988B6216C2318CCA90 (Func_2_tB124603C647DAC5696CA392A20B3EE6C213CCA26* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tB124603C647DAC5696CA392A20B3EE6C213CCA26*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mCF3F8A5B698C0456FE17CBDE86BF91173067D539_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Ray>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m98E8F4CF0D573CA4AD26355795FDFDAF15C5470F (Func_2_t4467783C00F409FFB8ED1FDCE87EF974AB372C4D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t4467783C00F409FFB8ED1FDCE87EF974AB372C4D*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mC189715B5F495CDD877F81F199A0810F51DCD9B2_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Matrix4x4>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m814C6DAB91B9E1D8C0B44552EAC9D7A765571E7E (Func_2_tAAC77BA3AEC5902F635E82ED90239D6BCD31F70A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tAAC77BA3AEC5902F635E82ED90239D6BCD31F70A*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m09AECDE4994C9122308EC501F2BCFBDCEBF98CC3_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<UnityEngine.Matrix4x4>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m5DD5911338E6A07FE8B87A49AD6DB7DEE40727CE (Func_2_t4F5890FDEA23094C33678E54A33233DBBC85F64B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t4F5890FDEA23094C33678E54A33233DBBC85F64B*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mF02608D0427390919A28909F9DDEAB9AE3360C22_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Guid>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m242227B1E3EA30328974365FD5F1A854F3000F9F (Func_2_tEC73485627298AE849634B626697EFC64D213D43* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tEC73485627298AE849634B626697EFC64D213D43*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mF555B385B4BDA7CAA449193CC6171BE7C0962947_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Nullable`1<System.Guid>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m6B8AECE3E335F2AAEB7318EADA50E566859DD177 (Func_2_tDCE62FBEE709378CCBDB8A5F059E3D9EB17469CB* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tDCE62FBEE709378CCBDB8A5F059E3D9EB17469CB*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m2198BB76DA1151D5468437793BDFC4B8677D1F23_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.NetworkIdentity>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m04DBF2925BC7EC91E116CE5BAAF1DEAF9177FD34 (Func_2_t8D8F451E2C46B99D55F5781D36B4F981BE62BD6A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t8D8F451E2C46B99D55F5781D36B4F981BE62BD6A*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.NetworkBehaviour>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m2F8D8CF0B14913832CE43E52FDD07F8A6E42AC5C (Func_2_t763657E22AECC9ED46856683B3045624CF6351E6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t763657E22AECC9ED46856683B3045624CF6351E6*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m560EA5A4E1183E0F54F8811384EB83438520FC48 (Func_2_t842043449CFBC0F4C127867ED6917B5566AC5576* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t842043449CFBC0F4C127867ED6917B5566AC5576*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mCF76F13F01BE18A846A15ED3D38AFCFA0EC78FC2_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mF133B0268455362C2FDDF6BE242D881C138975E6 (Func_2_t8085A3B2562300C528C41159E557B58E555D6798* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t8085A3B2562300C528C41159E557B58E555D6798*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.GameObject>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m87C3A95AEDA3A817E2F7CD434A823DD7F93479D3 (Func_2_t1E82FEEB6D88844359FA37AD374ACC1AA39DDC4A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t1E82FEEB6D88844359FA37AD374ACC1AA39DDC4A*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,System.Uri>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m2030BB6181363ECFAFF85456FCD23A59C2784D3A (Func_2_t2DBAA86C74F086DBA285C7B8ECE41C9F35E419D4* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t2DBAA86C74F086DBA285C7B8ECE41C9F35E419D4*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Texture2D>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mB8990ED204A2B979A83499D899C19A1B631F451F (Func_2_t58133EDD30520660CD4F542594E8D913BB704B55* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t58133EDD30520660CD4F542594E8D913BB704B55*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,UnityEngine.Sprite>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m78CF1B401D3C9F0556D621AD8D8DF55E4770CACB (Func_2_tB35F19D43C3D74BFE4F16B609D5DAFBCCE477EC1* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tB35F19D43C3D74BFE4F16B609D5DAFBCCE477EC1*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.ReadyMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m9E0BBEDE74B5D75035A52EA54E0C47542C604EBE (Func_2_t31743D3E7DDCFE0001801AD8A7459B2FE016E401* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t31743D3E7DDCFE0001801AD8A7459B2FE016E401*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mCD2D383C2FB019239FE1DBEC7EF8FB424B5DBC0F_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.NotReadyMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mDFEC253B59A6F180A481F8F9CB18D974D46C3620 (Func_2_t9B160232C1403CEF10F27D1D1BE4368650EE22CD* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t9B160232C1403CEF10F27D1D1BE4368650EE22CD*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mB5DD68FD377D0B4AE63535B9DF23D7ECC424BF45_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.AddPlayerMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m32B293DC530099CEB8C5AD22D251B39D2F37B303 (Func_2_tC36B775425D425FC2AF1D16C0CD70386004FFEA0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tC36B775425D425FC2AF1D16C0CD70386004FFEA0*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mFAA6E18BE4D8A686CD184B5B399BE74743056209_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.SceneMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m7CCABC0E5B392E1AFBF33C570F01C80C4A422F97 (Func_2_t8C43EB67A953E2DA4EA0F861438FEF6C6F87360E* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t8C43EB67A953E2DA4EA0F861438FEF6C6F87360E*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m8631958585DE743935CBB3C962D7557967A03225_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.SceneOperation>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m4FE270A4DB67D3D0622934DD44DE88672FD1C700 (Func_2_tA736A0330DE6964C91228A379FAB411414BBBB31* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tA736A0330DE6964C91228A379FAB411414BBBB31*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7BE3802A697DD49E577E0B122FD3C96AF4AEB6D5_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.CommandMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mFC452C2FE1CDD30101156FFD8D93FC224FB98087 (Func_2_tDB9FD64DF7D2CA31FFF8279F26075DFCF440D33C* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tDB9FD64DF7D2CA31FFF8279F26075DFCF440D33C*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mC66467E2F6FDE0762616CF893F4CCDA0CC7C0766_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.RpcMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mC9BA73108B1E7CD5DC2190361856DDE258C25FBC (Func_2_t5F7A34ACC37197D2AFFFFF3A63EE51E2B44D6607* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t5F7A34ACC37197D2AFFFFF3A63EE51E2B44D6607*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m9ED02C7210DB2224AA2286AA7CF4B26E3B728C03_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.SpawnMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mA5D72440406261E5883E58D5A6D1B3A96BF68ABF (Func_2_t1FE35EF0C4BBDC7AFA697EBBB5B4D64547326DCC* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t1FE35EF0C4BBDC7AFA697EBBB5B4D64547326DCC*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m032D89FC15227676D2D18D443F179CE229A3C232_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.ChangeOwnerMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mE75CD4D3A38684C2195915A8435DF2D7579C3B2C (Func_2_tAF26F2C22D87F8F669162B6542990070635D2A30* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tAF26F2C22D87F8F669162B6542990070635D2A30*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mC0FC7199340AB97EDAD69AE90C60DA8A9CE4CEB1_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.ObjectSpawnStartedMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mDF071593EB218CD46A09DBB869CFE900C7DE702F (Func_2_tBD4C61172F81BE2A292FAE166832A0324BB00B1B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tBD4C61172F81BE2A292FAE166832A0324BB00B1B*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m8301D01266BFB09C863C904500B7EB9D86A296F2_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.ObjectSpawnFinishedMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mC1FF94D326E4383A2ED9B12FE9BA7C6B5D689DF7 (Func_2_tAE6F8E9F1706B23DD7BC49D738569171CF6E8C3B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tAE6F8E9F1706B23DD7BC49D738569171CF6E8C3B*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m974B92BFD44CB8869450E84BC229D289EA342ED7_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.ObjectDestroyMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m5C824E4C4CABDCECD70454ABB6854F61676803A6 (Func_2_t056E7181DAB40DF2A9D14BB4450C0DCC566784E6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t056E7181DAB40DF2A9D14BB4450C0DCC566784E6*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m14645801EB1832479F0856137D68C7408E823C68_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.ObjectHideMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m2483C821901785CE42A2FFEF511EB1DC50060094 (Func_2_t76221365B0738498867CB728129555B7A8617C15* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t76221365B0738498867CB728129555B7A8617C15*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mF0634E1C586D4F090250925858969DD40AC91E39_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.EntityStateMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m4B3544D9A6F24E37D069A76B2D0BE847126EE92D (Func_2_tB637CE13E86654599FE7556001525095E552A933* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tB637CE13E86654599FE7556001525095E552A933*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mF31C8EFEAC54CF635E51D7E3FA4A9783B424F0FB_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.NetworkPingMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m774F67BF80330546FD3906729F0DD18536E9D2FA (Func_2_t3C7B620CCBA5E47EED93AC22DB224F015928D9BF* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t3C7B620CCBA5E47EED93AC22DB224F015928D9BF*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m14EA83B893C7636539B4D80A6E1A07B9528B355F_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.NetworkPongMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m05B2B6CD0EBE9546AA45D196A8BB5EDAB7F104B3 (Func_2_t57653632E080A4623E41FABED4B1B6707C4D1B44* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t57653632E080A4623E41FABED4B1B6707C4D1B44*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m217F5FB2F2FE3C57D9EB739FDC48257A21D5C1CD_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.ServerMatchMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m09D4615AB8251027E1C3D5AF83D85E140981D689 (Func_2_t2EFEEFDAE61B1D665B02E8D247FA7A8AE1B3D3FF* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t2EFEEFDAE61B1D665B02E8D247FA7A8AE1B3D3FF*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m9D9B0F568D33F486468BBDC03A08A6674923D9EB_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.ServerMatchOperation>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m50EDBC8444E6CBF1964E72B1CAC2E6DAAF7C86CF (Func_2_t4A60C2EC0E8ECFA6E6872E11BFAD744A805D5FF8* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t4A60C2EC0E8ECFA6E6872E11BFAD744A805D5FF8*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7BE3802A697DD49E577E0B122FD3C96AF4AEB6D5_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.ClientMatchMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m36CF69079E9F30D185E759A619586D8BAE734275 (Func_2_t21336B204733D902134B7F0FB71D1D4D85655F8E* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t21336B204733D902134B7F0FB71D1D4D85655F8E*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m3D431B743B14AFC5C39381BDFC9C3398FE1E824A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.ClientMatchOperation>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m7C5A0F5C2643DB8BB6EE6C47BFBE77373EA2C09F (Func_2_tFBF2D2CE91617E529AC8196465432685D724AEBE* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tFBF2D2CE91617E529AC8196465432685D724AEBE*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7BE3802A697DD49E577E0B122FD3C96AF4AEB6D5_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.MatchInfo[]>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mAA8BAF9145D19220900229CD9A074B243A603C85 (Func_2_tEAEC0CEBCFCC9157E40E1A7FFFEB82D8A082B205* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tEAEC0CEBCFCC9157E40E1A7FFFEB82D8A082B205*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.MatchInfo>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mC8512C24ADB03A516688EFABCA57C176C722CE70 (Func_2_t0D870FCF01A0B3EEF980A0B74CCA07765A7552B3* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t0D870FCF01A0B3EEF980A0B74CCA07765A7552B3*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m8E1D68853309ADD19D6ACC13BCA4C8A8B65E3839_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.PlayerInfo[]>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m2119F2CA2CF856D3ED9F162F4D061CC242ECB561 (Func_2_t57B5F9E47ED4C8F5611A672DF5C949F2D02972C8* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t57B5F9E47ED4C8F5611A672DF5C949F2D02972C8*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.PlayerInfo>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m36BCE7C48675E2254A9ACEC67519A94EA81776F6 (Func_2_t966D11E60D81CE62911BC8F172BA2326E5F49FE0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t966D11E60D81CE62911BC8F172BA2326E5F49FE0*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mB40E9E079583CE4F4DB26BF7778C6042B07B60D9_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mFFB05A516BD11E3B95ABEFDF3D4FAC2BA629F29D (Func_2_t4CD599531AE1FA8CA3CF4057D910B500DBF99E45* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t4CD599531AE1FA8CA3CF4057D910B500DBF99E45*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mD156B6DCCBE963380AAEC4144B32001BB9EF09EA_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mEB3D94E335C7B0856B5C5D4F4742C1CFF9E7CB3B (Func_2_t5DF2F494B8936184EE2E2F51E135F530C09B76D0* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t5DF2F494B8936184EE2E2F51E135F530C09B76D0*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m9B92330D0970FA0ED7A79279EF247B7F2735F7F4_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.MatchPlayerData>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m87ACAC5A4D6CCCD69D70361725EE81715F68B42A (Func_2_t747EDA19750AE43DE30476D128A90ACDBEAC48AF* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t747EDA19750AE43DE30476D128A90ACDBEAC48AF*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mF886556BBCCC11410D64B900FFC8934FAE5C0094_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Func`2<Mirror.NetworkReader,Mirror.Examples.MultipleMatch.CellValue>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m76B116CE887E4AF6CDACA82B7696B7A0B33D7417 (Func_2_t22170DBE4CAFFA15631BB8B0CE681A822A3A160A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t22170DBE4CAFFA15631BB8B0CE681A822A3A160A*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m310FEE70AC898B237CF249B0F77DE33197C37314_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) ;
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 Color_op_Implicit_m6D1353534AD23E43DFD104850D55C469CFCEF340_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___c0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector4_op_Equality_m80E2AA0626A70EF9DCC4F4C215F674A22D6DE937_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___lhs0, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___rhs1, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline (float ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBlittable<UnityEngine.Color32>(T)
inline void NetworkWriter_WriteBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mDB472FF359C94951E696CB3FD4F6016CFE6F82B7_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value0, const RuntimeMethod* method)
{
	((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B, const RuntimeMethod*))NetworkWriter_WriteBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mDB472FF359C94951E696CB3FD4F6016CFE6F82B7_gshared_inline)(__this, ___value0, method);
}
// System.Void Mirror.NetworkWriter::WriteBlittable<System.UInt64>(T)
inline void NetworkWriter_WriteBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m869219CA464A4A9CCE03043BF274E8EBD19428AC_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, uint64_t ___value0, const RuntimeMethod* method)
{
	((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, uint64_t, const RuntimeMethod*))NetworkWriter_WriteBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m869219CA464A4A9CCE03043BF274E8EBD19428AC_gshared_inline)(__this, ___value0, method);
}
// T Mirror.NetworkReader::ReadBlittable<UnityEngine.Color32>()
inline Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B NetworkReader_ReadBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_m6DBD713FC553C165E92A9ABA7691638AA9C2E7BD_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method)
{
	return ((  Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))NetworkReader_ReadBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_m6DBD713FC553C165E92A9ABA7691638AA9C2E7BD_gshared_inline)(__this, method);
}
// T Mirror.NetworkReader::ReadBlittable<System.UInt64>()
inline uint64_t NetworkReader_ReadBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m5B0E578FA24816AF3240FCA5BFF9353FBB4B958A_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method)
{
	return ((  uint64_t (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))NetworkReader_ReadBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m5B0E578FA24816AF3240FCA5BFF9353FBB4B958A_gshared_inline)(__this, method);
}
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806 (String_t* ___format0, RuntimeObject* ___arg01, RuntimeObject* ___arg12, const RuntimeMethod* method) ;
// System.Void System.IO.EndOfStreamException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4 (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* __this, String_t* ___message0, const RuntimeMethod* method) ;
// System.ArraySegment`1<System.Byte> Mirror.NetworkReader::ReadBytesSegment(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 NetworkReader_ReadBytesSegment_mA17220D13799B7AA2EFF9B49C6F1F98B486A330E_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, int32_t ___count0, const RuntimeMethod* method) ;
// T[] System.ArraySegment`1<System.Byte>::get_Array()
inline ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, const RuntimeMethod* method)
{
	return ((  ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* (*) (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*, const RuntimeMethod*))ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_gshared_inline)(__this, method);
}
// System.Int32 System.ArraySegment`1<System.Byte>::get_Offset()
inline int32_t ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*, const RuntimeMethod*))ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_gshared_inline)(__this, method);
}
// System.Int32 System.ArraySegment`1<System.Byte>::get_Count()
inline int32_t ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*, const RuntimeMethod*))ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_gshared_inline)(__this, method);
}
// T Mirror.NetworkReader::ReadBlittable<System.Byte>()
inline uint8_t NetworkReader_ReadBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mD424A6F72F431A64491144A4321E8915B2A9F179_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method)
{
	return ((  uint8_t (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))NetworkReader_ReadBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mD424A6F72F431A64491144A4321E8915B2A9F179_gshared_inline)(__this, method);
}
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexOutOfRangeException__ctor_mFD06819F05B815BE2D6E826D4E04F4C449D0A425 (IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82* __this, String_t* ___message0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBytes(System.Byte[],System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBytes_m0F3058BA3B1B973C3D99B647DA231D9E82AFEDEC_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBlittable<System.Byte>(T)
inline void NetworkWriter_WriteBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mCDD7CA43B0B14857B8777A663BE02A0AA5917764_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, uint8_t ___value0, const RuntimeMethod* method)
{
	((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, uint8_t, const RuntimeMethod*))NetworkWriter_WriteBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mCDD7CA43B0B14857B8777A663BE02A0AA5917764_gshared_inline)(__this, ___value0, method);
}
// T Mirror.NetworkReader::ReadBlittable<System.UInt32>()
inline uint32_t NetworkReader_ReadBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m20406E9280C5EE4AA46F5E308A00B1CE728A70A3_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method)
{
	return ((  uint32_t (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))NetworkReader_ReadBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m20406E9280C5EE4AA46F5E308A00B1CE728A70A3_gshared_inline)(__this, method);
}
// T Mirror.NetworkReader::ReadBlittable<System.Int32>()
inline int32_t NetworkReader_ReadBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m4436F2F119D575AC7A0EA84CCD5C3CF655A3FFFC_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))NetworkReader_ReadBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m4436F2F119D575AC7A0EA84CCD5C3CF655A3FFFC_gshared_inline)(__this, method);
}
// System.Void Mirror.NetworkWriter::WriteBlittable<System.UInt32>(T)
inline void NetworkWriter_WriteBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m3F8565D904BC20262924C7AF8BFBB3F7FE770535_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, uint32_t ___value0, const RuntimeMethod* method)
{
	((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, uint32_t, const RuntimeMethod*))NetworkWriter_WriteBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m3F8565D904BC20262924C7AF8BFBB3F7FE770535_gshared_inline)(__this, ___value0, method);
}
// System.Void Mirror.NetworkWriter::WriteBlittable<System.Int32>(T)
inline void NetworkWriter_WriteBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m5745AC0FB4E1ABCB68691585D1B48F92DA99AEFB_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, int32_t ___value0, const RuntimeMethod* method)
{
	((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, int32_t, const RuntimeMethod*))NetworkWriter_WriteBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m5745AC0FB4E1ABCB68691585D1B48F92DA99AEFB_gshared_inline)(__this, ___value0, method);
}
// System.Void Mirror.NetworkWriterExtensions::WriteBytesAndSize(Mirror.NetworkWriter,System.Byte[],System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteBytesAndSize_m3A29964C9A4F7D85B49358324A238EADA7DE57BA_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___buffer1, int32_t ___offset2, int32_t ___count3, const RuntimeMethod* method) ;
// System.Byte[] Mirror.NetworkReaderExtensions::ReadBytes(Mirror.NetworkReader,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* NetworkReaderExtensions_ReadBytes_mF2B3E392F976B37C12A9BB81DBEB98726813730D_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, int32_t ___count1, const RuntimeMethod* method) ;
// System.Void System.Guid::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Guid__ctor_m9BEFD9FC285BE9ACEC2EB97FC76C0E35E14D725C (Guid_t* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___b0, const RuntimeMethod* method) ;
// T Mirror.NetworkReader::ReadBlittable<UnityEngine.Vector3>()
inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 NetworkReader_ReadBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m17B9EA1CB3FF9F5A7936974AAD62B19383E2D69F_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method)
{
	return ((  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))NetworkReader_ReadBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m17B9EA1CB3FF9F5A7936974AAD62B19383E2D69F_gshared_inline)(__this, method);
}
// T Mirror.NetworkReader::ReadBlittable<UnityEngine.Quaternion>()
inline Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 NetworkReader_ReadBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mF27EFEBBC75EBD54AF10CE92FF64D5C780A97A69_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method)
{
	return ((  Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))NetworkReader_ReadBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mF27EFEBBC75EBD54AF10CE92FF64D5C780A97A69_gshared_inline)(__this, method);
}
// System.Byte[] System.Guid::ToByteArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* Guid_ToByteArray_m6EBFB2F42D3760D9143050A3A8ED03F085F3AFE9 (Guid_t* __this, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkWriter::WriteBlittable<UnityEngine.Vector3>(T)
inline void NetworkWriter_WriteBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m6552B19126A3F040F7E78F41CEB63CA85B0EF8BC_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method)
{
	((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2, const RuntimeMethod*))NetworkWriter_WriteBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m6552B19126A3F040F7E78F41CEB63CA85B0EF8BC_gshared_inline)(__this, ___value0, method);
}
// System.Void Mirror.NetworkWriter::WriteBlittable<UnityEngine.Quaternion>(T)
inline void NetworkWriter_WriteBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mD95146DDEF099B2575801811770B7527E72A4969_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___value0, const RuntimeMethod* method)
{
	((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974, const RuntimeMethod*))NetworkWriter_WriteBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mD95146DDEF099B2575801811770B7527E72A4969_gshared_inline)(__this, ___value0, method);
}
// T Mirror.NetworkReader::ReadBlittable<System.Double>()
inline double NetworkReader_ReadBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m9F3B82FB2EF977402985132FD7A7B3F531AC32C8_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method)
{
	return ((  double (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))NetworkReader_ReadBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m9F3B82FB2EF977402985132FD7A7B3F531AC32C8_gshared_inline)(__this, method);
}
// System.Void Mirror.NetworkWriter::WriteBlittable<System.Double>(T)
inline void NetworkWriter_WriteBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m520D47927C9E53348DF0990757101E4408B4E4E8_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, double ___value0, const RuntimeMethod* method)
{
	((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, double, const RuntimeMethod*))NetworkWriter_WriteBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m520D47927C9E53348DF0990757101E4408B4E4E8_gshared_inline)(__this, ___value0, method);
}
// T Mirror.NetworkReader::ReadBlittable<System.UInt16>()
inline uint16_t NetworkReader_ReadBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m5CB828366095AF0971BD2196AC15F944092E1E91_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method)
{
	return ((  uint16_t (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))NetworkReader_ReadBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m5CB828366095AF0971BD2196AC15F944092E1E91_gshared_inline)(__this, method);
}
// System.Void Mirror.NetworkWriter::WriteBlittable<System.UInt16>(T)
inline void NetworkWriter_WriteBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m7A05C5F5DD5D33FC126D2DAC895A096EC3402A14_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, uint16_t ___value0, const RuntimeMethod* method)
{
	((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, uint16_t, const RuntimeMethod*))NetworkWriter_WriteBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m7A05C5F5DD5D33FC126D2DAC895A096EC3402A14_gshared_inline)(__this, ___value0, method);
}
// System.Boolean Mirror.NetworkServer::get_localClientActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NetworkServer_get_localClientActive_mF819059FAC02CCA8DCAEDEE3D7D9A0F4EB077846 (const RuntimeMethod* method) ;
// System.Boolean Mirror.NetworkBehaviour::GetSyncVarHookGuard(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NetworkBehaviour_GetSyncVarHookGuard_mF9DBFE6D19AC4402C44FDD0BEF11C1C99B694ED8 (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, uint64_t ___dirtyBit0, const RuntimeMethod* method) ;
// System.Void Mirror.NetworkBehaviour::SetSyncVarHookGuard(System.UInt64,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkBehaviour_SetSyncVarHookGuard_m8098794BA748ACFFE6B68C69B9D5609E43F71334 (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, uint64_t ___dirtyBit0, bool ___value1, const RuntimeMethod* method) ;
// System.Int32 Mirror.NetworkReader::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t NetworkReader_get_Length_m23094EDDFE84816D0846ED68F1EFEA79220B93EE_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30 (String_t* ___format0, RuntimeObject* ___arg01, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) ;
// System.Void System.ArraySegment`1<System.Byte>::.ctor(T[],System.Int32,System.Int32)
inline void ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20 (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___array0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	((  void (*) (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t, const RuntimeMethod*))ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20_gshared)(__this, ___array0, ___offset1, ___count2, method);
}
// System.Void Mirror.NetworkWriter::EnsureCapacity(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Void System.Array::ConstrainedCopy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_ConstrainedCopy_m14D61795896B63A77E396C63457AD6700410531C (RuntimeArray* ___sourceArray0, int32_t ___sourceIndex1, RuntimeArray* ___destinationArray2, int32_t ___destinationIndex3, int32_t ___length4, const RuntimeMethod* method) ;
// System.Byte[] Mirror.NetworkReader::ReadBytes(System.Byte[],System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* NetworkReader_ReadBytes_mA1E53425AAD4AD3038C9759F6971533248347130_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___bytes0, int32_t ___count1, const RuntimeMethod* method) ;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E (RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ___handle0, const RuntimeMethod* method) ;
// System.Int32 System.Math::Max(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Max_m830F00B616D7A2130E46E974DFB27E9DA7FE30E5 (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method) ;
// System.Void System.Array::Resize<System.Byte>(T[]&,System.Int32)
inline void Array_Resize_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_m82FEC5823560947D2B12C8D675AED2C190DF4F3F (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031** ___array0, int32_t ___newSize1, const RuntimeMethod* method)
{
	((  void (*) (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031**, int32_t, const RuntimeMethod*))Array_Resize_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_m82FEC5823560947D2B12C8D675AED2C190DF4F3F_gshared)(___array0, ___newSize1, method);
}
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Copy_m2CC3EA1129E9B8EA82E6FA31EDE0D4F87BF67EC7 (RuntimeArray* ___sourceArray0, int32_t ___sourceIndex1, RuntimeArray* ___destinationArray2, int32_t ___destinationIndex3, int32_t ___length4, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mirror.Examples.AdditiveLevels.RandomColor::SetColor(UnityEngine.Color32,UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomColor_SetColor_mBDDBD9EE5A4F48CE9121665E51B8DE6AC5E643DD (RandomColor_t85537EF520483D7F0DD339CC9F802E1A53CC282F* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ____0, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___newColor1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInChildren_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m7CBAFA50AB995C9F53D6140718FCD31D7BEC7CC8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (cachedMaterial == null) cachedMaterial = GetComponentInChildren<Renderer>().material;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = __this->___cachedMaterial_14;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		// if (cachedMaterial == null) cachedMaterial = GetComponentInChildren<Renderer>().material;
		Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_2;
		L_2 = Component_GetComponentInChildren_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m7CBAFA50AB995C9F53D6140718FCD31D7BEC7CC8(__this, Component_GetComponentInChildren_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m7CBAFA50AB995C9F53D6140718FCD31D7BEC7CC8_RuntimeMethod_var);
		NullCheck(L_2);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3;
		L_3 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_2, NULL);
		__this->___cachedMaterial_14 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___cachedMaterial_14), (void*)L_3);
	}

IL_001f:
	{
		// cachedMaterial.color = newColor;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_4 = __this->___cachedMaterial_14;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_5 = ___newColor1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_6;
		L_6 = Color32_op_Implicit_m203A634DBB77053C9400C68065CA29529103D172_inline(L_5, NULL);
		NullCheck(L_4);
		Material_set_color_m5C32DEBB215FF9EE35E7B575297D8C2F29CC2A2D(L_4, L_6, NULL);
		// }
		return;
	}
}
// System.Void Mirror.Examples.AdditiveLevels.RandomColor::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomColor_OnDestroy_m278010FB39DD333EDF4FF28DA545ECA0AA0B1CCE (RandomColor_t85537EF520483D7F0DD339CC9F802E1A53CC282F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Destroy(cachedMaterial);
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = __this->___cachedMaterial_14;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_0, NULL);
		// }
		return;
	}
}
// System.Void Mirror.Examples.AdditiveLevels.RandomColor::OnStartServer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomColor_OnStartServer_m72789C65F9979440403CE89A19B20B5A639968E7 (RandomColor_t85537EF520483D7F0DD339CC9F802E1A53CC282F* __this, const RuntimeMethod* method) 
{
	{
		// base.OnStartServer();
		NetworkBehaviour_OnStartServer_m3419FB42E919414317B3F207B92C54C416787074(__this, NULL);
		// if (color == Color.black)
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_0 = __this->___color_13;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1;
		L_1 = Color32_op_Implicit_m203A634DBB77053C9400C68065CA29529103D172_inline(L_0, NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2;
		L_2 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
		bool L_3;
		L_3 = Color_op_Equality_m3A255F888F9300ABB36ED2BC0640CFFDAAEFED2F_inline(L_1, L_2, NULL);
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		// color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_4;
		L_4 = Random_ColorHSV_mC4AE87DC3711E0B9BC42F07625345F9443A3AF3B((0.0f), (1.0f), (1.0f), (1.0f), (0.5f), (1.0f), NULL);
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_5;
		L_5 = Color32_op_Implicit_m7EFA0B83AD1AE15567E9BC2FA2B8E66D3BFE1395_inline(L_4, NULL);
		RandomColor_set_Networkcolor_mF1B49792161BF2F51CF5745D283D69E1EEF2CCC3(__this, L_5, NULL);
	}

IL_004b:
	{
		// }
		return;
	}
}
// System.Void Mirror.Examples.AdditiveLevels.RandomColor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomColor__ctor_mFF707E424E1CEC94939F12E421684B0DD959D2D0 (RandomColor_t85537EF520483D7F0DD339CC9F802E1A53CC282F* __this, const RuntimeMethod* method) 
{
	{
		// public Color32 color = Color.black;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		L_0 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_1;
		L_1 = Color32_op_Implicit_m7EFA0B83AD1AE15567E9BC2FA2B8E66D3BFE1395_inline(L_0, NULL);
		__this->___color_13 = L_1;
		NetworkBehaviour__ctor_m63A1588EA5424B45068E2E6F74CDF520BF889765(__this, NULL);
		return;
	}
}
// System.Void Mirror.Examples.AdditiveLevels.RandomColor::MirrorProcessed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomColor_MirrorProcessed_m934BE01399217DC5F8A3F524C16C07797B01E214 (RandomColor_t85537EF520483D7F0DD339CC9F802E1A53CC282F* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// UnityEngine.Color32 Mirror.Examples.AdditiveLevels.RandomColor::get_Networkcolor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B RandomColor_get_Networkcolor_mF9458CE8BD5DCFB7C917742935B912995BC78326 (RandomColor_t85537EF520483D7F0DD339CC9F802E1A53CC282F* __this, const RuntimeMethod* method) 
{
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_0 = __this->___color_13;
		return L_0;
	}
}
// System.Void Mirror.Examples.AdditiveLevels.RandomColor::set_Networkcolor(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomColor_set_Networkcolor_mF1B49792161BF2F51CF5745D283D69E1EEF2CCC3 (RandomColor_t85537EF520483D7F0DD339CC9F802E1A53CC282F* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkBehaviour_GeneratedSyncVarSetter_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mD43D62C7BD24575258CA0CF86ABD19131A08BE2D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RandomColor_SetColor_mBDDBD9EE5A4F48CE9121665E51B8DE6AC5E643DD_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_0 = ___value0;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* L_1 = (&__this->___color_13);
		Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* L_2 = (Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA*)il2cpp_codegen_object_new(Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		Action_2__ctor_m7FD44B2A35F4476C6ECC24DA778FEB2EA6A0AAA3(L_2, __this, (intptr_t)((void*)RandomColor_SetColor_mBDDBD9EE5A4F48CE9121665E51B8DE6AC5E643DD_RuntimeMethod_var), NULL);
		NetworkBehaviour_GeneratedSyncVarSetter_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mD43D62C7BD24575258CA0CF86ABD19131A08BE2D_inline(__this, L_0, L_1, ((int64_t)1LL), L_2, NetworkBehaviour_GeneratedSyncVarSetter_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mD43D62C7BD24575258CA0CF86ABD19131A08BE2D_RuntimeMethod_var);
		return;
	}
}
// System.Boolean Mirror.Examples.AdditiveLevels.RandomColor::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool RandomColor_SerializeSyncVars_m5ACC9F3AAA5AA3B9D49636D69B4F79630B459DF7 (RandomColor_t85537EF520483D7F0DD339CC9F802E1A53CC282F* __this, NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, bool ___forceAll1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		bool L_1 = ___forceAll1;
		bool L_2;
		L_2 = NetworkBehaviour_SerializeSyncVars_mF60B2EAAE4B8BBE7044C528F6B3D34CBEB09EA65(__this, L_0, L_1, NULL);
		V_0 = L_2;
		bool L_3 = ___forceAll1;
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_4 = ___writer0;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_5 = __this->___color_13;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteColor32_m123810E64991275156516FBB8AD2CFA67A7C3B7B_inline(L_4, L_5, NULL);
		return (bool)1;
	}

IL_001d:
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_6 = ___writer0;
		uint64_t L_7;
		L_7 = NetworkBehaviour_get_syncVarDirtyBits_m96E01E08568BF6521B3B69F8F8F36884CAABEEB4_inline(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteULong_mC0AE4801C58209EF02B73E3B353100B3AB95D28C_inline(L_6, L_7, NULL);
		uint64_t L_8;
		L_8 = NetworkBehaviour_get_syncVarDirtyBits_m96E01E08568BF6521B3B69F8F8F36884CAABEEB4_inline(__this, NULL);
		if (!((int64_t)((int64_t)L_8&((int64_t)1LL))))
		{
			goto IL_004d;
		}
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_9 = ___writer0;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_10 = __this->___color_13;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteColor32_m123810E64991275156516FBB8AD2CFA67A7C3B7B_inline(L_9, L_10, NULL);
		V_0 = (bool)1;
	}

IL_004d:
	{
		bool L_11 = V_0;
		return L_11;
	}
}
// System.Void Mirror.Examples.AdditiveLevels.RandomColor::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomColor_DeserializeSyncVars_m90144C33771846F61AA6F3A8D61883723496928E (RandomColor_t85537EF520483D7F0DD339CC9F802E1A53CC282F* __this, NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, bool ___initialState1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkBehaviour_GeneratedSyncVarDeserialize_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mCD3E867438BB843F76C76561EC9F138D8CC6694D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RandomColor_SetColor_mBDDBD9EE5A4F48CE9121665E51B8DE6AC5E643DD_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	{
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		bool L_1 = ___initialState1;
		NetworkBehaviour_DeserializeSyncVars_m4EECC3AAB3CAAD098D0B6056C590BBD98AEAF68C(__this, L_0, L_1, NULL);
		bool L_2 = ___initialState1;
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* L_3 = (&__this->___color_13);
		Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* L_4 = (Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA*)il2cpp_codegen_object_new(Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Action_2__ctor_m7FD44B2A35F4476C6ECC24DA778FEB2EA6A0AAA3(L_4, __this, (intptr_t)((void*)RandomColor_SetColor_mBDDBD9EE5A4F48CE9121665E51B8DE6AC5E643DD_RuntimeMethod_var), NULL);
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_5 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_6;
		L_6 = NetworkReaderExtensions_ReadColor32_m0F0066C51CACC736B893D9F3C1D4324F87641BEF_inline(L_5, NULL);
		NetworkBehaviour_GeneratedSyncVarDeserialize_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mCD3E867438BB843F76C76561EC9F138D8CC6694D_inline(__this, L_3, L_4, L_6, NetworkBehaviour_GeneratedSyncVarDeserialize_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mCD3E867438BB843F76C76561EC9F138D8CC6694D_RuntimeMethod_var);
		return;
	}

IL_002d:
	{
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_7 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint64_t L_8;
		L_8 = NetworkReaderExtensions_ReadULong_mAC6B83521EBA7FFEDFC72A6AAE1BF5D87221A5F5_inline(L_7, NULL);
		V_0 = L_8;
		int64_t L_9 = V_0;
		if (!((int64_t)(L_9&((int64_t)1LL))))
		{
			goto IL_0063;
		}
	}
	{
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* L_10 = (&__this->___color_13);
		Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* L_11 = (Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA*)il2cpp_codegen_object_new(Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA_il2cpp_TypeInfo_var);
		NullCheck(L_11);
		Action_2__ctor_m7FD44B2A35F4476C6ECC24DA778FEB2EA6A0AAA3(L_11, __this, (intptr_t)((void*)RandomColor_SetColor_mBDDBD9EE5A4F48CE9121665E51B8DE6AC5E643DD_RuntimeMethod_var), NULL);
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_12 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_13;
		L_13 = NetworkReaderExtensions_ReadColor32_m0F0066C51CACC736B893D9F3C1D4324F87641BEF_inline(L_12, NULL);
		NetworkBehaviour_GeneratedSyncVarDeserialize_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mCD3E867438BB843F76C76561EC9F138D8CC6694D_inline(__this, L_10, L_11, L_13, NetworkBehaviour_GeneratedSyncVarDeserialize_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mCD3E867438BB843F76C76561EC9F138D8CC6694D_RuntimeMethod_var);
	}

IL_0063:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Mirror.ReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ReadyMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadyMessage_t827D165B99D0F8834C4F35860876486AFB9867F8 GeneratedNetworkCode__Read_Mirror_ReadyMessage_m39CE2D271EF006C1BF90E631FE38A0509A2A7174 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	ReadyMessage_t827D165B99D0F8834C4F35860876486AFB9867F8 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(ReadyMessage_t827D165B99D0F8834C4F35860876486AFB9867F8));
		ReadyMessage_t827D165B99D0F8834C4F35860876486AFB9867F8 L_0 = V_0;
		return L_0;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ReadyMessage(Mirror.NetworkWriter,Mirror.ReadyMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_ReadyMessage_mBAE8C7940A984DEEA5331A23E0A01BB3A6B481CD (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, ReadyMessage_t827D165B99D0F8834C4F35860876486AFB9867F8 ___value1, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// Mirror.NotReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NotReadyMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NotReadyMessage_tF34CF670A9AD115E0FDC1F7BCE4F75A4C9172036 GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m208EB5BA02CD99D1236C15A7B3E6C13E0935702B (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	NotReadyMessage_tF34CF670A9AD115E0FDC1F7BCE4F75A4C9172036 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(NotReadyMessage_tF34CF670A9AD115E0FDC1F7BCE4F75A4C9172036));
		NotReadyMessage_tF34CF670A9AD115E0FDC1F7BCE4F75A4C9172036 L_0 = V_0;
		return L_0;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NotReadyMessage(Mirror.NetworkWriter,Mirror.NotReadyMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_NotReadyMessage_mF6F4E00A0FB4198BA8291861F03B44A5EA9473E2 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, NotReadyMessage_tF34CF670A9AD115E0FDC1F7BCE4F75A4C9172036 ___value1, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// Mirror.AddPlayerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.AddPlayerMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AddPlayerMessage_t8B70DF20613DB8A7E5B2A4DAAF2674D0DFD2F8F6 GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_m7C5BA3DA8EFBFCD37258B6FCA8B0DD3DAC4DC9B3 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	AddPlayerMessage_t8B70DF20613DB8A7E5B2A4DAAF2674D0DFD2F8F6 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(AddPlayerMessage_t8B70DF20613DB8A7E5B2A4DAAF2674D0DFD2F8F6));
		AddPlayerMessage_t8B70DF20613DB8A7E5B2A4DAAF2674D0DFD2F8F6 L_0 = V_0;
		return L_0;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.AddPlayerMessage(Mirror.NetworkWriter,Mirror.AddPlayerMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_mEDEBEED5BBA0B7D7E68879616FC283492746A528 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, AddPlayerMessage_t8B70DF20613DB8A7E5B2A4DAAF2674D0DFD2F8F6 ___value1, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// Mirror.SceneMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SceneMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SceneMessage_t47621E5F4F96B23FBA41C9E4015DCDB9F0BE6C26 GeneratedNetworkCode__Read_Mirror_SceneMessage_m5E357A51D411045B49C0B390BBBEEA981F754A43 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	SceneMessage_t47621E5F4F96B23FBA41C9E4015DCDB9F0BE6C26 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(SceneMessage_t47621E5F4F96B23FBA41C9E4015DCDB9F0BE6C26));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = NetworkReaderExtensions_ReadString_m5A56179D2C1CB509EC9C762F97BBD4133A5AD394_inline(L_0, NULL);
		(&V_0)->___sceneName_0 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_0)->___sceneName_0), (void*)L_1);
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		uint8_t L_3;
		L_3 = GeneratedNetworkCode__Read_Mirror_SceneOperation_m74E8C7FCF4974E3652B0B512F68390584B1B3DEF(L_2, NULL);
		(&V_0)->___sceneOperation_1 = L_3;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_4 = ___reader0;
		bool L_5;
		L_5 = NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06_inline(L_4, NULL);
		(&V_0)->___customHandling_2 = L_5;
		SceneMessage_t47621E5F4F96B23FBA41C9E4015DCDB9F0BE6C26 L_6 = V_0;
		return L_6;
	}
}
// Mirror.SceneOperation Mirror.GeneratedNetworkCode::_Read_Mirror.SceneOperation(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t GeneratedNetworkCode__Read_Mirror_SceneOperation_m74E8C7FCF4974E3652B0B512F68390584B1B3DEF (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint8_t L_1;
		L_1 = NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_inline(L_0, NULL);
		return (uint8_t)(L_1);
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneMessage(Mirror.NetworkWriter,Mirror.SceneMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_SceneMessage_m2415AA836382B52EAF54E4CBB6425300C99BEF86 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, SceneMessage_t47621E5F4F96B23FBA41C9E4015DCDB9F0BE6C26 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		SceneMessage_t47621E5F4F96B23FBA41C9E4015DCDB9F0BE6C26 L_1 = ___value1;
		String_t* L_2 = L_1.___sceneName_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98_inline(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		SceneMessage_t47621E5F4F96B23FBA41C9E4015DCDB9F0BE6C26 L_4 = ___value1;
		uint8_t L_5 = L_4.___sceneOperation_1;
		GeneratedNetworkCode__Write_Mirror_SceneOperation_mA2C6453809A524632C4C882EE0133AAE2B596560(L_3, L_5, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_6 = ___writer0;
		SceneMessage_t47621E5F4F96B23FBA41C9E4015DCDB9F0BE6C26 L_7 = ___value1;
		bool L_8 = L_7.___customHandling_2;
		NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45_inline(L_6, L_8, NULL);
		return;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneOperation(Mirror.NetworkWriter,Mirror.SceneOperation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_SceneOperation_mA2C6453809A524632C4C882EE0133AAE2B596560 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint8_t ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		uint8_t L_1 = ___value1;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_inline(L_0, L_1, NULL);
		return;
	}
}
// Mirror.CommandMessage Mirror.GeneratedNetworkCode::_Read_Mirror.CommandMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CommandMessage_t33C5D102BB2924A7CB43AC46B166A390E5EB893E GeneratedNetworkCode__Read_Mirror_CommandMessage_m1EB0BBD20B46C4D33A437C5D4CF2D8EA43F18A8B (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CommandMessage_t33C5D102BB2924A7CB43AC46B166A390E5EB893E V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(CommandMessage_t33C5D102BB2924A7CB43AC46B166A390E5EB893E));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint32_t L_1;
		L_1 = NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_inline(L_0, NULL);
		(&V_0)->___netId_0 = L_1;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		uint8_t L_3;
		L_3 = NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_inline(L_2, NULL);
		(&V_0)->___componentIndex_1 = L_3;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_4 = ___reader0;
		int32_t L_5;
		L_5 = NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A_inline(L_4, NULL);
		(&V_0)->___functionHash_2 = L_5;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_6 = ___reader0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_7;
		L_7 = NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F_inline(L_6, NULL);
		(&V_0)->___payload_3 = L_7;
		Il2CppCodeGenWriteBarrier((void**)&(((&(&V_0)->___payload_3))->____array_1), (void*)NULL);
		CommandMessage_t33C5D102BB2924A7CB43AC46B166A390E5EB893E L_8 = V_0;
		return L_8;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.CommandMessage(Mirror.NetworkWriter,Mirror.CommandMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_CommandMessage_mB652F843580DC0EB2C46997A80B317C449CEC38F (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, CommandMessage_t33C5D102BB2924A7CB43AC46B166A390E5EB893E ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		CommandMessage_t33C5D102BB2924A7CB43AC46B166A390E5EB893E L_1 = ___value1;
		uint32_t L_2 = L_1.___netId_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_inline(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		CommandMessage_t33C5D102BB2924A7CB43AC46B166A390E5EB893E L_4 = ___value1;
		uint8_t L_5 = L_4.___componentIndex_1;
		NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_inline(L_3, L_5, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_6 = ___writer0;
		CommandMessage_t33C5D102BB2924A7CB43AC46B166A390E5EB893E L_7 = ___value1;
		int32_t L_8 = L_7.___functionHash_2;
		NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_inline(L_6, L_8, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_9 = ___writer0;
		CommandMessage_t33C5D102BB2924A7CB43AC46B166A390E5EB893E L_10 = ___value1;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_11 = L_10.___payload_3;
		NetworkWriterExtensions_WriteBytesAndSizeSegment_mD7FCDC44AB0313C3ED0AFBFEC77366B6947672AC_inline(L_9, L_11, NULL);
		return;
	}
}
// Mirror.RpcMessage Mirror.GeneratedNetworkCode::_Read_Mirror.RpcMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RpcMessage_tA24B1AA9AB9593C9491786C4AD6EBD8CD822D4AD GeneratedNetworkCode__Read_Mirror_RpcMessage_m4FC20C673517D44D58414A7AA047AF7D75A377C4 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RpcMessage_tA24B1AA9AB9593C9491786C4AD6EBD8CD822D4AD V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(RpcMessage_tA24B1AA9AB9593C9491786C4AD6EBD8CD822D4AD));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint32_t L_1;
		L_1 = NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_inline(L_0, NULL);
		(&V_0)->___netId_0 = L_1;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		uint8_t L_3;
		L_3 = NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_inline(L_2, NULL);
		(&V_0)->___componentIndex_1 = L_3;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_4 = ___reader0;
		int32_t L_5;
		L_5 = NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A_inline(L_4, NULL);
		(&V_0)->___functionHash_2 = L_5;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_6 = ___reader0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_7;
		L_7 = NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F_inline(L_6, NULL);
		(&V_0)->___payload_3 = L_7;
		Il2CppCodeGenWriteBarrier((void**)&(((&(&V_0)->___payload_3))->____array_1), (void*)NULL);
		RpcMessage_tA24B1AA9AB9593C9491786C4AD6EBD8CD822D4AD L_8 = V_0;
		return L_8;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.RpcMessage(Mirror.NetworkWriter,Mirror.RpcMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_RpcMessage_m690CA90FADE7D8E3E22236A59E88C3EA5D0F720B (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, RpcMessage_tA24B1AA9AB9593C9491786C4AD6EBD8CD822D4AD ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		RpcMessage_tA24B1AA9AB9593C9491786C4AD6EBD8CD822D4AD L_1 = ___value1;
		uint32_t L_2 = L_1.___netId_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_inline(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		RpcMessage_tA24B1AA9AB9593C9491786C4AD6EBD8CD822D4AD L_4 = ___value1;
		uint8_t L_5 = L_4.___componentIndex_1;
		NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_inline(L_3, L_5, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_6 = ___writer0;
		RpcMessage_tA24B1AA9AB9593C9491786C4AD6EBD8CD822D4AD L_7 = ___value1;
		int32_t L_8 = L_7.___functionHash_2;
		NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_inline(L_6, L_8, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_9 = ___writer0;
		RpcMessage_tA24B1AA9AB9593C9491786C4AD6EBD8CD822D4AD L_10 = ___value1;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_11 = L_10.___payload_3;
		NetworkWriterExtensions_WriteBytesAndSizeSegment_mD7FCDC44AB0313C3ED0AFBFEC77366B6947672AC_inline(L_9, L_11, NULL);
		return;
	}
}
// Mirror.SpawnMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SpawnMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 GeneratedNetworkCode__Read_Mirror_SpawnMessage_mEC8E19BCC23380500B3605B78866DDBEA4C904EE (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint32_t L_1;
		L_1 = NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_inline(L_0, NULL);
		(&V_0)->___netId_0 = L_1;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		bool L_3;
		L_3 = NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06_inline(L_2, NULL);
		(&V_0)->___isLocalPlayer_1 = L_3;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_4 = ___reader0;
		bool L_5;
		L_5 = NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06_inline(L_4, NULL);
		(&V_0)->___isOwner_2 = L_5;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_6 = ___reader0;
		uint64_t L_7;
		L_7 = NetworkReaderExtensions_ReadULong_mAC6B83521EBA7FFEDFC72A6AAE1BF5D87221A5F5_inline(L_6, NULL);
		(&V_0)->___sceneId_3 = L_7;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_8 = ___reader0;
		Guid_t L_9;
		L_9 = NetworkReaderExtensions_ReadGuid_mCFFAB7379132286F7C9CC70EC291F8B28EA08B0E_inline(L_8, NULL);
		(&V_0)->___assetId_4 = L_9;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_10 = ___reader0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11;
		L_11 = NetworkReaderExtensions_ReadVector3_mD35BF8B14DD5F75688AB9C360D138D1BAB432637_inline(L_10, NULL);
		(&V_0)->___position_5 = L_11;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_12 = ___reader0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_13;
		L_13 = NetworkReaderExtensions_ReadQuaternion_m135F5C523703C700E6A266DA9718E44D160BB567_inline(L_12, NULL);
		(&V_0)->___rotation_6 = L_13;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_14 = ___reader0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
		L_15 = NetworkReaderExtensions_ReadVector3_mD35BF8B14DD5F75688AB9C360D138D1BAB432637_inline(L_14, NULL);
		(&V_0)->___scale_7 = L_15;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_16 = ___reader0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_17;
		L_17 = NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F_inline(L_16, NULL);
		(&V_0)->___payload_8 = L_17;
		Il2CppCodeGenWriteBarrier((void**)&(((&(&V_0)->___payload_8))->____array_1), (void*)NULL);
		SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 L_18 = V_0;
		return L_18;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SpawnMessage(Mirror.NetworkWriter,Mirror.SpawnMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_SpawnMessage_m4D40E1220150E253BDECE7A9915957EDB4AFC499 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 L_1 = ___value1;
		uint32_t L_2 = L_1.___netId_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_inline(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 L_4 = ___value1;
		bool L_5 = L_4.___isLocalPlayer_1;
		NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45_inline(L_3, L_5, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_6 = ___writer0;
		SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 L_7 = ___value1;
		bool L_8 = L_7.___isOwner_2;
		NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45_inline(L_6, L_8, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_9 = ___writer0;
		SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 L_10 = ___value1;
		uint64_t L_11 = L_10.___sceneId_3;
		NetworkWriterExtensions_WriteULong_mC0AE4801C58209EF02B73E3B353100B3AB95D28C_inline(L_9, L_11, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_12 = ___writer0;
		SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 L_13 = ___value1;
		Guid_t L_14 = L_13.___assetId_4;
		NetworkWriterExtensions_WriteGuid_mFF6C6A1BC90A9A7BBC9C179FA6FC25753689D3F9_inline(L_12, L_14, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_15 = ___writer0;
		SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 L_16 = ___value1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_17 = L_16.___position_5;
		NetworkWriterExtensions_WriteVector3_mB3C2B2F2D3C9874F883C12813FB20B9D5AABC882_inline(L_15, L_17, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_18 = ___writer0;
		SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 L_19 = ___value1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_20 = L_19.___rotation_6;
		NetworkWriterExtensions_WriteQuaternion_mFC2E046965F6BA1B694218D5E60E26807974ACBA_inline(L_18, L_20, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_21 = ___writer0;
		SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 L_22 = ___value1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_23 = L_22.___scale_7;
		NetworkWriterExtensions_WriteVector3_mB3C2B2F2D3C9874F883C12813FB20B9D5AABC882_inline(L_21, L_23, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_24 = ___writer0;
		SpawnMessage_tE475B0CF6074D61D6776360B70400F64B15E1475 L_25 = ___value1;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_26 = L_25.___payload_8;
		NetworkWriterExtensions_WriteBytesAndSizeSegment_mD7FCDC44AB0313C3ED0AFBFEC77366B6947672AC_inline(L_24, L_26, NULL);
		return;
	}
}
// Mirror.ChangeOwnerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ChangeOwnerMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ChangeOwnerMessage_t6539B3468B62CDA56EBE80C50BA9CF93FEF8F55F GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m1ED249926C5FFFF3711276C020ABE54B928BAAEE (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ChangeOwnerMessage_t6539B3468B62CDA56EBE80C50BA9CF93FEF8F55F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(ChangeOwnerMessage_t6539B3468B62CDA56EBE80C50BA9CF93FEF8F55F));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint32_t L_1;
		L_1 = NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_inline(L_0, NULL);
		(&V_0)->___netId_0 = L_1;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		bool L_3;
		L_3 = NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06_inline(L_2, NULL);
		(&V_0)->___isOwner_1 = L_3;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_4 = ___reader0;
		bool L_5;
		L_5 = NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06_inline(L_4, NULL);
		(&V_0)->___isLocalPlayer_2 = L_5;
		ChangeOwnerMessage_t6539B3468B62CDA56EBE80C50BA9CF93FEF8F55F L_6 = V_0;
		return L_6;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ChangeOwnerMessage(Mirror.NetworkWriter,Mirror.ChangeOwnerMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m71C4764A857C326F63101E4181166D807AB065B0 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, ChangeOwnerMessage_t6539B3468B62CDA56EBE80C50BA9CF93FEF8F55F ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		ChangeOwnerMessage_t6539B3468B62CDA56EBE80C50BA9CF93FEF8F55F L_1 = ___value1;
		uint32_t L_2 = L_1.___netId_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_inline(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		ChangeOwnerMessage_t6539B3468B62CDA56EBE80C50BA9CF93FEF8F55F L_4 = ___value1;
		bool L_5 = L_4.___isOwner_1;
		NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45_inline(L_3, L_5, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_6 = ___writer0;
		ChangeOwnerMessage_t6539B3468B62CDA56EBE80C50BA9CF93FEF8F55F L_7 = ___value1;
		bool L_8 = L_7.___isLocalPlayer_2;
		NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45_inline(L_6, L_8, NULL);
		return;
	}
}
// Mirror.ObjectSpawnStartedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectSpawnStartedMessage_tD4E40048359CFB70648E9DEEDF4DD292084FD774 GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_m7236240B1388E3E63F1E45767148C047F2B86681 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	ObjectSpawnStartedMessage_tD4E40048359CFB70648E9DEEDF4DD292084FD774 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(ObjectSpawnStartedMessage_tD4E40048359CFB70648E9DEEDF4DD292084FD774));
		ObjectSpawnStartedMessage_tD4E40048359CFB70648E9DEEDF4DD292084FD774 L_0 = V_0;
		return L_0;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnStartedMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m2875D87F1F21B322D53603292FFE5BF8D604EC47 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, ObjectSpawnStartedMessage_tD4E40048359CFB70648E9DEEDF4DD292084FD774 ___value1, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// Mirror.ObjectSpawnFinishedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectSpawnFinishedMessage_t2E367844E4D843D90ED9E273573BF341B04658D8 GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_m9DCF582A7F2ACD58B46CE0B5006DC8DF925AAB07 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	ObjectSpawnFinishedMessage_t2E367844E4D843D90ED9E273573BF341B04658D8 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(ObjectSpawnFinishedMessage_t2E367844E4D843D90ED9E273573BF341B04658D8));
		ObjectSpawnFinishedMessage_t2E367844E4D843D90ED9E273573BF341B04658D8 L_0 = V_0;
		return L_0;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnFinishedMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_m8F8249A46B9C3061ED971ADF650AC9609E486619 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, ObjectSpawnFinishedMessage_t2E367844E4D843D90ED9E273573BF341B04658D8 ___value1, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// Mirror.ObjectDestroyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectDestroyMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectDestroyMessage_tF205F01F24B264A63044BA3FAC1E9B080DB068D2 GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_m1805A0712B400427B3DA28B6B83D2DB7BA47184C (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ObjectDestroyMessage_tF205F01F24B264A63044BA3FAC1E9B080DB068D2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(ObjectDestroyMessage_tF205F01F24B264A63044BA3FAC1E9B080DB068D2));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint32_t L_1;
		L_1 = NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_inline(L_0, NULL);
		(&V_0)->___netId_0 = L_1;
		ObjectDestroyMessage_tF205F01F24B264A63044BA3FAC1E9B080DB068D2 L_2 = V_0;
		return L_2;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectDestroyMessage(Mirror.NetworkWriter,Mirror.ObjectDestroyMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_m635CCE7CF2563B4550CB476F6AC5EEB18A6D939B (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, ObjectDestroyMessage_tF205F01F24B264A63044BA3FAC1E9B080DB068D2 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		ObjectDestroyMessage_tF205F01F24B264A63044BA3FAC1E9B080DB068D2 L_1 = ___value1;
		uint32_t L_2 = L_1.___netId_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_inline(L_0, L_2, NULL);
		return;
	}
}
// Mirror.ObjectHideMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectHideMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectHideMessage_t13A7D352E4B0A0D08A38BCAC3E454CDB59756F3C GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_mEFA99EDE85679F1A1946C256D3829EC04CBF8A40 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ObjectHideMessage_t13A7D352E4B0A0D08A38BCAC3E454CDB59756F3C V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(ObjectHideMessage_t13A7D352E4B0A0D08A38BCAC3E454CDB59756F3C));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint32_t L_1;
		L_1 = NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_inline(L_0, NULL);
		(&V_0)->___netId_0 = L_1;
		ObjectHideMessage_t13A7D352E4B0A0D08A38BCAC3E454CDB59756F3C L_2 = V_0;
		return L_2;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectHideMessage(Mirror.NetworkWriter,Mirror.ObjectHideMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_m2233A20099B4B9AAD2F5232DC7B04C59A1581350 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, ObjectHideMessage_t13A7D352E4B0A0D08A38BCAC3E454CDB59756F3C ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		ObjectHideMessage_t13A7D352E4B0A0D08A38BCAC3E454CDB59756F3C L_1 = ___value1;
		uint32_t L_2 = L_1.___netId_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_inline(L_0, L_2, NULL);
		return;
	}
}
// Mirror.EntityStateMessage Mirror.GeneratedNetworkCode::_Read_Mirror.EntityStateMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EntityStateMessage_t741DD321283AD1933F59FB67E67905E4DE9A744E GeneratedNetworkCode__Read_Mirror_EntityStateMessage_mFACC1BD75FFD70F53F10FD27A7EDB7AC60E2DB5D (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EntityStateMessage_t741DD321283AD1933F59FB67E67905E4DE9A744E V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(EntityStateMessage_t741DD321283AD1933F59FB67E67905E4DE9A744E));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint32_t L_1;
		L_1 = NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_inline(L_0, NULL);
		(&V_0)->___netId_0 = L_1;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_3;
		L_3 = NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F_inline(L_2, NULL);
		(&V_0)->___payload_1 = L_3;
		Il2CppCodeGenWriteBarrier((void**)&(((&(&V_0)->___payload_1))->____array_1), (void*)NULL);
		EntityStateMessage_t741DD321283AD1933F59FB67E67905E4DE9A744E L_4 = V_0;
		return L_4;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.EntityStateMessage(Mirror.NetworkWriter,Mirror.EntityStateMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m68475F9DBE473336E6961AAF9477B31B544CEB61 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, EntityStateMessage_t741DD321283AD1933F59FB67E67905E4DE9A744E ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		EntityStateMessage_t741DD321283AD1933F59FB67E67905E4DE9A744E L_1 = ___value1;
		uint32_t L_2 = L_1.___netId_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_inline(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		EntityStateMessage_t741DD321283AD1933F59FB67E67905E4DE9A744E L_4 = ___value1;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_5 = L_4.___payload_1;
		NetworkWriterExtensions_WriteBytesAndSizeSegment_mD7FCDC44AB0313C3ED0AFBFEC77366B6947672AC_inline(L_3, L_5, NULL);
		return;
	}
}
// Mirror.NetworkPingMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPingMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetworkPingMessage_t254AA1B47CDBC1136A16C49B6147AC5462C60B27 GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m2AB11C6E07E94D6C3DB059650CCA9BD6D9838F5C (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	NetworkPingMessage_t254AA1B47CDBC1136A16C49B6147AC5462C60B27 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(NetworkPingMessage_t254AA1B47CDBC1136A16C49B6147AC5462C60B27));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = NetworkReaderExtensions_ReadDouble_m949A60A21C6EB3B9952A43355903F08B3A7E0EF9_inline(L_0, NULL);
		(&V_0)->___clientTime_0 = L_1;
		NetworkPingMessage_t254AA1B47CDBC1136A16C49B6147AC5462C60B27 L_2 = V_0;
		return L_2;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPingMessage(Mirror.NetworkWriter,Mirror.NetworkPingMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_mFAEC5102CEF441E12C2E1A9A267D3A70C60C7B5C (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, NetworkPingMessage_t254AA1B47CDBC1136A16C49B6147AC5462C60B27 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		NetworkPingMessage_t254AA1B47CDBC1136A16C49B6147AC5462C60B27 L_1 = ___value1;
		double L_2 = L_1.___clientTime_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteDouble_m0475F5FB9E1D69D60501C7158AD3431680BC1BDB_inline(L_0, L_2, NULL);
		return;
	}
}
// Mirror.NetworkPongMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPongMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NetworkPongMessage_tD0BD2C925B3E72156657A78E2D9AD09D3E3B4EC6 GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_mD30EC9CAE30F7761C7D307A8671F27E9D63BAC17 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	NetworkPongMessage_tD0BD2C925B3E72156657A78E2D9AD09D3E3B4EC6 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(NetworkPongMessage_tD0BD2C925B3E72156657A78E2D9AD09D3E3B4EC6));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = NetworkReaderExtensions_ReadDouble_m949A60A21C6EB3B9952A43355903F08B3A7E0EF9_inline(L_0, NULL);
		(&V_0)->___clientTime_0 = L_1;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		double L_3;
		L_3 = NetworkReaderExtensions_ReadDouble_m949A60A21C6EB3B9952A43355903F08B3A7E0EF9_inline(L_2, NULL);
		(&V_0)->___serverTime_1 = L_3;
		NetworkPongMessage_tD0BD2C925B3E72156657A78E2D9AD09D3E3B4EC6 L_4 = V_0;
		return L_4;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPongMessage(Mirror.NetworkWriter,Mirror.NetworkPongMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_m79990113E28B9ADA181528EFC5D92C21AEC24CAF (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, NetworkPongMessage_tD0BD2C925B3E72156657A78E2D9AD09D3E3B4EC6 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		NetworkPongMessage_tD0BD2C925B3E72156657A78E2D9AD09D3E3B4EC6 L_1 = ___value1;
		double L_2 = L_1.___clientTime_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteDouble_m0475F5FB9E1D69D60501C7158AD3431680BC1BDB_inline(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		NetworkPongMessage_tD0BD2C925B3E72156657A78E2D9AD09D3E3B4EC6 L_4 = ___value1;
		double L_5 = L_4.___serverTime_1;
		NetworkWriterExtensions_WriteDouble_m0475F5FB9E1D69D60501C7158AD3431680BC1BDB_inline(L_3, L_5, NULL);
		return;
	}
}
// Mirror.Examples.MultipleMatch.ServerMatchMessage Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.ServerMatchMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ServerMatchMessage_tAA90E663A53A2A48E57057D0B00DA4251AC000B8 GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ServerMatchMessage_m45838D1053B8C35602D526DA8A918170A84E882B (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ServerMatchMessage_tAA90E663A53A2A48E57057D0B00DA4251AC000B8 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(ServerMatchMessage_tAA90E663A53A2A48E57057D0B00DA4251AC000B8));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		uint8_t L_1;
		L_1 = GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ServerMatchOperation_m0E0217A1EA28419B9541B868BB7E160A7F9876C8(L_0, NULL);
		(&V_0)->___serverMatchOperation_0 = L_1;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		Guid_t L_3;
		L_3 = NetworkReaderExtensions_ReadGuid_mCFFAB7379132286F7C9CC70EC291F8B28EA08B0E_inline(L_2, NULL);
		(&V_0)->___matchId_1 = L_3;
		ServerMatchMessage_tAA90E663A53A2A48E57057D0B00DA4251AC000B8 L_4 = V_0;
		return L_4;
	}
}
// Mirror.Examples.MultipleMatch.ServerMatchOperation Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.ServerMatchOperation(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ServerMatchOperation_m0E0217A1EA28419B9541B868BB7E160A7F9876C8 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint8_t L_1;
		L_1 = NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_inline(L_0, NULL);
		return (uint8_t)(L_1);
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.ServerMatchMessage(Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ServerMatchMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ServerMatchMessage_mF90A563F787035F17D0CD90966372FB8DEFF4A03 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, ServerMatchMessage_tAA90E663A53A2A48E57057D0B00DA4251AC000B8 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		ServerMatchMessage_tAA90E663A53A2A48E57057D0B00DA4251AC000B8 L_1 = ___value1;
		uint8_t L_2 = L_1.___serverMatchOperation_0;
		GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ServerMatchOperation_m4ED27F7C3469C55F345EFBE642DC3F4DB0FB9B5C(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		ServerMatchMessage_tAA90E663A53A2A48E57057D0B00DA4251AC000B8 L_4 = ___value1;
		Guid_t L_5 = L_4.___matchId_1;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteGuid_mFF6C6A1BC90A9A7BBC9C179FA6FC25753689D3F9_inline(L_3, L_5, NULL);
		return;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.ServerMatchOperation(Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ServerMatchOperation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ServerMatchOperation_m4ED27F7C3469C55F345EFBE642DC3F4DB0FB9B5C (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint8_t ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		uint8_t L_1 = ___value1;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_inline(L_0, L_1, NULL);
		return;
	}
}
// Mirror.Examples.MultipleMatch.ClientMatchMessage Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.ClientMatchMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ClientMatchMessage_tE0C6260F938E859985838F9C68E0C87107E97837 GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ClientMatchMessage_m588555F2CD175A2C40F8045755BF3F659CDB65B4 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ClientMatchMessage_tE0C6260F938E859985838F9C68E0C87107E97837 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(ClientMatchMessage_tE0C6260F938E859985838F9C68E0C87107E97837));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		uint8_t L_1;
		L_1 = GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ClientMatchOperation_m967DCEAD7E36E480A8496E822CACE1F60AE9AFB9(L_0, NULL);
		(&V_0)->___clientMatchOperation_0 = L_1;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		Guid_t L_3;
		L_3 = NetworkReaderExtensions_ReadGuid_mCFFAB7379132286F7C9CC70EC291F8B28EA08B0E_inline(L_2, NULL);
		(&V_0)->___matchId_1 = L_3;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_4 = ___reader0;
		MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* L_5;
		L_5 = GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchInfoU5BU5D_mE23CB91C4A5FF33675CF8A56B16BA006EC9047E9(L_4, NULL);
		(&V_0)->___matchInfos_2 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_0)->___matchInfos_2), (void*)L_5);
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_6 = ___reader0;
		PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* L_7;
		L_7 = GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_PlayerInfoU5BU5D_mDB000F31EDB9991937FE2C78FE0E3009EF2C1209(L_6, NULL);
		(&V_0)->___playerInfos_3 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_0)->___playerInfos_3), (void*)L_7);
		ClientMatchMessage_tE0C6260F938E859985838F9C68E0C87107E97837 L_8 = V_0;
		return L_8;
	}
}
// Mirror.Examples.MultipleMatch.ClientMatchOperation Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.ClientMatchOperation(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ClientMatchOperation_m967DCEAD7E36E480A8496E822CACE1F60AE9AFB9 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint8_t L_1;
		L_1 = NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_inline(L_0, NULL);
		return (uint8_t)(L_1);
	}
}
// Mirror.Examples.MultipleMatch.MatchInfo[] Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.MatchInfo[](Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchInfoU5BU5D_mE23CB91C4A5FF33675CF8A56B16BA006EC9047E9 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m3CE6D0D53E5BA355729F19428F259391B4A375B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* L_1;
		L_1 = NetworkReaderExtensions_ReadArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m3CE6D0D53E5BA355729F19428F259391B4A375B5_inline(L_0, NetworkReaderExtensions_ReadArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m3CE6D0D53E5BA355729F19428F259391B4A375B5_RuntimeMethod_var);
		return L_1;
	}
}
// Mirror.Examples.MultipleMatch.MatchInfo Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.MatchInfo(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchInfo_mB709C9D76C0446BF9C8C43CEBFCEC92A0D42551A (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		Guid_t L_1;
		L_1 = NetworkReaderExtensions_ReadGuid_mCFFAB7379132286F7C9CC70EC291F8B28EA08B0E_inline(L_0, NULL);
		(&V_0)->___matchId_0 = L_1;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		uint8_t L_3;
		L_3 = NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_inline(L_2, NULL);
		(&V_0)->___players_1 = L_3;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_4 = ___reader0;
		uint8_t L_5;
		L_5 = NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_inline(L_4, NULL);
		(&V_0)->___maxPlayers_2 = L_5;
		MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 L_6 = V_0;
		return L_6;
	}
}
// Mirror.Examples.MultipleMatch.PlayerInfo[] Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.PlayerInfo[](Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_PlayerInfoU5BU5D_mDB000F31EDB9991937FE2C78FE0E3009EF2C1209 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_mC8AFAB257D0DC0249D387758D4F204833AADE730_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* L_1;
		L_1 = NetworkReaderExtensions_ReadArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_mC8AFAB257D0DC0249D387758D4F204833AADE730_inline(L_0, NetworkReaderExtensions_ReadArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_mC8AFAB257D0DC0249D387758D4F204833AADE730_RuntimeMethod_var);
		return L_1;
	}
}
// Mirror.Examples.MultipleMatch.PlayerInfo Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.PlayerInfo(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_PlayerInfo_mAAF3F3CF61F837BA88391A854E75733E0F5014F6 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A_inline(L_0, NULL);
		(&V_0)->___playerIndex_0 = L_1;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		bool L_3;
		L_3 = NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06_inline(L_2, NULL);
		(&V_0)->___ready_1 = L_3;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_4 = ___reader0;
		Guid_t L_5;
		L_5 = NetworkReaderExtensions_ReadGuid_mCFFAB7379132286F7C9CC70EC291F8B28EA08B0E_inline(L_4, NULL);
		(&V_0)->___matchId_2 = L_5;
		PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 L_6 = V_0;
		return L_6;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.ClientMatchMessage(Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ClientMatchMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ClientMatchMessage_m9534DBB36A6BF3241779E9E5168D444DDF31E51C (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, ClientMatchMessage_tE0C6260F938E859985838F9C68E0C87107E97837 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		ClientMatchMessage_tE0C6260F938E859985838F9C68E0C87107E97837 L_1 = ___value1;
		uint8_t L_2 = L_1.___clientMatchOperation_0;
		GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ClientMatchOperation_m08496CD05975A28AB59D4513B9BA36209FD081C0(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		ClientMatchMessage_tE0C6260F938E859985838F9C68E0C87107E97837 L_4 = ___value1;
		Guid_t L_5 = L_4.___matchId_1;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteGuid_mFF6C6A1BC90A9A7BBC9C179FA6FC25753689D3F9_inline(L_3, L_5, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_6 = ___writer0;
		ClientMatchMessage_tE0C6260F938E859985838F9C68E0C87107E97837 L_7 = ___value1;
		MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* L_8 = L_7.___matchInfos_2;
		GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchInfoU5BU5D_m2A631F7A481D201FABBF213396D9D3C548AB54C0(L_6, L_8, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_9 = ___writer0;
		ClientMatchMessage_tE0C6260F938E859985838F9C68E0C87107E97837 L_10 = ___value1;
		PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* L_11 = L_10.___playerInfos_3;
		GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_PlayerInfoU5BU5D_m258F7D9CB39C3B6D8C6B2BA7E8B1FE9491B38B50(L_9, L_11, NULL);
		return;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.ClientMatchOperation(Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.ClientMatchOperation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ClientMatchOperation_m08496CD05975A28AB59D4513B9BA36209FD081C0 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint8_t ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		uint8_t L_1 = ___value1;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_inline(L_0, L_1, NULL);
		return;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.MatchInfo[](Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchInfo[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchInfoU5BU5D_m2A631F7A481D201FABBF213396D9D3C548AB54C0 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m0946D038BE696722FB3ED0523DEC8233C19FFAC2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* L_1 = ___value1;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m0946D038BE696722FB3ED0523DEC8233C19FFAC2_inline(L_0, L_1, NetworkWriterExtensions_WriteArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m0946D038BE696722FB3ED0523DEC8233C19FFAC2_RuntimeMethod_var);
		return;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.MatchInfo(Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchInfo_m154E3D80BB1984768FBF6C11EC8128FFF90EB743 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 L_1 = ___value1;
		Guid_t L_2 = L_1.___matchId_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteGuid_mFF6C6A1BC90A9A7BBC9C179FA6FC25753689D3F9_inline(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 L_4 = ___value1;
		uint8_t L_5 = L_4.___players_1;
		NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_inline(L_3, L_5, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_6 = ___writer0;
		MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 L_7 = ___value1;
		uint8_t L_8 = L_7.___maxPlayers_2;
		NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_inline(L_6, L_8, NULL);
		return;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.PlayerInfo[](Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.PlayerInfo[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_PlayerInfoU5BU5D_m258F7D9CB39C3B6D8C6B2BA7E8B1FE9491B38B50 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_m348C67F42E07DE2F8A0295C5B388A6985FF81A0E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* L_1 = ___value1;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_m348C67F42E07DE2F8A0295C5B388A6985FF81A0E_inline(L_0, L_1, NetworkWriterExtensions_WriteArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_m348C67F42E07DE2F8A0295C5B388A6985FF81A0E_RuntimeMethod_var);
		return;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.PlayerInfo(Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.PlayerInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_PlayerInfo_m03B2F18306AC1FE8C7C933859A265C2B38C03700 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 L_1 = ___value1;
		int32_t L_2 = L_1.___playerIndex_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_inline(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 L_4 = ___value1;
		bool L_5 = L_4.___ready_1;
		NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45_inline(L_3, L_5, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_6 = ___writer0;
		PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 L_7 = ___value1;
		Guid_t L_8 = L_7.___matchId_2;
		NetworkWriterExtensions_WriteGuid_mFF6C6A1BC90A9A7BBC9C179FA6FC25753689D3F9_inline(L_6, L_8, NULL);
		return;
	}
}
// Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AuthRequestMessage_t73733A4DC155F7E3523FC84D17B810F3EEFC7BE2 GeneratedNetworkCode__Read_Mirror_Examples_Chat_ChatAuthenticator_AuthRequestMessage_mA629A2F8CCBFF4C76544ED51BD2A35A5947315EF (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	AuthRequestMessage_t73733A4DC155F7E3523FC84D17B810F3EEFC7BE2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(AuthRequestMessage_t73733A4DC155F7E3523FC84D17B810F3EEFC7BE2));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = NetworkReaderExtensions_ReadString_m5A56179D2C1CB509EC9C762F97BBD4133A5AD394_inline(L_0, NULL);
		(&V_0)->___authUsername_0 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_0)->___authUsername_0), (void*)L_1);
		AuthRequestMessage_t73733A4DC155F7E3523FC84D17B810F3EEFC7BE2 L_2 = V_0;
		return L_2;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage(Mirror.NetworkWriter,Mirror.Examples.Chat.ChatAuthenticator/AuthRequestMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_Chat_ChatAuthenticator_AuthRequestMessage_m8266348F8A557605B0C42EC1EF5EA60DBA5E1C53 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, AuthRequestMessage_t73733A4DC155F7E3523FC84D17B810F3EEFC7BE2 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		AuthRequestMessage_t73733A4DC155F7E3523FC84D17B810F3EEFC7BE2 L_1 = ___value1;
		String_t* L_2 = L_1.___authUsername_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98_inline(L_0, L_2, NULL);
		return;
	}
}
// Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AuthResponseMessage_t0A0B56A8B7E569EF74D718C068255179F3E04CE7 GeneratedNetworkCode__Read_Mirror_Examples_Chat_ChatAuthenticator_AuthResponseMessage_m43271020844960E04376F6C896CFE7D9AC33124D (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	AuthResponseMessage_t0A0B56A8B7E569EF74D718C068255179F3E04CE7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(AuthResponseMessage_t0A0B56A8B7E569EF74D718C068255179F3E04CE7));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint8_t L_1;
		L_1 = NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_inline(L_0, NULL);
		(&V_0)->___code_0 = L_1;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		String_t* L_3;
		L_3 = NetworkReaderExtensions_ReadString_m5A56179D2C1CB509EC9C762F97BBD4133A5AD394_inline(L_2, NULL);
		(&V_0)->___message_1 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_0)->___message_1), (void*)L_3);
		AuthResponseMessage_t0A0B56A8B7E569EF74D718C068255179F3E04CE7 L_4 = V_0;
		return L_4;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage(Mirror.NetworkWriter,Mirror.Examples.Chat.ChatAuthenticator/AuthResponseMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_Chat_ChatAuthenticator_AuthResponseMessage_m12300CEF77BCC2BC22FE9F7B9385CE87D7B02E10 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, AuthResponseMessage_t0A0B56A8B7E569EF74D718C068255179F3E04CE7 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		AuthResponseMessage_t0A0B56A8B7E569EF74D718C068255179F3E04CE7 L_1 = ___value1;
		uint8_t L_2 = L_1.___code_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_inline(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		AuthResponseMessage_t0A0B56A8B7E569EF74D718C068255179F3E04CE7 L_4 = ___value1;
		String_t* L_5 = L_4.___message_1;
		NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98_inline(L_3, L_5, NULL);
		return;
	}
}
// Mirror.Examples.MultipleMatch.MatchPlayerData Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.MatchPlayerData(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MatchPlayerData_t8CBE176A0E06C3BDF615BAAF008FBD6E419977A9 GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchPlayerData_m3268F6B9575F69594F5CEE743E0EA6FDF9ED56E3 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	MatchPlayerData_t8CBE176A0E06C3BDF615BAAF008FBD6E419977A9 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(MatchPlayerData_t8CBE176A0E06C3BDF615BAAF008FBD6E419977A9));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A_inline(L_0, NULL);
		(&V_0)->___playerIndex_0 = L_1;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		int32_t L_3;
		L_3 = NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A_inline(L_2, NULL);
		(&V_0)->___wins_1 = L_3;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_4 = ___reader0;
		uint16_t L_5;
		L_5 = GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_CellValue_m960AA8E4580568652E48F8810768B4DEA14B34F8(L_4, NULL);
		(&V_0)->___currentScore_2 = L_5;
		MatchPlayerData_t8CBE176A0E06C3BDF615BAAF008FBD6E419977A9 L_6 = V_0;
		return L_6;
	}
}
// Mirror.Examples.MultipleMatch.CellValue Mirror.GeneratedNetworkCode::_Read_Mirror.Examples.MultipleMatch.CellValue(Mirror.NetworkReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint16_t GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_CellValue_m960AA8E4580568652E48F8810768B4DEA14B34F8 (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint16_t L_1;
		L_1 = NetworkReaderExtensions_ReadUShort_mA98395DD1B1DA249096858B171B8BC23D95DF765_inline(L_0, NULL);
		return (uint16_t)(L_1);
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.MatchPlayerData(Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.MatchPlayerData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchPlayerData_m30BD65E22A872300D89020C18828BD180BA41FA7 (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, MatchPlayerData_t8CBE176A0E06C3BDF615BAAF008FBD6E419977A9 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		MatchPlayerData_t8CBE176A0E06C3BDF615BAAF008FBD6E419977A9 L_1 = ___value1;
		int32_t L_2 = L_1.___playerIndex_0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_inline(L_0, L_2, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_3 = ___writer0;
		MatchPlayerData_t8CBE176A0E06C3BDF615BAAF008FBD6E419977A9 L_4 = ___value1;
		int32_t L_5 = L_4.___wins_1;
		NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_inline(L_3, L_5, NULL);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_6 = ___writer0;
		MatchPlayerData_t8CBE176A0E06C3BDF615BAAF008FBD6E419977A9 L_7 = ___value1;
		uint16_t L_8 = L_7.___currentScore_2;
		GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_CellValue_mE850D02656D8C7B9181D58B92D4CA0583C063A1D(L_6, L_8, NULL);
		return;
	}
}
// System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Examples.MultipleMatch.CellValue(Mirror.NetworkWriter,Mirror.Examples.MultipleMatch.CellValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_CellValue_mE850D02656D8C7B9181D58B92D4CA0583C063A1D (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint16_t ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		uint16_t L_1 = ___value1;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteUShort_m4AEC8147034117F9EB131043089577CD2DB42DB4_inline(L_0, L_1, NULL);
		return;
	}
}
// System.Void Mirror.GeneratedNetworkCode::InitReadWriters()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedNetworkCode_InitReadWriters_mF40A9E3C3CC8A4A9AE00D7EE2807AD0558D76161 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t00EE65E013F88779EEEE98835E86BDF400A3BF36_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t06CF1E09D2E968AE317C1B4066FE59281D0D7F35_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t0708A1448BB2410AD3F0DF78AF427D6730760C7F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t0726BDF29C00FDD4DA0F278B3AFC4E47F69A4CED_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t0FDD9213EEB281B92425DB7083B3E28D691AA59F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t102F2BE697BDA0818FA414975B57C948D9C2BBDF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t104D6887DC3FA703E6D350257E4EE72A3EFCF3A3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t117A07FAD93D252BC5E69F05373B4E286F1F8D56_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t15EAEA1713B2DE57B0F7BFED86F3CF5915138314_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t19A08A9BB7D482718CD3474975A6559CA159FA9F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t1AB30C8E5E10864F469D7062D99016352CC3602F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t1B2365362928779807EDAEE12F27010A7A8D39C6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t1D8E4389A2FC4D611519FC7C996BF96F763E5017_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t27076E67A79536591E04C63FD1582DB7D564BA08_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t284D0A27595992EAC03994E6C1A1E82225441B67_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t3913958E2393CE5F44A661B2F0835A6FC4513CBE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t3C170C2BFCCAA591C6DA755A86E3FEDBB4428D90_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t3D709A9DD3DD60B2669C03CAF9A9607B98F0CC44_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t4125FEC2DD5C797C8E273617CF2C3E2ABB98FA45_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t427A3AAB52F4338A54E05C0F9CCE238604B75463_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t437C51FCE7D4E11D08E66AC6884ADD843A29305E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t4A43C94439BC4A524A4463B987E9326B340BB5D7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t4CDBF06B555A45F0D103E46E4114CEEEFA34B691_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t4D3AA2594C46CEF975DCD32BFF9A0FEE380568B3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t5199EDEE1233BFABE45CDA21E9230C8346645716_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t51E55DFCEC17AF21DB946B2527D3669CD8E09542_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t63FAAE13C5F96B474021BB5448158556C8EBB594_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t6B81023D4D427FA4343B3A8E4F4A88A145C6884F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t6CB11000550B779DE6F0A9873A4FA47684E8A996_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t771EB50B64E2F517312250DEC5230DC62C7E36FD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t77285E9313B73F391F0816DD5738EBF05F8774AD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t776DBFE440EDCE827698E6B849C13676E750F733_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t7962FDDC98F8F0028DDCD9B0CADBA4C9C051E1C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t7A7E74A9B38640A94D5CC807EC76F706FB6693C0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t8863334817D651A8E3B8E434C682CA5A76CE9498_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t88B940D5126F22A0E314062C9CB9B8CF14430308_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t8CF38B31F52C4EF713F3D2BAC6B806B321ADC0A1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t92B2C6139E3081F85D498D28EED40F2C004C7B20_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t92D149BA0BB9E6C54B7036167F4A14DFF1B5BE08_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t932E36A723FFA4E592D80F6F8AE785A34669DA75_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t98800A090710915B3E861E237B1A9B6E9F29163F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t993E6168F76ADC356CC943B6992CDD55115F3057_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t9BBCAC312FAACBDD9B08E82065F0C66CB92596DD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t9E34BCADC1F9EF87E3BC9817BF9ABF3086286388_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tA50DCC93E783D93FEFF521E5255E78C184ED8E02_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tA527CAC169A61C3874E9DEC5127904806AF2463C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tA6272A2BBF53770CE719C794145D53E55F0DCC51_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tA70CFBAAF2BDFBA2BEE4003F4C2E6E5B12924D8E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tA9611CADC87D4AD3B0FB66AF118E5B269E6FA658_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tAF32C09EC861FEA50FE0977ED9C0DAD82BA3D637_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tAF93D04C699FAC16A658F8DA78DD252A93E952E8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tB31DAF5F45A869010A4A3D1EA9EA5C56DCFAB947_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tB478A835A6DC624AEA7C01C383390CE10F4D2BF1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tB5D2418D8D5BB1CA27E37232842C681B92ABDB9B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tBE61BBF1961F6628AEE63F058E11FEB22FB6A6ED_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tC4556EDF997707D306A86E36FC8EE3751F9C687C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tC5312D54454D5411CB706A055837C90CBD0FEC12_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tC9D1D64ED53FED0525E8EE017F8CAEBD3D9CF610_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tC9DC1064C14B7AF886718CB747591EC132E1E301_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tCAF5159105AB227514D4319BDE35C44C14FB6C67_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tCB8F678502AD5BFA52EE6F149351DAC3037677AF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tCF4F398AAC858F0B69CF49EA4DB6878F5E85A2FE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tCFCEE592405B35148F5E1AC506FEB4F8CB2F49B6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tD01A9F5ED6BB951B86BE216B2B8DE2C17FFAE551_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tD12F3221DBF89BF11C6CB2855E9BDBD70D6A8AB9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tD28B743D21DCE5F5BE026041A86D0F58543D86C4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tD39051A11B2175464C227775F3F6FD44715C0767_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tD63675C2600BD13840BDC184B61A1282E2FA7667_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tD872DFCCB454D89FE5878BA1B3A03AA4FF2F7A72_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tD87BC92995377D813FAEA7DFF07C8F114451FAAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tD938883EC204367D5A295A7CB2BB5D4D20A90029_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tDC9B80B3DDEFFE6AD670786AA0C0A41F09DFE02D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tDD6B83C4A6E2ECFC616A05B485C0D7E24A750E90_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tDDEAD4B308FD0F5A9299458AF5A6B47CA5D3732F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tDF176CFB33B35A021E5DA20E9485F6A452F2C345_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tE1C508C7DB184AC769DA763897EA5CE208D96BE9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tE28F1C5549DD431F6354E325F28C3FA842F94E57_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tE45BEC1FBF44C71FEAD78DC6323066FD51D7FE55_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tE631CB825D55269F80774B2F55C5E62192891CF8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tE7875C2C92DB47F882FCBBE26BB848F3264D0B63_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tE79E56090404F1ED684677C80F34095664D0010D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tE7DE89222EAD5CBE5FBA94711CB3C71222202DFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tED2F1B32CB312EA2AFE3FF65E3EBD21ADF00A34C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tEE379855D79D043D165FD05C3786F7B0ADB89B38_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tEE3DE27FC7A1D873CE799E7124798B9A979DE913_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tEF11BC50B4B015730C0409DC35EAC7D6E03CEE11_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tF808E86BBEE3D1817CE8A0C2E299320C9DD9D1BA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tF8B877CE67C7AF23C81D6C5C15EB3D2FE44321A0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tFAE875C9728E772ACF44D3BCF5F2B94860FB1CDE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tFAFB5B1636A32CF85A452A078736260BB496C253_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t056E7181DAB40DF2A9D14BB4450C0DCC566784E6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t0968F31AEE953CA42422B45BEE8A04FB272E086F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t0BC506BED5AC85C37292910C8BDAD52A9F6C9986_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t0D870FCF01A0B3EEF980A0B74CCA07765A7552B3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t0D88BB68D7BF644024EA7DFB9E7CDBA1EE7EF69C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t1D54F05297BE0A4ACFBE3D87381C38332A2F7837_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t1E82FEEB6D88844359FA37AD374ACC1AA39DDC4A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t1FE35EF0C4BBDC7AFA697EBBB5B4D64547326DCC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t1FE4698618B38731C4E2D67BCC314778BA13ECC0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t21336B204733D902134B7F0FB71D1D4D85655F8E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t22170DBE4CAFFA15631BB8B0CE681A822A3A160A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t26F6F63A0DA84EBAD15F6BCC374071D9F6C8B42C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t2A9651B5311D10F2FDA41A113A50E18A22D93138_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t2CF9D64DD882AC30B207D3D6C3C4EFC846A40BC9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t2DBAA86C74F086DBA285C7B8ECE41C9F35E419D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t2ED5FBB4104367F73E820E5864485F4EDD912E0F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t2EFEEFDAE61B1D665B02E8D247FA7A8AE1B3D3FF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t31743D3E7DDCFE0001801AD8A7459B2FE016E401_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t340EA86AEF734F68ACF5E9FF2A124F9ADDED52DE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t3C7B620CCBA5E47EED93AC22DB224F015928D9BF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t3C87EC87405703EAD0A26AD3BD3A5B3615EB39E6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t3EE5A9E53DE82B19DAC86C3D30AC7BD98D305238_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t3EF4F45A92179F162ABDFA4C40C59CEAB4DF8C95_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t3F9C9CE2DD4A455F6BF2ACA2CBC6A00343B798EA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t4467783C00F409FFB8ED1FDCE87EF974AB372C4D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t4842934F57735E601CC8B995D6E040EAF256FBF4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t4A60C2EC0E8ECFA6E6872E11BFAD744A805D5FF8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t4B1235C83470DD7C3286754A4CFC840FBFD35741_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t4CD599531AE1FA8CA3CF4057D910B500DBF99E45_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t4F5890FDEA23094C33678E54A33233DBBC85F64B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t57653632E080A4623E41FABED4B1B6707C4D1B44_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t57B5F9E47ED4C8F5611A672DF5C949F2D02972C8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t58133EDD30520660CD4F542594E8D913BB704B55_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t589ED36A1F9FA112DA0EA54F4A46CBAEE886A07A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t58F06EBEDB50273CFFCE1BED6156FF00537E7BF6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t5B94AD2B8545A777AE2428975D92A19545B20228_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t5DF2F494B8936184EE2E2F51E135F530C09B76D0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t5F7A34ACC37197D2AFFFFF3A63EE51E2B44D6607_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t603A0F2238D55FD8325BC0816D55C2970606BB81_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t643EFCAEA913218C33F9E05144DAB3702712A18C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t68753017D74F6BABDA2E69D18EC2FA64AC20B03F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t6DF06C835E4A0B6416DE67676C67EDBE1EBBFD78_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t747EDA19750AE43DE30476D128A90ACDBEAC48AF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t76221365B0738498867CB728129555B7A8617C15_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t763657E22AECC9ED46856683B3045624CF6351E6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t8085A3B2562300C528C41159E557B58E555D6798_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t842043449CFBC0F4C127867ED6917B5566AC5576_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t86251E51D7B41B00F1CC89068612DB2EE6F5FDE0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t8C43EB67A953E2DA4EA0F861438FEF6C6F87360E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t8D8F451E2C46B99D55F5781D36B4F981BE62BD6A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t8F78F44B218E20FDA86CA793AEC9892B18B978F6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t93378D91D2D47434F10E8104D47A5A2363B48D08_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t966D11E60D81CE62911BC8F172BA2326E5F49FE0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t9B160232C1403CEF10F27D1D1BE4368650EE22CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t9BD17C9037D00DFA5EFF1643A78C3E4300A31331_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t9C4AED5DAF5E665602E3D752BDF50BAE7C2C5D0F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tA1D462828AADE4C7A521611177F1A49476CFE773_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tA3EC1D81F5C9B991E5E551D9C0197BBCFA6ECFC8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tA736A0330DE6964C91228A379FAB411414BBBB31_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tA8008B61C32E0E3A90F98593FFE956D148147DC5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tAAC77BA3AEC5902F635E82ED90239D6BCD31F70A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tAE6F8E9F1706B23DD7BC49D738569171CF6E8C3B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tAF26F2C22D87F8F669162B6542990070635D2A30_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tB124603C647DAC5696CA392A20B3EE6C213CCA26_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tB35F19D43C3D74BFE4F16B609D5DAFBCCE477EC1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tB637CE13E86654599FE7556001525095E552A933_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tB643983D6CBD6220B483FEE44F3207125D7D3726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tB67E11F122FE23A54C3DC5758CF7ABCED89CA2B9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tB85C8267AB8EBEC2E4C056F08CC1288F0C2055B7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tBB870D080A3294867CDE63CF2AF4F2D4699C33B2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tBD4C61172F81BE2A292FAE166832A0324BB00B1B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tC36B775425D425FC2AF1D16C0CD70386004FFEA0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tC5D3F83B86462F66072C190B541019F5456F2DB9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tCD175F70DBC690C8EF0231BF4AF1180C50BE25E0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tD28C61E3A0A47F71DB0CDCB3B43C7F13D4F82235_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tDA855C737BB7E9546B48245EC0F04DAA56A1CB03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tDB9FD64DF7D2CA31FFF8279F26075DFCF440D33C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tDCE62FBEE709378CCBDB8A5F059E3D9EB17469CB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tDF2B4816BD9F902CA19B4AFA6CA78C2A61867EA3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tE2F89B2F7AD6F1B1ED5373840C57D1603C193D7D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tE35FCB04208B127B021F241EA079464ACC551B7C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tE53CE7EE59BDF79AA9E78FAB9A62695508883396_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tEA21835A50BEE04419A104B2F4D8CCE015122924_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tEAEC0CEBCFCC9157E40E1A7FFFEB82D8A082B205_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tEC73485627298AE849634B626697EFC64D213D43_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tEE61CDD83F7808585CB68357D75DC2637FBA492F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tF047810C662C3A551DDB01290047E803F32DA440_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tF1646D6248B9372FDA9C49F858144441B68F98FD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tFB31016B91F9F5C6B2CAC98095F7D025353DD033_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tFBF2D2CE91617E529AC8196465432685D724AEBE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tFD61C39BCF897AD7E0777243349B4558DB639EC5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_m7C5BA3DA8EFBFCD37258B6FCA8B0DD3DAC4DC9B3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m1ED249926C5FFFF3711276C020ABE54B928BAAEE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_CommandMessage_m1EB0BBD20B46C4D33A437C5D4CF2D8EA43F18A8B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_EntityStateMessage_mFACC1BD75FFD70F53F10FD27A7EDB7AC60E2DB5D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_Examples_Chat_ChatAuthenticator_AuthRequestMessage_mA629A2F8CCBFF4C76544ED51BD2A35A5947315EF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_Examples_Chat_ChatAuthenticator_AuthResponseMessage_m43271020844960E04376F6C896CFE7D9AC33124D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_CellValue_m960AA8E4580568652E48F8810768B4DEA14B34F8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ClientMatchMessage_m588555F2CD175A2C40F8045755BF3F659CDB65B4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ClientMatchOperation_m967DCEAD7E36E480A8496E822CACE1F60AE9AFB9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchInfoU5BU5D_mE23CB91C4A5FF33675CF8A56B16BA006EC9047E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchInfo_mB709C9D76C0446BF9C8C43CEBFCEC92A0D42551A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchPlayerData_m3268F6B9575F69594F5CEE743E0EA6FDF9ED56E3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_PlayerInfoU5BU5D_mDB000F31EDB9991937FE2C78FE0E3009EF2C1209_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_PlayerInfo_mAAF3F3CF61F837BA88391A854E75733E0F5014F6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ServerMatchMessage_m45838D1053B8C35602D526DA8A918170A84E882B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ServerMatchOperation_m0E0217A1EA28419B9541B868BB7E160A7F9876C8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m2AB11C6E07E94D6C3DB059650CCA9BD6D9838F5C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_mD30EC9CAE30F7761C7D307A8671F27E9D63BAC17_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m208EB5BA02CD99D1236C15A7B3E6C13E0935702B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_m1805A0712B400427B3DA28B6B83D2DB7BA47184C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_mEFA99EDE85679F1A1946C256D3829EC04CBF8A40_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_m9DCF582A7F2ACD58B46CE0B5006DC8DF925AAB07_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_m7236240B1388E3E63F1E45767148C047F2B86681_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_ReadyMessage_m39CE2D271EF006C1BF90E631FE38A0509A2A7174_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_RpcMessage_m4FC20C673517D44D58414A7AA047AF7D75A377C4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_SceneMessage_m5E357A51D411045B49C0B390BBBEEA981F754A43_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_SceneOperation_m74E8C7FCF4974E3652B0B512F68390584B1B3DEF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Read_Mirror_SpawnMessage_mEC8E19BCC23380500B3605B78866DDBEA4C904EE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_mEDEBEED5BBA0B7D7E68879616FC283492746A528_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m71C4764A857C326F63101E4181166D807AB065B0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_CommandMessage_mB652F843580DC0EB2C46997A80B317C449CEC38F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m68475F9DBE473336E6961AAF9477B31B544CEB61_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_Examples_Chat_ChatAuthenticator_AuthRequestMessage_m8266348F8A557605B0C42EC1EF5EA60DBA5E1C53_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_Examples_Chat_ChatAuthenticator_AuthResponseMessage_m12300CEF77BCC2BC22FE9F7B9385CE87D7B02E10_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_CellValue_mE850D02656D8C7B9181D58B92D4CA0583C063A1D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ClientMatchMessage_m9534DBB36A6BF3241779E9E5168D444DDF31E51C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ClientMatchOperation_m08496CD05975A28AB59D4513B9BA36209FD081C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchInfoU5BU5D_m2A631F7A481D201FABBF213396D9D3C548AB54C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchInfo_m154E3D80BB1984768FBF6C11EC8128FFF90EB743_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchPlayerData_m30BD65E22A872300D89020C18828BD180BA41FA7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_PlayerInfoU5BU5D_m258F7D9CB39C3B6D8C6B2BA7E8B1FE9491B38B50_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_PlayerInfo_m03B2F18306AC1FE8C7C933859A265C2B38C03700_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ServerMatchMessage_mF90A563F787035F17D0CD90966372FB8DEFF4A03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ServerMatchOperation_m4ED27F7C3469C55F345EFBE642DC3F4DB0FB9B5C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_mFAEC5102CEF441E12C2E1A9A267D3A70C60C7B5C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_m79990113E28B9ADA181528EFC5D92C21AEC24CAF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_NotReadyMessage_mF6F4E00A0FB4198BA8291861F03B44A5EA9473E2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_m635CCE7CF2563B4550CB476F6AC5EEB18A6D939B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_m2233A20099B4B9AAD2F5232DC7B04C59A1581350_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_m8F8249A46B9C3061ED971ADF650AC9609E486619_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m2875D87F1F21B322D53603292FFE5BF8D604EC47_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_ReadyMessage_mBAE8C7940A984DEEA5331A23E0A01BB3A6B481CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_RpcMessage_m690CA90FADE7D8E3E22236A59E88C3EA5D0F720B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_SceneMessage_m2415AA836382B52EAF54E4CBB6425300C99BEF86_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_SceneOperation_mA2C6453809A524632C4C882EE0133AAE2B596560_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeneratedNetworkCode__Write_Mirror_SpawnMessage_m4D40E1220150E253BDECE7A9915957EDB4AFC499_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadBoolNullable_m49866827FF66A52CA4E36AAED2D3ACC8766F8B38_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadByteNullable_mB622478495C2AE927128F9F196A47DCFEB666E4E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadBytesAndSize_mB707572AAF6CBDE9E6FAC190629882468EAFAD8E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadCharNullable_m728E4E8F336F06A0BDB8BDEE69842C707ED4540A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadChar_mCCA8829AD9CA54D8510AE4C3E3D1CA0F6F6E8966_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadColor32Nullable_m95A7EDB77042A0B8D6D00D2C96E9A530DEA6AF8C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadColor32_m0F0066C51CACC736B893D9F3C1D4324F87641BEF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadColorNullable_mDA6AADFE45C4CE1364429EACA43199CB319C9065_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadColor_mC5D200708B20F2ADC42224245960E2ED7E5DD27A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadDecimalNullable_m18D27D0176D98F043EC804A512EB8B55856229E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadDecimal_m79DE6589996D493A3A95BAD98036B09FF9CB144E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadDoubleNullable_mE52DB83CB818F30F912FD40175B39731A2FBD33B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadDouble_m949A60A21C6EB3B9952A43355903F08B3A7E0EF9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadFloatNullable_m1EB56AA1F1CDB7981728CACF5941EB0B6B4275BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadFloat_mF3D9834531FC09112A506971638FB9682A231D97_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadGameObject_m037E8EDDA39F95DA70EE3226939F677F9E3A2EBD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadGuidNullable_m884FD11E39BB14010073AB443D46779317340927_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadGuid_mCFFAB7379132286F7C9CC70EC291F8B28EA08B0E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadIntNullable_m9F68CD73D47D10DE2E1C6934DE14234E19D02E71_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadLongNullable_m747B938C128B0CAD7E22D0909E1AEE9DFDB54F67_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadLong_m67D408F9D8D9FB04A0101AAE2AB9B01120E34435_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadMatrix4x4Nullable_m508241752BEC24CCF4BD45230613444685553D06_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadMatrix4x4_mBB21ACB1A8610F3813CE4A37DBF1608CA31A0E2C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadNetworkBehaviourSyncVar_mD3DCF91C73BB12C70E487EA4C4C85EAC62FE8A1A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadNetworkBehaviour_m6D724C97DE822B84C3FF75E80DA169D7C44E5E0B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadNetworkIdentity_mFDB6779F9A77F88F9760FD9902EFFDF3331E62AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadPlaneNullable_m252E55444808DDA4A5CEBCBE440E34728ECA5120_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadPlane_m2DA9573A8252F9B24A10E9E1AB448976D9963B96_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadQuaternionNullable_mE4E31E56C486837C0EC9C6047B276C9452D02C9D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadQuaternion_m135F5C523703C700E6A266DA9718E44D160BB567_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadRayNullable_mEE7ECB615AEFA818E73B366F681EA86595CA8F19_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadRay_mBE12F756FAAA9395B88F69C6A43F8576921AB20C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadRectNullable_m58AF30FAB6E523648BA18026AD02B220FBDDBC85_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadRect_mA4B7FDD8840C7E3A299614815C36EFB27232AB3C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadSByteNullable_mEAB105DEC52D7789AEE2A6E110B66A3C2EB8785E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadSByte_m1B3975CC87DD10621C8A369EA7D053AFE57E958B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadShortNullable_m6B7ED0AC2C951C2461A34D3DEB05E6055399B896_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadShort_m5FFC8A9D90AE04D1D0AD681F1D2C32564DBC8677_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadSprite_mA5B19DCF570BA845B63AA79858FCFDF27DEAE040_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadString_m5A56179D2C1CB509EC9C762F97BBD4133A5AD394_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadTexture2D_m5795D8D017B66A5ED4BDE243E306BD2B77A35EDC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadTransform_m56C2AB03C3891F0A72C1FC7153655E7AE4DCD6E8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadUIntNullable_m3D4906C1707F48E5439F4EA0E9DE4B8860AB2E73_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadULongNullable_mE853C2A40E3E7F9FD1BB49D5E16BCB9310B0752E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadULong_mAC6B83521EBA7FFEDFC72A6AAE1BF5D87221A5F5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadUShortNullable_mEDDEE70BF7A15DC1503C4BDF580F54A26C82DCA4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadUShort_mA98395DD1B1DA249096858B171B8BC23D95DF765_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadUri_m9CB721F84C66F0749E586B02C4CE8E472F266C06_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadVector2IntNullable_m698F001AADBF901CE9571E3AA5687DFC1DD65701_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadVector2Int_mC9CEB6A103CD7C5DBCD8A944A57A59C0D1311F25_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadVector2Nullable_mCDCE58B581701AC12499A36355838E45F298C817_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadVector2_m673B821E39E194BA5E2B7E5F444D6CCD76812811_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadVector3IntNullable_mB88012F753982406CA6C49E8440318BFF784AF97_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadVector3Int_m59BAA3EBC52DB1635EA840D23B9D4A011E480E3F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadVector3Nullable_m17D39303F570FAB53014718C07327F9DAAD8DB18_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadVector3_mD35BF8B14DD5F75688AB9C360D138D1BAB432637_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadVector4Nullable_m1303DE93C2EB13F32622A8B868B17610B8C4AD09_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadVector4_m7870D12D4D86684F68719E7F040A33A085C2F1D4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteBoolNullable_mFB1ADF7E798F7991680382003FA5584DA972EBBE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteByteNullable_mE34CDDB5354D7536941F1AB0DB0EFA4E3269C5A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteBytesAndSizeSegment_mD7FCDC44AB0313C3ED0AFBFEC77366B6947672AC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteBytesAndSize_mD5E8FA492EACCF5C68D0E76D84C20689CDAA0F27_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteCharNullable_m3D59A08FC508A0A1469115A766844A3DC3F1E420_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteChar_m79E8B11FA260E5C83FAAB385A039B9B73F4E15E7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteColor32Nullable_m3931F587C14E96A05B25E3446B6F7AE6D81115C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteColor32_m123810E64991275156516FBB8AD2CFA67A7C3B7B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteColorNullable_m0B728B0EF504CC3FB0CE87FAD1505794AB82CDBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteColor_m142E05754268CB4F297199994A61605D0FF1D9A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteDecimalNullable_mACB5EA0A0661A694C8521C3C245AC9EE3ECAE1DA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteDecimal_mFDD008D98CD77D9B4E63EF9AE0421FABAE70F483_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteDoubleNullable_m91EB95539CA35FAA383E01BEFA894A346A218ACA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteDouble_m0475F5FB9E1D69D60501C7158AD3431680BC1BDB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteFloatNullable_m0F2D06A7FA1A84F3F3C54537D6A3ABAB3206585F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteFloat_mA3AEF60E8288F55D5A3365AA0E4730AFFC231050_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteGameObject_m1B7DE5CB70EE416C894BA361CE421473734456AD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteGuidNullable_m19C9499197D2DEEB57A46D81FC9993D6EBFA14D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteGuid_mFF6C6A1BC90A9A7BBC9C179FA6FC25753689D3F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteIntNullable_mD476FA9C8F66E723E823733776EC840B4DCE6FAF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteLongNullable_mE9A8A93B2C853063398E244A3FA8342BB51D0C5C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteLong_m631751934892884B4E8B0FAF18BC616ADBAE1E90_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteMatrix4x4Nullable_mEBA4E2383B008575F3957933958627DEE30465D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteMatrix4x4_m19A3B92281557AC6E231E3B5C663ACB8366CDFE2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteNetworkBehaviour_mDEE6FB11729AF7833D749E1C0573A559113E26A3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteNetworkIdentity_m670598EE39418EC82E5A35DD60EBDA69D7B8A74A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WritePlaneNullable_m1D7DE145477804CB9A5F078D51F2C31408A9CA2D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WritePlane_m5BF0BAF633E94AAE16D6D7E44B78E474E601077F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteQuaternionNullable_m7E68536A12BD33C4E0D063841FCB8B3319CE546A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteQuaternion_mFC2E046965F6BA1B694218D5E60E26807974ACBA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteRayNullable_m4C9EE3A8F4B5A24523EE0A02827A481F300A7C36_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteRay_mE3C68E64E43515730710198FF05734D077BDBEBA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteRectNullable_mD90246AB0237C3D9B3D669CEDFF1548D9DE26364_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteRect_m52D47BD93F73E06FB131C75A78127E3CC9073093_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteSByteNullable_m3330A77E2E4D2AC1B90BF53BC8150063BB6F8B30_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteSByte_m777D700EE0D8256617BE1128DE65C2DEBF674EB3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteShortNullable_mB84CEDD1AF6243DABA5C235B4013AD5A1801BE6B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteShort_m8593C0C47C9EADF1A65AA97BCBA9C15BF3739089_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteSprite_mC849B7B1044D0DC1989BC8F5A77DD93CDB7B0C82_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteTexture2D_mFA5FE217BF0E9D2F1CBF3A50C7A0B9C689B79782_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteTransform_m2F65EBB30598661EE20259C40E58691589593CD4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteUIntNullable_m5C618D0B2F565D4C20CACAE51E3D8A9AAA7EC3A4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteULongNullable_m5CD68A058B65F0F3B8729DEE05D43A94E222055E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteULong_mC0AE4801C58209EF02B73E3B353100B3AB95D28C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteUShortNullable_mE77F289B55D295E545826AAEA6CAAFCD26FA11A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteUShort_m4AEC8147034117F9EB131043089577CD2DB42DB4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteUri_mFB6E40C094D853A44F750835EF778B567546D775_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteVector2IntNullable_mDAC15DF3BA2A0FDA9705D0D35A3C4F486D2DDFA5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteVector2Int_m0099C36CFAF8015034E1CBC4CFCD7623543C758F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteVector2Nullable_mF82E294E5D5AB3D06DEA7404DAD4C9430D89C728_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteVector2_m5C9C94ECCE2643B670009D710BA8D6A2434F8BA5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteVector3IntNullable_m6EDE27130713A9C3A4012DCE58D53E488EE7E36A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteVector3Int_m2A6D52133117098B0C8A65520CBEFF8C4297B47B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteVector3Nullable_mD35B4E68313CFC87EF7B80823D9F0502C4D63E62_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteVector3_mB3C2B2F2D3C9874F883C12813FB20B9D5AABC882_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteVector4Nullable_m225B849A988CD2861387E8368F780E90E3D956CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteVector4_m710FEA287EE2C56C2C7DA468B394D23FE2424023_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t00DA01ACAF536E0AF78C6E8BC015679E9934DB88_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t0676D3BA8C95F6153F7DA06A84ECA06D85436585_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t0CB918AF0F66B31D68CF91A09F89A19D747B6B71_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t16EF44DD5D73A84B2F654B9671D58A442A179C99_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t198B364FF80EB504EC1C04BA6BDB4431FCAC173E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t1EC17F900AFBDD0AA0263D3A62B0630B495FE255_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t23067DF0051C0D5F767FB367AFEEE3C196DC518C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t259EB4559035BAE63FEDCB6F4C5B72D8CA00F6C7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t286A71B8B0F59FB5063D0E327C76169FAF8F45D6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t28D5381047606A72C4A5B7E019C0F77FBCEE1C15_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t2A59AD3448B11193AB518EAC5FFE24BA1ED83727_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t2C531D10760B34519D87218F56AE3866A9C6B924_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t2FDE27056CF1FB6DE71EBDBFE9EA615E76F9B215_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t33986420F424835F9DB93EEB52C71E46AB50C09F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t36FED30A7BD4508A497B3FDF00A93EFCBF2CC6C6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t39F933887B1FE66BE8404A5FAC7840D47C4CE2F2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t3E733A5CBD67C2A9860A6535DC31C53DC3294C05_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t4135AEE9236AC48C3B0D8C260D3A2DC89E212394_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t41E7ACDBED757B1830FB93B6F8CBF75F959CD886_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t429249FC13D808B0B1401B5B6BD0815F37C082B8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t436C9D757F7667ABD9FB2D630506ED1ED29652AA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t497575125F4FF1F1EEB32CB0C8FF12569655666B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t4AF495607AC4510AD0529F12D6A0BB0418C352A7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t4BEAA8B6A674DE41A4F74F8F9F7628DAEB444E28_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t4C51943F9874BC9563E868F14B951239A3817126_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t4EB488AA5A482EE20D6D1D3A5E90B63F672378C7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t4F335D70E11B569B7B2CFBE0E13B13D07B3BA4CF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t5072BFCCD801A3F27F8B16872FF18C70016F2CC0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t526D37680AAEFF5888DD8FAF5DE39A4018267FDB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t59A60ECBC527BDCD550C648C4F309C4E55AE4AA7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t5AAF2D6763D2C4497F18AC3DE6C04568FBA25219_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t5F1A5D57BC2BD2B0AB5EF928D722588060144152_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t637274CA1AF284DD25208101A16558A23B6E67B5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t65AE09028DBFFFB995B18BC7147A9A651D389384_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t6757FC52418E33974D53365FEF9C7406320DED1A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t6B6A1FCF933F958098FE25A100627E7241FEB588_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t6B88F366A149F8A58032F93153EF0A2DFC60E74F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t6FC28304AD44844C4A3BFD56F3139BA2B74B01FF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t7A28D876B4CE12F3E4C96FA0EDF6E7175F65C810_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t7D07B6E13CF9AA9927D4FD2385DB61464F9B5FE4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t80077764BF20B75B1B8B47491E64EF0F64D019DF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t822A5642F255AE3ACC0D29BCD12B45F8C7A6EBAE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t84A9F65298C551CBBA05DF543DBED0468412CCD3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t85795B601B4F9B63136F7F928000E1996B634723_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t88B56AF4E9141686F1DE1F3D155CB3FBA34B913D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t894EA749E730DD156D39D5D3C89DD01B1840FC90_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t8CE8050417CA35F7B7ED704A952421B24A331DFB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t8CFA4E86EAB3C0AE3848A8BC147CB1A393CB3B50_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t8D947140B208E58AB2F0C97B38D53DDA61149A4A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t92A37B251D88CED6738CA5720BFA048525E94646_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t936C97C09526A419E67C5F81F13C53B05B727833_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t942164B32720705E43130F8A5332C5B7AFF4FAE5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t94C4EBCF7B54695CBEF81ACC847D9D01B8D34339_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t9773E186C54234048A015DDEBB899F90C35E96B0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t99038D5B7B0CB7EA48F6A69A3DA42DDF77732757_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t9999666C4A090A6C9AD43987D46A3326989F2E39_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t9C04509E1AAFBF9B8BE4514971DC0FAFCF576DCE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t9C76556BF9E8625EB258DEFF7A1656D33B4B236A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t9F020D67D02A1B6BF8EF51B03B6F298B7042AC26_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_t9F0D38236764D927F7809A9EF55BC5E04C385A80_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tA05950C10C6D79E26FD7C70ACCED03FCC01F5FC0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tA1A9ED8F30B3E4079D52171EBCE69E3D54C0E32A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tA2296DCE521FC65EB6FBA78472797DC918D9FE4E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tA296681D381BFB2393CD36BF8D1737E846F15634_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tA5BF854AFBD00B4F60644779FA6DEB2DC22F7F61_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tA9538DBB44C0A8B65EDF025A5D5FA611D152FA31_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tACF841391431A7BEF83CACCF4CAF12E6601B5A00_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tADD3DCC9F400A548DD461E9BE16C5ACCBCA80B8E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tAF89048D8EB67FD9887BFA7A88596629C237E4CA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tB18271683F45221F345D04D571881186F2B19615_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tB3752A50A44FC8C286392A80358AFF6F3CE44155_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tB498EAF4882CB62E4E9E943CFD2165144F1AD05B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tB503DFFAE8032B3381FEE0779316357D1CE28658_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tBE1B76CC86F1EDCE1B7A3203682971C129964518_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tC8C6D6D608FA323A6019F7C773782247361141D0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tCB389B7DC3DE02BC9C35A7B57D2B65F26E3A7C60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tCF89C1D29B73DD90DA2BB475B19671460984C68D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tDB685B03953454DEB76F09E00F64F95F5B14E21F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tDC04CC49F13C042AA2FC9CEA968B413D1B6F3F38_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tDC213F8037D359AA1C0EC410CE387A872B5F05EB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tDF34563EB909B8D900E05A73EAE294A1BE35E704_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tE473AB6B794D2436C26789A313B67896AB07A48D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tE674AC89FB334BAAB07744B62BEB917F65466F8B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tF19F6E8C291C912786327989D4FD0124DF26DB09_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tF38A27591F97AE59155FCEDBC831BE975EBD3842_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tF46320E57E205382BE23057439BFC8EA1F2457F0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tF4BD915B07DF8D3A5EB55BCAAD572DEE9ECE8FB5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tF4E8B721D496EDF9F9FFF783C41F20BF05DE9F34_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tFBA7DC5EFAB4785CF16735020B26CD15760FF936_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tFD0BF8A2A5368E4951ED7917611F5A25E4D46A7B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Reader_1_tFFD0A7FB0A8C37CF4374C67CB0DF9F2448CA9784_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t02378C060D52C54C236D7DC3C612BD8B0B2153AE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t03D9486B5C228414D227AD0F4B1C32582EF57381_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t080683EBC9B7A30F6121E3179DCB7370B1E8F62A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t0BA0F75C0F12434151A45FAC15BD8A05CCF138C9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t0C51959312818298C3E0C1C9872771D904AD5062_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t110A7C2C82FDE2873D9B37CDBE36F7F5D9B42E86_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t1382F19A71CF32746849E9722B51E1B6EBC080DC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t190700FB2EFCFE964C8FDD64548F48100C8B992B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t1D6C09BFBA988FC86B2ABBD261ECF76022F5691B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t2190E1DFD8994854D9985EB6B653D18BBC814BBD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t21E999198EB9EDBDB5B29C76134D9493E50B5775_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t23D4BACC405E3FA13539AE7E93E94435ED73C740_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t243E336527949636CD7438A1DBBBDA88A829C4CC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t256EBB9B83C8B6F6F428369117FBBEC36C7E390C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t271DE44B9F6C5A28AB5ADF72AEDCAA3628BABB90_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t279D7AD1213DC4E446FDB2E12DD927F5544311B9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t2862DC0A99F1512C1E98B5CEEFEAB76FBB011055_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t28CA88055E5AD2A0F9B106076EF93C52105F504A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t28DF10100EA6DD2804A3081EC57C156DE5808B4F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t3A7BC19F51D39AA83BAC5C1DEC028E0AE21067DA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t3C56C64613D535D9C73CA3D587F334CD7CE30434_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t3D2702EC8F6235254020DB789887C072FDDFECAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t3F4AFD2264190A12B59C26F438E4D9A0D5C6B8A0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t43CDE8B30140776D24809288A7A734939B0C8392_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t44DF034032ED1B1CEEAF5EAF3332EDE362ED947D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t4BDE3F806C4CC59448CAF1EE19A59A5CFEAD58F7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t4C15B820BC518F2CE261CF46CCD5C0763E35D445_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t4C3ABDA4E9C5685EE6B45F6ED3FACEE79F085657_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t4CC6F64E17DBF07E25DEBC8F9A2FC129D5E21F6B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t4D548019E9743B81B59DEF8D2478BEACFDC3CE09_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t4D7D68DB9331B49DDA144EDF2DEE5BCBCAC34F6F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t56257DBAFE2A2FE91E9247E46AC489415891F173_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t5B4A56E0557BF93D873BD64F49D7D7EB2FFAC769_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t5E74ADF66E1728C9F5589CB7743629820364AF71_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t5EA9A1C053345BA806F8721312945B9DF2E1FFA3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t633DC4AA7F39C88128E15553F3C582C26A375D17_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t6BDF566A850D915F8607498B2A74D90E6AD3DFE4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t6C6CC5FCDF55B79BC00FD028E0A44F85A45210CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t72E49BCDFE4B2A53733511DC5BCDB5E02D0EBF54_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t76BD90233C405D75BFDE795DB7AADBF14659D95A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t79E192D8556A9F0A0CFF931EEF19A382A96A85B3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t7B4F1E8858C145418CFBD4E81EEB412568ABB75B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t7D22F5C863050135C73CAB5F962B1CB2C9DD592F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t7F01052FAB46FDA22D39911D07363E1D87E72BBA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t8390A5CF97A52E052D8612D70A087CFAF6A64170_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t83950ACDC982F33C59176AEBFE823CBB074A0BFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t864220A64B0EDB912027564DF90AE901084A093D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t86A60D54965CEA1D0E31A7BF6611C9E4EA2A7B87_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t88385B356F11A0E27B1FA1BB321C08F75AE44C4D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t89600A4E62D36536DC444EAF231664989DD702F3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t918CD45EC571EF06C238F361375BF18469663559_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t93E7560AB93CC6616158EBA6F0ADF0F95A12807A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t95C4BC990ED47CA1945D53AA60D4F38C75743BAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_t978255B2785507C424403C582ECD6DC09003148E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tA5144EF44424ACEA981DF61078CF47C5A0E75FA4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tA63F5424DDE9B2587D7E34744054819D40E1EF86_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tA7C0E92EDDBAEE98892D2BFC9229C66F5D6C0D50_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tAB097320742DBC1A8B8DF7BFB4F762C902C20077_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tACC5A4F72D8EB16086B351E1784F11620EED13F0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tADD2D185EF8CADFAA813221CFBDB1706512C378A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tAFAB4C7529C18C2FED4E4A6D66D55F4EB4CA10CE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tAFDA6C5A95301BCFC4E49DC4B9E31CE5E13B51A3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tB00DD74436318ED1FFBD6AE43C142C83E0E441E7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tB611FFB23F108BACF0D008A5913B87BDA0B78161_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tBAC97F7921836787FC871388114C4389B83F337E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tBCB1D512EE5CFE7E2A2BA73BC72DA709A669FA52_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tC35EE38E41AE63A92291A26CB18FBF2D36A07896_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tC900E33B3D8F5D4C68DB3645A60C917BAC711321_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tCA15166526F2EA72615546B0168BD1A9B7067C0B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tCF7983600918F8F12E621BA83FD47D9F458B363A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tD572915EE39BA9844F8AB175F9C7BE4C23CA9B97_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tD6B98FC179BE9F9519958863FD7D3515B8FFC6A7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tD6CDFC673E86CE3DA4B9A902DFE96553C7C89B49_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tD745BB380F67A854010D62CF4FE8179D1D9A0FFF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tDB03F8AA9D2A369BF9554E72C2BAF71BECCC4D81_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tDC62F016728AC1B9962FAB7BA7DB68E48CECC6A9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tDCFFA2190DEB474AD0651E28A8D50F5AE9BA52E1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tDD7CE7C0539A0A26F84206A36C8A80400C6E622E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tE1FE9E7BA2D0C18CB94133B384971FAAD2EABAB7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tE36053BA0B28C83CF0A86F8398DABFC7F10A4B88_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tE837CDE21BF382361B1ED24E4C34DC6B7B9C65E0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tE871E366A3A0953860589F6AC9B876E03571DD8F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tEB9DBA2CB2FA886294119B2A97206EB710E97D34_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tECEB6A5A5CE5B7325CE0ED4C1A86ED28CC960D40_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tED1C9085F349458B13C36D062822203E85AF457B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tEF909D3F88F3F38B3C89136F9CEE84B8AF352E3F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tF790A3D1EF8C3E9D9DC8802687D0B51121BA2551_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tF86673563FB7B0CBA7838723701DABAE01570F38_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tF902FB09B54D9973B6DE4667D18385E6F351DE14_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Writer_1_tFB29484EAFF97FE2C50FDB48351489533752921F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_2_tDDEAD4B308FD0F5A9299458AF5A6B47CA5D3732F* L_0 = (Action_2_tDDEAD4B308FD0F5A9299458AF5A6B47CA5D3732F*)il2cpp_codegen_object_new(Action_2_tDDEAD4B308FD0F5A9299458AF5A6B47CA5D3732F_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Action_2__ctor_m450CD99F88341CB21DB0FC2C3DD7C89F4D5C0AD0(L_0, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_RuntimeMethod_var), NULL);
		((Writer_1_t6C6CC5FCDF55B79BC00FD028E0A44F85A45210CD_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t6C6CC5FCDF55B79BC00FD028E0A44F85A45210CD_il2cpp_TypeInfo_var))->___write_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t6C6CC5FCDF55B79BC00FD028E0A44F85A45210CD_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t6C6CC5FCDF55B79BC00FD028E0A44F85A45210CD_il2cpp_TypeInfo_var))->___write_0), (void*)L_0);
		Action_2_t104D6887DC3FA703E6D350257E4EE72A3EFCF3A3* L_1 = (Action_2_t104D6887DC3FA703E6D350257E4EE72A3EFCF3A3*)il2cpp_codegen_object_new(Action_2_t104D6887DC3FA703E6D350257E4EE72A3EFCF3A3_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		Action_2__ctor_m23AD0D146D008C29D60BC1BC7B02142C50AB71B0(L_1, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteByteNullable_mE34CDDB5354D7536941F1AB0DB0EFA4E3269C5A2_RuntimeMethod_var), NULL);
		((Writer_1_t44DF034032ED1B1CEEAF5EAF3332EDE362ED947D_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t44DF034032ED1B1CEEAF5EAF3332EDE362ED947D_il2cpp_TypeInfo_var))->___write_0 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t44DF034032ED1B1CEEAF5EAF3332EDE362ED947D_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t44DF034032ED1B1CEEAF5EAF3332EDE362ED947D_il2cpp_TypeInfo_var))->___write_0), (void*)L_1);
		Action_2_tEE3DE27FC7A1D873CE799E7124798B9A979DE913* L_2 = (Action_2_tEE3DE27FC7A1D873CE799E7124798B9A979DE913*)il2cpp_codegen_object_new(Action_2_tEE3DE27FC7A1D873CE799E7124798B9A979DE913_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		Action_2__ctor_m0E3455CF54826F6B843F72903D6B054DD7963DA1(L_2, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteSByte_m777D700EE0D8256617BE1128DE65C2DEBF674EB3_RuntimeMethod_var), NULL);
		((Writer_1_t4C3ABDA4E9C5685EE6B45F6ED3FACEE79F085657_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t4C3ABDA4E9C5685EE6B45F6ED3FACEE79F085657_il2cpp_TypeInfo_var))->___write_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t4C3ABDA4E9C5685EE6B45F6ED3FACEE79F085657_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t4C3ABDA4E9C5685EE6B45F6ED3FACEE79F085657_il2cpp_TypeInfo_var))->___write_0), (void*)L_2);
		Action_2_tEE379855D79D043D165FD05C3786F7B0ADB89B38* L_3 = (Action_2_tEE379855D79D043D165FD05C3786F7B0ADB89B38*)il2cpp_codegen_object_new(Action_2_tEE379855D79D043D165FD05C3786F7B0ADB89B38_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		Action_2__ctor_m8B23EE7AB9CB6E996119CD5572F20B2326E154A1(L_3, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteSByteNullable_m3330A77E2E4D2AC1B90BF53BC8150063BB6F8B30_RuntimeMethod_var), NULL);
		((Writer_1_tACC5A4F72D8EB16086B351E1784F11620EED13F0_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tACC5A4F72D8EB16086B351E1784F11620EED13F0_il2cpp_TypeInfo_var))->___write_0 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tACC5A4F72D8EB16086B351E1784F11620EED13F0_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tACC5A4F72D8EB16086B351E1784F11620EED13F0_il2cpp_TypeInfo_var))->___write_0), (void*)L_3);
		Action_2_t3C170C2BFCCAA591C6DA755A86E3FEDBB4428D90* L_4 = (Action_2_t3C170C2BFCCAA591C6DA755A86E3FEDBB4428D90*)il2cpp_codegen_object_new(Action_2_t3C170C2BFCCAA591C6DA755A86E3FEDBB4428D90_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Action_2__ctor_mE178A62D8B5C362FCD4233DC326FA2E5C6888D1F(L_4, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteChar_m79E8B11FA260E5C83FAAB385A039B9B73F4E15E7_RuntimeMethod_var), NULL);
		((Writer_1_t28DF10100EA6DD2804A3081EC57C156DE5808B4F_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t28DF10100EA6DD2804A3081EC57C156DE5808B4F_il2cpp_TypeInfo_var))->___write_0 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t28DF10100EA6DD2804A3081EC57C156DE5808B4F_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t28DF10100EA6DD2804A3081EC57C156DE5808B4F_il2cpp_TypeInfo_var))->___write_0), (void*)L_4);
		Action_2_t1B2365362928779807EDAEE12F27010A7A8D39C6* L_5 = (Action_2_t1B2365362928779807EDAEE12F27010A7A8D39C6*)il2cpp_codegen_object_new(Action_2_t1B2365362928779807EDAEE12F27010A7A8D39C6_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		Action_2__ctor_m1FA0EF4E6C4ED10AE76DB38281127F9DA725A3CF(L_5, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteCharNullable_m3D59A08FC508A0A1469115A766844A3DC3F1E420_RuntimeMethod_var), NULL);
		((Writer_1_t4C15B820BC518F2CE261CF46CCD5C0763E35D445_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t4C15B820BC518F2CE261CF46CCD5C0763E35D445_il2cpp_TypeInfo_var))->___write_0 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t4C15B820BC518F2CE261CF46CCD5C0763E35D445_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t4C15B820BC518F2CE261CF46CCD5C0763E35D445_il2cpp_TypeInfo_var))->___write_0), (void*)L_5);
		Action_2_t771EB50B64E2F517312250DEC5230DC62C7E36FD* L_6 = (Action_2_t771EB50B64E2F517312250DEC5230DC62C7E36FD*)il2cpp_codegen_object_new(Action_2_t771EB50B64E2F517312250DEC5230DC62C7E36FD_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		Action_2__ctor_m537376D2B2BD93BFE47E52FF9BA6D6E53F3A191B(L_6, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45_RuntimeMethod_var), NULL);
		((Writer_1_t4D548019E9743B81B59DEF8D2478BEACFDC3CE09_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t4D548019E9743B81B59DEF8D2478BEACFDC3CE09_il2cpp_TypeInfo_var))->___write_0 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t4D548019E9743B81B59DEF8D2478BEACFDC3CE09_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t4D548019E9743B81B59DEF8D2478BEACFDC3CE09_il2cpp_TypeInfo_var))->___write_0), (void*)L_6);
		Action_2_tFAFB5B1636A32CF85A452A078736260BB496C253* L_7 = (Action_2_tFAFB5B1636A32CF85A452A078736260BB496C253*)il2cpp_codegen_object_new(Action_2_tFAFB5B1636A32CF85A452A078736260BB496C253_il2cpp_TypeInfo_var);
		NullCheck(L_7);
		Action_2__ctor_mFB41A794E4F6AB08FCCA7F54C3917A44EFEBAFC7(L_7, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteBoolNullable_mFB1ADF7E798F7991680382003FA5584DA972EBBE_RuntimeMethod_var), NULL);
		((Writer_1_t1D6C09BFBA988FC86B2ABBD261ECF76022F5691B_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t1D6C09BFBA988FC86B2ABBD261ECF76022F5691B_il2cpp_TypeInfo_var))->___write_0 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t1D6C09BFBA988FC86B2ABBD261ECF76022F5691B_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t1D6C09BFBA988FC86B2ABBD261ECF76022F5691B_il2cpp_TypeInfo_var))->___write_0), (void*)L_7);
		Action_2_tED2F1B32CB312EA2AFE3FF65E3EBD21ADF00A34C* L_8 = (Action_2_tED2F1B32CB312EA2AFE3FF65E3EBD21ADF00A34C*)il2cpp_codegen_object_new(Action_2_tED2F1B32CB312EA2AFE3FF65E3EBD21ADF00A34C_il2cpp_TypeInfo_var);
		NullCheck(L_8);
		Action_2__ctor_mA7FDA03514437E579E951B398A1F19A4C40E1E68(L_8, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteShort_m8593C0C47C9EADF1A65AA97BCBA9C15BF3739089_RuntimeMethod_var), NULL);
		((Writer_1_tF86673563FB7B0CBA7838723701DABAE01570F38_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tF86673563FB7B0CBA7838723701DABAE01570F38_il2cpp_TypeInfo_var))->___write_0 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tF86673563FB7B0CBA7838723701DABAE01570F38_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tF86673563FB7B0CBA7838723701DABAE01570F38_il2cpp_TypeInfo_var))->___write_0), (void*)L_8);
		Action_2_tE28F1C5549DD431F6354E325F28C3FA842F94E57* L_9 = (Action_2_tE28F1C5549DD431F6354E325F28C3FA842F94E57*)il2cpp_codegen_object_new(Action_2_tE28F1C5549DD431F6354E325F28C3FA842F94E57_il2cpp_TypeInfo_var);
		NullCheck(L_9);
		Action_2__ctor_m0789F991A271A02D8263C3865AAA4FAAEAE1C9B0(L_9, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteShortNullable_mB84CEDD1AF6243DABA5C235B4013AD5A1801BE6B_RuntimeMethod_var), NULL);
		((Writer_1_tC35EE38E41AE63A92291A26CB18FBF2D36A07896_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tC35EE38E41AE63A92291A26CB18FBF2D36A07896_il2cpp_TypeInfo_var))->___write_0 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tC35EE38E41AE63A92291A26CB18FBF2D36A07896_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tC35EE38E41AE63A92291A26CB18FBF2D36A07896_il2cpp_TypeInfo_var))->___write_0), (void*)L_9);
		Action_2_tDC9B80B3DDEFFE6AD670786AA0C0A41F09DFE02D* L_10 = (Action_2_tDC9B80B3DDEFFE6AD670786AA0C0A41F09DFE02D*)il2cpp_codegen_object_new(Action_2_tDC9B80B3DDEFFE6AD670786AA0C0A41F09DFE02D_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		Action_2__ctor_mC6EA866F606C983D483B73525E3B1CFB128B3591(L_10, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteUShort_m4AEC8147034117F9EB131043089577CD2DB42DB4_RuntimeMethod_var), NULL);
		((Writer_1_tD6B98FC179BE9F9519958863FD7D3515B8FFC6A7_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tD6B98FC179BE9F9519958863FD7D3515B8FFC6A7_il2cpp_TypeInfo_var))->___write_0 = L_10;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tD6B98FC179BE9F9519958863FD7D3515B8FFC6A7_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tD6B98FC179BE9F9519958863FD7D3515B8FFC6A7_il2cpp_TypeInfo_var))->___write_0), (void*)L_10);
		Action_2_tDD6B83C4A6E2ECFC616A05B485C0D7E24A750E90* L_11 = (Action_2_tDD6B83C4A6E2ECFC616A05B485C0D7E24A750E90*)il2cpp_codegen_object_new(Action_2_tDD6B83C4A6E2ECFC616A05B485C0D7E24A750E90_il2cpp_TypeInfo_var);
		NullCheck(L_11);
		Action_2__ctor_m1735274EB0760EAE8A299DB6E281C7A877B54973(L_11, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteUShortNullable_mE77F289B55D295E545826AAEA6CAAFCD26FA11A2_RuntimeMethod_var), NULL);
		((Writer_1_tAFAB4C7529C18C2FED4E4A6D66D55F4EB4CA10CE_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tAFAB4C7529C18C2FED4E4A6D66D55F4EB4CA10CE_il2cpp_TypeInfo_var))->___write_0 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tAFAB4C7529C18C2FED4E4A6D66D55F4EB4CA10CE_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tAFAB4C7529C18C2FED4E4A6D66D55F4EB4CA10CE_il2cpp_TypeInfo_var))->___write_0), (void*)L_11);
		Action_2_t4D3AA2594C46CEF975DCD32BFF9A0FEE380568B3* L_12 = (Action_2_t4D3AA2594C46CEF975DCD32BFF9A0FEE380568B3*)il2cpp_codegen_object_new(Action_2_t4D3AA2594C46CEF975DCD32BFF9A0FEE380568B3_il2cpp_TypeInfo_var);
		NullCheck(L_12);
		Action_2__ctor_mC03200ADD4C8D842C9DBD941F1461C6F78466418(L_12, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_RuntimeMethod_var), NULL);
		((Writer_1_tAFDA6C5A95301BCFC4E49DC4B9E31CE5E13B51A3_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tAFDA6C5A95301BCFC4E49DC4B9E31CE5E13B51A3_il2cpp_TypeInfo_var))->___write_0 = L_12;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tAFDA6C5A95301BCFC4E49DC4B9E31CE5E13B51A3_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tAFDA6C5A95301BCFC4E49DC4B9E31CE5E13B51A3_il2cpp_TypeInfo_var))->___write_0), (void*)L_12);
		Action_2_tD938883EC204367D5A295A7CB2BB5D4D20A90029* L_13 = (Action_2_tD938883EC204367D5A295A7CB2BB5D4D20A90029*)il2cpp_codegen_object_new(Action_2_tD938883EC204367D5A295A7CB2BB5D4D20A90029_il2cpp_TypeInfo_var);
		NullCheck(L_13);
		Action_2__ctor_mDDEA3388C6E9EA1735B2AAE13C38B7B08D143D7B(L_13, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteIntNullable_mD476FA9C8F66E723E823733776EC840B4DCE6FAF_RuntimeMethod_var), NULL);
		((Writer_1_tED1C9085F349458B13C36D062822203E85AF457B_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tED1C9085F349458B13C36D062822203E85AF457B_il2cpp_TypeInfo_var))->___write_0 = L_13;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tED1C9085F349458B13C36D062822203E85AF457B_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tED1C9085F349458B13C36D062822203E85AF457B_il2cpp_TypeInfo_var))->___write_0), (void*)L_13);
		Action_2_t0FDD9213EEB281B92425DB7083B3E28D691AA59F* L_14 = (Action_2_t0FDD9213EEB281B92425DB7083B3E28D691AA59F*)il2cpp_codegen_object_new(Action_2_t0FDD9213EEB281B92425DB7083B3E28D691AA59F_il2cpp_TypeInfo_var);
		NullCheck(L_14);
		Action_2__ctor_m626DDD1C02889779E51819503D673C1724B71941(L_14, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_RuntimeMethod_var), NULL);
		((Writer_1_tE1FE9E7BA2D0C18CB94133B384971FAAD2EABAB7_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tE1FE9E7BA2D0C18CB94133B384971FAAD2EABAB7_il2cpp_TypeInfo_var))->___write_0 = L_14;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tE1FE9E7BA2D0C18CB94133B384971FAAD2EABAB7_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tE1FE9E7BA2D0C18CB94133B384971FAAD2EABAB7_il2cpp_TypeInfo_var))->___write_0), (void*)L_14);
		Action_2_tC9DC1064C14B7AF886718CB747591EC132E1E301* L_15 = (Action_2_tC9DC1064C14B7AF886718CB747591EC132E1E301*)il2cpp_codegen_object_new(Action_2_tC9DC1064C14B7AF886718CB747591EC132E1E301_il2cpp_TypeInfo_var);
		NullCheck(L_15);
		Action_2__ctor_m91E102E6C4CFEE5995B6B0CDCFA19811D4555667(L_15, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteUIntNullable_m5C618D0B2F565D4C20CACAE51E3D8A9AAA7EC3A4_RuntimeMethod_var), NULL);
		((Writer_1_tA63F5424DDE9B2587D7E34744054819D40E1EF86_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tA63F5424DDE9B2587D7E34744054819D40E1EF86_il2cpp_TypeInfo_var))->___write_0 = L_15;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tA63F5424DDE9B2587D7E34744054819D40E1EF86_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tA63F5424DDE9B2587D7E34744054819D40E1EF86_il2cpp_TypeInfo_var))->___write_0), (void*)L_15);
		Action_2_tE79E56090404F1ED684677C80F34095664D0010D* L_16 = (Action_2_tE79E56090404F1ED684677C80F34095664D0010D*)il2cpp_codegen_object_new(Action_2_tE79E56090404F1ED684677C80F34095664D0010D_il2cpp_TypeInfo_var);
		NullCheck(L_16);
		Action_2__ctor_m1F3C63E311D92D34187BF9C6A02BED00C2A321F2(L_16, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteLong_m631751934892884B4E8B0FAF18BC616ADBAE1E90_RuntimeMethod_var), NULL);
		((Writer_1_t3D2702EC8F6235254020DB789887C072FDDFECAA_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t3D2702EC8F6235254020DB789887C072FDDFECAA_il2cpp_TypeInfo_var))->___write_0 = L_16;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t3D2702EC8F6235254020DB789887C072FDDFECAA_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t3D2702EC8F6235254020DB789887C072FDDFECAA_il2cpp_TypeInfo_var))->___write_0), (void*)L_16);
		Action_2_tDF176CFB33B35A021E5DA20E9485F6A452F2C345* L_17 = (Action_2_tDF176CFB33B35A021E5DA20E9485F6A452F2C345*)il2cpp_codegen_object_new(Action_2_tDF176CFB33B35A021E5DA20E9485F6A452F2C345_il2cpp_TypeInfo_var);
		NullCheck(L_17);
		Action_2__ctor_mA3CF307C9CE476D251662DDDA235CC085B1130AC(L_17, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteLongNullable_mE9A8A93B2C853063398E244A3FA8342BB51D0C5C_RuntimeMethod_var), NULL);
		((Writer_1_t0BA0F75C0F12434151A45FAC15BD8A05CCF138C9_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t0BA0F75C0F12434151A45FAC15BD8A05CCF138C9_il2cpp_TypeInfo_var))->___write_0 = L_17;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t0BA0F75C0F12434151A45FAC15BD8A05CCF138C9_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t0BA0F75C0F12434151A45FAC15BD8A05CCF138C9_il2cpp_TypeInfo_var))->___write_0), (void*)L_17);
		Action_2_t7962FDDC98F8F0028DDCD9B0CADBA4C9C051E1C5* L_18 = (Action_2_t7962FDDC98F8F0028DDCD9B0CADBA4C9C051E1C5*)il2cpp_codegen_object_new(Action_2_t7962FDDC98F8F0028DDCD9B0CADBA4C9C051E1C5_il2cpp_TypeInfo_var);
		NullCheck(L_18);
		Action_2__ctor_mF5892B15904085FCE623E0895697F33916A3DEB4(L_18, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteULong_mC0AE4801C58209EF02B73E3B353100B3AB95D28C_RuntimeMethod_var), NULL);
		((Writer_1_tE871E366A3A0953860589F6AC9B876E03571DD8F_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tE871E366A3A0953860589F6AC9B876E03571DD8F_il2cpp_TypeInfo_var))->___write_0 = L_18;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tE871E366A3A0953860589F6AC9B876E03571DD8F_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tE871E366A3A0953860589F6AC9B876E03571DD8F_il2cpp_TypeInfo_var))->___write_0), (void*)L_18);
		Action_2_t9E34BCADC1F9EF87E3BC9817BF9ABF3086286388* L_19 = (Action_2_t9E34BCADC1F9EF87E3BC9817BF9ABF3086286388*)il2cpp_codegen_object_new(Action_2_t9E34BCADC1F9EF87E3BC9817BF9ABF3086286388_il2cpp_TypeInfo_var);
		NullCheck(L_19);
		Action_2__ctor_m8B9741EFE4CF735550B34447D80680135080B161(L_19, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteULongNullable_m5CD68A058B65F0F3B8729DEE05D43A94E222055E_RuntimeMethod_var), NULL);
		((Writer_1_t243E336527949636CD7438A1DBBBDA88A829C4CC_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t243E336527949636CD7438A1DBBBDA88A829C4CC_il2cpp_TypeInfo_var))->___write_0 = L_19;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t243E336527949636CD7438A1DBBBDA88A829C4CC_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t243E336527949636CD7438A1DBBBDA88A829C4CC_il2cpp_TypeInfo_var))->___write_0), (void*)L_19);
		Action_2_tE45BEC1FBF44C71FEAD78DC6323066FD51D7FE55* L_20 = (Action_2_tE45BEC1FBF44C71FEAD78DC6323066FD51D7FE55*)il2cpp_codegen_object_new(Action_2_tE45BEC1FBF44C71FEAD78DC6323066FD51D7FE55_il2cpp_TypeInfo_var);
		NullCheck(L_20);
		Action_2__ctor_m0209EF27E442D70A0261C1B2591ACBDDB9F146CC(L_20, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteFloat_mA3AEF60E8288F55D5A3365AA0E4730AFFC231050_RuntimeMethod_var), NULL);
		((Writer_1_t3C56C64613D535D9C73CA3D587F334CD7CE30434_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t3C56C64613D535D9C73CA3D587F334CD7CE30434_il2cpp_TypeInfo_var))->___write_0 = L_20;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t3C56C64613D535D9C73CA3D587F334CD7CE30434_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t3C56C64613D535D9C73CA3D587F334CD7CE30434_il2cpp_TypeInfo_var))->___write_0), (void*)L_20);
		Action_2_tD01A9F5ED6BB951B86BE216B2B8DE2C17FFAE551* L_21 = (Action_2_tD01A9F5ED6BB951B86BE216B2B8DE2C17FFAE551*)il2cpp_codegen_object_new(Action_2_tD01A9F5ED6BB951B86BE216B2B8DE2C17FFAE551_il2cpp_TypeInfo_var);
		NullCheck(L_21);
		Action_2__ctor_m7E7EDC9B9F57AFF56485B0903C188A3E4092A7F7(L_21, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteFloatNullable_m0F2D06A7FA1A84F3F3C54537D6A3ABAB3206585F_RuntimeMethod_var), NULL);
		((Writer_1_tAB097320742DBC1A8B8DF7BFB4F762C902C20077_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tAB097320742DBC1A8B8DF7BFB4F762C902C20077_il2cpp_TypeInfo_var))->___write_0 = L_21;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tAB097320742DBC1A8B8DF7BFB4F762C902C20077_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tAB097320742DBC1A8B8DF7BFB4F762C902C20077_il2cpp_TypeInfo_var))->___write_0), (void*)L_21);
		Action_2_tA70CFBAAF2BDFBA2BEE4003F4C2E6E5B12924D8E* L_22 = (Action_2_tA70CFBAAF2BDFBA2BEE4003F4C2E6E5B12924D8E*)il2cpp_codegen_object_new(Action_2_tA70CFBAAF2BDFBA2BEE4003F4C2E6E5B12924D8E_il2cpp_TypeInfo_var);
		NullCheck(L_22);
		Action_2__ctor_mC7F3CEF4AD030B10080D5A785DA693E1B7B456A5(L_22, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteDouble_m0475F5FB9E1D69D60501C7158AD3431680BC1BDB_RuntimeMethod_var), NULL);
		((Writer_1_t4BDE3F806C4CC59448CAF1EE19A59A5CFEAD58F7_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t4BDE3F806C4CC59448CAF1EE19A59A5CFEAD58F7_il2cpp_TypeInfo_var))->___write_0 = L_22;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t4BDE3F806C4CC59448CAF1EE19A59A5CFEAD58F7_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t4BDE3F806C4CC59448CAF1EE19A59A5CFEAD58F7_il2cpp_TypeInfo_var))->___write_0), (void*)L_22);
		Action_2_tD872DFCCB454D89FE5878BA1B3A03AA4FF2F7A72* L_23 = (Action_2_tD872DFCCB454D89FE5878BA1B3A03AA4FF2F7A72*)il2cpp_codegen_object_new(Action_2_tD872DFCCB454D89FE5878BA1B3A03AA4FF2F7A72_il2cpp_TypeInfo_var);
		NullCheck(L_23);
		Action_2__ctor_m10E6D1F2202B9181AA7954A3F49D5DC32D977528(L_23, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteDoubleNullable_m91EB95539CA35FAA383E01BEFA894A346A218ACA_RuntimeMethod_var), NULL);
		((Writer_1_t4CC6F64E17DBF07E25DEBC8F9A2FC129D5E21F6B_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t4CC6F64E17DBF07E25DEBC8F9A2FC129D5E21F6B_il2cpp_TypeInfo_var))->___write_0 = L_23;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t4CC6F64E17DBF07E25DEBC8F9A2FC129D5E21F6B_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t4CC6F64E17DBF07E25DEBC8F9A2FC129D5E21F6B_il2cpp_TypeInfo_var))->___write_0), (void*)L_23);
		Action_2_tC4556EDF997707D306A86E36FC8EE3751F9C687C* L_24 = (Action_2_tC4556EDF997707D306A86E36FC8EE3751F9C687C*)il2cpp_codegen_object_new(Action_2_tC4556EDF997707D306A86E36FC8EE3751F9C687C_il2cpp_TypeInfo_var);
		NullCheck(L_24);
		Action_2__ctor_m2FE11FE651F5AE8150006365EBA478BD731C9B0E(L_24, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteDecimal_mFDD008D98CD77D9B4E63EF9AE0421FABAE70F483_RuntimeMethod_var), NULL);
		((Writer_1_t8390A5CF97A52E052D8612D70A087CFAF6A64170_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t8390A5CF97A52E052D8612D70A087CFAF6A64170_il2cpp_TypeInfo_var))->___write_0 = L_24;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t8390A5CF97A52E052D8612D70A087CFAF6A64170_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t8390A5CF97A52E052D8612D70A087CFAF6A64170_il2cpp_TypeInfo_var))->___write_0), (void*)L_24);
		Action_2_t3913958E2393CE5F44A661B2F0835A6FC4513CBE* L_25 = (Action_2_t3913958E2393CE5F44A661B2F0835A6FC4513CBE*)il2cpp_codegen_object_new(Action_2_t3913958E2393CE5F44A661B2F0835A6FC4513CBE_il2cpp_TypeInfo_var);
		NullCheck(L_25);
		Action_2__ctor_m7528A1A23D14871E7C355BAC759391CCF83EA771(L_25, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteDecimalNullable_mACB5EA0A0661A694C8521C3C245AC9EE3ECAE1DA_RuntimeMethod_var), NULL);
		((Writer_1_tCF7983600918F8F12E621BA83FD47D9F458B363A_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tCF7983600918F8F12E621BA83FD47D9F458B363A_il2cpp_TypeInfo_var))->___write_0 = L_25;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tCF7983600918F8F12E621BA83FD47D9F458B363A_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tCF7983600918F8F12E621BA83FD47D9F458B363A_il2cpp_TypeInfo_var))->___write_0), (void*)L_25);
		Action_2_t63FAAE13C5F96B474021BB5448158556C8EBB594* L_26 = (Action_2_t63FAAE13C5F96B474021BB5448158556C8EBB594*)il2cpp_codegen_object_new(Action_2_t63FAAE13C5F96B474021BB5448158556C8EBB594_il2cpp_TypeInfo_var);
		NullCheck(L_26);
		Action_2__ctor_m96D1FE2D89915F6DCFE7C522C131EC85218DF44A(L_26, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98_RuntimeMethod_var), NULL);
		((Writer_1_tC900E33B3D8F5D4C68DB3645A60C917BAC711321_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tC900E33B3D8F5D4C68DB3645A60C917BAC711321_il2cpp_TypeInfo_var))->___write_0 = L_26;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tC900E33B3D8F5D4C68DB3645A60C917BAC711321_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tC900E33B3D8F5D4C68DB3645A60C917BAC711321_il2cpp_TypeInfo_var))->___write_0), (void*)L_26);
		Action_2_tE7DE89222EAD5CBE5FBA94711CB3C71222202DFC* L_27 = (Action_2_tE7DE89222EAD5CBE5FBA94711CB3C71222202DFC*)il2cpp_codegen_object_new(Action_2_tE7DE89222EAD5CBE5FBA94711CB3C71222202DFC_il2cpp_TypeInfo_var);
		NullCheck(L_27);
		Action_2__ctor_m145A15B63C274B6AC19BC4C615E51AAADD02AB2B(L_27, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteBytesAndSizeSegment_mD7FCDC44AB0313C3ED0AFBFEC77366B6947672AC_RuntimeMethod_var), NULL);
		((Writer_1_t02378C060D52C54C236D7DC3C612BD8B0B2153AE_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t02378C060D52C54C236D7DC3C612BD8B0B2153AE_il2cpp_TypeInfo_var))->___write_0 = L_27;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t02378C060D52C54C236D7DC3C612BD8B0B2153AE_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t02378C060D52C54C236D7DC3C612BD8B0B2153AE_il2cpp_TypeInfo_var))->___write_0), (void*)L_27);
		Action_2_t6CB11000550B779DE6F0A9873A4FA47684E8A996* L_28 = (Action_2_t6CB11000550B779DE6F0A9873A4FA47684E8A996*)il2cpp_codegen_object_new(Action_2_t6CB11000550B779DE6F0A9873A4FA47684E8A996_il2cpp_TypeInfo_var);
		NullCheck(L_28);
		Action_2__ctor_m8E435DEA2338A3D48843DAAA8D21C85A4D1D39B0(L_28, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteBytesAndSize_mD5E8FA492EACCF5C68D0E76D84C20689CDAA0F27_RuntimeMethod_var), NULL);
		((Writer_1_tA7C0E92EDDBAEE98892D2BFC9229C66F5D6C0D50_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tA7C0E92EDDBAEE98892D2BFC9229C66F5D6C0D50_il2cpp_TypeInfo_var))->___write_0 = L_28;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tA7C0E92EDDBAEE98892D2BFC9229C66F5D6C0D50_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tA7C0E92EDDBAEE98892D2BFC9229C66F5D6C0D50_il2cpp_TypeInfo_var))->___write_0), (void*)L_28);
		Action_2_tCAF5159105AB227514D4319BDE35C44C14FB6C67* L_29 = (Action_2_tCAF5159105AB227514D4319BDE35C44C14FB6C67*)il2cpp_codegen_object_new(Action_2_tCAF5159105AB227514D4319BDE35C44C14FB6C67_il2cpp_TypeInfo_var);
		NullCheck(L_29);
		Action_2__ctor_m51724E50D62540A2CF078C8B78E9209761EF2D06(L_29, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteVector2_m5C9C94ECCE2643B670009D710BA8D6A2434F8BA5_RuntimeMethod_var), NULL);
		((Writer_1_tEB9DBA2CB2FA886294119B2A97206EB710E97D34_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tEB9DBA2CB2FA886294119B2A97206EB710E97D34_il2cpp_TypeInfo_var))->___write_0 = L_29;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tEB9DBA2CB2FA886294119B2A97206EB710E97D34_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tEB9DBA2CB2FA886294119B2A97206EB710E97D34_il2cpp_TypeInfo_var))->___write_0), (void*)L_29);
		Action_2_t06CF1E09D2E968AE317C1B4066FE59281D0D7F35* L_30 = (Action_2_t06CF1E09D2E968AE317C1B4066FE59281D0D7F35*)il2cpp_codegen_object_new(Action_2_t06CF1E09D2E968AE317C1B4066FE59281D0D7F35_il2cpp_TypeInfo_var);
		NullCheck(L_30);
		Action_2__ctor_m5D26AEB049FB2E3AFFE418B1A76F0CE17ECE0D09(L_30, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteVector2Nullable_mF82E294E5D5AB3D06DEA7404DAD4C9430D89C728_RuntimeMethod_var), NULL);
		((Writer_1_tF902FB09B54D9973B6DE4667D18385E6F351DE14_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tF902FB09B54D9973B6DE4667D18385E6F351DE14_il2cpp_TypeInfo_var))->___write_0 = L_30;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tF902FB09B54D9973B6DE4667D18385E6F351DE14_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tF902FB09B54D9973B6DE4667D18385E6F351DE14_il2cpp_TypeInfo_var))->___write_0), (void*)L_30);
		Action_2_tD63675C2600BD13840BDC184B61A1282E2FA7667* L_31 = (Action_2_tD63675C2600BD13840BDC184B61A1282E2FA7667*)il2cpp_codegen_object_new(Action_2_tD63675C2600BD13840BDC184B61A1282E2FA7667_il2cpp_TypeInfo_var);
		NullCheck(L_31);
		Action_2__ctor_m2DBDE75E3894CE657FF776915BA6C959E49A01D0(L_31, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteVector3_mB3C2B2F2D3C9874F883C12813FB20B9D5AABC882_RuntimeMethod_var), NULL);
		((Writer_1_t7F01052FAB46FDA22D39911D07363E1D87E72BBA_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t7F01052FAB46FDA22D39911D07363E1D87E72BBA_il2cpp_TypeInfo_var))->___write_0 = L_31;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t7F01052FAB46FDA22D39911D07363E1D87E72BBA_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t7F01052FAB46FDA22D39911D07363E1D87E72BBA_il2cpp_TypeInfo_var))->___write_0), (void*)L_31);
		Action_2_t3D709A9DD3DD60B2669C03CAF9A9607B98F0CC44* L_32 = (Action_2_t3D709A9DD3DD60B2669C03CAF9A9607B98F0CC44*)il2cpp_codegen_object_new(Action_2_t3D709A9DD3DD60B2669C03CAF9A9607B98F0CC44_il2cpp_TypeInfo_var);
		NullCheck(L_32);
		Action_2__ctor_mE65B7ABA1299F1C5EB05CD59D377D5C576209304(L_32, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteVector3Nullable_mD35B4E68313CFC87EF7B80823D9F0502C4D63E62_RuntimeMethod_var), NULL);
		((Writer_1_tF790A3D1EF8C3E9D9DC8802687D0B51121BA2551_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tF790A3D1EF8C3E9D9DC8802687D0B51121BA2551_il2cpp_TypeInfo_var))->___write_0 = L_32;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tF790A3D1EF8C3E9D9DC8802687D0B51121BA2551_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tF790A3D1EF8C3E9D9DC8802687D0B51121BA2551_il2cpp_TypeInfo_var))->___write_0), (void*)L_32);
		Action_2_t0708A1448BB2410AD3F0DF78AF427D6730760C7F* L_33 = (Action_2_t0708A1448BB2410AD3F0DF78AF427D6730760C7F*)il2cpp_codegen_object_new(Action_2_t0708A1448BB2410AD3F0DF78AF427D6730760C7F_il2cpp_TypeInfo_var);
		NullCheck(L_33);
		Action_2__ctor_m4E3B75D5192791920CF89272FB90E620B075BD16(L_33, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteVector4_m710FEA287EE2C56C2C7DA468B394D23FE2424023_RuntimeMethod_var), NULL);
		((Writer_1_tBAC97F7921836787FC871388114C4389B83F337E_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tBAC97F7921836787FC871388114C4389B83F337E_il2cpp_TypeInfo_var))->___write_0 = L_33;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tBAC97F7921836787FC871388114C4389B83F337E_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tBAC97F7921836787FC871388114C4389B83F337E_il2cpp_TypeInfo_var))->___write_0), (void*)L_33);
		Action_2_t437C51FCE7D4E11D08E66AC6884ADD843A29305E* L_34 = (Action_2_t437C51FCE7D4E11D08E66AC6884ADD843A29305E*)il2cpp_codegen_object_new(Action_2_t437C51FCE7D4E11D08E66AC6884ADD843A29305E_il2cpp_TypeInfo_var);
		NullCheck(L_34);
		Action_2__ctor_mFE7DECE762FED2E02629F28B9966DE881014D291(L_34, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteVector4Nullable_m225B849A988CD2861387E8368F780E90E3D956CD_RuntimeMethod_var), NULL);
		((Writer_1_t271DE44B9F6C5A28AB5ADF72AEDCAA3628BABB90_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t271DE44B9F6C5A28AB5ADF72AEDCAA3628BABB90_il2cpp_TypeInfo_var))->___write_0 = L_34;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t271DE44B9F6C5A28AB5ADF72AEDCAA3628BABB90_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t271DE44B9F6C5A28AB5ADF72AEDCAA3628BABB90_il2cpp_TypeInfo_var))->___write_0), (void*)L_34);
		Action_2_tE7875C2C92DB47F882FCBBE26BB848F3264D0B63* L_35 = (Action_2_tE7875C2C92DB47F882FCBBE26BB848F3264D0B63*)il2cpp_codegen_object_new(Action_2_tE7875C2C92DB47F882FCBBE26BB848F3264D0B63_il2cpp_TypeInfo_var);
		NullCheck(L_35);
		Action_2__ctor_m4D7A6253073C76DC89FE5C83A73CA70F505973B6(L_35, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteVector2Int_m0099C36CFAF8015034E1CBC4CFCD7623543C758F_RuntimeMethod_var), NULL);
		((Writer_1_t72E49BCDFE4B2A53733511DC5BCDB5E02D0EBF54_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t72E49BCDFE4B2A53733511DC5BCDB5E02D0EBF54_il2cpp_TypeInfo_var))->___write_0 = L_35;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t72E49BCDFE4B2A53733511DC5BCDB5E02D0EBF54_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t72E49BCDFE4B2A53733511DC5BCDB5E02D0EBF54_il2cpp_TypeInfo_var))->___write_0), (void*)L_35);
		Action_2_tF8B877CE67C7AF23C81D6C5C15EB3D2FE44321A0* L_36 = (Action_2_tF8B877CE67C7AF23C81D6C5C15EB3D2FE44321A0*)il2cpp_codegen_object_new(Action_2_tF8B877CE67C7AF23C81D6C5C15EB3D2FE44321A0_il2cpp_TypeInfo_var);
		NullCheck(L_36);
		Action_2__ctor_m60D5109A818B5495DE26C493F3BCBE7239FF2236(L_36, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteVector2IntNullable_mDAC15DF3BA2A0FDA9705D0D35A3C4F486D2DDFA5_RuntimeMethod_var), NULL);
		((Writer_1_tD572915EE39BA9844F8AB175F9C7BE4C23CA9B97_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tD572915EE39BA9844F8AB175F9C7BE4C23CA9B97_il2cpp_TypeInfo_var))->___write_0 = L_36;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tD572915EE39BA9844F8AB175F9C7BE4C23CA9B97_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tD572915EE39BA9844F8AB175F9C7BE4C23CA9B97_il2cpp_TypeInfo_var))->___write_0), (void*)L_36);
		Action_2_tA9611CADC87D4AD3B0FB66AF118E5B269E6FA658* L_37 = (Action_2_tA9611CADC87D4AD3B0FB66AF118E5B269E6FA658*)il2cpp_codegen_object_new(Action_2_tA9611CADC87D4AD3B0FB66AF118E5B269E6FA658_il2cpp_TypeInfo_var);
		NullCheck(L_37);
		Action_2__ctor_m1F945F52AF5C891A1C89B8C3E7C50B54664E0EEF(L_37, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteVector3Int_m2A6D52133117098B0C8A65520CBEFF8C4297B47B_RuntimeMethod_var), NULL);
		((Writer_1_t2862DC0A99F1512C1E98B5CEEFEAB76FBB011055_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t2862DC0A99F1512C1E98B5CEEFEAB76FBB011055_il2cpp_TypeInfo_var))->___write_0 = L_37;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t2862DC0A99F1512C1E98B5CEEFEAB76FBB011055_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t2862DC0A99F1512C1E98B5CEEFEAB76FBB011055_il2cpp_TypeInfo_var))->___write_0), (void*)L_37);
		Action_2_tCF4F398AAC858F0B69CF49EA4DB6878F5E85A2FE* L_38 = (Action_2_tCF4F398AAC858F0B69CF49EA4DB6878F5E85A2FE*)il2cpp_codegen_object_new(Action_2_tCF4F398AAC858F0B69CF49EA4DB6878F5E85A2FE_il2cpp_TypeInfo_var);
		NullCheck(L_38);
		Action_2__ctor_m3B8F1FBBC6BBD141F9E7AB0ED6696F97D28E0874(L_38, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteVector3IntNullable_m6EDE27130713A9C3A4012DCE58D53E488EE7E36A_RuntimeMethod_var), NULL);
		((Writer_1_tECEB6A5A5CE5B7325CE0ED4C1A86ED28CC960D40_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tECEB6A5A5CE5B7325CE0ED4C1A86ED28CC960D40_il2cpp_TypeInfo_var))->___write_0 = L_38;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tECEB6A5A5CE5B7325CE0ED4C1A86ED28CC960D40_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tECEB6A5A5CE5B7325CE0ED4C1A86ED28CC960D40_il2cpp_TypeInfo_var))->___write_0), (void*)L_38);
		Action_2_t993E6168F76ADC356CC943B6992CDD55115F3057* L_39 = (Action_2_t993E6168F76ADC356CC943B6992CDD55115F3057*)il2cpp_codegen_object_new(Action_2_t993E6168F76ADC356CC943B6992CDD55115F3057_il2cpp_TypeInfo_var);
		NullCheck(L_39);
		Action_2__ctor_mDA326A2B24806A313B762DAB862967D52B3C4263(L_39, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteColor_m142E05754268CB4F297199994A61605D0FF1D9A2_RuntimeMethod_var), NULL);
		((Writer_1_tFB29484EAFF97FE2C50FDB48351489533752921F_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tFB29484EAFF97FE2C50FDB48351489533752921F_il2cpp_TypeInfo_var))->___write_0 = L_39;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tFB29484EAFF97FE2C50FDB48351489533752921F_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tFB29484EAFF97FE2C50FDB48351489533752921F_il2cpp_TypeInfo_var))->___write_0), (void*)L_39);
		Action_2_t1AB30C8E5E10864F469D7062D99016352CC3602F* L_40 = (Action_2_t1AB30C8E5E10864F469D7062D99016352CC3602F*)il2cpp_codegen_object_new(Action_2_t1AB30C8E5E10864F469D7062D99016352CC3602F_il2cpp_TypeInfo_var);
		NullCheck(L_40);
		Action_2__ctor_m95A3ECC89DD934CE7F7704EE287FAD33456470B0(L_40, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteColorNullable_m0B728B0EF504CC3FB0CE87FAD1505794AB82CDBC_RuntimeMethod_var), NULL);
		((Writer_1_t86A60D54965CEA1D0E31A7BF6611C9E4EA2A7B87_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t86A60D54965CEA1D0E31A7BF6611C9E4EA2A7B87_il2cpp_TypeInfo_var))->___write_0 = L_40;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t86A60D54965CEA1D0E31A7BF6611C9E4EA2A7B87_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t86A60D54965CEA1D0E31A7BF6611C9E4EA2A7B87_il2cpp_TypeInfo_var))->___write_0), (void*)L_40);
		Action_2_t27076E67A79536591E04C63FD1582DB7D564BA08* L_41 = (Action_2_t27076E67A79536591E04C63FD1582DB7D564BA08*)il2cpp_codegen_object_new(Action_2_t27076E67A79536591E04C63FD1582DB7D564BA08_il2cpp_TypeInfo_var);
		NullCheck(L_41);
		Action_2__ctor_m02D1798F3FF6629BE5763B034D7DBCC3A170BBCE(L_41, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteColor32_m123810E64991275156516FBB8AD2CFA67A7C3B7B_RuntimeMethod_var), NULL);
		((Writer_1_t110A7C2C82FDE2873D9B37CDBE36F7F5D9B42E86_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t110A7C2C82FDE2873D9B37CDBE36F7F5D9B42E86_il2cpp_TypeInfo_var))->___write_0 = L_41;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t110A7C2C82FDE2873D9B37CDBE36F7F5D9B42E86_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t110A7C2C82FDE2873D9B37CDBE36F7F5D9B42E86_il2cpp_TypeInfo_var))->___write_0), (void*)L_41);
		Action_2_t284D0A27595992EAC03994E6C1A1E82225441B67* L_42 = (Action_2_t284D0A27595992EAC03994E6C1A1E82225441B67*)il2cpp_codegen_object_new(Action_2_t284D0A27595992EAC03994E6C1A1E82225441B67_il2cpp_TypeInfo_var);
		NullCheck(L_42);
		Action_2__ctor_m761EA55F9D98E81F418148D5EBBDA59F8E2A8014(L_42, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteColor32Nullable_m3931F587C14E96A05B25E3446B6F7AE6D81115C0_RuntimeMethod_var), NULL);
		((Writer_1_t633DC4AA7F39C88128E15553F3C582C26A375D17_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t633DC4AA7F39C88128E15553F3C582C26A375D17_il2cpp_TypeInfo_var))->___write_0 = L_42;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t633DC4AA7F39C88128E15553F3C582C26A375D17_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t633DC4AA7F39C88128E15553F3C582C26A375D17_il2cpp_TypeInfo_var))->___write_0), (void*)L_42);
		Action_2_tD28B743D21DCE5F5BE026041A86D0F58543D86C4* L_43 = (Action_2_tD28B743D21DCE5F5BE026041A86D0F58543D86C4*)il2cpp_codegen_object_new(Action_2_tD28B743D21DCE5F5BE026041A86D0F58543D86C4_il2cpp_TypeInfo_var);
		NullCheck(L_43);
		Action_2__ctor_m49389BFEA959AD5452A6075D222159D8B2CEF0CD(L_43, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteQuaternion_mFC2E046965F6BA1B694218D5E60E26807974ACBA_RuntimeMethod_var), NULL);
		((Writer_1_t5E74ADF66E1728C9F5589CB7743629820364AF71_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t5E74ADF66E1728C9F5589CB7743629820364AF71_il2cpp_TypeInfo_var))->___write_0 = L_43;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t5E74ADF66E1728C9F5589CB7743629820364AF71_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t5E74ADF66E1728C9F5589CB7743629820364AF71_il2cpp_TypeInfo_var))->___write_0), (void*)L_43);
		Action_2_tFAE875C9728E772ACF44D3BCF5F2B94860FB1CDE* L_44 = (Action_2_tFAE875C9728E772ACF44D3BCF5F2B94860FB1CDE*)il2cpp_codegen_object_new(Action_2_tFAE875C9728E772ACF44D3BCF5F2B94860FB1CDE_il2cpp_TypeInfo_var);
		NullCheck(L_44);
		Action_2__ctor_m0B572B0211EAE72327BDCDCACE96224357E558AF(L_44, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteQuaternionNullable_m7E68536A12BD33C4E0D063841FCB8B3319CE546A_RuntimeMethod_var), NULL);
		((Writer_1_t23D4BACC405E3FA13539AE7E93E94435ED73C740_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t23D4BACC405E3FA13539AE7E93E94435ED73C740_il2cpp_TypeInfo_var))->___write_0 = L_44;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t23D4BACC405E3FA13539AE7E93E94435ED73C740_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t23D4BACC405E3FA13539AE7E93E94435ED73C740_il2cpp_TypeInfo_var))->___write_0), (void*)L_44);
		Action_2_t776DBFE440EDCE827698E6B849C13676E750F733* L_45 = (Action_2_t776DBFE440EDCE827698E6B849C13676E750F733*)il2cpp_codegen_object_new(Action_2_t776DBFE440EDCE827698E6B849C13676E750F733_il2cpp_TypeInfo_var);
		NullCheck(L_45);
		Action_2__ctor_mC5F661551C6AA2C25B713BF1DF016D4636BC3239(L_45, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteRect_m52D47BD93F73E06FB131C75A78127E3CC9073093_RuntimeMethod_var), NULL);
		((Writer_1_tBCB1D512EE5CFE7E2A2BA73BC72DA709A669FA52_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tBCB1D512EE5CFE7E2A2BA73BC72DA709A669FA52_il2cpp_TypeInfo_var))->___write_0 = L_45;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tBCB1D512EE5CFE7E2A2BA73BC72DA709A669FA52_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tBCB1D512EE5CFE7E2A2BA73BC72DA709A669FA52_il2cpp_TypeInfo_var))->___write_0), (void*)L_45);
		Action_2_tD39051A11B2175464C227775F3F6FD44715C0767* L_46 = (Action_2_tD39051A11B2175464C227775F3F6FD44715C0767*)il2cpp_codegen_object_new(Action_2_tD39051A11B2175464C227775F3F6FD44715C0767_il2cpp_TypeInfo_var);
		NullCheck(L_46);
		Action_2__ctor_mFBD125BE0060AF3338538D4175299B3F7EF6AA24(L_46, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteRectNullable_mD90246AB0237C3D9B3D669CEDFF1548D9DE26364_RuntimeMethod_var), NULL);
		((Writer_1_t918CD45EC571EF06C238F361375BF18469663559_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t918CD45EC571EF06C238F361375BF18469663559_il2cpp_TypeInfo_var))->___write_0 = L_46;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t918CD45EC571EF06C238F361375BF18469663559_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t918CD45EC571EF06C238F361375BF18469663559_il2cpp_TypeInfo_var))->___write_0), (void*)L_46);
		Action_2_tF808E86BBEE3D1817CE8A0C2E299320C9DD9D1BA* L_47 = (Action_2_tF808E86BBEE3D1817CE8A0C2E299320C9DD9D1BA*)il2cpp_codegen_object_new(Action_2_tF808E86BBEE3D1817CE8A0C2E299320C9DD9D1BA_il2cpp_TypeInfo_var);
		NullCheck(L_47);
		Action_2__ctor_m3E58F4F05B8EF5DFABBC848F3A02B5706F268172(L_47, NULL, (intptr_t)((void*)NetworkWriterExtensions_WritePlane_m5BF0BAF633E94AAE16D6D7E44B78E474E601077F_RuntimeMethod_var), NULL);
		((Writer_1_t978255B2785507C424403C582ECD6DC09003148E_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t978255B2785507C424403C582ECD6DC09003148E_il2cpp_TypeInfo_var))->___write_0 = L_47;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t978255B2785507C424403C582ECD6DC09003148E_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t978255B2785507C424403C582ECD6DC09003148E_il2cpp_TypeInfo_var))->___write_0), (void*)L_47);
		Action_2_t9BBCAC312FAACBDD9B08E82065F0C66CB92596DD* L_48 = (Action_2_t9BBCAC312FAACBDD9B08E82065F0C66CB92596DD*)il2cpp_codegen_object_new(Action_2_t9BBCAC312FAACBDD9B08E82065F0C66CB92596DD_il2cpp_TypeInfo_var);
		NullCheck(L_48);
		Action_2__ctor_m062962270F6BC7BB56727545E83C74101673DC36(L_48, NULL, (intptr_t)((void*)NetworkWriterExtensions_WritePlaneNullable_m1D7DE145477804CB9A5F078D51F2C31408A9CA2D_RuntimeMethod_var), NULL);
		((Writer_1_tD745BB380F67A854010D62CF4FE8179D1D9A0FFF_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tD745BB380F67A854010D62CF4FE8179D1D9A0FFF_il2cpp_TypeInfo_var))->___write_0 = L_48;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tD745BB380F67A854010D62CF4FE8179D1D9A0FFF_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tD745BB380F67A854010D62CF4FE8179D1D9A0FFF_il2cpp_TypeInfo_var))->___write_0), (void*)L_48);
		Action_2_t0726BDF29C00FDD4DA0F278B3AFC4E47F69A4CED* L_49 = (Action_2_t0726BDF29C00FDD4DA0F278B3AFC4E47F69A4CED*)il2cpp_codegen_object_new(Action_2_t0726BDF29C00FDD4DA0F278B3AFC4E47F69A4CED_il2cpp_TypeInfo_var);
		NullCheck(L_49);
		Action_2__ctor_m02CD61DADA5CFB5AF7B6BE7A4308480F4C2D1B35(L_49, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteRay_mE3C68E64E43515730710198FF05734D077BDBEBA_RuntimeMethod_var), NULL);
		((Writer_1_t83950ACDC982F33C59176AEBFE823CBB074A0BFC_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t83950ACDC982F33C59176AEBFE823CBB074A0BFC_il2cpp_TypeInfo_var))->___write_0 = L_49;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t83950ACDC982F33C59176AEBFE823CBB074A0BFC_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t83950ACDC982F33C59176AEBFE823CBB074A0BFC_il2cpp_TypeInfo_var))->___write_0), (void*)L_49);
		Action_2_tBE61BBF1961F6628AEE63F058E11FEB22FB6A6ED* L_50 = (Action_2_tBE61BBF1961F6628AEE63F058E11FEB22FB6A6ED*)il2cpp_codegen_object_new(Action_2_tBE61BBF1961F6628AEE63F058E11FEB22FB6A6ED_il2cpp_TypeInfo_var);
		NullCheck(L_50);
		Action_2__ctor_m87D4F54DD955944BF8C781C605896FF9C98B5738(L_50, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteRayNullable_m4C9EE3A8F4B5A24523EE0A02827A481F300A7C36_RuntimeMethod_var), NULL);
		((Writer_1_t3A7BC19F51D39AA83BAC5C1DEC028E0AE21067DA_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t3A7BC19F51D39AA83BAC5C1DEC028E0AE21067DA_il2cpp_TypeInfo_var))->___write_0 = L_50;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t3A7BC19F51D39AA83BAC5C1DEC028E0AE21067DA_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t3A7BC19F51D39AA83BAC5C1DEC028E0AE21067DA_il2cpp_TypeInfo_var))->___write_0), (void*)L_50);
		Action_2_tD12F3221DBF89BF11C6CB2855E9BDBD70D6A8AB9* L_51 = (Action_2_tD12F3221DBF89BF11C6CB2855E9BDBD70D6A8AB9*)il2cpp_codegen_object_new(Action_2_tD12F3221DBF89BF11C6CB2855E9BDBD70D6A8AB9_il2cpp_TypeInfo_var);
		NullCheck(L_51);
		Action_2__ctor_m7FFD1C135C73A31984A52AF176F0593816B0844F(L_51, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteMatrix4x4_m19A3B92281557AC6E231E3B5C663ACB8366CDFE2_RuntimeMethod_var), NULL);
		((Writer_1_t0C51959312818298C3E0C1C9872771D904AD5062_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t0C51959312818298C3E0C1C9872771D904AD5062_il2cpp_TypeInfo_var))->___write_0 = L_51;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t0C51959312818298C3E0C1C9872771D904AD5062_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t0C51959312818298C3E0C1C9872771D904AD5062_il2cpp_TypeInfo_var))->___write_0), (void*)L_51);
		Action_2_tB31DAF5F45A869010A4A3D1EA9EA5C56DCFAB947* L_52 = (Action_2_tB31DAF5F45A869010A4A3D1EA9EA5C56DCFAB947*)il2cpp_codegen_object_new(Action_2_tB31DAF5F45A869010A4A3D1EA9EA5C56DCFAB947_il2cpp_TypeInfo_var);
		NullCheck(L_52);
		Action_2__ctor_m3B7D9DE1EBC4E44C33D0FA00791767AC00309665(L_52, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteMatrix4x4Nullable_mEBA4E2383B008575F3957933958627DEE30465D7_RuntimeMethod_var), NULL);
		((Writer_1_t7D22F5C863050135C73CAB5F962B1CB2C9DD592F_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t7D22F5C863050135C73CAB5F962B1CB2C9DD592F_il2cpp_TypeInfo_var))->___write_0 = L_52;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t7D22F5C863050135C73CAB5F962B1CB2C9DD592F_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t7D22F5C863050135C73CAB5F962B1CB2C9DD592F_il2cpp_TypeInfo_var))->___write_0), (void*)L_52);
		Action_2_tCB8F678502AD5BFA52EE6F149351DAC3037677AF* L_53 = (Action_2_tCB8F678502AD5BFA52EE6F149351DAC3037677AF*)il2cpp_codegen_object_new(Action_2_tCB8F678502AD5BFA52EE6F149351DAC3037677AF_il2cpp_TypeInfo_var);
		NullCheck(L_53);
		Action_2__ctor_m968C81385D6BB7EEFD019E41D96199BB6E3A367D(L_53, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteGuid_mFF6C6A1BC90A9A7BBC9C179FA6FC25753689D3F9_RuntimeMethod_var), NULL);
		((Writer_1_t5EA9A1C053345BA806F8721312945B9DF2E1FFA3_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t5EA9A1C053345BA806F8721312945B9DF2E1FFA3_il2cpp_TypeInfo_var))->___write_0 = L_53;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t5EA9A1C053345BA806F8721312945B9DF2E1FFA3_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t5EA9A1C053345BA806F8721312945B9DF2E1FFA3_il2cpp_TypeInfo_var))->___write_0), (void*)L_53);
		Action_2_t00EE65E013F88779EEEE98835E86BDF400A3BF36* L_54 = (Action_2_t00EE65E013F88779EEEE98835E86BDF400A3BF36*)il2cpp_codegen_object_new(Action_2_t00EE65E013F88779EEEE98835E86BDF400A3BF36_il2cpp_TypeInfo_var);
		NullCheck(L_54);
		Action_2__ctor_mC9D631DEF0104B3FF747C0CDF9186A29A55FE010(L_54, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteGuidNullable_m19C9499197D2DEEB57A46D81FC9993D6EBFA14D9_RuntimeMethod_var), NULL);
		((Writer_1_tCA15166526F2EA72615546B0168BD1A9B7067C0B_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tCA15166526F2EA72615546B0168BD1A9B7067C0B_il2cpp_TypeInfo_var))->___write_0 = L_54;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tCA15166526F2EA72615546B0168BD1A9B7067C0B_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tCA15166526F2EA72615546B0168BD1A9B7067C0B_il2cpp_TypeInfo_var))->___write_0), (void*)L_54);
		Action_2_t51E55DFCEC17AF21DB946B2527D3669CD8E09542* L_55 = (Action_2_t51E55DFCEC17AF21DB946B2527D3669CD8E09542*)il2cpp_codegen_object_new(Action_2_t51E55DFCEC17AF21DB946B2527D3669CD8E09542_il2cpp_TypeInfo_var);
		NullCheck(L_55);
		Action_2__ctor_m76B26FEADA52D5D5EDCCCA0D1CE4445461158472(L_55, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteNetworkIdentity_m670598EE39418EC82E5A35DD60EBDA69D7B8A74A_RuntimeMethod_var), NULL);
		((Writer_1_tA5144EF44424ACEA981DF61078CF47C5A0E75FA4_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tA5144EF44424ACEA981DF61078CF47C5A0E75FA4_il2cpp_TypeInfo_var))->___write_0 = L_55;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tA5144EF44424ACEA981DF61078CF47C5A0E75FA4_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tA5144EF44424ACEA981DF61078CF47C5A0E75FA4_il2cpp_TypeInfo_var))->___write_0), (void*)L_55);
		Action_2_tC5312D54454D5411CB706A055837C90CBD0FEC12* L_56 = (Action_2_tC5312D54454D5411CB706A055837C90CBD0FEC12*)il2cpp_codegen_object_new(Action_2_tC5312D54454D5411CB706A055837C90CBD0FEC12_il2cpp_TypeInfo_var);
		NullCheck(L_56);
		Action_2__ctor_m018A30909323728A244BB88F8FCE714ED2B35B92(L_56, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteNetworkBehaviour_mDEE6FB11729AF7833D749E1C0573A559113E26A3_RuntimeMethod_var), NULL);
		((Writer_1_t79E192D8556A9F0A0CFF931EEF19A382A96A85B3_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t79E192D8556A9F0A0CFF931EEF19A382A96A85B3_il2cpp_TypeInfo_var))->___write_0 = L_56;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t79E192D8556A9F0A0CFF931EEF19A382A96A85B3_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t79E192D8556A9F0A0CFF931EEF19A382A96A85B3_il2cpp_TypeInfo_var))->___write_0), (void*)L_56);
		Action_2_tD87BC92995377D813FAEA7DFF07C8F114451FAAA* L_57 = (Action_2_tD87BC92995377D813FAEA7DFF07C8F114451FAAA*)il2cpp_codegen_object_new(Action_2_tD87BC92995377D813FAEA7DFF07C8F114451FAAA_il2cpp_TypeInfo_var);
		NullCheck(L_57);
		Action_2__ctor_m3B708985FBC8A1C1BFDF36AD6EF0594211A84245(L_57, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteTransform_m2F65EBB30598661EE20259C40E58691589593CD4_RuntimeMethod_var), NULL);
		((Writer_1_t28CA88055E5AD2A0F9B106076EF93C52105F504A_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t28CA88055E5AD2A0F9B106076EF93C52105F504A_il2cpp_TypeInfo_var))->___write_0 = L_57;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t28CA88055E5AD2A0F9B106076EF93C52105F504A_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t28CA88055E5AD2A0F9B106076EF93C52105F504A_il2cpp_TypeInfo_var))->___write_0), (void*)L_57);
		Action_2_t7A7E74A9B38640A94D5CC807EC76F706FB6693C0* L_58 = (Action_2_t7A7E74A9B38640A94D5CC807EC76F706FB6693C0*)il2cpp_codegen_object_new(Action_2_t7A7E74A9B38640A94D5CC807EC76F706FB6693C0_il2cpp_TypeInfo_var);
		NullCheck(L_58);
		Action_2__ctor_mD03C913E03D4340B7C63BEC0771E1AC6B6A2A0BE(L_58, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteGameObject_m1B7DE5CB70EE416C894BA361CE421473734456AD_RuntimeMethod_var), NULL);
		((Writer_1_t89600A4E62D36536DC444EAF231664989DD702F3_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t89600A4E62D36536DC444EAF231664989DD702F3_il2cpp_TypeInfo_var))->___write_0 = L_58;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t89600A4E62D36536DC444EAF231664989DD702F3_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t89600A4E62D36536DC444EAF231664989DD702F3_il2cpp_TypeInfo_var))->___write_0), (void*)L_58);
		Action_2_t92B2C6139E3081F85D498D28EED40F2C004C7B20* L_59 = (Action_2_t92B2C6139E3081F85D498D28EED40F2C004C7B20*)il2cpp_codegen_object_new(Action_2_t92B2C6139E3081F85D498D28EED40F2C004C7B20_il2cpp_TypeInfo_var);
		NullCheck(L_59);
		Action_2__ctor_mBFE16439C5F13D2FA3150BF43F7B28F2A91B755B(L_59, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteUri_mFB6E40C094D853A44F750835EF778B567546D775_RuntimeMethod_var), NULL);
		((Writer_1_t7B4F1E8858C145418CFBD4E81EEB412568ABB75B_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t7B4F1E8858C145418CFBD4E81EEB412568ABB75B_il2cpp_TypeInfo_var))->___write_0 = L_59;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t7B4F1E8858C145418CFBD4E81EEB412568ABB75B_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t7B4F1E8858C145418CFBD4E81EEB412568ABB75B_il2cpp_TypeInfo_var))->___write_0), (void*)L_59);
		Action_2_tA50DCC93E783D93FEFF521E5255E78C184ED8E02* L_60 = (Action_2_tA50DCC93E783D93FEFF521E5255E78C184ED8E02*)il2cpp_codegen_object_new(Action_2_tA50DCC93E783D93FEFF521E5255E78C184ED8E02_il2cpp_TypeInfo_var);
		NullCheck(L_60);
		Action_2__ctor_mBF279502827A8972D70378B6D4888F0506FD21DE(L_60, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteTexture2D_mFA5FE217BF0E9D2F1CBF3A50C7A0B9C689B79782_RuntimeMethod_var), NULL);
		((Writer_1_t2190E1DFD8994854D9985EB6B653D18BBC814BBD_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t2190E1DFD8994854D9985EB6B653D18BBC814BBD_il2cpp_TypeInfo_var))->___write_0 = L_60;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t2190E1DFD8994854D9985EB6B653D18BBC814BBD_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t2190E1DFD8994854D9985EB6B653D18BBC814BBD_il2cpp_TypeInfo_var))->___write_0), (void*)L_60);
		Action_2_tA527CAC169A61C3874E9DEC5127904806AF2463C* L_61 = (Action_2_tA527CAC169A61C3874E9DEC5127904806AF2463C*)il2cpp_codegen_object_new(Action_2_tA527CAC169A61C3874E9DEC5127904806AF2463C_il2cpp_TypeInfo_var);
		NullCheck(L_61);
		Action_2__ctor_m19E6335D2500A4A6E09FD4371235F0B6100BF35C(L_61, NULL, (intptr_t)((void*)NetworkWriterExtensions_WriteSprite_mC849B7B1044D0DC1989BC8F5A77DD93CDB7B0C82_RuntimeMethod_var), NULL);
		((Writer_1_tEF909D3F88F3F38B3C89136F9CEE84B8AF352E3F_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tEF909D3F88F3F38B3C89136F9CEE84B8AF352E3F_il2cpp_TypeInfo_var))->___write_0 = L_61;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tEF909D3F88F3F38B3C89136F9CEE84B8AF352E3F_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tEF909D3F88F3F38B3C89136F9CEE84B8AF352E3F_il2cpp_TypeInfo_var))->___write_0), (void*)L_61);
		Action_2_t4CDBF06B555A45F0D103E46E4114CEEEFA34B691* L_62 = (Action_2_t4CDBF06B555A45F0D103E46E4114CEEEFA34B691*)il2cpp_codegen_object_new(Action_2_t4CDBF06B555A45F0D103E46E4114CEEEFA34B691_il2cpp_TypeInfo_var);
		NullCheck(L_62);
		Action_2__ctor_mC23A039B775A0DFE21EBC500F50E285F39530E53(L_62, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_ReadyMessage_mBAE8C7940A984DEEA5331A23E0A01BB3A6B481CD_RuntimeMethod_var), NULL);
		((Writer_1_t95C4BC990ED47CA1945D53AA60D4F38C75743BAB_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t95C4BC990ED47CA1945D53AA60D4F38C75743BAB_il2cpp_TypeInfo_var))->___write_0 = L_62;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t95C4BC990ED47CA1945D53AA60D4F38C75743BAB_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t95C4BC990ED47CA1945D53AA60D4F38C75743BAB_il2cpp_TypeInfo_var))->___write_0), (void*)L_62);
		Action_2_tE1C508C7DB184AC769DA763897EA5CE208D96BE9* L_63 = (Action_2_tE1C508C7DB184AC769DA763897EA5CE208D96BE9*)il2cpp_codegen_object_new(Action_2_tE1C508C7DB184AC769DA763897EA5CE208D96BE9_il2cpp_TypeInfo_var);
		NullCheck(L_63);
		Action_2__ctor_mF286C1D11E18BCB8031E6551BDCB4184178A9713(L_63, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_NotReadyMessage_mF6F4E00A0FB4198BA8291861F03B44A5EA9473E2_RuntimeMethod_var), NULL);
		((Writer_1_tDD7CE7C0539A0A26F84206A36C8A80400C6E622E_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tDD7CE7C0539A0A26F84206A36C8A80400C6E622E_il2cpp_TypeInfo_var))->___write_0 = L_63;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tDD7CE7C0539A0A26F84206A36C8A80400C6E622E_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tDD7CE7C0539A0A26F84206A36C8A80400C6E622E_il2cpp_TypeInfo_var))->___write_0), (void*)L_63);
		Action_2_t98800A090710915B3E861E237B1A9B6E9F29163F* L_64 = (Action_2_t98800A090710915B3E861E237B1A9B6E9F29163F*)il2cpp_codegen_object_new(Action_2_t98800A090710915B3E861E237B1A9B6E9F29163F_il2cpp_TypeInfo_var);
		NullCheck(L_64);
		Action_2__ctor_mA2007694D53E8ED7315A5A24D888F3BA8C89AF64(L_64, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_mEDEBEED5BBA0B7D7E68879616FC283492746A528_RuntimeMethod_var), NULL);
		((Writer_1_tE36053BA0B28C83CF0A86F8398DABFC7F10A4B88_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tE36053BA0B28C83CF0A86F8398DABFC7F10A4B88_il2cpp_TypeInfo_var))->___write_0 = L_64;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tE36053BA0B28C83CF0A86F8398DABFC7F10A4B88_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tE36053BA0B28C83CF0A86F8398DABFC7F10A4B88_il2cpp_TypeInfo_var))->___write_0), (void*)L_64);
		Action_2_t4A43C94439BC4A524A4463B987E9326B340BB5D7* L_65 = (Action_2_t4A43C94439BC4A524A4463B987E9326B340BB5D7*)il2cpp_codegen_object_new(Action_2_t4A43C94439BC4A524A4463B987E9326B340BB5D7_il2cpp_TypeInfo_var);
		NullCheck(L_65);
		Action_2__ctor_m87022428E316DF22BCAB52DC647624C6B8ED38E1(L_65, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_SceneMessage_m2415AA836382B52EAF54E4CBB6425300C99BEF86_RuntimeMethod_var), NULL);
		((Writer_1_t4D7D68DB9331B49DDA144EDF2DEE5BCBCAC34F6F_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t4D7D68DB9331B49DDA144EDF2DEE5BCBCAC34F6F_il2cpp_TypeInfo_var))->___write_0 = L_65;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t4D7D68DB9331B49DDA144EDF2DEE5BCBCAC34F6F_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t4D7D68DB9331B49DDA144EDF2DEE5BCBCAC34F6F_il2cpp_TypeInfo_var))->___write_0), (void*)L_65);
		Action_2_t102F2BE697BDA0818FA414975B57C948D9C2BBDF* L_66 = (Action_2_t102F2BE697BDA0818FA414975B57C948D9C2BBDF*)il2cpp_codegen_object_new(Action_2_t102F2BE697BDA0818FA414975B57C948D9C2BBDF_il2cpp_TypeInfo_var);
		NullCheck(L_66);
		Action_2__ctor_mA38AE26C37DA086615388A426BB2E181EC97EC23(L_66, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_SceneOperation_mA2C6453809A524632C4C882EE0133AAE2B596560_RuntimeMethod_var), NULL);
		((Writer_1_t56257DBAFE2A2FE91E9247E46AC489415891F173_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t56257DBAFE2A2FE91E9247E46AC489415891F173_il2cpp_TypeInfo_var))->___write_0 = L_66;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t56257DBAFE2A2FE91E9247E46AC489415891F173_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t56257DBAFE2A2FE91E9247E46AC489415891F173_il2cpp_TypeInfo_var))->___write_0), (void*)L_66);
		Action_2_t8863334817D651A8E3B8E434C682CA5A76CE9498* L_67 = (Action_2_t8863334817D651A8E3B8E434C682CA5A76CE9498*)il2cpp_codegen_object_new(Action_2_t8863334817D651A8E3B8E434C682CA5A76CE9498_il2cpp_TypeInfo_var);
		NullCheck(L_67);
		Action_2__ctor_m233601E9FE7EA83CD29B2774BB3384D00AD2CA9A(L_67, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_CommandMessage_mB652F843580DC0EB2C46997A80B317C449CEC38F_RuntimeMethod_var), NULL);
		((Writer_1_tD6CDFC673E86CE3DA4B9A902DFE96553C7C89B49_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tD6CDFC673E86CE3DA4B9A902DFE96553C7C89B49_il2cpp_TypeInfo_var))->___write_0 = L_67;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tD6CDFC673E86CE3DA4B9A902DFE96553C7C89B49_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tD6CDFC673E86CE3DA4B9A902DFE96553C7C89B49_il2cpp_TypeInfo_var))->___write_0), (void*)L_67);
		Action_2_tAF32C09EC861FEA50FE0977ED9C0DAD82BA3D637* L_68 = (Action_2_tAF32C09EC861FEA50FE0977ED9C0DAD82BA3D637*)il2cpp_codegen_object_new(Action_2_tAF32C09EC861FEA50FE0977ED9C0DAD82BA3D637_il2cpp_TypeInfo_var);
		NullCheck(L_68);
		Action_2__ctor_mEB7542DAF4E595B6B775A313F4653E33F8728247(L_68, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_RpcMessage_m690CA90FADE7D8E3E22236A59E88C3EA5D0F720B_RuntimeMethod_var), NULL);
		((Writer_1_tDCFFA2190DEB474AD0651E28A8D50F5AE9BA52E1_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tDCFFA2190DEB474AD0651E28A8D50F5AE9BA52E1_il2cpp_TypeInfo_var))->___write_0 = L_68;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tDCFFA2190DEB474AD0651E28A8D50F5AE9BA52E1_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tDCFFA2190DEB474AD0651E28A8D50F5AE9BA52E1_il2cpp_TypeInfo_var))->___write_0), (void*)L_68);
		Action_2_t8CF38B31F52C4EF713F3D2BAC6B806B321ADC0A1* L_69 = (Action_2_t8CF38B31F52C4EF713F3D2BAC6B806B321ADC0A1*)il2cpp_codegen_object_new(Action_2_t8CF38B31F52C4EF713F3D2BAC6B806B321ADC0A1_il2cpp_TypeInfo_var);
		NullCheck(L_69);
		Action_2__ctor_m5F1F50CC15F7E0541F768F03B29697D7B1D1C821(L_69, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_SpawnMessage_m4D40E1220150E253BDECE7A9915957EDB4AFC499_RuntimeMethod_var), NULL);
		((Writer_1_t1382F19A71CF32746849E9722B51E1B6EBC080DC_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t1382F19A71CF32746849E9722B51E1B6EBC080DC_il2cpp_TypeInfo_var))->___write_0 = L_69;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t1382F19A71CF32746849E9722B51E1B6EBC080DC_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t1382F19A71CF32746849E9722B51E1B6EBC080DC_il2cpp_TypeInfo_var))->___write_0), (void*)L_69);
		Action_2_tA6272A2BBF53770CE719C794145D53E55F0DCC51* L_70 = (Action_2_tA6272A2BBF53770CE719C794145D53E55F0DCC51*)il2cpp_codegen_object_new(Action_2_tA6272A2BBF53770CE719C794145D53E55F0DCC51_il2cpp_TypeInfo_var);
		NullCheck(L_70);
		Action_2__ctor_mF0E4B938B893D67DAD1A1A97D8BB9128DB588838(L_70, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m71C4764A857C326F63101E4181166D807AB065B0_RuntimeMethod_var), NULL);
		((Writer_1_t03D9486B5C228414D227AD0F4B1C32582EF57381_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t03D9486B5C228414D227AD0F4B1C32582EF57381_il2cpp_TypeInfo_var))->___write_0 = L_70;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t03D9486B5C228414D227AD0F4B1C32582EF57381_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t03D9486B5C228414D227AD0F4B1C32582EF57381_il2cpp_TypeInfo_var))->___write_0), (void*)L_70);
		Action_2_t932E36A723FFA4E592D80F6F8AE785A34669DA75* L_71 = (Action_2_t932E36A723FFA4E592D80F6F8AE785A34669DA75*)il2cpp_codegen_object_new(Action_2_t932E36A723FFA4E592D80F6F8AE785A34669DA75_il2cpp_TypeInfo_var);
		NullCheck(L_71);
		Action_2__ctor_m1F9ADDDB8C090B5DBA2E23D2B86EABBFC1F1780B(L_71, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m2875D87F1F21B322D53603292FFE5BF8D604EC47_RuntimeMethod_var), NULL);
		((Writer_1_t21E999198EB9EDBDB5B29C76134D9493E50B5775_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t21E999198EB9EDBDB5B29C76134D9493E50B5775_il2cpp_TypeInfo_var))->___write_0 = L_71;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t21E999198EB9EDBDB5B29C76134D9493E50B5775_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t21E999198EB9EDBDB5B29C76134D9493E50B5775_il2cpp_TypeInfo_var))->___write_0), (void*)L_71);
		Action_2_t88B940D5126F22A0E314062C9CB9B8CF14430308* L_72 = (Action_2_t88B940D5126F22A0E314062C9CB9B8CF14430308*)il2cpp_codegen_object_new(Action_2_t88B940D5126F22A0E314062C9CB9B8CF14430308_il2cpp_TypeInfo_var);
		NullCheck(L_72);
		Action_2__ctor_mC06148B8AB62C775FB10920B5579CA5A71EA16E0(L_72, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_m8F8249A46B9C3061ED971ADF650AC9609E486619_RuntimeMethod_var), NULL);
		((Writer_1_tDC62F016728AC1B9962FAB7BA7DB68E48CECC6A9_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tDC62F016728AC1B9962FAB7BA7DB68E48CECC6A9_il2cpp_TypeInfo_var))->___write_0 = L_72;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tDC62F016728AC1B9962FAB7BA7DB68E48CECC6A9_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tDC62F016728AC1B9962FAB7BA7DB68E48CECC6A9_il2cpp_TypeInfo_var))->___write_0), (void*)L_72);
		Action_2_t4125FEC2DD5C797C8E273617CF2C3E2ABB98FA45* L_73 = (Action_2_t4125FEC2DD5C797C8E273617CF2C3E2ABB98FA45*)il2cpp_codegen_object_new(Action_2_t4125FEC2DD5C797C8E273617CF2C3E2ABB98FA45_il2cpp_TypeInfo_var);
		NullCheck(L_73);
		Action_2__ctor_mC7C0FB7B89D872EF7E8D1A8DA8D0C39B1E664180(L_73, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_m635CCE7CF2563B4550CB476F6AC5EEB18A6D939B_RuntimeMethod_var), NULL);
		((Writer_1_t864220A64B0EDB912027564DF90AE901084A093D_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t864220A64B0EDB912027564DF90AE901084A093D_il2cpp_TypeInfo_var))->___write_0 = L_73;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t864220A64B0EDB912027564DF90AE901084A093D_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t864220A64B0EDB912027564DF90AE901084A093D_il2cpp_TypeInfo_var))->___write_0), (void*)L_73);
		Action_2_tB5D2418D8D5BB1CA27E37232842C681B92ABDB9B* L_74 = (Action_2_tB5D2418D8D5BB1CA27E37232842C681B92ABDB9B*)il2cpp_codegen_object_new(Action_2_tB5D2418D8D5BB1CA27E37232842C681B92ABDB9B_il2cpp_TypeInfo_var);
		NullCheck(L_74);
		Action_2__ctor_m17850095E89A0ED92C274F9DD97E792B5249EA42(L_74, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_m2233A20099B4B9AAD2F5232DC7B04C59A1581350_RuntimeMethod_var), NULL);
		((Writer_1_t190700FB2EFCFE964C8FDD64548F48100C8B992B_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t190700FB2EFCFE964C8FDD64548F48100C8B992B_il2cpp_TypeInfo_var))->___write_0 = L_74;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t190700FB2EFCFE964C8FDD64548F48100C8B992B_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t190700FB2EFCFE964C8FDD64548F48100C8B992B_il2cpp_TypeInfo_var))->___write_0), (void*)L_74);
		Action_2_t92D149BA0BB9E6C54B7036167F4A14DFF1B5BE08* L_75 = (Action_2_t92D149BA0BB9E6C54B7036167F4A14DFF1B5BE08*)il2cpp_codegen_object_new(Action_2_t92D149BA0BB9E6C54B7036167F4A14DFF1B5BE08_il2cpp_TypeInfo_var);
		NullCheck(L_75);
		Action_2__ctor_m045FACF9055F4BFC210539F7561F4F190BAAFACC(L_75, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m68475F9DBE473336E6961AAF9477B31B544CEB61_RuntimeMethod_var), NULL);
		((Writer_1_t43CDE8B30140776D24809288A7A734939B0C8392_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t43CDE8B30140776D24809288A7A734939B0C8392_il2cpp_TypeInfo_var))->___write_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t43CDE8B30140776D24809288A7A734939B0C8392_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t43CDE8B30140776D24809288A7A734939B0C8392_il2cpp_TypeInfo_var))->___write_0), (void*)L_75);
		Action_2_t427A3AAB52F4338A54E05C0F9CCE238604B75463* L_76 = (Action_2_t427A3AAB52F4338A54E05C0F9CCE238604B75463*)il2cpp_codegen_object_new(Action_2_t427A3AAB52F4338A54E05C0F9CCE238604B75463_il2cpp_TypeInfo_var);
		NullCheck(L_76);
		Action_2__ctor_m8E94D4420B59CF9ADA5F0454DC31ECA7750E462E(L_76, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_mFAEC5102CEF441E12C2E1A9A267D3A70C60C7B5C_RuntimeMethod_var), NULL);
		((Writer_1_t88385B356F11A0E27B1FA1BB321C08F75AE44C4D_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t88385B356F11A0E27B1FA1BB321C08F75AE44C4D_il2cpp_TypeInfo_var))->___write_0 = L_76;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t88385B356F11A0E27B1FA1BB321C08F75AE44C4D_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t88385B356F11A0E27B1FA1BB321C08F75AE44C4D_il2cpp_TypeInfo_var))->___write_0), (void*)L_76);
		Action_2_t19A08A9BB7D482718CD3474975A6559CA159FA9F* L_77 = (Action_2_t19A08A9BB7D482718CD3474975A6559CA159FA9F*)il2cpp_codegen_object_new(Action_2_t19A08A9BB7D482718CD3474975A6559CA159FA9F_il2cpp_TypeInfo_var);
		NullCheck(L_77);
		Action_2__ctor_mADF37BEC66DF6AB901EC870BE27B3318E0FC01EC(L_77, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_m79990113E28B9ADA181528EFC5D92C21AEC24CAF_RuntimeMethod_var), NULL);
		((Writer_1_t279D7AD1213DC4E446FDB2E12DD927F5544311B9_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t279D7AD1213DC4E446FDB2E12DD927F5544311B9_il2cpp_TypeInfo_var))->___write_0 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t279D7AD1213DC4E446FDB2E12DD927F5544311B9_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t279D7AD1213DC4E446FDB2E12DD927F5544311B9_il2cpp_TypeInfo_var))->___write_0), (void*)L_77);
		Action_2_tAF93D04C699FAC16A658F8DA78DD252A93E952E8* L_78 = (Action_2_tAF93D04C699FAC16A658F8DA78DD252A93E952E8*)il2cpp_codegen_object_new(Action_2_tAF93D04C699FAC16A658F8DA78DD252A93E952E8_il2cpp_TypeInfo_var);
		NullCheck(L_78);
		Action_2__ctor_m262F49A58D1EDD285AE54334A93FF77B4D892FFE(L_78, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ServerMatchMessage_mF90A563F787035F17D0CD90966372FB8DEFF4A03_RuntimeMethod_var), NULL);
		((Writer_1_t3F4AFD2264190A12B59C26F438E4D9A0D5C6B8A0_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t3F4AFD2264190A12B59C26F438E4D9A0D5C6B8A0_il2cpp_TypeInfo_var))->___write_0 = L_78;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t3F4AFD2264190A12B59C26F438E4D9A0D5C6B8A0_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t3F4AFD2264190A12B59C26F438E4D9A0D5C6B8A0_il2cpp_TypeInfo_var))->___write_0), (void*)L_78);
		Action_2_tEF11BC50B4B015730C0409DC35EAC7D6E03CEE11* L_79 = (Action_2_tEF11BC50B4B015730C0409DC35EAC7D6E03CEE11*)il2cpp_codegen_object_new(Action_2_tEF11BC50B4B015730C0409DC35EAC7D6E03CEE11_il2cpp_TypeInfo_var);
		NullCheck(L_79);
		Action_2__ctor_mCE1240AAB1D925B326D8A3F7F8FD25B6AFBA9D29(L_79, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ServerMatchOperation_m4ED27F7C3469C55F345EFBE642DC3F4DB0FB9B5C_RuntimeMethod_var), NULL);
		((Writer_1_tB00DD74436318ED1FFBD6AE43C142C83E0E441E7_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tB00DD74436318ED1FFBD6AE43C142C83E0E441E7_il2cpp_TypeInfo_var))->___write_0 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tB00DD74436318ED1FFBD6AE43C142C83E0E441E7_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tB00DD74436318ED1FFBD6AE43C142C83E0E441E7_il2cpp_TypeInfo_var))->___write_0), (void*)L_79);
		Action_2_tCFCEE592405B35148F5E1AC506FEB4F8CB2F49B6* L_80 = (Action_2_tCFCEE592405B35148F5E1AC506FEB4F8CB2F49B6*)il2cpp_codegen_object_new(Action_2_tCFCEE592405B35148F5E1AC506FEB4F8CB2F49B6_il2cpp_TypeInfo_var);
		NullCheck(L_80);
		Action_2__ctor_m76DF3E8268347D234290E4A12EC5A91BACEF6F18(L_80, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ClientMatchMessage_m9534DBB36A6BF3241779E9E5168D444DDF31E51C_RuntimeMethod_var), NULL);
		((Writer_1_tB611FFB23F108BACF0D008A5913B87BDA0B78161_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tB611FFB23F108BACF0D008A5913B87BDA0B78161_il2cpp_TypeInfo_var))->___write_0 = L_80;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tB611FFB23F108BACF0D008A5913B87BDA0B78161_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tB611FFB23F108BACF0D008A5913B87BDA0B78161_il2cpp_TypeInfo_var))->___write_0), (void*)L_80);
		Action_2_tE631CB825D55269F80774B2F55C5E62192891CF8* L_81 = (Action_2_tE631CB825D55269F80774B2F55C5E62192891CF8*)il2cpp_codegen_object_new(Action_2_tE631CB825D55269F80774B2F55C5E62192891CF8_il2cpp_TypeInfo_var);
		NullCheck(L_81);
		Action_2__ctor_mDD710CDD6E3F504530D5CFFD1FFC8A25E2264BBE(L_81, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_ClientMatchOperation_m08496CD05975A28AB59D4513B9BA36209FD081C0_RuntimeMethod_var), NULL);
		((Writer_1_t5B4A56E0557BF93D873BD64F49D7D7EB2FFAC769_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t5B4A56E0557BF93D873BD64F49D7D7EB2FFAC769_il2cpp_TypeInfo_var))->___write_0 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t5B4A56E0557BF93D873BD64F49D7D7EB2FFAC769_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t5B4A56E0557BF93D873BD64F49D7D7EB2FFAC769_il2cpp_TypeInfo_var))->___write_0), (void*)L_81);
		Action_2_t77285E9313B73F391F0816DD5738EBF05F8774AD* L_82 = (Action_2_t77285E9313B73F391F0816DD5738EBF05F8774AD*)il2cpp_codegen_object_new(Action_2_t77285E9313B73F391F0816DD5738EBF05F8774AD_il2cpp_TypeInfo_var);
		NullCheck(L_82);
		Action_2__ctor_m6827C4A015D2EB0AD992BF402EE39B204304F29D(L_82, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchInfoU5BU5D_m2A631F7A481D201FABBF213396D9D3C548AB54C0_RuntimeMethod_var), NULL);
		((Writer_1_t6BDF566A850D915F8607498B2A74D90E6AD3DFE4_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t6BDF566A850D915F8607498B2A74D90E6AD3DFE4_il2cpp_TypeInfo_var))->___write_0 = L_82;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t6BDF566A850D915F8607498B2A74D90E6AD3DFE4_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t6BDF566A850D915F8607498B2A74D90E6AD3DFE4_il2cpp_TypeInfo_var))->___write_0), (void*)L_82);
		Action_2_t1D8E4389A2FC4D611519FC7C996BF96F763E5017* L_83 = (Action_2_t1D8E4389A2FC4D611519FC7C996BF96F763E5017*)il2cpp_codegen_object_new(Action_2_t1D8E4389A2FC4D611519FC7C996BF96F763E5017_il2cpp_TypeInfo_var);
		NullCheck(L_83);
		Action_2__ctor_m9402B782F534C705854D68C8257E6F68F153EAC7(L_83, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchInfo_m154E3D80BB1984768FBF6C11EC8128FFF90EB743_RuntimeMethod_var), NULL);
		((Writer_1_tE837CDE21BF382361B1ED24E4C34DC6B7B9C65E0_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tE837CDE21BF382361B1ED24E4C34DC6B7B9C65E0_il2cpp_TypeInfo_var))->___write_0 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tE837CDE21BF382361B1ED24E4C34DC6B7B9C65E0_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tE837CDE21BF382361B1ED24E4C34DC6B7B9C65E0_il2cpp_TypeInfo_var))->___write_0), (void*)L_83);
		Action_2_t5199EDEE1233BFABE45CDA21E9230C8346645716* L_84 = (Action_2_t5199EDEE1233BFABE45CDA21E9230C8346645716*)il2cpp_codegen_object_new(Action_2_t5199EDEE1233BFABE45CDA21E9230C8346645716_il2cpp_TypeInfo_var);
		NullCheck(L_84);
		Action_2__ctor_m6BCDF8708D0F6E4FD910C30E564B2ADB417AE0B0(L_84, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_PlayerInfoU5BU5D_m258F7D9CB39C3B6D8C6B2BA7E8B1FE9491B38B50_RuntimeMethod_var), NULL);
		((Writer_1_tDB03F8AA9D2A369BF9554E72C2BAF71BECCC4D81_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tDB03F8AA9D2A369BF9554E72C2BAF71BECCC4D81_il2cpp_TypeInfo_var))->___write_0 = L_84;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tDB03F8AA9D2A369BF9554E72C2BAF71BECCC4D81_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tDB03F8AA9D2A369BF9554E72C2BAF71BECCC4D81_il2cpp_TypeInfo_var))->___write_0), (void*)L_84);
		Action_2_t15EAEA1713B2DE57B0F7BFED86F3CF5915138314* L_85 = (Action_2_t15EAEA1713B2DE57B0F7BFED86F3CF5915138314*)il2cpp_codegen_object_new(Action_2_t15EAEA1713B2DE57B0F7BFED86F3CF5915138314_il2cpp_TypeInfo_var);
		NullCheck(L_85);
		Action_2__ctor_mC80DE69A229698030304FD2BA3320438FA30FEAA(L_85, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_PlayerInfo_m03B2F18306AC1FE8C7C933859A265C2B38C03700_RuntimeMethod_var), NULL);
		((Writer_1_t080683EBC9B7A30F6121E3179DCB7370B1E8F62A_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t080683EBC9B7A30F6121E3179DCB7370B1E8F62A_il2cpp_TypeInfo_var))->___write_0 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t080683EBC9B7A30F6121E3179DCB7370B1E8F62A_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t080683EBC9B7A30F6121E3179DCB7370B1E8F62A_il2cpp_TypeInfo_var))->___write_0), (void*)L_85);
		Action_2_tC9D1D64ED53FED0525E8EE017F8CAEBD3D9CF610* L_86 = (Action_2_tC9D1D64ED53FED0525E8EE017F8CAEBD3D9CF610*)il2cpp_codegen_object_new(Action_2_tC9D1D64ED53FED0525E8EE017F8CAEBD3D9CF610_il2cpp_TypeInfo_var);
		NullCheck(L_86);
		Action_2__ctor_mB98661590A449BCEE8A7E3E99D8266BD99EBE5F4(L_86, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_Examples_Chat_ChatAuthenticator_AuthRequestMessage_m8266348F8A557605B0C42EC1EF5EA60DBA5E1C53_RuntimeMethod_var), NULL);
		((Writer_1_t256EBB9B83C8B6F6F428369117FBBEC36C7E390C_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t256EBB9B83C8B6F6F428369117FBBEC36C7E390C_il2cpp_TypeInfo_var))->___write_0 = L_86;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t256EBB9B83C8B6F6F428369117FBBEC36C7E390C_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t256EBB9B83C8B6F6F428369117FBBEC36C7E390C_il2cpp_TypeInfo_var))->___write_0), (void*)L_86);
		Action_2_t6B81023D4D427FA4343B3A8E4F4A88A145C6884F* L_87 = (Action_2_t6B81023D4D427FA4343B3A8E4F4A88A145C6884F*)il2cpp_codegen_object_new(Action_2_t6B81023D4D427FA4343B3A8E4F4A88A145C6884F_il2cpp_TypeInfo_var);
		NullCheck(L_87);
		Action_2__ctor_m2430D42BE1E29623D9B59695BC96B80DF7BA3CD1(L_87, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_Examples_Chat_ChatAuthenticator_AuthResponseMessage_m12300CEF77BCC2BC22FE9F7B9385CE87D7B02E10_RuntimeMethod_var), NULL);
		((Writer_1_t93E7560AB93CC6616158EBA6F0ADF0F95A12807A_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t93E7560AB93CC6616158EBA6F0ADF0F95A12807A_il2cpp_TypeInfo_var))->___write_0 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t93E7560AB93CC6616158EBA6F0ADF0F95A12807A_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t93E7560AB93CC6616158EBA6F0ADF0F95A12807A_il2cpp_TypeInfo_var))->___write_0), (void*)L_87);
		Action_2_tB478A835A6DC624AEA7C01C383390CE10F4D2BF1* L_88 = (Action_2_tB478A835A6DC624AEA7C01C383390CE10F4D2BF1*)il2cpp_codegen_object_new(Action_2_tB478A835A6DC624AEA7C01C383390CE10F4D2BF1_il2cpp_TypeInfo_var);
		NullCheck(L_88);
		Action_2__ctor_m024345F091E97FDED57D66405C674043E1EBD6A6(L_88, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_MatchPlayerData_m30BD65E22A872300D89020C18828BD180BA41FA7_RuntimeMethod_var), NULL);
		((Writer_1_t76BD90233C405D75BFDE795DB7AADBF14659D95A_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t76BD90233C405D75BFDE795DB7AADBF14659D95A_il2cpp_TypeInfo_var))->___write_0 = L_88;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_t76BD90233C405D75BFDE795DB7AADBF14659D95A_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_t76BD90233C405D75BFDE795DB7AADBF14659D95A_il2cpp_TypeInfo_var))->___write_0), (void*)L_88);
		Action_2_t117A07FAD93D252BC5E69F05373B4E286F1F8D56* L_89 = (Action_2_t117A07FAD93D252BC5E69F05373B4E286F1F8D56*)il2cpp_codegen_object_new(Action_2_t117A07FAD93D252BC5E69F05373B4E286F1F8D56_il2cpp_TypeInfo_var);
		NullCheck(L_89);
		Action_2__ctor_m56261BE7DB6718F58FD8A022A04A8AD1FDF095C4(L_89, NULL, (intptr_t)((void*)GeneratedNetworkCode__Write_Mirror_Examples_MultipleMatch_CellValue_mE850D02656D8C7B9181D58B92D4CA0583C063A1D_RuntimeMethod_var), NULL);
		((Writer_1_tADD2D185EF8CADFAA813221CFBDB1706512C378A_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tADD2D185EF8CADFAA813221CFBDB1706512C378A_il2cpp_TypeInfo_var))->___write_0 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&((Writer_1_tADD2D185EF8CADFAA813221CFBDB1706512C378A_StaticFields*)il2cpp_codegen_static_fields_for(Writer_1_tADD2D185EF8CADFAA813221CFBDB1706512C378A_il2cpp_TypeInfo_var))->___write_0), (void*)L_89);
		Func_2_t0BC506BED5AC85C37292910C8BDAD52A9F6C9986* L_90 = (Func_2_t0BC506BED5AC85C37292910C8BDAD52A9F6C9986*)il2cpp_codegen_object_new(Func_2_t0BC506BED5AC85C37292910C8BDAD52A9F6C9986_il2cpp_TypeInfo_var);
		NullCheck(L_90);
		Func_2__ctor_m5070845CE83FAC65D5C1A17411A99898A16002D6(L_90, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_RuntimeMethod_var), NULL);
		((Reader_1_t894EA749E730DD156D39D5D3C89DD01B1840FC90_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t894EA749E730DD156D39D5D3C89DD01B1840FC90_il2cpp_TypeInfo_var))->___read_0 = L_90;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t894EA749E730DD156D39D5D3C89DD01B1840FC90_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t894EA749E730DD156D39D5D3C89DD01B1840FC90_il2cpp_TypeInfo_var))->___read_0), (void*)L_90);
		Func_2_tC5D3F83B86462F66072C190B541019F5456F2DB9* L_91 = (Func_2_tC5D3F83B86462F66072C190B541019F5456F2DB9*)il2cpp_codegen_object_new(Func_2_tC5D3F83B86462F66072C190B541019F5456F2DB9_il2cpp_TypeInfo_var);
		NullCheck(L_91);
		Func_2__ctor_m623460CD3E0ED5E1D2A78F2D45931C5D4B106446(L_91, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadByteNullable_mB622478495C2AE927128F9F196A47DCFEB666E4E_RuntimeMethod_var), NULL);
		((Reader_1_t4F335D70E11B569B7B2CFBE0E13B13D07B3BA4CF_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t4F335D70E11B569B7B2CFBE0E13B13D07B3BA4CF_il2cpp_TypeInfo_var))->___read_0 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t4F335D70E11B569B7B2CFBE0E13B13D07B3BA4CF_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t4F335D70E11B569B7B2CFBE0E13B13D07B3BA4CF_il2cpp_TypeInfo_var))->___read_0), (void*)L_91);
		Func_2_t3EE5A9E53DE82B19DAC86C3D30AC7BD98D305238* L_92 = (Func_2_t3EE5A9E53DE82B19DAC86C3D30AC7BD98D305238*)il2cpp_codegen_object_new(Func_2_t3EE5A9E53DE82B19DAC86C3D30AC7BD98D305238_il2cpp_TypeInfo_var);
		NullCheck(L_92);
		Func_2__ctor_m5591363477B7D64C33CBFEA8D4EB049A3B5C84D1(L_92, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadSByte_m1B3975CC87DD10621C8A369EA7D053AFE57E958B_RuntimeMethod_var), NULL);
		((Reader_1_t9773E186C54234048A015DDEBB899F90C35E96B0_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t9773E186C54234048A015DDEBB899F90C35E96B0_il2cpp_TypeInfo_var))->___read_0 = L_92;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t9773E186C54234048A015DDEBB899F90C35E96B0_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t9773E186C54234048A015DDEBB899F90C35E96B0_il2cpp_TypeInfo_var))->___read_0), (void*)L_92);
		Func_2_tFB31016B91F9F5C6B2CAC98095F7D025353DD033* L_93 = (Func_2_tFB31016B91F9F5C6B2CAC98095F7D025353DD033*)il2cpp_codegen_object_new(Func_2_tFB31016B91F9F5C6B2CAC98095F7D025353DD033_il2cpp_TypeInfo_var);
		NullCheck(L_93);
		Func_2__ctor_m063EDE6FCD17CDBD99B22C07C6DD4FC2B8A92DF1(L_93, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadSByteNullable_mEAB105DEC52D7789AEE2A6E110B66A3C2EB8785E_RuntimeMethod_var), NULL);
		((Reader_1_t94C4EBCF7B54695CBEF81ACC847D9D01B8D34339_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t94C4EBCF7B54695CBEF81ACC847D9D01B8D34339_il2cpp_TypeInfo_var))->___read_0 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t94C4EBCF7B54695CBEF81ACC847D9D01B8D34339_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t94C4EBCF7B54695CBEF81ACC847D9D01B8D34339_il2cpp_TypeInfo_var))->___read_0), (void*)L_93);
		Func_2_t26F6F63A0DA84EBAD15F6BCC374071D9F6C8B42C* L_94 = (Func_2_t26F6F63A0DA84EBAD15F6BCC374071D9F6C8B42C*)il2cpp_codegen_object_new(Func_2_t26F6F63A0DA84EBAD15F6BCC374071D9F6C8B42C_il2cpp_TypeInfo_var);
		NullCheck(L_94);
		Func_2__ctor_m4BA6B0FD3F6DB1E9057511A3857A5B01026A7E1A(L_94, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadChar_mCCA8829AD9CA54D8510AE4C3E3D1CA0F6F6E8966_RuntimeMethod_var), NULL);
		((Reader_1_t8D947140B208E58AB2F0C97B38D53DDA61149A4A_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t8D947140B208E58AB2F0C97B38D53DDA61149A4A_il2cpp_TypeInfo_var))->___read_0 = L_94;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t8D947140B208E58AB2F0C97B38D53DDA61149A4A_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t8D947140B208E58AB2F0C97B38D53DDA61149A4A_il2cpp_TypeInfo_var))->___read_0), (void*)L_94);
		Func_2_tBB870D080A3294867CDE63CF2AF4F2D4699C33B2* L_95 = (Func_2_tBB870D080A3294867CDE63CF2AF4F2D4699C33B2*)il2cpp_codegen_object_new(Func_2_tBB870D080A3294867CDE63CF2AF4F2D4699C33B2_il2cpp_TypeInfo_var);
		NullCheck(L_95);
		Func_2__ctor_m30FB8CA63098A957572E1D071C547AF43811FBC5(L_95, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadCharNullable_m728E4E8F336F06A0BDB8BDEE69842C707ED4540A_RuntimeMethod_var), NULL);
		((Reader_1_tDB685B03953454DEB76F09E00F64F95F5B14E21F_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tDB685B03953454DEB76F09E00F64F95F5B14E21F_il2cpp_TypeInfo_var))->___read_0 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tDB685B03953454DEB76F09E00F64F95F5B14E21F_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tDB685B03953454DEB76F09E00F64F95F5B14E21F_il2cpp_TypeInfo_var))->___read_0), (void*)L_95);
		Func_2_tE35FCB04208B127B021F241EA079464ACC551B7C* L_96 = (Func_2_tE35FCB04208B127B021F241EA079464ACC551B7C*)il2cpp_codegen_object_new(Func_2_tE35FCB04208B127B021F241EA079464ACC551B7C_il2cpp_TypeInfo_var);
		NullCheck(L_96);
		Func_2__ctor_mC260EE2E3AADF08FECA8211082B9843A2049F86A(L_96, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06_RuntimeMethod_var), NULL);
		((Reader_1_t942164B32720705E43130F8A5332C5B7AFF4FAE5_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t942164B32720705E43130F8A5332C5B7AFF4FAE5_il2cpp_TypeInfo_var))->___read_0 = L_96;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t942164B32720705E43130F8A5332C5B7AFF4FAE5_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t942164B32720705E43130F8A5332C5B7AFF4FAE5_il2cpp_TypeInfo_var))->___read_0), (void*)L_96);
		Func_2_t2CF9D64DD882AC30B207D3D6C3C4EFC846A40BC9* L_97 = (Func_2_t2CF9D64DD882AC30B207D3D6C3C4EFC846A40BC9*)il2cpp_codegen_object_new(Func_2_t2CF9D64DD882AC30B207D3D6C3C4EFC846A40BC9_il2cpp_TypeInfo_var);
		NullCheck(L_97);
		Func_2__ctor_mAA5ECC078690E371A556E1D41024A45A75322618(L_97, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadBoolNullable_m49866827FF66A52CA4E36AAED2D3ACC8766F8B38_RuntimeMethod_var), NULL);
		((Reader_1_t36FED30A7BD4508A497B3FDF00A93EFCBF2CC6C6_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t36FED30A7BD4508A497B3FDF00A93EFCBF2CC6C6_il2cpp_TypeInfo_var))->___read_0 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t36FED30A7BD4508A497B3FDF00A93EFCBF2CC6C6_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t36FED30A7BD4508A497B3FDF00A93EFCBF2CC6C6_il2cpp_TypeInfo_var))->___read_0), (void*)L_97);
		Func_2_tEE61CDD83F7808585CB68357D75DC2637FBA492F* L_98 = (Func_2_tEE61CDD83F7808585CB68357D75DC2637FBA492F*)il2cpp_codegen_object_new(Func_2_tEE61CDD83F7808585CB68357D75DC2637FBA492F_il2cpp_TypeInfo_var);
		NullCheck(L_98);
		Func_2__ctor_mB1D9D8504E593D505ADEAF64E72EB0B4C33D0A4A(L_98, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadShort_m5FFC8A9D90AE04D1D0AD681F1D2C32564DBC8677_RuntimeMethod_var), NULL);
		((Reader_1_t436C9D757F7667ABD9FB2D630506ED1ED29652AA_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t436C9D757F7667ABD9FB2D630506ED1ED29652AA_il2cpp_TypeInfo_var))->___read_0 = L_98;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t436C9D757F7667ABD9FB2D630506ED1ED29652AA_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t436C9D757F7667ABD9FB2D630506ED1ED29652AA_il2cpp_TypeInfo_var))->___read_0), (void*)L_98);
		Func_2_tA3EC1D81F5C9B991E5E551D9C0197BBCFA6ECFC8* L_99 = (Func_2_tA3EC1D81F5C9B991E5E551D9C0197BBCFA6ECFC8*)il2cpp_codegen_object_new(Func_2_tA3EC1D81F5C9B991E5E551D9C0197BBCFA6ECFC8_il2cpp_TypeInfo_var);
		NullCheck(L_99);
		Func_2__ctor_mBE9EDCAC9C310837BC45D0CF3BFF615DC4A9D163(L_99, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadShortNullable_m6B7ED0AC2C951C2461A34D3DEB05E6055399B896_RuntimeMethod_var), NULL);
		((Reader_1_tE473AB6B794D2436C26789A313B67896AB07A48D_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tE473AB6B794D2436C26789A313B67896AB07A48D_il2cpp_TypeInfo_var))->___read_0 = L_99;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tE473AB6B794D2436C26789A313B67896AB07A48D_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tE473AB6B794D2436C26789A313B67896AB07A48D_il2cpp_TypeInfo_var))->___read_0), (void*)L_99);
		Func_2_tD28C61E3A0A47F71DB0CDCB3B43C7F13D4F82235* L_100 = (Func_2_tD28C61E3A0A47F71DB0CDCB3B43C7F13D4F82235*)il2cpp_codegen_object_new(Func_2_tD28C61E3A0A47F71DB0CDCB3B43C7F13D4F82235_il2cpp_TypeInfo_var);
		NullCheck(L_100);
		Func_2__ctor_mBA9972836E55A9428AE3C331A510B4ABB56BEBCA(L_100, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadUShort_mA98395DD1B1DA249096858B171B8BC23D95DF765_RuntimeMethod_var), NULL);
		((Reader_1_t198B364FF80EB504EC1C04BA6BDB4431FCAC173E_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t198B364FF80EB504EC1C04BA6BDB4431FCAC173E_il2cpp_TypeInfo_var))->___read_0 = L_100;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t198B364FF80EB504EC1C04BA6BDB4431FCAC173E_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t198B364FF80EB504EC1C04BA6BDB4431FCAC173E_il2cpp_TypeInfo_var))->___read_0), (void*)L_100);
		Func_2_t4842934F57735E601CC8B995D6E040EAF256FBF4* L_101 = (Func_2_t4842934F57735E601CC8B995D6E040EAF256FBF4*)il2cpp_codegen_object_new(Func_2_t4842934F57735E601CC8B995D6E040EAF256FBF4_il2cpp_TypeInfo_var);
		NullCheck(L_101);
		Func_2__ctor_m7BDA824E493FDB492F25EB39A3F521CF201ACC12(L_101, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadUShortNullable_mEDDEE70BF7A15DC1503C4BDF580F54A26C82DCA4_RuntimeMethod_var), NULL);
		((Reader_1_tADD3DCC9F400A548DD461E9BE16C5ACCBCA80B8E_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tADD3DCC9F400A548DD461E9BE16C5ACCBCA80B8E_il2cpp_TypeInfo_var))->___read_0 = L_101;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tADD3DCC9F400A548DD461E9BE16C5ACCBCA80B8E_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tADD3DCC9F400A548DD461E9BE16C5ACCBCA80B8E_il2cpp_TypeInfo_var))->___read_0), (void*)L_101);
		Func_2_t3F9C9CE2DD4A455F6BF2ACA2CBC6A00343B798EA* L_102 = (Func_2_t3F9C9CE2DD4A455F6BF2ACA2CBC6A00343B798EA*)il2cpp_codegen_object_new(Func_2_t3F9C9CE2DD4A455F6BF2ACA2CBC6A00343B798EA_il2cpp_TypeInfo_var);
		NullCheck(L_102);
		Func_2__ctor_m6D793C4BB9EFC6D203C711F6DFCC319E4293DF80(L_102, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A_RuntimeMethod_var), NULL);
		((Reader_1_t259EB4559035BAE63FEDCB6F4C5B72D8CA00F6C7_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t259EB4559035BAE63FEDCB6F4C5B72D8CA00F6C7_il2cpp_TypeInfo_var))->___read_0 = L_102;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t259EB4559035BAE63FEDCB6F4C5B72D8CA00F6C7_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t259EB4559035BAE63FEDCB6F4C5B72D8CA00F6C7_il2cpp_TypeInfo_var))->___read_0), (void*)L_102);
		Func_2_tA8008B61C32E0E3A90F98593FFE956D148147DC5* L_103 = (Func_2_tA8008B61C32E0E3A90F98593FFE956D148147DC5*)il2cpp_codegen_object_new(Func_2_tA8008B61C32E0E3A90F98593FFE956D148147DC5_il2cpp_TypeInfo_var);
		NullCheck(L_103);
		Func_2__ctor_mBDE37F18AF96D73F7BF1AE78624BBC669E47EE73(L_103, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadIntNullable_m9F68CD73D47D10DE2E1C6934DE14234E19D02E71_RuntimeMethod_var), NULL);
		((Reader_1_tA2296DCE521FC65EB6FBA78472797DC918D9FE4E_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tA2296DCE521FC65EB6FBA78472797DC918D9FE4E_il2cpp_TypeInfo_var))->___read_0 = L_103;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tA2296DCE521FC65EB6FBA78472797DC918D9FE4E_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tA2296DCE521FC65EB6FBA78472797DC918D9FE4E_il2cpp_TypeInfo_var))->___read_0), (void*)L_103);
		Func_2_t1FE4698618B38731C4E2D67BCC314778BA13ECC0* L_104 = (Func_2_t1FE4698618B38731C4E2D67BCC314778BA13ECC0*)il2cpp_codegen_object_new(Func_2_t1FE4698618B38731C4E2D67BCC314778BA13ECC0_il2cpp_TypeInfo_var);
		NullCheck(L_104);
		Func_2__ctor_m0F6B5856378B49521EC96E3CDC25AFB2072D2338(L_104, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_RuntimeMethod_var), NULL);
		((Reader_1_t3E733A5CBD67C2A9860A6535DC31C53DC3294C05_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t3E733A5CBD67C2A9860A6535DC31C53DC3294C05_il2cpp_TypeInfo_var))->___read_0 = L_104;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t3E733A5CBD67C2A9860A6535DC31C53DC3294C05_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t3E733A5CBD67C2A9860A6535DC31C53DC3294C05_il2cpp_TypeInfo_var))->___read_0), (void*)L_104);
		Func_2_tE53CE7EE59BDF79AA9E78FAB9A62695508883396* L_105 = (Func_2_tE53CE7EE59BDF79AA9E78FAB9A62695508883396*)il2cpp_codegen_object_new(Func_2_tE53CE7EE59BDF79AA9E78FAB9A62695508883396_il2cpp_TypeInfo_var);
		NullCheck(L_105);
		Func_2__ctor_mC30D04ED011FDBB56C538B5F51AD9850C112485B(L_105, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadUIntNullable_m3D4906C1707F48E5439F4EA0E9DE4B8860AB2E73_RuntimeMethod_var), NULL);
		((Reader_1_t4C51943F9874BC9563E868F14B951239A3817126_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t4C51943F9874BC9563E868F14B951239A3817126_il2cpp_TypeInfo_var))->___read_0 = L_105;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t4C51943F9874BC9563E868F14B951239A3817126_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t4C51943F9874BC9563E868F14B951239A3817126_il2cpp_TypeInfo_var))->___read_0), (void*)L_105);
		Func_2_t0D88BB68D7BF644024EA7DFB9E7CDBA1EE7EF69C* L_106 = (Func_2_t0D88BB68D7BF644024EA7DFB9E7CDBA1EE7EF69C*)il2cpp_codegen_object_new(Func_2_t0D88BB68D7BF644024EA7DFB9E7CDBA1EE7EF69C_il2cpp_TypeInfo_var);
		NullCheck(L_106);
		Func_2__ctor_m11E9C1A471F4D9E73120EF4CAAAEA62C795BED5B(L_106, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadLong_m67D408F9D8D9FB04A0101AAE2AB9B01120E34435_RuntimeMethod_var), NULL);
		((Reader_1_tF38A27591F97AE59155FCEDBC831BE975EBD3842_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tF38A27591F97AE59155FCEDBC831BE975EBD3842_il2cpp_TypeInfo_var))->___read_0 = L_106;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tF38A27591F97AE59155FCEDBC831BE975EBD3842_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tF38A27591F97AE59155FCEDBC831BE975EBD3842_il2cpp_TypeInfo_var))->___read_0), (void*)L_106);
		Func_2_t8F78F44B218E20FDA86CA793AEC9892B18B978F6* L_107 = (Func_2_t8F78F44B218E20FDA86CA793AEC9892B18B978F6*)il2cpp_codegen_object_new(Func_2_t8F78F44B218E20FDA86CA793AEC9892B18B978F6_il2cpp_TypeInfo_var);
		NullCheck(L_107);
		Func_2__ctor_m7D133FF807CD80F365E68C437CA8D0AB82D0AEB9(L_107, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadLongNullable_m747B938C128B0CAD7E22D0909E1AEE9DFDB54F67_RuntimeMethod_var), NULL);
		((Reader_1_t16EF44DD5D73A84B2F654B9671D58A442A179C99_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t16EF44DD5D73A84B2F654B9671D58A442A179C99_il2cpp_TypeInfo_var))->___read_0 = L_107;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t16EF44DD5D73A84B2F654B9671D58A442A179C99_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t16EF44DD5D73A84B2F654B9671D58A442A179C99_il2cpp_TypeInfo_var))->___read_0), (void*)L_107);
		Func_2_t86251E51D7B41B00F1CC89068612DB2EE6F5FDE0* L_108 = (Func_2_t86251E51D7B41B00F1CC89068612DB2EE6F5FDE0*)il2cpp_codegen_object_new(Func_2_t86251E51D7B41B00F1CC89068612DB2EE6F5FDE0_il2cpp_TypeInfo_var);
		NullCheck(L_108);
		Func_2__ctor_m3059ADDD45E7205D5C2639AE508CA8657796F711(L_108, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadULong_mAC6B83521EBA7FFEDFC72A6AAE1BF5D87221A5F5_RuntimeMethod_var), NULL);
		((Reader_1_t99038D5B7B0CB7EA48F6A69A3DA42DDF77732757_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t99038D5B7B0CB7EA48F6A69A3DA42DDF77732757_il2cpp_TypeInfo_var))->___read_0 = L_108;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t99038D5B7B0CB7EA48F6A69A3DA42DDF77732757_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t99038D5B7B0CB7EA48F6A69A3DA42DDF77732757_il2cpp_TypeInfo_var))->___read_0), (void*)L_108);
		Func_2_t4B1235C83470DD7C3286754A4CFC840FBFD35741* L_109 = (Func_2_t4B1235C83470DD7C3286754A4CFC840FBFD35741*)il2cpp_codegen_object_new(Func_2_t4B1235C83470DD7C3286754A4CFC840FBFD35741_il2cpp_TypeInfo_var);
		NullCheck(L_109);
		Func_2__ctor_mB28A3EB5FCA927A61BE69EA4BB8342654C70D6AB(L_109, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadULongNullable_mE853C2A40E3E7F9FD1BB49D5E16BCB9310B0752E_RuntimeMethod_var), NULL);
		((Reader_1_tFFD0A7FB0A8C37CF4374C67CB0DF9F2448CA9784_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tFFD0A7FB0A8C37CF4374C67CB0DF9F2448CA9784_il2cpp_TypeInfo_var))->___read_0 = L_109;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tFFD0A7FB0A8C37CF4374C67CB0DF9F2448CA9784_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tFFD0A7FB0A8C37CF4374C67CB0DF9F2448CA9784_il2cpp_TypeInfo_var))->___read_0), (void*)L_109);
		Func_2_tDA855C737BB7E9546B48245EC0F04DAA56A1CB03* L_110 = (Func_2_tDA855C737BB7E9546B48245EC0F04DAA56A1CB03*)il2cpp_codegen_object_new(Func_2_tDA855C737BB7E9546B48245EC0F04DAA56A1CB03_il2cpp_TypeInfo_var);
		NullCheck(L_110);
		Func_2__ctor_m5826E0D00074A31DA58583D4A1F1FFA110E9EED6(L_110, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadFloat_mF3D9834531FC09112A506971638FB9682A231D97_RuntimeMethod_var), NULL);
		((Reader_1_t7D07B6E13CF9AA9927D4FD2385DB61464F9B5FE4_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t7D07B6E13CF9AA9927D4FD2385DB61464F9B5FE4_il2cpp_TypeInfo_var))->___read_0 = L_110;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t7D07B6E13CF9AA9927D4FD2385DB61464F9B5FE4_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t7D07B6E13CF9AA9927D4FD2385DB61464F9B5FE4_il2cpp_TypeInfo_var))->___read_0), (void*)L_110);
		Func_2_t2A9651B5311D10F2FDA41A113A50E18A22D93138* L_111 = (Func_2_t2A9651B5311D10F2FDA41A113A50E18A22D93138*)il2cpp_codegen_object_new(Func_2_t2A9651B5311D10F2FDA41A113A50E18A22D93138_il2cpp_TypeInfo_var);
		NullCheck(L_111);
		Func_2__ctor_mA5F35AB5F92DCFC66D9D99AC7287972743050602(L_111, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadFloatNullable_m1EB56AA1F1CDB7981728CACF5941EB0B6B4275BD_RuntimeMethod_var), NULL);
		((Reader_1_t0676D3BA8C95F6153F7DA06A84ECA06D85436585_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t0676D3BA8C95F6153F7DA06A84ECA06D85436585_il2cpp_TypeInfo_var))->___read_0 = L_111;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t0676D3BA8C95F6153F7DA06A84ECA06D85436585_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t0676D3BA8C95F6153F7DA06A84ECA06D85436585_il2cpp_TypeInfo_var))->___read_0), (void*)L_111);
		Func_2_t589ED36A1F9FA112DA0EA54F4A46CBAEE886A07A* L_112 = (Func_2_t589ED36A1F9FA112DA0EA54F4A46CBAEE886A07A*)il2cpp_codegen_object_new(Func_2_t589ED36A1F9FA112DA0EA54F4A46CBAEE886A07A_il2cpp_TypeInfo_var);
		NullCheck(L_112);
		Func_2__ctor_m95A1D4FB759F59D21AE946C9FA4D83F32EDB1310(L_112, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadDouble_m949A60A21C6EB3B9952A43355903F08B3A7E0EF9_RuntimeMethod_var), NULL);
		((Reader_1_tF46320E57E205382BE23057439BFC8EA1F2457F0_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tF46320E57E205382BE23057439BFC8EA1F2457F0_il2cpp_TypeInfo_var))->___read_0 = L_112;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tF46320E57E205382BE23057439BFC8EA1F2457F0_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tF46320E57E205382BE23057439BFC8EA1F2457F0_il2cpp_TypeInfo_var))->___read_0), (void*)L_112);
		Func_2_t3C87EC87405703EAD0A26AD3BD3A5B3615EB39E6* L_113 = (Func_2_t3C87EC87405703EAD0A26AD3BD3A5B3615EB39E6*)il2cpp_codegen_object_new(Func_2_t3C87EC87405703EAD0A26AD3BD3A5B3615EB39E6_il2cpp_TypeInfo_var);
		NullCheck(L_113);
		Func_2__ctor_m104798A3ACA5A83E5355D889A5E09A0E1BE047BC(L_113, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadDoubleNullable_mE52DB83CB818F30F912FD40175B39731A2FBD33B_RuntimeMethod_var), NULL);
		((Reader_1_tC8C6D6D608FA323A6019F7C773782247361141D0_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tC8C6D6D608FA323A6019F7C773782247361141D0_il2cpp_TypeInfo_var))->___read_0 = L_113;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tC8C6D6D608FA323A6019F7C773782247361141D0_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tC8C6D6D608FA323A6019F7C773782247361141D0_il2cpp_TypeInfo_var))->___read_0), (void*)L_113);
		Func_2_tB643983D6CBD6220B483FEE44F3207125D7D3726* L_114 = (Func_2_tB643983D6CBD6220B483FEE44F3207125D7D3726*)il2cpp_codegen_object_new(Func_2_tB643983D6CBD6220B483FEE44F3207125D7D3726_il2cpp_TypeInfo_var);
		NullCheck(L_114);
		Func_2__ctor_m4E78040095EF78EF445101B5D92C9741EC6F76F5(L_114, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadDecimal_m79DE6589996D493A3A95BAD98036B09FF9CB144E_RuntimeMethod_var), NULL);
		((Reader_1_tA05950C10C6D79E26FD7C70ACCED03FCC01F5FC0_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tA05950C10C6D79E26FD7C70ACCED03FCC01F5FC0_il2cpp_TypeInfo_var))->___read_0 = L_114;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tA05950C10C6D79E26FD7C70ACCED03FCC01F5FC0_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tA05950C10C6D79E26FD7C70ACCED03FCC01F5FC0_il2cpp_TypeInfo_var))->___read_0), (void*)L_114);
		Func_2_t2ED5FBB4104367F73E820E5864485F4EDD912E0F* L_115 = (Func_2_t2ED5FBB4104367F73E820E5864485F4EDD912E0F*)il2cpp_codegen_object_new(Func_2_t2ED5FBB4104367F73E820E5864485F4EDD912E0F_il2cpp_TypeInfo_var);
		NullCheck(L_115);
		Func_2__ctor_m564CECA9ABF0AAB5D6346FBF31136AF3E515B7D7(L_115, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadDecimalNullable_m18D27D0176D98F043EC804A512EB8B55856229E9_RuntimeMethod_var), NULL);
		((Reader_1_t9C04509E1AAFBF9B8BE4514971DC0FAFCF576DCE_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t9C04509E1AAFBF9B8BE4514971DC0FAFCF576DCE_il2cpp_TypeInfo_var))->___read_0 = L_115;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t9C04509E1AAFBF9B8BE4514971DC0FAFCF576DCE_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t9C04509E1AAFBF9B8BE4514971DC0FAFCF576DCE_il2cpp_TypeInfo_var))->___read_0), (void*)L_115);
		Func_2_tF047810C662C3A551DDB01290047E803F32DA440* L_116 = (Func_2_tF047810C662C3A551DDB01290047E803F32DA440*)il2cpp_codegen_object_new(Func_2_tF047810C662C3A551DDB01290047E803F32DA440_il2cpp_TypeInfo_var);
		NullCheck(L_116);
		Func_2__ctor_mA7515F595687974D1E2D8CF2075226849F6ED4DE(L_116, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadString_m5A56179D2C1CB509EC9C762F97BBD4133A5AD394_RuntimeMethod_var), NULL);
		((Reader_1_t822A5642F255AE3ACC0D29BCD12B45F8C7A6EBAE_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t822A5642F255AE3ACC0D29BCD12B45F8C7A6EBAE_il2cpp_TypeInfo_var))->___read_0 = L_116;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t822A5642F255AE3ACC0D29BCD12B45F8C7A6EBAE_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t822A5642F255AE3ACC0D29BCD12B45F8C7A6EBAE_il2cpp_TypeInfo_var))->___read_0), (void*)L_116);
		Func_2_t68753017D74F6BABDA2E69D18EC2FA64AC20B03F* L_117 = (Func_2_t68753017D74F6BABDA2E69D18EC2FA64AC20B03F*)il2cpp_codegen_object_new(Func_2_t68753017D74F6BABDA2E69D18EC2FA64AC20B03F_il2cpp_TypeInfo_var);
		NullCheck(L_117);
		Func_2__ctor_m0D22158C554642C2D76B64BA604FFF9BE0826A76(L_117, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadBytesAndSize_mB707572AAF6CBDE9E6FAC190629882468EAFAD8E_RuntimeMethod_var), NULL);
		((Reader_1_tDF34563EB909B8D900E05A73EAE294A1BE35E704_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tDF34563EB909B8D900E05A73EAE294A1BE35E704_il2cpp_TypeInfo_var))->___read_0 = L_117;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tDF34563EB909B8D900E05A73EAE294A1BE35E704_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tDF34563EB909B8D900E05A73EAE294A1BE35E704_il2cpp_TypeInfo_var))->___read_0), (void*)L_117);
		Func_2_t0968F31AEE953CA42422B45BEE8A04FB272E086F* L_118 = (Func_2_t0968F31AEE953CA42422B45BEE8A04FB272E086F*)il2cpp_codegen_object_new(Func_2_t0968F31AEE953CA42422B45BEE8A04FB272E086F_il2cpp_TypeInfo_var);
		NullCheck(L_118);
		Func_2__ctor_m82AC9C94C7FDC3C7453D4D1B4AF7C0A8F211231D(L_118, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F_RuntimeMethod_var), NULL);
		((Reader_1_tDC213F8037D359AA1C0EC410CE387A872B5F05EB_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tDC213F8037D359AA1C0EC410CE387A872B5F05EB_il2cpp_TypeInfo_var))->___read_0 = L_118;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tDC213F8037D359AA1C0EC410CE387A872B5F05EB_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tDC213F8037D359AA1C0EC410CE387A872B5F05EB_il2cpp_TypeInfo_var))->___read_0), (void*)L_118);
		Func_2_t9C4AED5DAF5E665602E3D752BDF50BAE7C2C5D0F* L_119 = (Func_2_t9C4AED5DAF5E665602E3D752BDF50BAE7C2C5D0F*)il2cpp_codegen_object_new(Func_2_t9C4AED5DAF5E665602E3D752BDF50BAE7C2C5D0F_il2cpp_TypeInfo_var);
		NullCheck(L_119);
		Func_2__ctor_mAE410B36A1308D178CFC6F7F427D01B5E52C98E9(L_119, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadVector2_m673B821E39E194BA5E2B7E5F444D6CCD76812811_RuntimeMethod_var), NULL);
		((Reader_1_t7A28D876B4CE12F3E4C96FA0EDF6E7175F65C810_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t7A28D876B4CE12F3E4C96FA0EDF6E7175F65C810_il2cpp_TypeInfo_var))->___read_0 = L_119;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t7A28D876B4CE12F3E4C96FA0EDF6E7175F65C810_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t7A28D876B4CE12F3E4C96FA0EDF6E7175F65C810_il2cpp_TypeInfo_var))->___read_0), (void*)L_119);
		Func_2_t603A0F2238D55FD8325BC0816D55C2970606BB81* L_120 = (Func_2_t603A0F2238D55FD8325BC0816D55C2970606BB81*)il2cpp_codegen_object_new(Func_2_t603A0F2238D55FD8325BC0816D55C2970606BB81_il2cpp_TypeInfo_var);
		NullCheck(L_120);
		Func_2__ctor_m6EA96A3743D0E29565D31A08F3F6641E7224A5CB(L_120, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadVector2Nullable_mCDCE58B581701AC12499A36355838E45F298C817_RuntimeMethod_var), NULL);
		((Reader_1_tA9538DBB44C0A8B65EDF025A5D5FA611D152FA31_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tA9538DBB44C0A8B65EDF025A5D5FA611D152FA31_il2cpp_TypeInfo_var))->___read_0 = L_120;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tA9538DBB44C0A8B65EDF025A5D5FA611D152FA31_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tA9538DBB44C0A8B65EDF025A5D5FA611D152FA31_il2cpp_TypeInfo_var))->___read_0), (void*)L_120);
		Func_2_t9BD17C9037D00DFA5EFF1643A78C3E4300A31331* L_121 = (Func_2_t9BD17C9037D00DFA5EFF1643A78C3E4300A31331*)il2cpp_codegen_object_new(Func_2_t9BD17C9037D00DFA5EFF1643A78C3E4300A31331_il2cpp_TypeInfo_var);
		NullCheck(L_121);
		Func_2__ctor_m2A154129F35F2D94A66B118368E22F485E2E1754(L_121, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadVector3_mD35BF8B14DD5F75688AB9C360D138D1BAB432637_RuntimeMethod_var), NULL);
		((Reader_1_tA296681D381BFB2393CD36BF8D1737E846F15634_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tA296681D381BFB2393CD36BF8D1737E846F15634_il2cpp_TypeInfo_var))->___read_0 = L_121;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tA296681D381BFB2393CD36BF8D1737E846F15634_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tA296681D381BFB2393CD36BF8D1737E846F15634_il2cpp_TypeInfo_var))->___read_0), (void*)L_121);
		Func_2_t58F06EBEDB50273CFFCE1BED6156FF00537E7BF6* L_122 = (Func_2_t58F06EBEDB50273CFFCE1BED6156FF00537E7BF6*)il2cpp_codegen_object_new(Func_2_t58F06EBEDB50273CFFCE1BED6156FF00537E7BF6_il2cpp_TypeInfo_var);
		NullCheck(L_122);
		Func_2__ctor_m8AFF539B9E7BEE8788570878F77A6B1EB9E56BFA(L_122, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadVector3Nullable_m17D39303F570FAB53014718C07327F9DAAD8DB18_RuntimeMethod_var), NULL);
		((Reader_1_t59A60ECBC527BDCD550C648C4F309C4E55AE4AA7_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t59A60ECBC527BDCD550C648C4F309C4E55AE4AA7_il2cpp_TypeInfo_var))->___read_0 = L_122;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t59A60ECBC527BDCD550C648C4F309C4E55AE4AA7_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t59A60ECBC527BDCD550C648C4F309C4E55AE4AA7_il2cpp_TypeInfo_var))->___read_0), (void*)L_122);
		Func_2_tEA21835A50BEE04419A104B2F4D8CCE015122924* L_123 = (Func_2_tEA21835A50BEE04419A104B2F4D8CCE015122924*)il2cpp_codegen_object_new(Func_2_tEA21835A50BEE04419A104B2F4D8CCE015122924_il2cpp_TypeInfo_var);
		NullCheck(L_123);
		Func_2__ctor_m0CE18E4741D0AEEF77AAD040BB74509D63E4F135(L_123, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadVector4_m7870D12D4D86684F68719E7F040A33A085C2F1D4_RuntimeMethod_var), NULL);
		((Reader_1_tFBA7DC5EFAB4785CF16735020B26CD15760FF936_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tFBA7DC5EFAB4785CF16735020B26CD15760FF936_il2cpp_TypeInfo_var))->___read_0 = L_123;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tFBA7DC5EFAB4785CF16735020B26CD15760FF936_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tFBA7DC5EFAB4785CF16735020B26CD15760FF936_il2cpp_TypeInfo_var))->___read_0), (void*)L_123);
		Func_2_t3EF4F45A92179F162ABDFA4C40C59CEAB4DF8C95* L_124 = (Func_2_t3EF4F45A92179F162ABDFA4C40C59CEAB4DF8C95*)il2cpp_codegen_object_new(Func_2_t3EF4F45A92179F162ABDFA4C40C59CEAB4DF8C95_il2cpp_TypeInfo_var);
		NullCheck(L_124);
		Func_2__ctor_mC4084E3AF33665FF7FE3011ABEC4B0600702DEB4(L_124, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadVector4Nullable_m1303DE93C2EB13F32622A8B868B17610B8C4AD09_RuntimeMethod_var), NULL);
		((Reader_1_t6757FC52418E33974D53365FEF9C7406320DED1A_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t6757FC52418E33974D53365FEF9C7406320DED1A_il2cpp_TypeInfo_var))->___read_0 = L_124;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t6757FC52418E33974D53365FEF9C7406320DED1A_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t6757FC52418E33974D53365FEF9C7406320DED1A_il2cpp_TypeInfo_var))->___read_0), (void*)L_124);
		Func_2_t1D54F05297BE0A4ACFBE3D87381C38332A2F7837* L_125 = (Func_2_t1D54F05297BE0A4ACFBE3D87381C38332A2F7837*)il2cpp_codegen_object_new(Func_2_t1D54F05297BE0A4ACFBE3D87381C38332A2F7837_il2cpp_TypeInfo_var);
		NullCheck(L_125);
		Func_2__ctor_m6222FE4CA8B3758CAC98A1ADD440A9F8E7ACF8EC(L_125, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadVector2Int_mC9CEB6A103CD7C5DBCD8A944A57A59C0D1311F25_RuntimeMethod_var), NULL);
		((Reader_1_t39F933887B1FE66BE8404A5FAC7840D47C4CE2F2_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t39F933887B1FE66BE8404A5FAC7840D47C4CE2F2_il2cpp_TypeInfo_var))->___read_0 = L_125;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t39F933887B1FE66BE8404A5FAC7840D47C4CE2F2_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t39F933887B1FE66BE8404A5FAC7840D47C4CE2F2_il2cpp_TypeInfo_var))->___read_0), (void*)L_125);
		Func_2_tE2F89B2F7AD6F1B1ED5373840C57D1603C193D7D* L_126 = (Func_2_tE2F89B2F7AD6F1B1ED5373840C57D1603C193D7D*)il2cpp_codegen_object_new(Func_2_tE2F89B2F7AD6F1B1ED5373840C57D1603C193D7D_il2cpp_TypeInfo_var);
		NullCheck(L_126);
		Func_2__ctor_m94D28186D109C0CB497C56C95938494A13E99BD5(L_126, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadVector2IntNullable_m698F001AADBF901CE9571E3AA5687DFC1DD65701_RuntimeMethod_var), NULL);
		((Reader_1_t637274CA1AF284DD25208101A16558A23B6E67B5_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t637274CA1AF284DD25208101A16558A23B6E67B5_il2cpp_TypeInfo_var))->___read_0 = L_126;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t637274CA1AF284DD25208101A16558A23B6E67B5_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t637274CA1AF284DD25208101A16558A23B6E67B5_il2cpp_TypeInfo_var))->___read_0), (void*)L_126);
		Func_2_tFD61C39BCF897AD7E0777243349B4558DB639EC5* L_127 = (Func_2_tFD61C39BCF897AD7E0777243349B4558DB639EC5*)il2cpp_codegen_object_new(Func_2_tFD61C39BCF897AD7E0777243349B4558DB639EC5_il2cpp_TypeInfo_var);
		NullCheck(L_127);
		Func_2__ctor_m217139F1BECEBC6BBBE9E9EF9EB72AFFCB2F048A(L_127, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadVector3Int_m59BAA3EBC52DB1635EA840D23B9D4A011E480E3F_RuntimeMethod_var), NULL);
		((Reader_1_tACF841391431A7BEF83CACCF4CAF12E6601B5A00_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tACF841391431A7BEF83CACCF4CAF12E6601B5A00_il2cpp_TypeInfo_var))->___read_0 = L_127;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tACF841391431A7BEF83CACCF4CAF12E6601B5A00_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tACF841391431A7BEF83CACCF4CAF12E6601B5A00_il2cpp_TypeInfo_var))->___read_0), (void*)L_127);
		Func_2_tB67E11F122FE23A54C3DC5758CF7ABCED89CA2B9* L_128 = (Func_2_tB67E11F122FE23A54C3DC5758CF7ABCED89CA2B9*)il2cpp_codegen_object_new(Func_2_tB67E11F122FE23A54C3DC5758CF7ABCED89CA2B9_il2cpp_TypeInfo_var);
		NullCheck(L_128);
		Func_2__ctor_m731CC3B334459200D7A73025EFD53DD1F367904D(L_128, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadVector3IntNullable_mB88012F753982406CA6C49E8440318BFF784AF97_RuntimeMethod_var), NULL);
		((Reader_1_tAF89048D8EB67FD9887BFA7A88596629C237E4CA_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tAF89048D8EB67FD9887BFA7A88596629C237E4CA_il2cpp_TypeInfo_var))->___read_0 = L_128;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tAF89048D8EB67FD9887BFA7A88596629C237E4CA_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tAF89048D8EB67FD9887BFA7A88596629C237E4CA_il2cpp_TypeInfo_var))->___read_0), (void*)L_128);
		Func_2_t6DF06C835E4A0B6416DE67676C67EDBE1EBBFD78* L_129 = (Func_2_t6DF06C835E4A0B6416DE67676C67EDBE1EBBFD78*)il2cpp_codegen_object_new(Func_2_t6DF06C835E4A0B6416DE67676C67EDBE1EBBFD78_il2cpp_TypeInfo_var);
		NullCheck(L_129);
		Func_2__ctor_mAD4F798590659BE2E777E0ACE42340F428825FAD(L_129, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadColor_mC5D200708B20F2ADC42224245960E2ED7E5DD27A_RuntimeMethod_var), NULL);
		((Reader_1_tB18271683F45221F345D04D571881186F2B19615_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tB18271683F45221F345D04D571881186F2B19615_il2cpp_TypeInfo_var))->___read_0 = L_129;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tB18271683F45221F345D04D571881186F2B19615_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tB18271683F45221F345D04D571881186F2B19615_il2cpp_TypeInfo_var))->___read_0), (void*)L_129);
		Func_2_tB85C8267AB8EBEC2E4C056F08CC1288F0C2055B7* L_130 = (Func_2_tB85C8267AB8EBEC2E4C056F08CC1288F0C2055B7*)il2cpp_codegen_object_new(Func_2_tB85C8267AB8EBEC2E4C056F08CC1288F0C2055B7_il2cpp_TypeInfo_var);
		NullCheck(L_130);
		Func_2__ctor_mFDB7A6FE1249299DD1A26AD6EF239EA1914B6FFA(L_130, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadColorNullable_mDA6AADFE45C4CE1364429EACA43199CB319C9065_RuntimeMethod_var), NULL);
		((Reader_1_t5F1A5D57BC2BD2B0AB5EF928D722588060144152_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t5F1A5D57BC2BD2B0AB5EF928D722588060144152_il2cpp_TypeInfo_var))->___read_0 = L_130;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t5F1A5D57BC2BD2B0AB5EF928D722588060144152_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t5F1A5D57BC2BD2B0AB5EF928D722588060144152_il2cpp_TypeInfo_var))->___read_0), (void*)L_130);
		Func_2_tA1D462828AADE4C7A521611177F1A49476CFE773* L_131 = (Func_2_tA1D462828AADE4C7A521611177F1A49476CFE773*)il2cpp_codegen_object_new(Func_2_tA1D462828AADE4C7A521611177F1A49476CFE773_il2cpp_TypeInfo_var);
		NullCheck(L_131);
		Func_2__ctor_m39287045E5A0907A42D35A9EE4A4DF09881759EF(L_131, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadColor32_m0F0066C51CACC736B893D9F3C1D4324F87641BEF_RuntimeMethod_var), NULL);
		((Reader_1_tA1A9ED8F30B3E4079D52171EBCE69E3D54C0E32A_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tA1A9ED8F30B3E4079D52171EBCE69E3D54C0E32A_il2cpp_TypeInfo_var))->___read_0 = L_131;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tA1A9ED8F30B3E4079D52171EBCE69E3D54C0E32A_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tA1A9ED8F30B3E4079D52171EBCE69E3D54C0E32A_il2cpp_TypeInfo_var))->___read_0), (void*)L_131);
		Func_2_t5B94AD2B8545A777AE2428975D92A19545B20228* L_132 = (Func_2_t5B94AD2B8545A777AE2428975D92A19545B20228*)il2cpp_codegen_object_new(Func_2_t5B94AD2B8545A777AE2428975D92A19545B20228_il2cpp_TypeInfo_var);
		NullCheck(L_132);
		Func_2__ctor_mD0E32851A51CE9193117D36268D301E4A08AAEF9(L_132, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadColor32Nullable_m95A7EDB77042A0B8D6D00D2C96E9A530DEA6AF8C_RuntimeMethod_var), NULL);
		((Reader_1_t4EB488AA5A482EE20D6D1D3A5E90B63F672378C7_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t4EB488AA5A482EE20D6D1D3A5E90B63F672378C7_il2cpp_TypeInfo_var))->___read_0 = L_132;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t4EB488AA5A482EE20D6D1D3A5E90B63F672378C7_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t4EB488AA5A482EE20D6D1D3A5E90B63F672378C7_il2cpp_TypeInfo_var))->___read_0), (void*)L_132);
		Func_2_tDF2B4816BD9F902CA19B4AFA6CA78C2A61867EA3* L_133 = (Func_2_tDF2B4816BD9F902CA19B4AFA6CA78C2A61867EA3*)il2cpp_codegen_object_new(Func_2_tDF2B4816BD9F902CA19B4AFA6CA78C2A61867EA3_il2cpp_TypeInfo_var);
		NullCheck(L_133);
		Func_2__ctor_m20AAA57716BA46CE5CE092302B9871E665C8B2DD(L_133, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadQuaternion_m135F5C523703C700E6A266DA9718E44D160BB567_RuntimeMethod_var), NULL);
		((Reader_1_tF4BD915B07DF8D3A5EB55BCAAD572DEE9ECE8FB5_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tF4BD915B07DF8D3A5EB55BCAAD572DEE9ECE8FB5_il2cpp_TypeInfo_var))->___read_0 = L_133;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tF4BD915B07DF8D3A5EB55BCAAD572DEE9ECE8FB5_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tF4BD915B07DF8D3A5EB55BCAAD572DEE9ECE8FB5_il2cpp_TypeInfo_var))->___read_0), (void*)L_133);
		Func_2_t643EFCAEA913218C33F9E05144DAB3702712A18C* L_134 = (Func_2_t643EFCAEA913218C33F9E05144DAB3702712A18C*)il2cpp_codegen_object_new(Func_2_t643EFCAEA913218C33F9E05144DAB3702712A18C_il2cpp_TypeInfo_var);
		NullCheck(L_134);
		Func_2__ctor_m381A9AE2DFC9AC217CE781A47C3BDEDE755C44C9(L_134, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadQuaternionNullable_mE4E31E56C486837C0EC9C6047B276C9452D02C9D_RuntimeMethod_var), NULL);
		((Reader_1_t41E7ACDBED757B1830FB93B6F8CBF75F959CD886_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t41E7ACDBED757B1830FB93B6F8CBF75F959CD886_il2cpp_TypeInfo_var))->___read_0 = L_134;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t41E7ACDBED757B1830FB93B6F8CBF75F959CD886_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t41E7ACDBED757B1830FB93B6F8CBF75F959CD886_il2cpp_TypeInfo_var))->___read_0), (void*)L_134);
		Func_2_t340EA86AEF734F68ACF5E9FF2A124F9ADDED52DE* L_135 = (Func_2_t340EA86AEF734F68ACF5E9FF2A124F9ADDED52DE*)il2cpp_codegen_object_new(Func_2_t340EA86AEF734F68ACF5E9FF2A124F9ADDED52DE_il2cpp_TypeInfo_var);
		NullCheck(L_135);
		Func_2__ctor_m4854C63996A46CD4AD4F9245B66996B869921F1F(L_135, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadRect_mA4B7FDD8840C7E3A299614815C36EFB27232AB3C_RuntimeMethod_var), NULL);
		((Reader_1_t6B88F366A149F8A58032F93153EF0A2DFC60E74F_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t6B88F366A149F8A58032F93153EF0A2DFC60E74F_il2cpp_TypeInfo_var))->___read_0 = L_135;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t6B88F366A149F8A58032F93153EF0A2DFC60E74F_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t6B88F366A149F8A58032F93153EF0A2DFC60E74F_il2cpp_TypeInfo_var))->___read_0), (void*)L_135);
		Func_2_tCD175F70DBC690C8EF0231BF4AF1180C50BE25E0* L_136 = (Func_2_tCD175F70DBC690C8EF0231BF4AF1180C50BE25E0*)il2cpp_codegen_object_new(Func_2_tCD175F70DBC690C8EF0231BF4AF1180C50BE25E0_il2cpp_TypeInfo_var);
		NullCheck(L_136);
		Func_2__ctor_mED004AEBC522BA1D0E91B10B3B29E75B0A323A81(L_136, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadRectNullable_m58AF30FAB6E523648BA18026AD02B220FBDDBC85_RuntimeMethod_var), NULL);
		((Reader_1_t936C97C09526A419E67C5F81F13C53B05B727833_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t936C97C09526A419E67C5F81F13C53B05B727833_il2cpp_TypeInfo_var))->___read_0 = L_136;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t936C97C09526A419E67C5F81F13C53B05B727833_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t936C97C09526A419E67C5F81F13C53B05B727833_il2cpp_TypeInfo_var))->___read_0), (void*)L_136);
		Func_2_tF1646D6248B9372FDA9C49F858144441B68F98FD* L_137 = (Func_2_tF1646D6248B9372FDA9C49F858144441B68F98FD*)il2cpp_codegen_object_new(Func_2_tF1646D6248B9372FDA9C49F858144441B68F98FD_il2cpp_TypeInfo_var);
		NullCheck(L_137);
		Func_2__ctor_mF1D4E3B7C5E6B42EA0B6D65FB8C0019AAB1D0DE7(L_137, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadPlane_m2DA9573A8252F9B24A10E9E1AB448976D9963B96_RuntimeMethod_var), NULL);
		((Reader_1_t9999666C4A090A6C9AD43987D46A3326989F2E39_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t9999666C4A090A6C9AD43987D46A3326989F2E39_il2cpp_TypeInfo_var))->___read_0 = L_137;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t9999666C4A090A6C9AD43987D46A3326989F2E39_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t9999666C4A090A6C9AD43987D46A3326989F2E39_il2cpp_TypeInfo_var))->___read_0), (void*)L_137);
		Func_2_t93378D91D2D47434F10E8104D47A5A2363B48D08* L_138 = (Func_2_t93378D91D2D47434F10E8104D47A5A2363B48D08*)il2cpp_codegen_object_new(Func_2_t93378D91D2D47434F10E8104D47A5A2363B48D08_il2cpp_TypeInfo_var);
		NullCheck(L_138);
		Func_2__ctor_m532BA2795A3C3E358A83BEF75171D6D43EF6AB61(L_138, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadPlaneNullable_m252E55444808DDA4A5CEBCBE440E34728ECA5120_RuntimeMethod_var), NULL);
		((Reader_1_t0CB918AF0F66B31D68CF91A09F89A19D747B6B71_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t0CB918AF0F66B31D68CF91A09F89A19D747B6B71_il2cpp_TypeInfo_var))->___read_0 = L_138;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t0CB918AF0F66B31D68CF91A09F89A19D747B6B71_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t0CB918AF0F66B31D68CF91A09F89A19D747B6B71_il2cpp_TypeInfo_var))->___read_0), (void*)L_138);
		Func_2_tB124603C647DAC5696CA392A20B3EE6C213CCA26* L_139 = (Func_2_tB124603C647DAC5696CA392A20B3EE6C213CCA26*)il2cpp_codegen_object_new(Func_2_tB124603C647DAC5696CA392A20B3EE6C213CCA26_il2cpp_TypeInfo_var);
		NullCheck(L_139);
		Func_2__ctor_m80F47435B96AA1788E79B6988B6216C2318CCA90(L_139, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadRay_mBE12F756FAAA9395B88F69C6A43F8576921AB20C_RuntimeMethod_var), NULL);
		((Reader_1_t88B56AF4E9141686F1DE1F3D155CB3FBA34B913D_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t88B56AF4E9141686F1DE1F3D155CB3FBA34B913D_il2cpp_TypeInfo_var))->___read_0 = L_139;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t88B56AF4E9141686F1DE1F3D155CB3FBA34B913D_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t88B56AF4E9141686F1DE1F3D155CB3FBA34B913D_il2cpp_TypeInfo_var))->___read_0), (void*)L_139);
		Func_2_t4467783C00F409FFB8ED1FDCE87EF974AB372C4D* L_140 = (Func_2_t4467783C00F409FFB8ED1FDCE87EF974AB372C4D*)il2cpp_codegen_object_new(Func_2_t4467783C00F409FFB8ED1FDCE87EF974AB372C4D_il2cpp_TypeInfo_var);
		NullCheck(L_140);
		Func_2__ctor_m98E8F4CF0D573CA4AD26355795FDFDAF15C5470F(L_140, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadRayNullable_mEE7ECB615AEFA818E73B366F681EA86595CA8F19_RuntimeMethod_var), NULL);
		((Reader_1_t526D37680AAEFF5888DD8FAF5DE39A4018267FDB_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t526D37680AAEFF5888DD8FAF5DE39A4018267FDB_il2cpp_TypeInfo_var))->___read_0 = L_140;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t526D37680AAEFF5888DD8FAF5DE39A4018267FDB_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t526D37680AAEFF5888DD8FAF5DE39A4018267FDB_il2cpp_TypeInfo_var))->___read_0), (void*)L_140);
		Func_2_tAAC77BA3AEC5902F635E82ED90239D6BCD31F70A* L_141 = (Func_2_tAAC77BA3AEC5902F635E82ED90239D6BCD31F70A*)il2cpp_codegen_object_new(Func_2_tAAC77BA3AEC5902F635E82ED90239D6BCD31F70A_il2cpp_TypeInfo_var);
		NullCheck(L_141);
		Func_2__ctor_m814C6DAB91B9E1D8C0B44552EAC9D7A765571E7E(L_141, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadMatrix4x4_mBB21ACB1A8610F3813CE4A37DBF1608CA31A0E2C_RuntimeMethod_var), NULL);
		((Reader_1_t4AF495607AC4510AD0529F12D6A0BB0418C352A7_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t4AF495607AC4510AD0529F12D6A0BB0418C352A7_il2cpp_TypeInfo_var))->___read_0 = L_141;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t4AF495607AC4510AD0529F12D6A0BB0418C352A7_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t4AF495607AC4510AD0529F12D6A0BB0418C352A7_il2cpp_TypeInfo_var))->___read_0), (void*)L_141);
		Func_2_t4F5890FDEA23094C33678E54A33233DBBC85F64B* L_142 = (Func_2_t4F5890FDEA23094C33678E54A33233DBBC85F64B*)il2cpp_codegen_object_new(Func_2_t4F5890FDEA23094C33678E54A33233DBBC85F64B_il2cpp_TypeInfo_var);
		NullCheck(L_142);
		Func_2__ctor_m5DD5911338E6A07FE8B87A49AD6DB7DEE40727CE(L_142, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadMatrix4x4Nullable_m508241752BEC24CCF4BD45230613444685553D06_RuntimeMethod_var), NULL);
		((Reader_1_t8CE8050417CA35F7B7ED704A952421B24A331DFB_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t8CE8050417CA35F7B7ED704A952421B24A331DFB_il2cpp_TypeInfo_var))->___read_0 = L_142;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t8CE8050417CA35F7B7ED704A952421B24A331DFB_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t8CE8050417CA35F7B7ED704A952421B24A331DFB_il2cpp_TypeInfo_var))->___read_0), (void*)L_142);
		Func_2_tEC73485627298AE849634B626697EFC64D213D43* L_143 = (Func_2_tEC73485627298AE849634B626697EFC64D213D43*)il2cpp_codegen_object_new(Func_2_tEC73485627298AE849634B626697EFC64D213D43_il2cpp_TypeInfo_var);
		NullCheck(L_143);
		Func_2__ctor_m242227B1E3EA30328974365FD5F1A854F3000F9F(L_143, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadGuid_mCFFAB7379132286F7C9CC70EC291F8B28EA08B0E_RuntimeMethod_var), NULL);
		((Reader_1_t84A9F65298C551CBBA05DF543DBED0468412CCD3_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t84A9F65298C551CBBA05DF543DBED0468412CCD3_il2cpp_TypeInfo_var))->___read_0 = L_143;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t84A9F65298C551CBBA05DF543DBED0468412CCD3_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t84A9F65298C551CBBA05DF543DBED0468412CCD3_il2cpp_TypeInfo_var))->___read_0), (void*)L_143);
		Func_2_tDCE62FBEE709378CCBDB8A5F059E3D9EB17469CB* L_144 = (Func_2_tDCE62FBEE709378CCBDB8A5F059E3D9EB17469CB*)il2cpp_codegen_object_new(Func_2_tDCE62FBEE709378CCBDB8A5F059E3D9EB17469CB_il2cpp_TypeInfo_var);
		NullCheck(L_144);
		Func_2__ctor_m6B8AECE3E335F2AAEB7318EADA50E566859DD177(L_144, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadGuidNullable_m884FD11E39BB14010073AB443D46779317340927_RuntimeMethod_var), NULL);
		((Reader_1_tB498EAF4882CB62E4E9E943CFD2165144F1AD05B_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tB498EAF4882CB62E4E9E943CFD2165144F1AD05B_il2cpp_TypeInfo_var))->___read_0 = L_144;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tB498EAF4882CB62E4E9E943CFD2165144F1AD05B_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tB498EAF4882CB62E4E9E943CFD2165144F1AD05B_il2cpp_TypeInfo_var))->___read_0), (void*)L_144);
		Func_2_t8D8F451E2C46B99D55F5781D36B4F981BE62BD6A* L_145 = (Func_2_t8D8F451E2C46B99D55F5781D36B4F981BE62BD6A*)il2cpp_codegen_object_new(Func_2_t8D8F451E2C46B99D55F5781D36B4F981BE62BD6A_il2cpp_TypeInfo_var);
		NullCheck(L_145);
		Func_2__ctor_m04DBF2925BC7EC91E116CE5BAAF1DEAF9177FD34(L_145, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadNetworkIdentity_mFDB6779F9A77F88F9760FD9902EFFDF3331E62AE_RuntimeMethod_var), NULL);
		((Reader_1_t2C531D10760B34519D87218F56AE3866A9C6B924_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t2C531D10760B34519D87218F56AE3866A9C6B924_il2cpp_TypeInfo_var))->___read_0 = L_145;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t2C531D10760B34519D87218F56AE3866A9C6B924_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t2C531D10760B34519D87218F56AE3866A9C6B924_il2cpp_TypeInfo_var))->___read_0), (void*)L_145);
		Func_2_t763657E22AECC9ED46856683B3045624CF6351E6* L_146 = (Func_2_t763657E22AECC9ED46856683B3045624CF6351E6*)il2cpp_codegen_object_new(Func_2_t763657E22AECC9ED46856683B3045624CF6351E6_il2cpp_TypeInfo_var);
		NullCheck(L_146);
		Func_2__ctor_m2F8D8CF0B14913832CE43E52FDD07F8A6E42AC5C(L_146, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadNetworkBehaviour_m6D724C97DE822B84C3FF75E80DA169D7C44E5E0B_RuntimeMethod_var), NULL);
		((Reader_1_t497575125F4FF1F1EEB32CB0C8FF12569655666B_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t497575125F4FF1F1EEB32CB0C8FF12569655666B_il2cpp_TypeInfo_var))->___read_0 = L_146;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t497575125F4FF1F1EEB32CB0C8FF12569655666B_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t497575125F4FF1F1EEB32CB0C8FF12569655666B_il2cpp_TypeInfo_var))->___read_0), (void*)L_146);
		Func_2_t842043449CFBC0F4C127867ED6917B5566AC5576* L_147 = (Func_2_t842043449CFBC0F4C127867ED6917B5566AC5576*)il2cpp_codegen_object_new(Func_2_t842043449CFBC0F4C127867ED6917B5566AC5576_il2cpp_TypeInfo_var);
		NullCheck(L_147);
		Func_2__ctor_m560EA5A4E1183E0F54F8811384EB83438520FC48(L_147, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadNetworkBehaviourSyncVar_mD3DCF91C73BB12C70E487EA4C4C85EAC62FE8A1A_RuntimeMethod_var), NULL);
		((Reader_1_tA5BF854AFBD00B4F60644779FA6DEB2DC22F7F61_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tA5BF854AFBD00B4F60644779FA6DEB2DC22F7F61_il2cpp_TypeInfo_var))->___read_0 = L_147;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tA5BF854AFBD00B4F60644779FA6DEB2DC22F7F61_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tA5BF854AFBD00B4F60644779FA6DEB2DC22F7F61_il2cpp_TypeInfo_var))->___read_0), (void*)L_147);
		Func_2_t8085A3B2562300C528C41159E557B58E555D6798* L_148 = (Func_2_t8085A3B2562300C528C41159E557B58E555D6798*)il2cpp_codegen_object_new(Func_2_t8085A3B2562300C528C41159E557B58E555D6798_il2cpp_TypeInfo_var);
		NullCheck(L_148);
		Func_2__ctor_mF133B0268455362C2FDDF6BE242D881C138975E6(L_148, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadTransform_m56C2AB03C3891F0A72C1FC7153655E7AE4DCD6E8_RuntimeMethod_var), NULL);
		((Reader_1_t5AAF2D6763D2C4497F18AC3DE6C04568FBA25219_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t5AAF2D6763D2C4497F18AC3DE6C04568FBA25219_il2cpp_TypeInfo_var))->___read_0 = L_148;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t5AAF2D6763D2C4497F18AC3DE6C04568FBA25219_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t5AAF2D6763D2C4497F18AC3DE6C04568FBA25219_il2cpp_TypeInfo_var))->___read_0), (void*)L_148);
		Func_2_t1E82FEEB6D88844359FA37AD374ACC1AA39DDC4A* L_149 = (Func_2_t1E82FEEB6D88844359FA37AD374ACC1AA39DDC4A*)il2cpp_codegen_object_new(Func_2_t1E82FEEB6D88844359FA37AD374ACC1AA39DDC4A_il2cpp_TypeInfo_var);
		NullCheck(L_149);
		Func_2__ctor_m87C3A95AEDA3A817E2F7CD434A823DD7F93479D3(L_149, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadGameObject_m037E8EDDA39F95DA70EE3226939F677F9E3A2EBD_RuntimeMethod_var), NULL);
		((Reader_1_tE674AC89FB334BAAB07744B62BEB917F65466F8B_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tE674AC89FB334BAAB07744B62BEB917F65466F8B_il2cpp_TypeInfo_var))->___read_0 = L_149;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tE674AC89FB334BAAB07744B62BEB917F65466F8B_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tE674AC89FB334BAAB07744B62BEB917F65466F8B_il2cpp_TypeInfo_var))->___read_0), (void*)L_149);
		Func_2_t2DBAA86C74F086DBA285C7B8ECE41C9F35E419D4* L_150 = (Func_2_t2DBAA86C74F086DBA285C7B8ECE41C9F35E419D4*)il2cpp_codegen_object_new(Func_2_t2DBAA86C74F086DBA285C7B8ECE41C9F35E419D4_il2cpp_TypeInfo_var);
		NullCheck(L_150);
		Func_2__ctor_m2030BB6181363ECFAFF85456FCD23A59C2784D3A(L_150, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadUri_m9CB721F84C66F0749E586B02C4CE8E472F266C06_RuntimeMethod_var), NULL);
		((Reader_1_t1EC17F900AFBDD0AA0263D3A62B0630B495FE255_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t1EC17F900AFBDD0AA0263D3A62B0630B495FE255_il2cpp_TypeInfo_var))->___read_0 = L_150;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t1EC17F900AFBDD0AA0263D3A62B0630B495FE255_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t1EC17F900AFBDD0AA0263D3A62B0630B495FE255_il2cpp_TypeInfo_var))->___read_0), (void*)L_150);
		Func_2_t58133EDD30520660CD4F542594E8D913BB704B55* L_151 = (Func_2_t58133EDD30520660CD4F542594E8D913BB704B55*)il2cpp_codegen_object_new(Func_2_t58133EDD30520660CD4F542594E8D913BB704B55_il2cpp_TypeInfo_var);
		NullCheck(L_151);
		Func_2__ctor_mB8990ED204A2B979A83499D899C19A1B631F451F(L_151, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadTexture2D_m5795D8D017B66A5ED4BDE243E306BD2B77A35EDC_RuntimeMethod_var), NULL);
		((Reader_1_t8CFA4E86EAB3C0AE3848A8BC147CB1A393CB3B50_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t8CFA4E86EAB3C0AE3848A8BC147CB1A393CB3B50_il2cpp_TypeInfo_var))->___read_0 = L_151;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t8CFA4E86EAB3C0AE3848A8BC147CB1A393CB3B50_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t8CFA4E86EAB3C0AE3848A8BC147CB1A393CB3B50_il2cpp_TypeInfo_var))->___read_0), (void*)L_151);
		Func_2_tB35F19D43C3D74BFE4F16B609D5DAFBCCE477EC1* L_152 = (Func_2_tB35F19D43C3D74BFE4F16B609D5DAFBCCE477EC1*)il2cpp_codegen_object_new(Func_2_tB35F19D43C3D74BFE4F16B609D5DAFBCCE477EC1_il2cpp_TypeInfo_var);
		NullCheck(L_152);
		Func_2__ctor_m78CF1B401D3C9F0556D621AD8D8DF55E4770CACB(L_152, NULL, (intptr_t)((void*)NetworkReaderExtensions_ReadSprite_mA5B19DCF570BA845B63AA79858FCFDF27DEAE040_RuntimeMethod_var), NULL);
		((Reader_1_t65AE09028DBFFFB995B18BC7147A9A651D389384_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t65AE09028DBFFFB995B18BC7147A9A651D389384_il2cpp_TypeInfo_var))->___read_0 = L_152;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t65AE09028DBFFFB995B18BC7147A9A651D389384_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t65AE09028DBFFFB995B18BC7147A9A651D389384_il2cpp_TypeInfo_var))->___read_0), (void*)L_152);
		Func_2_t31743D3E7DDCFE0001801AD8A7459B2FE016E401* L_153 = (Func_2_t31743D3E7DDCFE0001801AD8A7459B2FE016E401*)il2cpp_codegen_object_new(Func_2_t31743D3E7DDCFE0001801AD8A7459B2FE016E401_il2cpp_TypeInfo_var);
		NullCheck(L_153);
		Func_2__ctor_m9E0BBEDE74B5D75035A52EA54E0C47542C604EBE(L_153, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_ReadyMessage_m39CE2D271EF006C1BF90E631FE38A0509A2A7174_RuntimeMethod_var), NULL);
		((Reader_1_t00DA01ACAF536E0AF78C6E8BC015679E9934DB88_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t00DA01ACAF536E0AF78C6E8BC015679E9934DB88_il2cpp_TypeInfo_var))->___read_0 = L_153;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t00DA01ACAF536E0AF78C6E8BC015679E9934DB88_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t00DA01ACAF536E0AF78C6E8BC015679E9934DB88_il2cpp_TypeInfo_var))->___read_0), (void*)L_153);
		Func_2_t9B160232C1403CEF10F27D1D1BE4368650EE22CD* L_154 = (Func_2_t9B160232C1403CEF10F27D1D1BE4368650EE22CD*)il2cpp_codegen_object_new(Func_2_t9B160232C1403CEF10F27D1D1BE4368650EE22CD_il2cpp_TypeInfo_var);
		NullCheck(L_154);
		Func_2__ctor_mDFEC253B59A6F180A481F8F9CB18D974D46C3620(L_154, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m208EB5BA02CD99D1236C15A7B3E6C13E0935702B_RuntimeMethod_var), NULL);
		((Reader_1_t4BEAA8B6A674DE41A4F74F8F9F7628DAEB444E28_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t4BEAA8B6A674DE41A4F74F8F9F7628DAEB444E28_il2cpp_TypeInfo_var))->___read_0 = L_154;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t4BEAA8B6A674DE41A4F74F8F9F7628DAEB444E28_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t4BEAA8B6A674DE41A4F74F8F9F7628DAEB444E28_il2cpp_TypeInfo_var))->___read_0), (void*)L_154);
		Func_2_tC36B775425D425FC2AF1D16C0CD70386004FFEA0* L_155 = (Func_2_tC36B775425D425FC2AF1D16C0CD70386004FFEA0*)il2cpp_codegen_object_new(Func_2_tC36B775425D425FC2AF1D16C0CD70386004FFEA0_il2cpp_TypeInfo_var);
		NullCheck(L_155);
		Func_2__ctor_m32B293DC530099CEB8C5AD22D251B39D2F37B303(L_155, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_m7C5BA3DA8EFBFCD37258B6FCA8B0DD3DAC4DC9B3_RuntimeMethod_var), NULL);
		((Reader_1_t2FDE27056CF1FB6DE71EBDBFE9EA615E76F9B215_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t2FDE27056CF1FB6DE71EBDBFE9EA615E76F9B215_il2cpp_TypeInfo_var))->___read_0 = L_155;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t2FDE27056CF1FB6DE71EBDBFE9EA615E76F9B215_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t2FDE27056CF1FB6DE71EBDBFE9EA615E76F9B215_il2cpp_TypeInfo_var))->___read_0), (void*)L_155);
		Func_2_t8C43EB67A953E2DA4EA0F861438FEF6C6F87360E* L_156 = (Func_2_t8C43EB67A953E2DA4EA0F861438FEF6C6F87360E*)il2cpp_codegen_object_new(Func_2_t8C43EB67A953E2DA4EA0F861438FEF6C6F87360E_il2cpp_TypeInfo_var);
		NullCheck(L_156);
		Func_2__ctor_m7CCABC0E5B392E1AFBF33C570F01C80C4A422F97(L_156, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_SceneMessage_m5E357A51D411045B49C0B390BBBEEA981F754A43_RuntimeMethod_var), NULL);
		((Reader_1_t4135AEE9236AC48C3B0D8C260D3A2DC89E212394_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t4135AEE9236AC48C3B0D8C260D3A2DC89E212394_il2cpp_TypeInfo_var))->___read_0 = L_156;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t4135AEE9236AC48C3B0D8C260D3A2DC89E212394_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t4135AEE9236AC48C3B0D8C260D3A2DC89E212394_il2cpp_TypeInfo_var))->___read_0), (void*)L_156);
		Func_2_tA736A0330DE6964C91228A379FAB411414BBBB31* L_157 = (Func_2_tA736A0330DE6964C91228A379FAB411414BBBB31*)il2cpp_codegen_object_new(Func_2_tA736A0330DE6964C91228A379FAB411414BBBB31_il2cpp_TypeInfo_var);
		NullCheck(L_157);
		Func_2__ctor_m4FE270A4DB67D3D0622934DD44DE88672FD1C700(L_157, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_SceneOperation_m74E8C7FCF4974E3652B0B512F68390584B1B3DEF_RuntimeMethod_var), NULL);
		((Reader_1_t6FC28304AD44844C4A3BFD56F3139BA2B74B01FF_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t6FC28304AD44844C4A3BFD56F3139BA2B74B01FF_il2cpp_TypeInfo_var))->___read_0 = L_157;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t6FC28304AD44844C4A3BFD56F3139BA2B74B01FF_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t6FC28304AD44844C4A3BFD56F3139BA2B74B01FF_il2cpp_TypeInfo_var))->___read_0), (void*)L_157);
		Func_2_tDB9FD64DF7D2CA31FFF8279F26075DFCF440D33C* L_158 = (Func_2_tDB9FD64DF7D2CA31FFF8279F26075DFCF440D33C*)il2cpp_codegen_object_new(Func_2_tDB9FD64DF7D2CA31FFF8279F26075DFCF440D33C_il2cpp_TypeInfo_var);
		NullCheck(L_158);
		Func_2__ctor_mFC452C2FE1CDD30101156FFD8D93FC224FB98087(L_158, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_CommandMessage_m1EB0BBD20B46C4D33A437C5D4CF2D8EA43F18A8B_RuntimeMethod_var), NULL);
		((Reader_1_t92A37B251D88CED6738CA5720BFA048525E94646_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t92A37B251D88CED6738CA5720BFA048525E94646_il2cpp_TypeInfo_var))->___read_0 = L_158;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t92A37B251D88CED6738CA5720BFA048525E94646_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t92A37B251D88CED6738CA5720BFA048525E94646_il2cpp_TypeInfo_var))->___read_0), (void*)L_158);
		Func_2_t5F7A34ACC37197D2AFFFFF3A63EE51E2B44D6607* L_159 = (Func_2_t5F7A34ACC37197D2AFFFFF3A63EE51E2B44D6607*)il2cpp_codegen_object_new(Func_2_t5F7A34ACC37197D2AFFFFF3A63EE51E2B44D6607_il2cpp_TypeInfo_var);
		NullCheck(L_159);
		Func_2__ctor_mC9BA73108B1E7CD5DC2190361856DDE258C25FBC(L_159, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_RpcMessage_m4FC20C673517D44D58414A7AA047AF7D75A377C4_RuntimeMethod_var), NULL);
		((Reader_1_t33986420F424835F9DB93EEB52C71E46AB50C09F_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t33986420F424835F9DB93EEB52C71E46AB50C09F_il2cpp_TypeInfo_var))->___read_0 = L_159;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t33986420F424835F9DB93EEB52C71E46AB50C09F_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t33986420F424835F9DB93EEB52C71E46AB50C09F_il2cpp_TypeInfo_var))->___read_0), (void*)L_159);
		Func_2_t1FE35EF0C4BBDC7AFA697EBBB5B4D64547326DCC* L_160 = (Func_2_t1FE35EF0C4BBDC7AFA697EBBB5B4D64547326DCC*)il2cpp_codegen_object_new(Func_2_t1FE35EF0C4BBDC7AFA697EBBB5B4D64547326DCC_il2cpp_TypeInfo_var);
		NullCheck(L_160);
		Func_2__ctor_mA5D72440406261E5883E58D5A6D1B3A96BF68ABF(L_160, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_SpawnMessage_mEC8E19BCC23380500B3605B78866DDBEA4C904EE_RuntimeMethod_var), NULL);
		((Reader_1_tBE1B76CC86F1EDCE1B7A3203682971C129964518_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tBE1B76CC86F1EDCE1B7A3203682971C129964518_il2cpp_TypeInfo_var))->___read_0 = L_160;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tBE1B76CC86F1EDCE1B7A3203682971C129964518_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tBE1B76CC86F1EDCE1B7A3203682971C129964518_il2cpp_TypeInfo_var))->___read_0), (void*)L_160);
		Func_2_tAF26F2C22D87F8F669162B6542990070635D2A30* L_161 = (Func_2_tAF26F2C22D87F8F669162B6542990070635D2A30*)il2cpp_codegen_object_new(Func_2_tAF26F2C22D87F8F669162B6542990070635D2A30_il2cpp_TypeInfo_var);
		NullCheck(L_161);
		Func_2__ctor_mE75CD4D3A38684C2195915A8435DF2D7579C3B2C(L_161, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m1ED249926C5FFFF3711276C020ABE54B928BAAEE_RuntimeMethod_var), NULL);
		((Reader_1_tB503DFFAE8032B3381FEE0779316357D1CE28658_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tB503DFFAE8032B3381FEE0779316357D1CE28658_il2cpp_TypeInfo_var))->___read_0 = L_161;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tB503DFFAE8032B3381FEE0779316357D1CE28658_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tB503DFFAE8032B3381FEE0779316357D1CE28658_il2cpp_TypeInfo_var))->___read_0), (void*)L_161);
		Func_2_tBD4C61172F81BE2A292FAE166832A0324BB00B1B* L_162 = (Func_2_tBD4C61172F81BE2A292FAE166832A0324BB00B1B*)il2cpp_codegen_object_new(Func_2_tBD4C61172F81BE2A292FAE166832A0324BB00B1B_il2cpp_TypeInfo_var);
		NullCheck(L_162);
		Func_2__ctor_mDF071593EB218CD46A09DBB869CFE900C7DE702F(L_162, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_m7236240B1388E3E63F1E45767148C047F2B86681_RuntimeMethod_var), NULL);
		((Reader_1_t5072BFCCD801A3F27F8B16872FF18C70016F2CC0_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t5072BFCCD801A3F27F8B16872FF18C70016F2CC0_il2cpp_TypeInfo_var))->___read_0 = L_162;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t5072BFCCD801A3F27F8B16872FF18C70016F2CC0_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t5072BFCCD801A3F27F8B16872FF18C70016F2CC0_il2cpp_TypeInfo_var))->___read_0), (void*)L_162);
		Func_2_tAE6F8E9F1706B23DD7BC49D738569171CF6E8C3B* L_163 = (Func_2_tAE6F8E9F1706B23DD7BC49D738569171CF6E8C3B*)il2cpp_codegen_object_new(Func_2_tAE6F8E9F1706B23DD7BC49D738569171CF6E8C3B_il2cpp_TypeInfo_var);
		NullCheck(L_163);
		Func_2__ctor_mC1FF94D326E4383A2ED9B12FE9BA7C6B5D689DF7(L_163, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_m9DCF582A7F2ACD58B46CE0B5006DC8DF925AAB07_RuntimeMethod_var), NULL);
		((Reader_1_t6B6A1FCF933F958098FE25A100627E7241FEB588_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t6B6A1FCF933F958098FE25A100627E7241FEB588_il2cpp_TypeInfo_var))->___read_0 = L_163;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t6B6A1FCF933F958098FE25A100627E7241FEB588_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t6B6A1FCF933F958098FE25A100627E7241FEB588_il2cpp_TypeInfo_var))->___read_0), (void*)L_163);
		Func_2_t056E7181DAB40DF2A9D14BB4450C0DCC566784E6* L_164 = (Func_2_t056E7181DAB40DF2A9D14BB4450C0DCC566784E6*)il2cpp_codegen_object_new(Func_2_t056E7181DAB40DF2A9D14BB4450C0DCC566784E6_il2cpp_TypeInfo_var);
		NullCheck(L_164);
		Func_2__ctor_m5C824E4C4CABDCECD70454ABB6854F61676803A6(L_164, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_m1805A0712B400427B3DA28B6B83D2DB7BA47184C_RuntimeMethod_var), NULL);
		((Reader_1_tCB389B7DC3DE02BC9C35A7B57D2B65F26E3A7C60_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tCB389B7DC3DE02BC9C35A7B57D2B65F26E3A7C60_il2cpp_TypeInfo_var))->___read_0 = L_164;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tCB389B7DC3DE02BC9C35A7B57D2B65F26E3A7C60_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tCB389B7DC3DE02BC9C35A7B57D2B65F26E3A7C60_il2cpp_TypeInfo_var))->___read_0), (void*)L_164);
		Func_2_t76221365B0738498867CB728129555B7A8617C15* L_165 = (Func_2_t76221365B0738498867CB728129555B7A8617C15*)il2cpp_codegen_object_new(Func_2_t76221365B0738498867CB728129555B7A8617C15_il2cpp_TypeInfo_var);
		NullCheck(L_165);
		Func_2__ctor_m2483C821901785CE42A2FFEF511EB1DC50060094(L_165, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_mEFA99EDE85679F1A1946C256D3829EC04CBF8A40_RuntimeMethod_var), NULL);
		((Reader_1_t28D5381047606A72C4A5B7E019C0F77FBCEE1C15_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t28D5381047606A72C4A5B7E019C0F77FBCEE1C15_il2cpp_TypeInfo_var))->___read_0 = L_165;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t28D5381047606A72C4A5B7E019C0F77FBCEE1C15_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t28D5381047606A72C4A5B7E019C0F77FBCEE1C15_il2cpp_TypeInfo_var))->___read_0), (void*)L_165);
		Func_2_tB637CE13E86654599FE7556001525095E552A933* L_166 = (Func_2_tB637CE13E86654599FE7556001525095E552A933*)il2cpp_codegen_object_new(Func_2_tB637CE13E86654599FE7556001525095E552A933_il2cpp_TypeInfo_var);
		NullCheck(L_166);
		Func_2__ctor_m4B3544D9A6F24E37D069A76B2D0BE847126EE92D(L_166, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_EntityStateMessage_mFACC1BD75FFD70F53F10FD27A7EDB7AC60E2DB5D_RuntimeMethod_var), NULL);
		((Reader_1_tCF89C1D29B73DD90DA2BB475B19671460984C68D_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tCF89C1D29B73DD90DA2BB475B19671460984C68D_il2cpp_TypeInfo_var))->___read_0 = L_166;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tCF89C1D29B73DD90DA2BB475B19671460984C68D_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tCF89C1D29B73DD90DA2BB475B19671460984C68D_il2cpp_TypeInfo_var))->___read_0), (void*)L_166);
		Func_2_t3C7B620CCBA5E47EED93AC22DB224F015928D9BF* L_167 = (Func_2_t3C7B620CCBA5E47EED93AC22DB224F015928D9BF*)il2cpp_codegen_object_new(Func_2_t3C7B620CCBA5E47EED93AC22DB224F015928D9BF_il2cpp_TypeInfo_var);
		NullCheck(L_167);
		Func_2__ctor_m774F67BF80330546FD3906729F0DD18536E9D2FA(L_167, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m2AB11C6E07E94D6C3DB059650CCA9BD6D9838F5C_RuntimeMethod_var), NULL);
		((Reader_1_tF4E8B721D496EDF9F9FFF783C41F20BF05DE9F34_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tF4E8B721D496EDF9F9FFF783C41F20BF05DE9F34_il2cpp_TypeInfo_var))->___read_0 = L_167;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tF4E8B721D496EDF9F9FFF783C41F20BF05DE9F34_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tF4E8B721D496EDF9F9FFF783C41F20BF05DE9F34_il2cpp_TypeInfo_var))->___read_0), (void*)L_167);
		Func_2_t57653632E080A4623E41FABED4B1B6707C4D1B44* L_168 = (Func_2_t57653632E080A4623E41FABED4B1B6707C4D1B44*)il2cpp_codegen_object_new(Func_2_t57653632E080A4623E41FABED4B1B6707C4D1B44_il2cpp_TypeInfo_var);
		NullCheck(L_168);
		Func_2__ctor_m05B2B6CD0EBE9546AA45D196A8BB5EDAB7F104B3(L_168, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_mD30EC9CAE30F7761C7D307A8671F27E9D63BAC17_RuntimeMethod_var), NULL);
		((Reader_1_tDC04CC49F13C042AA2FC9CEA968B413D1B6F3F38_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tDC04CC49F13C042AA2FC9CEA968B413D1B6F3F38_il2cpp_TypeInfo_var))->___read_0 = L_168;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tDC04CC49F13C042AA2FC9CEA968B413D1B6F3F38_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tDC04CC49F13C042AA2FC9CEA968B413D1B6F3F38_il2cpp_TypeInfo_var))->___read_0), (void*)L_168);
		Func_2_t2EFEEFDAE61B1D665B02E8D247FA7A8AE1B3D3FF* L_169 = (Func_2_t2EFEEFDAE61B1D665B02E8D247FA7A8AE1B3D3FF*)il2cpp_codegen_object_new(Func_2_t2EFEEFDAE61B1D665B02E8D247FA7A8AE1B3D3FF_il2cpp_TypeInfo_var);
		NullCheck(L_169);
		Func_2__ctor_m09D4615AB8251027E1C3D5AF83D85E140981D689(L_169, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ServerMatchMessage_m45838D1053B8C35602D526DA8A918170A84E882B_RuntimeMethod_var), NULL);
		((Reader_1_tF19F6E8C291C912786327989D4FD0124DF26DB09_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tF19F6E8C291C912786327989D4FD0124DF26DB09_il2cpp_TypeInfo_var))->___read_0 = L_169;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tF19F6E8C291C912786327989D4FD0124DF26DB09_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tF19F6E8C291C912786327989D4FD0124DF26DB09_il2cpp_TypeInfo_var))->___read_0), (void*)L_169);
		Func_2_t4A60C2EC0E8ECFA6E6872E11BFAD744A805D5FF8* L_170 = (Func_2_t4A60C2EC0E8ECFA6E6872E11BFAD744A805D5FF8*)il2cpp_codegen_object_new(Func_2_t4A60C2EC0E8ECFA6E6872E11BFAD744A805D5FF8_il2cpp_TypeInfo_var);
		NullCheck(L_170);
		Func_2__ctor_m50EDBC8444E6CBF1964E72B1CAC2E6DAAF7C86CF(L_170, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ServerMatchOperation_m0E0217A1EA28419B9541B868BB7E160A7F9876C8_RuntimeMethod_var), NULL);
		((Reader_1_t80077764BF20B75B1B8B47491E64EF0F64D019DF_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t80077764BF20B75B1B8B47491E64EF0F64D019DF_il2cpp_TypeInfo_var))->___read_0 = L_170;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t80077764BF20B75B1B8B47491E64EF0F64D019DF_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t80077764BF20B75B1B8B47491E64EF0F64D019DF_il2cpp_TypeInfo_var))->___read_0), (void*)L_170);
		Func_2_t21336B204733D902134B7F0FB71D1D4D85655F8E* L_171 = (Func_2_t21336B204733D902134B7F0FB71D1D4D85655F8E*)il2cpp_codegen_object_new(Func_2_t21336B204733D902134B7F0FB71D1D4D85655F8E_il2cpp_TypeInfo_var);
		NullCheck(L_171);
		Func_2__ctor_m36CF69079E9F30D185E759A619586D8BAE734275(L_171, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ClientMatchMessage_m588555F2CD175A2C40F8045755BF3F659CDB65B4_RuntimeMethod_var), NULL);
		((Reader_1_t286A71B8B0F59FB5063D0E327C76169FAF8F45D6_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t286A71B8B0F59FB5063D0E327C76169FAF8F45D6_il2cpp_TypeInfo_var))->___read_0 = L_171;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t286A71B8B0F59FB5063D0E327C76169FAF8F45D6_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t286A71B8B0F59FB5063D0E327C76169FAF8F45D6_il2cpp_TypeInfo_var))->___read_0), (void*)L_171);
		Func_2_tFBF2D2CE91617E529AC8196465432685D724AEBE* L_172 = (Func_2_tFBF2D2CE91617E529AC8196465432685D724AEBE*)il2cpp_codegen_object_new(Func_2_tFBF2D2CE91617E529AC8196465432685D724AEBE_il2cpp_TypeInfo_var);
		NullCheck(L_172);
		Func_2__ctor_m7C5A0F5C2643DB8BB6EE6C47BFBE77373EA2C09F(L_172, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_ClientMatchOperation_m967DCEAD7E36E480A8496E822CACE1F60AE9AFB9_RuntimeMethod_var), NULL);
		((Reader_1_t23067DF0051C0D5F767FB367AFEEE3C196DC518C_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t23067DF0051C0D5F767FB367AFEEE3C196DC518C_il2cpp_TypeInfo_var))->___read_0 = L_172;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t23067DF0051C0D5F767FB367AFEEE3C196DC518C_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t23067DF0051C0D5F767FB367AFEEE3C196DC518C_il2cpp_TypeInfo_var))->___read_0), (void*)L_172);
		Func_2_tEAEC0CEBCFCC9157E40E1A7FFFEB82D8A082B205* L_173 = (Func_2_tEAEC0CEBCFCC9157E40E1A7FFFEB82D8A082B205*)il2cpp_codegen_object_new(Func_2_tEAEC0CEBCFCC9157E40E1A7FFFEB82D8A082B205_il2cpp_TypeInfo_var);
		NullCheck(L_173);
		Func_2__ctor_mAA8BAF9145D19220900229CD9A074B243A603C85(L_173, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchInfoU5BU5D_mE23CB91C4A5FF33675CF8A56B16BA006EC9047E9_RuntimeMethod_var), NULL);
		((Reader_1_t9C76556BF9E8625EB258DEFF7A1656D33B4B236A_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t9C76556BF9E8625EB258DEFF7A1656D33B4B236A_il2cpp_TypeInfo_var))->___read_0 = L_173;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t9C76556BF9E8625EB258DEFF7A1656D33B4B236A_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t9C76556BF9E8625EB258DEFF7A1656D33B4B236A_il2cpp_TypeInfo_var))->___read_0), (void*)L_173);
		Func_2_t0D870FCF01A0B3EEF980A0B74CCA07765A7552B3* L_174 = (Func_2_t0D870FCF01A0B3EEF980A0B74CCA07765A7552B3*)il2cpp_codegen_object_new(Func_2_t0D870FCF01A0B3EEF980A0B74CCA07765A7552B3_il2cpp_TypeInfo_var);
		NullCheck(L_174);
		Func_2__ctor_mC8512C24ADB03A516688EFABCA57C176C722CE70(L_174, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchInfo_mB709C9D76C0446BF9C8C43CEBFCEC92A0D42551A_RuntimeMethod_var), NULL);
		((Reader_1_t9F020D67D02A1B6BF8EF51B03B6F298B7042AC26_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t9F020D67D02A1B6BF8EF51B03B6F298B7042AC26_il2cpp_TypeInfo_var))->___read_0 = L_174;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t9F020D67D02A1B6BF8EF51B03B6F298B7042AC26_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t9F020D67D02A1B6BF8EF51B03B6F298B7042AC26_il2cpp_TypeInfo_var))->___read_0), (void*)L_174);
		Func_2_t57B5F9E47ED4C8F5611A672DF5C949F2D02972C8* L_175 = (Func_2_t57B5F9E47ED4C8F5611A672DF5C949F2D02972C8*)il2cpp_codegen_object_new(Func_2_t57B5F9E47ED4C8F5611A672DF5C949F2D02972C8_il2cpp_TypeInfo_var);
		NullCheck(L_175);
		Func_2__ctor_m2119F2CA2CF856D3ED9F162F4D061CC242ECB561(L_175, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_PlayerInfoU5BU5D_mDB000F31EDB9991937FE2C78FE0E3009EF2C1209_RuntimeMethod_var), NULL);
		((Reader_1_tB3752A50A44FC8C286392A80358AFF6F3CE44155_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tB3752A50A44FC8C286392A80358AFF6F3CE44155_il2cpp_TypeInfo_var))->___read_0 = L_175;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tB3752A50A44FC8C286392A80358AFF6F3CE44155_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tB3752A50A44FC8C286392A80358AFF6F3CE44155_il2cpp_TypeInfo_var))->___read_0), (void*)L_175);
		Func_2_t966D11E60D81CE62911BC8F172BA2326E5F49FE0* L_176 = (Func_2_t966D11E60D81CE62911BC8F172BA2326E5F49FE0*)il2cpp_codegen_object_new(Func_2_t966D11E60D81CE62911BC8F172BA2326E5F49FE0_il2cpp_TypeInfo_var);
		NullCheck(L_176);
		Func_2__ctor_m36BCE7C48675E2254A9ACEC67519A94EA81776F6(L_176, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_PlayerInfo_mAAF3F3CF61F837BA88391A854E75733E0F5014F6_RuntimeMethod_var), NULL);
		((Reader_1_t9F0D38236764D927F7809A9EF55BC5E04C385A80_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t9F0D38236764D927F7809A9EF55BC5E04C385A80_il2cpp_TypeInfo_var))->___read_0 = L_176;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t9F0D38236764D927F7809A9EF55BC5E04C385A80_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t9F0D38236764D927F7809A9EF55BC5E04C385A80_il2cpp_TypeInfo_var))->___read_0), (void*)L_176);
		Func_2_t4CD599531AE1FA8CA3CF4057D910B500DBF99E45* L_177 = (Func_2_t4CD599531AE1FA8CA3CF4057D910B500DBF99E45*)il2cpp_codegen_object_new(Func_2_t4CD599531AE1FA8CA3CF4057D910B500DBF99E45_il2cpp_TypeInfo_var);
		NullCheck(L_177);
		Func_2__ctor_mFFB05A516BD11E3B95ABEFDF3D4FAC2BA629F29D(L_177, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_Examples_Chat_ChatAuthenticator_AuthRequestMessage_mA629A2F8CCBFF4C76544ED51BD2A35A5947315EF_RuntimeMethod_var), NULL);
		((Reader_1_t429249FC13D808B0B1401B5B6BD0815F37C082B8_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t429249FC13D808B0B1401B5B6BD0815F37C082B8_il2cpp_TypeInfo_var))->___read_0 = L_177;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t429249FC13D808B0B1401B5B6BD0815F37C082B8_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t429249FC13D808B0B1401B5B6BD0815F37C082B8_il2cpp_TypeInfo_var))->___read_0), (void*)L_177);
		Func_2_t5DF2F494B8936184EE2E2F51E135F530C09B76D0* L_178 = (Func_2_t5DF2F494B8936184EE2E2F51E135F530C09B76D0*)il2cpp_codegen_object_new(Func_2_t5DF2F494B8936184EE2E2F51E135F530C09B76D0_il2cpp_TypeInfo_var);
		NullCheck(L_178);
		Func_2__ctor_mEB3D94E335C7B0856B5C5D4F4742C1CFF9E7CB3B(L_178, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_Examples_Chat_ChatAuthenticator_AuthResponseMessage_m43271020844960E04376F6C896CFE7D9AC33124D_RuntimeMethod_var), NULL);
		((Reader_1_t2A59AD3448B11193AB518EAC5FFE24BA1ED83727_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t2A59AD3448B11193AB518EAC5FFE24BA1ED83727_il2cpp_TypeInfo_var))->___read_0 = L_178;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t2A59AD3448B11193AB518EAC5FFE24BA1ED83727_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t2A59AD3448B11193AB518EAC5FFE24BA1ED83727_il2cpp_TypeInfo_var))->___read_0), (void*)L_178);
		Func_2_t747EDA19750AE43DE30476D128A90ACDBEAC48AF* L_179 = (Func_2_t747EDA19750AE43DE30476D128A90ACDBEAC48AF*)il2cpp_codegen_object_new(Func_2_t747EDA19750AE43DE30476D128A90ACDBEAC48AF_il2cpp_TypeInfo_var);
		NullCheck(L_179);
		Func_2__ctor_m87ACAC5A4D6CCCD69D70361725EE81715F68B42A(L_179, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_MatchPlayerData_m3268F6B9575F69594F5CEE743E0EA6FDF9ED56E3_RuntimeMethod_var), NULL);
		((Reader_1_tFD0BF8A2A5368E4951ED7917611F5A25E4D46A7B_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tFD0BF8A2A5368E4951ED7917611F5A25E4D46A7B_il2cpp_TypeInfo_var))->___read_0 = L_179;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_tFD0BF8A2A5368E4951ED7917611F5A25E4D46A7B_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_tFD0BF8A2A5368E4951ED7917611F5A25E4D46A7B_il2cpp_TypeInfo_var))->___read_0), (void*)L_179);
		Func_2_t22170DBE4CAFFA15631BB8B0CE681A822A3A160A* L_180 = (Func_2_t22170DBE4CAFFA15631BB8B0CE681A822A3A160A*)il2cpp_codegen_object_new(Func_2_t22170DBE4CAFFA15631BB8B0CE681A822A3A160A_il2cpp_TypeInfo_var);
		NullCheck(L_180);
		Func_2__ctor_m76B116CE887E4AF6CDACA82B7696B7A0B33D7417(L_180, NULL, (intptr_t)((void*)GeneratedNetworkCode__Read_Mirror_Examples_MultipleMatch_CellValue_m960AA8E4580568652E48F8810768B4DEA14B34F8_RuntimeMethod_var), NULL);
		((Reader_1_t85795B601B4F9B63136F7F928000E1996B634723_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t85795B601B4F9B63136F7F928000E1996B634723_il2cpp_TypeInfo_var))->___read_0 = L_180;
		Il2CppCodeGenWriteBarrier((void**)(&((Reader_1_t85795B601B4F9B63136F7F928000E1996B634723_StaticFields*)il2cpp_codegen_static_fields_for(Reader_1_t85795B601B4F9B63136F7F928000E1996B634723_il2cpp_TypeInfo_var))->___read_0), (void*)L_180);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color32_op_Implicit_m203A634DBB77053C9400C68065CA29529103D172_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___c0, const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_0 = ___c0;
		uint8_t L_1 = L_0.___r_1;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_2 = ___c0;
		uint8_t L_3 = L_2.___g_2;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_4 = ___c0;
		uint8_t L_5 = L_4.___b_3;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_6 = ___c0;
		uint8_t L_7 = L_6.___a_4;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_8;
		memset((&L_8), 0, sizeof(L_8));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_8), ((float)(((float)L_1)/(255.0f))), ((float)(((float)L_3)/(255.0f))), ((float)(((float)L_5)/(255.0f))), ((float)(((float)L_7)/(255.0f))), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_003d;
	}

IL_003d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Color_op_Equality_m3A255F888F9300ABB36ED2BC0640CFFDAAEFED2F_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___lhs0, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___rhs1, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___lhs0;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_1;
		L_1 = Color_op_Implicit_m6D1353534AD23E43DFD104850D55C469CFCEF340_inline(L_0, NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2 = ___rhs1;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_3;
		L_3 = Color_op_Implicit_m6D1353534AD23E43DFD104850D55C469CFCEF340_inline(L_2, NULL);
		bool L_4;
		L_4 = Vector4_op_Equality_m80E2AA0626A70EF9DCC4F4C215F674A22D6DE937_inline(L_1, L_3, NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B Color32_op_Implicit_m7EFA0B83AD1AE15567E9BC2FA2B8E66D3BFE1395_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___c0, const RuntimeMethod* method) 
{
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___c0;
		float L_1 = L_0.___r_0;
		float L_2;
		L_2 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_1, NULL);
		float L_3;
		L_3 = bankers_roundf(((float)il2cpp_codegen_multiply(L_2, (255.0f))));
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_4 = ___c0;
		float L_5 = L_4.___g_1;
		float L_6;
		L_6 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_5, NULL);
		float L_7;
		L_7 = bankers_roundf(((float)il2cpp_codegen_multiply(L_6, (255.0f))));
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_8 = ___c0;
		float L_9 = L_8.___b_2;
		float L_10;
		L_10 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_9, NULL);
		float L_11;
		L_11 = bankers_roundf(((float)il2cpp_codegen_multiply(L_10, (255.0f))));
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_12 = ___c0;
		float L_13 = L_12.___a_3;
		float L_14;
		L_14 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_13, NULL);
		float L_15;
		L_15 = bankers_roundf(((float)il2cpp_codegen_multiply(L_14, (255.0f))));
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_16;
		memset((&L_16), 0, sizeof(L_16));
		Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline((&L_16), (uint8_t)il2cpp_codegen_cast_floating_point<uint8_t, int32_t, float>(L_3), (uint8_t)il2cpp_codegen_cast_floating_point<uint8_t, int32_t, float>(L_7), (uint8_t)il2cpp_codegen_cast_floating_point<uint8_t, int32_t, float>(L_11), (uint8_t)il2cpp_codegen_cast_floating_point<uint8_t, int32_t, float>(L_15), /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_0065;
	}

IL_0065:
	{
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_17 = V_0;
		return L_17;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteColor32_m123810E64991275156516FBB8AD2CFA67A7C3B7B_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriter_WriteBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mDB472FF359C94951E696CB3FD4F6016CFE6F82B7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static void WriteColor32(this NetworkWriter writer, Color32 value) => writer.WriteBlittable(value);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_1 = ___value1;
		NullCheck(L_0);
		NetworkWriter_WriteBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mDB472FF359C94951E696CB3FD4F6016CFE6F82B7_inline(L_0, L_1, NetworkWriter_WriteBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mDB472FF359C94951E696CB3FD4F6016CFE6F82B7_RuntimeMethod_var);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint64_t NetworkBehaviour_get_syncVarDirtyBits_m96E01E08568BF6521B3B69F8F8F36884CAABEEB4_inline (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, const RuntimeMethod* method) 
{
	{
		// protected ulong syncVarDirtyBits { get; private set; }
		uint64_t L_0 = __this->___U3CsyncVarDirtyBitsU3Ek__BackingField_10;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteULong_mC0AE4801C58209EF02B73E3B353100B3AB95D28C_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint64_t ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriter_WriteBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m869219CA464A4A9CCE03043BF274E8EBD19428AC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static void WriteULong(this NetworkWriter writer, ulong value) => writer.WriteBlittable(value);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		uint64_t L_1 = ___value1;
		NullCheck(L_0);
		NetworkWriter_WriteBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m869219CA464A4A9CCE03043BF274E8EBD19428AC_inline(L_0, L_1, NetworkWriter_WriteBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m869219CA464A4A9CCE03043BF274E8EBD19428AC_RuntimeMethod_var);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B NetworkReaderExtensions_ReadColor32_m0F0066C51CACC736B893D9F3C1D4324F87641BEF_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReader_ReadBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_m6DBD713FC553C165E92A9ABA7691638AA9C2E7BD_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Color32 ReadColor32(this NetworkReader reader) => reader.ReadBlittable<Color32>();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		NullCheck(L_0);
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_1;
		L_1 = NetworkReader_ReadBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_m6DBD713FC553C165E92A9ABA7691638AA9C2E7BD_inline(L_0, NetworkReader_ReadBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_m6DBD713FC553C165E92A9ABA7691638AA9C2E7BD_RuntimeMethod_var);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint64_t NetworkReaderExtensions_ReadULong_mAC6B83521EBA7FFEDFC72A6AAE1BF5D87221A5F5_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReader_ReadBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m5B0E578FA24816AF3240FCA5BFF9353FBB4B958A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static ulong ReadULong(this NetworkReader reader) => reader.ReadBlittable<ulong>();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		NullCheck(L_0);
		uint64_t L_1;
		L_1 = NetworkReader_ReadBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m5B0E578FA24816AF3240FCA5BFF9353FBB4B958A_inline(L_0, NetworkReader_ReadBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m5B0E578FA24816AF3240FCA5BFF9353FBB4B958A_RuntimeMethod_var);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* NetworkReaderExtensions_ReadString_m5A56179D2C1CB509EC9C762F97BBD4133A5AD394_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	uint16_t V_0 = 0;
	int32_t V_1 = 0;
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// ushort size = reader.ReadUShort();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint16_t L_1;
		L_1 = NetworkReaderExtensions_ReadUShort_mA98395DD1B1DA249096858B171B8BC23D95DF765_inline(L_0, NULL);
		V_0 = L_1;
		// if (size == 0)
		uint16_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000c;
		}
	}
	{
		// return null;
		return (String_t*)NULL;
	}

IL_000c:
	{
		// int realSize = size - 1;
		uint16_t L_3 = V_0;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_3, 1));
		// if (realSize >= NetworkWriter.MaxStringLength)
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) < ((int32_t)((int32_t)32768))))
		{
			goto IL_0038;
		}
	}
	{
		// throw new EndOfStreamException($"ReadString too long: {realSize}. Limit is: {NetworkWriter.MaxStringLength}");
		int32_t L_5 = V_1;
		int32_t L_6 = L_5;
		RuntimeObject* L_7 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_6);
		int32_t L_8 = ((int32_t)32768);
		RuntimeObject* L_9 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_8);
		String_t* L_10;
		L_10 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralB7A997A8E0DBFC3D9382D423EC562F92A6E66F5B)), L_7, L_9, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_11 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_11);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_11, L_10, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReaderExtensions_ReadString_m5A56179D2C1CB509EC9C762F97BBD4133A5AD394_RuntimeMethod_var)));
	}

IL_0038:
	{
		// ArraySegment<byte> data = reader.ReadBytesSegment(realSize);
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_12 = ___reader0;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_14;
		L_14 = NetworkReader_ReadBytesSegment_mA17220D13799B7AA2EFF9B49C6F1F98B486A330E_inline(L_12, L_13, NULL);
		V_2 = L_14;
		// return encoding.GetString(data.Array, data.Offset, data.Count);
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		UTF8Encoding_t90B56215A1B0B7ED5CDEA772E695F0DDAFBCD3BE* L_15 = ((NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_StaticFields*)il2cpp_codegen_static_fields_for(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var))->___encoding_0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_16;
		L_16 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline((&V_2), ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		int32_t L_17;
		L_17 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline((&V_2), ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_18;
		L_18 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline((&V_2), ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		NullCheck(L_15);
		String_t* L_19;
		L_19 = VirtualFuncInvoker3< String_t*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_15, L_16, L_17, L_18);
		return L_19;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReader_ReadBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mD424A6F72F431A64491144A4321E8915B2A9F179_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static bool ReadBool(this NetworkReader reader) => reader.ReadBlittable<byte>() != 0;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		NullCheck(L_0);
		uint8_t L_1;
		L_1 = NetworkReader_ReadBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mD424A6F72F431A64491144A4321E8915B2A9F179_inline(L_0, NetworkReader_ReadBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mD424A6F72F431A64491144A4321E8915B2A9F179_RuntimeMethod_var);
		return (bool)((!(((uint32_t)L_1) <= ((uint32_t)0)))? 1 : 0);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint8_t NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReader_ReadBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mD424A6F72F431A64491144A4321E8915B2A9F179_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static byte ReadByte(this NetworkReader reader) => reader.ReadBlittable<byte>();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		NullCheck(L_0);
		uint8_t L_1;
		L_1 = NetworkReader_ReadBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mD424A6F72F431A64491144A4321E8915B2A9F179_inline(L_0, NetworkReader_ReadBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mD424A6F72F431A64491144A4321E8915B2A9F179_RuntimeMethod_var);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, String_t* ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (value == null)
		String_t* L_0 = ___value1;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// writer.WriteUShort(0);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_1 = ___writer0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteUShort_m4AEC8147034117F9EB131043089577CD2DB42DB4_inline(L_1, (uint16_t)0, NULL);
		// return;
		return;
	}

IL_000b:
	{
		// int size = encoding.GetBytes(value, 0, value.Length, stringBuffer, 0);
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		UTF8Encoding_t90B56215A1B0B7ED5CDEA772E695F0DDAFBCD3BE* L_2 = ((NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_StaticFields*)il2cpp_codegen_static_fields_for(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var))->___encoding_0;
		String_t* L_3 = ___value1;
		String_t* L_4 = ___value1;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_4, NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_6 = ((NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_StaticFields*)il2cpp_codegen_static_fields_for(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var))->___stringBuffer_1;
		NullCheck(L_2);
		int32_t L_7;
		L_7 = VirtualFuncInvoker5< int32_t, String_t*, int32_t, int32_t, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t >::Invoke(17 /* System.Int32 System.Text.Encoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32) */, L_2, L_3, 0, L_5, L_6, 0);
		V_0 = L_7;
		// if (size >= NetworkWriter.MaxStringLength)
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) < ((int32_t)((int32_t)32768))))
		{
			goto IL_004c;
		}
	}
	{
		// throw new IndexOutOfRangeException($"NetworkWriter.Write(string) too long: {size}. Limit: {NetworkWriter.MaxStringLength}");
		int32_t L_9 = V_0;
		int32_t L_10 = L_9;
		RuntimeObject* L_11 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_10);
		int32_t L_12 = ((int32_t)32768);
		RuntimeObject* L_13 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_12);
		String_t* L_14;
		L_14 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral8958537A1C371340CA2DB0CDC27257F8CB3BC9D9)), L_11, L_13, NULL);
		IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82* L_15 = (IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82_il2cpp_TypeInfo_var)));
		NullCheck(L_15);
		IndexOutOfRangeException__ctor_mFD06819F05B815BE2D6E826D4E04F4C449D0A425(L_15, L_14, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98_RuntimeMethod_var)));
	}

IL_004c:
	{
		// writer.WriteUShort(checked((ushort)(size + 1)));
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_16 = ___writer0;
		int32_t L_17 = V_0;
		if (((int64_t)L_17 + (int64_t)1 < (int64_t)kIl2CppInt32Min) || ((int64_t)L_17 + (int64_t)1 > (int64_t)kIl2CppInt32Max))
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98_RuntimeMethod_var);
		if ((int64_t)(((int32_t)il2cpp_codegen_add(L_17, 1))) > 65535LL) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98_RuntimeMethod_var);
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteUShort_m4AEC8147034117F9EB131043089577CD2DB42DB4_inline(L_16, ((uint16_t)((int32_t)il2cpp_codegen_add(L_17, 1))), NULL);
		// writer.WriteBytes(stringBuffer, 0, size);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_18 = ___writer0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_19 = ((NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_StaticFields*)il2cpp_codegen_static_fields_for(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var))->___stringBuffer_1;
		int32_t L_20 = V_0;
		NullCheck(L_18);
		NetworkWriter_WriteBytes_m0F3058BA3B1B973C3D99B647DA231D9E82AFEDEC_inline(L_18, L_19, 0, L_20, NULL);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, bool ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriter_WriteBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mCDD7CA43B0B14857B8777A663BE02A0AA5917764_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* G_B2_0 = NULL;
	NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* G_B3_1 = NULL;
	{
		// public static void WriteBool(this NetworkWriter writer, bool value) => writer.WriteBlittable((byte)(value ? 1 : 0));
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		bool L_1 = ___value1;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0007;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0008;
	}

IL_0007:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_0008:
	{
		NullCheck(G_B3_1);
		NetworkWriter_WriteBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mCDD7CA43B0B14857B8777A663BE02A0AA5917764_inline(G_B3_1, (uint8_t)((int32_t)(uint8_t)G_B3_0), NetworkWriter_WriteBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mCDD7CA43B0B14857B8777A663BE02A0AA5917764_RuntimeMethod_var);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint8_t ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriter_WriteBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mCDD7CA43B0B14857B8777A663BE02A0AA5917764_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static void WriteByte(this NetworkWriter writer, byte value) => writer.WriteBlittable(value);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		uint8_t L_1 = ___value1;
		NullCheck(L_0);
		NetworkWriter_WriteBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mCDD7CA43B0B14857B8777A663BE02A0AA5917764_inline(L_0, L_1, NetworkWriter_WriteBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mCDD7CA43B0B14857B8777A663BE02A0AA5917764_RuntimeMethod_var);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReader_ReadBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m20406E9280C5EE4AA46F5E308A00B1CE728A70A3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static uint ReadUInt(this NetworkReader reader) => reader.ReadBlittable<uint>();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		NullCheck(L_0);
		uint32_t L_1;
		L_1 = NetworkReader_ReadBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m20406E9280C5EE4AA46F5E308A00B1CE728A70A3_inline(L_0, NetworkReader_ReadBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m20406E9280C5EE4AA46F5E308A00B1CE728A70A3_RuntimeMethod_var);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReader_ReadBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m4436F2F119D575AC7A0EA84CCD5C3CF655A3FFFC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static int ReadInt(this NetworkReader reader) => reader.ReadBlittable<int>();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = NetworkReader_ReadBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m4436F2F119D575AC7A0EA84CCD5C3CF655A3FFFC_inline(L_0, NetworkReader_ReadBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m4436F2F119D575AC7A0EA84CCD5C3CF655A3FFFC_RuntimeMethod_var);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// uint count = reader.ReadUInt();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		uint32_t L_1;
		L_1 = NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4_inline(L_0, NULL);
		V_0 = L_1;
		// return count == 0 ? default : reader.ReadBytesSegment(checked((int)(count - 1u)));
		uint32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_3 = ___reader0;
		uint32_t L_4 = V_0;
		if ((uint64_t)(uint32_t)L_4 - (uint64_t)(uint32_t)1 > (uint64_t)(uint32_t)kIl2CppUInt32Max)
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F_RuntimeMethod_var);
		if ((uint64_t)(((int32_t)il2cpp_codegen_subtract((int32_t)L_4, 1))) > 2147483647LL) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F_RuntimeMethod_var);
		NullCheck(L_3);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_5;
		L_5 = NetworkReader_ReadBytesSegment_mA17220D13799B7AA2EFF9B49C6F1F98B486A330E_inline(L_3, ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_4, 1))), NULL);
		return L_5;
	}

IL_0015:
	{
		il2cpp_codegen_initobj((&V_1), sizeof(ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093));
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_6 = V_1;
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint32_t ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriter_WriteBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m3F8565D904BC20262924C7AF8BFBB3F7FE770535_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static void WriteUInt(this NetworkWriter writer, uint value) => writer.WriteBlittable(value);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		uint32_t L_1 = ___value1;
		NullCheck(L_0);
		NetworkWriter_WriteBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m3F8565D904BC20262924C7AF8BFBB3F7FE770535_inline(L_0, L_1, NetworkWriter_WriteBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m3F8565D904BC20262924C7AF8BFBB3F7FE770535_RuntimeMethod_var);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, int32_t ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriter_WriteBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m5745AC0FB4E1ABCB68691585D1B48F92DA99AEFB_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static void WriteInt(this NetworkWriter writer, int value) => writer.WriteBlittable(value);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		int32_t L_1 = ___value1;
		NullCheck(L_0);
		NetworkWriter_WriteBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m5745AC0FB4E1ABCB68691585D1B48F92DA99AEFB_inline(L_0, L_1, NetworkWriter_WriteBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m5745AC0FB4E1ABCB68691585D1B48F92DA99AEFB_RuntimeMethod_var);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteBytesAndSizeSegment_mD7FCDC44AB0313C3ED0AFBFEC77366B6947672AC_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___buffer1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// writer.WriteBytesAndSize(buffer.Array, buffer.Offset, buffer.Count);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1;
		L_1 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline((&___buffer1), ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		int32_t L_2;
		L_2 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline((&___buffer1), ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_3;
		L_3 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline((&___buffer1), ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteBytesAndSize_m3A29964C9A4F7D85B49358324A238EADA7DE57BA_inline(L_0, L_1, L_2, L_3, NULL);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Guid_t NetworkReaderExtensions_ReadGuid_mCFFAB7379132286F7C9CC70EC291F8B28EA08B0E_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Guid ReadGuid(this NetworkReader reader) => new Guid(reader.ReadBytes(16));
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1;
		L_1 = NetworkReaderExtensions_ReadBytes_mF2B3E392F976B37C12A9BB81DBEB98726813730D_inline(L_0, ((int32_t)16), NULL);
		Guid_t L_2;
		memset((&L_2), 0, sizeof(L_2));
		Guid__ctor_m9BEFD9FC285BE9ACEC2EB97FC76C0E35E14D725C((&L_2), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 NetworkReaderExtensions_ReadVector3_mD35BF8B14DD5F75688AB9C360D138D1BAB432637_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReader_ReadBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m17B9EA1CB3FF9F5A7936974AAD62B19383E2D69F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Vector3 ReadVector3(this NetworkReader reader) => reader.ReadBlittable<Vector3>();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		NullCheck(L_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = NetworkReader_ReadBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m17B9EA1CB3FF9F5A7936974AAD62B19383E2D69F_inline(L_0, NetworkReader_ReadBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m17B9EA1CB3FF9F5A7936974AAD62B19383E2D69F_RuntimeMethod_var);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 NetworkReaderExtensions_ReadQuaternion_m135F5C523703C700E6A266DA9718E44D160BB567_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReader_ReadBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mF27EFEBBC75EBD54AF10CE92FF64D5C780A97A69_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Quaternion ReadQuaternion(this NetworkReader reader) => reader.ReadBlittable<Quaternion>();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		NullCheck(L_0);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1;
		L_1 = NetworkReader_ReadBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mF27EFEBBC75EBD54AF10CE92FF64D5C780A97A69_inline(L_0, NetworkReader_ReadBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mF27EFEBBC75EBD54AF10CE92FF64D5C780A97A69_RuntimeMethod_var);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteGuid_mFF6C6A1BC90A9A7BBC9C179FA6FC25753689D3F9_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, Guid_t ___value1, const RuntimeMethod* method) 
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_0 = NULL;
	{
		// byte[] data = value.ToByteArray();
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0;
		L_0 = Guid_ToByteArray_m6EBFB2F42D3760D9143050A3A8ED03F085F3AFE9((&___value1), NULL);
		V_0 = L_0;
		// writer.WriteBytes(data, 0, data.Length);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_1 = ___writer0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_2 = V_0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = V_0;
		NullCheck(L_3);
		NullCheck(L_1);
		NetworkWriter_WriteBytes_m0F3058BA3B1B973C3D99B647DA231D9E82AFEDEC_inline(L_1, L_2, 0, ((int32_t)(((RuntimeArray*)L_3)->max_length)), NULL);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteVector3_mB3C2B2F2D3C9874F883C12813FB20B9D5AABC882_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriter_WriteBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m6552B19126A3F040F7E78F41CEB63CA85B0EF8BC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static void WriteVector3(this NetworkWriter writer, Vector3 value) => writer.WriteBlittable(value);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = ___value1;
		NullCheck(L_0);
		NetworkWriter_WriteBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m6552B19126A3F040F7E78F41CEB63CA85B0EF8BC_inline(L_0, L_1, NetworkWriter_WriteBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m6552B19126A3F040F7E78F41CEB63CA85B0EF8BC_RuntimeMethod_var);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteQuaternion_mFC2E046965F6BA1B694218D5E60E26807974ACBA_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriter_WriteBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mD95146DDEF099B2575801811770B7527E72A4969_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static void WriteQuaternion(this NetworkWriter writer, Quaternion value) => writer.WriteBlittable(value);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1 = ___value1;
		NullCheck(L_0);
		NetworkWriter_WriteBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mD95146DDEF099B2575801811770B7527E72A4969_inline(L_0, L_1, NetworkWriter_WriteBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mD95146DDEF099B2575801811770B7527E72A4969_RuntimeMethod_var);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double NetworkReaderExtensions_ReadDouble_m949A60A21C6EB3B9952A43355903F08B3A7E0EF9_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReader_ReadBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m9F3B82FB2EF977402985132FD7A7B3F531AC32C8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static double ReadDouble(this NetworkReader reader) => reader.ReadBlittable<double>();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		NullCheck(L_0);
		double L_1;
		L_1 = NetworkReader_ReadBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m9F3B82FB2EF977402985132FD7A7B3F531AC32C8_inline(L_0, NetworkReader_ReadBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m9F3B82FB2EF977402985132FD7A7B3F531AC32C8_RuntimeMethod_var);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteDouble_m0475F5FB9E1D69D60501C7158AD3431680BC1BDB_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, double ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriter_WriteBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m520D47927C9E53348DF0990757101E4408B4E4E8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// writer.WriteBlittable(value);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		double L_1 = ___value1;
		NullCheck(L_0);
		NetworkWriter_WriteBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m520D47927C9E53348DF0990757101E4408B4E4E8_inline(L_0, L_1, NetworkWriter_WriteBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m520D47927C9E53348DF0990757101E4408B4E4E8_RuntimeMethod_var);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint16_t NetworkReaderExtensions_ReadUShort_mA98395DD1B1DA249096858B171B8BC23D95DF765_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReader_ReadBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m5CB828366095AF0971BD2196AC15F944092E1E91_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static ushort ReadUShort(this NetworkReader reader) => reader.ReadBlittable<ushort>();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		NullCheck(L_0);
		uint16_t L_1;
		L_1 = NetworkReader_ReadBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m5CB828366095AF0971BD2196AC15F944092E1E91_inline(L_0, NetworkReader_ReadBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m5CB828366095AF0971BD2196AC15F944092E1E91_RuntimeMethod_var);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteUShort_m4AEC8147034117F9EB131043089577CD2DB42DB4_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, uint16_t ___value1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriter_WriteBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m7A05C5F5DD5D33FC126D2DAC895A096EC3402A14_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static void WriteUShort(this NetworkWriter writer, ushort value) => writer.WriteBlittable(value);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_0 = ___writer0;
		uint16_t L_1 = ___value1;
		NullCheck(L_0);
		NetworkWriter_WriteBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m7A05C5F5DD5D33FC126D2DAC895A096EC3402A14_inline(L_0, L_1, NetworkWriter_WriteBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m7A05C5F5DD5D33FC126D2DAC895A096EC3402A14_RuntimeMethod_var);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkBehaviour_GeneratedSyncVarSetter_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mD43D62C7BD24575258CA0CF86ABD19131A08BE2D_gshared_inline (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value0, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* ___field1, uint64_t ___dirtyBit2, Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* ___OnChanged3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkServer_t90298DAB739AB649EFA5EE04950D68A903D6E920_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (!SyncVarEqual(value, ref field))
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_0 = ___value0;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* L_1 = ___field1;
		bool L_2;
		L_2 = ((  bool (*) (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->rgctx_data, 0)))(L_0, L_1, il2cpp_rgctx_method(method->rgctx_data, 0));
		if (L_2)
		{
			goto IL_0046;
		}
	}
	{
		// T oldValue = field;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* L_3 = ___field1;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_4 = (*(Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B*)L_3);
		V_0 = L_4;
		// SetSyncVar(value, ref field, dirtyBit);
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_5 = ___value0;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* L_6 = ___field1;
		uint64_t L_7 = ___dirtyBit2;
		((  void (*) (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C*, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B*, uint64_t, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->rgctx_data, 1)))(__this, L_5, L_6, L_7, il2cpp_rgctx_method(method->rgctx_data, 1));
		// if (OnChanged != null)
		Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* L_8 = ___OnChanged3;
		if (!L_8)
		{
			goto IL_0046;
		}
	}
	{
		// if (NetworkServer.localClientActive && !GetSyncVarHookGuard(dirtyBit))
		il2cpp_codegen_runtime_class_init_inline(NetworkServer_t90298DAB739AB649EFA5EE04950D68A903D6E920_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = NetworkServer_get_localClientActive_mF819059FAC02CCA8DCAEDEE3D7D9A0F4EB077846(NULL);
		if (!L_9)
		{
			goto IL_0046;
		}
	}
	{
		uint64_t L_10 = ___dirtyBit2;
		bool L_11;
		L_11 = NetworkBehaviour_GetSyncVarHookGuard_mF9DBFE6D19AC4402C44FDD0BEF11C1C99B694ED8(__this, L_10, NULL);
		if (L_11)
		{
			goto IL_0046;
		}
	}
	{
		// SetSyncVarHookGuard(dirtyBit, true);
		uint64_t L_12 = ___dirtyBit2;
		NetworkBehaviour_SetSyncVarHookGuard_m8098794BA748ACFFE6B68C69B9D5609E43F71334(__this, L_12, (bool)1, NULL);
		// OnChanged(oldValue, value);
		Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* L_13 = ___OnChanged3;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_14 = V_0;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_15 = ___value0;
		NullCheck(L_13);
		((  void (*) (Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA*, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->rgctx_data, 3)))(L_13, L_14, L_15, il2cpp_rgctx_method(method->rgctx_data, 3));
		// SetSyncVarHookGuard(dirtyBit, false);
		uint64_t L_16 = ___dirtyBit2;
		NetworkBehaviour_SetSyncVarHookGuard_m8098794BA748ACFFE6B68C69B9D5609E43F71334(__this, L_16, (bool)0, NULL);
	}

IL_0046:
	{
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkBehaviour_GeneratedSyncVarDeserialize_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mCD3E867438BB843F76C76561EC9F138D8CC6694D_gshared_inline (NetworkBehaviour_tB9808F4640389688B2CE5EBBB553626DA4FEE88C* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* ___field0, Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* ___OnChanged1, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value2, const RuntimeMethod* method) 
{
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// T previous = field;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* L_0 = ___field0;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_1 = (*(Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B*)L_0);
		V_0 = L_1;
		// field = value;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* L_2 = ___field0;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_3 = ___value2;
		*(Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B*)L_2 = L_3;
		// if (OnChanged != null && !SyncVarEqual(previous, ref field))
		Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* L_4 = ___OnChanged1;
		if (!L_4)
		{
			goto IL_0027;
		}
	}
	{
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_5 = V_0;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* L_6 = ___field0;
		bool L_7;
		L_7 = ((  bool (*) (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->rgctx_data, 0)))(L_5, L_6, il2cpp_rgctx_method(method->rgctx_data, 0));
		if (L_7)
		{
			goto IL_0027;
		}
	}
	{
		// OnChanged(previous, field);
		Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA* L_8 = ___OnChanged1;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_9 = V_0;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* L_10 = ___field0;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_11 = (*(Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B*)L_10);
		NullCheck(L_8);
		((  void (*) (Action_2_tEB8734650097696B3E2A474514F32097FE5D6ECA*, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->rgctx_data, 2)))(L_8, L_9, L_11, il2cpp_rgctx_method(method->rgctx_data, 2));
	}

IL_0027:
	{
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* NetworkReaderExtensions_ReadArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m3CE6D0D53E5BA355729F19428F259391B4A375B5_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* V_1 = NULL;
	int32_t V_2 = 0;
	{
		// int length = reader.ReadInt();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A_inline(L_0, NULL);
		V_0 = L_1;
		// if (length < 0)
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		// return null;
		return (MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572*)NULL;
	}

IL_000d:
	{
		// if (length > reader.Length - reader.Position)
		int32_t L_3 = V_0;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_4 = ___reader0;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = NetworkReader_get_Length_m23094EDDFE84816D0846ED68F1EFEA79220B93EE_inline(L_4, NULL);
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_6 = ___reader0;
		NullCheck(L_6);
		int32_t L_7 = (int32_t)L_6->___Position_1;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)il2cpp_codegen_subtract(L_5, L_7)))))
		{
			goto IL_0033;
		}
	}
	{
		// throw new EndOfStreamException($"Received array that is too large: {length}");
		int32_t L_8 = V_0;
		int32_t L_9 = L_8;
		RuntimeObject* L_10 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_9);
		String_t* L_11;
		L_11 = String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral9EEC476CC0C4E4E34F2F7CFB59773BB801BA4C9C)), L_10, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_12 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_12);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_12, L_11, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReaderExtensions_ReadArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m3CE6D0D53E5BA355729F19428F259391B4A375B5_RuntimeMethod_var)));
	}

IL_0033:
	{
		// T[] result = new T[length];
		int32_t L_13 = V_0;
		MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* L_14 = (MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572*)(MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572*)SZArrayNew(il2cpp_rgctx_data(method->rgctx_data, 0), (uint32_t)L_13);
		V_1 = L_14;
		// for (int i = 0; i < length; i++)
		V_2 = 0;
		goto IL_004f;
	}

IL_003e:
	{
		// result[i] = reader.Read<T>();
		MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* L_15 = V_1;
		int32_t L_16 = V_2;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_17 = ___reader0;
		NullCheck(L_17);
		MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 L_18;
		L_18 = ((  MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->rgctx_data, 1)))(L_17, il2cpp_rgctx_method(method->rgctx_data, 1));
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41)L_18);
		// for (int i = 0; i < length; i++)
		int32_t L_19 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_004f:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_20 = V_2;
		int32_t L_21 = V_0;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_003e;
		}
	}
	{
		// return result;
		MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* L_22 = V_1;
		return L_22;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* NetworkReaderExtensions_ReadArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_mC8AFAB257D0DC0249D387758D4F204833AADE730_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* V_1 = NULL;
	int32_t V_2 = 0;
	{
		// int length = reader.ReadInt();
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_0 = ___reader0;
		il2cpp_codegen_runtime_class_init_inline(NetworkReaderExtensions_tB5D0ED93EA873923145734230A038740BBDB217B_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A_inline(L_0, NULL);
		V_0 = L_1;
		// if (length < 0)
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		// return null;
		return (PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415*)NULL;
	}

IL_000d:
	{
		// if (length > reader.Length - reader.Position)
		int32_t L_3 = V_0;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_4 = ___reader0;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = NetworkReader_get_Length_m23094EDDFE84816D0846ED68F1EFEA79220B93EE_inline(L_4, NULL);
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_6 = ___reader0;
		NullCheck(L_6);
		int32_t L_7 = (int32_t)L_6->___Position_1;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)il2cpp_codegen_subtract(L_5, L_7)))))
		{
			goto IL_0033;
		}
	}
	{
		// throw new EndOfStreamException($"Received array that is too large: {length}");
		int32_t L_8 = V_0;
		int32_t L_9 = L_8;
		RuntimeObject* L_10 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_9);
		String_t* L_11;
		L_11 = String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral9EEC476CC0C4E4E34F2F7CFB59773BB801BA4C9C)), L_10, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_12 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_12);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_12, L_11, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReaderExtensions_ReadArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_mC8AFAB257D0DC0249D387758D4F204833AADE730_RuntimeMethod_var)));
	}

IL_0033:
	{
		// T[] result = new T[length];
		int32_t L_13 = V_0;
		PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* L_14 = (PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415*)(PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415*)SZArrayNew(il2cpp_rgctx_data(method->rgctx_data, 0), (uint32_t)L_13);
		V_1 = L_14;
		// for (int i = 0; i < length; i++)
		V_2 = 0;
		goto IL_004f;
	}

IL_003e:
	{
		// result[i] = reader.Read<T>();
		PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* L_15 = V_1;
		int32_t L_16 = V_2;
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_17 = ___reader0;
		NullCheck(L_17);
		PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 L_18;
		L_18 = ((  PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 (*) (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->rgctx_data, 1)))(L_17, il2cpp_rgctx_method(method->rgctx_data, 1));
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942)L_18);
		// for (int i = 0; i < length; i++)
		int32_t L_19 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_004f:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_20 = V_2;
		int32_t L_21 = V_0;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_003e;
		}
	}
	{
		// return result;
		PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* L_22 = V_1;
		return L_22;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteArray_TisMatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41_m0946D038BE696722FB3ED0523DEC8233C19FFAC2_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* ___array1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (array is null)
		MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* L_0 = ___array1;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// writer.WriteInt(-1);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_1 = ___writer0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_inline(L_1, (-1), NULL);
		// return;
		return;
	}

IL_000b:
	{
		// writer.WriteInt(array.Length);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_2 = ___writer0;
		MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* L_3 = ___array1;
		NullCheck(L_3);
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_inline(L_2, ((int32_t)(((RuntimeArray*)L_3)->max_length)), NULL);
		// for (int i = 0; i < array.Length; i++)
		V_0 = 0;
		goto IL_0029;
	}

IL_0018:
	{
		// writer.Write(array[i]);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_4 = ___writer0;
		MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* L_5 = ___array1;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41 L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_4);
		((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, MatchInfo_tEDF27DC9DFC19E26535E0CBBE992F002B1A6CF41, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->rgctx_data, 0)))(L_4, L_8, il2cpp_rgctx_method(method->rgctx_data, 0));
		// for (int i = 0; i < array.Length; i++)
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_9, 1));
	}

IL_0029:
	{
		// for (int i = 0; i < array.Length; i++)
		int32_t L_10 = V_0;
		MatchInfoU5BU5D_t84F2B1AF508D4E935AE77C1C24AB38218EDD8572* L_11 = ___array1;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length)))))
		{
			goto IL_0018;
		}
	}
	{
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteArray_TisPlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942_m348C67F42E07DE2F8A0295C5B388A6985FF81A0E_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* ___array1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (array is null)
		PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* L_0 = ___array1;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// writer.WriteInt(-1);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_1 = ___writer0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_inline(L_1, (-1), NULL);
		// return;
		return;
	}

IL_000b:
	{
		// writer.WriteInt(array.Length);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_2 = ___writer0;
		PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* L_3 = ___array1;
		NullCheck(L_3);
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12_inline(L_2, ((int32_t)(((RuntimeArray*)L_3)->max_length)), NULL);
		// for (int i = 0; i < array.Length; i++)
		V_0 = 0;
		goto IL_0029;
	}

IL_0018:
	{
		// writer.Write(array[i]);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_4 = ___writer0;
		PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* L_5 = ___array1;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942 L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_4);
		((  void (*) (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C*, PlayerInfo_t1555E3F26E68838264BEDA26F4726BAF11E93942, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->rgctx_data, 0)))(L_4, L_8, il2cpp_rgctx_method(method->rgctx_data, 0));
		// for (int i = 0; i < array.Length; i++)
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_9, 1));
	}

IL_0029:
	{
		// for (int i = 0; i < array.Length; i++)
		int32_t L_10 = V_0;
		PlayerInfoU5BU5D_t59D07B232F694E4B564EF004D5EC70E663FAD415* L_11 = ___array1;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length)))))
		{
			goto IL_0018;
		}
	}
	{
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___r0;
		__this->___r_0 = L_0;
		float L_1 = ___g1;
		__this->___g_1 = L_1;
		float L_2 = ___b2;
		__this->___b_2 = L_2;
		float L_3 = ___a3;
		__this->___a_3 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 Color_op_Implicit_m6D1353534AD23E43DFD104850D55C469CFCEF340_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___c0, const RuntimeMethod* method) 
{
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___c0;
		float L_1 = L_0.___r_0;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2 = ___c0;
		float L_3 = L_2.___g_1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_4 = ___c0;
		float L_5 = L_4.___b_2;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_6 = ___c0;
		float L_7 = L_6.___a_3;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline((&L_8), L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0021;
	}

IL_0021:
	{
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector4_op_Equality_m80E2AA0626A70EF9DCC4F4C215F674A22D6DE937_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___lhs0, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___rhs1, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	bool V_5 = false;
	{
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_0 = ___lhs0;
		float L_1 = L_0.___x_1;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_2 = ___rhs1;
		float L_3 = L_2.___x_1;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_4 = ___lhs0;
		float L_5 = L_4.___y_2;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_6 = ___rhs1;
		float L_7 = L_6.___y_2;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_8 = ___lhs0;
		float L_9 = L_8.___z_3;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_10 = ___rhs1;
		float L_11 = L_10.___z_3;
		V_2 = ((float)il2cpp_codegen_subtract(L_9, L_11));
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_12 = ___lhs0;
		float L_13 = L_12.___w_4;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_14 = ___rhs1;
		float L_15 = L_14.___w_4;
		V_3 = ((float)il2cpp_codegen_subtract(L_13, L_15));
		float L_16 = V_0;
		float L_17 = V_0;
		float L_18 = V_1;
		float L_19 = V_1;
		float L_20 = V_2;
		float L_21 = V_2;
		float L_22 = V_3;
		float L_23 = V_3;
		V_4 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_16, L_17)), ((float)il2cpp_codegen_multiply(L_18, L_19)))), ((float)il2cpp_codegen_multiply(L_20, L_21)))), ((float)il2cpp_codegen_multiply(L_22, L_23))));
		float L_24 = V_4;
		V_5 = (bool)((((float)L_24) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_0057;
	}

IL_0057:
	{
		bool L_25 = V_5;
		return L_25;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline (float ___value0, const RuntimeMethod* method) 
{
	bool V_0 = false;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		float L_0 = ___value0;
		V_0 = (bool)((((float)L_0) < ((float)(0.0f)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (0.0f);
		goto IL_002d;
	}

IL_0015:
	{
		float L_2 = ___value0;
		V_2 = (bool)((((float)L_2) > ((float)(1.0f)))? 1 : 0);
		bool L_3 = V_2;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		V_1 = (1.0f);
		goto IL_002d;
	}

IL_0029:
	{
		float L_4 = ___value0;
		V_1 = L_4;
		goto IL_002d;
	}

IL_002d:
	{
		float L_5 = V_1;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method) 
{
	{
		__this->___rgba_0 = 0;
		uint8_t L_0 = ___r0;
		__this->___r_1 = L_0;
		uint8_t L_1 = ___g1;
		__this->___g_2 = L_1;
		uint8_t L_2 = ___b2;
		__this->___b_3 = L_2;
		uint8_t L_3 = ___a3;
		__this->___a_4 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 NetworkReader_ReadBytesSegment_mA17220D13799B7AA2EFF9B49C6F1F98B486A330E_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, int32_t ___count0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Position + count > buffer.Count)
		int32_t L_0 = __this->___Position_1;
		int32_t L_1 = ___count0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_2 = (&__this->___buffer_0);
		int32_t L_3;
		L_3 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline(L_2, ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(L_0, L_1))) <= ((int32_t)L_3)))
		{
			goto IL_0031;
		}
	}
	{
		// throw new EndOfStreamException($"ReadBytesSegment can't read {count} bytes because it would read past the end of the stream. {ToString()}");
		int32_t L_4 = ___count0;
		int32_t L_5 = L_4;
		RuntimeObject* L_6 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_5);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		String_t* L_8;
		L_8 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral97DBD483FF6D25D8A2CF2D9700D08EB0CFDD00D4)), L_6, L_7, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_9 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_9, L_8, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReader_ReadBytesSegment_mA17220D13799B7AA2EFF9B49C6F1F98B486A330E_RuntimeMethod_var)));
	}

IL_0031:
	{
		// ArraySegment<byte> result = new ArraySegment<byte>(buffer.Array, buffer.Offset + Position, count);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_10 = (&__this->___buffer_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11;
		L_11 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline(L_10, ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_12 = (&__this->___buffer_0);
		int32_t L_13;
		L_13 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline(L_12, ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_14 = __this->___Position_1;
		int32_t L_15 = ___count0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 L_16;
		memset((&L_16), 0, sizeof(L_16));
		ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20((&L_16), L_11, ((int32_t)il2cpp_codegen_add(L_13, L_14)), L_15, /*hidden argument*/ArraySegment_1__ctor_m664EA6AD314FAA6BCA4F6D0586AEF01559537F20_RuntimeMethod_var);
		// Position += count;
		int32_t L_17 = __this->___Position_1;
		int32_t L_18 = ___count0;
		__this->___Position_1 = ((int32_t)il2cpp_codegen_add(L_17, L_18));
		// return result;
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBytes_m0F3058BA3B1B973C3D99B647DA231D9E82AFEDEC_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method) 
{
	{
		// EnsureCapacity(Position + count);
		int32_t L_0 = __this->___Position_2;
		int32_t L_1 = ___count2;
		NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3_inline(__this, ((int32_t)il2cpp_codegen_add(L_0, L_1)), NULL);
		// Array.ConstrainedCopy(buffer, offset, this.buffer, Position, count);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_2 = ___buffer0;
		int32_t L_3 = ___offset1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_4 = __this->___buffer_1;
		int32_t L_5 = __this->___Position_2;
		int32_t L_6 = ___count2;
		Array_ConstrainedCopy_m14D61795896B63A77E396C63457AD6700410531C((RuntimeArray*)L_2, L_3, (RuntimeArray*)L_4, L_5, L_6, NULL);
		// Position += count;
		int32_t L_7 = __this->___Position_2;
		int32_t L_8 = ___count2;
		__this->___Position_2 = ((int32_t)il2cpp_codegen_add(L_7, L_8));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriterExtensions_WriteBytesAndSize_m3A29964C9A4F7D85B49358324A238EADA7DE57BA_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* ___writer0, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___buffer1, int32_t ___offset2, int32_t ___count3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_WriteBytesAndSize_m3A29964C9A4F7D85B49358324A238EADA7DE57BA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (buffer == null)
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___buffer1;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// writer.WriteUInt(0u);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_1 = ___writer0;
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_inline(L_1, 0, NULL);
		// return;
		return;
	}

IL_000b:
	{
		// writer.WriteUInt(checked((uint)count) + 1u);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_2 = ___writer0;
		int32_t L_3 = ___count3;
		if ((int64_t)(L_3) > 4294967295LL) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), NetworkWriterExtensions_WriteBytesAndSize_m3A29964C9A4F7D85B49358324A238EADA7DE57BA_RuntimeMethod_var);
		il2cpp_codegen_runtime_class_init_inline(NetworkWriterExtensions_t2D2B2848AD867C9392E353747A5A82CB5FED9DBD_il2cpp_TypeInfo_var);
		NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1_inline(L_2, ((int32_t)il2cpp_codegen_add((int32_t)((uint32_t)L_3), 1)), NULL);
		// writer.WriteBytes(buffer, offset, count);
		NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* L_4 = ___writer0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_5 = ___buffer1;
		int32_t L_6 = ___offset2;
		int32_t L_7 = ___count3;
		NullCheck(L_4);
		NetworkWriter_WriteBytes_m0F3058BA3B1B973C3D99B647DA231D9E82AFEDEC_inline(L_4, L_5, L_6, L_7, NULL);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* NetworkReaderExtensions_ReadBytes_mF2B3E392F976B37C12A9BB81DBEB98726813730D_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* ___reader0, int32_t ___count1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_0 = NULL;
	{
		// byte[] bytes = new byte[count];
		int32_t L_0 = ___count1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)L_0);
		V_0 = L_1;
		// reader.ReadBytes(bytes, count);
		NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* L_2 = ___reader0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = V_0;
		int32_t L_4 = ___count1;
		NullCheck(L_2);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_5;
		L_5 = NetworkReader_ReadBytes_mA1E53425AAD4AD3038C9759F6971533248347130_inline(L_2, L_3, L_4, NULL);
		// return bytes;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_6 = V_0;
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t NetworkReader_get_Length_m23094EDDFE84816D0846ED68F1EFEA79220B93EE_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get => buffer.Count;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_0 = (&__this->___buffer_0);
		int32_t L_1;
		L_1 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline(L_0, ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_mDB472FF359C94951E696CB3FD4F6016CFE6F82B7_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___value0, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B);
		V_0 = (int32_t)L_0;
		// EnsureCapacity(Position + size);
		int32_t L_1 = (int32_t)__this->___Position_2;
		int32_t L_2 = V_0;
		NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3_inline(__this, ((int32_t)il2cpp_codegen_add(L_1, L_2)), NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)__this->___buffer_1;
		int32_t L_4 = (int32_t)__this->___Position_2;
		NullCheck(L_3);
		V_1 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)));
		// fixed (byte* ptr = &buffer[Position])
		uint8_t* L_5 = V_1;
		// *(T*)ptr = value;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_6 = ___value0;
		*(Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B*)((uintptr_t)L_5) = L_6;
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_7 = (int32_t)__this->___Position_2;
		int32_t L_8 = V_0;
		__this->___Position_2 = ((int32_t)il2cpp_codegen_add(L_7, L_8));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m869219CA464A4A9CCE03043BF274E8EBD19428AC_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, uint64_t ___value0, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(uint64_t);
		V_0 = (int32_t)L_0;
		// EnsureCapacity(Position + size);
		int32_t L_1 = (int32_t)__this->___Position_2;
		int32_t L_2 = V_0;
		NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3_inline(__this, ((int32_t)il2cpp_codegen_add(L_1, L_2)), NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)__this->___buffer_1;
		int32_t L_4 = (int32_t)__this->___Position_2;
		NullCheck(L_3);
		V_1 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)));
		// fixed (byte* ptr = &buffer[Position])
		uint8_t* L_5 = V_1;
		// *(T*)ptr = value;
		uint64_t L_6 = ___value0;
		*(uint64_t*)((uintptr_t)L_5) = L_6;
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_7 = (int32_t)__this->___Position_2;
		int32_t L_8 = V_0;
		__this->___Position_2 = ((int32_t)il2cpp_codegen_add(L_7, L_8));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B NetworkReader_ReadBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_m6DBD713FC553C165E92A9ABA7691638AA9C2E7BD_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B);
		V_0 = (int32_t)L_0;
		// if (Position + size > buffer.Count)
		int32_t L_1 = (int32_t)__this->___Position_1;
		int32_t L_2 = V_0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_3 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_4;
		L_4 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline(L_3, ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(L_1, L_2))) <= ((int32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		// throw new EndOfStreamException($"ReadBlittable<{typeof(T)}> out of range: {ToString()}");
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_5 = { reinterpret_cast<intptr_t> (il2cpp_rgctx_type(method->rgctx_data, 1)) };
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t* L_6;
		L_6 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_5, NULL);
		NullCheck((RuntimeObject*)__this);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject*)__this);
		String_t* L_8;
		L_8 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral03E170E8209664E0E5EC4BFA0C00EC477E7A12B8)), (RuntimeObject*)L_6, (RuntimeObject*)L_7, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_9 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_9, L_8, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReader_ReadBlittable_TisColor32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B_m6DBD713FC553C165E92A9ABA7691638AA9C2E7BD_RuntimeMethod_var)));
	}

IL_003c:
	{
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_10 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11;
		L_11 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline(L_10, ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_12 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_13;
		L_13 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline(L_12, ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_14 = (int32_t)__this->___Position_1;
		NullCheck(L_11);
		V_1 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_13, L_14)))));
		// fixed (byte* ptr = &buffer.Array[buffer.Offset + Position])
		uint8_t* L_15 = V_1;
		// value = *(T*)ptr;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_16 = (*(Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B*)((uintptr_t)L_15));
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_17 = (int32_t)__this->___Position_1;
		int32_t L_18 = V_0;
		__this->___Position_1 = ((int32_t)il2cpp_codegen_add(L_17, L_18));
		// return value;
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint64_t NetworkReader_ReadBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m5B0E578FA24816AF3240FCA5BFF9353FBB4B958A_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(uint64_t);
		V_0 = (int32_t)L_0;
		// if (Position + size > buffer.Count)
		int32_t L_1 = (int32_t)__this->___Position_1;
		int32_t L_2 = V_0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_3 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_4;
		L_4 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline(L_3, ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(L_1, L_2))) <= ((int32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		// throw new EndOfStreamException($"ReadBlittable<{typeof(T)}> out of range: {ToString()}");
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_5 = { reinterpret_cast<intptr_t> (il2cpp_rgctx_type(method->rgctx_data, 1)) };
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t* L_6;
		L_6 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_5, NULL);
		NullCheck((RuntimeObject*)__this);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject*)__this);
		String_t* L_8;
		L_8 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral03E170E8209664E0E5EC4BFA0C00EC477E7A12B8)), (RuntimeObject*)L_6, (RuntimeObject*)L_7, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_9 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_9, L_8, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReader_ReadBlittable_TisUInt64_t8F12534CC8FC4B5860F2A2CD1EE79D322E7A41AF_m5B0E578FA24816AF3240FCA5BFF9353FBB4B958A_RuntimeMethod_var)));
	}

IL_003c:
	{
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_10 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11;
		L_11 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline(L_10, ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_12 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_13;
		L_13 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline(L_12, ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_14 = (int32_t)__this->___Position_1;
		NullCheck(L_11);
		V_1 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_13, L_14)))));
		// fixed (byte* ptr = &buffer.Array[buffer.Offset + Position])
		uint8_t* L_15 = V_1;
		// value = *(T*)ptr;
		uint64_t L_16 = (*(uint64_t*)((uintptr_t)L_15));
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_17 = (int32_t)__this->___Position_1;
		int32_t L_18 = V_0;
		__this->___Position_1 = ((int32_t)il2cpp_codegen_add(L_17, L_18));
		// return value;
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_gshared_inline (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, const RuntimeMethod* method) 
{
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)__this->____array_1;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_gshared_inline (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->____offset_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_gshared_inline (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->____count_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint8_t NetworkReader_ReadBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mD424A6F72F431A64491144A4321E8915B2A9F179_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(uint8_t);
		V_0 = (int32_t)L_0;
		// if (Position + size > buffer.Count)
		int32_t L_1 = (int32_t)__this->___Position_1;
		int32_t L_2 = V_0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_3 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_4;
		L_4 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline(L_3, ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(L_1, L_2))) <= ((int32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		// throw new EndOfStreamException($"ReadBlittable<{typeof(T)}> out of range: {ToString()}");
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_5 = { reinterpret_cast<intptr_t> (il2cpp_rgctx_type(method->rgctx_data, 1)) };
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t* L_6;
		L_6 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_5, NULL);
		NullCheck((RuntimeObject*)__this);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject*)__this);
		String_t* L_8;
		L_8 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral03E170E8209664E0E5EC4BFA0C00EC477E7A12B8)), (RuntimeObject*)L_6, (RuntimeObject*)L_7, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_9 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_9, L_8, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReader_ReadBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mD424A6F72F431A64491144A4321E8915B2A9F179_RuntimeMethod_var)));
	}

IL_003c:
	{
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_10 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11;
		L_11 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline(L_10, ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_12 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_13;
		L_13 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline(L_12, ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_14 = (int32_t)__this->___Position_1;
		NullCheck(L_11);
		V_1 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_13, L_14)))));
		// fixed (byte* ptr = &buffer.Array[buffer.Offset + Position])
		uint8_t* L_15 = V_1;
		// value = *(T*)ptr;
		uint8_t L_16 = (*(uint8_t*)((uintptr_t)L_15));
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_17 = (int32_t)__this->___Position_1;
		int32_t L_18 = V_0;
		__this->___Position_1 = ((int32_t)il2cpp_codegen_add(L_17, L_18));
		// return value;
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_mCDD7CA43B0B14857B8777A663BE02A0AA5917764_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, uint8_t ___value0, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(uint8_t);
		V_0 = (int32_t)L_0;
		// EnsureCapacity(Position + size);
		int32_t L_1 = (int32_t)__this->___Position_2;
		int32_t L_2 = V_0;
		NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3_inline(__this, ((int32_t)il2cpp_codegen_add(L_1, L_2)), NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)__this->___buffer_1;
		int32_t L_4 = (int32_t)__this->___Position_2;
		NullCheck(L_3);
		V_1 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)));
		// fixed (byte* ptr = &buffer[Position])
		uint8_t* L_5 = V_1;
		// *(T*)ptr = value;
		uint8_t L_6 = ___value0;
		*(uint8_t*)((uintptr_t)L_5) = L_6;
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_7 = (int32_t)__this->___Position_2;
		int32_t L_8 = V_0;
		__this->___Position_2 = ((int32_t)il2cpp_codegen_add(L_7, L_8));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t NetworkReader_ReadBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m20406E9280C5EE4AA46F5E308A00B1CE728A70A3_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(uint32_t);
		V_0 = (int32_t)L_0;
		// if (Position + size > buffer.Count)
		int32_t L_1 = (int32_t)__this->___Position_1;
		int32_t L_2 = V_0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_3 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_4;
		L_4 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline(L_3, ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(L_1, L_2))) <= ((int32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		// throw new EndOfStreamException($"ReadBlittable<{typeof(T)}> out of range: {ToString()}");
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_5 = { reinterpret_cast<intptr_t> (il2cpp_rgctx_type(method->rgctx_data, 1)) };
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t* L_6;
		L_6 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_5, NULL);
		NullCheck((RuntimeObject*)__this);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject*)__this);
		String_t* L_8;
		L_8 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral03E170E8209664E0E5EC4BFA0C00EC477E7A12B8)), (RuntimeObject*)L_6, (RuntimeObject*)L_7, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_9 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_9, L_8, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReader_ReadBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m20406E9280C5EE4AA46F5E308A00B1CE728A70A3_RuntimeMethod_var)));
	}

IL_003c:
	{
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_10 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11;
		L_11 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline(L_10, ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_12 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_13;
		L_13 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline(L_12, ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_14 = (int32_t)__this->___Position_1;
		NullCheck(L_11);
		V_1 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_13, L_14)))));
		// fixed (byte* ptr = &buffer.Array[buffer.Offset + Position])
		uint8_t* L_15 = V_1;
		// value = *(T*)ptr;
		uint32_t L_16 = (*(uint32_t*)((uintptr_t)L_15));
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_17 = (int32_t)__this->___Position_1;
		int32_t L_18 = V_0;
		__this->___Position_1 = ((int32_t)il2cpp_codegen_add(L_17, L_18));
		// return value;
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t NetworkReader_ReadBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m4436F2F119D575AC7A0EA84CCD5C3CF655A3FFFC_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(int32_t);
		V_0 = (int32_t)L_0;
		// if (Position + size > buffer.Count)
		int32_t L_1 = (int32_t)__this->___Position_1;
		int32_t L_2 = V_0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_3 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_4;
		L_4 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline(L_3, ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(L_1, L_2))) <= ((int32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		// throw new EndOfStreamException($"ReadBlittable<{typeof(T)}> out of range: {ToString()}");
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_5 = { reinterpret_cast<intptr_t> (il2cpp_rgctx_type(method->rgctx_data, 1)) };
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t* L_6;
		L_6 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_5, NULL);
		NullCheck((RuntimeObject*)__this);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject*)__this);
		String_t* L_8;
		L_8 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral03E170E8209664E0E5EC4BFA0C00EC477E7A12B8)), (RuntimeObject*)L_6, (RuntimeObject*)L_7, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_9 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_9, L_8, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReader_ReadBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m4436F2F119D575AC7A0EA84CCD5C3CF655A3FFFC_RuntimeMethod_var)));
	}

IL_003c:
	{
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_10 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11;
		L_11 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline(L_10, ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_12 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_13;
		L_13 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline(L_12, ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_14 = (int32_t)__this->___Position_1;
		NullCheck(L_11);
		V_1 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_13, L_14)))));
		// fixed (byte* ptr = &buffer.Array[buffer.Offset + Position])
		uint8_t* L_15 = V_1;
		// value = *(T*)ptr;
		int32_t L_16 = (*(int32_t*)((uintptr_t)L_15));
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_17 = (int32_t)__this->___Position_1;
		int32_t L_18 = V_0;
		__this->___Position_1 = ((int32_t)il2cpp_codegen_add(L_17, L_18));
		// return value;
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisUInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B_m3F8565D904BC20262924C7AF8BFBB3F7FE770535_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, uint32_t ___value0, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(uint32_t);
		V_0 = (int32_t)L_0;
		// EnsureCapacity(Position + size);
		int32_t L_1 = (int32_t)__this->___Position_2;
		int32_t L_2 = V_0;
		NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3_inline(__this, ((int32_t)il2cpp_codegen_add(L_1, L_2)), NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)__this->___buffer_1;
		int32_t L_4 = (int32_t)__this->___Position_2;
		NullCheck(L_3);
		V_1 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)));
		// fixed (byte* ptr = &buffer[Position])
		uint8_t* L_5 = V_1;
		// *(T*)ptr = value;
		uint32_t L_6 = ___value0;
		*(uint32_t*)((uintptr_t)L_5) = L_6;
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_7 = (int32_t)__this->___Position_2;
		int32_t L_8 = V_0;
		__this->___Position_2 = ((int32_t)il2cpp_codegen_add(L_7, L_8));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisInt32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_m5745AC0FB4E1ABCB68691585D1B48F92DA99AEFB_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(int32_t);
		V_0 = (int32_t)L_0;
		// EnsureCapacity(Position + size);
		int32_t L_1 = (int32_t)__this->___Position_2;
		int32_t L_2 = V_0;
		NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3_inline(__this, ((int32_t)il2cpp_codegen_add(L_1, L_2)), NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)__this->___buffer_1;
		int32_t L_4 = (int32_t)__this->___Position_2;
		NullCheck(L_3);
		V_1 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)));
		// fixed (byte* ptr = &buffer[Position])
		uint8_t* L_5 = V_1;
		// *(T*)ptr = value;
		int32_t L_6 = ___value0;
		*(int32_t*)((uintptr_t)L_5) = L_6;
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_7 = (int32_t)__this->___Position_2;
		int32_t L_8 = V_0;
		__this->___Position_2 = ((int32_t)il2cpp_codegen_add(L_7, L_8));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 NetworkReader_ReadBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m17B9EA1CB3FF9F5A7936974AAD62B19383E2D69F_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2);
		V_0 = (int32_t)L_0;
		// if (Position + size > buffer.Count)
		int32_t L_1 = (int32_t)__this->___Position_1;
		int32_t L_2 = V_0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_3 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_4;
		L_4 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline(L_3, ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(L_1, L_2))) <= ((int32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		// throw new EndOfStreamException($"ReadBlittable<{typeof(T)}> out of range: {ToString()}");
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_5 = { reinterpret_cast<intptr_t> (il2cpp_rgctx_type(method->rgctx_data, 1)) };
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t* L_6;
		L_6 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_5, NULL);
		NullCheck((RuntimeObject*)__this);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject*)__this);
		String_t* L_8;
		L_8 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral03E170E8209664E0E5EC4BFA0C00EC477E7A12B8)), (RuntimeObject*)L_6, (RuntimeObject*)L_7, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_9 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_9, L_8, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReader_ReadBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m17B9EA1CB3FF9F5A7936974AAD62B19383E2D69F_RuntimeMethod_var)));
	}

IL_003c:
	{
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_10 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11;
		L_11 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline(L_10, ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_12 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_13;
		L_13 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline(L_12, ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_14 = (int32_t)__this->___Position_1;
		NullCheck(L_11);
		V_1 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_13, L_14)))));
		// fixed (byte* ptr = &buffer.Array[buffer.Offset + Position])
		uint8_t* L_15 = V_1;
		// value = *(T*)ptr;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16 = (*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)((uintptr_t)L_15));
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_17 = (int32_t)__this->___Position_1;
		int32_t L_18 = V_0;
		__this->___Position_1 = ((int32_t)il2cpp_codegen_add(L_17, L_18));
		// return value;
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 NetworkReader_ReadBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mF27EFEBBC75EBD54AF10CE92FF64D5C780A97A69_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974);
		V_0 = (int32_t)L_0;
		// if (Position + size > buffer.Count)
		int32_t L_1 = (int32_t)__this->___Position_1;
		int32_t L_2 = V_0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_3 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_4;
		L_4 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline(L_3, ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(L_1, L_2))) <= ((int32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		// throw new EndOfStreamException($"ReadBlittable<{typeof(T)}> out of range: {ToString()}");
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_5 = { reinterpret_cast<intptr_t> (il2cpp_rgctx_type(method->rgctx_data, 1)) };
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t* L_6;
		L_6 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_5, NULL);
		NullCheck((RuntimeObject*)__this);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject*)__this);
		String_t* L_8;
		L_8 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral03E170E8209664E0E5EC4BFA0C00EC477E7A12B8)), (RuntimeObject*)L_6, (RuntimeObject*)L_7, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_9 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_9, L_8, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReader_ReadBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mF27EFEBBC75EBD54AF10CE92FF64D5C780A97A69_RuntimeMethod_var)));
	}

IL_003c:
	{
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_10 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11;
		L_11 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline(L_10, ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_12 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_13;
		L_13 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline(L_12, ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_14 = (int32_t)__this->___Position_1;
		NullCheck(L_11);
		V_1 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_13, L_14)))));
		// fixed (byte* ptr = &buffer.Array[buffer.Offset + Position])
		uint8_t* L_15 = V_1;
		// value = *(T*)ptr;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_16 = (*(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974*)((uintptr_t)L_15));
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_17 = (int32_t)__this->___Position_1;
		int32_t L_18 = V_0;
		__this->___Position_1 = ((int32_t)il2cpp_codegen_add(L_17, L_18));
		// return value;
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m6552B19126A3F040F7E78F41CEB63CA85B0EF8BC_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2);
		V_0 = (int32_t)L_0;
		// EnsureCapacity(Position + size);
		int32_t L_1 = (int32_t)__this->___Position_2;
		int32_t L_2 = V_0;
		NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3_inline(__this, ((int32_t)il2cpp_codegen_add(L_1, L_2)), NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)__this->___buffer_1;
		int32_t L_4 = (int32_t)__this->___Position_2;
		NullCheck(L_3);
		V_1 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)));
		// fixed (byte* ptr = &buffer[Position])
		uint8_t* L_5 = V_1;
		// *(T*)ptr = value;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___value0;
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)((uintptr_t)L_5) = L_6;
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_7 = (int32_t)__this->___Position_2;
		int32_t L_8 = V_0;
		__this->___Position_2 = ((int32_t)il2cpp_codegen_add(L_7, L_8));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisQuaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_mD95146DDEF099B2575801811770B7527E72A4969_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___value0, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974);
		V_0 = (int32_t)L_0;
		// EnsureCapacity(Position + size);
		int32_t L_1 = (int32_t)__this->___Position_2;
		int32_t L_2 = V_0;
		NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3_inline(__this, ((int32_t)il2cpp_codegen_add(L_1, L_2)), NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)__this->___buffer_1;
		int32_t L_4 = (int32_t)__this->___Position_2;
		NullCheck(L_3);
		V_1 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)));
		// fixed (byte* ptr = &buffer[Position])
		uint8_t* L_5 = V_1;
		// *(T*)ptr = value;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_6 = ___value0;
		*(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974*)((uintptr_t)L_5) = L_6;
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_7 = (int32_t)__this->___Position_2;
		int32_t L_8 = V_0;
		__this->___Position_2 = ((int32_t)il2cpp_codegen_add(L_7, L_8));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double NetworkReader_ReadBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m9F3B82FB2EF977402985132FD7A7B3F531AC32C8_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(double);
		V_0 = (int32_t)L_0;
		// if (Position + size > buffer.Count)
		int32_t L_1 = (int32_t)__this->___Position_1;
		int32_t L_2 = V_0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_3 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_4;
		L_4 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline(L_3, ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(L_1, L_2))) <= ((int32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		// throw new EndOfStreamException($"ReadBlittable<{typeof(T)}> out of range: {ToString()}");
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_5 = { reinterpret_cast<intptr_t> (il2cpp_rgctx_type(method->rgctx_data, 1)) };
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t* L_6;
		L_6 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_5, NULL);
		NullCheck((RuntimeObject*)__this);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject*)__this);
		String_t* L_8;
		L_8 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral03E170E8209664E0E5EC4BFA0C00EC477E7A12B8)), (RuntimeObject*)L_6, (RuntimeObject*)L_7, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_9 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_9, L_8, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReader_ReadBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m9F3B82FB2EF977402985132FD7A7B3F531AC32C8_RuntimeMethod_var)));
	}

IL_003c:
	{
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_10 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11;
		L_11 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline(L_10, ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_12 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_13;
		L_13 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline(L_12, ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_14 = (int32_t)__this->___Position_1;
		NullCheck(L_11);
		V_1 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_13, L_14)))));
		// fixed (byte* ptr = &buffer.Array[buffer.Offset + Position])
		uint8_t* L_15 = V_1;
		// value = *(T*)ptr;
		double L_16 = (*(double*)((uintptr_t)L_15));
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_17 = (int32_t)__this->___Position_1;
		int32_t L_18 = V_0;
		__this->___Position_1 = ((int32_t)il2cpp_codegen_add(L_17, L_18));
		// return value;
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_m520D47927C9E53348DF0990757101E4408B4E4E8_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, double ___value0, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(double);
		V_0 = (int32_t)L_0;
		// EnsureCapacity(Position + size);
		int32_t L_1 = (int32_t)__this->___Position_2;
		int32_t L_2 = V_0;
		NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3_inline(__this, ((int32_t)il2cpp_codegen_add(L_1, L_2)), NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)__this->___buffer_1;
		int32_t L_4 = (int32_t)__this->___Position_2;
		NullCheck(L_3);
		V_1 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)));
		// fixed (byte* ptr = &buffer[Position])
		uint8_t* L_5 = V_1;
		// *(T*)ptr = value;
		double L_6 = ___value0;
		*(double*)((uintptr_t)L_5) = L_6;
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_7 = (int32_t)__this->___Position_2;
		int32_t L_8 = V_0;
		__this->___Position_2 = ((int32_t)il2cpp_codegen_add(L_7, L_8));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint16_t NetworkReader_ReadBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m5CB828366095AF0971BD2196AC15F944092E1E91_gshared_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(uint16_t);
		V_0 = (int32_t)L_0;
		// if (Position + size > buffer.Count)
		int32_t L_1 = (int32_t)__this->___Position_1;
		int32_t L_2 = V_0;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_3 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_4;
		L_4 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline(L_3, ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(L_1, L_2))) <= ((int32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		// throw new EndOfStreamException($"ReadBlittable<{typeof(T)}> out of range: {ToString()}");
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_5 = { reinterpret_cast<intptr_t> (il2cpp_rgctx_type(method->rgctx_data, 1)) };
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t* L_6;
		L_6 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_5, NULL);
		NullCheck((RuntimeObject*)__this);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject*)__this);
		String_t* L_8;
		L_8 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral03E170E8209664E0E5EC4BFA0C00EC477E7A12B8)), (RuntimeObject*)L_6, (RuntimeObject*)L_7, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_9 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_9, L_8, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReader_ReadBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m5CB828366095AF0971BD2196AC15F944092E1E91_RuntimeMethod_var)));
	}

IL_003c:
	{
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_10 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_11;
		L_11 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline(L_10, ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_12 = (ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093*)(&__this->___buffer_0);
		int32_t L_13;
		L_13 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline(L_12, ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_14 = (int32_t)__this->___Position_1;
		NullCheck(L_11);
		V_1 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_13, L_14)))));
		// fixed (byte* ptr = &buffer.Array[buffer.Offset + Position])
		uint8_t* L_15 = V_1;
		// value = *(T*)ptr;
		uint16_t L_16 = (*(uint16_t*)((uintptr_t)L_15));
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_17 = (int32_t)__this->___Position_1;
		int32_t L_18 = V_0;
		__this->___Position_1 = ((int32_t)il2cpp_codegen_add(L_17, L_18));
		// return value;
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_WriteBlittable_TisUInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455_m7A05C5F5DD5D33FC126D2DAC895A096EC3402A14_gshared_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, uint16_t ___value0, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	{
		// int size = sizeof(T);
		uint32_t L_0 = sizeof(uint16_t);
		V_0 = (int32_t)L_0;
		// EnsureCapacity(Position + size);
		int32_t L_1 = (int32_t)__this->___Position_2;
		int32_t L_2 = V_0;
		NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3_inline(__this, ((int32_t)il2cpp_codegen_add(L_1, L_2)), NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)__this->___buffer_1;
		int32_t L_4 = (int32_t)__this->___Position_2;
		NullCheck(L_3);
		V_1 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)));
		// fixed (byte* ptr = &buffer[Position])
		uint8_t* L_5 = V_1;
		// *(T*)ptr = value;
		uint16_t L_6 = ___value0;
		*(uint16_t*)((uintptr_t)L_5) = L_6;
		V_1 = (uint8_t*)((uintptr_t)0);
		// Position += size;
		int32_t L_7 = (int32_t)__this->___Position_2;
		int32_t L_8 = V_0;
		__this->___Position_2 = ((int32_t)il2cpp_codegen_add(L_7, L_8));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_1 = L_0;
		float L_1 = ___y1;
		__this->___y_2 = L_1;
		float L_2 = ___z2;
		__this->___z_3 = L_2;
		float L_3 = ___w3;
		__this->___w_4 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3_inline (NetworkWriter_t27BA8C1DA1F8507E98CBD409717A57125ABDC37C* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Resize_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_m82FEC5823560947D2B12C8D675AED2C190DF4F3F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (buffer.Length < value)
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = __this->___buffer_1;
		NullCheck(L_0);
		int32_t L_1 = ___value0;
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length))) >= ((int32_t)L_1)))
		{
			goto IL_0028;
		}
	}
	{
		// int capacity = Math.Max(value, buffer.Length * 2);
		int32_t L_2 = ___value0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = __this->___buffer_1;
		NullCheck(L_3);
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		int32_t L_4;
		L_4 = Math_Max_m830F00B616D7A2130E46E974DFB27E9DA7FE30E5(L_2, ((int32_t)il2cpp_codegen_multiply(((int32_t)(((RuntimeArray*)L_3)->max_length)), 2)), NULL);
		V_0 = L_4;
		// Array.Resize(ref buffer, capacity);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031** L_5 = (&__this->___buffer_1);
		int32_t L_6 = V_0;
		Array_Resize_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_m82FEC5823560947D2B12C8D675AED2C190DF4F3F(L_5, L_6, Array_Resize_TisByte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_m82FEC5823560947D2B12C8D675AED2C190DF4F3F_RuntimeMethod_var);
	}

IL_0028:
	{
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* NetworkReader_ReadBytes_mA1E53425AAD4AD3038C9759F6971533248347130_inline (NetworkReader_t85516183CC81F0937A89B9B6A92FE815896935E1* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___bytes0, int32_t ___count1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (count > bytes.Length)
		int32_t L_0 = ___count1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = ___bytes0;
		NullCheck(L_1);
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))))
		{
			goto IL_0024;
		}
	}
	{
		// throw new EndOfStreamException($"ReadBytes can't read {count} + bytes because the passed byte[] only has length {bytes.Length}");
		int32_t L_2 = ___count1;
		int32_t L_3 = L_2;
		RuntimeObject* L_4 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_3);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_5 = ___bytes0;
		NullCheck(L_5);
		int32_t L_6 = ((int32_t)(((RuntimeArray*)L_5)->max_length));
		RuntimeObject* L_7 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_6);
		String_t* L_8;
		L_8 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC1B42A5ACC3B7F06923EA0BF4A5C01ED39F8C63B)), L_4, L_7, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_9 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_9, L_8, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReader_ReadBytes_mA1E53425AAD4AD3038C9759F6971533248347130_RuntimeMethod_var)));
	}

IL_0024:
	{
		// if (Position + count > buffer.Count)
		int32_t L_10 = __this->___Position_1;
		int32_t L_11 = ___count1;
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_12 = (&__this->___buffer_0);
		int32_t L_13;
		L_13 = ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_inline(L_12, ArraySegment_1_get_Count_m7B026228B16D905890B805EA70E9114D1517B053_RuntimeMethod_var);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(L_10, L_11))) <= ((int32_t)L_13)))
		{
			goto IL_0055;
		}
	}
	{
		// throw new EndOfStreamException($"ReadBytesSegment can't read {count} bytes because it would read past the end of the stream. {ToString()}");
		int32_t L_14 = ___count1;
		int32_t L_15 = L_14;
		RuntimeObject* L_16 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_15);
		String_t* L_17;
		L_17 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		String_t* L_18;
		L_18 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral97DBD483FF6D25D8A2CF2D9700D08EB0CFDD00D4)), L_16, L_17, NULL);
		EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028* L_19 = (EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_t6B6A2609418A69523CBEF305228B18E0E5778028_il2cpp_TypeInfo_var)));
		NullCheck(L_19);
		EndOfStreamException__ctor_m5629E1A514051A3D56052BD6D2D50C054308CCA4(L_19, L_18, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NetworkReader_ReadBytes_mA1E53425AAD4AD3038C9759F6971533248347130_RuntimeMethod_var)));
	}

IL_0055:
	{
		// Array.Copy(buffer.Array, buffer.Offset + Position, bytes, 0, count);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_20 = (&__this->___buffer_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_21;
		L_21 = ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_inline(L_20, ArraySegment_1_get_Array_m85F374406C1E34FDEFA7F160336A247891AF8105_RuntimeMethod_var);
		ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093* L_22 = (&__this->___buffer_0);
		int32_t L_23;
		L_23 = ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_inline(L_22, ArraySegment_1_get_Offset_m28FEFF65E8FA9A92DF84966071346BFD426CC3AA_RuntimeMethod_var);
		int32_t L_24 = __this->___Position_1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_25 = ___bytes0;
		int32_t L_26 = ___count1;
		Array_Copy_m2CC3EA1129E9B8EA82E6FA31EDE0D4F87BF67EC7((RuntimeArray*)L_21, ((int32_t)il2cpp_codegen_add(L_23, L_24)), (RuntimeArray*)L_25, 0, L_26, NULL);
		// Position += count;
		int32_t L_27 = __this->___Position_1;
		int32_t L_28 = ___count1;
		__this->___Position_1 = ((int32_t)il2cpp_codegen_add(L_27, L_28));
		// return bytes;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_29 = ___bytes0;
		return L_29;
	}
}

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* DeflateStreamNative_UnmanagedRead_m79E9628F6FF91A3995491997242DD83046C9B704_RuntimeMethod_var;
extern const RuntimeMethod* DeflateStreamNative_UnmanagedWrite_m509AE153ECB916CDF755432335FBAB2B51CA1486_RuntimeMethod_var;
extern const RuntimeMethod* MonoBtlsBioMono_Control_mE6F92D680A96B8B446D566D221BF925954FF43BB_RuntimeMethod_var;
extern const RuntimeMethod* MonoBtlsBioMono_OnRead_mE0FF0BFC134CD0F923BBC555E346B7DD7CAD8988_RuntimeMethod_var;
extern const RuntimeMethod* MonoBtlsBioMono_OnWrite_m605A12A8B0CE9517927656981580BFA330BED613_RuntimeMethod_var;
extern const RuntimeMethod* MonoBtlsSslCtx_NativeSelectCallback_m6CA34F71A6093C7260A9F066D6C9FB3CD0CC86DB_RuntimeMethod_var;
extern const RuntimeMethod* MonoBtlsSslCtx_NativeServerNameCallback_mFF311CE931F6A075940D1B6828D1FB517AF03838_RuntimeMethod_var;
extern const RuntimeMethod* MonoBtlsSslCtx_NativeVerifyCallback_mFDF10D01CAAD040E50EE6F53FC1D4155EF696218_RuntimeMethod_var;
extern const RuntimeMethod* MonoBtlsSsl_PrintErrorsCallback_mF161ADF7DBDB81C47BDD587C87E1275DEE5B7A45_RuntimeMethod_var;
extern const RuntimeMethod* MonoBtlsX509LookupMono_OnGetBySubject_m15FE6FBEE9849C324E8E099172A581EABEB83FB3_RuntimeMethod_var;
extern const RuntimeMethod* UnityTlsContext_CertificateCallback_mF5E626BA2545CFFA64428622678E409702C2045A_RuntimeMethod_var;
extern const RuntimeMethod* UnityTlsContext_ReadCallback_m15F3A217E44D480CAED06EB5A88503FB6259D7EC_RuntimeMethod_var;
extern const RuntimeMethod* UnityTlsContext_VerifyCallback_m75D7C072718405EBBF8A2A9C794C4DDFB2595BD0_RuntimeMethod_var;
extern const RuntimeMethod* UnityTlsContext_WriteCallback_m53DD70115C97432A676F3E437E8FB42F9FC068F5_RuntimeMethod_var;
extern const RuntimeMethod* UnityTlsProvider_x509verify_callback_m47823254E133F7304ABFAE5A1F0D218402BC5B45_RuntimeMethod_var;



// 0x00000001 Mono.X509PalImpl Mono.SystemCertificateProvider::GetX509Pal()
extern void SystemCertificateProvider_GetX509Pal_m25337E8316D705CEC31B70783EFE75F0F72256D4 (void);
// 0x00000002 System.Void Mono.SystemCertificateProvider::EnsureInitialized()
extern void SystemCertificateProvider_EnsureInitialized_m5270DDA49FA6EA014CBA47A3AD13FB9A4C99B206 (void);
// 0x00000003 Mono.X509PalImpl Mono.SystemCertificateProvider::get_X509Pal()
extern void SystemCertificateProvider_get_X509Pal_m0E8EA8BF5F1C65CA33C3C4E0FA723E93DFF74B97 (void);
// 0x00000004 System.Security.Cryptography.X509Certificates.X509CertificateImpl Mono.SystemCertificateProvider::Import(System.Byte[],Mono.CertificateImportFlags)
extern void SystemCertificateProvider_Import_m3349082A12B7F5513069393B9EE77CF5264AB0FA (void);
// 0x00000005 System.Security.Cryptography.X509Certificates.X509CertificateImpl Mono.SystemCertificateProvider::Mono.ISystemCertificateProvider.Import(System.Byte[],Microsoft.Win32.SafeHandles.SafePasswordHandle,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags,Mono.CertificateImportFlags)
extern void SystemCertificateProvider_Mono_ISystemCertificateProvider_Import_m4D2602C705772487B0FF370D6CB704324FDBE98F (void);
// 0x00000006 System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.SystemCertificateProvider::Import(System.Byte[],Microsoft.Win32.SafeHandles.SafePasswordHandle,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags,Mono.CertificateImportFlags)
extern void SystemCertificateProvider_Import_m530888F3CEA00BE89B9AF47DAFCF03E319A5AD73 (void);
// 0x00000007 System.Security.Cryptography.X509Certificates.X509CertificateImpl Mono.SystemCertificateProvider::Mono.ISystemCertificateProvider.Import(System.Security.Cryptography.X509Certificates.X509Certificate,Mono.CertificateImportFlags)
extern void SystemCertificateProvider_Mono_ISystemCertificateProvider_Import_mD7F4BAB2B4498500CD48A645D674188BFC30F036 (void);
// 0x00000008 System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.SystemCertificateProvider::Import(System.Security.Cryptography.X509Certificates.X509Certificate,Mono.CertificateImportFlags)
extern void SystemCertificateProvider_Import_mC81F89F5BDFCF15698FDCEFAAEF6ACE1F6CB585C (void);
// 0x00000009 System.Void Mono.SystemCertificateProvider::.ctor()
extern void SystemCertificateProvider__ctor_m28122065D2285B598B1F145BC122693E14F722AC (void);
// 0x0000000A System.Void Mono.SystemCertificateProvider::.cctor()
extern void SystemCertificateProvider__cctor_m03BD266351742E571EA0605CB05A4842B1999803 (void);
// 0x0000000B Mono.SystemDependencyProvider Mono.SystemDependencyProvider::get_Instance()
extern void SystemDependencyProvider_get_Instance_m410F141F466665CEBA34A72F26D78C77A8B66719 (void);
// 0x0000000C System.Void Mono.SystemDependencyProvider::Initialize()
extern void SystemDependencyProvider_Initialize_mC10ED912B2EC028539CA83BE2BAE4EE1E237E835 (void);
// 0x0000000D Mono.ISystemCertificateProvider Mono.SystemDependencyProvider::Mono.ISystemDependencyProvider.get_CertificateProvider()
extern void SystemDependencyProvider_Mono_ISystemDependencyProvider_get_CertificateProvider_mEEA1F11DB63C54CCFAE6EA976D5B349719FEACCF (void);
// 0x0000000E Mono.SystemCertificateProvider Mono.SystemDependencyProvider::get_CertificateProvider()
extern void SystemDependencyProvider_get_CertificateProvider_mEC19F34D14EB2B7BF5F8973DBC3BF3EAD5B1A19D (void);
// 0x0000000F Mono.X509PalImpl Mono.SystemDependencyProvider::get_X509Pal()
extern void SystemDependencyProvider_get_X509Pal_m4982FB6EE530F77FB076AD4D21A57BBEDCA8D961 (void);
// 0x00000010 System.Void Mono.SystemDependencyProvider::.ctor()
extern void SystemDependencyProvider__ctor_mB851EC1408E241AB573D319D5BFCC1E95CF40343 (void);
// 0x00000011 System.Void Mono.SystemDependencyProvider::.cctor()
extern void SystemDependencyProvider__cctor_mFFD9148E4D54BCAB5E5244CA21AE22F8032D4D5A (void);
// 0x00000012 Mono.X509PalImpl Mono.X509Pal::get_Instance()
extern void X509Pal_get_Instance_mBA83FEDE77C97026665ED6DCDDBDC047CBA632C3 (void);
// 0x00000013 System.Security.Cryptography.X509Certificates.X509CertificateImpl Mono.X509PalImplMono::Import(System.Byte[])
extern void X509PalImplMono_Import_m72547BAC14066F54342B6D09539D358F6F0F045B (void);
// 0x00000014 System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.X509PalImplMono::Import(System.Byte[],Microsoft.Win32.SafeHandles.SafePasswordHandle,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern void X509PalImplMono_Import_m3AC55B3BC7FEDCC4872F8D51A9229333470AA19E (void);
// 0x00000015 System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.X509PalImplMono::Import(System.Security.Cryptography.X509Certificates.X509Certificate)
extern void X509PalImplMono_Import_m152053C1A07898B4096D6E1537AFF88EE8AB9633 (void);
// 0x00000016 System.Void Mono.X509PalImplMono::.ctor()
extern void X509PalImplMono__ctor_m8DEF93E123E01393942BB5A18755A221C588DD47 (void);
// 0x00000017 System.Security.Cryptography.X509Certificates.X509CertificateImpl Mono.X509PalImpl::Import(System.Byte[])
// 0x00000018 System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.X509PalImpl::Import(System.Byte[],Microsoft.Win32.SafeHandles.SafePasswordHandle,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
// 0x00000019 System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.X509PalImpl::Import(System.Security.Cryptography.X509Certificates.X509Certificate)
// 0x0000001A System.Byte[] Mono.X509PalImpl::PEM(System.String,System.Byte[])
extern void X509PalImpl_PEM_m21975D897B5389B6BA20C629AAFF37691DEE76A2 (void);
// 0x0000001B System.Byte[] Mono.X509PalImpl::ConvertData(System.Byte[])
extern void X509PalImpl_ConvertData_mD638DA0765DA08BFD8FF87884A2BFCC9883131DF (void);
// 0x0000001C System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.X509PalImpl::ImportFallback(System.Byte[])
extern void X509PalImpl_ImportFallback_m7BF753657B715800F1F182A3B7198578D197DAC3 (void);
// 0x0000001D System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.X509PalImpl::ImportFallback(System.Byte[],Microsoft.Win32.SafeHandles.SafePasswordHandle,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern void X509PalImpl_ImportFallback_mA21AA40BF7FE8E3E3F62ADBEBBBF415DC9BC588E (void);
// 0x0000001E System.Boolean Mono.X509PalImpl::get_SupportsLegacyBasicConstraintsExtension()
extern void X509PalImpl_get_SupportsLegacyBasicConstraintsExtension_mA7FF5FA4ABFF9B8A028FD3937636C6E4D7AE2F38 (void);
// 0x0000001F System.Security.Cryptography.X509Certificates.X509ContentType Mono.X509PalImpl::GetCertContentType(System.Byte[])
extern void X509PalImpl_GetCertContentType_m41DC755893135DB1ACF78832BDAB201C46AC6F99 (void);
// 0x00000020 System.Void Mono.X509PalImpl::.ctor()
extern void X509PalImpl__ctor_m29605E82202DAC5123F4B35D87719454190299C0 (void);
// 0x00000021 System.Void Mono.X509PalImpl::.cctor()
extern void X509PalImpl__cctor_mDA462E335386A1004AE32139C485AA0D6474DA27 (void);
// 0x00000022 System.Void Mono.Util.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern void MonoPInvokeCallbackAttribute__ctor_m8B720CC4ADB295EF6F98ECB4B14AB5010CBC8C28 (void);
// 0x00000023 System.Void Mono.Unity.CertHelper::AddCertificatesToNativeChain(Mono.Unity.UnityTls/unitytls_x509list*,System.Security.Cryptography.X509Certificates.X509CertificateCollection,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void CertHelper_AddCertificatesToNativeChain_m6A078F5FFEA16184F19AEEDD4F9C8E00F68BDD48 (void);
// 0x00000024 System.Void Mono.Unity.CertHelper::AddCertificateToNativeChain(Mono.Unity.UnityTls/unitytls_x509list*,System.Security.Cryptography.X509Certificates.X509Certificate,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void CertHelper_AddCertificateToNativeChain_m04D6F01E00DF4D0C134704DF9403167E1878675A (void);
// 0x00000025 System.Void Mono.Unity.Debug::CheckAndThrow(Mono.Unity.UnityTls/unitytls_errorstate,System.String,Mono.Security.Interface.AlertDescription)
extern void Debug_CheckAndThrow_m0DC4C61C1A5FA2DF49E11420953CD3032843D59F (void);
// 0x00000026 System.Void Mono.Unity.Debug::CheckAndThrow(Mono.Unity.UnityTls/unitytls_errorstate,Mono.Unity.UnityTls/unitytls_x509verify_result,System.String,Mono.Security.Interface.AlertDescription)
extern void Debug_CheckAndThrow_m47BE0492786EEE30351AF6662609556CABAB0A80 (void);
// 0x00000027 System.IntPtr Mono.Unity.UnityTls::GetUnityTlsInterface()
extern void UnityTls_GetUnityTlsInterface_m47FB014BA17E899E2AE904DE3F59A84763D924F0 (void);
// 0x00000028 System.Boolean Mono.Unity.UnityTls::get_IsSupported()
extern void UnityTls_get_IsSupported_m7A43EE578D088D942479C11C7A1718432ED1F048 (void);
// 0x00000029 Mono.Unity.UnityTls/unitytls_interface_struct Mono.Unity.UnityTls::get_NativeInterface()
extern void UnityTls_get_NativeInterface_m48CD53391B9897F81D41D180A899D4C48F9B374B (void);
// 0x0000002A System.Void Mono.Unity.UnityTls/unitytls_x509verify_callback::.ctor(System.Object,System.IntPtr)
extern void unitytls_x509verify_callback__ctor_mEDC6857514FD696562638B3F1DFE959920D1EB26 (void);
// 0x0000002B Mono.Unity.UnityTls/unitytls_x509verify_result Mono.Unity.UnityTls/unitytls_x509verify_callback::Invoke(System.Void*,Mono.Unity.UnityTls/unitytls_x509_ref,Mono.Unity.UnityTls/unitytls_x509verify_result,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_x509verify_callback_Invoke_mB53E808E78AE5BD02CECCA0BE198295ECAB83B9A (void);
// 0x0000002C System.Void Mono.Unity.UnityTls/unitytls_tlsctx_write_callback::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_write_callback__ctor_m1A216E476EA91B415C0FD89765E6560410A7F7F5 (void);
// 0x0000002D System.IntPtr Mono.Unity.UnityTls/unitytls_tlsctx_write_callback::Invoke(System.Void*,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_write_callback_Invoke_mF1F1D27A9BE14950A905685370EDAE5EAB051712 (void);
// 0x0000002E System.Void Mono.Unity.UnityTls/unitytls_tlsctx_read_callback::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_read_callback__ctor_mEAC57775C6AB86A2E9E6DDC440E27C845BFAA4C0 (void);
// 0x0000002F System.IntPtr Mono.Unity.UnityTls/unitytls_tlsctx_read_callback::Invoke(System.Void*,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_read_callback_Invoke_m0B21AFD4EACBDBEC6EEFBEB61E142051F338194B (void);
// 0x00000030 System.Void Mono.Unity.UnityTls/unitytls_tlsctx_trace_callback::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_trace_callback__ctor_mB9CC97640B55911851D544F3F85CAFB5B85B647A (void);
// 0x00000031 System.Void Mono.Unity.UnityTls/unitytls_tlsctx_trace_callback::Invoke(System.Void*,Mono.Unity.UnityTls/unitytls_tlsctx*,System.Byte*,System.IntPtr)
extern void unitytls_tlsctx_trace_callback_Invoke_m9511E6D873C0FFE4EBDA45E52CA47228A483A01F (void);
// 0x00000032 System.Void Mono.Unity.UnityTls/unitytls_tlsctx_certificate_callback::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_certificate_callback__ctor_m9A3F05E07D9A69B8FF419421DF8167DF9CE7E77A (void);
// 0x00000033 System.Void Mono.Unity.UnityTls/unitytls_tlsctx_certificate_callback::Invoke(System.Void*,Mono.Unity.UnityTls/unitytls_tlsctx*,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_x509name*,System.IntPtr,Mono.Unity.UnityTls/unitytls_x509list_ref*,Mono.Unity.UnityTls/unitytls_key_ref*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_certificate_callback_Invoke_m37CB366C6D6E0A27CE4E1E4943DF2DE1F32E2BF1 (void);
// 0x00000034 System.Void Mono.Unity.UnityTls/unitytls_tlsctx_x509verify_callback::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_x509verify_callback__ctor_m8583619DBB13F6EDF2A385C37DFAE8796EB89C0F (void);
// 0x00000035 Mono.Unity.UnityTls/unitytls_x509verify_result Mono.Unity.UnityTls/unitytls_tlsctx_x509verify_callback::Invoke(System.Void*,Mono.Unity.UnityTls/unitytls_x509list_ref,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_x509verify_callback_Invoke_m1F7A6DA53EF61AC64F2DD9A33AF40541F97C6874 (void);
// 0x00000036 System.Void Mono.Unity.UnityTls/unitytls_interface_struct::.ctor()
extern void unitytls_interface_struct__ctor_mCBFC829FE6CEA8E6545EA7B61E756FFA2EC4A22D (void);
// 0x00000037 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_errorstate_create_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_errorstate_create_t__ctor_m291CCEF12E6C9729662E0BBECB5D4FA6BDBA2F02 (void);
// 0x00000038 Mono.Unity.UnityTls/unitytls_errorstate Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_errorstate_create_t::Invoke()
extern void unitytls_errorstate_create_t_Invoke_m8B9A9835EA7C6A11E1627CA8904F3F853FB61BC0 (void);
// 0x00000039 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_errorstate_raise_error_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_errorstate_raise_error_t__ctor_mF30A64F84A8FC246DD062993A45C038B30FC6009 (void);
// 0x0000003A System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_errorstate_raise_error_t::Invoke(Mono.Unity.UnityTls/unitytls_errorstate*,Mono.Unity.UnityTls/unitytls_error_code)
extern void unitytls_errorstate_raise_error_t_Invoke_m9B2B3433846A318340EDF0ED0E33C45030CCEA02 (void);
// 0x0000003B System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_get_ref_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_key_get_ref_t__ctor_mEDA7937D1EC76342E6CE92AF394E7E1A37F664B1 (void);
// 0x0000003C Mono.Unity.UnityTls/unitytls_key_ref Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_get_ref_t::Invoke(Mono.Unity.UnityTls/unitytls_key*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_key_get_ref_t_Invoke_mF5D3582922E868C42BCF32EB14471F8C800BB66E (void);
// 0x0000003D System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_parse_der_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_key_parse_der_t__ctor_m25AD8C01A87CF5F737E649F1FB9CA340A9A832A8 (void);
// 0x0000003E Mono.Unity.UnityTls/unitytls_key* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_parse_der_t::Invoke(System.Byte*,System.IntPtr,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_key_parse_der_t_Invoke_m244086296EBDE0C0B0F072B33AAD5BA39A030DB6 (void);
// 0x0000003F System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_parse_pem_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_key_parse_pem_t__ctor_m8F4D69BD7B0988BF7682B3E5EA5934141BFEE440 (void);
// 0x00000040 Mono.Unity.UnityTls/unitytls_key* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_parse_pem_t::Invoke(System.Byte*,System.IntPtr,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_key_parse_pem_t_Invoke_mE842DC1436977A2982224694DB5BCC2FF8A9B331 (void);
// 0x00000041 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_free_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_key_free_t__ctor_m935A5571CE60C369950D58C79432FB1C50231C37 (void);
// 0x00000042 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_key_free_t::Invoke(Mono.Unity.UnityTls/unitytls_key*)
extern void unitytls_key_free_t_Invoke_m860A3C396228B7ADF9406AE78D439BDCD12FB98E (void);
// 0x00000043 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509_export_der_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_x509_export_der_t__ctor_m9BF2B0B567856AE42AB0ED274CD26ACE5FC0186B (void);
// 0x00000044 System.IntPtr Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509_export_der_t::Invoke(Mono.Unity.UnityTls/unitytls_x509_ref,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_x509_export_der_t_Invoke_m13DB2E205EF9ED37B75F5B763C5080E7077841AD (void);
// 0x00000045 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_get_ref_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_x509list_get_ref_t__ctor_m6977636DC288FB0809D7C0CE8D9D31D25795F77C (void);
// 0x00000046 Mono.Unity.UnityTls/unitytls_x509list_ref Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_get_ref_t::Invoke(Mono.Unity.UnityTls/unitytls_x509list*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_x509list_get_ref_t_Invoke_mE7C675B7847FFEF96C25AE757D34CE920AA16EC2 (void);
// 0x00000047 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_get_x509_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_x509list_get_x509_t__ctor_mFF7BC99AB91FE7F9CCB586988B246F597ADF53DD (void);
// 0x00000048 Mono.Unity.UnityTls/unitytls_x509_ref Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_get_x509_t::Invoke(Mono.Unity.UnityTls/unitytls_x509list_ref,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_x509list_get_x509_t_Invoke_m9080273A34FE768B80024B96001D5B7451A46EC0 (void);
// 0x00000049 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_create_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_x509list_create_t__ctor_m41F2B1E6C767CD3A39F4E0FAD41B678AF61A14A6 (void);
// 0x0000004A Mono.Unity.UnityTls/unitytls_x509list* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_create_t::Invoke(Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_x509list_create_t_Invoke_m05D8719D913109E4997FF913015E66455BC3C7D3 (void);
// 0x0000004B System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_x509list_append_t__ctor_m22FAC52BA432FBCF2048DA6236633970E4E18CB0 (void);
// 0x0000004C System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_t::Invoke(Mono.Unity.UnityTls/unitytls_x509list*,Mono.Unity.UnityTls/unitytls_x509_ref,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_x509list_append_t_Invoke_mEF1811CFAD02743F745D8A8E09E2E84D68154976 (void);
// 0x0000004D System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_der_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_x509list_append_der_t__ctor_mB33965F4253393C8B17E44DACA1ECDBCC17EB673 (void);
// 0x0000004E System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_append_der_t::Invoke(Mono.Unity.UnityTls/unitytls_x509list*,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_x509list_append_der_t_Invoke_m3D900F0F07C8747B9F046512DAC7579D0C7CD193 (void);
// 0x0000004F System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_free_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_x509list_free_t__ctor_mA156855741A0AA67C20B87507BE55F188ECBFFD4 (void);
// 0x00000050 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509list_free_t::Invoke(Mono.Unity.UnityTls/unitytls_x509list*)
extern void unitytls_x509list_free_t_Invoke_m0974FBCD7B6920A4EC57BDCB080E1A7307EEEA02 (void);
// 0x00000051 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509verify_default_ca_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_x509verify_default_ca_t__ctor_m936B931A7A5A10B4A514630F84CF136A799AFA23 (void);
// 0x00000052 Mono.Unity.UnityTls/unitytls_x509verify_result Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509verify_default_ca_t::Invoke(Mono.Unity.UnityTls/unitytls_x509list_ref,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_x509verify_callback,System.Void*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_x509verify_default_ca_t_Invoke_m1D112FD2010361D2DB08F08A598000ADCD0875C3 (void);
// 0x00000053 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509verify_explicit_ca_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_x509verify_explicit_ca_t__ctor_m1812F92F4C92CE54233697CE380177EBA629A1F1 (void);
// 0x00000054 Mono.Unity.UnityTls/unitytls_x509verify_result Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_x509verify_explicit_ca_t::Invoke(Mono.Unity.UnityTls/unitytls_x509list_ref,Mono.Unity.UnityTls/unitytls_x509list_ref,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_x509verify_callback,System.Void*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_x509verify_explicit_ca_t_Invoke_m4028A9AE8FD6B69DB36BAE68CC3A3362A825738B (void);
// 0x00000055 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_create_server_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_create_server_t__ctor_m035F9FCC9C4B71D3BA4F1610FBCFB7E87E28EDD7 (void);
// 0x00000056 Mono.Unity.UnityTls/unitytls_tlsctx* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_create_server_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx_protocolrange,Mono.Unity.UnityTls/unitytls_tlsctx_callbacks,System.UInt64,System.UInt64,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_create_server_t_Invoke_mCEB3E8CADCB316F4DF606A68182A4E5C9EC4D3B9 (void);
// 0x00000057 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_create_client_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_create_client_t__ctor_m898460A0D30C6EEF737F445DFBA96076E4D22130 (void);
// 0x00000058 Mono.Unity.UnityTls/unitytls_tlsctx* Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_create_client_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx_protocolrange,Mono.Unity.UnityTls/unitytls_tlsctx_callbacks,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_create_client_t_Invoke_m728BCBF32A45326A1AD8B1E17933372F7D9E01EE (void);
// 0x00000059 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_server_require_client_authentication_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_server_require_client_authentication_t__ctor_m3E84F3D43AA2928DA825256C6179E2D0CF926C60 (void);
// 0x0000005A System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_server_require_client_authentication_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx*,Mono.Unity.UnityTls/unitytls_x509list_ref,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_server_require_client_authentication_t_Invoke_m0B6DAC7CA95263D1245C9B0A466AF00D073059EB (void);
// 0x0000005B System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_certificate_callback_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_set_certificate_callback_t__ctor_m31E0497908A2C272B9AB80B4659D1517F9647E20 (void);
// 0x0000005C System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_certificate_callback_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx*,Mono.Unity.UnityTls/unitytls_tlsctx_certificate_callback,System.Void*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_set_certificate_callback_t_Invoke_mFC277320977BC310A0EB484EE505E25B6E2C7CA3 (void);
// 0x0000005D System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_trace_callback_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_set_trace_callback_t__ctor_m262F20124214D62819468C226E925221D2527D96 (void);
// 0x0000005E System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_trace_callback_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx*,Mono.Unity.UnityTls/unitytls_tlsctx_trace_callback,System.Void*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_set_trace_callback_t_Invoke_mC98468DD35AA05DE628FEAE7669B4F71205CCBD7 (void);
// 0x0000005F System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_x509verify_callback_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_set_x509verify_callback_t__ctor_mE6C3A39FA8A1903C74826A5BFF748DFEB1A4D06D (void);
// 0x00000060 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_x509verify_callback_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx*,Mono.Unity.UnityTls/unitytls_tlsctx_x509verify_callback,System.Void*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_set_x509verify_callback_t_Invoke_mA9AE094A7DB71B4B7B051BB3D67CB98A7F48D3CE (void);
// 0x00000061 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_supported_ciphersuites_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_set_supported_ciphersuites_t__ctor_m856B5362E3479A5C75709F62B5A316C0A0F913FF (void);
// 0x00000062 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_set_supported_ciphersuites_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx*,Mono.Unity.UnityTls/unitytls_ciphersuite*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_set_supported_ciphersuites_t_Invoke_m8E153461EFCEAAAF59D1E918F7C06B14358B43E2 (void);
// 0x00000063 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_get_ciphersuite_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_get_ciphersuite_t__ctor_mFD8DF58601BB4A8F00C3A56F881F2E92D7536CA9 (void);
// 0x00000064 Mono.Unity.UnityTls/unitytls_ciphersuite Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_get_ciphersuite_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_get_ciphersuite_t_Invoke_m2DF155D62800CFFD66AC63538F69DB2275EC3FFD (void);
// 0x00000065 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_get_protocol_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_get_protocol_t__ctor_mAA5710F913B95E0964AD90EDF59FD71E984DD00E (void);
// 0x00000066 Mono.Unity.UnityTls/unitytls_protocol Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_get_protocol_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_get_protocol_t_Invoke_m3D931DDD241F0B3EDA8F6F1EFA159C0669E7E3F0 (void);
// 0x00000067 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_process_handshake_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_process_handshake_t__ctor_m163A6D53CE45B5161FD5586DBBD8BB90B9EDDA45 (void);
// 0x00000068 Mono.Unity.UnityTls/unitytls_x509verify_result Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_process_handshake_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_process_handshake_t_Invoke_mD8F2422C5C6C9FF0ADDAB17AECB998C2E225CF7A (void);
// 0x00000069 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_read_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_read_t__ctor_m55B52EA0C5ED94A8030D80A9B7967F34850AC799 (void);
// 0x0000006A System.IntPtr Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_read_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx*,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_read_t_Invoke_m0FB34952D7592128A211AA0DA5536A325494FA04 (void);
// 0x0000006B System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_write_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_write_t__ctor_mD229A38085F7CAE944D6CF0D505C6F167C709E96 (void);
// 0x0000006C System.IntPtr Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_write_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx*,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_write_t_Invoke_mC6A06FBF8E4C5652830248B521C83865B3F2DBBC (void);
// 0x0000006D System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_notify_close_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_notify_close_t__ctor_m700B77B204CFF6F4113F3E3F4CF6673CF7ACC0FB (void);
// 0x0000006E System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_notify_close_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_tlsctx_notify_close_t_Invoke_m6E5688DDA03F313A1C470E4C30263C5B4A174138 (void);
// 0x0000006F System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_free_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_tlsctx_free_t__ctor_m46E06E2335A31589194453E910356B6456160E7E (void);
// 0x00000070 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_tlsctx_free_t::Invoke(Mono.Unity.UnityTls/unitytls_tlsctx*)
extern void unitytls_tlsctx_free_t_Invoke_mC5D5191740D2507A83D95B4B3E6CE4C4A9FBAFD2 (void);
// 0x00000071 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_random_generate_bytes_t::.ctor(System.Object,System.IntPtr)
extern void unitytls_random_generate_bytes_t__ctor_mFAC4CCD07765DE0827186AEC35DCCFC2E66BE066 (void);
// 0x00000072 System.Void Mono.Unity.UnityTls/unitytls_interface_struct/unitytls_random_generate_bytes_t::Invoke(System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void unitytls_random_generate_bytes_t_Invoke_mF0A5B5E7279FF32FBFF48DBB98EB07C88C63EF92 (void);
// 0x00000073 System.Void Mono.Unity.UnityTlsContext::.ctor(Mono.Net.Security.MobileAuthenticatedStream,Mono.Net.Security.MonoSslAuthenticationOptions)
extern void UnityTlsContext__ctor_m4B8251E305EF5AC2CCB5D963BCB2F61D0F18408C (void);
// 0x00000074 System.Void Mono.Unity.UnityTlsContext::ExtractNativeKeyAndChainFromManagedCertificate(System.Security.Cryptography.X509Certificates.X509Certificate,Mono.Unity.UnityTls/unitytls_errorstate*,Mono.Unity.UnityTls/unitytls_x509list*&,Mono.Unity.UnityTls/unitytls_key*&)
extern void UnityTlsContext_ExtractNativeKeyAndChainFromManagedCertificate_mAAD3045A2D64C314FD288C3468D41767DD47205A (void);
// 0x00000075 System.Boolean Mono.Unity.UnityTlsContext::get_IsAuthenticated()
extern void UnityTlsContext_get_IsAuthenticated_mA566E1D02B55C267233595651ACBDE0AB14D0361 (void);
// 0x00000076 System.Security.Cryptography.X509Certificates.X509Certificate2 Mono.Unity.UnityTlsContext::get_RemoteCertificate()
extern void UnityTlsContext_get_RemoteCertificate_m01F83F7747AEA21356AF35E31870F03909474FA7 (void);
// 0x00000077 System.Void Mono.Unity.UnityTlsContext::Flush()
extern void UnityTlsContext_Flush_mBE19169C723528F75B3A13B17ECB1A36690E27C6 (void);
// 0x00000078 System.ValueTuple`2<System.Int32,System.Boolean> Mono.Unity.UnityTlsContext::Read(System.Byte[],System.Int32,System.Int32)
extern void UnityTlsContext_Read_m461E85BF4B42B72297B42A6E7EE5A756A25259FD (void);
// 0x00000079 System.ValueTuple`2<System.Int32,System.Boolean> Mono.Unity.UnityTlsContext::Write(System.Byte[],System.Int32,System.Int32)
extern void UnityTlsContext_Write_m11529FF555E48CA82577431E6856FEE3E4E0FA28 (void);
// 0x0000007A System.Void Mono.Unity.UnityTlsContext::Renegotiate()
extern void UnityTlsContext_Renegotiate_m5E5182B59BAA90319DC59CD711DDE930C39BEB75 (void);
// 0x0000007B System.Boolean Mono.Unity.UnityTlsContext::PendingRenegotiation()
extern void UnityTlsContext_PendingRenegotiation_m9F5831ECA8A6BBA97585A08ED9F51CA08F502187 (void);
// 0x0000007C System.Void Mono.Unity.UnityTlsContext::Shutdown()
extern void UnityTlsContext_Shutdown_m6DD342ACE3206195381E15E5CD5BB4845CDBDBE6 (void);
// 0x0000007D System.Void Mono.Unity.UnityTlsContext::Dispose(System.Boolean)
extern void UnityTlsContext_Dispose_m5E0A4A0D9AF08F03E82CBF45BD14477E7D2D34DB (void);
// 0x0000007E System.Void Mono.Unity.UnityTlsContext::StartHandshake()
extern void UnityTlsContext_StartHandshake_m56718DFC2F7D4A03C3B1AD6C9ABE502EDCB6BC0A (void);
// 0x0000007F System.Boolean Mono.Unity.UnityTlsContext::ProcessHandshake()
extern void UnityTlsContext_ProcessHandshake_m098B5D0081E137B05DB4FB7B7F74805EF749C09D (void);
// 0x00000080 System.Void Mono.Unity.UnityTlsContext::FinishHandshake()
extern void UnityTlsContext_FinishHandshake_m4F1BF52160E1BFCF223717415D5F1DB96B5F9556 (void);
// 0x00000081 System.IntPtr Mono.Unity.UnityTlsContext::WriteCallback(System.Void*,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void UnityTlsContext_WriteCallback_m53DD70115C97432A676F3E437E8FB42F9FC068F5 (void);
// 0x00000082 System.IntPtr Mono.Unity.UnityTlsContext::WriteCallback(System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void UnityTlsContext_WriteCallback_m28708B36ED5744CED29D543B86A2E117DFF661BA (void);
// 0x00000083 System.IntPtr Mono.Unity.UnityTlsContext::ReadCallback(System.Void*,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void UnityTlsContext_ReadCallback_m15F3A217E44D480CAED06EB5A88503FB6259D7EC (void);
// 0x00000084 System.IntPtr Mono.Unity.UnityTlsContext::ReadCallback(System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void UnityTlsContext_ReadCallback_mE4765DDC1755E4A030F1D3B59B255072FF656ED1 (void);
// 0x00000085 Mono.Unity.UnityTls/unitytls_x509verify_result Mono.Unity.UnityTlsContext::VerifyCallback(System.Void*,Mono.Unity.UnityTls/unitytls_x509list_ref,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void UnityTlsContext_VerifyCallback_m75D7C072718405EBBF8A2A9C794C4DDFB2595BD0 (void);
// 0x00000086 Mono.Unity.UnityTls/unitytls_x509verify_result Mono.Unity.UnityTlsContext::VerifyCallback(Mono.Unity.UnityTls/unitytls_x509list_ref,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void UnityTlsContext_VerifyCallback_m2498A768960C1A792A1B60A1F47D16624E543537 (void);
// 0x00000087 System.Void Mono.Unity.UnityTlsContext::CertificateCallback(System.Void*,Mono.Unity.UnityTls/unitytls_tlsctx*,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_x509name*,System.IntPtr,Mono.Unity.UnityTls/unitytls_x509list_ref*,Mono.Unity.UnityTls/unitytls_key_ref*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void UnityTlsContext_CertificateCallback_mF5E626BA2545CFFA64428622678E409702C2045A (void);
// 0x00000088 System.Void Mono.Unity.UnityTlsContext::CertificateCallback(Mono.Unity.UnityTls/unitytls_tlsctx*,System.Byte*,System.IntPtr,Mono.Unity.UnityTls/unitytls_x509name*,System.IntPtr,Mono.Unity.UnityTls/unitytls_x509list_ref*,Mono.Unity.UnityTls/unitytls_key_ref*,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void UnityTlsContext_CertificateCallback_m5A4C050D5D49709202B72A50F33533E7C5C6B113 (void);
// 0x00000089 Mono.Unity.UnityTls/unitytls_protocol Mono.Unity.UnityTlsConversions::GetMinProtocol(System.Security.Authentication.SslProtocols)
extern void UnityTlsConversions_GetMinProtocol_m0E691D4227CAB79BDC7F6E30E5CF2698D2F5813F (void);
// 0x0000008A Mono.Unity.UnityTls/unitytls_protocol Mono.Unity.UnityTlsConversions::GetMaxProtocol(System.Security.Authentication.SslProtocols)
extern void UnityTlsConversions_GetMaxProtocol_mC7381248C4DA2168508E3A4C5DE049BE58C7F250 (void);
// 0x0000008B Mono.Security.Interface.TlsProtocols Mono.Unity.UnityTlsConversions::ConvertProtocolVersion(Mono.Unity.UnityTls/unitytls_protocol)
extern void UnityTlsConversions_ConvertProtocolVersion_mB9C0E5A59CC1564EDF56D06C396461DA678CAD0B (void);
// 0x0000008C Mono.Security.Interface.AlertDescription Mono.Unity.UnityTlsConversions::VerifyResultToAlertDescription(Mono.Unity.UnityTls/unitytls_x509verify_result,Mono.Security.Interface.AlertDescription)
extern void UnityTlsConversions_VerifyResultToAlertDescription_m23B95B4CEFBE8FB8329DF4A53E744F9592C40DA6 (void);
// 0x0000008D System.Net.Security.SslPolicyErrors Mono.Unity.UnityTlsConversions::VerifyResultToPolicyErrror(Mono.Unity.UnityTls/unitytls_x509verify_result)
extern void UnityTlsConversions_VerifyResultToPolicyErrror_m6D9F0BBA8B599BF2133E28C4D804472CEDA5AFCF (void);
// 0x0000008E System.Security.Cryptography.X509Certificates.X509ChainStatusFlags Mono.Unity.UnityTlsConversions::VerifyResultToChainStatus(Mono.Unity.UnityTls/unitytls_x509verify_result)
extern void UnityTlsConversions_VerifyResultToChainStatus_m266E87DEE12D19BFD385BCCE2F5F30C527D6DB74 (void);
// 0x0000008F System.String Mono.Unity.UnityTlsProvider::get_Name()
extern void UnityTlsProvider_get_Name_m24407BD8B3C23C4DCF2EDF41F9056463763E7389 (void);
// 0x00000090 System.Guid Mono.Unity.UnityTlsProvider::get_ID()
extern void UnityTlsProvider_get_ID_mE43288EBB589EFFC6EE6E49209FBE8295BE577F1 (void);
// 0x00000091 System.Boolean Mono.Unity.UnityTlsProvider::get_SupportsSslStream()
extern void UnityTlsProvider_get_SupportsSslStream_mBB478D61296FAF8CAC3145C816E27AA7A1CE8CC8 (void);
// 0x00000092 System.Boolean Mono.Unity.UnityTlsProvider::get_SupportsMonoExtensions()
extern void UnityTlsProvider_get_SupportsMonoExtensions_mC34E3757CB6273E3838F8DF33503CF6E4E4ADC01 (void);
// 0x00000093 System.Boolean Mono.Unity.UnityTlsProvider::get_SupportsConnectionInfo()
extern void UnityTlsProvider_get_SupportsConnectionInfo_mF18DD7A3E1806F86AB471A580EEAD68AB736FB73 (void);
// 0x00000094 System.Boolean Mono.Unity.UnityTlsProvider::get_SupportsCleanShutdown()
extern void UnityTlsProvider_get_SupportsCleanShutdown_mBB7DCD0ACF8B5239D89F4695859B90D2F5275D50 (void);
// 0x00000095 System.Security.Authentication.SslProtocols Mono.Unity.UnityTlsProvider::get_SupportedProtocols()
extern void UnityTlsProvider_get_SupportedProtocols_m59C751B343CAC7A9692B1C12FC986450EF700C88 (void);
// 0x00000096 Mono.Net.Security.MobileAuthenticatedStream Mono.Unity.UnityTlsProvider::CreateSslStream(System.Net.Security.SslStream,System.IO.Stream,System.Boolean,Mono.Security.Interface.MonoTlsSettings)
extern void UnityTlsProvider_CreateSslStream_m0B98A540CD6B4AE49FFD79ACCC226E69F21DE848 (void);
// 0x00000097 Mono.Unity.UnityTls/unitytls_x509verify_result Mono.Unity.UnityTlsProvider::x509verify_callback(System.Void*,Mono.Unity.UnityTls/unitytls_x509_ref,Mono.Unity.UnityTls/unitytls_x509verify_result,Mono.Unity.UnityTls/unitytls_errorstate*)
extern void UnityTlsProvider_x509verify_callback_m47823254E133F7304ABFAE5A1F0D218402BC5B45 (void);
// 0x00000098 System.Boolean Mono.Unity.UnityTlsProvider::ValidateCertificate(Mono.Net.Security.ChainValidationHelper,System.String,System.Boolean,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Boolean,System.Security.Cryptography.X509Certificates.X509Chain&,System.Net.Security.SslPolicyErrors&,System.Int32&)
extern void UnityTlsProvider_ValidateCertificate_mE2F233ED2EDE0D248D79D8133BE8F1C9133856A6 (void);
// 0x00000099 System.Void Mono.Unity.UnityTlsProvider::.ctor()
extern void UnityTlsProvider__ctor_m0C11027324407A76EB81EB51C37FF5C8F6F219A8 (void);
// 0x0000009A System.Void Mono.Unity.UnityTlsStream::.ctor(System.IO.Stream,System.Boolean,System.Net.Security.SslStream,Mono.Security.Interface.MonoTlsSettings,Mono.Net.Security.MobileTlsProvider)
extern void UnityTlsStream__ctor_mEE3913BCF1FE718A3C5209FC2AD90D44D312F593 (void);
// 0x0000009B Mono.Net.Security.MobileTlsContext Mono.Unity.UnityTlsStream::CreateContext(Mono.Net.Security.MonoSslAuthenticationOptions)
extern void UnityTlsStream_CreateContext_m113223853E0216291BEE53F8DCEACC7E3BECE451 (void);
// 0x0000009C System.Void Mono.Unity.X509ChainImplUnityTls::.ctor(Mono.Unity.UnityTls/unitytls_x509list_ref,System.Boolean)
extern void X509ChainImplUnityTls__ctor_mD653A56ED8AD47C0A46DB3D89749A055D4BB1ABC (void);
// 0x0000009D System.Boolean Mono.Unity.X509ChainImplUnityTls::get_IsValid()
extern void X509ChainImplUnityTls_get_IsValid_mC01A80C0AF85FC18A5C790EF35586C8503D6B36F (void);
// 0x0000009E Mono.Unity.UnityTls/unitytls_x509list_ref Mono.Unity.X509ChainImplUnityTls::get_NativeCertificateChain()
extern void X509ChainImplUnityTls_get_NativeCertificateChain_mBFE146631BDB57BB66B00EBB21414AC7AFF73E72 (void);
// 0x0000009F System.Security.Cryptography.X509Certificates.X509ChainElementCollection Mono.Unity.X509ChainImplUnityTls::get_ChainElements()
extern void X509ChainImplUnityTls_get_ChainElements_m88A7F21DC30467829291CF89448C65025D7A15A4 (void);
// 0x000000A0 System.Void Mono.Unity.X509ChainImplUnityTls::AddStatus(System.Security.Cryptography.X509Certificates.X509ChainStatusFlags)
extern void X509ChainImplUnityTls_AddStatus_mF8C26841864A662C616E5A94A762EA3A19EF71D4 (void);
// 0x000000A1 System.Security.Cryptography.X509Certificates.X509ChainPolicy Mono.Unity.X509ChainImplUnityTls::get_ChainPolicy()
extern void X509ChainImplUnityTls_get_ChainPolicy_m6FF8496FAA1CCC30DA4691A4CFCC1C6F8E7939B6 (void);
// 0x000000A2 System.Boolean Mono.Unity.X509ChainImplUnityTls::Build(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainImplUnityTls_Build_m9B33FDCF42AE118B28749FDE388231A6E6316768 (void);
// 0x000000A3 System.Void Mono.Unity.X509ChainImplUnityTls::Reset()
extern void X509ChainImplUnityTls_Reset_m1AD27C4C44348EADFBF5C6CF802F8AF60B562474 (void);
// 0x000000A4 System.Void Mono.Unity.X509ChainImplUnityTls::Dispose(System.Boolean)
extern void X509ChainImplUnityTls_Dispose_m8E23D2EF6C322FF694E5E6098A52FB54388FC446 (void);
// 0x000000A5 System.Int32 Mono.Net.Security.BufferOffsetSize::get_EndOffset()
extern void BufferOffsetSize_get_EndOffset_m114650CE0C422157F8EDCD8C0F1B89C5FFE7F9C9 (void);
// 0x000000A6 System.Int32 Mono.Net.Security.BufferOffsetSize::get_Remaining()
extern void BufferOffsetSize_get_Remaining_mB3CB4287B2AAAD7976374B914E95ABFCE71B1A4E (void);
// 0x000000A7 System.Void Mono.Net.Security.BufferOffsetSize::.ctor(System.Byte[],System.Int32,System.Int32)
extern void BufferOffsetSize__ctor_mB62BB1F461998563FB6793F509FF26FB79B81955 (void);
// 0x000000A8 System.String Mono.Net.Security.BufferOffsetSize::ToString()
extern void BufferOffsetSize_ToString_m2C7B9A215EDE5C22ED8C51534365E110BB56C083 (void);
// 0x000000A9 System.Void Mono.Net.Security.BufferOffsetSize2::.ctor(System.Int32)
extern void BufferOffsetSize2__ctor_m773DC687399F95F23BE42179B942303C0B52FF07 (void);
// 0x000000AA System.Void Mono.Net.Security.BufferOffsetSize2::Reset()
extern void BufferOffsetSize2_Reset_m1B80AE03ED0C2BAC447F3BDEC8F827CEE6053E7F (void);
// 0x000000AB System.Void Mono.Net.Security.BufferOffsetSize2::MakeRoom(System.Int32)
extern void BufferOffsetSize2_MakeRoom_mCE9261D07E745C72E3032A9FDC19FA54FEF0BE28 (void);
// 0x000000AC System.Void Mono.Net.Security.BufferOffsetSize2::AppendData(System.Byte[],System.Int32,System.Int32)
extern void BufferOffsetSize2_AppendData_mA85DD811BF73EB8F153F9DB2668C11E907F258A5 (void);
// 0x000000AD System.Int32 Mono.Net.Security.AsyncProtocolResult::get_UserResult()
extern void AsyncProtocolResult_get_UserResult_mEDF85928DEE138CA072E0BADB697024616981ABF (void);
// 0x000000AE System.Runtime.ExceptionServices.ExceptionDispatchInfo Mono.Net.Security.AsyncProtocolResult::get_Error()
extern void AsyncProtocolResult_get_Error_m2A01329BA71151C59E17E3D8812932AD02EF5C05 (void);
// 0x000000AF System.Void Mono.Net.Security.AsyncProtocolResult::.ctor(System.Int32)
extern void AsyncProtocolResult__ctor_m2999944493F7C75F19D5BA3811118ED10A889305 (void);
// 0x000000B0 System.Void Mono.Net.Security.AsyncProtocolResult::.ctor(System.Runtime.ExceptionServices.ExceptionDispatchInfo)
extern void AsyncProtocolResult__ctor_m80187A7036019F9768BA1941362CB6785F10E99F (void);
// 0x000000B1 Mono.Net.Security.MobileAuthenticatedStream Mono.Net.Security.AsyncProtocolRequest::get_Parent()
extern void AsyncProtocolRequest_get_Parent_m20DA5526115969E9D32B61999A044FF0618AFF92 (void);
// 0x000000B2 System.Boolean Mono.Net.Security.AsyncProtocolRequest::get_RunSynchronously()
extern void AsyncProtocolRequest_get_RunSynchronously_m5E10561209866EE44A27D35A1F4762008063B4BF (void);
// 0x000000B3 System.String Mono.Net.Security.AsyncProtocolRequest::get_Name()
extern void AsyncProtocolRequest_get_Name_m3D5DC63A43FFCA24B1E5A9825B76BD391D790F84 (void);
// 0x000000B4 System.Int32 Mono.Net.Security.AsyncProtocolRequest::get_UserResult()
extern void AsyncProtocolRequest_get_UserResult_mA77AFFB0D9A5FD69E5EEC51CB0F80C25E31D0759 (void);
// 0x000000B5 System.Void Mono.Net.Security.AsyncProtocolRequest::set_UserResult(System.Int32)
extern void AsyncProtocolRequest_set_UserResult_mD84274B3348783A6E03B9000BB82E0737E1EC974 (void);
// 0x000000B6 System.Void Mono.Net.Security.AsyncProtocolRequest::.ctor(Mono.Net.Security.MobileAuthenticatedStream,System.Boolean)
extern void AsyncProtocolRequest__ctor_mDF1F675DC3E001867DFFD9B1C14CF5559BE3035A (void);
// 0x000000B7 System.Void Mono.Net.Security.AsyncProtocolRequest::RequestRead(System.Int32)
extern void AsyncProtocolRequest_RequestRead_mD55DC015A50E42EF2FA7DB75FE1E7E3FCFA3CFA0 (void);
// 0x000000B8 System.Void Mono.Net.Security.AsyncProtocolRequest::RequestWrite()
extern void AsyncProtocolRequest_RequestWrite_mA6AB8ACEA16A14CD6A232DA256C417CD183EE69F (void);
// 0x000000B9 System.Threading.Tasks.Task`1<Mono.Net.Security.AsyncProtocolResult> Mono.Net.Security.AsyncProtocolRequest::StartOperation(System.Threading.CancellationToken)
extern void AsyncProtocolRequest_StartOperation_m3BFF3E84B25BBE13881D3592FF4CAEC3736FBEDD (void);
// 0x000000BA System.Threading.Tasks.Task Mono.Net.Security.AsyncProtocolRequest::ProcessOperation(System.Threading.CancellationToken)
extern void AsyncProtocolRequest_ProcessOperation_m7210D0C2351D69FF9FF798860C85FAA7F91C050F (void);
// 0x000000BB System.Threading.Tasks.Task`1<System.Nullable`1<System.Int32>> Mono.Net.Security.AsyncProtocolRequest::InnerRead(System.Threading.CancellationToken)
extern void AsyncProtocolRequest_InnerRead_mF418FD1D9403BFD08A44D00EEAF8E9F3ECD25A02 (void);
// 0x000000BC Mono.Net.Security.AsyncOperationStatus Mono.Net.Security.AsyncProtocolRequest::Run(Mono.Net.Security.AsyncOperationStatus)
// 0x000000BD System.String Mono.Net.Security.AsyncProtocolRequest::ToString()
extern void AsyncProtocolRequest_ToString_m76F3449064B096829BE4288E38F10DF76B124F1F (void);
// 0x000000BE System.Void Mono.Net.Security.AsyncProtocolRequest/<StartOperation>d__23::MoveNext()
extern void U3CStartOperationU3Ed__23_MoveNext_mDE591A3806D9523FC8D2E26ED510CEE7963F8FF2 (void);
// 0x000000BF System.Void Mono.Net.Security.AsyncProtocolRequest/<StartOperation>d__23::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartOperationU3Ed__23_SetStateMachine_mA0C8C029457FAAE036C6098FDC66C98574D0BD03 (void);
// 0x000000C0 System.Void Mono.Net.Security.AsyncProtocolRequest/<ProcessOperation>d__24::MoveNext()
extern void U3CProcessOperationU3Ed__24_MoveNext_mA73F0FD80CE65D2E3E12390E49E3F34CFF47E98B (void);
// 0x000000C1 System.Void Mono.Net.Security.AsyncProtocolRequest/<ProcessOperation>d__24::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CProcessOperationU3Ed__24_SetStateMachine_mA9912D191EE4A99536A61DEED059DC89B765A823 (void);
// 0x000000C2 System.Void Mono.Net.Security.AsyncProtocolRequest/<InnerRead>d__25::MoveNext()
extern void U3CInnerReadU3Ed__25_MoveNext_mC4567E370C71DA9DA0F704C570308D106D4DDB2C (void);
// 0x000000C3 System.Void Mono.Net.Security.AsyncProtocolRequest/<InnerRead>d__25::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CInnerReadU3Ed__25_SetStateMachine_m15DB2E97371A46CBCCD53DCAE7A4FE6F5FF84DCE (void);
// 0x000000C4 System.Void Mono.Net.Security.AsyncHandshakeRequest::.ctor(Mono.Net.Security.MobileAuthenticatedStream,System.Boolean)
extern void AsyncHandshakeRequest__ctor_m741ADA23A424389AD20579B23AE2DBE8FA00CFF7 (void);
// 0x000000C5 Mono.Net.Security.AsyncOperationStatus Mono.Net.Security.AsyncHandshakeRequest::Run(Mono.Net.Security.AsyncOperationStatus)
extern void AsyncHandshakeRequest_Run_m345A76BCFDDE5E054B97BD4F5FE238A05D3F4C26 (void);
// 0x000000C6 Mono.Net.Security.BufferOffsetSize Mono.Net.Security.AsyncReadOrWriteRequest::get_UserBuffer()
extern void AsyncReadOrWriteRequest_get_UserBuffer_mD158BF301713ADC8899B3F3CFF7D18B76A956417 (void);
// 0x000000C7 System.Int32 Mono.Net.Security.AsyncReadOrWriteRequest::get_CurrentSize()
extern void AsyncReadOrWriteRequest_get_CurrentSize_m4230AD05749403B840042BB7531124C6A96980E6 (void);
// 0x000000C8 System.Void Mono.Net.Security.AsyncReadOrWriteRequest::set_CurrentSize(System.Int32)
extern void AsyncReadOrWriteRequest_set_CurrentSize_mBCD300A1CD6FF110A3527E80711A820FEFC2F9E1 (void);
// 0x000000C9 System.Void Mono.Net.Security.AsyncReadOrWriteRequest::.ctor(Mono.Net.Security.MobileAuthenticatedStream,System.Boolean,System.Byte[],System.Int32,System.Int32)
extern void AsyncReadOrWriteRequest__ctor_m4DCAC7C20097985A9A4FDCB73719DD2E508CF07D (void);
// 0x000000CA System.String Mono.Net.Security.AsyncReadOrWriteRequest::ToString()
extern void AsyncReadOrWriteRequest_ToString_m1D1115D6B97BD767A3482863B6B64642D0733696 (void);
// 0x000000CB System.Void Mono.Net.Security.AsyncReadRequest::.ctor(Mono.Net.Security.MobileAuthenticatedStream,System.Boolean,System.Byte[],System.Int32,System.Int32)
extern void AsyncReadRequest__ctor_mE67D11354154F1844761E0FFB869BBF05F1AE741 (void);
// 0x000000CC Mono.Net.Security.AsyncOperationStatus Mono.Net.Security.AsyncReadRequest::Run(Mono.Net.Security.AsyncOperationStatus)
extern void AsyncReadRequest_Run_m2D98252200B926F53624E90AE9649767F0C110ED (void);
// 0x000000CD System.Void Mono.Net.Security.AsyncWriteRequest::.ctor(Mono.Net.Security.MobileAuthenticatedStream,System.Boolean,System.Byte[],System.Int32,System.Int32)
extern void AsyncWriteRequest__ctor_m627E554957F61C80484B10AD0D85849C1E9A0548 (void);
// 0x000000CE Mono.Net.Security.AsyncOperationStatus Mono.Net.Security.AsyncWriteRequest::Run(Mono.Net.Security.AsyncOperationStatus)
extern void AsyncWriteRequest_Run_m10D193A545B0D8D5B68FED13056B0EE6099A92A0 (void);
// 0x000000CF Mono.Net.Security.ChainValidationHelper Mono.Net.Security.ChainValidationHelper::GetInternalValidator(System.Net.Security.SslStream,Mono.Net.Security.MobileTlsProvider,Mono.Security.Interface.MonoTlsSettings)
extern void ChainValidationHelper_GetInternalValidator_m9BF3791525A447EBA2283618A85A3E3C346205B7 (void);
// 0x000000D0 System.Void Mono.Net.Security.ChainValidationHelper::.ctor(System.Net.Security.SslStream,Mono.Net.Security.MobileTlsProvider,Mono.Security.Interface.MonoTlsSettings,System.Boolean,Mono.Net.Security.MonoTlsStream)
extern void ChainValidationHelper__ctor_m57CDE5571532D21835801745135CEE1ACF96A092 (void);
// 0x000000D1 System.Net.ServerCertValidationCallback Mono.Net.Security.ChainValidationHelper::GetValidationCallback(Mono.Security.Interface.MonoTlsSettings)
extern void ChainValidationHelper_GetValidationCallback_m48153D51B837278F4F2E6DADE32FC5460C5F7935 (void);
// 0x000000D2 System.Security.Cryptography.X509Certificates.X509Certificate Mono.Net.Security.ChainValidationHelper::DefaultSelectionCallback(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern void ChainValidationHelper_DefaultSelectionCallback_m9662623B23235E63F7081F6632A3305D3C6C0220 (void);
// 0x000000D3 Mono.Security.Interface.MonoTlsSettings Mono.Net.Security.ChainValidationHelper::get_Settings()
extern void ChainValidationHelper_get_Settings_m94E2DC79CB696A4F54956D812983E62FEB33D22E (void);
// 0x000000D4 System.Boolean Mono.Net.Security.ChainValidationHelper::SelectClientCertificate(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[],System.Security.Cryptography.X509Certificates.X509Certificate&)
extern void ChainValidationHelper_SelectClientCertificate_m77B577FD626B93B7018F62E6843727168F7876EA (void);
// 0x000000D5 Mono.Security.Interface.ValidationResult Mono.Net.Security.ChainValidationHelper::ValidateCertificate(System.String,System.Boolean,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain)
extern void ChainValidationHelper_ValidateCertificate_m859E258B2CF6F570984C618241A26AAFE2B7104D (void);
// 0x000000D6 Mono.Security.Interface.ValidationResult Mono.Net.Security.ChainValidationHelper::ValidateChain(System.String,System.Boolean,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Net.Security.SslPolicyErrors)
extern void ChainValidationHelper_ValidateChain_m60FB1695D7C8F3A0C01C864FDFEC2EC3CF648F79 (void);
// 0x000000D7 Mono.Security.Interface.ValidationResult Mono.Net.Security.ChainValidationHelper::ValidateChain(System.String,System.Boolean,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain&,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Net.Security.SslPolicyErrors)
extern void ChainValidationHelper_ValidateChain_m6E88B46B65F70D52BDD9438D218D7E3BE55BB1C7 (void);
// 0x000000D8 System.Boolean Mono.Net.Security.ChainValidationHelper::InvokeCallback(System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void ChainValidationHelper_InvokeCallback_m1E6AABFB2535B6E22428BDDC3252CE6ADB4D78CE (void);
// 0x000000D9 System.Void Mono.Net.Security.ChainValidationHelper/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m33705296E35208F621BECF6D7957DED9C7317D78 (void);
// 0x000000DA System.Boolean Mono.Net.Security.ChainValidationHelper/<>c__DisplayClass11_0::<GetValidationCallback>b__0(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void U3CU3Ec__DisplayClass11_0_U3CGetValidationCallbackU3Eb__0_m02FE972124C9B2EE3AE5CF1F1D4952D8DEF21D22 (void);
// 0x000000DB System.Void Mono.Net.Security.MobileAuthenticatedStream::.ctor(System.IO.Stream,System.Boolean,System.Net.Security.SslStream,Mono.Security.Interface.MonoTlsSettings,Mono.Net.Security.MobileTlsProvider)
extern void MobileAuthenticatedStream__ctor_mFA70F7AAAE52C6559DC554D363E5FFD4DE665CED (void);
// 0x000000DC System.Net.Security.SslStream Mono.Net.Security.MobileAuthenticatedStream::get_SslStream()
extern void MobileAuthenticatedStream_get_SslStream_mAF94411F0FF2126C8D8C1E9AABBC8CDA7EB73087 (void);
// 0x000000DD Mono.Security.Interface.MonoTlsSettings Mono.Net.Security.MobileAuthenticatedStream::get_Settings()
extern void MobileAuthenticatedStream_get_Settings_m21B268EC66FBAF762EC443B8FC784F1251444A96 (void);
// 0x000000DE Mono.Net.Security.MobileTlsProvider Mono.Net.Security.MobileAuthenticatedStream::get_Provider()
extern void MobileAuthenticatedStream_get_Provider_m9AE08411140D0CC27ADC979E0E290ECD70A47359 (void);
// 0x000000DF System.String Mono.Net.Security.MobileAuthenticatedStream::get_TargetHost()
extern void MobileAuthenticatedStream_get_TargetHost_m572DA701CB3CE3FA4006D5745F9A3B9A52739CAB (void);
// 0x000000E0 System.Void Mono.Net.Security.MobileAuthenticatedStream::set_TargetHost(System.String)
extern void MobileAuthenticatedStream_set_TargetHost_m490855169728C94C8FEAD0689D2FA3E578046BB6 (void);
// 0x000000E1 System.Void Mono.Net.Security.MobileAuthenticatedStream::CheckThrow(System.Boolean,System.Boolean)
extern void MobileAuthenticatedStream_CheckThrow_m9E35E722A793A807809AD00C9756997AB7561396 (void);
// 0x000000E2 System.Exception Mono.Net.Security.MobileAuthenticatedStream::GetSSPIException(System.Exception)
extern void MobileAuthenticatedStream_GetSSPIException_m04F38FF55AEEC19087840F3A722D2C225050F427 (void);
// 0x000000E3 System.Exception Mono.Net.Security.MobileAuthenticatedStream::GetIOException(System.Exception,System.String)
extern void MobileAuthenticatedStream_GetIOException_m80A6F9660142728B73A169E1B09AB8BE268DD517 (void);
// 0x000000E4 System.Exception Mono.Net.Security.MobileAuthenticatedStream::GetInternalError()
extern void MobileAuthenticatedStream_GetInternalError_m59D75502C4CAC329B87974927FF39AF718D15BDC (void);
// 0x000000E5 System.Exception Mono.Net.Security.MobileAuthenticatedStream::GetInvalidNestedCallException()
extern void MobileAuthenticatedStream_GetInvalidNestedCallException_mC7EBE64F717F86F3D399AA54CC8C60F18876725D (void);
// 0x000000E6 System.Runtime.ExceptionServices.ExceptionDispatchInfo Mono.Net.Security.MobileAuthenticatedStream::SetException(System.Exception)
extern void MobileAuthenticatedStream_SetException_m490704BA8728AB01E7BA158FE545FD4D7C080448 (void);
// 0x000000E7 System.Void Mono.Net.Security.MobileAuthenticatedStream::AuthenticateAsClient(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Authentication.SslProtocols,System.Boolean)
extern void MobileAuthenticatedStream_AuthenticateAsClient_mEB1237B52A4AB1FF3D7D3E52F61CE78A744973FC (void);
// 0x000000E8 System.Void Mono.Net.Security.MobileAuthenticatedStream::AuthenticateAsServer(System.Security.Cryptography.X509Certificates.X509Certificate,System.Boolean,System.Security.Authentication.SslProtocols,System.Boolean)
extern void MobileAuthenticatedStream_AuthenticateAsServer_m38A5035F798D754F9C460A3C4E2B90B673DD9091 (void);
// 0x000000E9 System.Threading.Tasks.Task Mono.Net.Security.MobileAuthenticatedStream::ProcessAuthentication(System.Boolean,Mono.Net.Security.MonoSslAuthenticationOptions,System.Threading.CancellationToken)
extern void MobileAuthenticatedStream_ProcessAuthentication_m0449087259B996D81ED6AC7A06ED19D46FAB0AF7 (void);
// 0x000000EA Mono.Net.Security.MobileTlsContext Mono.Net.Security.MobileAuthenticatedStream::CreateContext(Mono.Net.Security.MonoSslAuthenticationOptions)
// 0x000000EB System.Int32 Mono.Net.Security.MobileAuthenticatedStream::Read(System.Byte[],System.Int32,System.Int32)
extern void MobileAuthenticatedStream_Read_m6EC3DEF7CDD081863F58DC0E16120040DEB79622 (void);
// 0x000000EC System.Void Mono.Net.Security.MobileAuthenticatedStream::Write(System.Byte[],System.Int32,System.Int32)
extern void MobileAuthenticatedStream_Write_mD339F8D2C127E4A82A7C22A0EE9AF82F0A07A942 (void);
// 0x000000ED System.Threading.Tasks.Task`1<System.Int32> Mono.Net.Security.MobileAuthenticatedStream::ReadAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void MobileAuthenticatedStream_ReadAsync_mFA908C90B562D2C958DC1762A90F883DB3AC5F13 (void);
// 0x000000EE System.Threading.Tasks.Task Mono.Net.Security.MobileAuthenticatedStream::WriteAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void MobileAuthenticatedStream_WriteAsync_m764C5294A8B64C33B593CD388DC0BD9FFE120753 (void);
// 0x000000EF System.Threading.Tasks.Task`1<System.Int32> Mono.Net.Security.MobileAuthenticatedStream::StartOperation(Mono.Net.Security.MobileAuthenticatedStream/OperationType,Mono.Net.Security.AsyncProtocolRequest,System.Threading.CancellationToken)
extern void MobileAuthenticatedStream_StartOperation_m8E632D323D991C8393DE86E4EC3E02DCA5AB31AF (void);
// 0x000000F0 System.Int32 Mono.Net.Security.MobileAuthenticatedStream::InternalRead(System.Byte[],System.Int32,System.Int32,System.Boolean&)
extern void MobileAuthenticatedStream_InternalRead_m90FE0F2015227506EF5E6EA18E49EEA5E3A1C319 (void);
// 0x000000F1 System.ValueTuple`2<System.Int32,System.Boolean> Mono.Net.Security.MobileAuthenticatedStream::InternalRead(Mono.Net.Security.AsyncProtocolRequest,Mono.Net.Security.BufferOffsetSize,System.Byte[],System.Int32,System.Int32)
extern void MobileAuthenticatedStream_InternalRead_m5989243EA8E865C58630A151F50FD9286501238A (void);
// 0x000000F2 System.Boolean Mono.Net.Security.MobileAuthenticatedStream::InternalWrite(System.Byte[],System.Int32,System.Int32)
extern void MobileAuthenticatedStream_InternalWrite_mDCC759D5D81736625897B956239DBEEE8F7AE743 (void);
// 0x000000F3 System.Boolean Mono.Net.Security.MobileAuthenticatedStream::InternalWrite(Mono.Net.Security.AsyncProtocolRequest,Mono.Net.Security.BufferOffsetSize2,System.Byte[],System.Int32,System.Int32)
extern void MobileAuthenticatedStream_InternalWrite_m0B6A441B8194AAE474EECE582DA3627AB26CFB54 (void);
// 0x000000F4 System.Threading.Tasks.Task`1<System.Int32> Mono.Net.Security.MobileAuthenticatedStream::InnerRead(System.Boolean,System.Int32,System.Threading.CancellationToken)
extern void MobileAuthenticatedStream_InnerRead_m62766E47172B5468B6934BED507A4EF4054396ED (void);
// 0x000000F5 System.Threading.Tasks.Task Mono.Net.Security.MobileAuthenticatedStream::InnerWrite(System.Boolean,System.Threading.CancellationToken)
extern void MobileAuthenticatedStream_InnerWrite_m137287DEDB6C2BC5E8DDDAC999787FA5A0B4B570 (void);
// 0x000000F6 Mono.Net.Security.AsyncOperationStatus Mono.Net.Security.MobileAuthenticatedStream::ProcessHandshake(Mono.Net.Security.AsyncOperationStatus,System.Boolean)
extern void MobileAuthenticatedStream_ProcessHandshake_m30F67A233EAB7CE44A6B6E8E7F4E958DD888F63F (void);
// 0x000000F7 System.ValueTuple`2<System.Int32,System.Boolean> Mono.Net.Security.MobileAuthenticatedStream::ProcessRead(Mono.Net.Security.BufferOffsetSize)
extern void MobileAuthenticatedStream_ProcessRead_mB8FC6C872B55E0631A80824636220F3B73270441 (void);
// 0x000000F8 System.ValueTuple`2<System.Int32,System.Boolean> Mono.Net.Security.MobileAuthenticatedStream::ProcessWrite(Mono.Net.Security.BufferOffsetSize)
extern void MobileAuthenticatedStream_ProcessWrite_mC26A594E550853A399B5AD111CE704D06C500992 (void);
// 0x000000F9 System.Boolean Mono.Net.Security.MobileAuthenticatedStream::get_IsAuthenticated()
extern void MobileAuthenticatedStream_get_IsAuthenticated_m46A9D2671C2864D1F5C559FC6C5284701BA8AB1E (void);
// 0x000000FA System.Void Mono.Net.Security.MobileAuthenticatedStream::Dispose(System.Boolean)
extern void MobileAuthenticatedStream_Dispose_m0D094AAD63D7E66A3F88EA02B58CC039BF9D0F52 (void);
// 0x000000FB System.Void Mono.Net.Security.MobileAuthenticatedStream::Flush()
extern void MobileAuthenticatedStream_Flush_mCD0E572BA47A644E4B22167074AC8B4B926E6C60 (void);
// 0x000000FC System.Int64 Mono.Net.Security.MobileAuthenticatedStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void MobileAuthenticatedStream_Seek_mC6C077D712D53180D6D3676CCAB67810A57041DE (void);
// 0x000000FD System.Void Mono.Net.Security.MobileAuthenticatedStream::SetLength(System.Int64)
extern void MobileAuthenticatedStream_SetLength_m46CDB4C9A73652B8DF812B305342C6874B063A60 (void);
// 0x000000FE System.Boolean Mono.Net.Security.MobileAuthenticatedStream::get_CanRead()
extern void MobileAuthenticatedStream_get_CanRead_m206F7DA5CC6E445B2E23678344AD847C72929BED (void);
// 0x000000FF System.Boolean Mono.Net.Security.MobileAuthenticatedStream::get_CanWrite()
extern void MobileAuthenticatedStream_get_CanWrite_m6FBD6254C19E3391BAC2EFFDE46005BB0CDDEA84 (void);
// 0x00000100 System.Boolean Mono.Net.Security.MobileAuthenticatedStream::get_CanSeek()
extern void MobileAuthenticatedStream_get_CanSeek_mF7A4E5EC7EAFCAA07CBDD7D35B1A764177612503 (void);
// 0x00000101 System.Int64 Mono.Net.Security.MobileAuthenticatedStream::get_Length()
extern void MobileAuthenticatedStream_get_Length_mF8ACA717EFBD5E1E15B9ABBEFC9F5EC41127469C (void);
// 0x00000102 System.Int64 Mono.Net.Security.MobileAuthenticatedStream::get_Position()
extern void MobileAuthenticatedStream_get_Position_mDC516AFB4C932E61DA6DBFEF056CD9D798129035 (void);
// 0x00000103 System.Void Mono.Net.Security.MobileAuthenticatedStream::set_Position(System.Int64)
extern void MobileAuthenticatedStream_set_Position_mDA69F8B3575A0D6E3B11621BCB334371E80B9D56 (void);
// 0x00000104 System.Void Mono.Net.Security.MobileAuthenticatedStream::.cctor()
extern void MobileAuthenticatedStream__cctor_mAE00872557415DE85E6CB57122CAD50E9F3080FE (void);
// 0x00000105 System.Void Mono.Net.Security.MobileAuthenticatedStream::<InnerWrite>b__67_0()
extern void MobileAuthenticatedStream_U3CInnerWriteU3Eb__67_0_mEC1BC0A19130E44F6E542F229CE3FFAC0CD78697 (void);
// 0x00000106 System.Void Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__48::MoveNext()
extern void U3CProcessAuthenticationU3Ed__48_MoveNext_m982934120035A46C0FB96F7DF55F7FA0822B4940 (void);
// 0x00000107 System.Void Mono.Net.Security.MobileAuthenticatedStream/<ProcessAuthentication>d__48::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CProcessAuthenticationU3Ed__48_SetStateMachine_mC87BA3C30B7501EF0D0435E4C2C08EE79238576F (void);
// 0x00000108 System.Void Mono.Net.Security.MobileAuthenticatedStream/<StartOperation>d__57::MoveNext()
extern void U3CStartOperationU3Ed__57_MoveNext_m2CD8CDF02548ACC068670F3F670590EEC77F7853 (void);
// 0x00000109 System.Void Mono.Net.Security.MobileAuthenticatedStream/<StartOperation>d__57::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartOperationU3Ed__57_SetStateMachine_m1EC071633621977A0623BF79A53028A2A766BED9 (void);
// 0x0000010A System.Void Mono.Net.Security.MobileAuthenticatedStream/<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_m0649BF5458CDCAB6017C443988C36967E3000993 (void);
// 0x0000010B System.Int32 Mono.Net.Security.MobileAuthenticatedStream/<>c__DisplayClass66_0::<InnerRead>b__0()
extern void U3CU3Ec__DisplayClass66_0_U3CInnerReadU3Eb__0_m3F3293E1DD48D7532F3B8D84D7C8402698D60F36 (void);
// 0x0000010C System.Void Mono.Net.Security.MobileAuthenticatedStream/<InnerRead>d__66::MoveNext()
extern void U3CInnerReadU3Ed__66_MoveNext_m5DFDC2139602BBC2F86D14BDA64B4A49ECDD90EB (void);
// 0x0000010D System.Void Mono.Net.Security.MobileAuthenticatedStream/<InnerRead>d__66::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CInnerReadU3Ed__66_SetStateMachine_mBA42C0B565B5D3F43D109D2A8CC66EE8CE9BC920 (void);
// 0x0000010E System.Void Mono.Net.Security.MobileAuthenticatedStream/<InnerWrite>d__67::MoveNext()
extern void U3CInnerWriteU3Ed__67_MoveNext_mDF6F0FEA0FCAE97EEB2E5A2546446C00674CCEA3 (void);
// 0x0000010F System.Void Mono.Net.Security.MobileAuthenticatedStream/<InnerWrite>d__67::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CInnerWriteU3Ed__67_SetStateMachine_m7448CF503BCD3FEC38241FB22DBA3AB4B91FF5B1 (void);
// 0x00000110 System.Void Mono.Net.Security.MobileTlsContext::.ctor(Mono.Net.Security.MobileAuthenticatedStream,Mono.Net.Security.MonoSslAuthenticationOptions)
extern void MobileTlsContext__ctor_mAB891D57AD3DD87C68B97E458DCE7C03634B6404 (void);
// 0x00000111 Mono.Net.Security.MonoSslAuthenticationOptions Mono.Net.Security.MobileTlsContext::get_Options()
extern void MobileTlsContext_get_Options_m27C5391B99A94D7411FE2732BE3A49C4525862D4 (void);
// 0x00000112 Mono.Net.Security.MobileAuthenticatedStream Mono.Net.Security.MobileTlsContext::get_Parent()
extern void MobileTlsContext_get_Parent_mC509F430FE5581A104C62FB3A08AE9D97DE19098 (void);
// 0x00000113 Mono.Security.Interface.MonoTlsSettings Mono.Net.Security.MobileTlsContext::get_Settings()
extern void MobileTlsContext_get_Settings_m68D8D1488C1778311BE2EE65F549081B0E7D362C (void);
// 0x00000114 System.Boolean Mono.Net.Security.MobileTlsContext::get_IsAuthenticated()
// 0x00000115 System.Boolean Mono.Net.Security.MobileTlsContext::get_IsServer()
extern void MobileTlsContext_get_IsServer_m79E765F4FA8DAF1C98322134D48F6CACBEC824B7 (void);
// 0x00000116 System.String Mono.Net.Security.MobileTlsContext::get_TargetHost()
extern void MobileTlsContext_get_TargetHost_m82FF5791372BEF41AC5E73E368824231CC4F5528 (void);
// 0x00000117 System.String Mono.Net.Security.MobileTlsContext::get_ServerName()
extern void MobileTlsContext_get_ServerName_m3CF7B9F7D249B9B2F553B5A72F439B5502229BF3 (void);
// 0x00000118 System.Boolean Mono.Net.Security.MobileTlsContext::get_AskForClientCertificate()
extern void MobileTlsContext_get_AskForClientCertificate_mABDEDBD5B9CA52FCAD16A9C271C73FA05218B767 (void);
// 0x00000119 System.Security.Authentication.SslProtocols Mono.Net.Security.MobileTlsContext::get_EnabledProtocols()
extern void MobileTlsContext_get_EnabledProtocols_mE69073C19E4863B1EE7AA959E0804B18E169C85B (void);
// 0x0000011A System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Net.Security.MobileTlsContext::get_ClientCertificates()
extern void MobileTlsContext_get_ClientCertificates_m675A2D5E4F7FCB85B4B412A5659953184065C9EE (void);
// 0x0000011B System.Void Mono.Net.Security.MobileTlsContext::GetProtocolVersions(System.Nullable`1<Mono.Security.Interface.TlsProtocolCode>&,System.Nullable`1<Mono.Security.Interface.TlsProtocolCode>&)
extern void MobileTlsContext_GetProtocolVersions_m96D2EDF568161025E00CAEC740905545998BB36E (void);
// 0x0000011C System.Void Mono.Net.Security.MobileTlsContext::StartHandshake()
// 0x0000011D System.Boolean Mono.Net.Security.MobileTlsContext::ProcessHandshake()
// 0x0000011E System.Void Mono.Net.Security.MobileTlsContext::FinishHandshake()
// 0x0000011F System.Security.Cryptography.X509Certificates.X509Certificate Mono.Net.Security.MobileTlsContext::get_LocalServerCertificate()
extern void MobileTlsContext_get_LocalServerCertificate_m20FDC85F1F18C42644D48872EBAF9A6247624E09 (void);
// 0x00000120 System.Void Mono.Net.Security.MobileTlsContext::set_LocalServerCertificate(System.Security.Cryptography.X509Certificates.X509Certificate)
extern void MobileTlsContext_set_LocalServerCertificate_m32343DBF091AD80FA29DD8184C0C99C72FE43FEF (void);
// 0x00000121 System.Security.Cryptography.X509Certificates.X509Certificate2 Mono.Net.Security.MobileTlsContext::get_RemoteCertificate()
// 0x00000122 System.Void Mono.Net.Security.MobileTlsContext::Flush()
// 0x00000123 System.ValueTuple`2<System.Int32,System.Boolean> Mono.Net.Security.MobileTlsContext::Read(System.Byte[],System.Int32,System.Int32)
// 0x00000124 System.ValueTuple`2<System.Int32,System.Boolean> Mono.Net.Security.MobileTlsContext::Write(System.Byte[],System.Int32,System.Int32)
// 0x00000125 System.Void Mono.Net.Security.MobileTlsContext::Shutdown()
// 0x00000126 System.Boolean Mono.Net.Security.MobileTlsContext::PendingRenegotiation()
// 0x00000127 System.Boolean Mono.Net.Security.MobileTlsContext::ValidateCertificate(System.Security.Cryptography.X509Certificates.X509Certificate2,System.Security.Cryptography.X509Certificates.X509Chain)
extern void MobileTlsContext_ValidateCertificate_m7A7661E339AC2252062B3F7224621C9FACFEA234 (void);
// 0x00000128 System.Security.Cryptography.X509Certificates.X509Certificate Mono.Net.Security.MobileTlsContext::SelectServerCertificate(System.String)
extern void MobileTlsContext_SelectServerCertificate_m393E610B8E9273895A4E691DC8C78295FD563F2B (void);
// 0x00000129 System.Security.Cryptography.X509Certificates.X509Certificate Mono.Net.Security.MobileTlsContext::SelectClientCertificate(System.String[])
extern void MobileTlsContext_SelectClientCertificate_mF12113474A7EF6BDC8F7119D418722173A4EBDC1 (void);
// 0x0000012A System.Void Mono.Net.Security.MobileTlsContext::Renegotiate()
// 0x0000012B System.Void Mono.Net.Security.MobileTlsContext::Dispose()
extern void MobileTlsContext_Dispose_m7C7D11F95AC2F7663D397D6FA4ECF82ECC7C0F9A (void);
// 0x0000012C System.Void Mono.Net.Security.MobileTlsContext::Dispose(System.Boolean)
extern void MobileTlsContext_Dispose_m020A795EE75CF99916490B91697E0EB31C5A2F94 (void);
// 0x0000012D System.Void Mono.Net.Security.MobileTlsContext::Finalize()
extern void MobileTlsContext_Finalize_mC58200C1813B8458AD3DB37BC15338F2AC750DF9 (void);
// 0x0000012E Mono.Net.Security.MobileAuthenticatedStream Mono.Net.Security.MobileTlsProvider::CreateSslStream(System.Net.Security.SslStream,System.IO.Stream,System.Boolean,Mono.Security.Interface.MonoTlsSettings)
// 0x0000012F System.Boolean Mono.Net.Security.MobileTlsProvider::ValidateCertificate(Mono.Net.Security.ChainValidationHelper,System.String,System.Boolean,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Boolean,System.Security.Cryptography.X509Certificates.X509Chain&,System.Net.Security.SslPolicyErrors&,System.Int32&)
// 0x00000130 System.Void Mono.Net.Security.MobileTlsProvider::.ctor()
extern void MobileTlsProvider__ctor_m31D6E547C8B388F136D323088D376FD54FE69B68 (void);
// 0x00000131 System.Boolean Mono.Net.Security.MonoSslAuthenticationOptions::get_ServerMode()
// 0x00000132 System.Boolean Mono.Net.Security.MonoSslAuthenticationOptions::get_AllowRenegotiation()
// 0x00000133 System.Security.Authentication.SslProtocols Mono.Net.Security.MonoSslAuthenticationOptions::get_EnabledSslProtocols()
// 0x00000134 System.Void Mono.Net.Security.MonoSslAuthenticationOptions::set_EnabledSslProtocols(System.Security.Authentication.SslProtocols)
// 0x00000135 System.Void Mono.Net.Security.MonoSslAuthenticationOptions::set_EncryptionPolicy(System.Net.Security.EncryptionPolicy)
// 0x00000136 System.Void Mono.Net.Security.MonoSslAuthenticationOptions::set_CertificateRevocationCheckMode(System.Security.Cryptography.X509Certificates.X509RevocationMode)
// 0x00000137 System.String Mono.Net.Security.MonoSslAuthenticationOptions::get_TargetHost()
// 0x00000138 System.Void Mono.Net.Security.MonoSslAuthenticationOptions::set_TargetHost(System.String)
// 0x00000139 System.Security.Cryptography.X509Certificates.X509Certificate Mono.Net.Security.MonoSslAuthenticationOptions::get_ServerCertificate()
// 0x0000013A System.Void Mono.Net.Security.MonoSslAuthenticationOptions::set_ServerCertificate(System.Security.Cryptography.X509Certificates.X509Certificate)
// 0x0000013B System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Net.Security.MonoSslAuthenticationOptions::get_ClientCertificates()
// 0x0000013C System.Void Mono.Net.Security.MonoSslAuthenticationOptions::set_ClientCertificates(System.Security.Cryptography.X509Certificates.X509CertificateCollection)
// 0x0000013D System.Boolean Mono.Net.Security.MonoSslAuthenticationOptions::get_ClientCertificateRequired()
// 0x0000013E System.Void Mono.Net.Security.MonoSslAuthenticationOptions::set_ClientCertificateRequired(System.Boolean)
// 0x0000013F System.Net.Security.ServerCertSelectionCallback Mono.Net.Security.MonoSslAuthenticationOptions::get_ServerCertSelectionDelegate()
extern void MonoSslAuthenticationOptions_get_ServerCertSelectionDelegate_m8F35626050A015130D823BA68862416889F75A42 (void);
// 0x00000140 System.Void Mono.Net.Security.MonoSslAuthenticationOptions::.ctor()
extern void MonoSslAuthenticationOptions__ctor_mF6A5D221635C5E1ACF430108C1A11538E9F5349C (void);
// 0x00000141 System.Net.Security.SslClientAuthenticationOptions Mono.Net.Security.MonoSslClientAuthenticationOptions::get_Options()
extern void MonoSslClientAuthenticationOptions_get_Options_m67D736F913351C90F780A66F1A7C40832825D268 (void);
// 0x00000142 System.Boolean Mono.Net.Security.MonoSslClientAuthenticationOptions::get_ServerMode()
extern void MonoSslClientAuthenticationOptions_get_ServerMode_m7D7A5D3F111161ABF85FBFA44615F184880FBC44 (void);
// 0x00000143 System.Void Mono.Net.Security.MonoSslClientAuthenticationOptions::.ctor()
extern void MonoSslClientAuthenticationOptions__ctor_mFF45688A74504467AD6CC5CC1F416EAB53DDDDE1 (void);
// 0x00000144 System.Boolean Mono.Net.Security.MonoSslClientAuthenticationOptions::get_AllowRenegotiation()
extern void MonoSslClientAuthenticationOptions_get_AllowRenegotiation_mA6C88EE0477C658289190FD7E50A71990BD8AEEF (void);
// 0x00000145 System.Void Mono.Net.Security.MonoSslClientAuthenticationOptions::set_CertificateRevocationCheckMode(System.Security.Cryptography.X509Certificates.X509RevocationMode)
extern void MonoSslClientAuthenticationOptions_set_CertificateRevocationCheckMode_m99D8AF1B0AC110FA167DF8614E485EABC97E35B0 (void);
// 0x00000146 System.Void Mono.Net.Security.MonoSslClientAuthenticationOptions::set_EncryptionPolicy(System.Net.Security.EncryptionPolicy)
extern void MonoSslClientAuthenticationOptions_set_EncryptionPolicy_mA3DBA3D924CD48EFE642EC7AE41D194500803F95 (void);
// 0x00000147 System.Security.Authentication.SslProtocols Mono.Net.Security.MonoSslClientAuthenticationOptions::get_EnabledSslProtocols()
extern void MonoSslClientAuthenticationOptions_get_EnabledSslProtocols_m03F598BB877EDC3B07069EC3D5CD5D51464355F1 (void);
// 0x00000148 System.Void Mono.Net.Security.MonoSslClientAuthenticationOptions::set_EnabledSslProtocols(System.Security.Authentication.SslProtocols)
extern void MonoSslClientAuthenticationOptions_set_EnabledSslProtocols_m5D27CFC1927D070DAC713CE4F933E3565E753149 (void);
// 0x00000149 System.String Mono.Net.Security.MonoSslClientAuthenticationOptions::get_TargetHost()
extern void MonoSslClientAuthenticationOptions_get_TargetHost_m200D056C2694EC25E422728E35388E345889DB7B (void);
// 0x0000014A System.Void Mono.Net.Security.MonoSslClientAuthenticationOptions::set_TargetHost(System.String)
extern void MonoSslClientAuthenticationOptions_set_TargetHost_m069D8AB5AFF0FEE83E1C44770D3F3DE2D3CA8795 (void);
// 0x0000014B System.Boolean Mono.Net.Security.MonoSslClientAuthenticationOptions::get_ClientCertificateRequired()
extern void MonoSslClientAuthenticationOptions_get_ClientCertificateRequired_m9AE71A7C9696A450375687F64B4B8E69DEF0BD3F (void);
// 0x0000014C System.Void Mono.Net.Security.MonoSslClientAuthenticationOptions::set_ClientCertificateRequired(System.Boolean)
extern void MonoSslClientAuthenticationOptions_set_ClientCertificateRequired_m8768F1FD719CB1CE9970684B1CEF731DAF5FA266 (void);
// 0x0000014D System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Net.Security.MonoSslClientAuthenticationOptions::get_ClientCertificates()
extern void MonoSslClientAuthenticationOptions_get_ClientCertificates_m57BF4F92084EAAFFEF541E89D7ED519532DAEC0E (void);
// 0x0000014E System.Void Mono.Net.Security.MonoSslClientAuthenticationOptions::set_ClientCertificates(System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern void MonoSslClientAuthenticationOptions_set_ClientCertificates_mA5CD1A0990D617B249AE37CD4B49C53DB37B59A5 (void);
// 0x0000014F System.Security.Cryptography.X509Certificates.X509Certificate Mono.Net.Security.MonoSslClientAuthenticationOptions::get_ServerCertificate()
extern void MonoSslClientAuthenticationOptions_get_ServerCertificate_m7CD9CC5BF55B0C7749CDF0C813C9ACA4852B2985 (void);
// 0x00000150 System.Void Mono.Net.Security.MonoSslClientAuthenticationOptions::set_ServerCertificate(System.Security.Cryptography.X509Certificates.X509Certificate)
extern void MonoSslClientAuthenticationOptions_set_ServerCertificate_m1806D490852347DFAD35312C92722765F98311F6 (void);
// 0x00000151 System.Net.Security.SslServerAuthenticationOptions Mono.Net.Security.MonoSslServerAuthenticationOptions::get_Options()
extern void MonoSslServerAuthenticationOptions_get_Options_m2D07A52747C99A865F99FCE5BE8BBE439064CF8D (void);
// 0x00000152 System.Boolean Mono.Net.Security.MonoSslServerAuthenticationOptions::get_ServerMode()
extern void MonoSslServerAuthenticationOptions_get_ServerMode_mB4B5BBB94D83E6C0CBF7A609EA1E41295261A788 (void);
// 0x00000153 System.Void Mono.Net.Security.MonoSslServerAuthenticationOptions::.ctor()
extern void MonoSslServerAuthenticationOptions__ctor_m211250606F22155D74CF67A2B7C22BC2D8FD6D09 (void);
// 0x00000154 System.Boolean Mono.Net.Security.MonoSslServerAuthenticationOptions::get_AllowRenegotiation()
extern void MonoSslServerAuthenticationOptions_get_AllowRenegotiation_mFDE89EBE20D491413C6DA1725BE48C8A6D122692 (void);
// 0x00000155 System.Void Mono.Net.Security.MonoSslServerAuthenticationOptions::set_CertificateRevocationCheckMode(System.Security.Cryptography.X509Certificates.X509RevocationMode)
extern void MonoSslServerAuthenticationOptions_set_CertificateRevocationCheckMode_m6791C99760134F30DC6FAFD2AA4A5ED4BE22F67C (void);
// 0x00000156 System.Void Mono.Net.Security.MonoSslServerAuthenticationOptions::set_EncryptionPolicy(System.Net.Security.EncryptionPolicy)
extern void MonoSslServerAuthenticationOptions_set_EncryptionPolicy_m92DE237BB4391DCDCD2BC2EBB645C544A0F10D9A (void);
// 0x00000157 System.Security.Authentication.SslProtocols Mono.Net.Security.MonoSslServerAuthenticationOptions::get_EnabledSslProtocols()
extern void MonoSslServerAuthenticationOptions_get_EnabledSslProtocols_m09F833488BD7B306A97E09715877C024C6E4FCD5 (void);
// 0x00000158 System.Void Mono.Net.Security.MonoSslServerAuthenticationOptions::set_EnabledSslProtocols(System.Security.Authentication.SslProtocols)
extern void MonoSslServerAuthenticationOptions_set_EnabledSslProtocols_m3D5F51C998613D004EDC32998C26E895AF60CA5A (void);
// 0x00000159 System.Boolean Mono.Net.Security.MonoSslServerAuthenticationOptions::get_ClientCertificateRequired()
extern void MonoSslServerAuthenticationOptions_get_ClientCertificateRequired_m02CC06C33754C266C56A2FBB4D88BC3E16A3E7C1 (void);
// 0x0000015A System.Void Mono.Net.Security.MonoSslServerAuthenticationOptions::set_ClientCertificateRequired(System.Boolean)
extern void MonoSslServerAuthenticationOptions_set_ClientCertificateRequired_m40CFC42CA71E7FDED3293CA213CE0187EDD908D2 (void);
// 0x0000015B System.String Mono.Net.Security.MonoSslServerAuthenticationOptions::get_TargetHost()
extern void MonoSslServerAuthenticationOptions_get_TargetHost_mF15AC1BA010ADB337151222436D0DAE9F6DD1B63 (void);
// 0x0000015C System.Void Mono.Net.Security.MonoSslServerAuthenticationOptions::set_TargetHost(System.String)
extern void MonoSslServerAuthenticationOptions_set_TargetHost_m7FB1CB6CB8135731D73500E4BC59EADDBB311132 (void);
// 0x0000015D System.Security.Cryptography.X509Certificates.X509Certificate Mono.Net.Security.MonoSslServerAuthenticationOptions::get_ServerCertificate()
extern void MonoSslServerAuthenticationOptions_get_ServerCertificate_mAC142235AC40CA569A058861AF65CB5B744C6EE1 (void);
// 0x0000015E System.Void Mono.Net.Security.MonoSslServerAuthenticationOptions::set_ServerCertificate(System.Security.Cryptography.X509Certificates.X509Certificate)
extern void MonoSslServerAuthenticationOptions_set_ServerCertificate_mBC83A44B22D50324A5F4CC3AFAC4158148FE3674 (void);
// 0x0000015F System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Net.Security.MonoSslServerAuthenticationOptions::get_ClientCertificates()
extern void MonoSslServerAuthenticationOptions_get_ClientCertificates_m4451AAD3DDD09C5FA0A1466C358F07EBDFD81130 (void);
// 0x00000160 System.Void Mono.Net.Security.MonoSslServerAuthenticationOptions::set_ClientCertificates(System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern void MonoSslServerAuthenticationOptions_set_ClientCertificates_m222B1EDF6597135FFB4CDDB78C7246F34C4D4089 (void);
// 0x00000161 Mono.Net.Security.MobileTlsProvider Mono.Net.Security.MonoTlsProviderFactory::GetProviderInternal()
extern void MonoTlsProviderFactory_GetProviderInternal_m87C87ACEF272F007119C23121CD0D2E183785546 (void);
// 0x00000162 System.Void Mono.Net.Security.MonoTlsProviderFactory::InitializeInternal()
extern void MonoTlsProviderFactory_InitializeInternal_mB0843306BA58C8CBDE17485CBFCE45BAD8CFA356 (void);
// 0x00000163 Mono.Net.Security.MobileTlsProvider Mono.Net.Security.MonoTlsProviderFactory::LookupProvider(System.String,System.Boolean)
extern void MonoTlsProviderFactory_LookupProvider_m842219B5A2493A9AA3B4D1911A1D77EB2F260DD0 (void);
// 0x00000164 System.Void Mono.Net.Security.MonoTlsProviderFactory::InitializeProviderRegistration()
extern void MonoTlsProviderFactory_InitializeProviderRegistration_mDC8AEDEE018FFC5374E746A72B55A337B675CB82 (void);
// 0x00000165 System.Void Mono.Net.Security.MonoTlsProviderFactory::PopulateUnityProviders()
extern void MonoTlsProviderFactory_PopulateUnityProviders_m5A63191876243DF78B41ACDFA473F61C8B2243F3 (void);
// 0x00000166 System.Void Mono.Net.Security.MonoTlsProviderFactory::PopulateProviders()
extern void MonoTlsProviderFactory_PopulateProviders_mF2AB544C94407E541767C11141D5876CB079F838 (void);
// 0x00000167 System.Boolean Mono.Net.Security.MonoTlsProviderFactory::IsBtlsSupported()
extern void MonoTlsProviderFactory_IsBtlsSupported_m3AC62A64C40E98295932BFB36ABF44776241D780 (void);
// 0x00000168 Mono.Net.Security.MobileTlsProvider Mono.Net.Security.MonoTlsProviderFactory::CreateDefaultProviderImpl()
extern void MonoTlsProviderFactory_CreateDefaultProviderImpl_m674088AF2FC01E08CEE18728A7BBACDD9A7C2182 (void);
// 0x00000169 Mono.Net.Security.MobileTlsProvider Mono.Net.Security.MonoTlsProviderFactory::GetProvider()
extern void MonoTlsProviderFactory_GetProvider_m8CDA0A014E43C98804347DB5384F9C05BBA26FEE (void);
// 0x0000016A System.Void Mono.Net.Security.MonoTlsProviderFactory::.cctor()
extern void MonoTlsProviderFactory__cctor_mA2C103AE727F17D8706F964DB378FB0644CA70D1 (void);
// 0x0000016B System.Net.HttpWebRequest Mono.Net.Security.MonoTlsStream::get_Request()
extern void MonoTlsStream_get_Request_m5F3CDE9EA1921F1A45C0FAE20DD4BA634D0D8D83 (void);
// 0x0000016C System.Void Mono.Net.Security.MonoTlsStream::set_CertificateValidationFailed(System.Boolean)
extern void MonoTlsStream_set_CertificateValidationFailed_m225E1AA3894B292682A2BF06B886693A11DEAFEC (void);
// 0x0000016D System.Void Mono.Net.Security.MonoTlsStream::Dispose()
extern void MonoTlsStream_Dispose_m955C2295BC9317F9E6F756164528EE530E0032FC (void);
// 0x0000016E System.Void Mono.Net.Security.MonoTlsStream::CloseSslStream()
extern void MonoTlsStream_CloseSslStream_m82584A00391A5ACA6EC81C4848BBC7F92F801278 (void);
// 0x0000016F System.Object Mono.Net.Security.NoReflectionHelper::GetProvider()
extern void NoReflectionHelper_GetProvider_m07746C3FA62A325DCC22169E40CC5353A93F2A23 (void);
// 0x00000170 System.Void Mono.Net.Security.SystemCertificateValidator::.cctor()
extern void SystemCertificateValidator__cctor_m6C2E43AD95D74AF470013B88B823825B13937E30 (void);
// 0x00000171 System.Boolean Mono.Net.Security.SystemCertificateValidator::NeedsChain(Mono.Security.Interface.MonoTlsSettings)
extern void SystemCertificateValidator_NeedsChain_mEA1D649FC72CD6D61ACECF098C7CC871AEC5A215 (void);
// 0x00000172 Mono.Security.Interface.MonoRemoteCertificateValidationCallback Mono.Net.Security.Private.CallbackHelpers::PublicToMono(System.Net.Security.RemoteCertificateValidationCallback)
extern void CallbackHelpers_PublicToMono_m4B0ED4D1F9D6AC15E14D09F55642889B57CAE950 (void);
// 0x00000173 System.Net.Security.LocalCertSelectionCallback Mono.Net.Security.Private.CallbackHelpers::MonoToInternal(Mono.Security.Interface.MonoLocalCertificateSelectionCallback)
extern void CallbackHelpers_MonoToInternal_m895AC89217D5876DB6CC95D7C656C9BA13D4D618 (void);
// 0x00000174 System.Void Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m31154BF78DAACBD45BE85524DB60A0C96A1B2A12 (void);
// 0x00000175 System.Boolean Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass0_0::<PublicToMono>b__0(System.String,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,Mono.Security.Interface.MonoSslPolicyErrors)
extern void U3CU3Ec__DisplayClass0_0_U3CPublicToMonoU3Eb__0_m1C478ECA2AAAF7CF24DE679072777B23441DEFC8 (void);
// 0x00000176 System.Void Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_mD1E04FD28115DBD711A881B26347464C27140E23 (void);
// 0x00000177 System.Security.Cryptography.X509Certificates.X509Certificate Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass6_0::<MonoToInternal>b__0(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern void U3CU3Ec__DisplayClass6_0_U3CMonoToInternalU3Eb__0_mE37EA1777B5D0BCD42B36A717CF6FC0E18CD3AE9 (void);
// 0x00000178 System.Void Mono.Btls.MonoBtlsBio::.ctor(Mono.Btls.MonoBtlsBio/BoringBioHandle)
extern void MonoBtlsBio__ctor_m27999B362D4460C8CF038BF555FA4895B901AA5E (void);
// 0x00000179 Mono.Btls.MonoBtlsBio/BoringBioHandle Mono.Btls.MonoBtlsBio::get_Handle()
extern void MonoBtlsBio_get_Handle_m447149554862449317739255B72B29685EF522C7 (void);
// 0x0000017A System.Void Mono.Btls.MonoBtlsBio::mono_btls_bio_free(System.IntPtr)
extern void MonoBtlsBio_mono_btls_bio_free_mD35E178BE5716B6F65A35065550F53CDF569A0AB (void);
// 0x0000017B System.Void Mono.Btls.MonoBtlsBio/BoringBioHandle::.ctor(System.IntPtr)
extern void BoringBioHandle__ctor_m94DB76BFFBAFAAC740B93B8FEB2B9F26489725EA (void);
// 0x0000017C System.Boolean Mono.Btls.MonoBtlsBio/BoringBioHandle::ReleaseHandle()
extern void BoringBioHandle_ReleaseHandle_mFC55F3883FC1B9759C03679E032DB8A967785FE9 (void);
// 0x0000017D System.IntPtr Mono.Btls.MonoBtlsBioMemory::mono_btls_bio_mem_new()
extern void MonoBtlsBioMemory_mono_btls_bio_mem_new_m888889BBAAFF564D6D030981355733781B45FE40 (void);
// 0x0000017E System.Int32 Mono.Btls.MonoBtlsBioMemory::mono_btls_bio_mem_get_data(System.IntPtr,System.IntPtr&)
extern void MonoBtlsBioMemory_mono_btls_bio_mem_get_data_m68ECECE7237880D38B52B934CD42A2D64AD2FA59 (void);
// 0x0000017F System.Void Mono.Btls.MonoBtlsBioMemory::.ctor()
extern void MonoBtlsBioMemory__ctor_mA8389F42E3A775CB226FE64F143169BCE25018A7 (void);
// 0x00000180 System.Byte[] Mono.Btls.MonoBtlsBioMemory::GetData()
extern void MonoBtlsBioMemory_GetData_m3DA5A5971C27CAF5694AA01DA1A9136379A8147A (void);
// 0x00000181 System.Int32 Mono.Btls.IMonoBtlsBioMono::Read(System.Byte[],System.Int32,System.Int32,System.Boolean&)
// 0x00000182 System.Boolean Mono.Btls.IMonoBtlsBioMono::Write(System.Byte[],System.Int32,System.Int32)
// 0x00000183 System.Void Mono.Btls.IMonoBtlsBioMono::Flush()
// 0x00000184 System.Void Mono.Btls.IMonoBtlsBioMono::Close()
// 0x00000185 System.Void Mono.Btls.MonoBtlsBioMono::.ctor(Mono.Btls.IMonoBtlsBioMono)
extern void MonoBtlsBioMono__ctor_m1D9C5AB7F2108D6C7E9E817181EF0516F2BFD965 (void);
// 0x00000186 System.IntPtr Mono.Btls.MonoBtlsBioMono::mono_btls_bio_mono_new()
extern void MonoBtlsBioMono_mono_btls_bio_mono_new_mF53600E459C525834E106EE628F6DE06DF1A8D1A (void);
// 0x00000187 System.Void Mono.Btls.MonoBtlsBioMono::mono_btls_bio_mono_initialize(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern void MonoBtlsBioMono_mono_btls_bio_mono_initialize_m0607739C885E2566FCAF4CD6153A745992F80102 (void);
// 0x00000188 System.Int64 Mono.Btls.MonoBtlsBioMono::Control(Mono.Btls.MonoBtlsBioMono/ControlCommand,System.Int64)
extern void MonoBtlsBioMono_Control_mF7578B78CC76CA1B0D2FE532B658EFE092BFB533 (void);
// 0x00000189 System.Int32 Mono.Btls.MonoBtlsBioMono::OnRead(System.IntPtr,System.Int32,System.Int32&)
extern void MonoBtlsBioMono_OnRead_mB3B07BB23F84F8FA457FC186112435B1206D48F1 (void);
// 0x0000018A System.Int32 Mono.Btls.MonoBtlsBioMono::OnRead(System.IntPtr,System.IntPtr,System.Int32,System.Int32&)
extern void MonoBtlsBioMono_OnRead_mE0FF0BFC134CD0F923BBC555E346B7DD7CAD8988 (void);
// 0x0000018B System.Int32 Mono.Btls.MonoBtlsBioMono::OnWrite(System.IntPtr,System.Int32)
extern void MonoBtlsBioMono_OnWrite_m13ED5B856803BC1AECF385A8A8DDE3728131240D (void);
// 0x0000018C System.Int32 Mono.Btls.MonoBtlsBioMono::OnWrite(System.IntPtr,System.IntPtr,System.Int32)
extern void MonoBtlsBioMono_OnWrite_m605A12A8B0CE9517927656981580BFA330BED613 (void);
// 0x0000018D System.Int64 Mono.Btls.MonoBtlsBioMono::Control(System.IntPtr,Mono.Btls.MonoBtlsBioMono/ControlCommand,System.Int64)
extern void MonoBtlsBioMono_Control_mE6F92D680A96B8B446D566D221BF925954FF43BB (void);
// 0x0000018E System.Void Mono.Btls.MonoBtlsBioMono::Close()
extern void MonoBtlsBioMono_Close_m5A7EC104436F0CE6C30C116126361AD3E9EB0388 (void);
// 0x0000018F System.Void Mono.Btls.MonoBtlsBioMono/BioReadFunc::.ctor(System.Object,System.IntPtr)
extern void BioReadFunc__ctor_mF67045947F464E6421342B8F8C33D087121071EE (void);
// 0x00000190 System.Int32 Mono.Btls.MonoBtlsBioMono/BioReadFunc::Invoke(System.IntPtr,System.IntPtr,System.Int32,System.Int32&)
extern void BioReadFunc_Invoke_m744CB0CD8BE679279438EF430FB0E2FD6DDDB116 (void);
// 0x00000191 System.Void Mono.Btls.MonoBtlsBioMono/BioWriteFunc::.ctor(System.Object,System.IntPtr)
extern void BioWriteFunc__ctor_m3A5F86F45AF72BC45CEEDCB0CB4F8D6CAE430677 (void);
// 0x00000192 System.Int32 Mono.Btls.MonoBtlsBioMono/BioWriteFunc::Invoke(System.IntPtr,System.IntPtr,System.Int32)
extern void BioWriteFunc_Invoke_mBA1D9291830C4AC61989002AE28AA19329279027 (void);
// 0x00000193 System.Void Mono.Btls.MonoBtlsBioMono/BioControlFunc::.ctor(System.Object,System.IntPtr)
extern void BioControlFunc__ctor_mECCABB988AD4AB5F618BA15CBFA2E8038FAF47B8 (void);
// 0x00000194 System.Int64 Mono.Btls.MonoBtlsBioMono/BioControlFunc::Invoke(System.IntPtr,Mono.Btls.MonoBtlsBioMono/ControlCommand,System.Int64)
extern void BioControlFunc_Invoke_m9A7F407B786D5DBE70CE79098FE7A2C3C50F77B5 (void);
// 0x00000195 System.Void Mono.Btls.MonoBtlsContext::.ctor(Mono.Net.Security.MobileAuthenticatedStream,Mono.Net.Security.MonoSslAuthenticationOptions)
extern void MonoBtlsContext__ctor_mFF1354F7D4DC6FF9884B01D72E440171BEB484E6 (void);
// 0x00000196 Mono.Btls.X509CertificateImplBtls Mono.Btls.MonoBtlsContext::GetPrivateCertificate(System.Security.Cryptography.X509Certificates.X509Certificate)
extern void MonoBtlsContext_GetPrivateCertificate_m4DA83A2AA6134658390AAEEDD22D497527D69498 (void);
// 0x00000197 System.Int32 Mono.Btls.MonoBtlsContext::VerifyCallback(Mono.Btls.MonoBtlsX509StoreCtx)
extern void MonoBtlsContext_VerifyCallback_m72C449E20B2F9F5DC5EB079F1114FE71E5BD4D50 (void);
// 0x00000198 System.Int32 Mono.Btls.MonoBtlsContext::SelectCallback(System.String[])
extern void MonoBtlsContext_SelectCallback_mCC262DE516D940AD768EE975B0AB5E8E9A41A696 (void);
// 0x00000199 System.Int32 Mono.Btls.MonoBtlsContext::ServerNameCallback()
extern void MonoBtlsContext_ServerNameCallback_mEC03DB70C67F21082D38A723593413619C6923E3 (void);
// 0x0000019A System.Void Mono.Btls.MonoBtlsContext::StartHandshake()
extern void MonoBtlsContext_StartHandshake_mC61D033B4D1D8E6D19422FDAAC610D8ADAED2366 (void);
// 0x0000019B System.Void Mono.Btls.MonoBtlsContext::SetPrivateCertificate(Mono.Btls.X509CertificateImplBtls)
extern void MonoBtlsContext_SetPrivateCertificate_m282944AF213BD8B0FB26C98622D3CB1503B81E3B (void);
// 0x0000019C System.Exception Mono.Btls.MonoBtlsContext::GetException(Mono.Btls.MonoBtlsSslError)
extern void MonoBtlsContext_GetException_m9711314F100FA6FF002F56ABF4F4B7EC088D44CC (void);
// 0x0000019D System.Boolean Mono.Btls.MonoBtlsContext::ProcessHandshake()
extern void MonoBtlsContext_ProcessHandshake_m57F7BFA5557487E87D55C0F7A03001679850E4D1 (void);
// 0x0000019E Mono.Btls.MonoBtlsSslError Mono.Btls.MonoBtlsContext::DoProcessHandshake()
extern void MonoBtlsContext_DoProcessHandshake_m58BE2AD5EA649B65151234993D39D114EC04C7EF (void);
// 0x0000019F System.Void Mono.Btls.MonoBtlsContext::FinishHandshake()
extern void MonoBtlsContext_FinishHandshake_m27B7D33BE2074849236F0CBB1741CF5D461361A5 (void);
// 0x000001A0 System.Void Mono.Btls.MonoBtlsContext::InitializeConnection()
extern void MonoBtlsContext_InitializeConnection_m50A6DBB41A93C09A6CC23D6EB749368A6B0ED562 (void);
// 0x000001A1 System.Void Mono.Btls.MonoBtlsContext::GetPeerCertificate()
extern void MonoBtlsContext_GetPeerCertificate_m4EE3A44FE9F0445EE728F46AC7D7C9D9AE2847EC (void);
// 0x000001A2 System.Void Mono.Btls.MonoBtlsContext::InitializeSession()
extern void MonoBtlsContext_InitializeSession_m884FFEF19AC79E255090EBC4BF3A26FCB8167CF5 (void);
// 0x000001A3 Mono.Security.Interface.TlsProtocols Mono.Btls.MonoBtlsContext::GetProtocol(Mono.Security.Interface.TlsProtocolCode)
extern void MonoBtlsContext_GetProtocol_mA09A372EDAD446DE4C74137BD9A496A6432B076E (void);
// 0x000001A4 System.Void Mono.Btls.MonoBtlsContext::Flush()
extern void MonoBtlsContext_Flush_m461CB922D21FF484E3D8C4605174EEB608AABB5E (void);
// 0x000001A5 System.ValueTuple`2<System.Int32,System.Boolean> Mono.Btls.MonoBtlsContext::Read(System.Byte[],System.Int32,System.Int32)
extern void MonoBtlsContext_Read_m7C39CAEF7B9AEA3B7A7119B959ED5D53B884797C (void);
// 0x000001A6 System.ValueTuple`2<System.Int32,System.Boolean> Mono.Btls.MonoBtlsContext::Write(System.Byte[],System.Int32,System.Int32)
extern void MonoBtlsContext_Write_mAC097BE01F8D6C0A5DF058E406DF4FC0312941E7 (void);
// 0x000001A7 System.Void Mono.Btls.MonoBtlsContext::Renegotiate()
extern void MonoBtlsContext_Renegotiate_m7D619E3469DE30895949BA0C4C5B4A87F359B070 (void);
// 0x000001A8 System.Void Mono.Btls.MonoBtlsContext::Shutdown()
extern void MonoBtlsContext_Shutdown_m60EE94F649A4AD3CBA73373E073346AA27B90E9C (void);
// 0x000001A9 System.Boolean Mono.Btls.MonoBtlsContext::PendingRenegotiation()
extern void MonoBtlsContext_PendingRenegotiation_mF614A7BE29C70CCB70F7E84BEDA19A1A858F87B0 (void);
// 0x000001AA System.Void Mono.Btls.MonoBtlsContext::Dispose(T&)
// 0x000001AB System.Void Mono.Btls.MonoBtlsContext::Dispose(System.Boolean)
extern void MonoBtlsContext_Dispose_m01F050EFECECB8198D614D51FF0123B70C82B8B0 (void);
// 0x000001AC System.Int32 Mono.Btls.MonoBtlsContext::Mono.Btls.IMonoBtlsBioMono.Read(System.Byte[],System.Int32,System.Int32,System.Boolean&)
extern void MonoBtlsContext_Mono_Btls_IMonoBtlsBioMono_Read_mBADC4C379D8B6FB3ACA5068AE8147111DE1B5DEC (void);
// 0x000001AD System.Boolean Mono.Btls.MonoBtlsContext::Mono.Btls.IMonoBtlsBioMono.Write(System.Byte[],System.Int32,System.Int32)
extern void MonoBtlsContext_Mono_Btls_IMonoBtlsBioMono_Write_m67C5F3168753CEDA8CBB506A8A206D688D980C1C (void);
// 0x000001AE System.Void Mono.Btls.MonoBtlsContext::Mono.Btls.IMonoBtlsBioMono.Flush()
extern void MonoBtlsContext_Mono_Btls_IMonoBtlsBioMono_Flush_mC00CC6A40B9E148F9D63F8629D11F1DC0DA2A135 (void);
// 0x000001AF System.Void Mono.Btls.MonoBtlsContext::Mono.Btls.IMonoBtlsBioMono.Close()
extern void MonoBtlsContext_Mono_Btls_IMonoBtlsBioMono_Close_m830DA02726C9ADEB02B5AF73E39C22D43A847127 (void);
// 0x000001B0 System.Boolean Mono.Btls.MonoBtlsContext::get_IsAuthenticated()
extern void MonoBtlsContext_get_IsAuthenticated_m51942004793D9BC3D95DB7ABFD53CA2D4204D380 (void);
// 0x000001B1 System.Security.Cryptography.X509Certificates.X509Certificate2 Mono.Btls.MonoBtlsContext::get_RemoteCertificate()
extern void MonoBtlsContext_get_RemoteCertificate_mD114E4893BF27ED2FD87A9D86C36AA915DF7D4C9 (void);
// 0x000001B2 System.Void Mono.Btls.MonoBtlsError::mono_btls_error_clear_error()
extern void MonoBtlsError_mono_btls_error_clear_error_mF5ADFE111AF96E5591ABBD14A06504A90C4B1B45 (void);
// 0x000001B3 System.Int32 Mono.Btls.MonoBtlsError::mono_btls_error_get_error_line(System.IntPtr&,System.Int32&)
extern void MonoBtlsError_mono_btls_error_get_error_line_m66FDF6BA20A7A1B7F0736F3C271BF4BE5959261C (void);
// 0x000001B4 System.Void Mono.Btls.MonoBtlsError::mono_btls_error_get_error_string_n(System.Int32,System.IntPtr,System.Int32)
extern void MonoBtlsError_mono_btls_error_get_error_string_n_m8BA6D140E84E1F6E3FA891B1B86B44D10FDB5A3D (void);
// 0x000001B5 System.Int32 Mono.Btls.MonoBtlsError::mono_btls_error_get_reason(System.Int32)
extern void MonoBtlsError_mono_btls_error_get_reason_mA810B6689B39DC83767CD59EA99165B15A917D4D (void);
// 0x000001B6 System.Void Mono.Btls.MonoBtlsError::ClearError()
extern void MonoBtlsError_ClearError_m5D1732687E88DAAA64369CCA7FC40D5A71AC6878 (void);
// 0x000001B7 System.String Mono.Btls.MonoBtlsError::GetErrorString(System.Int32)
extern void MonoBtlsError_GetErrorString_m947E5B74ADAD69852AD20AEB8FDAD187157C2315 (void);
// 0x000001B8 System.Int32 Mono.Btls.MonoBtlsError::GetError(System.String&,System.Int32&)
extern void MonoBtlsError_GetError_m008CE301168688B62B435460D9727EEC4BA17314 (void);
// 0x000001B9 System.Int32 Mono.Btls.MonoBtlsError::GetErrorReason(System.Int32)
extern void MonoBtlsError_GetErrorReason_mAF26D002DE639B5AAC6D396B0ED09C70CA25FF9E (void);
// 0x000001BA System.Void Mono.Btls.MonoBtlsException::.ctor()
extern void MonoBtlsException__ctor_m5DCEFB95BE77CDFF5A8D0DA19B45F41E46188E67 (void);
// 0x000001BB System.Void Mono.Btls.MonoBtlsException::.ctor(Mono.Btls.MonoBtlsSslError)
extern void MonoBtlsException__ctor_mE99D10E7FF7F36DB79481CB71E6B1B3AF0EA2539 (void);
// 0x000001BC System.Void Mono.Btls.MonoBtlsException::.ctor(System.String)
extern void MonoBtlsException__ctor_m057F82E928B82D01EE415555AD059886A83808B0 (void);
// 0x000001BD System.Void Mono.Btls.MonoBtlsException::.ctor(System.String,System.Object[])
extern void MonoBtlsException__ctor_m86D66CEBF74847200F5205293C6B9421B7BBDB42 (void);
// 0x000001BE System.IntPtr Mono.Btls.MonoBtlsKey::mono_btls_key_new()
extern void MonoBtlsKey_mono_btls_key_new_mFF76A62C86DFE90713115D98A2D6B9CAA93167FC (void);
// 0x000001BF System.Void Mono.Btls.MonoBtlsKey::mono_btls_key_free(System.IntPtr)
extern void MonoBtlsKey_mono_btls_key_free_m09DC429805222FC2E7A9850A32F2674120E313EE (void);
// 0x000001C0 System.IntPtr Mono.Btls.MonoBtlsKey::mono_btls_key_up_ref(System.IntPtr)
extern void MonoBtlsKey_mono_btls_key_up_ref_m4DFC176C712A62F199FC933AF29A1F0B3A86E818 (void);
// 0x000001C1 System.Int32 Mono.Btls.MonoBtlsKey::mono_btls_key_get_bytes(System.IntPtr,System.IntPtr&,System.Int32&,System.Int32)
extern void MonoBtlsKey_mono_btls_key_get_bytes_mCA5AF65D687CD9E0E172CAB83AFF419F66590369 (void);
// 0x000001C2 System.Int32 Mono.Btls.MonoBtlsKey::mono_btls_key_assign_rsa_private_key(System.IntPtr,System.Byte[],System.Int32)
extern void MonoBtlsKey_mono_btls_key_assign_rsa_private_key_m589A698F64930277A6D0EF4993D041FC2759B7F4 (void);
// 0x000001C3 Mono.Btls.MonoBtlsKey/BoringKeyHandle Mono.Btls.MonoBtlsKey::get_Handle()
extern void MonoBtlsKey_get_Handle_m3F0EC3F04D1B2886BBD7896BEB3DE91E6D255E6C (void);
// 0x000001C4 System.Void Mono.Btls.MonoBtlsKey::.ctor(Mono.Btls.MonoBtlsKey/BoringKeyHandle)
extern void MonoBtlsKey__ctor_m41A5A6E3975D3CD98F64CB8B83325D87DEB01DDA (void);
// 0x000001C5 System.Byte[] Mono.Btls.MonoBtlsKey::GetBytes(System.Boolean)
extern void MonoBtlsKey_GetBytes_mD3A4E69C8BF5D40AB6BD6C7CF1A4650DF9BF5097 (void);
// 0x000001C6 Mono.Btls.MonoBtlsKey Mono.Btls.MonoBtlsKey::Copy()
extern void MonoBtlsKey_Copy_m27EC5A8D495C066523DB4558D7D148D9F2C5436E (void);
// 0x000001C7 Mono.Btls.MonoBtlsKey Mono.Btls.MonoBtlsKey::CreateFromRSAPrivateKey(System.Security.Cryptography.RSA)
extern void MonoBtlsKey_CreateFromRSAPrivateKey_mC544AA4E5CC589D60B6F12445782A0ECCBC9D4CF (void);
// 0x000001C8 System.Void Mono.Btls.MonoBtlsKey/BoringKeyHandle::.ctor(System.IntPtr)
extern void BoringKeyHandle__ctor_m61DE09782CCD143D59C1FC4A29A3B3D83631A5AD (void);
// 0x000001C9 System.Boolean Mono.Btls.MonoBtlsKey/BoringKeyHandle::ReleaseHandle()
extern void BoringKeyHandle_ReleaseHandle_mA9996A7F374DECC94D48660290157AB69179ADDD (void);
// 0x000001CA System.Void Mono.Btls.MonoBtlsObject::.ctor(Mono.Btls.MonoBtlsObject/MonoBtlsHandle)
extern void MonoBtlsObject__ctor_m357D70EACFD7EF183FA3F59B02746ED649B8B36F (void);
// 0x000001CB Mono.Btls.MonoBtlsObject/MonoBtlsHandle Mono.Btls.MonoBtlsObject::get_Handle()
extern void MonoBtlsObject_get_Handle_m929F55AA1CC6397BB81B37E33E6C295DF0F4B13E (void);
// 0x000001CC System.Boolean Mono.Btls.MonoBtlsObject::get_IsValid()
extern void MonoBtlsObject_get_IsValid_m81F9484EE0BB31515AC50158B182C87B8AB62B5F (void);
// 0x000001CD System.Void Mono.Btls.MonoBtlsObject::CheckThrow()
extern void MonoBtlsObject_CheckThrow_m3B61D9B98C8D6CF068CEEC067854D5A32C462FAD (void);
// 0x000001CE System.Exception Mono.Btls.MonoBtlsObject::SetException(System.Exception)
extern void MonoBtlsObject_SetException_mC444E69A432F5EBC62D37F29C5AF1E7F6480C01A (void);
// 0x000001CF System.Void Mono.Btls.MonoBtlsObject::CheckError(System.Boolean,System.String)
extern void MonoBtlsObject_CheckError_m64C6CD56095272D4EC8E67D2807460800CB6B877 (void);
// 0x000001D0 System.Void Mono.Btls.MonoBtlsObject::CheckError(System.Int32,System.String)
extern void MonoBtlsObject_CheckError_mC13A0B4648920B0530835D424FC350B1D0351DEF (void);
// 0x000001D1 System.Void Mono.Btls.MonoBtlsObject::CheckLastError(System.String)
extern void MonoBtlsObject_CheckLastError_m7E83AFAD66B4A04C528A019C790A7EA8F03D5CB4 (void);
// 0x000001D2 System.Void Mono.Btls.MonoBtlsObject::mono_btls_free(System.IntPtr)
extern void MonoBtlsObject_mono_btls_free_mC1D61AA77F5B78D22DEF4D0DC0AD151DB26FF087 (void);
// 0x000001D3 System.Void Mono.Btls.MonoBtlsObject::FreeDataPtr(System.IntPtr)
extern void MonoBtlsObject_FreeDataPtr_m762822A138134A35091FFAE34129CD0596901E0D (void);
// 0x000001D4 System.Void Mono.Btls.MonoBtlsObject::Close()
extern void MonoBtlsObject_Close_m721FFCCBC5320FF91B8F88651EC1013978B50A52 (void);
// 0x000001D5 System.Void Mono.Btls.MonoBtlsObject::Dispose(System.Boolean)
extern void MonoBtlsObject_Dispose_m5D56F90D3CDD44E63432AEB0F41F67DD6EB3E353 (void);
// 0x000001D6 System.Void Mono.Btls.MonoBtlsObject::Dispose()
extern void MonoBtlsObject_Dispose_m8E78236AF0BB3D6848B882BFAA2D574D24E5EB1A (void);
// 0x000001D7 System.Void Mono.Btls.MonoBtlsObject::Finalize()
extern void MonoBtlsObject_Finalize_mC56D7B0A1FEA92DDE1DBA0AF6C7715575CEBCE6A (void);
// 0x000001D8 System.Void Mono.Btls.MonoBtlsObject/MonoBtlsHandle::.ctor(System.IntPtr,System.Boolean)
extern void MonoBtlsHandle__ctor_mFE1DF2FF7208BD4481638701BDEF9F2B556B652D (void);
// 0x000001D9 System.Boolean Mono.Btls.MonoBtlsObject/MonoBtlsHandle::get_IsInvalid()
extern void MonoBtlsHandle_get_IsInvalid_mAEC6C53A163B00D46C9C36BD2196230616F6F43C (void);
// 0x000001DA Mono.Btls.MonoBtlsPkcs12/BoringPkcs12Handle Mono.Btls.MonoBtlsPkcs12::get_Handle()
extern void MonoBtlsPkcs12_get_Handle_m7AA7DEEA89783B8B9ECFE1B9351677A9BE4AD11C (void);
// 0x000001DB System.Void Mono.Btls.MonoBtlsPkcs12::mono_btls_pkcs12_free(System.IntPtr)
extern void MonoBtlsPkcs12_mono_btls_pkcs12_free_mEEED21018C033E7E6438617B6ABC241CFD86B853 (void);
// 0x000001DC System.IntPtr Mono.Btls.MonoBtlsPkcs12::mono_btls_pkcs12_new()
extern void MonoBtlsPkcs12_mono_btls_pkcs12_new_m49A58BD1E7CF96008821E9AE0A7AAAB196C52D4E (void);
// 0x000001DD System.Int32 Mono.Btls.MonoBtlsPkcs12::mono_btls_pkcs12_get_count(System.IntPtr)
extern void MonoBtlsPkcs12_mono_btls_pkcs12_get_count_mDE026ECA3212C9690582648FBDFBC00887779186 (void);
// 0x000001DE System.IntPtr Mono.Btls.MonoBtlsPkcs12::mono_btls_pkcs12_get_cert(System.IntPtr,System.Int32)
extern void MonoBtlsPkcs12_mono_btls_pkcs12_get_cert_m424422207D2723F4FAA1926E634D734F2E945574 (void);
// 0x000001DF System.Int32 Mono.Btls.MonoBtlsPkcs12::mono_btls_pkcs12_import(System.IntPtr,System.Void*,System.Int32,Microsoft.Win32.SafeHandles.SafePasswordHandle)
extern void MonoBtlsPkcs12_mono_btls_pkcs12_import_m1A9199FB8C08152D7420A0DDFCE061D7CB86530D (void);
// 0x000001E0 System.Int32 Mono.Btls.MonoBtlsPkcs12::mono_btls_pkcs12_has_private_key(System.IntPtr)
extern void MonoBtlsPkcs12_mono_btls_pkcs12_has_private_key_mF5CCCADB1D9E03972CC913034C8010412021AB1B (void);
// 0x000001E1 System.IntPtr Mono.Btls.MonoBtlsPkcs12::mono_btls_pkcs12_get_private_key(System.IntPtr)
extern void MonoBtlsPkcs12_mono_btls_pkcs12_get_private_key_m2D7ABD2B2FBA68CB1D7FDC4EE993ECB95678537B (void);
// 0x000001E2 System.Void Mono.Btls.MonoBtlsPkcs12::.ctor()
extern void MonoBtlsPkcs12__ctor_m0443539C1F555E7933C9C11738803D4C4A19F7A9 (void);
// 0x000001E3 System.Int32 Mono.Btls.MonoBtlsPkcs12::get_Count()
extern void MonoBtlsPkcs12_get_Count_m3B3AA72AB596F6F8D82B96F7B787C9AA718DF342 (void);
// 0x000001E4 Mono.Btls.MonoBtlsX509 Mono.Btls.MonoBtlsPkcs12::GetCertificate(System.Int32)
extern void MonoBtlsPkcs12_GetCertificate_m3EF3675C309529933E88E3D52E695B5E81AB5C78 (void);
// 0x000001E5 System.Void Mono.Btls.MonoBtlsPkcs12::Import(System.Byte[],Microsoft.Win32.SafeHandles.SafePasswordHandle)
extern void MonoBtlsPkcs12_Import_m7AE3F61C4E65DB51D4E6A266D42B80375B74C388 (void);
// 0x000001E6 System.Boolean Mono.Btls.MonoBtlsPkcs12::get_HasPrivateKey()
extern void MonoBtlsPkcs12_get_HasPrivateKey_m18B31C7E511B1C69BFD4F0C24207AA197C370033 (void);
// 0x000001E7 Mono.Btls.MonoBtlsKey Mono.Btls.MonoBtlsPkcs12::GetPrivateKey()
extern void MonoBtlsPkcs12_GetPrivateKey_m4B69EE9C50095AD4E0F9D224F52973AFA6F4982E (void);
// 0x000001E8 System.Void Mono.Btls.MonoBtlsPkcs12/BoringPkcs12Handle::.ctor(System.IntPtr)
extern void BoringPkcs12Handle__ctor_m29FBAECE2230F319C049BEFD64157C107BDD5FF3 (void);
// 0x000001E9 System.Boolean Mono.Btls.MonoBtlsPkcs12/BoringPkcs12Handle::ReleaseHandle()
extern void BoringPkcs12Handle_ReleaseHandle_m428BB05A57EF7DDCC5C35E185C1E869E06C9F8F4 (void);
// 0x000001EA System.Guid Mono.Btls.MonoBtlsProvider::get_ID()
extern void MonoBtlsProvider_get_ID_mCA08C6D8B652E685A654B072C3901E5D7C17482A (void);
// 0x000001EB System.String Mono.Btls.MonoBtlsProvider::get_Name()
extern void MonoBtlsProvider_get_Name_mB84D1E1A27CFAEC85FCDE48160773A36EF10D544 (void);
// 0x000001EC System.Void Mono.Btls.MonoBtlsProvider::.ctor()
extern void MonoBtlsProvider__ctor_m3BAD59FA2D9BCA7A2B1F029FE689A56A07B0810E (void);
// 0x000001ED System.Boolean Mono.Btls.MonoBtlsProvider::get_SupportsSslStream()
extern void MonoBtlsProvider_get_SupportsSslStream_mCA509641BE1177D1E2E5F774269A50A2124B28E4 (void);
// 0x000001EE System.Boolean Mono.Btls.MonoBtlsProvider::get_SupportsMonoExtensions()
extern void MonoBtlsProvider_get_SupportsMonoExtensions_mD63E3CF2CDBBFD5B03541937C90670D5AC5D3F49 (void);
// 0x000001EF System.Boolean Mono.Btls.MonoBtlsProvider::get_SupportsConnectionInfo()
extern void MonoBtlsProvider_get_SupportsConnectionInfo_m4EED7BD9FAA3ADD5D523AC635D0535C965DF2658 (void);
// 0x000001F0 System.Boolean Mono.Btls.MonoBtlsProvider::get_SupportsCleanShutdown()
extern void MonoBtlsProvider_get_SupportsCleanShutdown_m5489F01B86092220D84D502AF105E5034FE4A88E (void);
// 0x000001F1 System.Security.Authentication.SslProtocols Mono.Btls.MonoBtlsProvider::get_SupportedProtocols()
extern void MonoBtlsProvider_get_SupportedProtocols_m5D344FBA9F458952B8CB4FCFB609F4AEED56D80C (void);
// 0x000001F2 Mono.Net.Security.MobileAuthenticatedStream Mono.Btls.MonoBtlsProvider::CreateSslStream(System.Net.Security.SslStream,System.IO.Stream,System.Boolean,Mono.Security.Interface.MonoTlsSettings)
extern void MonoBtlsProvider_CreateSslStream_m36F5514259018235AD66D7DB0C84E5D7E35428C2 (void);
// 0x000001F3 System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.Btls.MonoBtlsProvider::GetNativeCertificate(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern void MonoBtlsProvider_GetNativeCertificate_m5277ABC9E56ED50DA43CB9E20AC005C4A31F392A (void);
// 0x000001F4 System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.Btls.MonoBtlsProvider::GetNativeCertificate(System.Security.Cryptography.X509Certificates.X509Certificate)
extern void MonoBtlsProvider_GetNativeCertificate_mFD2CBE90C78213D12B4F6A77BCA7E356D508F522 (void);
// 0x000001F5 System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.Btls.MonoBtlsProvider::GetNativeCertificate(System.Byte[],Microsoft.Win32.SafeHandles.SafePasswordHandle,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern void MonoBtlsProvider_GetNativeCertificate_m8CBBFBFC50A27D9AE00A738CDCE742A63ED40724 (void);
// 0x000001F6 Mono.Btls.MonoBtlsX509VerifyParam Mono.Btls.MonoBtlsProvider::GetVerifyParam(Mono.Security.Interface.MonoTlsSettings,System.String,System.Boolean)
extern void MonoBtlsProvider_GetVerifyParam_m4E0754CE4B0B9F9604E9996698168D5C8715D775 (void);
// 0x000001F7 System.Boolean Mono.Btls.MonoBtlsProvider::ValidateCertificate(Mono.Net.Security.ChainValidationHelper,System.String,System.Boolean,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Boolean,System.Security.Cryptography.X509Certificates.X509Chain&,System.Net.Security.SslPolicyErrors&,System.Int32&)
extern void MonoBtlsProvider_ValidateCertificate_m0272E85178C08F977C093FEE3251CE5DFA9161E9 (void);
// 0x000001F8 System.Boolean Mono.Btls.MonoBtlsProvider::ValidateCertificate(Mono.Btls.MonoBtlsX509Chain,Mono.Btls.MonoBtlsX509VerifyParam)
extern void MonoBtlsProvider_ValidateCertificate_mDFB6088BC65546798D540127BDD4872C57D96E12 (void);
// 0x000001F9 System.Void Mono.Btls.MonoBtlsProvider::CheckValidationResult(Mono.Net.Security.ChainValidationHelper,System.String,System.Boolean,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Boolean,System.Security.Cryptography.X509Certificates.X509Chain,Mono.Btls.MonoBtlsX509StoreCtx,System.Boolean,System.Net.Security.SslPolicyErrors&,System.Int32&)
extern void MonoBtlsProvider_CheckValidationResult_mADAFCE1E140B64E2BC7D6E233469960A3124C113 (void);
// 0x000001FA System.Security.Cryptography.X509Certificates.X509ChainStatusFlags Mono.Btls.MonoBtlsProvider::MapVerifyErrorToChainStatus(Mono.Btls.MonoBtlsX509Error)
extern void MonoBtlsProvider_MapVerifyErrorToChainStatus_m59B25FB5D5804BBC06AFFF9430D933BB75AA5143 (void);
// 0x000001FB System.Void Mono.Btls.MonoBtlsProvider::SetupCertificateStore(Mono.Btls.MonoBtlsX509Store,Mono.Security.Interface.MonoTlsSettings,System.Boolean)
extern void MonoBtlsProvider_SetupCertificateStore_mB0E6DC01DE3C8D962A6B352C022B67FE95648640 (void);
// 0x000001FC System.Void Mono.Btls.MonoBtlsProvider::SetupDefaultCertificateStore(Mono.Btls.MonoBtlsX509Store)
extern void MonoBtlsProvider_SetupDefaultCertificateStore_m1DC6267AC90D4E0C16C36219C2D3A29C5A8410E7 (void);
// 0x000001FD System.Void Mono.Btls.MonoBtlsProvider::AddUserStore(Mono.Btls.MonoBtlsX509Store)
extern void MonoBtlsProvider_AddUserStore_mFB218F3546B995CB7F72B94656ADDBC213F449A6 (void);
// 0x000001FE System.Void Mono.Btls.MonoBtlsProvider::AddMachineStore(Mono.Btls.MonoBtlsX509Store)
extern void MonoBtlsProvider_AddMachineStore_m0D472A8BBDC3EFC8CFF14F1A487C0C1C45DBA28E (void);
// 0x000001FF System.Void Mono.Btls.MonoBtlsProvider::AddTrustedRoots(Mono.Btls.MonoBtlsX509Store,Mono.Security.Interface.MonoTlsSettings,System.Boolean)
extern void MonoBtlsProvider_AddTrustedRoots_m2BD630627150BDA01BBD22D12D9ED0E08115CD44 (void);
// 0x00000200 System.Security.Cryptography.X509Certificates.X509Certificate2 Mono.Btls.MonoBtlsProvider::CreateCertificate(Mono.Btls.MonoBtlsX509)
extern void MonoBtlsProvider_CreateCertificate_m99776D7CD36710CDACE0FD72B86D4C0B1B8150D3 (void);
// 0x00000201 System.Security.Cryptography.X509Certificates.X509Chain Mono.Btls.MonoBtlsProvider::GetManagedChain(Mono.Btls.MonoBtlsX509Chain)
extern void MonoBtlsProvider_GetManagedChain_m3A7BF034BF541D0232C8F6B40C2191D34FD5DD25 (void);
// 0x00000202 Mono.Btls.MonoBtlsX509 Mono.Btls.MonoBtlsProvider::GetBtlsCertificate(System.Security.Cryptography.X509Certificates.X509Certificate)
extern void MonoBtlsProvider_GetBtlsCertificate_mAE23E9D7430A7F9FEE847269DFC37B4489C95EFD (void);
// 0x00000203 Mono.Btls.MonoBtlsX509Chain Mono.Btls.MonoBtlsProvider::GetNativeChain(System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern void MonoBtlsProvider_GetNativeChain_m2C514D3E480513FB201FF510A5EA3B769FF67A75 (void);
// 0x00000204 System.Void Mono.Btls.MonoBtlsVerifyCallback::.ctor(System.Object,System.IntPtr)
extern void MonoBtlsVerifyCallback__ctor_m8F1128CB88D8E552034C35F1115F9642DB2A00CB (void);
// 0x00000205 System.Int32 Mono.Btls.MonoBtlsVerifyCallback::Invoke(Mono.Btls.MonoBtlsX509StoreCtx)
extern void MonoBtlsVerifyCallback_Invoke_m825965B6B4C69CA0287ED84C20688B62BC5009DB (void);
// 0x00000206 System.Void Mono.Btls.MonoBtlsSelectCallback::.ctor(System.Object,System.IntPtr)
extern void MonoBtlsSelectCallback__ctor_m7B7D62D2596DC675786AA7FA342F7188BB6D9834 (void);
// 0x00000207 System.Int32 Mono.Btls.MonoBtlsSelectCallback::Invoke(System.String[])
extern void MonoBtlsSelectCallback_Invoke_m758D66BCE6FFA5CF23148236694A0ADE7835F833 (void);
// 0x00000208 System.Void Mono.Btls.MonoBtlsServerNameCallback::.ctor(System.Object,System.IntPtr)
extern void MonoBtlsServerNameCallback__ctor_m245B337F77B4B68D1DD967C5AC999F6950AFA039 (void);
// 0x00000209 System.Int32 Mono.Btls.MonoBtlsServerNameCallback::Invoke()
extern void MonoBtlsServerNameCallback_Invoke_m4AA0CCFC7FEEACD68818537DC6D829872A2B619C (void);
// 0x0000020A System.Void Mono.Btls.MonoBtlsSsl::mono_btls_ssl_destroy(System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_destroy_m0AFA7E41AC0D4502C2DED1D4F43D179B992E8D8F (void);
// 0x0000020B System.IntPtr Mono.Btls.MonoBtlsSsl::mono_btls_ssl_new(System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_new_m5114FC79FE517C0A825E7EE033891A68DB098784 (void);
// 0x0000020C System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_use_certificate(System.IntPtr,System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_use_certificate_m5F43AA1BA2388D702700762D2D619ED674650D4E (void);
// 0x0000020D System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_use_private_key(System.IntPtr,System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_use_private_key_mFE842927801E542794BA55118A94ACDB74415FE1 (void);
// 0x0000020E System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_add_chain_certificate(System.IntPtr,System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_add_chain_certificate_mAC4BEB909E6E286B069E36DA0F50D8A3C0C43C0E (void);
// 0x0000020F System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_accept(System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_accept_mEF4E97286CC13D00C5B92E146EA886E258A892DC (void);
// 0x00000210 System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_connect(System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_connect_m47EC7A445C611BE5ED51827D177D099AFFD05F10 (void);
// 0x00000211 System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_handshake(System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_handshake_m7E16FC5F7F1800C74B54C1D78651FFF8D3800BFF (void);
// 0x00000212 System.Void Mono.Btls.MonoBtlsSsl::mono_btls_ssl_close(System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_close_mA473DEE6ED19B3D23DD0D63D82E4213D339E0484 (void);
// 0x00000213 System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_shutdown(System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_shutdown_mA15CB14FB516D07D3B7F358C076599E135417450 (void);
// 0x00000214 System.Void Mono.Btls.MonoBtlsSsl::mono_btls_ssl_set_quiet_shutdown(System.IntPtr,System.Int32)
extern void MonoBtlsSsl_mono_btls_ssl_set_quiet_shutdown_m5A553D7EC20BCFF8D5653C2777B679AFBD54E237 (void);
// 0x00000215 System.Void Mono.Btls.MonoBtlsSsl::mono_btls_ssl_set_bio(System.IntPtr,System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_set_bio_mF5A8F4790AA61B1FCA72D063F6A689957A72EF01 (void);
// 0x00000216 System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_read(System.IntPtr,System.IntPtr,System.Int32)
extern void MonoBtlsSsl_mono_btls_ssl_read_m47C6ACDE0165F28162E26DEA766353AD12FFABA2 (void);
// 0x00000217 System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_write(System.IntPtr,System.IntPtr,System.Int32)
extern void MonoBtlsSsl_mono_btls_ssl_write_m8BEF50C4A00E2E386021FF5BA4DB26BF26F94D8E (void);
// 0x00000218 System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_get_error(System.IntPtr,System.Int32)
extern void MonoBtlsSsl_mono_btls_ssl_get_error_mA76FBC1696B6F7A315998918EE8662B00850A84A (void);
// 0x00000219 System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_get_version(System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_get_version_m9D689AB91D22F5B7F086B27D6F662716AC1C3F46 (void);
// 0x0000021A System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_get_cipher(System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_get_cipher_mD0F85F9EC48100506E63309B68931B087CB4B447 (void);
// 0x0000021B System.IntPtr Mono.Btls.MonoBtlsSsl::mono_btls_ssl_get_peer_certificate(System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_get_peer_certificate_m125C12698C7C59D398513DBD2FB9FE11F543FC6D (void);
// 0x0000021C System.Void Mono.Btls.MonoBtlsSsl::mono_btls_ssl_print_errors_cb(System.IntPtr,System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_print_errors_cb_m96A1A5C32FDDFA460C49DEF5A46A965204E575DE (void);
// 0x0000021D System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_set_server_name(System.IntPtr,System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_set_server_name_mBA32BA41B58854F039990D3CDFD4E1C7B1592E51 (void);
// 0x0000021E System.IntPtr Mono.Btls.MonoBtlsSsl::mono_btls_ssl_get_server_name(System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_get_server_name_m0954C61331CCFEBCD971F088F8007D15E072071C (void);
// 0x0000021F System.Void Mono.Btls.MonoBtlsSsl::mono_btls_ssl_set_renegotiate_mode(System.IntPtr,System.Int32)
extern void MonoBtlsSsl_mono_btls_ssl_set_renegotiate_mode_mA7D9D649F08A5DEFDAD0E8531FDB9B22BEDF202F (void);
// 0x00000220 System.Int32 Mono.Btls.MonoBtlsSsl::mono_btls_ssl_renegotiate_pending(System.IntPtr)
extern void MonoBtlsSsl_mono_btls_ssl_renegotiate_pending_m581B2265253163ACCE6A50515544A30CCA3EB255 (void);
// 0x00000221 Mono.Btls.MonoBtlsSsl/BoringSslHandle Mono.Btls.MonoBtlsSsl::Create_internal(Mono.Btls.MonoBtlsSslCtx)
extern void MonoBtlsSsl_Create_internal_mB7DBB50C4AE91B6347E11600F4BC53C94739FBED (void);
// 0x00000222 System.Void Mono.Btls.MonoBtlsSsl::.ctor(Mono.Btls.MonoBtlsSslCtx)
extern void MonoBtlsSsl__ctor_mE665FFA57F03F58B3516A2E113031E24E3AEB57C (void);
// 0x00000223 Mono.Btls.MonoBtlsSsl/BoringSslHandle Mono.Btls.MonoBtlsSsl::get_Handle()
extern void MonoBtlsSsl_get_Handle_mD7D81179837A024646AF720D8E390C67ADFF2453 (void);
// 0x00000224 System.Void Mono.Btls.MonoBtlsSsl::SetBio(Mono.Btls.MonoBtlsBio)
extern void MonoBtlsSsl_SetBio_m12B1F338F813EEDFE846539A618CB47009BCC8A1 (void);
// 0x00000225 System.Exception Mono.Btls.MonoBtlsSsl::ThrowError(System.String)
extern void MonoBtlsSsl_ThrowError_m433E4D0C73EBC76A2DEE8A5DBC78AFCE07F40CCC (void);
// 0x00000226 Mono.Btls.MonoBtlsSslError Mono.Btls.MonoBtlsSsl::GetError(System.Int32)
extern void MonoBtlsSsl_GetError_mCD0707D0B720A563FA21F804ACC411922B79F994 (void);
// 0x00000227 System.Void Mono.Btls.MonoBtlsSsl::SetCertificate(Mono.Btls.MonoBtlsX509)
extern void MonoBtlsSsl_SetCertificate_mEB04028459613ADE7035551AAB5C015319D8568D (void);
// 0x00000228 System.Void Mono.Btls.MonoBtlsSsl::SetPrivateKey(Mono.Btls.MonoBtlsKey)
extern void MonoBtlsSsl_SetPrivateKey_m73596A1183D40F136A5539E41F0269C264EC1182 (void);
// 0x00000229 System.Void Mono.Btls.MonoBtlsSsl::AddIntermediateCertificate(Mono.Btls.MonoBtlsX509)
extern void MonoBtlsSsl_AddIntermediateCertificate_mE4967C4FC58AF0D78CB6F887667AFC6BFBC78028 (void);
// 0x0000022A Mono.Btls.MonoBtlsSslError Mono.Btls.MonoBtlsSsl::Accept()
extern void MonoBtlsSsl_Accept_m2FFF1FA57235A2DCB0C04E3B5A11CC41D90F0485 (void);
// 0x0000022B Mono.Btls.MonoBtlsSslError Mono.Btls.MonoBtlsSsl::Connect()
extern void MonoBtlsSsl_Connect_mFE6BEB2E52F8E6FFC8DCBBCE0405E0ED62553A77 (void);
// 0x0000022C Mono.Btls.MonoBtlsSslError Mono.Btls.MonoBtlsSsl::Handshake()
extern void MonoBtlsSsl_Handshake_m99EE68C3129DD273EBE5138ACC0D288F5D862BBB (void);
// 0x0000022D System.Int32 Mono.Btls.MonoBtlsSsl::PrintErrorsCallback(System.IntPtr,System.IntPtr,System.IntPtr)
extern void MonoBtlsSsl_PrintErrorsCallback_mF161ADF7DBDB81C47BDD587C87E1275DEE5B7A45 (void);
// 0x0000022E System.String Mono.Btls.MonoBtlsSsl::GetErrors()
extern void MonoBtlsSsl_GetErrors_m608539CB7B246064023EE5BD33D782401D07DC40 (void);
// 0x0000022F System.Void Mono.Btls.MonoBtlsSsl::PrintErrors()
extern void MonoBtlsSsl_PrintErrors_mC20509AB8D7DEE3B6779B92C4DDF55CC211E95DC (void);
// 0x00000230 Mono.Btls.MonoBtlsSslError Mono.Btls.MonoBtlsSsl::Read(System.IntPtr,System.Int32&)
extern void MonoBtlsSsl_Read_mE903D7417006C8A9D77AACD96A73863DDF7050BB (void);
// 0x00000231 Mono.Btls.MonoBtlsSslError Mono.Btls.MonoBtlsSsl::Write(System.IntPtr,System.Int32&)
extern void MonoBtlsSsl_Write_mB6524341838C199A948FD6DA6B81DE3591655B5D (void);
// 0x00000232 System.Int32 Mono.Btls.MonoBtlsSsl::GetVersion()
extern void MonoBtlsSsl_GetVersion_m3EE8DEF9D9F0187848EFE277A29120D7F2C49726 (void);
// 0x00000233 System.Int32 Mono.Btls.MonoBtlsSsl::GetCipher()
extern void MonoBtlsSsl_GetCipher_m394E4C612F94448B5C24F0EB0D6FAAACB85EE554 (void);
// 0x00000234 Mono.Btls.MonoBtlsX509 Mono.Btls.MonoBtlsSsl::GetPeerCertificate()
extern void MonoBtlsSsl_GetPeerCertificate_mD0C0DB8E9C3B7E577EF88B1EB522364B3FC78B1B (void);
// 0x00000235 System.Void Mono.Btls.MonoBtlsSsl::SetServerName(System.String)
extern void MonoBtlsSsl_SetServerName_mB31156F4873EB863AC0172FE329DA0464FC55F47 (void);
// 0x00000236 System.String Mono.Btls.MonoBtlsSsl::GetServerName()
extern void MonoBtlsSsl_GetServerName_mE4D22C3884972C938F097D6B15AADDCA25B29048 (void);
// 0x00000237 System.Void Mono.Btls.MonoBtlsSsl::Shutdown()
extern void MonoBtlsSsl_Shutdown_m703B0B4E90128785E6412DCF184BC4A9B01EF169 (void);
// 0x00000238 System.Void Mono.Btls.MonoBtlsSsl::SetQuietShutdown()
extern void MonoBtlsSsl_SetQuietShutdown_m73C451BE677698E14C633EDC8F33A30F0E4B326D (void);
// 0x00000239 System.Void Mono.Btls.MonoBtlsSsl::Close()
extern void MonoBtlsSsl_Close_mB59931062F42A522BD0281DFA0169FC724ABEC27 (void);
// 0x0000023A System.Void Mono.Btls.MonoBtlsSsl::SetRenegotiateMode(Mono.Btls.MonoBtlsSslRenegotiateMode)
extern void MonoBtlsSsl_SetRenegotiateMode_m3D1E1B6E4E1460680B43E35CB032C8571E0705DC (void);
// 0x0000023B System.Boolean Mono.Btls.MonoBtlsSsl::RenegotiatePending()
extern void MonoBtlsSsl_RenegotiatePending_mF3477EE7E3F6DF2222F987F3F5889A120A45915D (void);
// 0x0000023C System.Void Mono.Btls.MonoBtlsSsl/BoringSslHandle::.ctor(System.IntPtr)
extern void BoringSslHandle__ctor_m0DA0F6EDC111D843B46B27029C94E5B1DEFB780C (void);
// 0x0000023D System.Boolean Mono.Btls.MonoBtlsSsl/BoringSslHandle::ReleaseHandle()
extern void BoringSslHandle_ReleaseHandle_m9701A56FF4D5ADFAE0624ABBB9157D20EF3DF5A8 (void);
// 0x0000023E System.Void Mono.Btls.MonoBtlsSsl/PrintErrorsCallbackFunc::.ctor(System.Object,System.IntPtr)
extern void PrintErrorsCallbackFunc__ctor_mD64624B7F3BC980A50DA2D168E8B354DBF173D20 (void);
// 0x0000023F System.Int32 Mono.Btls.MonoBtlsSsl/PrintErrorsCallbackFunc::Invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern void PrintErrorsCallbackFunc_Invoke_m1C7DE7EF19681297B4451713E2706DB4414B57E6 (void);
// 0x00000240 Mono.Btls.MonoBtlsSslCtx/BoringSslCtxHandle Mono.Btls.MonoBtlsSslCtx::get_Handle()
extern void MonoBtlsSslCtx_get_Handle_mB22E16E1A65DD17AE1714D71366C945B5C4616C5 (void);
// 0x00000241 System.IntPtr Mono.Btls.MonoBtlsSslCtx::mono_btls_ssl_ctx_new()
extern void MonoBtlsSslCtx_mono_btls_ssl_ctx_new_mA803FADA54CA1ED023AF761296ACC291F1CDACF3 (void);
// 0x00000242 System.Int32 Mono.Btls.MonoBtlsSslCtx::mono_btls_ssl_ctx_free(System.IntPtr)
extern void MonoBtlsSslCtx_mono_btls_ssl_ctx_free_m1A7BE8FC9A5BA191A7265FACFCDC5F05CABCBB5B (void);
// 0x00000243 System.Void Mono.Btls.MonoBtlsSslCtx::mono_btls_ssl_ctx_initialize(System.IntPtr,System.IntPtr)
extern void MonoBtlsSslCtx_mono_btls_ssl_ctx_initialize_m9FC92A37AE4BCA9B6365082A39203A8B128BD1FF (void);
// 0x00000244 System.Void Mono.Btls.MonoBtlsSslCtx::mono_btls_ssl_ctx_set_cert_verify_callback(System.IntPtr,System.IntPtr,System.Int32)
extern void MonoBtlsSslCtx_mono_btls_ssl_ctx_set_cert_verify_callback_m821C35D7B4DBC48ADD2629297367FDEA9FDCA8AB (void);
// 0x00000245 System.Void Mono.Btls.MonoBtlsSslCtx::mono_btls_ssl_ctx_set_cert_select_callback(System.IntPtr,System.IntPtr)
extern void MonoBtlsSslCtx_mono_btls_ssl_ctx_set_cert_select_callback_mE6BA9666ECC96DC257D1AF16706624202CF9C928 (void);
// 0x00000246 System.Void Mono.Btls.MonoBtlsSslCtx::mono_btls_ssl_ctx_set_min_version(System.IntPtr,System.Int32)
extern void MonoBtlsSslCtx_mono_btls_ssl_ctx_set_min_version_mB656FCEDFCC04D6C49FA4DCB873046DD5B8B99A4 (void);
// 0x00000247 System.Void Mono.Btls.MonoBtlsSslCtx::mono_btls_ssl_ctx_set_max_version(System.IntPtr,System.Int32)
extern void MonoBtlsSslCtx_mono_btls_ssl_ctx_set_max_version_mEF4057F269BFFD8B495C52AF53207BA5F669C890 (void);
// 0x00000248 System.Int32 Mono.Btls.MonoBtlsSslCtx::mono_btls_ssl_ctx_set_ciphers(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
extern void MonoBtlsSslCtx_mono_btls_ssl_ctx_set_ciphers_m5264971DF99AE39A29279ED15A9429014660491F (void);
// 0x00000249 System.Int32 Mono.Btls.MonoBtlsSslCtx::mono_btls_ssl_ctx_set_verify_param(System.IntPtr,System.IntPtr)
extern void MonoBtlsSslCtx_mono_btls_ssl_ctx_set_verify_param_m1DEFE9109A85CB4A8D1C889C530C17B9A4738999 (void);
// 0x0000024A System.Int32 Mono.Btls.MonoBtlsSslCtx::mono_btls_ssl_ctx_set_client_ca_list(System.IntPtr,System.Int32,System.IntPtr,System.IntPtr)
extern void MonoBtlsSslCtx_mono_btls_ssl_ctx_set_client_ca_list_mB1FB9F45EC726867A26E76423A6C92F51DF008F6 (void);
// 0x0000024B System.Void Mono.Btls.MonoBtlsSslCtx::mono_btls_ssl_ctx_set_server_name_callback(System.IntPtr,System.IntPtr)
extern void MonoBtlsSslCtx_mono_btls_ssl_ctx_set_server_name_callback_mCBCDD78039946B3C1DEF05EF8383C55247CF4C2B (void);
// 0x0000024C System.Void Mono.Btls.MonoBtlsSslCtx::.ctor()
extern void MonoBtlsSslCtx__ctor_m3E69692A614EBED4C22B0E942121CBB38198D12B (void);
// 0x0000024D System.Void Mono.Btls.MonoBtlsSslCtx::.ctor(Mono.Btls.MonoBtlsSslCtx/BoringSslCtxHandle)
extern void MonoBtlsSslCtx__ctor_m5576AC303A8B0D1C2360CC2147579F35748BFF2D (void);
// 0x0000024E Mono.Btls.MonoBtlsX509Store Mono.Btls.MonoBtlsSslCtx::get_CertificateStore()
extern void MonoBtlsSslCtx_get_CertificateStore_m70BC1E033FDC7C6E1E57301670787F89EC20AE76 (void);
// 0x0000024F System.Int32 Mono.Btls.MonoBtlsSslCtx::VerifyCallback(System.Boolean,Mono.Btls.MonoBtlsX509StoreCtx)
extern void MonoBtlsSslCtx_VerifyCallback_mB0FE711DA675DBB6992CA8D4A9C7832458B48FDA (void);
// 0x00000250 System.Int32 Mono.Btls.MonoBtlsSslCtx::NativeVerifyCallback(System.IntPtr,System.Int32,System.IntPtr)
extern void MonoBtlsSslCtx_NativeVerifyCallback_mFDF10D01CAAD040E50EE6F53FC1D4155EF696218 (void);
// 0x00000251 System.Int32 Mono.Btls.MonoBtlsSslCtx::NativeSelectCallback(System.IntPtr,System.Int32,System.IntPtr,System.IntPtr)
extern void MonoBtlsSslCtx_NativeSelectCallback_m6CA34F71A6093C7260A9F066D6C9FB3CD0CC86DB (void);
// 0x00000252 System.String[] Mono.Btls.MonoBtlsSslCtx::CopyIssuers(System.Int32,System.IntPtr,System.IntPtr)
extern void MonoBtlsSslCtx_CopyIssuers_m1EEE67A4577682033606F576823D8E6BC7795330 (void);
// 0x00000253 System.Void Mono.Btls.MonoBtlsSslCtx::SetVerifyCallback(Mono.Btls.MonoBtlsVerifyCallback,System.Boolean)
extern void MonoBtlsSslCtx_SetVerifyCallback_m0D163F264758B0F20DEEE8218DD4D9E727AFD799 (void);
// 0x00000254 System.Void Mono.Btls.MonoBtlsSslCtx::SetSelectCallback(Mono.Btls.MonoBtlsSelectCallback)
extern void MonoBtlsSslCtx_SetSelectCallback_mF70092A4C30087EF4DCC62D13D0F4879DD6F3803 (void);
// 0x00000255 System.Void Mono.Btls.MonoBtlsSslCtx::SetMinVersion(System.Int32)
extern void MonoBtlsSslCtx_SetMinVersion_m7853260977C5BF2F2B51D35C8E1E8A423EE6EC09 (void);
// 0x00000256 System.Void Mono.Btls.MonoBtlsSslCtx::SetMaxVersion(System.Int32)
extern void MonoBtlsSslCtx_SetMaxVersion_m51CF8D0C80F0CBE225C23B08D8222894E609CB6B (void);
// 0x00000257 System.Void Mono.Btls.MonoBtlsSslCtx::SetCiphers(System.Int16[],System.Boolean)
extern void MonoBtlsSslCtx_SetCiphers_mC3EF46AFD033E9C4C62772C17E983B8240C9FCB5 (void);
// 0x00000258 System.Void Mono.Btls.MonoBtlsSslCtx::SetVerifyParam(Mono.Btls.MonoBtlsX509VerifyParam)
extern void MonoBtlsSslCtx_SetVerifyParam_m4D65EAA326696C938B22E9D96AEE91A02A294F64 (void);
// 0x00000259 System.Void Mono.Btls.MonoBtlsSslCtx::SetClientCertificateIssuers(System.String[])
extern void MonoBtlsSslCtx_SetClientCertificateIssuers_m2E5E54CC451A210F235833240AC2B7A3989554CC (void);
// 0x0000025A System.Void Mono.Btls.MonoBtlsSslCtx::SetServerNameCallback(Mono.Btls.MonoBtlsServerNameCallback)
extern void MonoBtlsSslCtx_SetServerNameCallback_m93CCA4BE25A5BE978B79EE6DC2150A447FE66D9D (void);
// 0x0000025B System.Int32 Mono.Btls.MonoBtlsSslCtx::NativeServerNameCallback(System.IntPtr)
extern void MonoBtlsSslCtx_NativeServerNameCallback_mFF311CE931F6A075940D1B6828D1FB517AF03838 (void);
// 0x0000025C System.Void Mono.Btls.MonoBtlsSslCtx::Close()
extern void MonoBtlsSslCtx_Close_mB3C805DE72F5DD81D7F035E5F869412D870EF711 (void);
// 0x0000025D System.Void Mono.Btls.MonoBtlsSslCtx/BoringSslCtxHandle::.ctor(System.IntPtr)
extern void BoringSslCtxHandle__ctor_mF43CD26E987DABD205A2383B900C30A14BCF3C0F (void);
// 0x0000025E System.Boolean Mono.Btls.MonoBtlsSslCtx/BoringSslCtxHandle::ReleaseHandle()
extern void BoringSslCtxHandle_ReleaseHandle_m1D261E3DA2C2B52CE8C0EA15587DB7193A56346C (void);
// 0x0000025F System.Void Mono.Btls.MonoBtlsSslCtx/NativeVerifyFunc::.ctor(System.Object,System.IntPtr)
extern void NativeVerifyFunc__ctor_m57E97D28673DE3AA35DB5F421B50E4DEFA8CE5BE (void);
// 0x00000260 System.Int32 Mono.Btls.MonoBtlsSslCtx/NativeVerifyFunc::Invoke(System.IntPtr,System.Int32,System.IntPtr)
extern void NativeVerifyFunc_Invoke_m83D46A16DA70BD1D0BDEFE8B94F360B42C27ABED (void);
// 0x00000261 System.Void Mono.Btls.MonoBtlsSslCtx/NativeSelectFunc::.ctor(System.Object,System.IntPtr)
extern void NativeSelectFunc__ctor_mCC793EE6C123121F715F3CFD8C23A9A331169608 (void);
// 0x00000262 System.Int32 Mono.Btls.MonoBtlsSslCtx/NativeSelectFunc::Invoke(System.IntPtr,System.Int32,System.IntPtr,System.IntPtr)
extern void NativeSelectFunc_Invoke_m92191EA94B7513F04CEB52D1D2EEEB1DA3E78063 (void);
// 0x00000263 System.Void Mono.Btls.MonoBtlsSslCtx/NativeServerNameFunc::.ctor(System.Object,System.IntPtr)
extern void NativeServerNameFunc__ctor_mB04711B6D7A98DB9A773CC0041F6099EB3CBF171 (void);
// 0x00000264 System.Int32 Mono.Btls.MonoBtlsSslCtx/NativeServerNameFunc::Invoke(System.IntPtr)
extern void NativeServerNameFunc_Invoke_mC6F38842460FFECBD5545EBD2B0115151283F0B4 (void);
// 0x00000265 System.Void Mono.Btls.MonoBtlsStream::.ctor(System.IO.Stream,System.Boolean,System.Net.Security.SslStream,Mono.Security.Interface.MonoTlsSettings,Mono.Net.Security.MobileTlsProvider)
extern void MonoBtlsStream__ctor_mFF14FECC3A05B0FEA3779DDEE2B2486F4ADF9BCE (void);
// 0x00000266 Mono.Net.Security.MobileTlsContext Mono.Btls.MonoBtlsStream::CreateContext(Mono.Net.Security.MonoSslAuthenticationOptions)
extern void MonoBtlsStream_CreateContext_m738A83D66948E39D29AE299287E9E9D6695FD512 (void);
// 0x00000267 System.Boolean Mono.Btls.MonoBtlsUtils::Compare(System.Byte[],System.Byte[])
extern void MonoBtlsUtils_Compare_mE8D3A803B86EBCF644E1B535FD3E64B881AA26E9 (void);
// 0x00000268 System.Boolean Mono.Btls.MonoBtlsUtils::AppendEntry(System.Text.StringBuilder,Mono.Btls.MonoBtlsX509Name,System.Int32,System.String,System.Boolean)
extern void MonoBtlsUtils_AppendEntry_mFBB6CD4FB64E595EABD27AD9F0F764732B1FBA96 (void);
// 0x00000269 System.String Mono.Btls.MonoBtlsUtils::FormatName(Mono.Btls.MonoBtlsX509Name,System.Boolean,System.String,System.Boolean)
extern void MonoBtlsUtils_FormatName_mA05387DD7121DE816001E82D3AD3DA73BCD00EC4 (void);
// 0x0000026A System.Void Mono.Btls.MonoBtlsUtils::.cctor()
extern void MonoBtlsUtils__cctor_m7677185E4B3BCD92B665FDAB6E014D98317D8E2E (void);
// 0x0000026B Mono.Btls.MonoBtlsX509/BoringX509Handle Mono.Btls.MonoBtlsX509::get_Handle()
extern void MonoBtlsX509_get_Handle_m3FD0B08C08DC8EB7CAB3F19C54B14AA4A228B3B8 (void);
// 0x0000026C System.Void Mono.Btls.MonoBtlsX509::.ctor(Mono.Btls.MonoBtlsX509/BoringX509Handle)
extern void MonoBtlsX509__ctor_m05B9938DCE1DE8F04B7405D3EB10C63BF895BE6B (void);
// 0x0000026D System.IntPtr Mono.Btls.MonoBtlsX509::mono_btls_x509_up_ref(System.IntPtr)
extern void MonoBtlsX509_mono_btls_x509_up_ref_mCC6CE3CA7AE784DD9F1FB6583F1CB8B5351571DE (void);
// 0x0000026E System.IntPtr Mono.Btls.MonoBtlsX509::mono_btls_x509_from_data(System.IntPtr,System.Int32,Mono.Btls.MonoBtlsX509Format)
extern void MonoBtlsX509_mono_btls_x509_from_data_m9EC8E0AEE86C66F5A3AF364275D1F2D1DEC3DB2A (void);
// 0x0000026F System.IntPtr Mono.Btls.MonoBtlsX509::mono_btls_x509_get_subject_name(System.IntPtr)
extern void MonoBtlsX509_mono_btls_x509_get_subject_name_mF3AD0FD1FD33BB61A230459BEF4FF5492A65871E (void);
// 0x00000270 System.Int32 Mono.Btls.MonoBtlsX509::mono_btls_x509_get_raw_data(System.IntPtr,System.IntPtr,Mono.Btls.MonoBtlsX509Format)
extern void MonoBtlsX509_mono_btls_x509_get_raw_data_m2522AB2F57663D4193FD2D9E0D32F3200D7BC418 (void);
// 0x00000271 System.Int32 Mono.Btls.MonoBtlsX509::mono_btls_x509_cmp(System.IntPtr,System.IntPtr)
extern void MonoBtlsX509_mono_btls_x509_cmp_mAE0EFAB10B563BB9BE09CA6EE557C8B205C01D24 (void);
// 0x00000272 System.Void Mono.Btls.MonoBtlsX509::mono_btls_x509_free(System.IntPtr)
extern void MonoBtlsX509_mono_btls_x509_free_m61E30328A4F0976CAA4E8616417D7B9EF424A808 (void);
// 0x00000273 System.Int32 Mono.Btls.MonoBtlsX509::mono_btls_x509_add_explicit_trust(System.IntPtr,Mono.Btls.MonoBtlsX509TrustKind)
extern void MonoBtlsX509_mono_btls_x509_add_explicit_trust_m1B974EF4F14629491F9478D5BBBBC491D43A7C69 (void);
// 0x00000274 Mono.Btls.MonoBtlsX509 Mono.Btls.MonoBtlsX509::Copy()
extern void MonoBtlsX509_Copy_mE36DCC64F4761F579F1A6C28996101B23506BB1B (void);
// 0x00000275 Mono.Btls.MonoBtlsX509 Mono.Btls.MonoBtlsX509::LoadFromData(System.Byte[],Mono.Btls.MonoBtlsX509Format)
extern void MonoBtlsX509_LoadFromData_m2FACD8CC31EF32F86A20C2D6C25F750F3807D171 (void);
// 0x00000276 Mono.Btls.MonoBtlsX509Name Mono.Btls.MonoBtlsX509::GetSubjectName()
extern void MonoBtlsX509_GetSubjectName_m6E405347CF8D3ED807EBD1C46CD38767C34556D3 (void);
// 0x00000277 System.Int64 Mono.Btls.MonoBtlsX509::GetSubjectNameHash()
extern void MonoBtlsX509_GetSubjectNameHash_mDDEF7BAE05A848383436D739D74460B82D5B47FF (void);
// 0x00000278 System.Byte[] Mono.Btls.MonoBtlsX509::GetRawData(Mono.Btls.MonoBtlsX509Format)
extern void MonoBtlsX509_GetRawData_m4B52A8FF5917F7B0DB9B2E355623F315AC735906 (void);
// 0x00000279 System.Int32 Mono.Btls.MonoBtlsX509::Compare(Mono.Btls.MonoBtlsX509,Mono.Btls.MonoBtlsX509)
extern void MonoBtlsX509_Compare_m7197975E987FC0C0F66B6F5289B6BA48EEDFEF41 (void);
// 0x0000027A System.Void Mono.Btls.MonoBtlsX509::AddExplicitTrust(Mono.Btls.MonoBtlsX509TrustKind)
extern void MonoBtlsX509_AddExplicitTrust_m328F124CDBFB8D5649F58868425B2D9617398466 (void);
// 0x0000027B System.Void Mono.Btls.MonoBtlsX509/BoringX509Handle::.ctor(System.IntPtr)
extern void BoringX509Handle__ctor_mBA555CCB295495BD93AEEEBC93DADB85CA4C1597 (void);
// 0x0000027C System.Boolean Mono.Btls.MonoBtlsX509/BoringX509Handle::ReleaseHandle()
extern void BoringX509Handle_ReleaseHandle_m44A35C0FD8E3302AFD84F0276BDD7566C9708A33 (void);
// 0x0000027D System.IntPtr Mono.Btls.MonoBtlsX509/BoringX509Handle::StealHandle()
extern void BoringX509Handle_StealHandle_m096A11B8EC57A9AF11BA983B31E152F4A70E45C7 (void);
// 0x0000027E Mono.Btls.MonoBtlsX509Chain/BoringX509ChainHandle Mono.Btls.MonoBtlsX509Chain::get_Handle()
extern void MonoBtlsX509Chain_get_Handle_m4E499FE6819CFFF133A5BA29C8D5EBD81430A225 (void);
// 0x0000027F System.IntPtr Mono.Btls.MonoBtlsX509Chain::mono_btls_x509_chain_new()
extern void MonoBtlsX509Chain_mono_btls_x509_chain_new_mEC13DFD06CB21A809B1B94BF578E74890050A1C1 (void);
// 0x00000280 System.Int32 Mono.Btls.MonoBtlsX509Chain::mono_btls_x509_chain_get_count(System.IntPtr)
extern void MonoBtlsX509Chain_mono_btls_x509_chain_get_count_m612614A1FA864F9329D7F6307E0291B9332A8314 (void);
// 0x00000281 System.IntPtr Mono.Btls.MonoBtlsX509Chain::mono_btls_x509_chain_get_cert(System.IntPtr,System.Int32)
extern void MonoBtlsX509Chain_mono_btls_x509_chain_get_cert_m35035B0600C9CB527F96969A351A0A4350D078DA (void);
// 0x00000282 System.Int32 Mono.Btls.MonoBtlsX509Chain::mono_btls_x509_chain_add_cert(System.IntPtr,System.IntPtr)
extern void MonoBtlsX509Chain_mono_btls_x509_chain_add_cert_mE706CEB043F75C9E1764D4105EB059C5748756AA (void);
// 0x00000283 System.IntPtr Mono.Btls.MonoBtlsX509Chain::mono_btls_x509_chain_up_ref(System.IntPtr)
extern void MonoBtlsX509Chain_mono_btls_x509_chain_up_ref_m24821A143013BA4DE2B09C5214A19B441D104723 (void);
// 0x00000284 System.Void Mono.Btls.MonoBtlsX509Chain::mono_btls_x509_chain_free(System.IntPtr)
extern void MonoBtlsX509Chain_mono_btls_x509_chain_free_m2C99947F6A69FC697E3DBCB02DB632128A4E092E (void);
// 0x00000285 System.Void Mono.Btls.MonoBtlsX509Chain::.ctor()
extern void MonoBtlsX509Chain__ctor_m68C61C0629C37B9A65BABBCDBCBA69707487E0D8 (void);
// 0x00000286 System.Void Mono.Btls.MonoBtlsX509Chain::.ctor(Mono.Btls.MonoBtlsX509Chain/BoringX509ChainHandle)
extern void MonoBtlsX509Chain__ctor_m75184464AF4022BCD4D85FD3FF11C1F5B88791CE (void);
// 0x00000287 System.Int32 Mono.Btls.MonoBtlsX509Chain::get_Count()
extern void MonoBtlsX509Chain_get_Count_mAF59CA42465F37BC94EB371C25430B0044C01ED0 (void);
// 0x00000288 Mono.Btls.MonoBtlsX509 Mono.Btls.MonoBtlsX509Chain::GetCertificate(System.Int32)
extern void MonoBtlsX509Chain_GetCertificate_m6432C0F179B74CCC54D9965C406491287DFC9036 (void);
// 0x00000289 System.Void Mono.Btls.MonoBtlsX509Chain::AddCertificate(Mono.Btls.MonoBtlsX509)
extern void MonoBtlsX509Chain_AddCertificate_mF7444AA20B0EA6C6E697DEC2316ACC9BD69CB518 (void);
// 0x0000028A Mono.Btls.MonoBtlsX509Chain Mono.Btls.MonoBtlsX509Chain::Copy()
extern void MonoBtlsX509Chain_Copy_m91814D7F915489E580CA83905085ADC1BA386E9B (void);
// 0x0000028B System.Void Mono.Btls.MonoBtlsX509Chain/BoringX509ChainHandle::.ctor(System.IntPtr)
extern void BoringX509ChainHandle__ctor_m5A09F89FBAB71A1FBC8731056BF9DF188B5EA1C3 (void);
// 0x0000028C System.Boolean Mono.Btls.MonoBtlsX509Chain/BoringX509ChainHandle::ReleaseHandle()
extern void BoringX509ChainHandle_ReleaseHandle_m6F1169066011A81E31BE0F33DD22F2174420715A (void);
// 0x0000028D Mono.Btls.MonoBtlsX509Lookup/BoringX509LookupHandle Mono.Btls.MonoBtlsX509Lookup::get_Handle()
extern void MonoBtlsX509Lookup_get_Handle_m919AF42E0D90C34EB73A118066CA6E101E50FB31 (void);
// 0x0000028E System.IntPtr Mono.Btls.MonoBtlsX509Lookup::mono_btls_x509_lookup_new(System.IntPtr,Mono.Btls.MonoBtlsX509LookupType)
extern void MonoBtlsX509Lookup_mono_btls_x509_lookup_new_m9950505CB930A051CA8C4F8C95E7253FAC17479C (void);
// 0x0000028F System.Int32 Mono.Btls.MonoBtlsX509Lookup::mono_btls_x509_lookup_add_dir(System.IntPtr,System.IntPtr,Mono.Btls.MonoBtlsX509FileType)
extern void MonoBtlsX509Lookup_mono_btls_x509_lookup_add_dir_m8EA72A40759E7762D0425FA77106F1AEC235D552 (void);
// 0x00000290 System.Int32 Mono.Btls.MonoBtlsX509Lookup::mono_btls_x509_lookup_add_mono(System.IntPtr,System.IntPtr)
extern void MonoBtlsX509Lookup_mono_btls_x509_lookup_add_mono_m2E25E7B16639FA61D02302C122AD05D0A408CAD4 (void);
// 0x00000291 System.Void Mono.Btls.MonoBtlsX509Lookup::mono_btls_x509_lookup_free(System.IntPtr)
extern void MonoBtlsX509Lookup_mono_btls_x509_lookup_free_m53C7B263B1EB29CAFAF262D22956331A92085B67 (void);
// 0x00000292 System.IntPtr Mono.Btls.MonoBtlsX509Lookup::mono_btls_x509_lookup_peek_lookup(System.IntPtr)
extern void MonoBtlsX509Lookup_mono_btls_x509_lookup_peek_lookup_m0E1B1395362841F47F3C1906A055961E4583DCE4 (void);
// 0x00000293 Mono.Btls.MonoBtlsX509Lookup/BoringX509LookupHandle Mono.Btls.MonoBtlsX509Lookup::Create_internal(Mono.Btls.MonoBtlsX509Store,Mono.Btls.MonoBtlsX509LookupType)
extern void MonoBtlsX509Lookup_Create_internal_m8B83B63221A5E987780682B7E36DBC2E55A4F866 (void);
// 0x00000294 System.Void Mono.Btls.MonoBtlsX509Lookup::.ctor(Mono.Btls.MonoBtlsX509Store,Mono.Btls.MonoBtlsX509LookupType)
extern void MonoBtlsX509Lookup__ctor_mAA8D65D64981E5E421EEDE651D7089AE5BE891A2 (void);
// 0x00000295 System.IntPtr Mono.Btls.MonoBtlsX509Lookup::GetNativeLookup()
extern void MonoBtlsX509Lookup_GetNativeLookup_m226D773FE350EA76156B6817E867F11DE5EC7F79 (void);
// 0x00000296 System.Void Mono.Btls.MonoBtlsX509Lookup::AddDirectory(System.String,Mono.Btls.MonoBtlsX509FileType)
extern void MonoBtlsX509Lookup_AddDirectory_mD3C4A2781FFAFCDDE7A5954EA18E6255D0327916 (void);
// 0x00000297 System.Void Mono.Btls.MonoBtlsX509Lookup::AddMono(Mono.Btls.MonoBtlsX509LookupMono)
extern void MonoBtlsX509Lookup_AddMono_m225B3DD90B16F4C7224057DBF7F71247B0718967 (void);
// 0x00000298 System.Void Mono.Btls.MonoBtlsX509Lookup::AddCertificate(Mono.Btls.MonoBtlsX509)
extern void MonoBtlsX509Lookup_AddCertificate_m032B8F408E8C48D5A77E3F55FDA3EF0F13F20941 (void);
// 0x00000299 System.Void Mono.Btls.MonoBtlsX509Lookup::Close()
extern void MonoBtlsX509Lookup_Close_mF6F5AFB74D46E995DDC9F5DED9D8C40D680FCA31 (void);
// 0x0000029A System.Void Mono.Btls.MonoBtlsX509Lookup/BoringX509LookupHandle::.ctor(System.IntPtr)
extern void BoringX509LookupHandle__ctor_mC3377C6FDC82C379081CC89A03D3E66DBC28760E (void);
// 0x0000029B System.Boolean Mono.Btls.MonoBtlsX509Lookup/BoringX509LookupHandle::ReleaseHandle()
extern void BoringX509LookupHandle_ReleaseHandle_mA8F8F14635553D481B3581699FB1DFF5A60C43AD (void);
// 0x0000029C Mono.Btls.MonoBtlsX509LookupMono/BoringX509LookupMonoHandle Mono.Btls.MonoBtlsX509LookupMono::get_Handle()
extern void MonoBtlsX509LookupMono_get_Handle_mD34446E542FA06D76191F4A5758C319B08186001 (void);
// 0x0000029D System.IntPtr Mono.Btls.MonoBtlsX509LookupMono::mono_btls_x509_lookup_mono_new()
extern void MonoBtlsX509LookupMono_mono_btls_x509_lookup_mono_new_m93F177FB5C5229037B85E61CA05A78F9FB2ABF20 (void);
// 0x0000029E System.Void Mono.Btls.MonoBtlsX509LookupMono::mono_btls_x509_lookup_mono_init(System.IntPtr,System.IntPtr,System.IntPtr)
extern void MonoBtlsX509LookupMono_mono_btls_x509_lookup_mono_init_m53607769F147658FA2E4DD7729E52EBC01E6DB21 (void);
// 0x0000029F System.Int32 Mono.Btls.MonoBtlsX509LookupMono::mono_btls_x509_lookup_mono_free(System.IntPtr)
extern void MonoBtlsX509LookupMono_mono_btls_x509_lookup_mono_free_m9BAE11DA94E995D2C61ECEBCB49AE66E668CA495 (void);
// 0x000002A0 System.Void Mono.Btls.MonoBtlsX509LookupMono::.ctor()
extern void MonoBtlsX509LookupMono__ctor_m833FBC2503ABDCE001FC83F9BF5C03F3B8B51064 (void);
// 0x000002A1 System.Void Mono.Btls.MonoBtlsX509LookupMono::Install(Mono.Btls.MonoBtlsX509Lookup)
extern void MonoBtlsX509LookupMono_Install_m06ED46E5FD3761842A9B7750D5693003155F7016 (void);
// 0x000002A2 System.Void Mono.Btls.MonoBtlsX509LookupMono::AddCertificate(Mono.Btls.MonoBtlsX509)
extern void MonoBtlsX509LookupMono_AddCertificate_m2216A9F856CD64B208A17F228F1CDD39971FABC2 (void);
// 0x000002A3 Mono.Btls.MonoBtlsX509 Mono.Btls.MonoBtlsX509LookupMono::OnGetBySubject(Mono.Btls.MonoBtlsX509Name)
// 0x000002A4 System.Int32 Mono.Btls.MonoBtlsX509LookupMono::OnGetBySubject(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void MonoBtlsX509LookupMono_OnGetBySubject_m15FE6FBEE9849C324E8E099172A581EABEB83FB3 (void);
// 0x000002A5 System.Void Mono.Btls.MonoBtlsX509LookupMono::Close()
extern void MonoBtlsX509LookupMono_Close_mF6E1290CAC54656E312429B47DE52D1107E830F0 (void);
// 0x000002A6 System.Void Mono.Btls.MonoBtlsX509LookupMono/BoringX509LookupMonoHandle::.ctor(System.IntPtr)
extern void BoringX509LookupMonoHandle__ctor_mC6AC306800774EB0EEBE877AFF028335B3B8EAAE (void);
// 0x000002A7 System.Boolean Mono.Btls.MonoBtlsX509LookupMono/BoringX509LookupMonoHandle::ReleaseHandle()
extern void BoringX509LookupMonoHandle_ReleaseHandle_m3167A5697F5E0975C6CCE2F8F18D96DC91CE65BD (void);
// 0x000002A8 System.Void Mono.Btls.MonoBtlsX509LookupMono/BySubjectFunc::.ctor(System.Object,System.IntPtr)
extern void BySubjectFunc__ctor_m716B9DBA0E032168201B3F2604C5AD2625F10561 (void);
// 0x000002A9 System.Int32 Mono.Btls.MonoBtlsX509LookupMono/BySubjectFunc::Invoke(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void BySubjectFunc_Invoke_m27F18158D124F9C4EEAE02223A4B2372B3301B96 (void);
// 0x000002AA System.Void Mono.Btls.MonoBtlsX509LookupMonoCollection::.ctor(System.Security.Cryptography.X509Certificates.X509CertificateCollection,Mono.Btls.MonoBtlsX509TrustKind)
extern void MonoBtlsX509LookupMonoCollection__ctor_m997D5DCDBC344D684111875A5F99D4FD5991182C (void);
// 0x000002AB System.Void Mono.Btls.MonoBtlsX509LookupMonoCollection::Initialize()
extern void MonoBtlsX509LookupMonoCollection_Initialize_mB847A317C0B5C871C79126EBE63E1B7174325C38 (void);
// 0x000002AC Mono.Btls.MonoBtlsX509 Mono.Btls.MonoBtlsX509LookupMonoCollection::OnGetBySubject(Mono.Btls.MonoBtlsX509Name)
extern void MonoBtlsX509LookupMonoCollection_OnGetBySubject_m119F2F82E11008A9688EA9ACE83A64FA6FD57067 (void);
// 0x000002AD System.Void Mono.Btls.MonoBtlsX509LookupMonoCollection::Close()
extern void MonoBtlsX509LookupMonoCollection_Close_m131578243929C528FD4E660CAF442AFF47AF3706 (void);
// 0x000002AE System.Int64 Mono.Btls.MonoBtlsX509Name::mono_btls_x509_name_hash(System.IntPtr)
extern void MonoBtlsX509Name_mono_btls_x509_name_hash_mAA7A940CB0BF150FB73225F53EBA9D427E22A155 (void);
// 0x000002AF System.Int32 Mono.Btls.MonoBtlsX509Name::mono_btls_x509_name_get_entry_count(System.IntPtr)
extern void MonoBtlsX509Name_mono_btls_x509_name_get_entry_count_mB15EEB87BD5FA234A4FA843C0AC4170E97726026 (void);
// 0x000002B0 Mono.Btls.MonoBtlsX509NameEntryType Mono.Btls.MonoBtlsX509Name::mono_btls_x509_name_get_entry_type(System.IntPtr,System.Int32)
extern void MonoBtlsX509Name_mono_btls_x509_name_get_entry_type_mD7DFF5C6D95DF744939AEB9BF2F1B4C2BDE37F58 (void);
// 0x000002B1 System.Int32 Mono.Btls.MonoBtlsX509Name::mono_btls_x509_name_get_entry_oid(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
extern void MonoBtlsX509Name_mono_btls_x509_name_get_entry_oid_mBD127D617FEB7CB420D9216A2A99154DA0C47270 (void);
// 0x000002B2 System.Int32 Mono.Btls.MonoBtlsX509Name::mono_btls_x509_name_get_entry_oid_data(System.IntPtr,System.Int32,System.IntPtr&)
extern void MonoBtlsX509Name_mono_btls_x509_name_get_entry_oid_data_m85F7C59ACC9FE93F146C628D838288799A7AE1C7 (void);
// 0x000002B3 System.Int32 Mono.Btls.MonoBtlsX509Name::mono_btls_x509_name_get_entry_value(System.IntPtr,System.Int32,System.Int32&,System.IntPtr&)
extern void MonoBtlsX509Name_mono_btls_x509_name_get_entry_value_m7DA2D337402CF9E6FC97B31C7861483324971D41 (void);
// 0x000002B4 System.IntPtr Mono.Btls.MonoBtlsX509Name::mono_btls_x509_name_from_data(System.Void*,System.Int32,System.Int32)
extern void MonoBtlsX509Name_mono_btls_x509_name_from_data_m6B0A715456411D4727EAFF338CBB74400925490E (void);
// 0x000002B5 System.Void Mono.Btls.MonoBtlsX509Name::mono_btls_x509_name_free(System.IntPtr)
extern void MonoBtlsX509Name_mono_btls_x509_name_free_m4D32CE033146B665264781920D133F289BB8A52D (void);
// 0x000002B6 Mono.Btls.MonoBtlsX509Name/BoringX509NameHandle Mono.Btls.MonoBtlsX509Name::get_Handle()
extern void MonoBtlsX509Name_get_Handle_m6F6F858ABCFF67031A986F782E2945FDDEEA89E8 (void);
// 0x000002B7 System.Void Mono.Btls.MonoBtlsX509Name::.ctor(Mono.Btls.MonoBtlsX509Name/BoringX509NameHandle)
extern void MonoBtlsX509Name__ctor_mE79B982F5B1CD10752351373FBE3CB981FC84629 (void);
// 0x000002B8 System.Int64 Mono.Btls.MonoBtlsX509Name::GetHash()
extern void MonoBtlsX509Name_GetHash_m951517C1A1EBC07ED7213E1EE1293D977A36163E (void);
// 0x000002B9 System.Int32 Mono.Btls.MonoBtlsX509Name::GetEntryCount()
extern void MonoBtlsX509Name_GetEntryCount_mC03F2E7C9DB042167E9AB04AEDB298B4F5E20C62 (void);
// 0x000002BA Mono.Btls.MonoBtlsX509NameEntryType Mono.Btls.MonoBtlsX509Name::GetEntryType(System.Int32)
extern void MonoBtlsX509Name_GetEntryType_mAECDAFFD3F52321C02ED185056C0B42FC62835BC (void);
// 0x000002BB System.String Mono.Btls.MonoBtlsX509Name::GetEntryOid(System.Int32)
extern void MonoBtlsX509Name_GetEntryOid_mFB2561A2D361280DB134B7C1C426FECB541061AA (void);
// 0x000002BC System.Byte[] Mono.Btls.MonoBtlsX509Name::GetEntryOidData(System.Int32)
extern void MonoBtlsX509Name_GetEntryOidData_m7B4A22FC80FC24A0FCA68BAE76B2818B48EC8303 (void);
// 0x000002BD System.String Mono.Btls.MonoBtlsX509Name::GetEntryValue(System.Int32,System.Int32&)
extern void MonoBtlsX509Name_GetEntryValue_m9048C928E51BD319F44E55953584BB4A1403C8FF (void);
// 0x000002BE Mono.Btls.MonoBtlsX509Name Mono.Btls.MonoBtlsX509Name::CreateFromData(System.Byte[],System.Boolean)
extern void MonoBtlsX509Name_CreateFromData_mF6906700D30B086DEE6D9A25ADFCFA1ECBA58D64 (void);
// 0x000002BF System.Void Mono.Btls.MonoBtlsX509Name/BoringX509NameHandle::.ctor(System.IntPtr,System.Boolean)
extern void BoringX509NameHandle__ctor_m47C6D29BC27537D2A061E1EB231416E79AF08F6C (void);
// 0x000002C0 System.Boolean Mono.Btls.MonoBtlsX509Name/BoringX509NameHandle::ReleaseHandle()
extern void BoringX509NameHandle_ReleaseHandle_mD2170A02B59FFD9CE229236533ED39CF6A5D282D (void);
// 0x000002C1 Mono.Btls.MonoBtlsX509Store/BoringX509StoreHandle Mono.Btls.MonoBtlsX509Store::get_Handle()
extern void MonoBtlsX509Store_get_Handle_m89AE801D5B269E2E046407E0635740F404FABFE8 (void);
// 0x000002C2 System.IntPtr Mono.Btls.MonoBtlsX509Store::mono_btls_x509_store_new()
extern void MonoBtlsX509Store_mono_btls_x509_store_new_mF66BDE64C8126E1A047CD1A674D76740FD13FF1B (void);
// 0x000002C3 System.IntPtr Mono.Btls.MonoBtlsX509Store::mono_btls_x509_store_from_ssl_ctx(System.IntPtr)
extern void MonoBtlsX509Store_mono_btls_x509_store_from_ssl_ctx_m181AD147AC11B5F5805A36E58A4008AE4AAA5864 (void);
// 0x000002C4 System.Int32 Mono.Btls.MonoBtlsX509Store::mono_btls_x509_store_add_cert(System.IntPtr,System.IntPtr)
extern void MonoBtlsX509Store_mono_btls_x509_store_add_cert_mF7B1C07406B8793DBF5E6D450CBB9A9D7BAA755C (void);
// 0x000002C5 System.Void Mono.Btls.MonoBtlsX509Store::mono_btls_x509_store_free(System.IntPtr)
extern void MonoBtlsX509Store_mono_btls_x509_store_free_mD78608C3A44172D4A17295D6C5D74A5A673B55B7 (void);
// 0x000002C6 Mono.Btls.MonoBtlsX509Store/BoringX509StoreHandle Mono.Btls.MonoBtlsX509Store::Create_internal()
extern void MonoBtlsX509Store_Create_internal_m900BB0AE58DB23F957753C74B489997C7B8A89E5 (void);
// 0x000002C7 Mono.Btls.MonoBtlsX509Store/BoringX509StoreHandle Mono.Btls.MonoBtlsX509Store::Create_internal(Mono.Btls.MonoBtlsSslCtx/BoringSslCtxHandle)
extern void MonoBtlsX509Store_Create_internal_mEC0FA7ED8C7DA3E3D167AD510AA4EAC7B8CA9068 (void);
// 0x000002C8 System.Void Mono.Btls.MonoBtlsX509Store::.ctor()
extern void MonoBtlsX509Store__ctor_m848B0907C7C945C9AC29E2DC67BB49D4AF29122D (void);
// 0x000002C9 System.Void Mono.Btls.MonoBtlsX509Store::.ctor(Mono.Btls.MonoBtlsSslCtx/BoringSslCtxHandle)
extern void MonoBtlsX509Store__ctor_mDCA08A7A262CE8FD4CB0C275A147FFB9CE67C82F (void);
// 0x000002CA System.Void Mono.Btls.MonoBtlsX509Store::AddCertificate(Mono.Btls.MonoBtlsX509)
extern void MonoBtlsX509Store_AddCertificate_mC8C028325E40B61A290CF9ED06E1624B78BB139B (void);
// 0x000002CB Mono.Btls.MonoBtlsX509Lookup Mono.Btls.MonoBtlsX509Store::AddLookup(Mono.Btls.MonoBtlsX509LookupType)
extern void MonoBtlsX509Store_AddLookup_m9C6FA4053BF3D69EB22FE35A30C7F5FB182CCD24 (void);
// 0x000002CC System.Void Mono.Btls.MonoBtlsX509Store::AddDirectoryLookup(System.String,Mono.Btls.MonoBtlsX509FileType)
extern void MonoBtlsX509Store_AddDirectoryLookup_m1CB4E7CA7977A369F224404306002170BF2CA3E1 (void);
// 0x000002CD System.Void Mono.Btls.MonoBtlsX509Store::AddCollection(System.Security.Cryptography.X509Certificates.X509CertificateCollection,Mono.Btls.MonoBtlsX509TrustKind)
extern void MonoBtlsX509Store_AddCollection_m7E48F9B4620415231973398B2C5CB6860F1D13B1 (void);
// 0x000002CE System.Void Mono.Btls.MonoBtlsX509Store::Close()
extern void MonoBtlsX509Store_Close_mFC090B8A071362ECED1DB24D3471824C0B018402 (void);
// 0x000002CF System.Void Mono.Btls.MonoBtlsX509Store/BoringX509StoreHandle::.ctor(System.IntPtr)
extern void BoringX509StoreHandle__ctor_mB3C1BCAB5FF6B1292D07D5E04B32C0B176CA1E40 (void);
// 0x000002D0 System.Boolean Mono.Btls.MonoBtlsX509Store/BoringX509StoreHandle::ReleaseHandle()
extern void BoringX509StoreHandle_ReleaseHandle_mB2A57AB29100FC7A72B074E9FCC069E1ECB2A59C (void);
// 0x000002D1 Mono.Btls.MonoBtlsX509StoreCtx/BoringX509StoreCtxHandle Mono.Btls.MonoBtlsX509StoreCtx::get_Handle()
extern void MonoBtlsX509StoreCtx_get_Handle_m2E8C152DA7370B3EAA9CB2126ECA5CF2B30124C2 (void);
// 0x000002D2 System.IntPtr Mono.Btls.MonoBtlsX509StoreCtx::mono_btls_x509_store_ctx_new()
extern void MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_new_m148D71307C5C2F68C623D0865BF6604169DFE608 (void);
// 0x000002D3 System.IntPtr Mono.Btls.MonoBtlsX509StoreCtx::mono_btls_x509_store_ctx_from_ptr(System.IntPtr)
extern void MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_from_ptr_m0E4A355742DC4F59FE6ED1B5D49D65D3FAE9726E (void);
// 0x000002D4 Mono.Btls.MonoBtlsX509Error Mono.Btls.MonoBtlsX509StoreCtx::mono_btls_x509_store_ctx_get_error(System.IntPtr,System.IntPtr&)
extern void MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_get_error_mBA2BD5503DDCF594F5C1BBB195615B8A32363B64 (void);
// 0x000002D5 System.IntPtr Mono.Btls.MonoBtlsX509StoreCtx::mono_btls_x509_store_ctx_get_chain(System.IntPtr)
extern void MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_get_chain_m09FE29F1E29A7FAA0F1A020991ACF5CEA1577F8C (void);
// 0x000002D6 System.Int32 Mono.Btls.MonoBtlsX509StoreCtx::mono_btls_x509_store_ctx_init(System.IntPtr,System.IntPtr,System.IntPtr)
extern void MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_init_m21FC604D97D34897243452A3F5C4DC696195CB3E (void);
// 0x000002D7 System.Int32 Mono.Btls.MonoBtlsX509StoreCtx::mono_btls_x509_store_ctx_set_param(System.IntPtr,System.IntPtr)
extern void MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_set_param_m0CE4AF656DA1350EB4BE3BCA238B3D7738D6C65F (void);
// 0x000002D8 System.Int32 Mono.Btls.MonoBtlsX509StoreCtx::mono_btls_x509_store_ctx_verify_cert(System.IntPtr)
extern void MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_verify_cert_mF847C34B563D76E11C4281528F90D940C70025EC (void);
// 0x000002D9 System.IntPtr Mono.Btls.MonoBtlsX509StoreCtx::mono_btls_x509_store_ctx_get_untrusted(System.IntPtr)
extern void MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_get_untrusted_m90312DE8BB483B0DF0F4BDCA8126C7733832BF4A (void);
// 0x000002DA System.IntPtr Mono.Btls.MonoBtlsX509StoreCtx::mono_btls_x509_store_ctx_up_ref(System.IntPtr)
extern void MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_up_ref_m1EF1553C379B5B37305016D6A9D20CFB6743986A (void);
// 0x000002DB System.Void Mono.Btls.MonoBtlsX509StoreCtx::mono_btls_x509_store_ctx_free(System.IntPtr)
extern void MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_free_mF805084F9C02FD101E3C92384556E877D797B6A2 (void);
// 0x000002DC System.Void Mono.Btls.MonoBtlsX509StoreCtx::.ctor()
extern void MonoBtlsX509StoreCtx__ctor_m546652793A7C67EDD01661ECF54FD0ED3B50B75B (void);
// 0x000002DD Mono.Btls.MonoBtlsX509StoreCtx/BoringX509StoreCtxHandle Mono.Btls.MonoBtlsX509StoreCtx::Create_internal(System.IntPtr)
extern void MonoBtlsX509StoreCtx_Create_internal_m0AF6A7EFECDD27A17CFA824DAC752038D242FFB8 (void);
// 0x000002DE System.Void Mono.Btls.MonoBtlsX509StoreCtx::.ctor(System.Int32,System.IntPtr)
extern void MonoBtlsX509StoreCtx__ctor_m48FB15F8DD6681E5271FB29421DEAA89A1B6F196 (void);
// 0x000002DF System.Void Mono.Btls.MonoBtlsX509StoreCtx::.ctor(Mono.Btls.MonoBtlsX509StoreCtx/BoringX509StoreCtxHandle,System.Nullable`1<System.Int32>)
extern void MonoBtlsX509StoreCtx__ctor_mFD0079D6C6BD47357CD3E19BD121FD336AC17BFD (void);
// 0x000002E0 Mono.Btls.MonoBtlsX509Error Mono.Btls.MonoBtlsX509StoreCtx::GetError()
extern void MonoBtlsX509StoreCtx_GetError_m0BDF8083E134E7F9B3DBB8D862B2422F36133D46 (void);
// 0x000002E1 Mono.Btls.MonoBtlsX509Chain Mono.Btls.MonoBtlsX509StoreCtx::GetChain()
extern void MonoBtlsX509StoreCtx_GetChain_m589378381689FF4870F149D405B03F6FBB8EEA4B (void);
// 0x000002E2 Mono.Btls.MonoBtlsX509Chain Mono.Btls.MonoBtlsX509StoreCtx::GetUntrusted()
extern void MonoBtlsX509StoreCtx_GetUntrusted_m97D215DD2183B70E764FC20D9A3EDFC1329F5B97 (void);
// 0x000002E3 System.Void Mono.Btls.MonoBtlsX509StoreCtx::Initialize(Mono.Btls.MonoBtlsX509Store,Mono.Btls.MonoBtlsX509Chain)
extern void MonoBtlsX509StoreCtx_Initialize_mC15C81CDFF21089D13BE8383602CCFE10075D093 (void);
// 0x000002E4 System.Void Mono.Btls.MonoBtlsX509StoreCtx::SetVerifyParam(Mono.Btls.MonoBtlsX509VerifyParam)
extern void MonoBtlsX509StoreCtx_SetVerifyParam_mD1F431EC644C7400B85CB25323D8A63AAD195BFE (void);
// 0x000002E5 System.Int32 Mono.Btls.MonoBtlsX509StoreCtx::get_VerifyResult()
extern void MonoBtlsX509StoreCtx_get_VerifyResult_m46002D2AE1A138E54EBE5D5C002D8F58E690B732 (void);
// 0x000002E6 System.Int32 Mono.Btls.MonoBtlsX509StoreCtx::Verify()
extern void MonoBtlsX509StoreCtx_Verify_mB035E88C6FD9D5A45B28A9494A4EAB860136725B (void);
// 0x000002E7 Mono.Btls.MonoBtlsX509StoreCtx Mono.Btls.MonoBtlsX509StoreCtx::Copy()
extern void MonoBtlsX509StoreCtx_Copy_mEC93A3593445A8306AFCAE0864DF39682684407B (void);
// 0x000002E8 System.Void Mono.Btls.MonoBtlsX509StoreCtx/BoringX509StoreCtxHandle::.ctor(System.IntPtr,System.Boolean)
extern void BoringX509StoreCtxHandle__ctor_mB110C57F474AE661F51673C31DE8C452AB565778 (void);
// 0x000002E9 System.Boolean Mono.Btls.MonoBtlsX509StoreCtx/BoringX509StoreCtxHandle::ReleaseHandle()
extern void BoringX509StoreCtxHandle_ReleaseHandle_m387B2DFF84D204300011659B9F77E62CB1215C3E (void);
// 0x000002EA System.Void Mono.Btls.MonoBtlsX509StoreManager::Initialize()
extern void MonoBtlsX509StoreManager_Initialize_m8219904DEF9F837D89908552B7A1E4861E7DBF72 (void);
// 0x000002EB System.Void Mono.Btls.MonoBtlsX509StoreManager::DoInitialize()
extern void MonoBtlsX509StoreManager_DoInitialize_m1E82BF3BADFA373C88A766912843EE7F1D96B6A5 (void);
// 0x000002EC System.String Mono.Btls.MonoBtlsX509StoreManager::GetStorePath(Mono.Btls.MonoBtlsX509StoreType)
extern void MonoBtlsX509StoreManager_GetStorePath_mC53DDD09FADBA1DC7A382D97EA648C6B69E12635 (void);
// 0x000002ED Mono.Btls.MonoBtlsX509VerifyParam/BoringX509VerifyParamHandle Mono.Btls.MonoBtlsX509VerifyParam::get_Handle()
extern void MonoBtlsX509VerifyParam_get_Handle_m484BD6CF89C2F173000B8E2FC4442B13EEEC481B (void);
// 0x000002EE System.IntPtr Mono.Btls.MonoBtlsX509VerifyParam::mono_btls_x509_verify_param_copy(System.IntPtr)
extern void MonoBtlsX509VerifyParam_mono_btls_x509_verify_param_copy_mF05B4613E5AA97EF7506064B95AD47B8DB5BB953 (void);
// 0x000002EF System.IntPtr Mono.Btls.MonoBtlsX509VerifyParam::mono_btls_x509_verify_param_lookup(System.IntPtr)
extern void MonoBtlsX509VerifyParam_mono_btls_x509_verify_param_lookup_m914360F88B58BE56E7DDD807D705339131C9E296 (void);
// 0x000002F0 System.Int32 Mono.Btls.MonoBtlsX509VerifyParam::mono_btls_x509_verify_param_can_modify(System.IntPtr)
extern void MonoBtlsX509VerifyParam_mono_btls_x509_verify_param_can_modify_m857577AAE12C4CC171CC2EF3F720F6FA5CA9A2FD (void);
// 0x000002F1 System.Int32 Mono.Btls.MonoBtlsX509VerifyParam::mono_btls_x509_verify_param_set_host(System.IntPtr,System.IntPtr,System.Int32)
extern void MonoBtlsX509VerifyParam_mono_btls_x509_verify_param_set_host_m0F7B4789B71C7B327F7D703F50BF6C7460E147A2 (void);
// 0x000002F2 System.Int32 Mono.Btls.MonoBtlsX509VerifyParam::mono_btls_x509_verify_param_set_time(System.IntPtr,System.Int64)
extern void MonoBtlsX509VerifyParam_mono_btls_x509_verify_param_set_time_m9C770883C0CB7DCFB55DC96B9A163A02F36528F4 (void);
// 0x000002F3 System.Void Mono.Btls.MonoBtlsX509VerifyParam::mono_btls_x509_verify_param_free(System.IntPtr)
extern void MonoBtlsX509VerifyParam_mono_btls_x509_verify_param_free_m1A6DEE2E9AFCFE8A0A4E661F35BADF8A658C10B3 (void);
// 0x000002F4 System.Void Mono.Btls.MonoBtlsX509VerifyParam::.ctor(Mono.Btls.MonoBtlsX509VerifyParam/BoringX509VerifyParamHandle)
extern void MonoBtlsX509VerifyParam__ctor_m4211A69035B76E9BCDA406F56407CB6F7B2F64FC (void);
// 0x000002F5 Mono.Btls.MonoBtlsX509VerifyParam Mono.Btls.MonoBtlsX509VerifyParam::Copy()
extern void MonoBtlsX509VerifyParam_Copy_m8B543C8389E6D5D6DFB6AB4E1D1D91AA76D6876B (void);
// 0x000002F6 Mono.Btls.MonoBtlsX509VerifyParam Mono.Btls.MonoBtlsX509VerifyParam::GetSslClient()
extern void MonoBtlsX509VerifyParam_GetSslClient_mF8B59DC573161A26D2F9057A2001DD49E55168BC (void);
// 0x000002F7 Mono.Btls.MonoBtlsX509VerifyParam Mono.Btls.MonoBtlsX509VerifyParam::GetSslServer()
extern void MonoBtlsX509VerifyParam_GetSslServer_mA2B54BD4497A2E7734930BB4874E23F5F70DCE8D (void);
// 0x000002F8 Mono.Btls.MonoBtlsX509VerifyParam Mono.Btls.MonoBtlsX509VerifyParam::Lookup(System.String,System.Boolean)
extern void MonoBtlsX509VerifyParam_Lookup_mFC83C380CCDE6B1358C61F092E8239B7CA2AD793 (void);
// 0x000002F9 System.Boolean Mono.Btls.MonoBtlsX509VerifyParam::get_CanModify()
extern void MonoBtlsX509VerifyParam_get_CanModify_m2B36671A3BFD3F534F2587C90C36C97449E6E8D9 (void);
// 0x000002FA System.Void Mono.Btls.MonoBtlsX509VerifyParam::WantToModify()
extern void MonoBtlsX509VerifyParam_WantToModify_m339A1A65C4A81F5C5D33B17C7C0AC40386A1692F (void);
// 0x000002FB System.Void Mono.Btls.MonoBtlsX509VerifyParam::SetHost(System.String)
extern void MonoBtlsX509VerifyParam_SetHost_m8FFFFEFD9A58FF78B8A02DA6BF41FD83496EF883 (void);
// 0x000002FC System.Void Mono.Btls.MonoBtlsX509VerifyParam::SetTime(System.DateTime)
extern void MonoBtlsX509VerifyParam_SetTime_m7CC7C7BB09B44E22991F3D1DC7945616B66C7C36 (void);
// 0x000002FD System.Void Mono.Btls.MonoBtlsX509VerifyParam/BoringX509VerifyParamHandle::.ctor(System.IntPtr)
extern void BoringX509VerifyParamHandle__ctor_mC7D9DD61778AE4EFE7B63B271F758122B9CD65E9 (void);
// 0x000002FE System.Boolean Mono.Btls.MonoBtlsX509VerifyParam/BoringX509VerifyParamHandle::ReleaseHandle()
extern void BoringX509VerifyParamHandle_ReleaseHandle_m22C5C9F57FA8D3072B3702A738447437FA970B56 (void);
// 0x000002FF System.Void Mono.Btls.X509CertificateImplBtls::.ctor(Mono.Btls.MonoBtlsX509)
extern void X509CertificateImplBtls__ctor_m9B766C510B621586A897818C904AEA16DCEA1622 (void);
// 0x00000300 System.Void Mono.Btls.X509CertificateImplBtls::.ctor(Mono.Btls.X509CertificateImplBtls)
extern void X509CertificateImplBtls__ctor_mBD1BDF7B84B633622363B567E28C52FC0F0BDE65 (void);
// 0x00000301 System.Void Mono.Btls.X509CertificateImplBtls::.ctor(System.Byte[],Mono.Btls.MonoBtlsX509Format)
extern void X509CertificateImplBtls__ctor_mCD39E4EB7778DC3DF261DC7DB368FC8D10161C74 (void);
// 0x00000302 System.Void Mono.Btls.X509CertificateImplBtls::.ctor(System.Byte[],Microsoft.Win32.SafeHandles.SafePasswordHandle,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern void X509CertificateImplBtls__ctor_mF19371D8B0AB5381EF0D30574D126293018CA24D (void);
// 0x00000303 System.Boolean Mono.Btls.X509CertificateImplBtls::get_IsValid()
extern void X509CertificateImplBtls_get_IsValid_mB7EF3B8732696CB911745EC8FD923D251860268B (void);
// 0x00000304 Mono.Btls.MonoBtlsX509 Mono.Btls.X509CertificateImplBtls::get_X509()
extern void X509CertificateImplBtls_get_X509_m96B07D0BA825233DE2E209EDD7970363605F87C7 (void);
// 0x00000305 Mono.Btls.MonoBtlsKey Mono.Btls.X509CertificateImplBtls::get_NativePrivateKey()
extern void X509CertificateImplBtls_get_NativePrivateKey_mD4E6CD38DAE4CBEC361BFFA6A62237453F178E43 (void);
// 0x00000306 System.Security.Cryptography.X509Certificates.X509CertificateImpl Mono.Btls.X509CertificateImplBtls::Clone()
extern void X509CertificateImplBtls_Clone_mA1954F8E9B803C677836D3B3FD8E32FC5D2C0205 (void);
// 0x00000307 System.Byte[] Mono.Btls.X509CertificateImplBtls::GetRawCertData()
extern void X509CertificateImplBtls_GetRawCertData_m0C76E3E3B3E7C1C3DD58F326C4D2DC73669415BE (void);
// 0x00000308 System.Security.Cryptography.X509Certificates.X509CertificateImplCollection Mono.Btls.X509CertificateImplBtls::get_IntermediateCertificates()
extern void X509CertificateImplBtls_get_IntermediateCertificates_mFEBC8F779F0638037FF604AE4B1CF7543B8755A2 (void);
// 0x00000309 System.Void Mono.Btls.X509CertificateImplBtls::Dispose(System.Boolean)
extern void X509CertificateImplBtls_Dispose_m4475832886CC96446D1EE4B314D962B8406980A0 (void);
// 0x0000030A System.Boolean Mono.Btls.X509CertificateImplBtls::get_HasPrivateKey()
extern void X509CertificateImplBtls_get_HasPrivateKey_m33195867F1591B1CCEAB3BF987B81FBA0591C83D (void);
// 0x0000030B System.Security.Cryptography.AsymmetricAlgorithm Mono.Btls.X509CertificateImplBtls::get_PrivateKey()
extern void X509CertificateImplBtls_get_PrivateKey_m59B1F91805D97572E85200AF66E405C6318F6FEF (void);
// 0x0000030C System.Void Mono.Btls.X509CertificateImplBtls::set_PrivateKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern void X509CertificateImplBtls_set_PrivateKey_mD271BE07E8EC167512E651AB7D1142AA2ED21715 (void);
// 0x0000030D System.Security.Cryptography.RSA Mono.Btls.X509CertificateImplBtls::GetRSAPrivateKey()
extern void X509CertificateImplBtls_GetRSAPrivateKey_m73D73BEDF6FD28A6B0041A27ED1286003A626303 (void);
// 0x0000030E System.Security.Cryptography.DSA Mono.Btls.X509CertificateImplBtls::GetDSAPrivateKey()
extern void X509CertificateImplBtls_GetDSAPrivateKey_m23B3F54E2CD14841ABFFC18F39D7C3279819F4FA (void);
// 0x0000030F System.Void Mono.Btls.X509CertificateImplBtls::Import(System.Byte[])
extern void X509CertificateImplBtls_Import_m5C23917D44D1E1BCF37A6957BA8102DC263D7D10 (void);
// 0x00000310 System.Void Mono.Btls.X509CertificateImplBtls::ImportPkcs12(System.Byte[],Microsoft.Win32.SafeHandles.SafePasswordHandle)
extern void X509CertificateImplBtls_ImportPkcs12_m349710DD0FBBFDCC59064187312A21BA4ABC9FA6 (void);
// 0x00000311 System.Void Mono.Btls.X509CertificateImplBtls::ImportAuthenticode(System.Byte[])
extern void X509CertificateImplBtls_ImportAuthenticode_mB6EC779B036A5BF1691B767D6F4BE9B6C9087506 (void);
// 0x00000312 System.Boolean Mono.Btls.X509CertificateImplBtls::Verify(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509CertificateImplBtls_Verify_mE17AABFD574B56D52916106F61BDAFAC98EAB249 (void);
// 0x00000313 System.Void Mono.Btls.X509ChainImplBtls::.ctor(Mono.Btls.MonoBtlsX509Chain)
extern void X509ChainImplBtls__ctor_m9D5552CEA49BDD9BD694E3F3286F46214991EAC9 (void);
// 0x00000314 System.Void Mono.Btls.X509ChainImplBtls::.ctor(Mono.Btls.MonoBtlsX509StoreCtx)
extern void X509ChainImplBtls__ctor_m128255CC7901CDF5BFD9BE0D50087DC98E2F0E45 (void);
// 0x00000315 System.Boolean Mono.Btls.X509ChainImplBtls::get_IsValid()
extern void X509ChainImplBtls_get_IsValid_m32F89F48120D92C96E9708BD898F760FC7362F32 (void);
// 0x00000316 Mono.Btls.MonoBtlsX509StoreCtx Mono.Btls.X509ChainImplBtls::get_StoreCtx()
extern void X509ChainImplBtls_get_StoreCtx_m4B1A68C79457CDA59D832677790DA780DCE6915E (void);
// 0x00000317 System.Security.Cryptography.X509Certificates.X509ChainElementCollection Mono.Btls.X509ChainImplBtls::get_ChainElements()
extern void X509ChainImplBtls_get_ChainElements_mEA430114878721A40F550DB39D35AEF0734D217D (void);
// 0x00000318 System.Security.Cryptography.X509Certificates.X509ChainPolicy Mono.Btls.X509ChainImplBtls::get_ChainPolicy()
extern void X509ChainImplBtls_get_ChainPolicy_mF5DB0339D3EAE7F69964D1B6CA34A513A43B6408 (void);
// 0x00000319 System.Void Mono.Btls.X509ChainImplBtls::AddStatus(System.Security.Cryptography.X509Certificates.X509ChainStatusFlags)
extern void X509ChainImplBtls_AddStatus_mE854AF70220C73FA841B6CC72A68EB99774CB536 (void);
// 0x0000031A System.Boolean Mono.Btls.X509ChainImplBtls::Build(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainImplBtls_Build_m04F5DC966431BF7612DFBACC8A03DDB1B9B15BE5 (void);
// 0x0000031B System.Void Mono.Btls.X509ChainImplBtls::Reset()
extern void X509ChainImplBtls_Reset_m024486185BBFECB79D652EDA88DF16590C25F6A9 (void);
// 0x0000031C System.Void Mono.Btls.X509ChainImplBtls::Dispose(System.Boolean)
extern void X509ChainImplBtls_Dispose_mE4F62C67069BD1F3477E4C0FBB1198E4BDE46364 (void);
// 0x0000031D System.Void Mono.Btls.X509PalImplBtls::.ctor(Mono.Security.Interface.MonoTlsProvider)
extern void X509PalImplBtls__ctor_m3AF8288F8E0AF64AC97C373346A56E63DA8658AD (void);
// 0x0000031E Mono.Btls.MonoBtlsProvider Mono.Btls.X509PalImplBtls::get_Provider()
extern void X509PalImplBtls_get_Provider_mE31DF32B169BC4E69C99349B93A70EF246EB89CD (void);
// 0x0000031F System.Security.Cryptography.X509Certificates.X509CertificateImpl Mono.Btls.X509PalImplBtls::Import(System.Byte[])
extern void X509PalImplBtls_Import_mE864AD400885ABD4939F7813FF41F872B95CD3F5 (void);
// 0x00000320 System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.Btls.X509PalImplBtls::Import(System.Byte[],Microsoft.Win32.SafeHandles.SafePasswordHandle,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern void X509PalImplBtls_Import_m1582D613CDCAE1CB791B811B2E81EB3AEB2E021B (void);
// 0x00000321 System.Security.Cryptography.X509Certificates.X509Certificate2Impl Mono.Btls.X509PalImplBtls::Import(System.Security.Cryptography.X509Certificates.X509Certificate)
extern void X509PalImplBtls_Import_m1EB2C0587BAD69FAAA8FCEF5AC0D4CDC8517B288 (void);
// 0x00000322 System.String Internal.Cryptography.OidLookup::ToFriendlyName(System.String,System.Security.Cryptography.OidGroup,System.Boolean)
extern void OidLookup_ToFriendlyName_m34B7595764843A001047D30E77B0DB1987653103 (void);
// 0x00000323 System.String Internal.Cryptography.OidLookup::ToOid(System.String,System.Security.Cryptography.OidGroup,System.Boolean)
extern void OidLookup_ToOid_mE362E659C0A532B8D2E1638D8B50FBACFEBFCF2D (void);
// 0x00000324 System.Boolean Internal.Cryptography.OidLookup::ShouldUseCache(System.Security.Cryptography.OidGroup)
extern void OidLookup_ShouldUseCache_mB50BE42B791BF8314979BCF725BB50F5C1DF0461 (void);
// 0x00000325 System.String Internal.Cryptography.OidLookup::NativeOidToFriendlyName(System.String,System.Security.Cryptography.OidGroup,System.Boolean)
extern void OidLookup_NativeOidToFriendlyName_m54826DC2A46712C51D3BBE397372E01D0BCBDB34 (void);
// 0x00000326 System.String Internal.Cryptography.OidLookup::NativeFriendlyNameToOid(System.String,System.Security.Cryptography.OidGroup,System.Boolean)
extern void OidLookup_NativeFriendlyNameToOid_m1CEF74ADB4771FEAFE0C491AC36DF806517C55AC (void);
// 0x00000327 System.Void Internal.Cryptography.OidLookup::.cctor()
extern void OidLookup__cctor_m315F5D4656A8C145CD154D9DF653F5207E12AEBC (void);
// 0x00000328 System.Void Internal.Cryptography.OidLookup/<>c::.cctor()
extern void U3CU3Ec__cctor_m2E929BF5DE0AAD59E0C0E2AA6875DFF57F976962 (void);
// 0x00000329 System.Void Internal.Cryptography.OidLookup/<>c::.ctor()
extern void U3CU3Ec__ctor_m381C6259C7B5579501CBFB1E4D1DF201D5928470 (void);
// 0x0000032A System.String Internal.Cryptography.OidLookup/<>c::<.cctor>b__10_0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec_U3C_cctorU3Eb__10_0_m6CCA3946EAC08544EB57D764DBF2D23E12DE30A1 (void);
// 0x0000032B System.String Internal.Cryptography.OidLookup/<>c::<.cctor>b__10_1(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec_U3C_cctorU3Eb__10_1_m7F86444F2B083758A76759BD54D31A3538101A4E (void);
// 0x0000032C System.Void Internal.Cryptography.Pal.CertificateData::.ctor(System.Byte[])
extern void CertificateData__ctor_m623A235F902E7262CB9DA2CEB1BF5D3E64A6D23F (void);
// 0x0000032D System.String Internal.Cryptography.Pal.CertificateData::GetNameInfo(System.Security.Cryptography.X509Certificates.X509NameType,System.Boolean)
extern void CertificateData_GetNameInfo_m8DC5068DE35A32AC641DCBA45C409D05C4CE9D72 (void);
// 0x0000032E System.String Internal.Cryptography.Pal.CertificateData::GetSimpleNameInfo(System.Security.Cryptography.X509Certificates.X500DistinguishedName)
extern void CertificateData_GetSimpleNameInfo_mA631A4BFF5FA74EED7E44B2398323213F4505511 (void);
// 0x0000032F System.String Internal.Cryptography.Pal.CertificateData::FindAltNameMatch(System.Byte[],Internal.Cryptography.Pal.GeneralNameType,System.String)
extern void CertificateData_FindAltNameMatch_mE725F81D068254CD717F42AC769E934327BA6E13 (void);
// 0x00000330 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Internal.Cryptography.Pal.CertificateData::ReadReverseRdns(System.Security.Cryptography.X509Certificates.X500DistinguishedName)
extern void CertificateData_ReadReverseRdns_mCC9E641803F520C0B6ABE2CF0D5D087F85E921D1 (void);
// 0x00000331 System.Void Internal.Cryptography.Pal.CertificateData/<ReadReverseRdns>d__21::.ctor(System.Int32)
extern void U3CReadReverseRdnsU3Ed__21__ctor_m5A6BC49C73B7B87A9A4400C6B15A6B08990A46D4 (void);
// 0x00000332 System.Void Internal.Cryptography.Pal.CertificateData/<ReadReverseRdns>d__21::System.IDisposable.Dispose()
extern void U3CReadReverseRdnsU3Ed__21_System_IDisposable_Dispose_mDB6F5E4B3207652BEAED7B42745D734D10C5249C (void);
// 0x00000333 System.Boolean Internal.Cryptography.Pal.CertificateData/<ReadReverseRdns>d__21::MoveNext()
extern void U3CReadReverseRdnsU3Ed__21_MoveNext_m9D224CA977CE0147564D800919EBB3CA3584DAC4 (void);
// 0x00000334 System.Collections.Generic.KeyValuePair`2<System.String,System.String> Internal.Cryptography.Pal.CertificateData/<ReadReverseRdns>d__21::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<System.String,System.String>>.get_Current()
extern void U3CReadReverseRdnsU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_get_Current_mF0FA23744F644CF1371214864DD2A43728064474 (void);
// 0x00000335 System.Void Internal.Cryptography.Pal.CertificateData/<ReadReverseRdns>d__21::System.Collections.IEnumerator.Reset()
extern void U3CReadReverseRdnsU3Ed__21_System_Collections_IEnumerator_Reset_m14F8B978364B8DDAC56367B68EEA3D7C06732564 (void);
// 0x00000336 System.Object Internal.Cryptography.Pal.CertificateData/<ReadReverseRdns>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CReadReverseRdnsU3Ed__21_System_Collections_IEnumerator_get_Current_m3CA95125EAEEBACD8685B90E65831D0B4B69EAA7 (void);
// 0x00000337 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Internal.Cryptography.Pal.CertificateData/<ReadReverseRdns>d__21::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<System.String,System.String>>.GetEnumerator()
extern void U3CReadReverseRdnsU3Ed__21_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_GetEnumerator_m19CB560BFA57F0DD069DCCA4F99AA3A67CEB5427 (void);
// 0x00000338 System.Collections.IEnumerator Internal.Cryptography.Pal.CertificateData/<ReadReverseRdns>d__21::System.Collections.IEnumerable.GetEnumerator()
extern void U3CReadReverseRdnsU3Ed__21_System_Collections_IEnumerable_GetEnumerator_mA9604E14B40705140F1AD16409F6716BDF3BFF08 (void);
// 0x00000339 System.Int32 System.IPv4AddressHelper::ParseHostNumber(System.ReadOnlySpan`1<System.Char>,System.Int32,System.Int32)
extern void IPv4AddressHelper_ParseHostNumber_mA778BE5D8E7B36AFF00BC3A042D532C5E4718B74 (void);
// 0x0000033A System.Boolean System.IPv4AddressHelper::IsValid(System.Char*,System.Int32,System.Int32&,System.Boolean,System.Boolean,System.Boolean)
extern void IPv4AddressHelper_IsValid_m1E626B5740F8C0F4F4AEA9D8D9E06933BD79C7AE (void);
// 0x0000033B System.Boolean System.IPv4AddressHelper::ParseCanonical(System.ReadOnlySpan`1<System.Char>,System.Byte*,System.Int32,System.Int32)
extern void IPv4AddressHelper_ParseCanonical_m7BE948BF36EC5AD70DFFD320BD9EE8A343DA795E (void);
// 0x0000033C System.Boolean System.IPv4AddressHelper::IsValidCanonical(System.Char*,System.Int32,System.Int32&,System.Boolean,System.Boolean)
extern void IPv4AddressHelper_IsValidCanonical_mBE62564C4A9F8F584C9FBC0E6A5D5159071BE23E (void);
// 0x0000033D System.Int64 System.IPv4AddressHelper::ParseNonCanonical(System.Char*,System.Int32,System.Int32&,System.Boolean)
extern void IPv4AddressHelper_ParseNonCanonical_m4ACD00B972D7DBEB80BC3D7A9C36BB2CEA81638E (void);
// 0x0000033E System.String System.IPv4AddressHelper::ParseCanonicalName(System.String,System.Int32,System.Int32,System.Boolean&)
extern void IPv4AddressHelper_ParseCanonicalName_m25D822E5AC4B56E40C2404C8DE1C03E70ABCF72C (void);
// 0x0000033F System.Boolean System.IPv4AddressHelper::Parse(System.String,System.Byte*,System.Int32,System.Int32)
extern void IPv4AddressHelper_Parse_m61983BF8F1D7D827BABB3BD87AE4A2CE2BA92906 (void);
// 0x00000340 System.ValueTuple`2<System.Int32,System.Int32> System.IPv6AddressHelper::FindCompressionRange(System.ReadOnlySpan`1<System.UInt16>)
extern void IPv6AddressHelper_FindCompressionRange_m52DF5ADCE15CB8AE6F141E829F1252EBB436A39F (void);
// 0x00000341 System.Boolean System.IPv6AddressHelper::ShouldHaveIpv4Embedded(System.ReadOnlySpan`1<System.UInt16>)
extern void IPv6AddressHelper_ShouldHaveIpv4Embedded_m59F598D7493A037117972E6D4AF8F233B7D87ABC (void);
// 0x00000342 System.Boolean System.IPv6AddressHelper::IsValidStrict(System.Char*,System.Int32,System.Int32&)
extern void IPv6AddressHelper_IsValidStrict_m63AA667FE446A0F20109E4680BE64B9B60F73123 (void);
// 0x00000343 System.Void System.IPv6AddressHelper::Parse(System.ReadOnlySpan`1<System.Char>,System.UInt16*,System.Int32,System.String&)
extern void IPv6AddressHelper_Parse_mE09CDE68CBB5BE9195B68D52D79D81591C270CAF (void);
// 0x00000344 System.String System.IPv6AddressHelper::ParseCanonicalName(System.String,System.Int32,System.Boolean&,System.String&)
extern void IPv6AddressHelper_ParseCanonicalName_m148CB0071D79C29D97FE7E537B0BFA2CB27B709C (void);
// 0x00000345 System.Boolean System.IPv6AddressHelper::IsLoopback(System.ReadOnlySpan`1<System.UInt16>)
extern void IPv6AddressHelper_IsLoopback_mE5795ECD1129E9EB2368E4A7D4EB1433CF51A3F4 (void);
// 0x00000346 System.Boolean System.IPv6AddressHelper::InternalIsValid(System.Char*,System.Int32,System.Int32&,System.Boolean)
extern void IPv6AddressHelper_InternalIsValid_m23A4A563DF5766AC8391628217ADE8670A740D3D (void);
// 0x00000347 System.Boolean System.IPv6AddressHelper::IsValid(System.Char*,System.Int32,System.Int32&)
extern void IPv6AddressHelper_IsValid_mEA13C6DB64BF812706785DA99E9AFFBC95C1C3E7 (void);
// 0x00000348 System.Exception System.NotImplemented::ByDesignWithMessage(System.String)
extern void NotImplemented_ByDesignWithMessage_m91D3B27BDC19FF6AAEDD420C9A8E3A43FB3B9DE3 (void);
// 0x00000349 System.Void System.UriBuilder::.ctor()
extern void UriBuilder__ctor_m1CBBB001D2542111D5B3334356E2AC65AFB80D27 (void);
// 0x0000034A System.Void System.UriBuilder::.ctor(System.Uri)
extern void UriBuilder__ctor_mD45945E23BE6C1AA8C56E64DEC181E36EE826C25 (void);
// 0x0000034B System.Void System.UriBuilder::Init(System.Uri)
extern void UriBuilder_Init_m5A6628A92532D3CAACD649C7528C15261C17A99D (void);
// 0x0000034C System.Void System.UriBuilder::set_Host(System.String)
extern void UriBuilder_set_Host_m7CD9C7B0B9CACAF15A57ACFC363C766F19178930 (void);
// 0x0000034D System.Void System.UriBuilder::set_Port(System.Int32)
extern void UriBuilder_set_Port_mC6C31D266AA5790B8ED038C31489BCC2DE55DE3D (void);
// 0x0000034E System.Void System.UriBuilder::set_Scheme(System.String)
extern void UriBuilder_set_Scheme_m02C71DED53B5BDB8CA03E7484B8B554D0EF4B3BA (void);
// 0x0000034F System.Uri System.UriBuilder::get_Uri()
extern void UriBuilder_get_Uri_mD0DF3F7DDF7C56CC99E2D29E06C959F80A4171A1 (void);
// 0x00000350 System.Boolean System.UriBuilder::Equals(System.Object)
extern void UriBuilder_Equals_m2318FB925C63380B75D92D199A9C2AE4AA98F322 (void);
// 0x00000351 System.Int32 System.UriBuilder::GetHashCode()
extern void UriBuilder_GetHashCode_m8ECFACD3573A5E927B335A3443481248118C8310 (void);
// 0x00000352 System.Void System.UriBuilder::SetFieldsFromUri(System.Uri)
extern void UriBuilder_SetFieldsFromUri_mB6015DF5AA0D123A1B2749378D88FA26D409788E (void);
// 0x00000353 System.String System.UriBuilder::ToString()
extern void UriBuilder_ToString_m0DEFF1C8981338B9D30B81620E839DCF2CFF4713 (void);
// 0x00000354 System.Void System.InvariantComparer::.ctor()
extern void InvariantComparer__ctor_m42B33CC43C194F792B70B96258F930ECF46970A5 (void);
// 0x00000355 System.Int32 System.InvariantComparer::Compare(System.Object,System.Object)
extern void InvariantComparer_Compare_m74CBFBF1B02F2E9C149AC405D969DB2F4A802889 (void);
// 0x00000356 System.Void System.InvariantComparer::.cctor()
extern void InvariantComparer__cctor_m319780C01DB316EE447344855F394630AF6F3C6F (void);
// 0x00000357 System.Boolean System.IriHelper::CheckIriUnicodeRange(System.Char,System.Boolean)
extern void IriHelper_CheckIriUnicodeRange_m47EF9FBD5FE075A464AE2A242FB43BEBC7C4F775 (void);
// 0x00000358 System.Boolean System.IriHelper::CheckIriUnicodeRange(System.Char,System.Char,System.Boolean&,System.Boolean)
extern void IriHelper_CheckIriUnicodeRange_m76D54508F06B0BD021CF86889C71C44578B495FB (void);
// 0x00000359 System.Boolean System.IriHelper::CheckIsReserved(System.Char,System.UriComponents)
extern void IriHelper_CheckIsReserved_mFF947D4AEDC2D811E2B5379B9706901ED2B29868 (void);
// 0x0000035A System.String System.IriHelper::EscapeUnescapeIri(System.Char*,System.Int32,System.Int32,System.UriComponents)
extern void IriHelper_EscapeUnescapeIri_mE7C6E85447A41E32A050856BC24B40279EDFF6E5 (void);
// 0x0000035B System.Boolean System.Uri::get_IsImplicitFile()
extern void Uri_get_IsImplicitFile_m8D1A522158E4EB8AD8C077376D023ED1262998F6 (void);
// 0x0000035C System.Boolean System.Uri::get_IsUncOrDosPath()
extern void Uri_get_IsUncOrDosPath_m33F2D8808AB44816518C1C464152D4B7872374F0 (void);
// 0x0000035D System.Boolean System.Uri::get_IsDosPath()
extern void Uri_get_IsDosPath_m16357B61F5D111C45EF7D40276B96429E414F0D2 (void);
// 0x0000035E System.Boolean System.Uri::get_IsUncPath()
extern void Uri_get_IsUncPath_m2D9E7E2DB4EA57EF268B9BA167A1389E18AE9835 (void);
// 0x0000035F System.Uri/Flags System.Uri::get_HostType()
extern void Uri_get_HostType_mA30DC51045EE5FE94B3CB84B0D361A9C28EE572E (void);
// 0x00000360 System.UriParser System.Uri::get_Syntax()
extern void Uri_get_Syntax_mCBAC18B1DC71D269BF6C98417D923674DE258DC2 (void);
// 0x00000361 System.Boolean System.Uri::get_IsNotAbsoluteUri()
extern void Uri_get_IsNotAbsoluteUri_m374185EB37A209B6102F955427913168C5543028 (void);
// 0x00000362 System.Boolean System.Uri::IriParsingStatic(System.UriParser)
extern void Uri_IriParsingStatic_m5FACD38C4EDB61BC6AC465759CDB56F24C34C3B8 (void);
// 0x00000363 System.Boolean System.Uri::get_AllowIdn()
extern void Uri_get_AllowIdn_m612B747CE2F03FCA867D81F14710758B561209E7 (void);
// 0x00000364 System.Boolean System.Uri::AllowIdnStatic(System.UriParser,System.Uri/Flags)
extern void Uri_AllowIdnStatic_m0B61B2A586F2BF870D185A1BD65151F52EFBC457 (void);
// 0x00000365 System.Boolean System.Uri::IsIntranet(System.String)
extern void Uri_IsIntranet_m3AC6DBC7C35F309DFAD0951ABC7D95A8000548EE (void);
// 0x00000366 System.Boolean System.Uri::get_UserDrivenParsing()
extern void Uri_get_UserDrivenParsing_mF2426529046B3DF3292A7AA2F4885F8D6591BBBD (void);
// 0x00000367 System.Void System.Uri::SetUserDrivenParsing()
extern void Uri_SetUserDrivenParsing_mDE80D352D6343F50F152EB65EF142E84F7F79D69 (void);
// 0x00000368 System.UInt16 System.Uri::get_SecuredPathIndex()
extern void Uri_get_SecuredPathIndex_m575B7B28B17F8F7EE3FA563ACF6390B0926732AD (void);
// 0x00000369 System.Boolean System.Uri::NotAny(System.Uri/Flags)
extern void Uri_NotAny_mB88A38A602D4A784CADBD43FDF4CDA0235371E81 (void);
// 0x0000036A System.Boolean System.Uri::InFact(System.Uri/Flags)
extern void Uri_InFact_mEEE8ADE0780C0712A3DEB8935BD6FC7B23F38A7B (void);
// 0x0000036B System.Boolean System.Uri::StaticNotAny(System.Uri/Flags,System.Uri/Flags)
extern void Uri_StaticNotAny_mC49F6501AFE849E383AC6777CEB63D2B409B264E (void);
// 0x0000036C System.Boolean System.Uri::StaticInFact(System.Uri/Flags,System.Uri/Flags)
extern void Uri_StaticInFact_m85C3E521AD9916CC030AC01E311F79D90E981B05 (void);
// 0x0000036D System.Uri/UriInfo System.Uri::EnsureUriInfo()
extern void Uri_EnsureUriInfo_m6FE387306AF82CDCFC56E2E9C2171C406D1FA982 (void);
// 0x0000036E System.Void System.Uri::EnsureParseRemaining()
extern void Uri_EnsureParseRemaining_mDF7D4313A56047EB22457E12330C138156D54783 (void);
// 0x0000036F System.Void System.Uri::EnsureHostString(System.Boolean)
extern void Uri_EnsureHostString_m7F5F3ED8488F83170A857CD51F0CFEB8322AA055 (void);
// 0x00000370 System.Void System.Uri::.ctor(System.String)
extern void Uri__ctor_m6CA436E6AD2768A121FA851CBEEFA3623E849D3A (void);
// 0x00000371 System.Void System.Uri::.ctor(System.String,System.UriKind)
extern void Uri__ctor_m24549041BC5661EAC10BA8CB35B60AD6512AF69B (void);
// 0x00000372 System.Void System.Uri::.ctor(System.Uri,System.String)
extern void Uri__ctor_mD7EC916948CBFE71F80298DDA6E4209724958B09 (void);
// 0x00000373 System.Void System.Uri::CreateUri(System.Uri,System.String,System.Boolean)
extern void Uri_CreateUri_m9D81A5F3B332C44FDD875893EF619631708791F2 (void);
// 0x00000374 System.Void System.Uri::.ctor(System.Uri,System.Uri)
extern void Uri__ctor_m5AA91BCDE21E8AD62B9A48948CF3661B2F00E38F (void);
// 0x00000375 System.ParsingError System.Uri::GetCombinedString(System.Uri,System.String,System.Boolean,System.String&)
extern void Uri_GetCombinedString_mA3D01108BC32A4594A578A4345BA0F012F554521 (void);
// 0x00000376 System.UriFormatException System.Uri::GetException(System.ParsingError)
extern void Uri_GetException_m9BD1AF317D6DE516C8D3909A0ECD460BA35996F7 (void);
// 0x00000377 System.Void System.Uri::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void Uri__ctor_mE1AB9FB0563073B8DFB381A69A54E9E0459D583F (void);
// 0x00000378 System.Void System.Uri::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m39D9B260E68B8715E9B693A125A2116CE14D760D (void);
// 0x00000379 System.Void System.Uri::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void Uri_GetObjectData_m0D16C2BC060F0B88503DCC0EEACB151D43FA16CA (void);
// 0x0000037A System.String System.Uri::get_AbsolutePath()
extern void Uri_get_AbsolutePath_mABB93DD30D4C0F11948DE5C117650B1C3A9925CA (void);
// 0x0000037B System.String System.Uri::get_PrivateAbsolutePath()
extern void Uri_get_PrivateAbsolutePath_mF7B63FC5EBFAB1EE164BF868900B548962C5A546 (void);
// 0x0000037C System.String System.Uri::get_AbsoluteUri()
extern void Uri_get_AbsoluteUri_m080934F4F2E2160EBEABDF00F8B6D59888EA63AE (void);
// 0x0000037D System.String System.Uri::get_LocalPath()
extern void Uri_get_LocalPath_m60EE736E6F601238CBC20CE7DBB647C61F16DB0E (void);
// 0x0000037E System.String System.Uri::get_Authority()
extern void Uri_get_Authority_m5BE92DE43B1B4BCD6849FEBDE5530AB97E0C31EB (void);
// 0x0000037F System.Boolean System.Uri::get_IsDefaultPort()
extern void Uri_get_IsDefaultPort_m1BFC89B27306852E9AFDEF92897FD52B0B3ECD06 (void);
// 0x00000380 System.Boolean System.Uri::get_IsFile()
extern void Uri_get_IsFile_m760E384ED177DC9197D5010B763F64196552DF5F (void);
// 0x00000381 System.Boolean System.Uri::get_IsLoopback()
extern void Uri_get_IsLoopback_m0E865DCC324F483C0C60E7D8E6FEF7AD0F46059A (void);
// 0x00000382 System.String System.Uri::get_PathAndQuery()
extern void Uri_get_PathAndQuery_m33B64A4AAF08510C0199CF5A85918B91D1008E5C (void);
// 0x00000383 System.Boolean System.Uri::get_IsUnc()
extern void Uri_get_IsUnc_mB190B452D6A7B09FC8BDD967A5C38F32E87E0FCA (void);
// 0x00000384 System.String System.Uri::get_Host()
extern void Uri_get_Host_m2C0E258C7DFF7A340049BE9BC08FF45E90988D8C (void);
// 0x00000385 System.Boolean System.Uri::StaticIsFile(System.UriParser)
extern void Uri_StaticIsFile_mDA17D91238AB2D1C549C55C524F67C714145D9CF (void);
// 0x00000386 System.Object System.Uri::get_InitializeLock()
extern void Uri_get_InitializeLock_mBC92A79EFBC775C9192BB5992F88AB868576D9CF (void);
// 0x00000387 System.Void System.Uri::InitializeUriConfig()
extern void Uri_InitializeUriConfig_mADB8B77C49B053970C363E91FE4BDD8315401364 (void);
// 0x00000388 System.String System.Uri::GetLocalPath()
extern void Uri_GetLocalPath_mDC900B37A0BEA02CAD7298582C2042221F9859DE (void);
// 0x00000389 System.Int32 System.Uri::get_Port()
extern void Uri_get_Port_m7A1413AC9D9A2FC5DC8A7F89DF4A01ACA0241A02 (void);
// 0x0000038A System.String System.Uri::get_Query()
extern void Uri_get_Query_m17DEC663263C486C8C8177AF9C4BFE2FFBE51178 (void);
// 0x0000038B System.String System.Uri::get_Fragment()
extern void Uri_get_Fragment_m3CB7AD14DA9618ADE406039D4713F67D6951D835 (void);
// 0x0000038C System.String System.Uri::get_Scheme()
extern void Uri_get_Scheme_m29106D5109538220B22FC49DE7B44040E51B0F6F (void);
// 0x0000038D System.Boolean System.Uri::get_OriginalStringSwitched()
extern void Uri_get_OriginalStringSwitched_mBE7152D5DA7A1D922B728B9EA8C64815B0469FB7 (void);
// 0x0000038E System.String System.Uri::get_OriginalString()
extern void Uri_get_OriginalString_m3031F9054CA10F2C55C0E2415CC19810D360A5D6 (void);
// 0x0000038F System.String System.Uri::get_DnsSafeHost()
extern void Uri_get_DnsSafeHost_mE5001E7AE7F1879A2059024EA06C9031F6320A2B (void);
// 0x00000390 System.Boolean System.Uri::get_IsAbsoluteUri()
extern void Uri_get_IsAbsoluteUri_m2F9B759B85D295639D7959A616E1FBC203D756DF (void);
// 0x00000391 System.Boolean System.Uri::get_UserEscaped()
extern void Uri_get_UserEscaped_m5A376B8E2DDB815CAA2E34AF8890FBA943DC2085 (void);
// 0x00000392 System.String System.Uri::get_UserInfo()
extern void Uri_get_UserInfo_mC0189C851F71ACF0B48A462E74C7F3EB86501C8D (void);
// 0x00000393 System.Boolean System.Uri::IsGenDelim(System.Char)
extern void Uri_IsGenDelim_mB6256291D6D90DAFB8B923D197BBD48A371F4265 (void);
// 0x00000394 System.Boolean System.Uri::CheckSchemeName(System.String)
extern void Uri_CheckSchemeName_m00DF91AC5BC363425D403696C6251A6BCC5E9E62 (void);
// 0x00000395 System.Boolean System.Uri::IsHexDigit(System.Char)
extern void Uri_IsHexDigit_m65CBC0215C67105D79C7B49FAF5DD4676B86ED20 (void);
// 0x00000396 System.Int32 System.Uri::FromHex(System.Char)
extern void Uri_FromHex_m16EF28AD07AF8D499C8157124A9E7C01D6FFFAA8 (void);
// 0x00000397 System.Int32 System.Uri::GetHashCode()
extern void Uri_GetHashCode_m41959A208E51019C57C20014B2505993FB94B70C (void);
// 0x00000398 System.String System.Uri::ToString()
extern void Uri_ToString_mDD98375BF8CEBEF6E3708BBCA6C4AB0B54165DA2 (void);
// 0x00000399 System.Boolean System.Uri::op_Equality(System.Uri,System.Uri)
extern void Uri_op_Equality_mB299FA02A76FD12A781BCBAD53D65B73C1768682 (void);
// 0x0000039A System.Boolean System.Uri::op_Inequality(System.Uri,System.Uri)
extern void Uri_op_Inequality_mC32A3382EF16D80BF39005BDD2AB452203D76D6E (void);
// 0x0000039B System.Boolean System.Uri::Equals(System.Object)
extern void Uri_Equals_m1BD19791DD8A42FEFF5844C29623626C2849C452 (void);
// 0x0000039C System.ParsingError System.Uri::ParseScheme(System.String,System.Uri/Flags&,System.UriParser&)
extern void Uri_ParseScheme_m97A1B445A9528F29FC3007B02F2F8789AC201E82 (void);
// 0x0000039D System.UriFormatException System.Uri::ParseMinimal()
extern void Uri_ParseMinimal_m24D824EC77216C64F4B8B9175915CD6C3C6DB7B5 (void);
// 0x0000039E System.ParsingError System.Uri::PrivateParseMinimal()
extern void Uri_PrivateParseMinimal_m4D4C3CB26AC0DE8B0E1F7DC4143F2004D9D25B17 (void);
// 0x0000039F System.Void System.Uri::PrivateParseMinimalIri(System.String,System.UInt16)
extern void Uri_PrivateParseMinimalIri_mD8C7CA7EE14D994E915B48FB6A2CDE62253D4F1E (void);
// 0x000003A0 System.Void System.Uri::CreateUriInfo(System.Uri/Flags)
extern void Uri_CreateUriInfo_m3B4F804AB6CB010246F2A52F123D6E44DECDE998 (void);
// 0x000003A1 System.Void System.Uri::CreateHostString()
extern void Uri_CreateHostString_mBBAF0ACD4E2EE673F8F7F4E416D87517D94245CF (void);
// 0x000003A2 System.String System.Uri::CreateHostStringHelper(System.String,System.UInt16,System.UInt16,System.Uri/Flags&,System.String&)
extern void Uri_CreateHostStringHelper_mB7076A86E12D57D5722AA4AC0993B38332A21C8D (void);
// 0x000003A3 System.Void System.Uri::GetHostViaCustomSyntax()
extern void Uri_GetHostViaCustomSyntax_m6632DAD1F66901E82C60A47D626A9140A586C802 (void);
// 0x000003A4 System.String System.Uri::GetParts(System.UriComponents,System.UriFormat)
extern void Uri_GetParts_m432C93EAC8684C7410585D36DA00B04F6DC34357 (void);
// 0x000003A5 System.String System.Uri::GetEscapedParts(System.UriComponents)
extern void Uri_GetEscapedParts_m0C33275AFEA210E5CAD22649E8B61B49FB43971E (void);
// 0x000003A6 System.String System.Uri::GetUnescapedParts(System.UriComponents,System.UriFormat)
extern void Uri_GetUnescapedParts_m57F83B75B05C5D8EAB6B8ACCB6C8E4F0922CB7ED (void);
// 0x000003A7 System.String System.Uri::ReCreateParts(System.UriComponents,System.UInt16,System.UriFormat)
extern void Uri_ReCreateParts_mB622FFDF74676902D184FD96C4E6B740B65140AF (void);
// 0x000003A8 System.String System.Uri::GetUriPartsFromUserString(System.UriComponents)
extern void Uri_GetUriPartsFromUserString_m3D6EEB76609CCC54A0ABE65911CC24603A184B29 (void);
// 0x000003A9 System.Void System.Uri::ParseRemaining()
extern void Uri_ParseRemaining_m9134D26360C9EEDFAFAFEE1ECA1A5CEE66BE5A3A (void);
// 0x000003AA System.UInt16 System.Uri::ParseSchemeCheckImplicitFile(System.Char*,System.UInt16,System.ParsingError&,System.Uri/Flags&,System.UriParser&)
extern void Uri_ParseSchemeCheckImplicitFile_m8244C40705A553AA88036339B231B1BB279580DC (void);
// 0x000003AB System.Boolean System.Uri::CheckKnownSchemes(System.Int64*,System.UInt16,System.UriParser&)
extern void Uri_CheckKnownSchemes_mB4E8E77DD15C70FACC9DA5078083E670B0EC9FA1 (void);
// 0x000003AC System.ParsingError System.Uri::CheckSchemeSyntax(System.Char*,System.UInt16,System.UriParser&)
extern void Uri_CheckSchemeSyntax_m7D9517CBE8B09F13A24AF4EEF0AE2D185C3E1C2D (void);
// 0x000003AD System.UInt16 System.Uri::CheckAuthorityHelper(System.Char*,System.UInt16,System.UInt16,System.ParsingError&,System.Uri/Flags&,System.UriParser,System.String&)
extern void Uri_CheckAuthorityHelper_m7946931160B7EC50E7167A11E570A4AF8AEB5B2E (void);
// 0x000003AE System.Void System.Uri::CheckAuthorityHelperHandleDnsIri(System.Char*,System.UInt16,System.Int32,System.Int32,System.Boolean,System.Boolean,System.UriParser,System.String,System.Uri/Flags&,System.Boolean&,System.String&,System.ParsingError&)
extern void Uri_CheckAuthorityHelperHandleDnsIri_mB737EC35D3A477668E6E289D09DFB43EAD4D4D62 (void);
// 0x000003AF System.Void System.Uri::CheckAuthorityHelperHandleAnyHostIri(System.Char*,System.Int32,System.Int32,System.Boolean,System.Boolean,System.UriParser,System.Uri/Flags&,System.String&,System.ParsingError&)
extern void Uri_CheckAuthorityHelperHandleAnyHostIri_mE38845CEFA41AB52AB949CAE2A43D77FB397C08D (void);
// 0x000003B0 System.Void System.Uri::FindEndOfComponent(System.String,System.UInt16&,System.UInt16,System.Char)
extern void Uri_FindEndOfComponent_m59CB1EFDA410B42014EA7DCDBA0C9FC9F5996E02 (void);
// 0x000003B1 System.Void System.Uri::FindEndOfComponent(System.Char*,System.UInt16&,System.UInt16,System.Char)
extern void Uri_FindEndOfComponent_m198F7AC80BEA08E8DAA6B4577D8A872B41C519B1 (void);
// 0x000003B2 System.Uri/Check System.Uri::CheckCanonical(System.Char*,System.UInt16&,System.UInt16,System.Char)
extern void Uri_CheckCanonical_m54AFA85AA00A9909C2EC01F026873F12532AD7F7 (void);
// 0x000003B3 System.Char[] System.Uri::GetCanonicalPath(System.Char[],System.Int32&,System.UriFormat)
extern void Uri_GetCanonicalPath_m66FA22F6E85D79E360A7EE86C0E03F1A009F6F99 (void);
// 0x000003B4 System.Void System.Uri::UnescapeOnly(System.Char*,System.Int32,System.Int32&,System.Char,System.Char,System.Char)
extern void Uri_UnescapeOnly_m16364432252C3908D378B0BC2BC37F9AD5CED9A6 (void);
// 0x000003B5 System.Char[] System.Uri::Compress(System.Char[],System.UInt16,System.Int32&,System.UriParser)
extern void Uri_Compress_m0A24E0C5DD7555EA7723C37FF428B0B1CA6B976D (void);
// 0x000003B6 System.Int32 System.Uri::CalculateCaseInsensitiveHashCode(System.String)
extern void Uri_CalculateCaseInsensitiveHashCode_mC84DC2B6AE39DB00143C7DE8BD0CDA05872635A5 (void);
// 0x000003B7 System.String System.Uri::CombineUri(System.Uri,System.String,System.UriFormat)
extern void Uri_CombineUri_m0A99C4A1F80FF7E6C8747545BB4610ABC4B4F9D1 (void);
// 0x000003B8 System.Boolean System.Uri::get_HasAuthority()
extern void Uri_get_HasAuthority_m2180FC17FB6FE7A11EBA0BDE1EF5EB9EF808A909 (void);
// 0x000003B9 System.Boolean System.Uri::IsLWS(System.Char)
extern void Uri_IsLWS_m16D38DDA4D0A7CD3899DEEE31CEF160EAF2C3A44 (void);
// 0x000003BA System.Boolean System.Uri::IsAsciiLetter(System.Char)
extern void Uri_IsAsciiLetter_mFDC33D2D538AFF6A5819B73CB555709865ADE841 (void);
// 0x000003BB System.Boolean System.Uri::IsAsciiLetterOrDigit(System.Char)
extern void Uri_IsAsciiLetterOrDigit_m44901130AFF9E14663C5F9E6437BE44484A82BB8 (void);
// 0x000003BC System.Boolean System.Uri::IsBidiControlCharacter(System.Char)
extern void Uri_IsBidiControlCharacter_m17A486D6ED9263488C6F4B3F3EDF1BF95352765E (void);
// 0x000003BD System.String System.Uri::StripBidiControlCharacter(System.Char*,System.Int32,System.Int32)
extern void Uri_StripBidiControlCharacter_m259E2D3AA84B455ADD63DFBC4EC802241E4317FA (void);
// 0x000003BE System.Void System.Uri::CreateThis(System.String,System.Boolean,System.UriKind)
extern void Uri_CreateThis_m522F36D576D9A8803410439127DB32837331BA3A (void);
// 0x000003BF System.Void System.Uri::InitializeUri(System.ParsingError,System.UriKind,System.UriFormatException&)
extern void Uri_InitializeUri_m044EA7C6188B132874B4522D5CA41EEFDB9A1718 (void);
// 0x000003C0 System.Boolean System.Uri::CheckForConfigLoad(System.String)
extern void Uri_CheckForConfigLoad_mC7297EBC00884B1793FDB16F79BF0FCFD53E9FA7 (void);
// 0x000003C1 System.Boolean System.Uri::CheckForUnicode(System.String)
extern void Uri_CheckForUnicode_mC763EF0DDC61EE359A6256B95209736EC0353AA1 (void);
// 0x000003C2 System.Boolean System.Uri::CheckForEscapedUnreserved(System.String)
extern void Uri_CheckForEscapedUnreserved_mCEF702F733C72BE825E9FEBBEA751540A6A78D11 (void);
// 0x000003C3 System.Boolean System.Uri::TryCreate(System.String,System.UriKind,System.Uri&)
extern void Uri_TryCreate_mBDD7F98C3BEBD9C3936DDF7960533CBB56478FDF (void);
// 0x000003C4 System.String System.Uri::GetComponents(System.UriComponents,System.UriFormat)
extern void Uri_GetComponents_m30E3EAD4E9A39939456E764FEF94AFD4D6B7532A (void);
// 0x000003C5 System.String System.Uri::UnescapeDataString(System.String)
extern void Uri_UnescapeDataString_mD0A4732F7ED9CA5DB9E46D802971B5406094CC77 (void);
// 0x000003C6 System.String System.Uri::EscapeUnescapeIri(System.String,System.Int32,System.Int32,System.UriComponents)
extern void Uri_EscapeUnescapeIri_m14C0CD1A4C14E2458685E5A10049204B1A15C16C (void);
// 0x000003C7 System.Void System.Uri::.ctor(System.Uri/Flags,System.UriParser,System.String)
extern void Uri__ctor_m6EA3AF49C103A3D56EBBB24BCAEE8DE7E8676AF4 (void);
// 0x000003C8 System.Uri System.Uri::CreateHelper(System.String,System.Boolean,System.UriKind,System.UriFormatException&)
extern void Uri_CreateHelper_mC4FC141C7E569879417FFC24BCB47E22CE09163B (void);
// 0x000003C9 System.Uri System.Uri::ResolveHelper(System.Uri,System.Uri,System.String&,System.Boolean&,System.UriFormatException&)
extern void Uri_ResolveHelper_m8FD2A09E63507DB5178AE3C182C323E8EA4B8752 (void);
// 0x000003CA System.String System.Uri::GetRelativeSerializationString(System.UriFormat)
extern void Uri_GetRelativeSerializationString_mDEB0AD1094EC2842C2F0830C1E43FF2FFDEB6F5B (void);
// 0x000003CB System.String System.Uri::GetComponentsHelper(System.UriComponents,System.UriFormat)
extern void Uri_GetComponentsHelper_m83D8E4A027E180AFB34BA5290A8FF381EBCBCB75 (void);
// 0x000003CC System.Void System.Uri::CreateThisFromUri(System.Uri)
extern void Uri_CreateThisFromUri_m986BB8F1CF8D75EEFA12D9D0963BA927FA394BE5 (void);
// 0x000003CD System.Void System.Uri::.cctor()
extern void Uri__cctor_mED6507CF356205205DE18F0D9D2A660D4C094F23 (void);
// 0x000003CE System.Void System.Uri/UriInfo::.ctor()
extern void UriInfo__ctor_m88A6E3E7164B78B18C70D83CA5AD734517D308B3 (void);
// 0x000003CF System.Void System.Uri/MoreInfo::.ctor()
extern void MoreInfo__ctor_m4AC1286A4D4DAD0AB0EE99A2C952D54BFA5F44A7 (void);
// 0x000003D0 System.Void System.UriFormatException::.ctor()
extern void UriFormatException__ctor_m4D218C92C093CD0CDDDC012911A1E10CB327BB70 (void);
// 0x000003D1 System.Void System.UriFormatException::.ctor(System.String)
extern void UriFormatException__ctor_m718A5DEB020FAAD1F654CD05E63C3988AA7712D3 (void);
// 0x000003D2 System.Void System.UriFormatException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void UriFormatException__ctor_m15F8C1205D9A648F72DD5D94DB92FD1FA1397FE0 (void);
// 0x000003D3 System.Void System.UriFormatException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_mF5C6CA057915A5F7F4E9D8FBE5D6EB2595A4FD80 (void);
// 0x000003D4 System.Char[] System.UriHelper::EscapeString(System.String,System.Int32,System.Int32,System.Char[],System.Int32&,System.Boolean,System.Char,System.Char,System.Char)
extern void UriHelper_EscapeString_m616525587FFF13665945B9173F3C2EC41C9EDDA8 (void);
// 0x000003D5 System.Char[] System.UriHelper::EnsureDestinationSize(System.Char*,System.Char[],System.Int32,System.Int16,System.Int16,System.Int32&,System.Int32)
extern void UriHelper_EnsureDestinationSize_m4CAB570AAAC5145D6DAF9AC4362812693C324887 (void);
// 0x000003D6 System.Char[] System.UriHelper::UnescapeString(System.String,System.Int32,System.Int32,System.Char[],System.Int32&,System.Char,System.Char,System.Char,System.UnescapeMode,System.UriParser,System.Boolean)
extern void UriHelper_UnescapeString_m978B253231CE4F7BA6F6DC7778D679C19F77CB54 (void);
// 0x000003D7 System.Char[] System.UriHelper::UnescapeString(System.Char*,System.Int32,System.Int32,System.Char[],System.Int32&,System.Char,System.Char,System.Char,System.UnescapeMode,System.UriParser,System.Boolean)
extern void UriHelper_UnescapeString_mD137E2B3F648504A96E7F244796CDCEBE6BB9BE1 (void);
// 0x000003D8 System.Void System.UriHelper::MatchUTF8Sequence(System.Char*,System.Char[],System.Int32&,System.Char[],System.Int32,System.Byte[],System.Int32,System.Boolean,System.Boolean)
extern void UriHelper_MatchUTF8Sequence_m0E963F060C446C4BBBBD60E4D54CAE9A36D5A3F7 (void);
// 0x000003D9 System.Void System.UriHelper::EscapeAsciiChar(System.Char,System.Char[],System.Int32&)
extern void UriHelper_EscapeAsciiChar_m3B740B3721EF4D93E9B9DFB2F881138AF4DE4452 (void);
// 0x000003DA System.Char System.UriHelper::EscapedAscii(System.Char,System.Char)
extern void UriHelper_EscapedAscii_mC7BE08D5B7803E83C57B8269F298B6DFA9EC5120 (void);
// 0x000003DB System.Boolean System.UriHelper::IsNotSafeForUnescape(System.Char)
extern void UriHelper_IsNotSafeForUnescape_mD11825AB8FE6E410A5C0D6D7850C607DBF697D7E (void);
// 0x000003DC System.Boolean System.UriHelper::IsReservedUnreservedOrHash(System.Char)
extern void UriHelper_IsReservedUnreservedOrHash_mEAD756DAD5DB17F71960CC501BBB7CF9AB5EF74F (void);
// 0x000003DD System.Boolean System.UriHelper::IsUnreserved(System.Char)
extern void UriHelper_IsUnreserved_m3171C456B602B4F7052B4E865F2DF04C71553797 (void);
// 0x000003DE System.Boolean System.UriHelper::Is3986Unreserved(System.Char)
extern void UriHelper_Is3986Unreserved_m06ED29AEFC563547A29E1DAA6A55760ADD9FCC30 (void);
// 0x000003DF System.Void System.UriHelper::.cctor()
extern void UriHelper__cctor_mEBD15B6A7F8E49B4188075DC2EF1A2956E7E3185 (void);
// 0x000003E0 System.String System.UriParser::get_SchemeName()
extern void UriParser_get_SchemeName_m3BD969B4E8EEA3769D070FA42CE340CDC766F47D (void);
// 0x000003E1 System.Int32 System.UriParser::get_DefaultPort()
extern void UriParser_get_DefaultPort_m38684C86B40F59960533D8F812B311ACBBEB7AA5 (void);
// 0x000003E2 System.UriParser System.UriParser::OnNewUri()
extern void UriParser_OnNewUri_m752BC0174A2E3F7AA99F1088E1D539E5B8F7B5B2 (void);
// 0x000003E3 System.Void System.UriParser::InitializeAndValidate(System.Uri,System.UriFormatException&)
extern void UriParser_InitializeAndValidate_m5B38A10B57DC49B1CE13F361ABD78D7269EDA886 (void);
// 0x000003E4 System.String System.UriParser::Resolve(System.Uri,System.Uri,System.UriFormatException&)
extern void UriParser_Resolve_m7CC03524A2848FBD24BAB57CC62A19C751937182 (void);
// 0x000003E5 System.String System.UriParser::GetComponents(System.Uri,System.UriComponents,System.UriFormat)
extern void UriParser_GetComponents_m230F3341901B4B90DC4650093272BC15A1161FAF (void);
// 0x000003E6 System.Boolean System.UriParser::get_ShouldUseLegacyV2Quirks()
extern void UriParser_get_ShouldUseLegacyV2Quirks_m2BDF10B430FDA291AA6962104CD838776F31D20E (void);
// 0x000003E7 System.Void System.UriParser::.cctor()
extern void UriParser__cctor_m9E43C905DCA4EE1855EE2C0236A1FCB6075841C5 (void);
// 0x000003E8 System.UriSyntaxFlags System.UriParser::get_Flags()
extern void UriParser_get_Flags_mBE183D7CBEEEBEC670CCF1DF21D56B354796D6C3 (void);
// 0x000003E9 System.Boolean System.UriParser::NotAny(System.UriSyntaxFlags)
extern void UriParser_NotAny_mCE6CF6736414BE547215F6E44C3E1283DEA05F56 (void);
// 0x000003EA System.Boolean System.UriParser::InFact(System.UriSyntaxFlags)
extern void UriParser_InFact_m3C07C3050DE1E69F6B0BC816EC34AF610373E532 (void);
// 0x000003EB System.Boolean System.UriParser::IsAllSet(System.UriSyntaxFlags)
extern void UriParser_IsAllSet_mEE811F1E7BBCC34D7A32BC30A18AD5D34E690813 (void);
// 0x000003EC System.Boolean System.UriParser::IsFullMatch(System.UriSyntaxFlags,System.UriSyntaxFlags)
extern void UriParser_IsFullMatch_mFC1C9EEDD31F040997931B26EF54CE7988495550 (void);
// 0x000003ED System.Void System.UriParser::.ctor(System.UriSyntaxFlags)
extern void UriParser__ctor_m4FA54685DFA00FC6AEA73E94CD59C5FF2872E55A (void);
// 0x000003EE System.UriParser System.UriParser::FindOrFetchAsUnknownV1Syntax(System.String)
extern void UriParser_FindOrFetchAsUnknownV1Syntax_m29D45A9BF93C7C4FC7C819658043B6E753E7F01A (void);
// 0x000003EF System.UriParser System.UriParser::GetSyntax(System.String)
extern void UriParser_GetSyntax_mD610CEF6784E137AF251EBC41629E2FE1D344615 (void);
// 0x000003F0 System.Boolean System.UriParser::get_IsSimple()
extern void UriParser_get_IsSimple_m8ED850D585BA3BA0FDC239496065E6D2D973C7E5 (void);
// 0x000003F1 System.UriParser System.UriParser::InternalOnNewUri()
extern void UriParser_InternalOnNewUri_m93B84E04949FAE8FE3B6919DA9F933B682D4B2F8 (void);
// 0x000003F2 System.Void System.UriParser::InternalValidate(System.Uri,System.UriFormatException&)
extern void UriParser_InternalValidate_m69157B3E80381F3FC104788160F2213B3FE8F39D (void);
// 0x000003F3 System.String System.UriParser::InternalResolve(System.Uri,System.Uri,System.UriFormatException&)
extern void UriParser_InternalResolve_mD1963080EBCF493DA78656AA5AEA458BF5138F5B (void);
// 0x000003F4 System.String System.UriParser::InternalGetComponents(System.Uri,System.UriComponents,System.UriFormat)
extern void UriParser_InternalGetComponents_m52F450C5283A1820D5AFD07505E5FE82D4F476B0 (void);
// 0x000003F5 System.Void System.UriParser/BuiltInUriParser::.ctor(System.String,System.Int32,System.UriSyntaxFlags)
extern void BuiltInUriParser__ctor_m6E9775075E7360DD524B7E3E456A3FFA58F89E79 (void);
// 0x000003F6 System.String System.DomainNameHelper::ParseCanonicalName(System.String,System.Int32,System.Int32,System.Boolean&)
extern void DomainNameHelper_ParseCanonicalName_m12865C6C2427B6ED804747CB1117D0B80E332F4D (void);
// 0x000003F7 System.Boolean System.DomainNameHelper::IsValid(System.Char*,System.UInt16,System.Int32&,System.Boolean&,System.Boolean)
extern void DomainNameHelper_IsValid_m9E1909F6CA353F469ACF623BCBE1E3B545164752 (void);
// 0x000003F8 System.Boolean System.DomainNameHelper::IsValidByIri(System.Char*,System.UInt16,System.Int32&,System.Boolean&,System.Boolean)
extern void DomainNameHelper_IsValidByIri_mE5772C5B7528885976ACB5AB11881B79A410CE24 (void);
// 0x000003F9 System.String System.DomainNameHelper::IdnEquivalent(System.Char*,System.Int32,System.Int32,System.Boolean&,System.Boolean&)
extern void DomainNameHelper_IdnEquivalent_m14CEBD0BD7B1BF46F8806E4EB5BB1901F7F29E2A (void);
// 0x000003FA System.String System.DomainNameHelper::IdnEquivalent(System.Char*,System.Int32,System.Int32,System.Boolean&,System.String&)
extern void DomainNameHelper_IdnEquivalent_mD8EBF17C7A0A50FFAA03EAA9CA737899C9A0881E (void);
// 0x000003FB System.Boolean System.DomainNameHelper::IsIdnAce(System.String,System.Int32)
extern void DomainNameHelper_IsIdnAce_m063EE4431000F1D566D2D62B3BBB947A00CDA8EF (void);
// 0x000003FC System.Boolean System.DomainNameHelper::IsIdnAce(System.Char*,System.Int32)
extern void DomainNameHelper_IsIdnAce_mC429C00971921E7C357FA9A57D14AAB9A0CF3566 (void);
// 0x000003FD System.String System.DomainNameHelper::UnicodeEquivalent(System.String,System.Char*,System.Int32,System.Int32)
extern void DomainNameHelper_UnicodeEquivalent_mD229718EC61E3ECBFA7AF5C894D22ADEEDE1B4ED (void);
// 0x000003FE System.String System.DomainNameHelper::UnicodeEquivalent(System.Char*,System.Int32,System.Int32,System.Boolean&,System.Boolean&)
extern void DomainNameHelper_UnicodeEquivalent_m62B17C161795C1C6AC76D49A3FB50B16693CC9DB (void);
// 0x000003FF System.Boolean System.DomainNameHelper::IsASCIILetterOrDigit(System.Char,System.Boolean&)
extern void DomainNameHelper_IsASCIILetterOrDigit_mFAF590F20333B1D787428537FCD1CFF3993FE1F6 (void);
// 0x00000400 System.Boolean System.DomainNameHelper::IsValidDomainLabelCharacter(System.Char,System.Boolean&)
extern void DomainNameHelper_IsValidDomainLabelCharacter_m957B15160BE378C49B6E25CC4F0D99BBA1FE06C6 (void);
// 0x00000401 System.String System.UncNameHelper::ParseCanonicalName(System.String,System.Int32,System.Int32,System.Boolean&)
extern void UncNameHelper_ParseCanonicalName_mB07DBCB21F6996E1A2BC9D4D4DD427AE0E5D36E2 (void);
// 0x00000402 System.Boolean System.UncNameHelper::IsValid(System.Char*,System.UInt16,System.Int32&,System.Boolean)
extern void UncNameHelper_IsValid_m677F95E73B0278AC062F862BEA6BE646048127D4 (void);
// 0x00000403 System.Void System.IOAsyncCallback::.ctor(System.Object,System.IntPtr)
extern void IOAsyncCallback__ctor_m58A7C732444005CB72C5B352096C2DB252A6F113 (void);
// 0x00000404 System.Void System.IOAsyncCallback::Invoke(System.IOAsyncResult)
extern void IOAsyncCallback_Invoke_m70CC21061AD0965DBC87CB382AD395A35E1788B2 (void);
// 0x00000405 System.Void System.IOAsyncResult::.ctor()
extern void IOAsyncResult__ctor_mAA7211B9E6FDF23D0012354FEE350B6B5EB786F4 (void);
// 0x00000406 System.Void System.IOAsyncResult::Init(System.AsyncCallback,System.Object)
extern void IOAsyncResult_Init_m6580DFCCB89EF7CFF91F7040C62ECC3811A3A98D (void);
// 0x00000407 System.Void System.IOAsyncResult::.ctor(System.AsyncCallback,System.Object)
extern void IOAsyncResult__ctor_mCDC93C97872E9DAB6A1D9F27C1FA14DDFC6A400D (void);
// 0x00000408 System.AsyncCallback System.IOAsyncResult::get_AsyncCallback()
extern void IOAsyncResult_get_AsyncCallback_mC6150F5FC2CEE8FDC22EACD9B9BC72544FE801CC (void);
// 0x00000409 System.Object System.IOAsyncResult::get_AsyncState()
extern void IOAsyncResult_get_AsyncState_m8F7E705BB64B9C38D1F9EE7C746AAD46DDF1616A (void);
// 0x0000040A System.Threading.WaitHandle System.IOAsyncResult::get_AsyncWaitHandle()
extern void IOAsyncResult_get_AsyncWaitHandle_mA0350FACA25DDD429DBE840269101E8168DCD42E (void);
// 0x0000040B System.Boolean System.IOAsyncResult::get_CompletedSynchronously()
extern void IOAsyncResult_get_CompletedSynchronously_m26279BC02AFFE83009A7EEABFFF58EEB2D12CD24 (void);
// 0x0000040C System.Void System.IOAsyncResult::set_CompletedSynchronously(System.Boolean)
extern void IOAsyncResult_set_CompletedSynchronously_m29ECF9F6388C1F9A12AF83DF8CD19D9FFB4B4D6D (void);
// 0x0000040D System.Boolean System.IOAsyncResult::get_IsCompleted()
extern void IOAsyncResult_get_IsCompleted_m0EAD0BE09029EE4AFC303B76DC5E1EBE4269BEE8 (void);
// 0x0000040E System.Void System.IOAsyncResult::set_IsCompleted(System.Boolean)
extern void IOAsyncResult_set_IsCompleted_m72D907352B8AC32DF47864E4F4EDFADEC825A6F9 (void);
// 0x0000040F System.Void System.IOAsyncResult::CompleteDisposed()
// 0x00000410 System.Void System.IOSelectorJob::.ctor(System.IOOperation,System.IOAsyncCallback,System.IOAsyncResult)
extern void IOSelectorJob__ctor_mA67864E470C66B78F683571EBD6A6315D9247DE1 (void);
// 0x00000411 System.Void System.IOSelectorJob::System.Threading.IThreadPoolWorkItem.ExecuteWorkItem()
extern void IOSelectorJob_System_Threading_IThreadPoolWorkItem_ExecuteWorkItem_m896BBDBDBB91D3771569204789FEF8E700123D79 (void);
// 0x00000412 System.Void System.IOSelectorJob::System.Threading.IThreadPoolWorkItem.MarkAborted(System.Threading.ThreadAbortException)
extern void IOSelectorJob_System_Threading_IThreadPoolWorkItem_MarkAborted_m3F59E62D7927340D04264BAA081F7FAE0533C9CE (void);
// 0x00000413 System.Void System.IOSelectorJob::MarkDisposed()
extern void IOSelectorJob_MarkDisposed_mBCCE13D70B97BB7AE46D976AE207DF642C4531F0 (void);
// 0x00000414 System.Void System.IOSelector::Add(System.IntPtr,System.IOSelectorJob)
extern void IOSelector_Add_m170DDEAFEE849601BFE7ADBCFD630A7F2BA96057 (void);
// 0x00000415 System.Void System.UriTypeConverter::.ctor()
extern void UriTypeConverter__ctor_m02CB01E1305031BE5A45FB54601E520748B6EDB6 (void);
// 0x00000416 System.Boolean System.UriTypeConverter::CanConvert(System.Type)
extern void UriTypeConverter_CanConvert_m970377ED30FF1B6875CCDA64FDC87EFE7D381DE3 (void);
// 0x00000417 System.Boolean System.UriTypeConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void UriTypeConverter_CanConvertFrom_mD5D419C5B80FC3A5D175655A6278E54FC80928E7 (void);
// 0x00000418 System.Boolean System.UriTypeConverter::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void UriTypeConverter_CanConvertTo_m119145C6F2FDE221E99ACDAF0EF723003388E8EE (void);
// 0x00000419 System.Object System.UriTypeConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern void UriTypeConverter_ConvertFrom_m079712540B92F2B6AF1780A5793F36EA77CA32ED (void);
// 0x0000041A System.Object System.UriTypeConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern void UriTypeConverter_ConvertTo_mA975FACF630714EBEA487EFC770EA0742756A764 (void);
// 0x0000041B System.Void System.Text.RegularExpressions.Capture::.ctor(System.String,System.Int32,System.Int32)
extern void Capture__ctor_mDC1197B1E29487F3D086091CF3D5B1029D593402 (void);
// 0x0000041C System.Int32 System.Text.RegularExpressions.Capture::get_Index()
extern void Capture_get_Index_m35E8D105E5E2142415F85116BFD4BA369AF7A17A (void);
// 0x0000041D System.Void System.Text.RegularExpressions.Capture::set_Index(System.Int32)
extern void Capture_set_Index_mD5E578A0833A03602FBDAA2ABF77C9CC849CDABE (void);
// 0x0000041E System.Int32 System.Text.RegularExpressions.Capture::get_Length()
extern void Capture_get_Length_mA3E97C9DF116CD4B4C20EBFB70A1312F33DA7A92 (void);
// 0x0000041F System.Void System.Text.RegularExpressions.Capture::set_Length(System.Int32)
extern void Capture_set_Length_mF9069396819D5D0282F38E70F347CDC625BE016E (void);
// 0x00000420 System.String System.Text.RegularExpressions.Capture::get_Text()
extern void Capture_get_Text_mDD394681E5FC39FFDCCD9552C0BF09DF8A7054ED (void);
// 0x00000421 System.Void System.Text.RegularExpressions.Capture::set_Text(System.String)
extern void Capture_set_Text_m99AE1BB9AF9D7744D0E6F747DCF5A378591C353C (void);
// 0x00000422 System.String System.Text.RegularExpressions.Capture::get_Value()
extern void Capture_get_Value_m1AB4193C2FC4B0D08AA34FECF10D03876D848BDC (void);
// 0x00000423 System.String System.Text.RegularExpressions.Capture::ToString()
extern void Capture_ToString_mC843EDDC990DBE2696BA08D08B1081CA12228F2C (void);
// 0x00000424 System.ReadOnlySpan`1<System.Char> System.Text.RegularExpressions.Capture::GetLeftSubstring()
extern void Capture_GetLeftSubstring_mBFF01B80F38DA0FF4E56EFEF9FD9EE8899AE57DC (void);
// 0x00000425 System.ReadOnlySpan`1<System.Char> System.Text.RegularExpressions.Capture::GetRightSubstring()
extern void Capture_GetRightSubstring_m1F1F896A999BC08AEEBD1BB28616F12FC593D1C3 (void);
// 0x00000426 System.Void System.Text.RegularExpressions.Group::.ctor(System.String,System.Int32[],System.Int32,System.String)
extern void Group__ctor_mFDE74CF068520CBE797B563B419B8817E60CB58C (void);
// 0x00000427 System.Boolean System.Text.RegularExpressions.Group::get_Success()
extern void Group_get_Success_m4E0238EE4B1E7F927E2AF13E2E5901BCA92BE62F (void);
// 0x00000428 System.Void System.Text.RegularExpressions.Group::.cctor()
extern void Group__cctor_mC3032FEBEBFB00282A4485A8D95E47649CE2B881 (void);
// 0x00000429 System.Void System.Text.RegularExpressions.Group::.ctor()
extern void Group__ctor_mEB6936CDFD191A4C92766CE3F3301161D7505C5E (void);
// 0x0000042A System.Boolean System.Text.RegularExpressions.GroupCollection::get_IsReadOnly()
extern void GroupCollection_get_IsReadOnly_mF1DE2BBB4E573C6EDE85B787281F90951AAB8C4A (void);
// 0x0000042B System.Int32 System.Text.RegularExpressions.GroupCollection::get_Count()
extern void GroupCollection_get_Count_mE8775E53B4F5DA6D520BEF5ECEED2E0C08A26B39 (void);
// 0x0000042C System.Text.RegularExpressions.Group System.Text.RegularExpressions.GroupCollection::get_Item(System.Int32)
extern void GroupCollection_get_Item_m40EC174D4AC8FDD68F8819C35B779C79A44322F3 (void);
// 0x0000042D System.Collections.IEnumerator System.Text.RegularExpressions.GroupCollection::GetEnumerator()
extern void GroupCollection_GetEnumerator_m47E269C5A8C902948812AF4F52D0C96759550EE9 (void);
// 0x0000042E System.Collections.Generic.IEnumerator`1<System.Text.RegularExpressions.Group> System.Text.RegularExpressions.GroupCollection::System.Collections.Generic.IEnumerable<System.Text.RegularExpressions.Group>.GetEnumerator()
extern void GroupCollection_System_Collections_Generic_IEnumerableU3CSystem_Text_RegularExpressions_GroupU3E_GetEnumerator_m593F2B44A71E46606D2E8C0816E626675EE2087D (void);
// 0x0000042F System.Text.RegularExpressions.Group System.Text.RegularExpressions.GroupCollection::GetGroup(System.Int32)
extern void GroupCollection_GetGroup_m8EDFDB8D85D8BEC6EB98B68CA69EE0022FC16B13 (void);
// 0x00000430 System.Text.RegularExpressions.Group System.Text.RegularExpressions.GroupCollection::GetGroupImpl(System.Int32)
extern void GroupCollection_GetGroupImpl_m27CE16949BE0A9CFC61CC6CBF3FB179F63654D92 (void);
// 0x00000431 System.Object System.Text.RegularExpressions.GroupCollection::get_SyncRoot()
extern void GroupCollection_get_SyncRoot_m7505B29FFFE40E5319BAD3F3B3332F8A6299570A (void);
// 0x00000432 System.Void System.Text.RegularExpressions.GroupCollection::CopyTo(System.Array,System.Int32)
extern void GroupCollection_CopyTo_m122BEB82372AC34C575FCB8C71188A2A49CCC482 (void);
// 0x00000433 System.Void System.Text.RegularExpressions.GroupCollection::CopyTo(System.Text.RegularExpressions.Group[],System.Int32)
extern void GroupCollection_CopyTo_mCA848040E42611E6A969C9E4F632FD81FC27B52A (void);
// 0x00000434 System.Int32 System.Text.RegularExpressions.GroupCollection::System.Collections.Generic.IList<System.Text.RegularExpressions.Group>.IndexOf(System.Text.RegularExpressions.Group)
extern void GroupCollection_System_Collections_Generic_IListU3CSystem_Text_RegularExpressions_GroupU3E_IndexOf_m63AB69A8D37D49B1AFFE8CDEED6442D25A25F94A (void);
// 0x00000435 System.Void System.Text.RegularExpressions.GroupCollection::System.Collections.Generic.IList<System.Text.RegularExpressions.Group>.Insert(System.Int32,System.Text.RegularExpressions.Group)
extern void GroupCollection_System_Collections_Generic_IListU3CSystem_Text_RegularExpressions_GroupU3E_Insert_m515A74F3095C972A28657B89FE6B65A3EC5324FA (void);
// 0x00000436 System.Void System.Text.RegularExpressions.GroupCollection::System.Collections.Generic.IList<System.Text.RegularExpressions.Group>.RemoveAt(System.Int32)
extern void GroupCollection_System_Collections_Generic_IListU3CSystem_Text_RegularExpressions_GroupU3E_RemoveAt_m981151BB2C68E7F2DC0BF4DD61D4AD4AB5B5AE3A (void);
// 0x00000437 System.Text.RegularExpressions.Group System.Text.RegularExpressions.GroupCollection::System.Collections.Generic.IList<System.Text.RegularExpressions.Group>.get_Item(System.Int32)
extern void GroupCollection_System_Collections_Generic_IListU3CSystem_Text_RegularExpressions_GroupU3E_get_Item_m7FD6BE83AFD3DCC2F9ABB2AB4B938F552DF1D45D (void);
// 0x00000438 System.Void System.Text.RegularExpressions.GroupCollection::System.Collections.Generic.IList<System.Text.RegularExpressions.Group>.set_Item(System.Int32,System.Text.RegularExpressions.Group)
extern void GroupCollection_System_Collections_Generic_IListU3CSystem_Text_RegularExpressions_GroupU3E_set_Item_mB5DBB39B4E6F360B8105064E6659EFC49E8B1C85 (void);
// 0x00000439 System.Void System.Text.RegularExpressions.GroupCollection::System.Collections.Generic.ICollection<System.Text.RegularExpressions.Group>.Add(System.Text.RegularExpressions.Group)
extern void GroupCollection_System_Collections_Generic_ICollectionU3CSystem_Text_RegularExpressions_GroupU3E_Add_m96D9B7FCEC33784ED88BAC3C291146E64A921D8D (void);
// 0x0000043A System.Void System.Text.RegularExpressions.GroupCollection::System.Collections.Generic.ICollection<System.Text.RegularExpressions.Group>.Clear()
extern void GroupCollection_System_Collections_Generic_ICollectionU3CSystem_Text_RegularExpressions_GroupU3E_Clear_m67DAFAB787617D8A9B720AAF612436587E2C3172 (void);
// 0x0000043B System.Boolean System.Text.RegularExpressions.GroupCollection::System.Collections.Generic.ICollection<System.Text.RegularExpressions.Group>.Contains(System.Text.RegularExpressions.Group)
extern void GroupCollection_System_Collections_Generic_ICollectionU3CSystem_Text_RegularExpressions_GroupU3E_Contains_m260CC86A4FD94F5682B73C9C8BE03551C14B3568 (void);
// 0x0000043C System.Boolean System.Text.RegularExpressions.GroupCollection::System.Collections.Generic.ICollection<System.Text.RegularExpressions.Group>.Remove(System.Text.RegularExpressions.Group)
extern void GroupCollection_System_Collections_Generic_ICollectionU3CSystem_Text_RegularExpressions_GroupU3E_Remove_m008F559DB848823129DDCB7B832C731F0D4AFF83 (void);
// 0x0000043D System.Int32 System.Text.RegularExpressions.GroupCollection::System.Collections.IList.Add(System.Object)
extern void GroupCollection_System_Collections_IList_Add_m08D9178D05AE66397CBCF2184448AE712741E4D1 (void);
// 0x0000043E System.Void System.Text.RegularExpressions.GroupCollection::System.Collections.IList.Clear()
extern void GroupCollection_System_Collections_IList_Clear_mCFF90C2303D7106D722E8B574ED3261C80F250CE (void);
// 0x0000043F System.Boolean System.Text.RegularExpressions.GroupCollection::System.Collections.IList.Contains(System.Object)
extern void GroupCollection_System_Collections_IList_Contains_m9E34B033D0BB903B2491336D7F70BE5A59D46208 (void);
// 0x00000440 System.Int32 System.Text.RegularExpressions.GroupCollection::System.Collections.IList.IndexOf(System.Object)
extern void GroupCollection_System_Collections_IList_IndexOf_mA974FC7CFC23310A1CAAD8A750823D5E0ED762AC (void);
// 0x00000441 System.Void System.Text.RegularExpressions.GroupCollection::System.Collections.IList.Insert(System.Int32,System.Object)
extern void GroupCollection_System_Collections_IList_Insert_m3CDAAB43F292FD0ABD2D9595F02233FB55577623 (void);
// 0x00000442 System.Boolean System.Text.RegularExpressions.GroupCollection::System.Collections.IList.get_IsFixedSize()
extern void GroupCollection_System_Collections_IList_get_IsFixedSize_m5AA2CBFB17E2617597BDFBB56CCA1E435A763014 (void);
// 0x00000443 System.Void System.Text.RegularExpressions.GroupCollection::System.Collections.IList.Remove(System.Object)
extern void GroupCollection_System_Collections_IList_Remove_m32982B763D442200E71B8A5E6C85C9501A555F23 (void);
// 0x00000444 System.Void System.Text.RegularExpressions.GroupCollection::System.Collections.IList.RemoveAt(System.Int32)
extern void GroupCollection_System_Collections_IList_RemoveAt_m32BF4A16212F4849AD46F680E5D0D14268CB7C58 (void);
// 0x00000445 System.Object System.Text.RegularExpressions.GroupCollection::System.Collections.IList.get_Item(System.Int32)
extern void GroupCollection_System_Collections_IList_get_Item_m969EA1758BE453F49B700FABF29940C5C9800573 (void);
// 0x00000446 System.Void System.Text.RegularExpressions.GroupCollection::System.Collections.IList.set_Item(System.Int32,System.Object)
extern void GroupCollection_System_Collections_IList_set_Item_mDDFE5D350953DB61758FE64D556670EA5A402C48 (void);
// 0x00000447 System.Void System.Text.RegularExpressions.GroupCollection::.ctor()
extern void GroupCollection__ctor_mB178F93E8DB52AA6412FF28E5FE50943E2BABFFB (void);
// 0x00000448 System.Void System.Text.RegularExpressions.GroupCollection/Enumerator::.ctor(System.Text.RegularExpressions.GroupCollection)
extern void Enumerator__ctor_m17B823D35315CED0509F6B4C5F04C148B9388DDD (void);
// 0x00000449 System.Boolean System.Text.RegularExpressions.GroupCollection/Enumerator::MoveNext()
extern void Enumerator_MoveNext_m1DDDE3FC6B83FF15ED8AA895DE1357C37B7241C5 (void);
// 0x0000044A System.Text.RegularExpressions.Group System.Text.RegularExpressions.GroupCollection/Enumerator::get_Current()
extern void Enumerator_get_Current_m6201905A1315214B04F5001307F52887F9161F43 (void);
// 0x0000044B System.Object System.Text.RegularExpressions.GroupCollection/Enumerator::System.Collections.IEnumerator.get_Current()
extern void Enumerator_System_Collections_IEnumerator_get_Current_m16F94921184B09EDC3D45E9CFE3F3FAA1E5D862D (void);
// 0x0000044C System.Void System.Text.RegularExpressions.GroupCollection/Enumerator::System.Collections.IEnumerator.Reset()
extern void Enumerator_System_Collections_IEnumerator_Reset_m5B7067CCF9E8139F34641618492A2DAACA5419B0 (void);
// 0x0000044D System.Void System.Text.RegularExpressions.GroupCollection/Enumerator::System.IDisposable.Dispose()
extern void Enumerator_System_IDisposable_Dispose_m1C77D8E3DFF1A034E9329E11854F11012F20AA69 (void);
// 0x0000044E System.Void System.Text.RegularExpressions.Match::.ctor(System.Text.RegularExpressions.Regex,System.Int32,System.String,System.Int32,System.Int32,System.Int32)
extern void Match__ctor_m2A6BD37E98680890114AC62CDD23E1821E80A53A (void);
// 0x0000044F System.Text.RegularExpressions.Match System.Text.RegularExpressions.Match::get_Empty()
extern void Match_get_Empty_m9565EFA0F267BAF2886447CB8169A654C5471918 (void);
// 0x00000450 System.Void System.Text.RegularExpressions.Match::Reset(System.Text.RegularExpressions.Regex,System.String,System.Int32,System.Int32,System.Int32)
extern void Match_Reset_m53A78F16760B7CD5CC8A104F6CD95CA255D888AA (void);
// 0x00000451 System.Text.RegularExpressions.Match System.Text.RegularExpressions.Match::NextMatch()
extern void Match_NextMatch_m4B7BA2B21E09FA0937806F41DDE3EE0F052C4052 (void);
// 0x00000452 System.ReadOnlySpan`1<System.Char> System.Text.RegularExpressions.Match::GroupToStringImpl(System.Int32)
extern void Match_GroupToStringImpl_mF3F2A3BC4EC33DD1FD35244E2412E05151AF6A1A (void);
// 0x00000453 System.ReadOnlySpan`1<System.Char> System.Text.RegularExpressions.Match::LastGroupToStringImpl()
extern void Match_LastGroupToStringImpl_m78C890F3547E00454BEB81BC939FE680AAEBB29A (void);
// 0x00000454 System.Void System.Text.RegularExpressions.Match::AddMatch(System.Int32,System.Int32,System.Int32)
extern void Match_AddMatch_m5F28F3255C64D67D56C1542CE32A9D324F96780E (void);
// 0x00000455 System.Void System.Text.RegularExpressions.Match::BalanceMatch(System.Int32)
extern void Match_BalanceMatch_m85EA44453E637DEBE24ABC7562BF91EE1222DBFD (void);
// 0x00000456 System.Void System.Text.RegularExpressions.Match::RemoveMatch(System.Int32)
extern void Match_RemoveMatch_m154DB10030027B3738903FFB50DE511D71656B04 (void);
// 0x00000457 System.Boolean System.Text.RegularExpressions.Match::IsMatched(System.Int32)
extern void Match_IsMatched_m9D6E0C0AB1F3E1E8E49E77E3A0A0ECCD7C2C4FBA (void);
// 0x00000458 System.Int32 System.Text.RegularExpressions.Match::MatchIndex(System.Int32)
extern void Match_MatchIndex_m97F06DECDDA8BD3E4D8DF6B9224B8E24C0764F35 (void);
// 0x00000459 System.Int32 System.Text.RegularExpressions.Match::MatchLength(System.Int32)
extern void Match_MatchLength_m5C93EA60B018A79D9D05C8E69D25835848471B01 (void);
// 0x0000045A System.Void System.Text.RegularExpressions.Match::Tidy(System.Int32)
extern void Match_Tidy_mF0A46109EAF0E91CBCA732985265E1DC20CA3F51 (void);
// 0x0000045B System.Void System.Text.RegularExpressions.Match::.cctor()
extern void Match__cctor_mAB49EF64F5132A336358E3F1922FF173EA10CE3C (void);
// 0x0000045C System.Void System.Text.RegularExpressions.Match::.ctor()
extern void Match__ctor_mBABEF8F11A3BC6DB713519819D135CC87AC1B4CD (void);
// 0x0000045D System.Void System.Text.RegularExpressions.MatchSparse::.ctor(System.Text.RegularExpressions.Regex,System.Collections.Hashtable,System.Int32,System.String,System.Int32,System.Int32,System.Int32)
extern void MatchSparse__ctor_m7CEA8A7F538C706527778E8E75003DD74AC2FD41 (void);
// 0x0000045E System.Text.RegularExpressions.RegexRunner System.Text.RegularExpressions.ExclusiveReference::Get()
extern void ExclusiveReference_Get_mCC2B9161946C9532001438C65A38E8D8FD56FB3C (void);
// 0x0000045F System.Void System.Text.RegularExpressions.ExclusiveReference::Release(System.Text.RegularExpressions.RegexRunner)
extern void ExclusiveReference_Release_m7A911FBF52436AA832188BF89CC558155BE09018 (void);
// 0x00000460 System.Void System.Text.RegularExpressions.ExclusiveReference::.ctor()
extern void ExclusiveReference__ctor_m41EB96FC1FA261CB792752B89F15E6513FEA1722 (void);
// 0x00000461 System.Text.RegularExpressions.Regex/CachedCodeEntry System.Text.RegularExpressions.Regex::GetCachedCode(System.Text.RegularExpressions.Regex/CachedCodeEntryKey,System.Boolean)
extern void Regex_GetCachedCode_m2039E73548B61FE55B6C8D85C00076DBA50332E6 (void);
// 0x00000462 System.Text.RegularExpressions.Regex/CachedCodeEntry System.Text.RegularExpressions.Regex::GetCachedCodeEntryInternal(System.Text.RegularExpressions.Regex/CachedCodeEntryKey,System.Boolean)
extern void Regex_GetCachedCodeEntryInternal_m9FE175C47BF909F7F223013004C0EA0DAD660C06 (void);
// 0x00000463 System.Void System.Text.RegularExpressions.Regex::FillCacheDictionary()
extern void Regex_FillCacheDictionary_mC47F317967E556D1F03A344B78B215356D604755 (void);
// 0x00000464 System.Boolean System.Text.RegularExpressions.Regex::TryGetCacheValue(System.Text.RegularExpressions.Regex/CachedCodeEntryKey,System.Text.RegularExpressions.Regex/CachedCodeEntry&)
extern void Regex_TryGetCacheValue_m8785E636E24F8F0C3E5A7CFE516978A434C99DEB (void);
// 0x00000465 System.Boolean System.Text.RegularExpressions.Regex::TryGetCacheValueSmall(System.Text.RegularExpressions.Regex/CachedCodeEntryKey,System.Text.RegularExpressions.Regex/CachedCodeEntry&)
extern void Regex_TryGetCacheValueSmall_mEA1FE67593D84A849F8DDFD9BB4144FF5BE18A64 (void);
// 0x00000466 System.Text.RegularExpressions.Regex/CachedCodeEntry System.Text.RegularExpressions.Regex::LookupCachedAndPromote(System.Text.RegularExpressions.Regex/CachedCodeEntryKey)
extern void Regex_LookupCachedAndPromote_m41760A936BAED48327B5E239894DA154B7832DD3 (void);
// 0x00000467 System.Boolean System.Text.RegularExpressions.Regex::IsMatch(System.String,System.String)
extern void Regex_IsMatch_m967E98137B140DFD845C5E1699D1BC8EF08CC76C (void);
// 0x00000468 System.Boolean System.Text.RegularExpressions.Regex::IsMatch(System.String,System.String,System.Text.RegularExpressions.RegexOptions,System.TimeSpan)
extern void Regex_IsMatch_m3709329406E39A80F61F4DD2CC9AB672835A3E81 (void);
// 0x00000469 System.Boolean System.Text.RegularExpressions.Regex::IsMatch(System.String)
extern void Regex_IsMatch_m7E96E666FBE7259D7638A3A6A21BE824D2406F49 (void);
// 0x0000046A System.Boolean System.Text.RegularExpressions.Regex::IsMatch(System.String,System.Int32)
extern void Regex_IsMatch_mC358D80573E765ECC19F9F5882664E3B1FA0966E (void);
// 0x0000046B System.Text.RegularExpressions.Match System.Text.RegularExpressions.Regex::Match(System.String)
extern void Regex_Match_m58565ECF23ACCD2CA77D6F10A6A182B03CF0FF84 (void);
// 0x0000046C System.Text.RegularExpressions.Match System.Text.RegularExpressions.Regex::Match(System.String,System.Int32)
extern void Regex_Match_mDF110993CAA26CD41AE9F5DB7F0C307B2680CED3 (void);
// 0x0000046D System.String System.Text.RegularExpressions.Regex::Replace(System.String,System.String,System.String)
extern void Regex_Replace_mA40B00220816EB1850F503AAB876812C726D8D66 (void);
// 0x0000046E System.String System.Text.RegularExpressions.Regex::Replace(System.String,System.String,System.String,System.Text.RegularExpressions.RegexOptions,System.TimeSpan)
extern void Regex_Replace_m09B7AAE77B831CF7E8B1F469D62EF719069370EB (void);
// 0x0000046F System.String System.Text.RegularExpressions.Regex::Replace(System.String,System.String)
extern void Regex_Replace_m8E802C10B5A591BEA61FC00EA8E4AF398D815A90 (void);
// 0x00000470 System.String System.Text.RegularExpressions.Regex::Replace(System.String,System.String,System.Int32,System.Int32)
extern void Regex_Replace_mD86A837354847D86EC562069D67567F9F707C9C4 (void);
// 0x00000471 System.Void System.Text.RegularExpressions.Regex::.cctor()
extern void Regex__cctor_mF29CE2FFB1941E7609C5B67C0236AC98003F675F (void);
// 0x00000472 System.Void System.Text.RegularExpressions.Regex::ValidateMatchTimeout(System.TimeSpan)
extern void Regex_ValidateMatchTimeout_mA50CD0CB49AB9B42B548900335CA6EABC30BD18F (void);
// 0x00000473 System.TimeSpan System.Text.RegularExpressions.Regex::InitDefaultMatchTimeout()
extern void Regex_InitDefaultMatchTimeout_mC441DFFACCC15C2710E85B95820DAB49AE9C47A7 (void);
// 0x00000474 System.Void System.Text.RegularExpressions.Regex::.ctor(System.String)
extern void Regex__ctor_m082970AA73B8236360F0CA651FA24A8D1EBF89CD (void);
// 0x00000475 System.Void System.Text.RegularExpressions.Regex::.ctor(System.String,System.Text.RegularExpressions.RegexOptions)
extern void Regex__ctor_mE3996C71B04A4A6845745D01C93B1D27423D0621 (void);
// 0x00000476 System.Void System.Text.RegularExpressions.Regex::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void Regex_System_Runtime_Serialization_ISerializable_GetObjectData_mD788EE38607BF5D618FDA920048707DF4CA4B99C (void);
// 0x00000477 System.Void System.Text.RegularExpressions.Regex::.ctor(System.String,System.Text.RegularExpressions.RegexOptions,System.TimeSpan,System.Boolean)
extern void Regex__ctor_mA8EBF2F80CC00D741BFD2FFD7DC9DB26FBCEE2A4 (void);
// 0x00000478 System.Boolean System.Text.RegularExpressions.Regex::get_RightToLeft()
extern void Regex_get_RightToLeft_m52C4DEE1DC94FC9531785B74111022F3BCC1DF4B (void);
// 0x00000479 System.String System.Text.RegularExpressions.Regex::ToString()
extern void Regex_ToString_m4982DE4C344F7D529666314646F071580A450D5B (void);
// 0x0000047A System.String System.Text.RegularExpressions.Regex::GroupNameFromNumber(System.Int32)
extern void Regex_GroupNameFromNumber_mC99A37419C4C0C9C3869BCDF055EDC72782D53FF (void);
// 0x0000047B System.Void System.Text.RegularExpressions.Regex::InitializeReferences()
extern void Regex_InitializeReferences_m55385AD102411A38BF4426C08F6FEC6E55E4D4D6 (void);
// 0x0000047C System.Text.RegularExpressions.Match System.Text.RegularExpressions.Regex::Run(System.Boolean,System.Int32,System.String,System.Int32,System.Int32,System.Int32)
extern void Regex_Run_m4DCD2202C599273C00D6052A77BDD2F39E2F5BF8 (void);
// 0x0000047D System.Boolean System.Text.RegularExpressions.Regex::UseOptionR()
extern void Regex_UseOptionR_mFAB8780E9F998BE60C4B1127D57DD627A3B5629C (void);
// 0x0000047E System.Boolean System.Text.RegularExpressions.Regex::UseOptionInvariant()
extern void Regex_UseOptionInvariant_mD51BCBA0BF4C1B5CEBE9423B4AAA93CD83569B60 (void);
// 0x0000047F System.Void System.Text.RegularExpressions.Regex/CachedCodeEntryKey::.ctor(System.Text.RegularExpressions.RegexOptions,System.String,System.String)
extern void CachedCodeEntryKey__ctor_mE505E40C97820B3FD140F6212F0B4ECF8745585F (void);
// 0x00000480 System.Boolean System.Text.RegularExpressions.Regex/CachedCodeEntryKey::Equals(System.Object)
extern void CachedCodeEntryKey_Equals_m59F40708B97179EA5C9019EA6A958D7AB8BC8CA2 (void);
// 0x00000481 System.Boolean System.Text.RegularExpressions.Regex/CachedCodeEntryKey::Equals(System.Text.RegularExpressions.Regex/CachedCodeEntryKey)
extern void CachedCodeEntryKey_Equals_m55F9ED97CB9AF357034A75C2FDB8498FC55D3AF0 (void);
// 0x00000482 System.Boolean System.Text.RegularExpressions.Regex/CachedCodeEntryKey::op_Equality(System.Text.RegularExpressions.Regex/CachedCodeEntryKey,System.Text.RegularExpressions.Regex/CachedCodeEntryKey)
extern void CachedCodeEntryKey_op_Equality_m5C48CEBA5D7F893F4FE88D06917EB738615DDCC6 (void);
// 0x00000483 System.Int32 System.Text.RegularExpressions.Regex/CachedCodeEntryKey::GetHashCode()
extern void CachedCodeEntryKey_GetHashCode_m253B53C100F840B987A487020F02184019D776DE (void);
// 0x00000484 System.Void System.Text.RegularExpressions.Regex/CachedCodeEntry::.ctor(System.Text.RegularExpressions.Regex/CachedCodeEntryKey,System.Collections.Hashtable,System.String[],System.Text.RegularExpressions.RegexCode,System.Collections.Hashtable,System.Int32,System.Text.RegularExpressions.ExclusiveReference,System.WeakReference`1<System.Text.RegularExpressions.RegexReplacement>)
extern void CachedCodeEntry__ctor_m14145ADFF67BA7C888673D806C24E28C96B83B2A (void);
// 0x00000485 System.Void System.Text.RegularExpressions.RegexBoyerMoore::.ctor(System.String,System.Boolean,System.Boolean,System.Globalization.CultureInfo)
extern void RegexBoyerMoore__ctor_mFDCB1AB420E718548B67DE42C233402955BBB189 (void);
// 0x00000486 System.Boolean System.Text.RegularExpressions.RegexBoyerMoore::MatchPattern(System.String,System.Int32)
extern void RegexBoyerMoore_MatchPattern_m0E84EAABBFCDA85484466A41A7B90187D3489E8C (void);
// 0x00000487 System.Boolean System.Text.RegularExpressions.RegexBoyerMoore::IsMatch(System.String,System.Int32,System.Int32,System.Int32)
extern void RegexBoyerMoore_IsMatch_mC09692237823CC6C74D4F5D8900D82CEC840FB47 (void);
// 0x00000488 System.Int32 System.Text.RegularExpressions.RegexBoyerMoore::Scan(System.String,System.Int32,System.Int32,System.Int32)
extern void RegexBoyerMoore_Scan_m86EB5A209D33DA3F4C7335F8FE133238666FA57B (void);
// 0x00000489 System.Void System.Text.RegularExpressions.RegexCharClass::.ctor()
extern void RegexCharClass__ctor_m165680E6EA10035E0630220C967BD772F5D6F52E (void);
// 0x0000048A System.Void System.Text.RegularExpressions.RegexCharClass::.ctor(System.Boolean,System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexCharClass/SingleRange>,System.Text.StringBuilder,System.Text.RegularExpressions.RegexCharClass)
extern void RegexCharClass__ctor_mFAD2AF3D7B0D4D3DC8BC850B8899C5C4D55F8B49 (void);
// 0x0000048B System.Boolean System.Text.RegularExpressions.RegexCharClass::get_CanMerge()
extern void RegexCharClass_get_CanMerge_mC093FE4BE7C1933BA9F146CF2A33CD729A965172 (void);
// 0x0000048C System.Void System.Text.RegularExpressions.RegexCharClass::set_Negate(System.Boolean)
extern void RegexCharClass_set_Negate_m8EA71CB8F671E99EF0B388CB520F44B8B6653A6B (void);
// 0x0000048D System.Void System.Text.RegularExpressions.RegexCharClass::AddChar(System.Char)
extern void RegexCharClass_AddChar_mED269B31D5C606032B3394A9B9305ED172D16746 (void);
// 0x0000048E System.Void System.Text.RegularExpressions.RegexCharClass::AddCharClass(System.Text.RegularExpressions.RegexCharClass)
extern void RegexCharClass_AddCharClass_mDE8C70A6A99B2223DE4EA36A4753A13B5EEDE7BB (void);
// 0x0000048F System.Void System.Text.RegularExpressions.RegexCharClass::AddSet(System.String)
extern void RegexCharClass_AddSet_mFC202CC46631017C7D64ED17EAA9C786C5C7CE83 (void);
// 0x00000490 System.Void System.Text.RegularExpressions.RegexCharClass::AddSubtraction(System.Text.RegularExpressions.RegexCharClass)
extern void RegexCharClass_AddSubtraction_m4EF34AFB93C001C6A4521E1F1C5F4B54833E8A12 (void);
// 0x00000491 System.Void System.Text.RegularExpressions.RegexCharClass::AddRange(System.Char,System.Char)
extern void RegexCharClass_AddRange_mB1188A62712FE4ABB7F5DB28645351E375B5C6A7 (void);
// 0x00000492 System.Void System.Text.RegularExpressions.RegexCharClass::AddCategoryFromName(System.String,System.Boolean,System.Boolean,System.String)
extern void RegexCharClass_AddCategoryFromName_m032AED661665AE30D7F3F712914E2937F6E4FF97 (void);
// 0x00000493 System.Void System.Text.RegularExpressions.RegexCharClass::AddCategory(System.String)
extern void RegexCharClass_AddCategory_m3A3C78511F279FD0CF67A3EE36F59A548A37269A (void);
// 0x00000494 System.Void System.Text.RegularExpressions.RegexCharClass::AddLowercase(System.Globalization.CultureInfo)
extern void RegexCharClass_AddLowercase_mB87FC245CC837E582370E5410A6B40B23BEAC0BE (void);
// 0x00000495 System.Void System.Text.RegularExpressions.RegexCharClass::AddLowercaseRange(System.Char,System.Char,System.Globalization.CultureInfo)
extern void RegexCharClass_AddLowercaseRange_m75E07C55A082FE5B2926997F2564B611DF61A343 (void);
// 0x00000496 System.Void System.Text.RegularExpressions.RegexCharClass::AddWord(System.Boolean,System.Boolean)
extern void RegexCharClass_AddWord_mAED995BB6613893E0950C5895515204F1E95D152 (void);
// 0x00000497 System.Void System.Text.RegularExpressions.RegexCharClass::AddSpace(System.Boolean,System.Boolean)
extern void RegexCharClass_AddSpace_mD1BF3AD215D69EC6CF0E49A359F7FC709FD6FBB6 (void);
// 0x00000498 System.Void System.Text.RegularExpressions.RegexCharClass::AddDigit(System.Boolean,System.Boolean,System.String)
extern void RegexCharClass_AddDigit_m04C1001F7E8DA85CB2D262ED403BA9BD215A7A8E (void);
// 0x00000499 System.Char System.Text.RegularExpressions.RegexCharClass::SingletonChar(System.String)
extern void RegexCharClass_SingletonChar_m11683F48B3AA3E0B5CD5849C40ADDCECC91BFF10 (void);
// 0x0000049A System.Boolean System.Text.RegularExpressions.RegexCharClass::IsMergeable(System.String)
extern void RegexCharClass_IsMergeable_mD017475B43D0621F15ECAF5006410D4D0CCC46AD (void);
// 0x0000049B System.Boolean System.Text.RegularExpressions.RegexCharClass::IsEmpty(System.String)
extern void RegexCharClass_IsEmpty_mDD55987C8F4B887716E46869730F5E1E31593A44 (void);
// 0x0000049C System.Boolean System.Text.RegularExpressions.RegexCharClass::IsSingleton(System.String)
extern void RegexCharClass_IsSingleton_mF23CB7003B555ADB4C5035A1B3A218A8ED847D12 (void);
// 0x0000049D System.Boolean System.Text.RegularExpressions.RegexCharClass::IsSingletonInverse(System.String)
extern void RegexCharClass_IsSingletonInverse_m62E7E31998BEBC824A4EED3CEE5A2D3FB3BF5831 (void);
// 0x0000049E System.Boolean System.Text.RegularExpressions.RegexCharClass::IsSubtraction(System.String)
extern void RegexCharClass_IsSubtraction_m069CC20713C5757A8AAD9BA556F07C8E042F3BE8 (void);
// 0x0000049F System.Boolean System.Text.RegularExpressions.RegexCharClass::IsNegated(System.String)
extern void RegexCharClass_IsNegated_m66E1348EBCAAD253AEEA5EDC3F9A93E84A433947 (void);
// 0x000004A0 System.Boolean System.Text.RegularExpressions.RegexCharClass::IsECMAWordChar(System.Char)
extern void RegexCharClass_IsECMAWordChar_m3A2920E6309D58F982B8B7AE20D9166601DDCA0B (void);
// 0x000004A1 System.Boolean System.Text.RegularExpressions.RegexCharClass::IsWordChar(System.Char)
extern void RegexCharClass_IsWordChar_m9FEFF0FC22277053D7A4404FD919C7F41733DB94 (void);
// 0x000004A2 System.Boolean System.Text.RegularExpressions.RegexCharClass::CharInClass(System.Char,System.String)
extern void RegexCharClass_CharInClass_mAF6EE8C936383EB630F17C8C268F10BB8C10BC83 (void);
// 0x000004A3 System.Boolean System.Text.RegularExpressions.RegexCharClass::CharInClassRecursive(System.Char,System.String,System.Int32)
extern void RegexCharClass_CharInClassRecursive_m7B2DC6EF52CFA494C26DC8AA976F3A06DDAF22FC (void);
// 0x000004A4 System.Boolean System.Text.RegularExpressions.RegexCharClass::CharInClassInternal(System.Char,System.String,System.Int32,System.Int32,System.Int32)
extern void RegexCharClass_CharInClassInternal_m166B4DDEB0553E88B58A2F3E859CA2D29995CF54 (void);
// 0x000004A5 System.Boolean System.Text.RegularExpressions.RegexCharClass::CharInCategory(System.Char,System.String,System.Int32,System.Int32,System.Int32)
extern void RegexCharClass_CharInCategory_m1B20968374CE9C35FE81D172C3325E37433897E1 (void);
// 0x000004A6 System.Boolean System.Text.RegularExpressions.RegexCharClass::CharInCategoryGroup(System.Char,System.Globalization.UnicodeCategory,System.String,System.Int32&)
extern void RegexCharClass_CharInCategoryGroup_mDDAA3F1975AC1A7527137AE6951121F381703D48 (void);
// 0x000004A7 System.String System.Text.RegularExpressions.RegexCharClass::NegateCategory(System.String)
extern void RegexCharClass_NegateCategory_m1C615694A5F8EECBBE4C09BA293454D856F52F50 (void);
// 0x000004A8 System.Text.RegularExpressions.RegexCharClass System.Text.RegularExpressions.RegexCharClass::Parse(System.String)
extern void RegexCharClass_Parse_m4766E7E53BF572FD9815DEEBD9831380C33C796A (void);
// 0x000004A9 System.Text.RegularExpressions.RegexCharClass System.Text.RegularExpressions.RegexCharClass::ParseRecursive(System.String,System.Int32)
extern void RegexCharClass_ParseRecursive_m6ABF2F2B8B8C5A559866EBF3FBA305247DE4CCF6 (void);
// 0x000004AA System.Int32 System.Text.RegularExpressions.RegexCharClass::RangeCount()
extern void RegexCharClass_RangeCount_m36A38A50CD2E3E61B5F55D95562BD92923F981F1 (void);
// 0x000004AB System.String System.Text.RegularExpressions.RegexCharClass::ToStringClass()
extern void RegexCharClass_ToStringClass_m6B0B92A0A525001CD78A6C2A766295268E0FED57 (void);
// 0x000004AC System.Text.RegularExpressions.RegexCharClass/SingleRange System.Text.RegularExpressions.RegexCharClass::GetRangeAt(System.Int32)
extern void RegexCharClass_GetRangeAt_m67B8198B09F344044D8F742C5C9869034C4A3128 (void);
// 0x000004AD System.Void System.Text.RegularExpressions.RegexCharClass::Canonicalize()
extern void RegexCharClass_Canonicalize_m77EEBAD046B3EAD5110DD472434BDE16851C7A9E (void);
// 0x000004AE System.String System.Text.RegularExpressions.RegexCharClass::SetFromProperty(System.String,System.Boolean,System.String)
extern void RegexCharClass_SetFromProperty_m75FD04D8F9EA5FD996E4C8B4A2DF45BEEE6BABC5 (void);
// 0x000004AF System.Void System.Text.RegularExpressions.RegexCharClass::.cctor()
extern void RegexCharClass__cctor_m70495E564EC18CDEF2A2687F8012C96E571A0631 (void);
// 0x000004B0 System.Void System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::.ctor(System.Char,System.Char,System.Int32,System.Int32)
extern void LowerCaseMapping__ctor_m06D0A21D5D72E39535F82076EF046F98C62D7D5A (void);
// 0x000004B1 System.Void System.Text.RegularExpressions.RegexCharClass/SingleRangeComparer::.ctor()
extern void SingleRangeComparer__ctor_m9B430EFE70974D964D73913D81E7C6C9236BBCCE (void);
// 0x000004B2 System.Int32 System.Text.RegularExpressions.RegexCharClass/SingleRangeComparer::Compare(System.Text.RegularExpressions.RegexCharClass/SingleRange,System.Text.RegularExpressions.RegexCharClass/SingleRange)
extern void SingleRangeComparer_Compare_m917FDE86E5F31A755D07077EA095F8B7E27E130B (void);
// 0x000004B3 System.Void System.Text.RegularExpressions.RegexCharClass/SingleRangeComparer::.cctor()
extern void SingleRangeComparer__cctor_m512F77BDA73732EB8D39BB056D00CF9B200D5D1C (void);
// 0x000004B4 System.Void System.Text.RegularExpressions.RegexCharClass/SingleRange::.ctor(System.Char,System.Char)
extern void SingleRange__ctor_m14FD490AFB119E412298554D31FADDCB9465511E (void);
// 0x000004B5 System.Void System.Text.RegularExpressions.RegexCode::.ctor(System.Int32[],System.Collections.Generic.List`1<System.String>,System.Int32,System.Collections.Hashtable,System.Int32,System.Text.RegularExpressions.RegexBoyerMoore,System.Nullable`1<System.Text.RegularExpressions.RegexPrefix>,System.Int32,System.Boolean)
extern void RegexCode__ctor_mA50EE1CFA1A050BFC45E135DAB7E2704049512E0 (void);
// 0x000004B6 System.Boolean System.Text.RegularExpressions.RegexCode::OpcodeBacktracks(System.Int32)
extern void RegexCode_OpcodeBacktracks_m77D0680FD94F152F85C6CF8724A5EBFFE07A1397 (void);
// 0x000004B7 System.Void System.Text.RegularExpressions.RegexFCD::.ctor(System.Span`1<System.Int32>)
extern void RegexFCD__ctor_mE1C0A21180069D25EEF6F3702938ABB087F35C0C (void);
// 0x000004B8 System.Nullable`1<System.Text.RegularExpressions.RegexPrefix> System.Text.RegularExpressions.RegexFCD::FirstChars(System.Text.RegularExpressions.RegexTree)
extern void RegexFCD_FirstChars_mD151F8EF8DD64C9E1960D6C4E79E65A6766322CA (void);
// 0x000004B9 System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexFCD::Prefix(System.Text.RegularExpressions.RegexTree)
extern void RegexFCD_Prefix_m6D786604F799DF1EEF509BEA577E2DC50C75FB6E (void);
// 0x000004BA System.Int32 System.Text.RegularExpressions.RegexFCD::Anchors(System.Text.RegularExpressions.RegexTree)
extern void RegexFCD_Anchors_mF19C2B50200C6D7E38566E5C4854DB04EADFF5CE (void);
// 0x000004BB System.Int32 System.Text.RegularExpressions.RegexFCD::AnchorFromType(System.Int32)
extern void RegexFCD_AnchorFromType_mF944273F420E8D43F4F9EC93950E3797B4C10A47 (void);
// 0x000004BC System.Void System.Text.RegularExpressions.RegexFCD::PushInt(System.Int32)
extern void RegexFCD_PushInt_m0B38AFDAF0D14FF1E41AC90838B40C4C866D8875 (void);
// 0x000004BD System.Boolean System.Text.RegularExpressions.RegexFCD::IntIsEmpty()
extern void RegexFCD_IntIsEmpty_m05E3C6FF40B69E98D90CF1C12DF3F32AEC800B5E (void);
// 0x000004BE System.Int32 System.Text.RegularExpressions.RegexFCD::PopInt()
extern void RegexFCD_PopInt_m1DF4F5E6B4BA9E15AFF2B3C44D1A233EA46EFCD0 (void);
// 0x000004BF System.Void System.Text.RegularExpressions.RegexFCD::PushFC(System.Text.RegularExpressions.RegexFC)
extern void RegexFCD_PushFC_m1396C7FD5ACE3D28076FAFF667D9A0D169F8B675 (void);
// 0x000004C0 System.Boolean System.Text.RegularExpressions.RegexFCD::FCIsEmpty()
extern void RegexFCD_FCIsEmpty_m749E6423466EAD51E38445AC91E76DE31258574F (void);
// 0x000004C1 System.Text.RegularExpressions.RegexFC System.Text.RegularExpressions.RegexFCD::PopFC()
extern void RegexFCD_PopFC_mB83F2100E8EBACF17904BD5CACB57525D2966628 (void);
// 0x000004C2 System.Text.RegularExpressions.RegexFC System.Text.RegularExpressions.RegexFCD::TopFC()
extern void RegexFCD_TopFC_mAE41171B96938FD879E0C0AE7C0D06645AAA84CD (void);
// 0x000004C3 System.Void System.Text.RegularExpressions.RegexFCD::Dispose()
extern void RegexFCD_Dispose_m0B04A2F3B819FA7BFAF0AECCBF6D22E6D9789C07 (void);
// 0x000004C4 System.Text.RegularExpressions.RegexFC System.Text.RegularExpressions.RegexFCD::RegexFCFromRegexTree(System.Text.RegularExpressions.RegexTree)
extern void RegexFCD_RegexFCFromRegexTree_mD5A53219905969B914CE27404553A4360DA78500 (void);
// 0x000004C5 System.Void System.Text.RegularExpressions.RegexFCD::SkipChild()
extern void RegexFCD_SkipChild_m5ACE619BAC10DD5DFE755655DB6151E7DB0885FD (void);
// 0x000004C6 System.Void System.Text.RegularExpressions.RegexFCD::CalculateFC(System.Int32,System.Text.RegularExpressions.RegexNode,System.Int32)
extern void RegexFCD_CalculateFC_m16F7EB523C02B530A741A7F6364F2E46865DCC91 (void);
// 0x000004C7 System.Void System.Text.RegularExpressions.RegexFC::.ctor(System.Boolean)
extern void RegexFC__ctor_m18E889B95C8C6014582FD2C6991F789190519F0A (void);
// 0x000004C8 System.Void System.Text.RegularExpressions.RegexFC::.ctor(System.Char,System.Boolean,System.Boolean,System.Boolean)
extern void RegexFC__ctor_m073D88DF033828EB626DBEA191FE78296CADB57D (void);
// 0x000004C9 System.Void System.Text.RegularExpressions.RegexFC::.ctor(System.String,System.Boolean,System.Boolean)
extern void RegexFC__ctor_m69E0058CD2F4A36F14589250B3F5CBAD7025D6F5 (void);
// 0x000004CA System.Boolean System.Text.RegularExpressions.RegexFC::AddFC(System.Text.RegularExpressions.RegexFC,System.Boolean)
extern void RegexFC_AddFC_m61D0F8CDE635ED3A48AEA069A7700B100D2AE851 (void);
// 0x000004CB System.Boolean System.Text.RegularExpressions.RegexFC::get_CaseInsensitive()
extern void RegexFC_get_CaseInsensitive_m4FCD7C7545925463ADA4055E9FE62D5A452E2102 (void);
// 0x000004CC System.Void System.Text.RegularExpressions.RegexFC::set_CaseInsensitive(System.Boolean)
extern void RegexFC_set_CaseInsensitive_mF9A3745D1575160C2F2B1F7F9EB5541BD0C9FF4B (void);
// 0x000004CD System.String System.Text.RegularExpressions.RegexFC::GetFirstChars(System.Globalization.CultureInfo)
extern void RegexFC_GetFirstChars_m6BAC398ABDCA5AF58CBE678143C9792611A2A3E8 (void);
// 0x000004CE System.Void System.Text.RegularExpressions.RegexInterpreter::.ctor(System.Text.RegularExpressions.RegexCode,System.Globalization.CultureInfo)
extern void RegexInterpreter__ctor_mB81896A8292574F7532CF707F74A1AC1DECEA9FF (void);
// 0x000004CF System.Void System.Text.RegularExpressions.RegexInterpreter::InitTrackCount()
extern void RegexInterpreter_InitTrackCount_mFFFBE9294C6FCB854D4502362473C40A5ABBBFC4 (void);
// 0x000004D0 System.Void System.Text.RegularExpressions.RegexInterpreter::Advance(System.Int32)
extern void RegexInterpreter_Advance_m31E2455F84B61536DE535BE51D4D0B42DC33857B (void);
// 0x000004D1 System.Void System.Text.RegularExpressions.RegexInterpreter::Goto(System.Int32)
extern void RegexInterpreter_Goto_m9BF559085E75D4AA7457FAD60E879AB96BB4AEC9 (void);
// 0x000004D2 System.Void System.Text.RegularExpressions.RegexInterpreter::Textto(System.Int32)
extern void RegexInterpreter_Textto_m66DE4B949DBAA02672CFDE27162BCED542038850 (void);
// 0x000004D3 System.Void System.Text.RegularExpressions.RegexInterpreter::Trackto(System.Int32)
extern void RegexInterpreter_Trackto_mDFDFF0401BC8F2B2D26AEC65B54B963F84A089AE (void);
// 0x000004D4 System.Int32 System.Text.RegularExpressions.RegexInterpreter::Textstart()
extern void RegexInterpreter_Textstart_mDF185C6B3E245A2307A8B0B9F5FE5DF8C8C9936E (void);
// 0x000004D5 System.Int32 System.Text.RegularExpressions.RegexInterpreter::Textpos()
extern void RegexInterpreter_Textpos_m6E20619B1C7F8B77D7A57A8A6939C24011808DEC (void);
// 0x000004D6 System.Int32 System.Text.RegularExpressions.RegexInterpreter::Trackpos()
extern void RegexInterpreter_Trackpos_m6BDE792A7333FADB2533591B88211FD77E8F0401 (void);
// 0x000004D7 System.Void System.Text.RegularExpressions.RegexInterpreter::TrackPush()
extern void RegexInterpreter_TrackPush_mC82959F407482A4713C419296455A18F5D861E93 (void);
// 0x000004D8 System.Void System.Text.RegularExpressions.RegexInterpreter::TrackPush(System.Int32)
extern void RegexInterpreter_TrackPush_mA225A4A0302F57CE82842189DCB8895A9AAEDB3D (void);
// 0x000004D9 System.Void System.Text.RegularExpressions.RegexInterpreter::TrackPush(System.Int32,System.Int32)
extern void RegexInterpreter_TrackPush_m94D69F6CC8E122478F4EADE04FB7FC9482CEC878 (void);
// 0x000004DA System.Void System.Text.RegularExpressions.RegexInterpreter::TrackPush(System.Int32,System.Int32,System.Int32)
extern void RegexInterpreter_TrackPush_m606F28BD60B479F0FD3073F8B5627836DB89230E (void);
// 0x000004DB System.Void System.Text.RegularExpressions.RegexInterpreter::TrackPush2(System.Int32)
extern void RegexInterpreter_TrackPush2_m5131F1DCBCA4D4D2231B44A85B08DE319BE31C9F (void);
// 0x000004DC System.Void System.Text.RegularExpressions.RegexInterpreter::TrackPush2(System.Int32,System.Int32)
extern void RegexInterpreter_TrackPush2_m18FF10B704A6586A18D9AB21D28737DC32DA2797 (void);
// 0x000004DD System.Void System.Text.RegularExpressions.RegexInterpreter::Backtrack()
extern void RegexInterpreter_Backtrack_m8D227FAE1EFD91174D43FF070D9200291941E593 (void);
// 0x000004DE System.Void System.Text.RegularExpressions.RegexInterpreter::SetOperator(System.Int32)
extern void RegexInterpreter_SetOperator_m693B374C6827D36CE755EFFDA53E245FB04652EC (void);
// 0x000004DF System.Void System.Text.RegularExpressions.RegexInterpreter::TrackPop()
extern void RegexInterpreter_TrackPop_m2FE356634C8337D8B657D30790588E4A4D7DECC1 (void);
// 0x000004E0 System.Void System.Text.RegularExpressions.RegexInterpreter::TrackPop(System.Int32)
extern void RegexInterpreter_TrackPop_mD3125258D278581E337C822F3C9847233E06224B (void);
// 0x000004E1 System.Int32 System.Text.RegularExpressions.RegexInterpreter::TrackPeek()
extern void RegexInterpreter_TrackPeek_m8D2CDC071ADBBC028FBAA364E2C18BD19FD6D861 (void);
// 0x000004E2 System.Int32 System.Text.RegularExpressions.RegexInterpreter::TrackPeek(System.Int32)
extern void RegexInterpreter_TrackPeek_m45C4F44554EC086C01B03C054BF6A94105D39699 (void);
// 0x000004E3 System.Void System.Text.RegularExpressions.RegexInterpreter::StackPush(System.Int32)
extern void RegexInterpreter_StackPush_m9BC14D7DD0FAF99AF230B436F32557E056F8FB60 (void);
// 0x000004E4 System.Void System.Text.RegularExpressions.RegexInterpreter::StackPush(System.Int32,System.Int32)
extern void RegexInterpreter_StackPush_m7741DC1BAEE69DFB68E1D60A9FC39C718829DF0F (void);
// 0x000004E5 System.Void System.Text.RegularExpressions.RegexInterpreter::StackPop()
extern void RegexInterpreter_StackPop_m3284B40674B96F317D05BD2616AAA80181E0AEFF (void);
// 0x000004E6 System.Void System.Text.RegularExpressions.RegexInterpreter::StackPop(System.Int32)
extern void RegexInterpreter_StackPop_mAEBB71FEDEADC065C5FA19E7C2C0FF0E82CEE681 (void);
// 0x000004E7 System.Int32 System.Text.RegularExpressions.RegexInterpreter::StackPeek()
extern void RegexInterpreter_StackPeek_m16057890EE30C3383227F21632C02C6BEF981991 (void);
// 0x000004E8 System.Int32 System.Text.RegularExpressions.RegexInterpreter::StackPeek(System.Int32)
extern void RegexInterpreter_StackPeek_mAFBD38869C8A829D6ACF534643B8A6C9863A10D8 (void);
// 0x000004E9 System.Int32 System.Text.RegularExpressions.RegexInterpreter::Operator()
extern void RegexInterpreter_Operator_m3BD9AD7DCDA4E84DE4821DA786E1B1483E677B58 (void);
// 0x000004EA System.Int32 System.Text.RegularExpressions.RegexInterpreter::Operand(System.Int32)
extern void RegexInterpreter_Operand_m09F048ED7BBB36BEFC5023D420323E37A97C9560 (void);
// 0x000004EB System.Int32 System.Text.RegularExpressions.RegexInterpreter::Leftchars()
extern void RegexInterpreter_Leftchars_m6FD5AA399927D1767633B24AE234D596CBDD4002 (void);
// 0x000004EC System.Int32 System.Text.RegularExpressions.RegexInterpreter::Rightchars()
extern void RegexInterpreter_Rightchars_m0D684BDADC810F61B803D30D121506833AD07CD7 (void);
// 0x000004ED System.Int32 System.Text.RegularExpressions.RegexInterpreter::Bump()
extern void RegexInterpreter_Bump_mD5E6B4A80D6C620EDD37A5C04156ADF7C27EE9E1 (void);
// 0x000004EE System.Int32 System.Text.RegularExpressions.RegexInterpreter::Forwardchars()
extern void RegexInterpreter_Forwardchars_mE97F35195F994160F951E3C035F9E0C35EB739C6 (void);
// 0x000004EF System.Char System.Text.RegularExpressions.RegexInterpreter::Forwardcharnext()
extern void RegexInterpreter_Forwardcharnext_m78266C74DAAFD2D09050F148FFDA0A50656AC4C1 (void);
// 0x000004F0 System.Boolean System.Text.RegularExpressions.RegexInterpreter::Stringmatch(System.String)
extern void RegexInterpreter_Stringmatch_m89AE288DB294AC0146C7772193CAD3B702849E54 (void);
// 0x000004F1 System.Boolean System.Text.RegularExpressions.RegexInterpreter::Refmatch(System.Int32,System.Int32)
extern void RegexInterpreter_Refmatch_m457B37C3E3B0671E7E89B7F6C16396D36EB32F68 (void);
// 0x000004F2 System.Void System.Text.RegularExpressions.RegexInterpreter::Backwardnext()
extern void RegexInterpreter_Backwardnext_m0C573AFAD3FAEBED37991618A232ACB169F524FF (void);
// 0x000004F3 System.Char System.Text.RegularExpressions.RegexInterpreter::CharAt(System.Int32)
extern void RegexInterpreter_CharAt_m9EFCB7CE5FB311B6C061AA996705CD3D402F8E8C (void);
// 0x000004F4 System.Boolean System.Text.RegularExpressions.RegexInterpreter::FindFirstChar()
extern void RegexInterpreter_FindFirstChar_m87D986F0502AEEFC4F13EBD0EAD891C495C4845F (void);
// 0x000004F5 System.Void System.Text.RegularExpressions.RegexInterpreter::Go()
extern void RegexInterpreter_Go_m1CF26F8D95231F3B2235EA98AFB286C3F1629BED (void);
// 0x000004F6 System.Void System.Text.RegularExpressions.RegexMatchTimeoutException::.ctor(System.String,System.String,System.TimeSpan)
extern void RegexMatchTimeoutException__ctor_m24286DF7231B227E3A8B3C31AE33447361971005 (void);
// 0x000004F7 System.Void System.Text.RegularExpressions.RegexMatchTimeoutException::.ctor()
extern void RegexMatchTimeoutException__ctor_mA8C497C6700E1233B4953A8AA7B8F57C8682C410 (void);
// 0x000004F8 System.Void System.Text.RegularExpressions.RegexMatchTimeoutException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void RegexMatchTimeoutException__ctor_m7DE41C173C1A57CEA5D015FF63DDFCB70CFF4FBF (void);
// 0x000004F9 System.Void System.Text.RegularExpressions.RegexMatchTimeoutException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void RegexMatchTimeoutException_System_Runtime_Serialization_ISerializable_GetObjectData_m6F530DBA34FE5CBECBCD322C8FBD4AE28054688C (void);
// 0x000004FA System.String System.Text.RegularExpressions.RegexMatchTimeoutException::get_Input()
extern void RegexMatchTimeoutException_get_Input_mA4DCA4F3C9692ACDC45FBEB683EE4165C84E4073 (void);
// 0x000004FB System.String System.Text.RegularExpressions.RegexMatchTimeoutException::get_Pattern()
extern void RegexMatchTimeoutException_get_Pattern_m8F12503E2B66F34B8E8EFE46971C169EA1BFBAB2 (void);
// 0x000004FC System.TimeSpan System.Text.RegularExpressions.RegexMatchTimeoutException::get_MatchTimeout()
extern void RegexMatchTimeoutException_get_MatchTimeout_m12D9D88EFFA06EAC0FF06C81ECF29BAE0F906087 (void);
// 0x000004FD System.Void System.Text.RegularExpressions.RegexNode::.ctor(System.Int32,System.Text.RegularExpressions.RegexOptions)
extern void RegexNode__ctor_mAE4AC160F21E5DF26B99B74753BCFB9EF0951B2B (void);
// 0x000004FE System.Void System.Text.RegularExpressions.RegexNode::.ctor(System.Int32,System.Text.RegularExpressions.RegexOptions,System.Char)
extern void RegexNode__ctor_mFA693535F5B9610F075D30F19626A8AFFA879D7B (void);
// 0x000004FF System.Void System.Text.RegularExpressions.RegexNode::.ctor(System.Int32,System.Text.RegularExpressions.RegexOptions,System.String)
extern void RegexNode__ctor_m00D0AE9AB07549681FB13D96CC0AA20FED8CE6EA (void);
// 0x00000500 System.Void System.Text.RegularExpressions.RegexNode::.ctor(System.Int32,System.Text.RegularExpressions.RegexOptions,System.Int32)
extern void RegexNode__ctor_m3352B98BE0DAD220AB653F929BD6D95689B5E07F (void);
// 0x00000501 System.Void System.Text.RegularExpressions.RegexNode::.ctor(System.Int32,System.Text.RegularExpressions.RegexOptions,System.Int32,System.Int32)
extern void RegexNode__ctor_m5ECC62B9C3918CD043542E5BEAAD39A06158E27A (void);
// 0x00000502 System.Boolean System.Text.RegularExpressions.RegexNode::UseOptionR()
extern void RegexNode_UseOptionR_mEBD872419C386D7C7802D2A6D2187E7B6D1D1641 (void);
// 0x00000503 System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::ReverseLeft()
extern void RegexNode_ReverseLeft_m251846220299FD022068361A09B9A7A4CF67A586 (void);
// 0x00000504 System.Void System.Text.RegularExpressions.RegexNode::MakeRep(System.Int32,System.Int32,System.Int32)
extern void RegexNode_MakeRep_mFDCFC9F5DA5DCBFC66D6F00A8E08A602F00C5F48 (void);
// 0x00000505 System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::Reduce()
extern void RegexNode_Reduce_m9D14953F47CD1D924C3EEB9D0287D7F257AD2D98 (void);
// 0x00000506 System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::StripEnation(System.Int32)
extern void RegexNode_StripEnation_m2BACBC29E58CEC452B91534BCFA2A21639712B61 (void);
// 0x00000507 System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::ReduceGroup()
extern void RegexNode_ReduceGroup_m62F5B20C564AC8A8E8C1C9E00684A8BB9A988E3B (void);
// 0x00000508 System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::ReduceRep()
extern void RegexNode_ReduceRep_m85B3760500D732A04B60CD3F247CE8ECA1E6F8C1 (void);
// 0x00000509 System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::ReduceSet()
extern void RegexNode_ReduceSet_mD307DD149936830074467A0F24C2E20032F708E9 (void);
// 0x0000050A System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::ReduceAlternation()
extern void RegexNode_ReduceAlternation_m193F3C5A073401BA987E5AB7D35F59B1FC902341 (void);
// 0x0000050B System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::ReduceConcatenation()
extern void RegexNode_ReduceConcatenation_m95EE4F324360AC71D52EDEC1C53F6F81D73C67C3 (void);
// 0x0000050C System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::MakeQuantifier(System.Boolean,System.Int32,System.Int32)
extern void RegexNode_MakeQuantifier_mA6A10E1AEF5AA80F82BD1BE6D28E62259208B5BC (void);
// 0x0000050D System.Void System.Text.RegularExpressions.RegexNode::AddChild(System.Text.RegularExpressions.RegexNode)
extern void RegexNode_AddChild_m1A2B3A7445927E39341C233C0E533D77FC9517A8 (void);
// 0x0000050E System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexNode::Child(System.Int32)
extern void RegexNode_Child_m6F5C25EE5349D1BE0D65C89475FB0C42A15BFF27 (void);
// 0x0000050F System.Int32 System.Text.RegularExpressions.RegexNode::ChildCount()
extern void RegexNode_ChildCount_m833AF69D2711D8EEC5E50FD666A61F428A12396D (void);
// 0x00000510 System.Int32 System.Text.RegularExpressions.RegexNode::Type()
extern void RegexNode_Type_mDE9685FD99646016D0D6038C4509C6392B10F019 (void);
// 0x00000511 System.Text.RegularExpressions.RegexTree System.Text.RegularExpressions.RegexParser::Parse(System.String,System.Text.RegularExpressions.RegexOptions)
extern void RegexParser_Parse_m5AE448041BC21B17EF68A2039991EC85F91AA9BB (void);
// 0x00000512 System.Text.RegularExpressions.RegexReplacement System.Text.RegularExpressions.RegexParser::ParseReplacement(System.String,System.Collections.Hashtable,System.Int32,System.Collections.Hashtable,System.Text.RegularExpressions.RegexOptions)
extern void RegexParser_ParseReplacement_m721243B3049725DF4FCCBE8F1861CC47BAF380CD (void);
// 0x00000513 System.Void System.Text.RegularExpressions.RegexParser::.ctor(System.Globalization.CultureInfo)
extern void RegexParser__ctor_m99DC2B2C4B64E7298BBF49AD862BFD12007E38E4 (void);
// 0x00000514 System.Void System.Text.RegularExpressions.RegexParser::SetPattern(System.String)
extern void RegexParser_SetPattern_m852D668CE45FBCAA3A6ACC415B94BDD0A7190265 (void);
// 0x00000515 System.Void System.Text.RegularExpressions.RegexParser::Reset(System.Text.RegularExpressions.RegexOptions)
extern void RegexParser_Reset_m426A8C90F4D8065486802AE7AC9FB07FC599EB01 (void);
// 0x00000516 System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::ScanRegex()
extern void RegexParser_ScanRegex_m3CD95B8EAE4F2E8ECF3461F199B04A021C77231D (void);
// 0x00000517 System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::ScanReplacement()
extern void RegexParser_ScanReplacement_mC27972ADC38D857F44851CE642162928ED4D12D6 (void);
// 0x00000518 System.Text.RegularExpressions.RegexCharClass System.Text.RegularExpressions.RegexParser::ScanCharClass(System.Boolean,System.Boolean)
extern void RegexParser_ScanCharClass_m01448F1178653B15083659F29014BB87A6E2295D (void);
// 0x00000519 System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::ScanGroupOpen()
extern void RegexParser_ScanGroupOpen_m5ABFBB28982A5097B2D560091D2AC12EA8B028F7 (void);
// 0x0000051A System.Void System.Text.RegularExpressions.RegexParser::ScanBlank()
extern void RegexParser_ScanBlank_m4C1B18EC7A5DF860363CA17E69B20A00023B5E82 (void);
// 0x0000051B System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::ScanBackslash(System.Boolean)
extern void RegexParser_ScanBackslash_mAAE11106CB78789803468679894E1D7AE6B0C92E (void);
// 0x0000051C System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::ScanBasicBackslash(System.Boolean)
extern void RegexParser_ScanBasicBackslash_mB0F1728AD89C8F196601B3110F0BA807FBC8FCEC (void);
// 0x0000051D System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::ScanDollar()
extern void RegexParser_ScanDollar_mB8FD395274F630278543211B2BB583AA5B03D96A (void);
// 0x0000051E System.String System.Text.RegularExpressions.RegexParser::ScanCapname()
extern void RegexParser_ScanCapname_mC9524DCBE266D677078207375663506AC32BE3F7 (void);
// 0x0000051F System.Char System.Text.RegularExpressions.RegexParser::ScanOctal()
extern void RegexParser_ScanOctal_m95EA0AE4B61D5418914DDD3815C6277471B4EDFD (void);
// 0x00000520 System.Int32 System.Text.RegularExpressions.RegexParser::ScanDecimal()
extern void RegexParser_ScanDecimal_m1BFFE19E0698D0EC97D81C655F3D04B608897161 (void);
// 0x00000521 System.Char System.Text.RegularExpressions.RegexParser::ScanHex(System.Int32)
extern void RegexParser_ScanHex_mDD96A112E30DABDB5BC0C558373DA5607F8ACF95 (void);
// 0x00000522 System.Int32 System.Text.RegularExpressions.RegexParser::HexDigit(System.Char)
extern void RegexParser_HexDigit_mD112E7ACEDAD4A29DEA1A771854CA34CEB259053 (void);
// 0x00000523 System.Char System.Text.RegularExpressions.RegexParser::ScanControl()
extern void RegexParser_ScanControl_m26274BE5776CF5AC03E8EAC7B5972C0A8AF2A95B (void);
// 0x00000524 System.Boolean System.Text.RegularExpressions.RegexParser::IsOnlyTopOption(System.Text.RegularExpressions.RegexOptions)
extern void RegexParser_IsOnlyTopOption_mAA13B95864B8B314B8B6689B47FA4BFE3B433280 (void);
// 0x00000525 System.Void System.Text.RegularExpressions.RegexParser::ScanOptions()
extern void RegexParser_ScanOptions_m56F01464F53558D3AF3BF1CD874A2656930AF31C (void);
// 0x00000526 System.Char System.Text.RegularExpressions.RegexParser::ScanCharEscape()
extern void RegexParser_ScanCharEscape_mA872DDDD24E165F5B5C5F62DCFAE3B02CCF97C98 (void);
// 0x00000527 System.String System.Text.RegularExpressions.RegexParser::ParseProperty()
extern void RegexParser_ParseProperty_m3B765A1B30A38A723CAFE63AF9D6F3D08C7C6F38 (void);
// 0x00000528 System.Int32 System.Text.RegularExpressions.RegexParser::TypeFromCode(System.Char)
extern void RegexParser_TypeFromCode_m11B176B0B9A6EF391370E021B111101AE56F4CC6 (void);
// 0x00000529 System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.RegexParser::OptionFromCode(System.Char)
extern void RegexParser_OptionFromCode_mD6CDD96F9917C0518A437DB77BC53130AE702627 (void);
// 0x0000052A System.Void System.Text.RegularExpressions.RegexParser::CountCaptures()
extern void RegexParser_CountCaptures_m2ECDC9ED534F245A12224038BB86DE5D66DB56A3 (void);
// 0x0000052B System.Void System.Text.RegularExpressions.RegexParser::NoteCaptureSlot(System.Int32,System.Int32)
extern void RegexParser_NoteCaptureSlot_m21BD1F90FF2C62FFA445303D574D271757D62222 (void);
// 0x0000052C System.Void System.Text.RegularExpressions.RegexParser::NoteCaptureName(System.String,System.Int32)
extern void RegexParser_NoteCaptureName_m5CDAF94FD43B665DE067B6DC02BD72A315D48FFD (void);
// 0x0000052D System.Void System.Text.RegularExpressions.RegexParser::NoteCaptures(System.Collections.Hashtable,System.Int32,System.Collections.Hashtable)
extern void RegexParser_NoteCaptures_mC02396D9D7C522A5B701A2C1C88E742C98EAAF17 (void);
// 0x0000052E System.Void System.Text.RegularExpressions.RegexParser::AssignNameSlots()
extern void RegexParser_AssignNameSlots_mC5738B99DE328E7D1FA7279A717640EC2044857E (void);
// 0x0000052F System.Int32 System.Text.RegularExpressions.RegexParser::CaptureSlotFromName(System.String)
extern void RegexParser_CaptureSlotFromName_mE18BA49BCA1CE4B972DC804E52C78E48905FEFAB (void);
// 0x00000530 System.Boolean System.Text.RegularExpressions.RegexParser::IsCaptureSlot(System.Int32)
extern void RegexParser_IsCaptureSlot_m1EE21960AA0BB1C072BD9D765AFF8A6DFC3192C3 (void);
// 0x00000531 System.Boolean System.Text.RegularExpressions.RegexParser::IsCaptureName(System.String)
extern void RegexParser_IsCaptureName_m5DC0D7BF3AD01C929CB982048722BCD43797CEAE (void);
// 0x00000532 System.Boolean System.Text.RegularExpressions.RegexParser::UseOptionN()
extern void RegexParser_UseOptionN_mD441B71AC8368C644B8D9A0CCB3DB8BD681639B3 (void);
// 0x00000533 System.Boolean System.Text.RegularExpressions.RegexParser::UseOptionI()
extern void RegexParser_UseOptionI_mB3FB0740ED596E6ABC827E903DCE8AA4258BA701 (void);
// 0x00000534 System.Boolean System.Text.RegularExpressions.RegexParser::UseOptionM()
extern void RegexParser_UseOptionM_mAE3FC2655A6E80EA35D5CEBEE1D82B5B07529118 (void);
// 0x00000535 System.Boolean System.Text.RegularExpressions.RegexParser::UseOptionS()
extern void RegexParser_UseOptionS_m070AD4458172FB14FF21E4502DCA6ECEE8938994 (void);
// 0x00000536 System.Boolean System.Text.RegularExpressions.RegexParser::UseOptionX()
extern void RegexParser_UseOptionX_m0BFB48EDC8A2F38A29A8F6CEC85080D9E04010FC (void);
// 0x00000537 System.Boolean System.Text.RegularExpressions.RegexParser::UseOptionE()
extern void RegexParser_UseOptionE_mE0B7314EAB95CAC9B9762CD3AB9FBC71BFD410D9 (void);
// 0x00000538 System.Boolean System.Text.RegularExpressions.RegexParser::IsSpecial(System.Char)
extern void RegexParser_IsSpecial_m4AC43B42D2727513B568B534C6FF09093ACB2754 (void);
// 0x00000539 System.Boolean System.Text.RegularExpressions.RegexParser::IsStopperX(System.Char)
extern void RegexParser_IsStopperX_mF8FCEDFDBE9E5ED5469A04463782B951552BBB2F (void);
// 0x0000053A System.Boolean System.Text.RegularExpressions.RegexParser::IsQuantifier(System.Char)
extern void RegexParser_IsQuantifier_m7FFA04954A7E4C39D443D790483F2126A5119E25 (void);
// 0x0000053B System.Boolean System.Text.RegularExpressions.RegexParser::IsTrueQuantifier()
extern void RegexParser_IsTrueQuantifier_m22CCE851B85E0760270178DF1184746040E68AEF (void);
// 0x0000053C System.Boolean System.Text.RegularExpressions.RegexParser::IsSpace(System.Char)
extern void RegexParser_IsSpace_m4E37D473B564502A80E5C968E630AA2D4A56231A (void);
// 0x0000053D System.Void System.Text.RegularExpressions.RegexParser::AddConcatenate(System.Int32,System.Int32,System.Boolean)
extern void RegexParser_AddConcatenate_mDDD0350FE254D75D6707668332D0F3088A5572AF (void);
// 0x0000053E System.Void System.Text.RegularExpressions.RegexParser::PushGroup()
extern void RegexParser_PushGroup_m2D76CB852430A3B3F7454DDC3322B2A5E4CE398F (void);
// 0x0000053F System.Void System.Text.RegularExpressions.RegexParser::PopGroup()
extern void RegexParser_PopGroup_mEC525DC83E5BF2F8DA03129AE3DF06834EB76552 (void);
// 0x00000540 System.Boolean System.Text.RegularExpressions.RegexParser::EmptyStack()
extern void RegexParser_EmptyStack_m756AC5BB66D0F2077EC7DD655A3D57681200F5CA (void);
// 0x00000541 System.Void System.Text.RegularExpressions.RegexParser::StartGroup(System.Text.RegularExpressions.RegexNode)
extern void RegexParser_StartGroup_mBF861FCFB0FB9ED26744CAE2297537DAB6DF37C1 (void);
// 0x00000542 System.Void System.Text.RegularExpressions.RegexParser::AddAlternate()
extern void RegexParser_AddAlternate_m77BCF63DD4E1AB89B70F72821CAE4B67936CD847 (void);
// 0x00000543 System.Void System.Text.RegularExpressions.RegexParser::AddConcatenate()
extern void RegexParser_AddConcatenate_m77A3D9F6B1484C7B656BDF3CC5659248F9DEA1EA (void);
// 0x00000544 System.Void System.Text.RegularExpressions.RegexParser::AddConcatenate(System.Boolean,System.Int32,System.Int32)
extern void RegexParser_AddConcatenate_m3B10D68AE09CC6A2F06AC65E49B918D11D5375FF (void);
// 0x00000545 System.Text.RegularExpressions.RegexNode System.Text.RegularExpressions.RegexParser::Unit()
extern void RegexParser_Unit_m6C984A797C4A92AB0A9985E5A8207DB9CA9AE87F (void);
// 0x00000546 System.Void System.Text.RegularExpressions.RegexParser::AddUnitOne(System.Char)
extern void RegexParser_AddUnitOne_m991DCA85D3432B8F7F74D18125A69FFE25108F47 (void);
// 0x00000547 System.Void System.Text.RegularExpressions.RegexParser::AddUnitNotone(System.Char)
extern void RegexParser_AddUnitNotone_m4F79DE175B95FDB88323D7FE9DC3132E24F8F0C0 (void);
// 0x00000548 System.Void System.Text.RegularExpressions.RegexParser::AddUnitSet(System.String)
extern void RegexParser_AddUnitSet_mA262F57A7FAB27996D8115F66BC84A5C58317D7F (void);
// 0x00000549 System.Void System.Text.RegularExpressions.RegexParser::AddUnitNode(System.Text.RegularExpressions.RegexNode)
extern void RegexParser_AddUnitNode_mE8DC7CD039C933AE881097E041BA620AA325FDE8 (void);
// 0x0000054A System.Void System.Text.RegularExpressions.RegexParser::AddUnitType(System.Int32)
extern void RegexParser_AddUnitType_mEBDFA2325ABDA3FDA74184DBFB706824F6540553 (void);
// 0x0000054B System.Void System.Text.RegularExpressions.RegexParser::AddGroup()
extern void RegexParser_AddGroup_m8989D6A9CD4B700522D722DEF5FEFC267C4DBFB7 (void);
// 0x0000054C System.Void System.Text.RegularExpressions.RegexParser::PushOptions()
extern void RegexParser_PushOptions_m6E501EE3C5D6FE990ED74935A9AC9A6E834FD968 (void);
// 0x0000054D System.Void System.Text.RegularExpressions.RegexParser::PopOptions()
extern void RegexParser_PopOptions_mAE4848419A71162B8E563781AC817A8D6ACEB91B (void);
// 0x0000054E System.Boolean System.Text.RegularExpressions.RegexParser::EmptyOptionsStack()
extern void RegexParser_EmptyOptionsStack_mC16A4D18503FF296911710732A95062A8279163B (void);
// 0x0000054F System.Void System.Text.RegularExpressions.RegexParser::PopKeepOptions()
extern void RegexParser_PopKeepOptions_m911F83DFA290ED90B06D3D3166553E6205C256C1 (void);
// 0x00000550 System.ArgumentException System.Text.RegularExpressions.RegexParser::MakeException(System.String)
extern void RegexParser_MakeException_m385FDF5B69D02A0CE93393E92AFAD928A9CCE3FC (void);
// 0x00000551 System.Int32 System.Text.RegularExpressions.RegexParser::Textpos()
extern void RegexParser_Textpos_mBF50F3EB06FDA6C68AF4BF0D0C8D4FC09FDCFC88 (void);
// 0x00000552 System.Void System.Text.RegularExpressions.RegexParser::Textto(System.Int32)
extern void RegexParser_Textto_mA45B325CA6CCBC0012F1CBB0282AE114B34AEBE2 (void);
// 0x00000553 System.Char System.Text.RegularExpressions.RegexParser::RightCharMoveRight()
extern void RegexParser_RightCharMoveRight_m4386CDEFA7DC102FCA0E6E00E522E1712AE3AFE9 (void);
// 0x00000554 System.Void System.Text.RegularExpressions.RegexParser::MoveRight()
extern void RegexParser_MoveRight_m5F6B031B292FC0F81DF5E7039EDA6C9D0706A0A9 (void);
// 0x00000555 System.Void System.Text.RegularExpressions.RegexParser::MoveRight(System.Int32)
extern void RegexParser_MoveRight_m921C1D96B625B4C8AF1D658E514D57CC44E40B61 (void);
// 0x00000556 System.Void System.Text.RegularExpressions.RegexParser::MoveLeft()
extern void RegexParser_MoveLeft_m57BB7A0146D0E8834D715667048B75A517761AD5 (void);
// 0x00000557 System.Char System.Text.RegularExpressions.RegexParser::CharAt(System.Int32)
extern void RegexParser_CharAt_mF46179762DDF46FB78E32CCA3058950244CFCB70 (void);
// 0x00000558 System.Char System.Text.RegularExpressions.RegexParser::RightChar()
extern void RegexParser_RightChar_m23D21ECC3D6999A466C0A91BDF589BFE4801CF91 (void);
// 0x00000559 System.Char System.Text.RegularExpressions.RegexParser::RightChar(System.Int32)
extern void RegexParser_RightChar_m6BEE1FBF048A2DFC53235447BB2F6AC8C56A4147 (void);
// 0x0000055A System.Int32 System.Text.RegularExpressions.RegexParser::CharsRight()
extern void RegexParser_CharsRight_m6F83CD819B532331F5778143B6A95B31C8734A79 (void);
// 0x0000055B System.Void System.Text.RegularExpressions.RegexParser::.cctor()
extern void RegexParser__cctor_m4F6AA7D64102443052B58896EDF95DCC21C2C0C7 (void);
// 0x0000055C System.Void System.Text.RegularExpressions.RegexPrefix::.ctor(System.String,System.Boolean)
extern void RegexPrefix__ctor_m05CD44F32CB0B5F8B2C080EAB94886F9743E86A4 (void);
// 0x0000055D System.Boolean System.Text.RegularExpressions.RegexPrefix::get_CaseInsensitive()
extern void RegexPrefix_get_CaseInsensitive_m4DAE2D9E91916BFFAB928F7DCAA20A5ABD4560E6 (void);
// 0x0000055E System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexPrefix::get_Empty()
extern void RegexPrefix_get_Empty_m3D63F7F64529D4CA3C0929F94E035EEED7D9E0F9 (void);
// 0x0000055F System.String System.Text.RegularExpressions.RegexPrefix::get_Prefix()
extern void RegexPrefix_get_Prefix_m844D02262C0720B0DC488AD1541713AD37BC518E (void);
// 0x00000560 System.Void System.Text.RegularExpressions.RegexPrefix::.cctor()
extern void RegexPrefix__cctor_mF7EA052790930134AC69C755D0066C70CA86363E (void);
// 0x00000561 System.Void System.Text.RegularExpressions.RegexReplacement::.ctor(System.String,System.Text.RegularExpressions.RegexNode,System.Collections.Hashtable)
extern void RegexReplacement__ctor_m9CE743F0908A59F98461440AE5B842BE1BC72EA5 (void);
// 0x00000562 System.Text.RegularExpressions.RegexReplacement System.Text.RegularExpressions.RegexReplacement::GetOrCreate(System.WeakReference`1<System.Text.RegularExpressions.RegexReplacement>,System.String,System.Collections.Hashtable,System.Int32,System.Collections.Hashtable,System.Text.RegularExpressions.RegexOptions)
extern void RegexReplacement_GetOrCreate_m3DABA1D0A9D785394FC1B3E7373EBFD0A2FEB3B5 (void);
// 0x00000563 System.String System.Text.RegularExpressions.RegexReplacement::get_Pattern()
extern void RegexReplacement_get_Pattern_m146EA30FB574587E88DD056AC22F79375C3F55E6 (void);
// 0x00000564 System.Void System.Text.RegularExpressions.RegexReplacement::ReplacementImpl(System.Text.StringBuilder,System.Text.RegularExpressions.Match)
extern void RegexReplacement_ReplacementImpl_m60E4B82702C44912E56FED402AC793E7B7CCC7E8 (void);
// 0x00000565 System.Void System.Text.RegularExpressions.RegexReplacement::ReplacementImplRTL(System.Collections.Generic.List`1<System.String>,System.Text.RegularExpressions.Match)
extern void RegexReplacement_ReplacementImplRTL_m343AE6575EE1B8810D049CA7F8203237920C84E0 (void);
// 0x00000566 System.String System.Text.RegularExpressions.RegexReplacement::Replace(System.Text.RegularExpressions.Regex,System.String,System.Int32,System.Int32)
extern void RegexReplacement_Replace_m54375FA094C20D81305C8DDC17AB973BF1B461F9 (void);
// 0x00000567 System.Void System.Text.RegularExpressions.RegexRunner::.ctor()
extern void RegexRunner__ctor_mBC777F351AD6D1CDD5F20C408B936F7D0F36ED69 (void);
// 0x00000568 System.Text.RegularExpressions.Match System.Text.RegularExpressions.RegexRunner::Scan(System.Text.RegularExpressions.Regex,System.String,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,System.TimeSpan)
extern void RegexRunner_Scan_m903691F4DFB68C0267F240D61A6F166BB592CE53 (void);
// 0x00000569 System.Void System.Text.RegularExpressions.RegexRunner::StartTimeoutWatch()
extern void RegexRunner_StartTimeoutWatch_mA17870A15492787CA1384132267054AB09BE8BE9 (void);
// 0x0000056A System.Void System.Text.RegularExpressions.RegexRunner::CheckTimeout()
extern void RegexRunner_CheckTimeout_m20BF0CD8BC16738ED6E7F20AAF5B4EE695E7E120 (void);
// 0x0000056B System.Void System.Text.RegularExpressions.RegexRunner::DoCheckTimeout()
extern void RegexRunner_DoCheckTimeout_mC26B9239FA1CD3DC98F11A82800F61852793CEA5 (void);
// 0x0000056C System.Void System.Text.RegularExpressions.RegexRunner::Go()
// 0x0000056D System.Boolean System.Text.RegularExpressions.RegexRunner::FindFirstChar()
// 0x0000056E System.Void System.Text.RegularExpressions.RegexRunner::InitTrackCount()
// 0x0000056F System.Void System.Text.RegularExpressions.RegexRunner::InitMatch()
extern void RegexRunner_InitMatch_mCCF94A1E7CCE89275E47BB218C4DD58DF5D5593D (void);
// 0x00000570 System.Text.RegularExpressions.Match System.Text.RegularExpressions.RegexRunner::TidyMatch(System.Boolean)
extern void RegexRunner_TidyMatch_m0EEEF0CFBB55AFF485B4D9F9FD21834B9B3A01FA (void);
// 0x00000571 System.Void System.Text.RegularExpressions.RegexRunner::EnsureStorage()
extern void RegexRunner_EnsureStorage_mB3DB043396771740A05B2AABC293DAAB1250B024 (void);
// 0x00000572 System.Boolean System.Text.RegularExpressions.RegexRunner::IsBoundary(System.Int32,System.Int32,System.Int32)
extern void RegexRunner_IsBoundary_mCF4AE83F6FE3CC46C71C4558DBCDEC255BD422B8 (void);
// 0x00000573 System.Boolean System.Text.RegularExpressions.RegexRunner::IsECMABoundary(System.Int32,System.Int32,System.Int32)
extern void RegexRunner_IsECMABoundary_m56C692126DF69F0933BBFCBF122F764C6093DA26 (void);
// 0x00000574 System.Void System.Text.RegularExpressions.RegexRunner::DoubleTrack()
extern void RegexRunner_DoubleTrack_m15E35365FAF5F438C7BD0BB0C37594F765949F1F (void);
// 0x00000575 System.Void System.Text.RegularExpressions.RegexRunner::DoubleStack()
extern void RegexRunner_DoubleStack_m89C12DF438D7BE4ADA2A4D1DFA36F274F292E1F4 (void);
// 0x00000576 System.Void System.Text.RegularExpressions.RegexRunner::DoubleCrawl()
extern void RegexRunner_DoubleCrawl_m7B67CFD3071825C9F01BC0ACE0E27B1C4EC6DED5 (void);
// 0x00000577 System.Void System.Text.RegularExpressions.RegexRunner::Crawl(System.Int32)
extern void RegexRunner_Crawl_mBE60CC8F17E6D85E1A87F0A86BC684F32ACE7CA5 (void);
// 0x00000578 System.Int32 System.Text.RegularExpressions.RegexRunner::Popcrawl()
extern void RegexRunner_Popcrawl_m2C994BE22A9A4DE0D897691A1CEF87189DB43E67 (void);
// 0x00000579 System.Int32 System.Text.RegularExpressions.RegexRunner::Crawlpos()
extern void RegexRunner_Crawlpos_m4EC84964D3AD3DB49724BFC28CE89FC34478D532 (void);
// 0x0000057A System.Void System.Text.RegularExpressions.RegexRunner::Capture(System.Int32,System.Int32,System.Int32)
extern void RegexRunner_Capture_m3EB42B55A19BD7776156F04CBE9367C6EBA59F9C (void);
// 0x0000057B System.Void System.Text.RegularExpressions.RegexRunner::TransferCapture(System.Int32,System.Int32,System.Int32,System.Int32)
extern void RegexRunner_TransferCapture_m642CC595E9FA641B913CCBE02E233DBB319A6E8A (void);
// 0x0000057C System.Void System.Text.RegularExpressions.RegexRunner::Uncapture()
extern void RegexRunner_Uncapture_m70B0FFB308F48B4673F73311330BFF640F47B7D9 (void);
// 0x0000057D System.Boolean System.Text.RegularExpressions.RegexRunner::IsMatched(System.Int32)
extern void RegexRunner_IsMatched_m7E64237D020BFFA74813A8B6455D5F35671A876C (void);
// 0x0000057E System.Int32 System.Text.RegularExpressions.RegexRunner::MatchIndex(System.Int32)
extern void RegexRunner_MatchIndex_m150A8B71C07022C485630C71B2A9FA6FD195F88F (void);
// 0x0000057F System.Int32 System.Text.RegularExpressions.RegexRunner::MatchLength(System.Int32)
extern void RegexRunner_MatchLength_m19C9B24F4F6CAC56A391A0AC47A4358C61615BF7 (void);
// 0x00000580 System.Text.RegularExpressions.RegexRunner System.Text.RegularExpressions.RegexRunnerFactory::CreateInstance()
// 0x00000581 System.Void System.Text.RegularExpressions.RegexTree::.ctor(System.Text.RegularExpressions.RegexNode,System.Collections.Hashtable,System.Int32[],System.Int32,System.Collections.Hashtable,System.String[],System.Text.RegularExpressions.RegexOptions)
extern void RegexTree__ctor_m89DA1335AAB55B0707774E978E6078CE42DB0DA6 (void);
// 0x00000582 System.Void System.Text.RegularExpressions.RegexWriter::.ctor(System.Span`1<System.Int32>,System.Span`1<System.Int32>)
extern void RegexWriter__ctor_m37DF964F59213D287C9D812A3D0945744BF341EE (void);
// 0x00000583 System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.RegexWriter::Write(System.Text.RegularExpressions.RegexTree)
extern void RegexWriter_Write_mCDBE8B9E7703439F2F27C082808594B1056BEB99 (void);
// 0x00000584 System.Void System.Text.RegularExpressions.RegexWriter::Dispose()
extern void RegexWriter_Dispose_mD34F8BFC41D28F22FBC65E3F1A211DFE85625E04 (void);
// 0x00000585 System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.RegexWriter::RegexCodeFromRegexTree(System.Text.RegularExpressions.RegexTree)
extern void RegexWriter_RegexCodeFromRegexTree_m3352CAF05A1B1753D32D768B83BB778B3E2D4568 (void);
// 0x00000586 System.Void System.Text.RegularExpressions.RegexWriter::PatchJump(System.Int32,System.Int32)
extern void RegexWriter_PatchJump_mC1090F37107CAF7F4B52B020E070BDE9284118A6 (void);
// 0x00000587 System.Void System.Text.RegularExpressions.RegexWriter::Emit(System.Int32)
extern void RegexWriter_Emit_m5884414A6EA7431DA9C5708E650B20BEDF982CAC (void);
// 0x00000588 System.Void System.Text.RegularExpressions.RegexWriter::Emit(System.Int32,System.Int32)
extern void RegexWriter_Emit_m41C2B9DF58542C452CA4FC77A317884578757D75 (void);
// 0x00000589 System.Void System.Text.RegularExpressions.RegexWriter::Emit(System.Int32,System.Int32,System.Int32)
extern void RegexWriter_Emit_mDED2545E64CE95D941A1232DBA24EB6C81A7DB0C (void);
// 0x0000058A System.Int32 System.Text.RegularExpressions.RegexWriter::StringCode(System.String)
extern void RegexWriter_StringCode_m5D4A227BBAA0674B820D70BEA1A2F7D0B29635FE (void);
// 0x0000058B System.Int32 System.Text.RegularExpressions.RegexWriter::MapCapnum(System.Int32)
extern void RegexWriter_MapCapnum_m5EA7BA45A1C4D51DC20C281246D3997A3001A0B6 (void);
// 0x0000058C System.Void System.Text.RegularExpressions.RegexWriter::EmitFragment(System.Int32,System.Text.RegularExpressions.RegexNode,System.Int32)
extern void RegexWriter_EmitFragment_m0B35F7C3BDC22B5DE82735F2652C7948AE8494A5 (void);
// 0x0000058D System.Void System.Diagnostics.BooleanSwitch::.ctor(System.String,System.String)
extern void BooleanSwitch__ctor_mA9232C7B57B18DBDDFD4F14DA26483509C0FFD53 (void);
// 0x0000058E System.Void System.Diagnostics.Switch::.ctor(System.String,System.String)
extern void Switch__ctor_mA295E111B6A7F4B8D2B6AF796926017759669EA7 (void);
// 0x0000058F System.Void System.Diagnostics.Switch::.ctor(System.String,System.String,System.String)
extern void Switch__ctor_mE69262A89E3F19ACB4610B1EBFB35C763BB3EA48 (void);
// 0x00000590 System.Void System.Diagnostics.Switch::_pruneCachedSwitches()
extern void Switch__pruneCachedSwitches_mB732689A566AFB2C1DD9693451B9DE80125CBEFC (void);
// 0x00000591 System.Void System.Diagnostics.Switch::.cctor()
extern void Switch__cctor_m73E1B17596C50266B4FD30170C74991462125626 (void);
// 0x00000592 System.Void System.Diagnostics.SwitchLevelAttribute::.ctor(System.Type)
extern void SwitchLevelAttribute__ctor_mB0C0910F9AA6FC3C568CD3DCEF69731CC528B53A (void);
// 0x00000593 System.Void System.Diagnostics.SwitchLevelAttribute::set_SwitchLevelType(System.Type)
extern void SwitchLevelAttribute_set_SwitchLevelType_m4757AB3B3D77487056F17495239C40B56FC2E9AF (void);
// 0x00000594 System.Int64 System.Diagnostics.Stopwatch::GetTimestamp()
extern void Stopwatch_GetTimestamp_mD6D582A3E30369F05C829A5650BE2AE511EC807F (void);
// 0x00000595 System.Void System.Diagnostics.Stopwatch::.ctor()
extern void Stopwatch__ctor_mAFE6B2F45CF1C3469EF6D5307972BC098B473D0A (void);
// 0x00000596 System.TimeSpan System.Diagnostics.Stopwatch::get_Elapsed()
extern void Stopwatch_get_Elapsed_mF16CCA2F9B7E2EAEFCAF8D81F44370DBAC1B2178 (void);
// 0x00000597 System.Int64 System.Diagnostics.Stopwatch::get_ElapsedMilliseconds()
extern void Stopwatch_get_ElapsedMilliseconds_m8D8E60ADEB47D008C06D8E57D28D41EAE07AECBF (void);
// 0x00000598 System.Int64 System.Diagnostics.Stopwatch::get_ElapsedTicks()
extern void Stopwatch_get_ElapsedTicks_m3F4040FBF8C7CCDC69E0E04824019DEBB25AA410 (void);
// 0x00000599 System.Void System.Diagnostics.Stopwatch::Start()
extern void Stopwatch_Start_m35906A38C7D336CF9DD35854C7E170D762A55915 (void);
// 0x0000059A System.Void System.Diagnostics.Stopwatch::.cctor()
extern void Stopwatch__cctor_mB40A9951DF8D540C2432D56CBC9643E62C40039E (void);
// 0x0000059B System.Void System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute::.ctor()
extern void ExcludeFromCodeCoverageAttribute__ctor_m2CB8196E20519167E842D03BE4CFBCC84BEFC9E2 (void);
// 0x0000059C System.Void System.Security.Authentication.AuthenticationException::.ctor()
extern void AuthenticationException__ctor_m7A29E86FE3299D40A63064FF6DB61D0E1A70C1F6 (void);
// 0x0000059D System.Void System.Security.Authentication.AuthenticationException::.ctor(System.String)
extern void AuthenticationException__ctor_mF08E9BCF31EFCDBECB34BB56D05EEF158B200896 (void);
// 0x0000059E System.Void System.Security.Authentication.AuthenticationException::.ctor(System.String,System.Exception)
extern void AuthenticationException__ctor_m6EDDEE3840629AC06E70CDF7878F955FE1ED538A (void);
// 0x0000059F System.Void System.Security.Authentication.AuthenticationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void AuthenticationException__ctor_m091E6A6612916774EDF1C0F7544B70A5FA97A542 (void);
// 0x000005A0 System.Void System.Security.Cryptography.DerSequenceReader::set_ContentLength(System.Int32)
extern void DerSequenceReader_set_ContentLength_mA2B091D517ABF0AFDF9C54477720668E09F47BCA (void);
// 0x000005A1 System.Void System.Security.Cryptography.DerSequenceReader::.ctor(System.Byte[])
extern void DerSequenceReader__ctor_m899D7771F7F469E94812F8EB9BB0A75A0BE69CBD (void);
// 0x000005A2 System.Void System.Security.Cryptography.DerSequenceReader::.ctor(System.Byte[],System.Int32,System.Int32)
extern void DerSequenceReader__ctor_mAA932343BB190BC44E41A6D8FBA75EC976D0D09A (void);
// 0x000005A3 System.Void System.Security.Cryptography.DerSequenceReader::.ctor(System.Security.Cryptography.DerSequenceReader/DerTag,System.Byte[],System.Int32,System.Int32)
extern void DerSequenceReader__ctor_m09BDCF87C8D4558CABCC96159ADDBB6E49361141 (void);
// 0x000005A4 System.Boolean System.Security.Cryptography.DerSequenceReader::get_HasData()
extern void DerSequenceReader_get_HasData_mD6CE45854665F68A82771427FF58DDF7632D6A31 (void);
// 0x000005A5 System.Byte System.Security.Cryptography.DerSequenceReader::PeekTag()
extern void DerSequenceReader_PeekTag_mC22E34353BE4B4C5CE3579AE64B220263C159C8B (void);
// 0x000005A6 System.Void System.Security.Cryptography.DerSequenceReader::SkipValue()
extern void DerSequenceReader_SkipValue_m0F715CE70FF6EDD8575359FDC2E8ADBAC72FFACE (void);
// 0x000005A7 System.Byte[] System.Security.Cryptography.DerSequenceReader::ReadNextEncodedValue()
extern void DerSequenceReader_ReadNextEncodedValue_mB9D376F1ED7428BFE7DE86B327E1B80E52E91CB7 (void);
// 0x000005A8 System.Boolean System.Security.Cryptography.DerSequenceReader::ReadBoolean()
extern void DerSequenceReader_ReadBoolean_m568EA640EA9967B8785075997D5161FC7C6C5EBD (void);
// 0x000005A9 System.Int32 System.Security.Cryptography.DerSequenceReader::ReadInteger()
extern void DerSequenceReader_ReadInteger_mEFB23332B798605A0F474C5B43C59D70543B93BC (void);
// 0x000005AA System.Byte[] System.Security.Cryptography.DerSequenceReader::ReadIntegerBytes()
extern void DerSequenceReader_ReadIntegerBytes_mBF491D99194CDD9FC70F58CF07E66B535C3F63EB (void);
// 0x000005AB System.Byte[] System.Security.Cryptography.DerSequenceReader::ReadBitString()
extern void DerSequenceReader_ReadBitString_m6519951E58B9642238778071AF7926A6948F0387 (void);
// 0x000005AC System.Byte[] System.Security.Cryptography.DerSequenceReader::ReadOctetString()
extern void DerSequenceReader_ReadOctetString_mDF80CEA6892B5E2BAA457C153DEBE8C411EC70FE (void);
// 0x000005AD System.String System.Security.Cryptography.DerSequenceReader::ReadOidAsString()
extern void DerSequenceReader_ReadOidAsString_m53E76684261C012927DE60E2A8A5A4ECC6B33198 (void);
// 0x000005AE System.String System.Security.Cryptography.DerSequenceReader::ReadUtf8String()
extern void DerSequenceReader_ReadUtf8String_m695D8EC313847638A5E9FCB3B036B3AD4A73DB83 (void);
// 0x000005AF System.Security.Cryptography.DerSequenceReader System.Security.Cryptography.DerSequenceReader::ReadCollectionWithTag(System.Security.Cryptography.DerSequenceReader/DerTag)
extern void DerSequenceReader_ReadCollectionWithTag_mBF55466089C1EAABB9D84B29A9A6193525AC412F (void);
// 0x000005B0 System.Security.Cryptography.DerSequenceReader System.Security.Cryptography.DerSequenceReader::ReadSequence()
extern void DerSequenceReader_ReadSequence_m97DF1A79453D57FB5DB012273F34213180921AB4 (void);
// 0x000005B1 System.Security.Cryptography.DerSequenceReader System.Security.Cryptography.DerSequenceReader::ReadSet()
extern void DerSequenceReader_ReadSet_mB2770ADE8AF6F651659562AEF5B8F247619641FF (void);
// 0x000005B2 System.String System.Security.Cryptography.DerSequenceReader::ReadPrintableString()
extern void DerSequenceReader_ReadPrintableString_m9ED84D1BD26B7DB18C047355BD2011CAFC2E72FE (void);
// 0x000005B3 System.String System.Security.Cryptography.DerSequenceReader::ReadIA5String()
extern void DerSequenceReader_ReadIA5String_m0C23F1CCD96F9F4BD97A43459D1839BDFD1EAB40 (void);
// 0x000005B4 System.String System.Security.Cryptography.DerSequenceReader::ReadT61String()
extern void DerSequenceReader_ReadT61String_m30F888297CE3E176BE97EE993BA1FA945607999E (void);
// 0x000005B5 System.DateTime System.Security.Cryptography.DerSequenceReader::ReadX509Date()
extern void DerSequenceReader_ReadX509Date_m4B50094324675B16BCF2F77BCB1CD6827F04A568 (void);
// 0x000005B6 System.DateTime System.Security.Cryptography.DerSequenceReader::ReadUtcTime()
extern void DerSequenceReader_ReadUtcTime_m16083186C07F2771CB7B4EA9EA68248FCFA9B66B (void);
// 0x000005B7 System.DateTime System.Security.Cryptography.DerSequenceReader::ReadGeneralizedTime()
extern void DerSequenceReader_ReadGeneralizedTime_m24100E18CD48B8F2E7415BEA4095466A0DBF3042 (void);
// 0x000005B8 System.String System.Security.Cryptography.DerSequenceReader::ReadBMPString()
extern void DerSequenceReader_ReadBMPString_m6AD51BC4D16BF2659A5F0D197A0A2F184D36375E (void);
// 0x000005B9 System.String System.Security.Cryptography.DerSequenceReader::TrimTrailingNulls(System.String)
extern void DerSequenceReader_TrimTrailingNulls_m867B3CCF1312220E709B95AC95169DD059B6EC21 (void);
// 0x000005BA System.DateTime System.Security.Cryptography.DerSequenceReader::ReadTime(System.Security.Cryptography.DerSequenceReader/DerTag,System.String)
extern void DerSequenceReader_ReadTime_m492DB6BB481B1CFC586644EE3D233145A5486F32 (void);
// 0x000005BB System.Byte[] System.Security.Cryptography.DerSequenceReader::ReadContentAsBytes()
extern void DerSequenceReader_ReadContentAsBytes_m1DA571FBC10F30D7D68D49DAADF32C320A2B9907 (void);
// 0x000005BC System.Void System.Security.Cryptography.DerSequenceReader::EatTag(System.Security.Cryptography.DerSequenceReader/DerTag)
extern void DerSequenceReader_EatTag_mF2658A324EBFAF16DAEB9762EA7DFE9036D4FB8E (void);
// 0x000005BD System.Void System.Security.Cryptography.DerSequenceReader::CheckTag(System.Security.Cryptography.DerSequenceReader/DerTag,System.Byte[],System.Int32)
extern void DerSequenceReader_CheckTag_mC092A1AF116BB243A90001F1611B11D638D66675 (void);
// 0x000005BE System.Int32 System.Security.Cryptography.DerSequenceReader::EatLength()
extern void DerSequenceReader_EatLength_m2D1533B44A7CE4E4C97B206F704CF0BEE6D2A6A5 (void);
// 0x000005BF System.Int32 System.Security.Cryptography.DerSequenceReader::ScanContentLength(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern void DerSequenceReader_ScanContentLength_mB11C032022638AF8CE558C71D72AC7E51DC69ED8 (void);
// 0x000005C0 System.Void System.Security.Cryptography.DerSequenceReader/<>c::.cctor()
extern void U3CU3Ec__cctor_mD983523B317973CF165D528473020E47EBC3CDDA (void);
// 0x000005C1 System.Void System.Security.Cryptography.DerSequenceReader/<>c::.ctor()
extern void U3CU3Ec__ctor_mF050E3B4FEBB43DB13E684C3B309D33A66DCC861 (void);
// 0x000005C2 System.Text.Encoding System.Security.Cryptography.DerSequenceReader/<>c::<ReadT61String>b__45_0()
extern void U3CU3Ec_U3CReadT61StringU3Eb__45_0_m53F555E4EFBF56385A389ACA1681D68920A18735 (void);
// 0x000005C3 System.Text.Encoding System.Security.Cryptography.DerSequenceReader/<>c::<ReadT61String>b__45_1()
extern void U3CU3Ec_U3CReadT61StringU3Eb__45_1_m2DE60562D90243EC4880800B7AAE0463F5E8B557 (void);
// 0x000005C4 System.Globalization.DateTimeFormatInfo System.Security.Cryptography.DerSequenceReader/<>c::<ReadTime>b__51_0()
extern void U3CU3Ec_U3CReadTimeU3Eb__51_0_mA023CCE123BD5D9DF2635970BA3E5D664BCA33EC (void);
// 0x000005C5 System.Void System.Security.Cryptography.Oid::.ctor()
extern void Oid__ctor_m9032299ACEB6DC66674CB2D8889D875A1A9D308F (void);
// 0x000005C6 System.Void System.Security.Cryptography.Oid::.ctor(System.String)
extern void Oid__ctor_m4AF81B9D51BED48AE505251E0874EC654BA07B72 (void);
// 0x000005C7 System.Void System.Security.Cryptography.Oid::.ctor(System.String,System.String)
extern void Oid__ctor_m533C0126C7C8D382D3E702287A9AD0CED2D0377D (void);
// 0x000005C8 System.Void System.Security.Cryptography.Oid::.ctor(System.Security.Cryptography.Oid)
extern void Oid__ctor_m3973A5FE5482D45FABC8642E59B655274B0B5D59 (void);
// 0x000005C9 System.Security.Cryptography.Oid System.Security.Cryptography.Oid::FromOidValue(System.String,System.Security.Cryptography.OidGroup)
extern void Oid_FromOidValue_mFB93729052DE2FBAB456718907F3B64765D833E4 (void);
// 0x000005CA System.String System.Security.Cryptography.Oid::get_Value()
extern void Oid_get_Value_m59D678A83ED4DE7D87FB7450C8CF4084DB506FD3 (void);
// 0x000005CB System.Void System.Security.Cryptography.Oid::set_Value(System.String)
extern void Oid_set_Value_m122282994E6EE138CE4284B052EF8BE2086CA72F (void);
// 0x000005CC System.String System.Security.Cryptography.Oid::get_FriendlyName()
extern void Oid_get_FriendlyName_m1B7B5D80B6B533A33DAB2B28DCD0B53927D1BD17 (void);
// 0x000005CD System.Void System.Security.Cryptography.Oid::.ctor(System.String,System.String,System.Security.Cryptography.OidGroup)
extern void Oid__ctor_m29BE61A97566E5BE4D5AE6D0A5C31FE30885AADC (void);
// 0x000005CE System.Void System.Security.Cryptography.OidCollection::.ctor()
extern void OidCollection__ctor_m9B8C252AE048962DAD7419597007ABA2793F9D98 (void);
// 0x000005CF System.Int32 System.Security.Cryptography.OidCollection::Add(System.Security.Cryptography.Oid)
extern void OidCollection_Add_m1D4822DF4EA0B912A1DED6AA102D41F7FAEDC8F5 (void);
// 0x000005D0 System.Security.Cryptography.Oid System.Security.Cryptography.OidCollection::get_Item(System.Int32)
extern void OidCollection_get_Item_m0AEC71C746E735035A05C50EE2721C1E8FE08F7A (void);
// 0x000005D1 System.Int32 System.Security.Cryptography.OidCollection::get_Count()
extern void OidCollection_get_Count_m9560D70809087591935904924A15FBEC1FEFEB11 (void);
// 0x000005D2 System.Security.Cryptography.OidEnumerator System.Security.Cryptography.OidCollection::GetEnumerator()
extern void OidCollection_GetEnumerator_mBB791F48E305B3355A1BCE624E0BB367331F82AC (void);
// 0x000005D3 System.Collections.IEnumerator System.Security.Cryptography.OidCollection::System.Collections.IEnumerable.GetEnumerator()
extern void OidCollection_System_Collections_IEnumerable_GetEnumerator_m4624AA2C7F0693698228803D9B59EFE6AAD6AFE4 (void);
// 0x000005D4 System.Void System.Security.Cryptography.OidCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern void OidCollection_System_Collections_ICollection_CopyTo_m4EF1FB9A18E822BA196BF3F0204B7C06178B9CFC (void);
// 0x000005D5 System.Object System.Security.Cryptography.OidCollection::get_SyncRoot()
extern void OidCollection_get_SyncRoot_mD310358F3BF4454604AD96C734BA6EC180CE7A85 (void);
// 0x000005D6 System.Void System.Security.Cryptography.OidEnumerator::.ctor(System.Security.Cryptography.OidCollection)
extern void OidEnumerator__ctor_m2168FEE96614FB7460C84762ED3728A5413F42E6 (void);
// 0x000005D7 System.Security.Cryptography.Oid System.Security.Cryptography.OidEnumerator::get_Current()
extern void OidEnumerator_get_Current_mCED077699FA00ED57FA82F8B460C28B86F1B5C9F (void);
// 0x000005D8 System.Object System.Security.Cryptography.OidEnumerator::System.Collections.IEnumerator.get_Current()
extern void OidEnumerator_System_Collections_IEnumerator_get_Current_m0B627B9F510EF4F283F40EBE17164B9231BB7735 (void);
// 0x000005D9 System.Boolean System.Security.Cryptography.OidEnumerator::MoveNext()
extern void OidEnumerator_MoveNext_mD42201944C163E81DABDE08B1535C4AB9A5778DD (void);
// 0x000005DA System.Void System.Security.Cryptography.OidEnumerator::Reset()
extern void OidEnumerator_Reset_m24F1F25812A7BFE9B02B312BA9E670C930622F27 (void);
// 0x000005DB System.Void System.Security.Cryptography.AsnEncodedData::.ctor()
extern void AsnEncodedData__ctor_m90028EC56A5E36272EAA4A9279AE23358D46F547 (void);
// 0x000005DC System.Void System.Security.Cryptography.AsnEncodedData::.ctor(System.String,System.Byte[])
extern void AsnEncodedData__ctor_m90834437DE68932EB0BE8BB434DDA1DB896DA67E (void);
// 0x000005DD System.Void System.Security.Cryptography.AsnEncodedData::.ctor(System.Security.Cryptography.Oid,System.Byte[])
extern void AsnEncodedData__ctor_m3E8C1D1E0CE464176742B847DC99BF8EAABF15E6 (void);
// 0x000005DE System.Void System.Security.Cryptography.AsnEncodedData::.ctor(System.Security.Cryptography.AsnEncodedData)
extern void AsnEncodedData__ctor_mA80B22E2D7D98198B55D04474F6926EF80CBABF6 (void);
// 0x000005DF System.Security.Cryptography.Oid System.Security.Cryptography.AsnEncodedData::get_Oid()
extern void AsnEncodedData_get_Oid_mBDB4803B2ADEE3BD40596E82FF738C2DC339BC28 (void);
// 0x000005E0 System.Void System.Security.Cryptography.AsnEncodedData::set_Oid(System.Security.Cryptography.Oid)
extern void AsnEncodedData_set_Oid_m877771219F651EA3FF834952300180274531C9C7 (void);
// 0x000005E1 System.Byte[] System.Security.Cryptography.AsnEncodedData::get_RawData()
extern void AsnEncodedData_get_RawData_m629D858D7D20E72C1F46124E5032FE0D3B32AADF (void);
// 0x000005E2 System.Void System.Security.Cryptography.AsnEncodedData::set_RawData(System.Byte[])
extern void AsnEncodedData_set_RawData_m05F9F17765AD384ECE3A333DCBF6A0572099346B (void);
// 0x000005E3 System.Void System.Security.Cryptography.AsnEncodedData::CopyFrom(System.Security.Cryptography.AsnEncodedData)
extern void AsnEncodedData_CopyFrom_mA5DD4A419C5FC29D82DBE9B69C9F8CBC2872A1F2 (void);
// 0x000005E4 System.String System.Security.Cryptography.AsnEncodedData::Format(System.Boolean)
extern void AsnEncodedData_Format_m557790DA037FAFD8C7FE28D4579487863DCB3346 (void);
// 0x000005E5 System.String System.Security.Cryptography.AsnEncodedData::ToString(System.Boolean)
extern void AsnEncodedData_ToString_m669772943B78D9242BC5F176A4BD857326F95021 (void);
// 0x000005E6 System.String System.Security.Cryptography.AsnEncodedData::Default(System.Boolean)
extern void AsnEncodedData_Default_m883444CBD5238793D1B94A6DE273FF2F9C47EA70 (void);
// 0x000005E7 System.String System.Security.Cryptography.AsnEncodedData::BasicConstraintsExtension(System.Boolean)
extern void AsnEncodedData_BasicConstraintsExtension_m74BFC192666755E9D8A088880789AB6EA84F1FCC (void);
// 0x000005E8 System.String System.Security.Cryptography.AsnEncodedData::EnhancedKeyUsageExtension(System.Boolean)
extern void AsnEncodedData_EnhancedKeyUsageExtension_m3C95D5D2D8F81BEA5E33B264767C8A5E5575D7CA (void);
// 0x000005E9 System.String System.Security.Cryptography.AsnEncodedData::KeyUsageExtension(System.Boolean)
extern void AsnEncodedData_KeyUsageExtension_m9A9C72CF2A831A1849A55336FC8D6DEE097E28D3 (void);
// 0x000005EA System.String System.Security.Cryptography.AsnEncodedData::SubjectKeyIdentifierExtension(System.Boolean)
extern void AsnEncodedData_SubjectKeyIdentifierExtension_m3CEDDCED26AD5621407DAA401F385269348BB789 (void);
// 0x000005EB System.String System.Security.Cryptography.AsnEncodedData::SubjectAltName(System.Boolean)
extern void AsnEncodedData_SubjectAltName_mDE2F39E1A43C1E13D89319F221517EE54B1A6A14 (void);
// 0x000005EC System.String System.Security.Cryptography.AsnEncodedData::NetscapeCertType(System.Boolean)
extern void AsnEncodedData_NetscapeCertType_m55E43EBDA7882304E8FE5F3DC134A2719F519373 (void);
// 0x000005ED System.Void System.Security.Cryptography.X509Certificates.PublicKey::.ctor(System.Security.Cryptography.Oid,System.Security.Cryptography.AsnEncodedData,System.Security.Cryptography.AsnEncodedData)
extern void PublicKey__ctor_m810A1EF4D8A0BAE8A3CB36EB8CC0BBA99D799782 (void);
// 0x000005EE System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedKeyValue()
extern void PublicKey_get_EncodedKeyValue_m153EF781728D471C55002A40BBDA435F1B3A1213 (void);
// 0x000005EF System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedParameters()
extern void PublicKey_get_EncodedParameters_mE338A4A69A15907E38A644D624A07DDD55C521E0 (void);
// 0x000005F0 System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::get_Key()
extern void PublicKey_get_Key_mC0A4379DAA42B565878EE09310C43A4CCB45F273 (void);
// 0x000005F1 System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::get_Oid()
extern void PublicKey_get_Oid_mB5796F569C136759CB740F747C6A3B6F8976401A (void);
// 0x000005F2 System.Byte[] System.Security.Cryptography.X509Certificates.PublicKey::GetUnsignedBigInteger(System.Byte[])
extern void PublicKey_GetUnsignedBigInteger_mDC1A423F4649E751E2F44C534F52F1103F1E8CA2 (void);
// 0x000005F3 System.Security.Cryptography.DSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeDSA(System.Byte[],System.Byte[])
extern void PublicKey_DecodeDSA_m32E09E73807395C6F5B0DD37E72874DD1FD1215A (void);
// 0x000005F4 System.Security.Cryptography.RSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeRSA(System.Byte[])
extern void PublicKey_DecodeRSA_m462116935D07EA1121C50801E6CCB6B21F0C5DC1 (void);
// 0x000005F5 System.Void System.Security.Cryptography.X509Certificates.PublicKey::.cctor()
extern void PublicKey__cctor_mBA35606057B7AD6687CD52BFED47441593DC5004 (void);
// 0x000005F6 System.Void System.Security.Cryptography.X509Certificates.X500DistinguishedName::.ctor(System.Byte[])
extern void X500DistinguishedName__ctor_mF36480D37651E6FC6662E90ED7795F9A5C1C3DD2 (void);
// 0x000005F7 System.Void System.Security.Cryptography.X509Certificates.X500DistinguishedName::.ctor(System.String)
extern void X500DistinguishedName__ctor_m880E4D05D355F1B494990EFA3B9C7D0DFA0C8679 (void);
// 0x000005F8 System.Void System.Security.Cryptography.X509Certificates.X500DistinguishedName::.ctor(System.String,System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags)
extern void X500DistinguishedName__ctor_m95199AB45F165A1B774CA88B45F08889BFA5E162 (void);
// 0x000005F9 System.String System.Security.Cryptography.X509Certificates.X500DistinguishedName::get_Name()
extern void X500DistinguishedName_get_Name_m57B2708AAA2B4E15AB6F6B513D88C4468898B5A9 (void);
// 0x000005FA System.String System.Security.Cryptography.X509Certificates.X500DistinguishedName::Decode(System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags)
extern void X500DistinguishedName_Decode_mEC580A9699C0384B0196C80CD5ECFAA2151243BD (void);
// 0x000005FB System.String System.Security.Cryptography.X509Certificates.X500DistinguishedName::Format(System.Boolean)
extern void X500DistinguishedName_Format_mD7DD9D893081C391AA74C66CEC456324EDC36969 (void);
// 0x000005FC System.String System.Security.Cryptography.X509Certificates.X500DistinguishedName::GetSeparator(System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags)
extern void X500DistinguishedName_GetSeparator_m2317B62BFC2A082692A05C18874763719D2E5BF8 (void);
// 0x000005FD System.Void System.Security.Cryptography.X509Certificates.X500DistinguishedName::DecodeRawData()
extern void X500DistinguishedName_DecodeRawData_mB465AEF2F370353BB809B6B310064FF67DC2D970 (void);
// 0x000005FE System.String System.Security.Cryptography.X509Certificates.X500DistinguishedName::Canonize(System.String)
extern void X500DistinguishedName_Canonize_m7B0365D90F71C238DAA5795422320E1E4A63EB98 (void);
// 0x000005FF System.Boolean System.Security.Cryptography.X509Certificates.X500DistinguishedName::AreEqual(System.Security.Cryptography.X509Certificates.X500DistinguishedName,System.Security.Cryptography.X509Certificates.X500DistinguishedName)
extern void X500DistinguishedName_AreEqual_m91FA80CFEC70C71A5E3C73D5D40BFA220427588C (void);
// 0x00000600 System.Void System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::.ctor()
extern void X509BasicConstraintsExtension__ctor_m672BE9B41107A8C30454BF4CAC120D70512BA89A (void);
// 0x00000601 System.Void System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::.ctor(System.Security.Cryptography.AsnEncodedData,System.Boolean)
extern void X509BasicConstraintsExtension__ctor_m76C88E8D4C7CCEC7B2C3D56CC58A56352F7729E4 (void);
// 0x00000602 System.Void System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::.ctor(System.Boolean,System.Boolean,System.Int32,System.Boolean)
extern void X509BasicConstraintsExtension__ctor_m02334D8CAA280B339CA41E2019CF1F02C8B2E7DE (void);
// 0x00000603 System.Boolean System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::get_CertificateAuthority()
extern void X509BasicConstraintsExtension_get_CertificateAuthority_mB4ECEF43A245E97DA80C6323BE339BE2DF99FF5C (void);
// 0x00000604 System.Boolean System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::get_HasPathLengthConstraint()
extern void X509BasicConstraintsExtension_get_HasPathLengthConstraint_m8F7C371EFF99BD110663DB106BEE889955255BC7 (void);
// 0x00000605 System.Int32 System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::get_PathLengthConstraint()
extern void X509BasicConstraintsExtension_get_PathLengthConstraint_m4D3F016E42C243887D07DD955518A818DC14BBC5 (void);
// 0x00000606 System.Void System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::CopyFrom(System.Security.Cryptography.AsnEncodedData)
extern void X509BasicConstraintsExtension_CopyFrom_mCC603D99CEC828BDF86FDEB3C4650CF4D3753B8E (void);
// 0x00000607 System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::Decode(System.Byte[])
extern void X509BasicConstraintsExtension_Decode_m7C50097B6238FEF4DAD6A9A09A449DB0CAFFEBEB (void);
// 0x00000608 System.Byte[] System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::Encode()
extern void X509BasicConstraintsExtension_Encode_mD7FA31A70B025F67062C3844E4FEB09B14A4DE2B (void);
// 0x00000609 System.String System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::ToString(System.Boolean)
extern void X509BasicConstraintsExtension_ToString_mBFC61BA07CD2BB8EACCC19A5AE72EA81C11D02E4 (void);
// 0x0000060A System.Void System.Security.Cryptography.X509Certificates.X509Certificate2::Reset()
extern void X509Certificate2_Reset_mE779D76DC57C54B674D7588861324F02DD784C3E (void);
// 0x0000060B System.Void System.Security.Cryptography.X509Certificates.X509Certificate2::.ctor()
extern void X509Certificate2__ctor_m84519718D6C34074E7037786ADAE51290460C2B1 (void);
// 0x0000060C System.Void System.Security.Cryptography.X509Certificates.X509Certificate2::.ctor(System.Byte[])
extern void X509Certificate2__ctor_m34CFFC999D3A152729A5C59DBE80AB709547DA19 (void);
// 0x0000060D System.Void System.Security.Cryptography.X509Certificates.X509Certificate2::.ctor(System.Byte[],System.String)
extern void X509Certificate2__ctor_m196538E20CE749EB7220F5AEF93FEBF0C7072350 (void);
// 0x0000060E System.Void System.Security.Cryptography.X509Certificates.X509Certificate2::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2Impl)
extern void X509Certificate2__ctor_m7DA4EE40349835A87AAA8E5717489DFD6C44D0A2 (void);
// 0x0000060F System.Void System.Security.Cryptography.X509Certificates.X509Certificate2::.ctor(System.String,System.String)
extern void X509Certificate2__ctor_m909B8F5F1E15BC0006AF21031FF21AB2CDE8266C (void);
// 0x00000610 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate)
extern void X509Certificate2__ctor_m2C7F482BFC1A0BAAABDD8BE9BBB2026DA5F3E600 (void);
// 0x00000611 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void X509Certificate2__ctor_mB1BC3DC26B71A5D5D3753B04DCA16AA49627EC15 (void);
// 0x00000612 System.Security.Cryptography.X509Certificates.X509ExtensionCollection System.Security.Cryptography.X509Certificates.X509Certificate2::get_Extensions()
extern void X509Certificate2_get_Extensions_m3746E7EB004789DF5AF56AD36209676860901579 (void);
// 0x00000613 System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2::get_HasPrivateKey()
extern void X509Certificate2_get_HasPrivateKey_m561EE07579AC86B96856A73367961AD01DAD8567 (void);
// 0x00000614 System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.X509Certificate2::get_PrivateKey()
extern void X509Certificate2_get_PrivateKey_mB2D1370AFF6F005B4A18A2223363C353B8D24A4A (void);
// 0x00000615 System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2::get_IssuerName()
extern void X509Certificate2_get_IssuerName_m47CB829F5388F2E68A20D473F586FA847739B83D (void);
// 0x00000616 System.DateTime System.Security.Cryptography.X509Certificates.X509Certificate2::get_NotAfter()
extern void X509Certificate2_get_NotAfter_m925CA3901B7856D0692C6DED4F6428A40A726787 (void);
// 0x00000617 System.DateTime System.Security.Cryptography.X509Certificates.X509Certificate2::get_NotBefore()
extern void X509Certificate2_get_NotBefore_m656CC9913A0E6F6F0C06185F3B63F3A2EDCEB801 (void);
// 0x00000618 System.Security.Cryptography.X509Certificates.PublicKey System.Security.Cryptography.X509Certificates.X509Certificate2::get_PublicKey()
extern void X509Certificate2_get_PublicKey_mF1813BEE1EE04C593FBAA44590E2AEBC23A72304 (void);
// 0x00000619 System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2::get_RawData()
extern void X509Certificate2_get_RawData_m34518B0CB70515321029A2265AA89EFEB50177C6 (void);
// 0x0000061A System.String System.Security.Cryptography.X509Certificates.X509Certificate2::get_SerialNumber()
extern void X509Certificate2_get_SerialNumber_m12F610EEBE485A29FD49A06E8A33BB690476309B (void);
// 0x0000061B System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.X509Certificate2::get_SignatureAlgorithm()
extern void X509Certificate2_get_SignatureAlgorithm_mF3FE8751A82E00E38E315EA893575D125F4F7354 (void);
// 0x0000061C System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2::get_SubjectName()
extern void X509Certificate2_get_SubjectName_m0A767E93883BD9D984903B6753819CB7E02D90BC (void);
// 0x0000061D System.String System.Security.Cryptography.X509Certificates.X509Certificate2::get_Thumbprint()
extern void X509Certificate2_get_Thumbprint_m0A33F62238AF002C1B93A0F93A361B890E61DE8C (void);
// 0x0000061E System.Int32 System.Security.Cryptography.X509Certificates.X509Certificate2::get_Version()
extern void X509Certificate2_get_Version_m6A4FB2125DA64437D184A64E731F2B58738F0B09 (void);
// 0x0000061F System.Security.Cryptography.X509Certificates.X509ContentType System.Security.Cryptography.X509Certificates.X509Certificate2::GetCertContentType(System.Byte[])
extern void X509Certificate2_GetCertContentType_mDEFBB0D6E87233AE62662CA74E7142FD1379D3FA (void);
// 0x00000620 System.String System.Security.Cryptography.X509Certificates.X509Certificate2::GetNameInfo(System.Security.Cryptography.X509Certificates.X509NameType,System.Boolean)
extern void X509Certificate2_GetNameInfo_mDFE32FF38933F2B9F652B27B1EA857AD463222DE (void);
// 0x00000621 System.String System.Security.Cryptography.X509Certificates.X509Certificate2::ToString()
extern void X509Certificate2_ToString_mAC732FED5427D5551E2CC9B06C145128E7F6563B (void);
// 0x00000622 System.String System.Security.Cryptography.X509Certificates.X509Certificate2::ToString(System.Boolean)
extern void X509Certificate2_ToString_mA115124642674F646AF34E6B8E2FB54870026342 (void);
// 0x00000623 System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2::Verify()
extern void X509Certificate2_Verify_m5BDB9791EDBA2F518184D4AFC2FD1B656F4C4591 (void);
// 0x00000624 System.Security.Cryptography.X509Certificates.X509Extension System.Security.Cryptography.X509Certificates.X509Certificate2::CreateCustomExtensionIfAny(System.Security.Cryptography.Oid)
extern void X509Certificate2_CreateCustomExtensionIfAny_m2290FD40B2CB727A1504C2F0B34D1FE0E5B11000 (void);
// 0x00000625 System.Security.Cryptography.X509Certificates.X509Certificate2Impl System.Security.Cryptography.X509Certificates.X509Certificate2::get_Impl()
extern void X509Certificate2_get_Impl_m21C56437F9DD096FA112C2DF07F04EB2E381015F (void);
// 0x00000626 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Collection::.ctor()
extern void X509Certificate2Collection__ctor_mC7A17C695E7DB906D549A0BF60943D2490F4C2B9 (void);
// 0x00000627 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Collection::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2Collection)
extern void X509Certificate2Collection__ctor_mD3765F49CF526B51256B004DDA856A291EFEF74C (void);
// 0x00000628 System.Security.Cryptography.X509Certificates.X509Certificate2 System.Security.Cryptography.X509Certificates.X509Certificate2Collection::get_Item(System.Int32)
extern void X509Certificate2Collection_get_Item_m46D8DA4B824642F03A2E448CB52AB591850DE9C4 (void);
// 0x00000629 System.Int32 System.Security.Cryptography.X509Certificates.X509Certificate2Collection::Add(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509Certificate2Collection_Add_m6C2C142C7BF143E83388F8793A64890F44996ABB (void);
// 0x0000062A System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Collection::AddRange(System.Security.Cryptography.X509Certificates.X509Certificate2Collection)
extern void X509Certificate2Collection_AddRange_m764C331A677795DCA45A496DF9752B58F2FDA2A8 (void);
// 0x0000062B System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2Collection::Contains(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509Certificate2Collection_Contains_m71ADE296B2581756ED2484BC42CB4D51A9172688 (void);
// 0x0000062C System.String System.Security.Cryptography.X509Certificates.X509Certificate2Collection::GetKeyIdentifier(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509Certificate2Collection_GetKeyIdentifier_mA7189361FD18F2665E09354BA391E96B189B44EA (void);
// 0x0000062D System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509Certificate2Collection::Find(System.Security.Cryptography.X509Certificates.X509FindType,System.Object,System.Boolean)
extern void X509Certificate2Collection_Find_mA13F604A4AAF8C5EFFA436EFA7785B7E27C0471C (void);
// 0x0000062E System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator System.Security.Cryptography.X509Certificates.X509Certificate2Collection::GetEnumerator()
extern void X509Certificate2Collection_GetEnumerator_m2D2A261B2F68A857C3C8B0037C5E05F10A6761FC (void);
// 0x0000062F System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Collection::.cctor()
extern void X509Certificate2Collection__cctor_mC3757CA5D10515491E20F8CEBAEE82745C49F7FF (void);
// 0x00000630 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2Collection)
extern void X509Certificate2Enumerator__ctor_mBF306968F1D046A5084D7E70CE7A7A3E0412357B (void);
// 0x00000631 System.Security.Cryptography.X509Certificates.X509Certificate2 System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::get_Current()
extern void X509Certificate2Enumerator_get_Current_m7D19172490F76C81A7E7DB5AF8D172400BC95B53 (void);
// 0x00000632 System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::MoveNext()
extern void X509Certificate2Enumerator_MoveNext_m824241D32F6E1B3FF4CAE0E2B370BB5E32238F19 (void);
// 0x00000633 System.Object System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::System.Collections.IEnumerator.get_Current()
extern void X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m11E9442F6F2EB7E5829832ABAD721E4B7E4A1CC5 (void);
// 0x00000634 System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::System.Collections.IEnumerator.MoveNext()
extern void X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m28901EEB56D14B1402463992A26EE714EA1B2B85 (void);
// 0x00000635 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::System.Collections.IEnumerator.Reset()
extern void X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m84371FC9208871CE46B7D09A77AF055C52938FC6 (void);
// 0x00000636 System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509Extension> System.Security.Cryptography.X509Certificates.X509Certificate2Impl::get_Extensions()
// 0x00000637 System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2Impl::get_IssuerName()
// 0x00000638 System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.X509Certificate2Impl::get_PrivateKey()
// 0x00000639 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Impl::set_PrivateKey(System.Security.Cryptography.AsymmetricAlgorithm)
// 0x0000063A System.String System.Security.Cryptography.X509Certificates.X509Certificate2Impl::get_SignatureAlgorithm()
// 0x0000063B System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2Impl::get_SubjectName()
// 0x0000063C System.Int32 System.Security.Cryptography.X509Certificates.X509Certificate2Impl::get_Version()
// 0x0000063D System.Security.Cryptography.X509Certificates.X509CertificateImplCollection System.Security.Cryptography.X509Certificates.X509Certificate2Impl::get_IntermediateCertificates()
// 0x0000063E System.String System.Security.Cryptography.X509Certificates.X509Certificate2Impl::GetNameInfo(System.Security.Cryptography.X509Certificates.X509NameType,System.Boolean)
// 0x0000063F System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2Impl::Verify(System.Security.Cryptography.X509Certificates.X509Certificate2)
// 0x00000640 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Impl::AppendPrivateKeyInfo(System.Text.StringBuilder)
// 0x00000641 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Impl::.ctor()
extern void X509Certificate2Impl__ctor_mE6FF68AD589010E57C86555935DB601680AC1839 (void);
// 0x00000642 System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::get_IsValid()
extern void X509Certificate2ImplMono_get_IsValid_mA2D572B4E2382A93FEB9F88940269684D433439B (void);
// 0x00000643 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::.ctor(Mono.Security.X509.X509Certificate)
extern void X509Certificate2ImplMono__ctor_m3F6BBB4381EB9A720E11577B9F663DC10B3B93A7 (void);
// 0x00000644 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono)
extern void X509Certificate2ImplMono__ctor_m622D5FDEB76F561EA4AE9C8F4597D8E6AE1C6C28 (void);
// 0x00000645 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::.ctor(System.Byte[],Microsoft.Win32.SafeHandles.SafePasswordHandle,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern void X509Certificate2ImplMono__ctor_m1B2FB6BBB7558184F07C2E42ECE23C70075A9B7D (void);
// 0x00000646 System.Security.Cryptography.X509Certificates.X509CertificateImpl System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::Clone()
extern void X509Certificate2ImplMono_Clone_mE74EE518CD4FAD43F670E3B9140350A0C9766385 (void);
// 0x00000647 Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::get_Cert()
extern void X509Certificate2ImplMono_get_Cert_m456536A0FC9BB84D859CC7ECAA421203FDCE6E00 (void);
// 0x00000648 System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::GetRawCertData()
extern void X509Certificate2ImplMono_GetRawCertData_mF4AB5CCF83D23AFE455D75CC7545423221565CC0 (void);
// 0x00000649 System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::get_HasPrivateKey()
extern void X509Certificate2ImplMono_get_HasPrivateKey_mE1EE233BC55F71002EA56248BCCADF483BE07B22 (void);
// 0x0000064A System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::get_PrivateKey()
extern void X509Certificate2ImplMono_get_PrivateKey_m2B92DBE7562BBE32D2DC2F5ADC6E96FCD2497F60 (void);
// 0x0000064B System.Void System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::set_PrivateKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern void X509Certificate2ImplMono_set_PrivateKey_m6F611F15AA315BB5F2B21984CDCB7A955855D205 (void);
// 0x0000064C System.Security.Cryptography.RSA System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::GetRSAPrivateKey()
extern void X509Certificate2ImplMono_GetRSAPrivateKey_m88696965DA58E0E6EDAC4E6397F13A54031359A9 (void);
// 0x0000064D System.Security.Cryptography.DSA System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::GetDSAPrivateKey()
extern void X509Certificate2ImplMono_GetDSAPrivateKey_mFDE83C8B6DB600DE9FC20E791EDF3F1FA57DB77B (void);
// 0x0000064E Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::ImportPkcs12(System.Byte[],Microsoft.Win32.SafeHandles.SafePasswordHandle)
extern void X509Certificate2ImplMono_ImportPkcs12_mE10A545B13FB03F48E6F4E7210F6FE86BEAD77B0 (void);
// 0x0000064F Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::ImportPkcs12(System.Byte[],System.String)
extern void X509Certificate2ImplMono_ImportPkcs12_mC29B89A6EDBB108185C4A91028F6EB496ACB2C98 (void);
// 0x00000650 System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::Verify(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509Certificate2ImplMono_Verify_m2DEACB934D750CA4071F66F4A3067B9A2F24C9E8 (void);
// 0x00000651 System.Security.Cryptography.X509Certificates.X509CertificateImplCollection System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::get_IntermediateCertificates()
extern void X509Certificate2ImplMono_get_IntermediateCertificates_m304680821DC498B386CF82B8B82DD1248DE52C85 (void);
// 0x00000652 Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::get_MonoCertificate()
extern void X509Certificate2ImplMono_get_MonoCertificate_mE723B3BB0523A176164138FE73F1F34C28E3EB2A (void);
// 0x00000653 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::.cctor()
extern void X509Certificate2ImplMono__cctor_mAA23F03A760725B059D7DE6FB55EFDA78FFFD328 (void);
// 0x00000654 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::EnsureCertData()
extern void X509Certificate2ImplUnix_EnsureCertData_m4BD3C65CDE138D7239A18652543C7A93BFDD4E62 (void);
// 0x00000655 System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::GetRawCertData()
// 0x00000656 System.String System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_KeyAlgorithm()
extern void X509Certificate2ImplUnix_get_KeyAlgorithm_m91A6386635D6554DFE8D3ABE9BE166A2B39965DF (void);
// 0x00000657 System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_KeyAlgorithmParameters()
extern void X509Certificate2ImplUnix_get_KeyAlgorithmParameters_m97B958D5098C698F869D9C9C9BE13AD43AABCFD7 (void);
// 0x00000658 System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_PublicKeyValue()
extern void X509Certificate2ImplUnix_get_PublicKeyValue_m0C7862B1E1A55C681109592D4BD09439F43F301C (void);
// 0x00000659 System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_SerialNumber()
extern void X509Certificate2ImplUnix_get_SerialNumber_m5388A1C777603B896EFE49885A320520AE8BFE71 (void);
// 0x0000065A System.String System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_SignatureAlgorithm()
extern void X509Certificate2ImplUnix_get_SignatureAlgorithm_mC5519532C05449E473FDE2BCAB54F2429AE8A1C4 (void);
// 0x0000065B System.Int32 System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_Version()
extern void X509Certificate2ImplUnix_get_Version_mCE7533BFE8F2E54A4386855F1288E87CB431BA6D (void);
// 0x0000065C System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_SubjectName()
extern void X509Certificate2ImplUnix_get_SubjectName_m1080CB4AA55DEF3052FABE2681FCD500AEC2A786 (void);
// 0x0000065D System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_IssuerName()
extern void X509Certificate2ImplUnix_get_IssuerName_mE929B42F7137A781DBAE7BA1DB8704D773DF50FF (void);
// 0x0000065E System.String System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_Subject()
extern void X509Certificate2ImplUnix_get_Subject_m32692DEEF247791D84F4F69D487D11A1243919DE (void);
// 0x0000065F System.String System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_Issuer()
extern void X509Certificate2ImplUnix_get_Issuer_mD59B5CFCA2DABBDCA2B68A1FA13CD037BDEC5A35 (void);
// 0x00000660 System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_RawData()
extern void X509Certificate2ImplUnix_get_RawData_m113719E0C35A6A4917249A7F94D9146563ED6713 (void);
// 0x00000661 System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_Thumbprint()
extern void X509Certificate2ImplUnix_get_Thumbprint_m493D3178B247696830F366F95EDEEF7619D7A174 (void);
// 0x00000662 System.String System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::GetNameInfo(System.Security.Cryptography.X509Certificates.X509NameType,System.Boolean)
extern void X509Certificate2ImplUnix_GetNameInfo_m9F4BB6FBEE9CD1465008C26862D4C69854FAD653 (void);
// 0x00000663 System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509Extension> System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_Extensions()
extern void X509Certificate2ImplUnix_get_Extensions_m216C3BA296F656BC33797000BAD6B06DD871AF3F (void);
// 0x00000664 System.DateTime System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_NotAfter()
extern void X509Certificate2ImplUnix_get_NotAfter_mC7CE817FB14A0858D6FF56B92BEB9C62A54E3709 (void);
// 0x00000665 System.DateTime System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::get_NotBefore()
extern void X509Certificate2ImplUnix_get_NotBefore_mD0D40AAD63C151332B3A3F50B7856BAAE0D2A313 (void);
// 0x00000666 System.Void System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::AppendPrivateKeyInfo(System.Text.StringBuilder)
extern void X509Certificate2ImplUnix_AppendPrivateKeyInfo_m0A248F4E075D601150745C99FAA3DE8131A52FDF (void);
// 0x00000667 System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::Export(System.Security.Cryptography.X509Certificates.X509ContentType,Microsoft.Win32.SafeHandles.SafePasswordHandle)
extern void X509Certificate2ImplUnix_Export_m59E685C8306695FBCA2725F56D6127CDE6B0CA10 (void);
// 0x00000668 System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::ExportPkcs12(Microsoft.Win32.SafeHandles.SafePasswordHandle)
extern void X509Certificate2ImplUnix_ExportPkcs12_m936EF9B64E83A0660C321DD43CFBCBFE5807AD9E (void);
// 0x00000669 System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::ExportPkcs12(System.String)
extern void X509Certificate2ImplUnix_ExportPkcs12_mC956F970A09A223AAD1B163E787BEF552623FB0E (void);
// 0x0000066A System.Void System.Security.Cryptography.X509Certificates.X509Certificate2ImplUnix::.ctor()
extern void X509Certificate2ImplUnix__ctor_mF3392E6B555FBA22CBDC93DF0C53479586EBF327 (void);
// 0x0000066B System.Void System.Security.Cryptography.X509Certificates.X509CertificateCollection::.ctor()
extern void X509CertificateCollection__ctor_m237A27DC157785FBC50EBAF3562AAF0907BF97EB (void);
// 0x0000066C System.Void System.Security.Cryptography.X509Certificates.X509CertificateCollection::.ctor(System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern void X509CertificateCollection__ctor_mE486AD558BE6F0D8FD89AB88DE922DFA684C4AA7 (void);
// 0x0000066D System.Security.Cryptography.X509Certificates.X509Certificate System.Security.Cryptography.X509Certificates.X509CertificateCollection::get_Item(System.Int32)
extern void X509CertificateCollection_get_Item_m9EC79D4B62FB412278F92F3641969385252F6572 (void);
// 0x0000066E System.Int32 System.Security.Cryptography.X509Certificates.X509CertificateCollection::Add(System.Security.Cryptography.X509Certificates.X509Certificate)
extern void X509CertificateCollection_Add_mE6123E24D942801BC5A41196F92B0552015E110F (void);
// 0x0000066F System.Void System.Security.Cryptography.X509Certificates.X509CertificateCollection::AddRange(System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern void X509CertificateCollection_AddRange_m45914B11721824E8BE7FC2B2FC4ADD28F2C45EDB (void);
// 0x00000670 System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator System.Security.Cryptography.X509Certificates.X509CertificateCollection::GetEnumerator()
extern void X509CertificateCollection_GetEnumerator_mBD26239EE292204789D87CA73DBA5C898C2F4F14 (void);
// 0x00000671 System.Int32 System.Security.Cryptography.X509Certificates.X509CertificateCollection::GetHashCode()
extern void X509CertificateCollection_GetHashCode_m3F1BB9203AB722A6A9CF574390B0244F47485111 (void);
// 0x00000672 System.Void System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator::.ctor(System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern void X509CertificateEnumerator__ctor_m7FEB0CE4B71CB9CE860AAF29554279392249D237 (void);
// 0x00000673 System.Security.Cryptography.X509Certificates.X509Certificate System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator::get_Current()
extern void X509CertificateEnumerator_get_Current_mF256C5D7194A99B5C567F6E62806C153DA8864F1 (void);
// 0x00000674 System.Object System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator::System.Collections.IEnumerator.get_Current()
extern void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_mBBB3E8D212FD598C9C90F880D31B8227852F5075 (void);
// 0x00000675 System.Boolean System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator::System.Collections.IEnumerator.MoveNext()
extern void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m297DB4F7F1CB60E4B3E0BB2BBF1B80279E5E6936 (void);
// 0x00000676 System.Void System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator::System.Collections.IEnumerator.Reset()
extern void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_mC0D277DD2410455AE4EBCC14C4258F5F5F8677CC (void);
// 0x00000677 System.Boolean System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator::MoveNext()
extern void X509CertificateEnumerator_MoveNext_m2DCBF7B544E0E410B90DF3FD96B473B11A37E8DA (void);
// 0x00000678 System.Void System.Security.Cryptography.X509Certificates.X509CertificateImplCollection::.ctor()
extern void X509CertificateImplCollection__ctor_m9A1E10EE2FC833E8F8C938A91AD907BC437452E2 (void);
// 0x00000679 System.Void System.Security.Cryptography.X509Certificates.X509CertificateImplCollection::.ctor(System.Security.Cryptography.X509Certificates.X509CertificateImplCollection)
extern void X509CertificateImplCollection__ctor_mC7C4FAB75F44B53B8FE8D9CA3443F1411CD0BA11 (void);
// 0x0000067A System.Int32 System.Security.Cryptography.X509Certificates.X509CertificateImplCollection::get_Count()
extern void X509CertificateImplCollection_get_Count_mB81B621804D7FB640FDD4177610CEEDF0484378D (void);
// 0x0000067B System.Security.Cryptography.X509Certificates.X509CertificateImpl System.Security.Cryptography.X509Certificates.X509CertificateImplCollection::get_Item(System.Int32)
extern void X509CertificateImplCollection_get_Item_mA250EDFA1ACBE344A52C5CD818A6578CC07763A1 (void);
// 0x0000067C System.Void System.Security.Cryptography.X509Certificates.X509CertificateImplCollection::Add(System.Security.Cryptography.X509Certificates.X509CertificateImpl,System.Boolean)
extern void X509CertificateImplCollection_Add_mDF2DF7D94CEB84BCCC7292831C838FCDB1322B8C (void);
// 0x0000067D System.Security.Cryptography.X509Certificates.X509CertificateImplCollection System.Security.Cryptography.X509Certificates.X509CertificateImplCollection::Clone()
extern void X509CertificateImplCollection_Clone_m2AC54E741E6AB673D07464C05D27A3FF9F4112B0 (void);
// 0x0000067E System.Void System.Security.Cryptography.X509Certificates.X509CertificateImplCollection::Dispose()
extern void X509CertificateImplCollection_Dispose_m84BFDFFCD3B9DEC80EAFEBBFD156F1E246DBA617 (void);
// 0x0000067F System.Void System.Security.Cryptography.X509Certificates.X509CertificateImplCollection::Dispose(System.Boolean)
extern void X509CertificateImplCollection_Dispose_m77FC31F821A4066580E5F99685EB915466F034AC (void);
// 0x00000680 System.Void System.Security.Cryptography.X509Certificates.X509CertificateImplCollection::Finalize()
extern void X509CertificateImplCollection_Finalize_mE46191767109F329A19BBD0CC4BC1AA6E2C28A95 (void);
// 0x00000681 System.Security.Cryptography.X509Certificates.X509ChainImpl System.Security.Cryptography.X509Certificates.X509Chain::get_Impl()
extern void X509Chain_get_Impl_m4B712BC99067069BCE090957082F5FE17EE237A6 (void);
// 0x00000682 System.Void System.Security.Cryptography.X509Certificates.X509Chain::.ctor()
extern void X509Chain__ctor_mE938759A3C3CA007FB171DDD375312F03394D3F6 (void);
// 0x00000683 System.Void System.Security.Cryptography.X509Certificates.X509Chain::.ctor(System.Boolean)
extern void X509Chain__ctor_mF765A0057950154FC61FF1D112A8EFCB73314EC4 (void);
// 0x00000684 System.Void System.Security.Cryptography.X509Certificates.X509Chain::.ctor(System.Security.Cryptography.X509Certificates.X509ChainImpl)
extern void X509Chain__ctor_m6F2404400D7192FDA4CCB37DC67F18F12616000B (void);
// 0x00000685 System.Void System.Security.Cryptography.X509Certificates.X509Chain::.ctor(System.IntPtr)
extern void X509Chain__ctor_m2B6C93313DDBB62BE6E2AE073958B7662C5FBB7D (void);
// 0x00000686 System.Security.Cryptography.X509Certificates.X509ChainElementCollection System.Security.Cryptography.X509Certificates.X509Chain::get_ChainElements()
extern void X509Chain_get_ChainElements_m656A4A5EEA69BB8C4F5995DEF7C88AD9EB961AED (void);
// 0x00000687 System.Security.Cryptography.X509Certificates.X509ChainPolicy System.Security.Cryptography.X509Certificates.X509Chain::get_ChainPolicy()
extern void X509Chain_get_ChainPolicy_m947CBD207A65DB28D0EE7909004E44CBEDF34348 (void);
// 0x00000688 System.Boolean System.Security.Cryptography.X509Certificates.X509Chain::Build(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509Chain_Build_mF368D6BAE5925C8311B41BFBA08C5D91277C67E5 (void);
// 0x00000689 System.Void System.Security.Cryptography.X509Certificates.X509Chain::Reset()
extern void X509Chain_Reset_mE9CFDF83BD0B08994DEF29C30556429B2FD55860 (void);
// 0x0000068A System.Security.Cryptography.X509Certificates.X509Chain System.Security.Cryptography.X509Certificates.X509Chain::Create()
extern void X509Chain_Create_mA8CE0BB7C2DD4A66D217213F5D94B9EF21851E64 (void);
// 0x0000068B System.Void System.Security.Cryptography.X509Certificates.X509Chain::Dispose()
extern void X509Chain_Dispose_mC10F7CE7DAF0799F13C798184330C046222E305D (void);
// 0x0000068C System.Void System.Security.Cryptography.X509Certificates.X509Chain::Dispose(System.Boolean)
extern void X509Chain_Dispose_m24B2B8C5A29F06A8A1F8564EB0B39820616B3930 (void);
// 0x0000068D System.Void System.Security.Cryptography.X509Certificates.X509Chain::Finalize()
extern void X509Chain_Finalize_m76B5ABE85F3D5596788618FFD03C4831DB0EDB39 (void);
// 0x0000068E System.Void System.Security.Cryptography.X509Certificates.X509ChainElement::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainElement__ctor_m7088FBE9A69384945CA1A40CD973E1846FBD720F (void);
// 0x0000068F System.Security.Cryptography.X509Certificates.X509Certificate2 System.Security.Cryptography.X509Certificates.X509ChainElement::get_Certificate()
extern void X509ChainElement_get_Certificate_m6AF210BCAD2D94C19DFEF475BDAA84E5FF4AF105 (void);
// 0x00000690 System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509ChainElement::get_ChainElementStatus()
extern void X509ChainElement_get_ChainElementStatus_m24E928225ABCAD112A7D1D5A4ECDC2B89F3AF705 (void);
// 0x00000691 System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainElement::get_StatusFlags()
extern void X509ChainElement_get_StatusFlags_m8ECBC4E3E1149987BA1A837775A8AA68496B87D6 (void);
// 0x00000692 System.Void System.Security.Cryptography.X509Certificates.X509ChainElement::set_StatusFlags(System.Security.Cryptography.X509Certificates.X509ChainStatusFlags)
extern void X509ChainElement_set_StatusFlags_m7DF3FEE1B3B197C198FBA57215414725119B9B1F (void);
// 0x00000693 System.Int32 System.Security.Cryptography.X509Certificates.X509ChainElement::Count(System.Security.Cryptography.X509Certificates.X509ChainStatusFlags)
extern void X509ChainElement_Count_m2E6A6ADB8C118E8879B3C6E29093AE0172A4A0F6 (void);
// 0x00000694 System.Void System.Security.Cryptography.X509Certificates.X509ChainElement::Set(System.Security.Cryptography.X509Certificates.X509ChainStatus[],System.Int32&,System.Security.Cryptography.X509Certificates.X509ChainStatusFlags,System.Security.Cryptography.X509Certificates.X509ChainStatusFlags)
extern void X509ChainElement_Set_m7DB50EE91B2762D0623FA12E7A7B361602CE3D21 (void);
// 0x00000695 System.Void System.Security.Cryptography.X509Certificates.X509ChainElement::UncompressFlags()
extern void X509ChainElement_UncompressFlags_mB26BFCDCC458958F4A0A398DDA7516E7A54DC004 (void);
// 0x00000696 System.Void System.Security.Cryptography.X509Certificates.X509ChainElementCollection::.ctor()
extern void X509ChainElementCollection__ctor_mAEF26718ED02C8480A7041E3C6B66500950E013D (void);
// 0x00000697 System.Int32 System.Security.Cryptography.X509Certificates.X509ChainElementCollection::get_Count()
extern void X509ChainElementCollection_get_Count_m5448D972C3009FA67B3F4E13061B6012D561B78B (void);
// 0x00000698 System.Security.Cryptography.X509Certificates.X509ChainElement System.Security.Cryptography.X509Certificates.X509ChainElementCollection::get_Item(System.Int32)
extern void X509ChainElementCollection_get_Item_mC2A5AAFBAEF74FB085685620066058ACDC71D69A (void);
// 0x00000699 System.Object System.Security.Cryptography.X509Certificates.X509ChainElementCollection::get_SyncRoot()
extern void X509ChainElementCollection_get_SyncRoot_m435D18A280E3D7609FB490021AC2E1436BE61568 (void);
// 0x0000069A System.Void System.Security.Cryptography.X509Certificates.X509ChainElementCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern void X509ChainElementCollection_System_Collections_ICollection_CopyTo_m4909BF1B409D795DDD3D8D2206FA17B88AEA5B29 (void);
// 0x0000069B System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator System.Security.Cryptography.X509Certificates.X509ChainElementCollection::GetEnumerator()
extern void X509ChainElementCollection_GetEnumerator_m7C2AE0FD14790330E1C365116C6654B137E83817 (void);
// 0x0000069C System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ChainElementCollection::System.Collections.IEnumerable.GetEnumerator()
extern void X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m25F89A23A06D302469D28DDA337A0B0C5C463269 (void);
// 0x0000069D System.Void System.Security.Cryptography.X509Certificates.X509ChainElementCollection::Add(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainElementCollection_Add_mA032CE8B536713B0E070C9A211B084706D04F6C8 (void);
// 0x0000069E System.Void System.Security.Cryptography.X509Certificates.X509ChainElementCollection::Clear()
extern void X509ChainElementCollection_Clear_mF0C2AE76F73C525E29E808615576C7EE11409659 (void);
// 0x0000069F System.Boolean System.Security.Cryptography.X509Certificates.X509ChainElementCollection::Contains(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainElementCollection_Contains_m9B8619202762237150DD4DEDE83ECF8EA73C25FE (void);
// 0x000006A0 System.Void System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator::.ctor(System.Collections.IEnumerable)
extern void X509ChainElementEnumerator__ctor_m552B42A7693729672B7EF7F95B4211F6BD69AE6D (void);
// 0x000006A1 System.Security.Cryptography.X509Certificates.X509ChainElement System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator::get_Current()
extern void X509ChainElementEnumerator_get_Current_m0D089910DA01219AA1079B5C014349B28C106E68 (void);
// 0x000006A2 System.Object System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator::System.Collections.IEnumerator.get_Current()
extern void X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_mCE2DFBF81D34976171E7A936A04F87C0A625B025 (void);
// 0x000006A3 System.Boolean System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator::MoveNext()
extern void X509ChainElementEnumerator_MoveNext_mA8C7DDBB648F9B1F15FA3E169D4361374BE2B995 (void);
// 0x000006A4 System.Void System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator::Reset()
extern void X509ChainElementEnumerator_Reset_m4E0A51B45009F95A6E9B64C15F87CC9E48444A40 (void);
// 0x000006A5 System.Boolean System.Security.Cryptography.X509Certificates.X509ChainImpl::get_IsValid()
// 0x000006A6 System.Void System.Security.Cryptography.X509Certificates.X509ChainImpl::ThrowIfContextInvalid()
extern void X509ChainImpl_ThrowIfContextInvalid_m85AF480D1B87D8DA3D780040385CBB88D9B173CD (void);
// 0x000006A7 System.Security.Cryptography.X509Certificates.X509ChainElementCollection System.Security.Cryptography.X509Certificates.X509ChainImpl::get_ChainElements()
// 0x000006A8 System.Security.Cryptography.X509Certificates.X509ChainPolicy System.Security.Cryptography.X509Certificates.X509ChainImpl::get_ChainPolicy()
// 0x000006A9 System.Boolean System.Security.Cryptography.X509Certificates.X509ChainImpl::Build(System.Security.Cryptography.X509Certificates.X509Certificate2)
// 0x000006AA System.Void System.Security.Cryptography.X509Certificates.X509ChainImpl::AddStatus(System.Security.Cryptography.X509Certificates.X509ChainStatusFlags)
// 0x000006AB System.Void System.Security.Cryptography.X509Certificates.X509ChainImpl::Reset()
// 0x000006AC System.Void System.Security.Cryptography.X509Certificates.X509ChainImpl::Dispose()
extern void X509ChainImpl_Dispose_mB7A6D60B308E90F0173FA8160F7FF0A00709A25A (void);
// 0x000006AD System.Void System.Security.Cryptography.X509Certificates.X509ChainImpl::Dispose(System.Boolean)
extern void X509ChainImpl_Dispose_m9F8B96FFCB317F7F8C8E1ACA82061EA7404B732D (void);
// 0x000006AE System.Void System.Security.Cryptography.X509Certificates.X509ChainImpl::Finalize()
extern void X509ChainImpl_Finalize_mB70B742567D0E59AE522AA9F80800B308D87BC6F (void);
// 0x000006AF System.Void System.Security.Cryptography.X509Certificates.X509ChainImpl::.ctor()
extern void X509ChainImpl__ctor_m16F671EB135FB6E7E3CF6C42715D7106E6FAAF18 (void);
// 0x000006B0 System.Void System.Security.Cryptography.X509Certificates.X509ChainImplMono::.ctor(System.Boolean)
extern void X509ChainImplMono__ctor_m0ACF8D6927E705577989597DAAF5CFE6346B1A76 (void);
// 0x000006B1 System.Boolean System.Security.Cryptography.X509Certificates.X509ChainImplMono::get_IsValid()
extern void X509ChainImplMono_get_IsValid_m1140F06D0030BA5A55753B46FF61AE846B845F54 (void);
// 0x000006B2 System.Security.Cryptography.X509Certificates.X509ChainElementCollection System.Security.Cryptography.X509Certificates.X509ChainImplMono::get_ChainElements()
extern void X509ChainImplMono_get_ChainElements_m7EF05738D8235376B4B24F8AD95C59104C2ADAA7 (void);
// 0x000006B3 System.Security.Cryptography.X509Certificates.X509ChainPolicy System.Security.Cryptography.X509Certificates.X509ChainImplMono::get_ChainPolicy()
extern void X509ChainImplMono_get_ChainPolicy_m7E8861BDCB501BCD6371583BE75CBD4E0C754209 (void);
// 0x000006B4 System.Void System.Security.Cryptography.X509Certificates.X509ChainImplMono::AddStatus(System.Security.Cryptography.X509Certificates.X509ChainStatusFlags)
extern void X509ChainImplMono_AddStatus_mA336CCFEEB670FDF921E26F14C7CF32021A7D838 (void);
// 0x000006B5 System.Boolean System.Security.Cryptography.X509Certificates.X509ChainImplMono::Build(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainImplMono_Build_mE56172C044107FDA771CD1A9654948E079B2E6F9 (void);
// 0x000006B6 System.Void System.Security.Cryptography.X509Certificates.X509ChainImplMono::Reset()
extern void X509ChainImplMono_Reset_m1155C3B608B1BCEE6F5BD6351A3D63013F1CB292 (void);
// 0x000006B7 System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509ChainImplMono::get_Roots()
extern void X509ChainImplMono_get_Roots_m9F02570D4FEDCC596E09D548969BD82D1D6ACCE4 (void);
// 0x000006B8 System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509ChainImplMono::get_CertificateAuthorities()
extern void X509ChainImplMono_get_CertificateAuthorities_mA0B07CBA55DB93E0EA463F834358CB231F740282 (void);
// 0x000006B9 System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509ChainImplMono::get_LMRootStore()
extern void X509ChainImplMono_get_LMRootStore_m45317F8F6713CAB17096C8CC557A5660130D78A6 (void);
// 0x000006BA System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509ChainImplMono::get_UserRootStore()
extern void X509ChainImplMono_get_UserRootStore_m7D16C83BB80412C6697AB2A39D0AE8575659BFE1 (void);
// 0x000006BB System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509ChainImplMono::get_LMCAStore()
extern void X509ChainImplMono_get_LMCAStore_mC5519D87A02C14345D45110E5431A32E34D4E048 (void);
// 0x000006BC System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509ChainImplMono::get_UserCAStore()
extern void X509ChainImplMono_get_UserCAStore_m025075F5390E1CD388809D3857FF03FBB3CF12AB (void);
// 0x000006BD System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509ChainImplMono::get_CertificateCollection()
extern void X509ChainImplMono_get_CertificateCollection_m65D90AB33B635CD388E73D20EF02B3C1EDB57292 (void);
// 0x000006BE System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainImplMono::BuildChainFrom(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainImplMono_BuildChainFrom_m240087A2D01469F43D36A194637FAB93DE7D574F (void);
// 0x000006BF System.Security.Cryptography.X509Certificates.X509Certificate2 System.Security.Cryptography.X509Certificates.X509ChainImplMono::SelectBestFromCollection(System.Security.Cryptography.X509Certificates.X509Certificate2,System.Security.Cryptography.X509Certificates.X509Certificate2Collection)
extern void X509ChainImplMono_SelectBestFromCollection_m24FBA3CA0A492C7708FF11E763D8B1099F36BC06 (void);
// 0x000006C0 System.Security.Cryptography.X509Certificates.X509Certificate2 System.Security.Cryptography.X509Certificates.X509ChainImplMono::FindParent(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainImplMono_FindParent_mA64A7D939DE5F34944AFF6D58E2391BF47D3D0A6 (void);
// 0x000006C1 System.Boolean System.Security.Cryptography.X509Certificates.X509ChainImplMono::IsChainComplete(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainImplMono_IsChainComplete_m1FA60E2128868B2FDCB258B5D16F15411C540823 (void);
// 0x000006C2 System.Boolean System.Security.Cryptography.X509Certificates.X509ChainImplMono::IsSelfIssued(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainImplMono_IsSelfIssued_mDF746101F4634DE86C55A2AEB1936D8E68F751CA (void);
// 0x000006C3 System.Void System.Security.Cryptography.X509Certificates.X509ChainImplMono::ValidateChain(System.Security.Cryptography.X509Certificates.X509ChainStatusFlags)
extern void X509ChainImplMono_ValidateChain_mFC6C8BB61FC9DF99BDB656ACFACD0BB4BD57A177 (void);
// 0x000006C4 System.Void System.Security.Cryptography.X509Certificates.X509ChainImplMono::Process(System.Int32)
extern void X509ChainImplMono_Process_m469792B377599FD4347315913B28356AE5C57D0E (void);
// 0x000006C5 System.Void System.Security.Cryptography.X509Certificates.X509ChainImplMono::PrepareForNextCertificate(System.Int32)
extern void X509ChainImplMono_PrepareForNextCertificate_mD651DA79F6A64548599D510B1157DA0F44386872 (void);
// 0x000006C6 System.Void System.Security.Cryptography.X509Certificates.X509ChainImplMono::WrapUp()
extern void X509ChainImplMono_WrapUp_m1546AB65C518C3E3A175A1C907FB5641AB8D0233 (void);
// 0x000006C7 System.Void System.Security.Cryptography.X509Certificates.X509ChainImplMono::ProcessCertificateExtensions(System.Security.Cryptography.X509Certificates.X509ChainElement)
extern void X509ChainImplMono_ProcessCertificateExtensions_m24B4A9B57A31DED6492E65D47108DE0F0609990E (void);
// 0x000006C8 System.Boolean System.Security.Cryptography.X509Certificates.X509ChainImplMono::IsSignedWith(System.Security.Cryptography.X509Certificates.X509Certificate2,System.Security.Cryptography.AsymmetricAlgorithm)
extern void X509ChainImplMono_IsSignedWith_m841918EE1C928C94D6788FC5F446772123515913 (void);
// 0x000006C9 System.String System.Security.Cryptography.X509Certificates.X509ChainImplMono::GetSubjectKeyIdentifier(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainImplMono_GetSubjectKeyIdentifier_m0D8BA44577CA7AE09AA6E2998E8C77FCA7CC0BD0 (void);
// 0x000006CA System.String System.Security.Cryptography.X509Certificates.X509ChainImplMono::GetAuthorityKeyIdentifier(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainImplMono_GetAuthorityKeyIdentifier_mB0CD4B03B79394A27F337CDE31F3E1A7DD0BD0B1 (void);
// 0x000006CB System.String System.Security.Cryptography.X509Certificates.X509ChainImplMono::GetAuthorityKeyIdentifier(Mono.Security.X509.X509Crl)
extern void X509ChainImplMono_GetAuthorityKeyIdentifier_m63F1F5346CE98F07873A731826508D8E7E53B8B3 (void);
// 0x000006CC System.String System.Security.Cryptography.X509Certificates.X509ChainImplMono::GetAuthorityKeyIdentifier(Mono.Security.X509.X509Extension)
extern void X509ChainImplMono_GetAuthorityKeyIdentifier_m8815C38DD46B58C0322D8F4E217DB785637F12FA (void);
// 0x000006CD System.Void System.Security.Cryptography.X509Certificates.X509ChainImplMono::CheckRevocationOnChain(System.Security.Cryptography.X509Certificates.X509ChainStatusFlags)
extern void X509ChainImplMono_CheckRevocationOnChain_m8178244CB6B50DF10CBB3669ECAF005D649B2D01 (void);
// 0x000006CE System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainImplMono::CheckRevocation(System.Security.Cryptography.X509Certificates.X509Certificate2,System.Int32,System.Boolean)
extern void X509ChainImplMono_CheckRevocation_m8E1EDE1A95C4A09E39086F871B9D80C22D68BE35 (void);
// 0x000006CF System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainImplMono::CheckRevocation(System.Security.Cryptography.X509Certificates.X509Certificate2,System.Security.Cryptography.X509Certificates.X509Certificate2,System.Boolean)
extern void X509ChainImplMono_CheckRevocation_m73491E913ED7EB66E573B727C552A3012A229962 (void);
// 0x000006D0 Mono.Security.X509.X509Crl System.Security.Cryptography.X509Certificates.X509ChainImplMono::CheckCrls(System.String,System.String,Mono.Security.X509.X509Store)
extern void X509ChainImplMono_CheckCrls_mD2938D2C74DD5E9EA76205425E74A9EEB6C4C65E (void);
// 0x000006D1 Mono.Security.X509.X509Crl System.Security.Cryptography.X509Certificates.X509ChainImplMono::FindCrl(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509ChainImplMono_FindCrl_m10BA0B67DD7779475981229634AB40F239B60373 (void);
// 0x000006D2 System.Boolean System.Security.Cryptography.X509Certificates.X509ChainImplMono::ProcessCrlExtensions(Mono.Security.X509.X509Crl)
extern void X509ChainImplMono_ProcessCrlExtensions_mD92E0695E51E62A17AF7326E905C2592AD3F7072 (void);
// 0x000006D3 System.Boolean System.Security.Cryptography.X509Certificates.X509ChainImplMono::ProcessCrlEntryExtensions(Mono.Security.X509.X509Crl/X509CrlEntry)
extern void X509ChainImplMono_ProcessCrlEntryExtensions_mF1426E8193BC2553983F697EEB8F3035B70FE43E (void);
// 0x000006D4 System.Void System.Security.Cryptography.X509Certificates.X509ChainImplMono::.cctor()
extern void X509ChainImplMono__cctor_mA05EEC443B725B1B59B85DB2A6C9D4380D4CDCB2 (void);
// 0x000006D5 System.Void System.Security.Cryptography.X509Certificates.X509ChainPolicy::.ctor()
extern void X509ChainPolicy__ctor_m069F83BFFF3E79ED4CEBE8CF553CC65576F8D3D9 (void);
// 0x000006D6 System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509ChainPolicy::get_ExtraStore()
extern void X509ChainPolicy_get_ExtraStore_mAC7E1235E5CB8DD373B859F2B5C142638D7BBD2D (void);
// 0x000006D7 System.Void System.Security.Cryptography.X509Certificates.X509ChainPolicy::set_ExtraStore(System.Security.Cryptography.X509Certificates.X509Certificate2Collection)
extern void X509ChainPolicy_set_ExtraStore_mECC7CF51491C5A6E20F21CCD65F6EC66FF0BAF3E (void);
// 0x000006D8 System.Security.Cryptography.X509Certificates.X509RevocationFlag System.Security.Cryptography.X509Certificates.X509ChainPolicy::get_RevocationFlag()
extern void X509ChainPolicy_get_RevocationFlag_mB2DEC5907796536DB5304CAC972EA7B34C49CDAD (void);
// 0x000006D9 System.Security.Cryptography.X509Certificates.X509RevocationMode System.Security.Cryptography.X509Certificates.X509ChainPolicy::get_RevocationMode()
extern void X509ChainPolicy_get_RevocationMode_mDC8382C2ABF29DFE3972E40343F025A52488AF29 (void);
// 0x000006DA System.Void System.Security.Cryptography.X509Certificates.X509ChainPolicy::set_RevocationMode(System.Security.Cryptography.X509Certificates.X509RevocationMode)
extern void X509ChainPolicy_set_RevocationMode_mD31CF4CA6A09443829F91980F8AC98E21D5888AE (void);
// 0x000006DB System.Security.Cryptography.X509Certificates.X509VerificationFlags System.Security.Cryptography.X509Certificates.X509ChainPolicy::get_VerificationFlags()
extern void X509ChainPolicy_get_VerificationFlags_m66FC764D76DC6AF35F600BFFA710D826B2EF9B80 (void);
// 0x000006DC System.Void System.Security.Cryptography.X509Certificates.X509ChainPolicy::set_VerificationFlags(System.Security.Cryptography.X509Certificates.X509VerificationFlags)
extern void X509ChainPolicy_set_VerificationFlags_mA0C6910C20FCC1FB1DA1A35ED2E7429FA527A69E (void);
// 0x000006DD System.DateTime System.Security.Cryptography.X509Certificates.X509ChainPolicy::get_VerificationTime()
extern void X509ChainPolicy_get_VerificationTime_mDC1D5996C7077FECCB41D38974815178627EE82D (void);
// 0x000006DE System.Void System.Security.Cryptography.X509Certificates.X509ChainPolicy::Reset()
extern void X509ChainPolicy_Reset_mEE02175020B69CE1732C0E89A2AE60F0BA5B1417 (void);
// 0x000006DF System.Void System.Security.Cryptography.X509Certificates.X509ChainStatus::.ctor(System.Security.Cryptography.X509Certificates.X509ChainStatusFlags)
extern void X509ChainStatus__ctor_m5AD96E11604F8DB96673677AC2FFF68EBA550859 (void);
// 0x000006E0 System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainStatus::get_Status()
extern void X509ChainStatus_get_Status_m58AB369B6D5CDF2436644C1936953058FBBDEF99 (void);
// 0x000006E1 System.Void System.Security.Cryptography.X509Certificates.X509ChainStatus::set_Status(System.Security.Cryptography.X509Certificates.X509ChainStatusFlags)
extern void X509ChainStatus_set_Status_m55D0302200912F7B74F7805470551EB33C28246D (void);
// 0x000006E2 System.Void System.Security.Cryptography.X509Certificates.X509ChainStatus::set_StatusInformation(System.String)
extern void X509ChainStatus_set_StatusInformation_m0A4B88F2DEF120DB1D38E3EFF86170C9D272DB3F (void);
// 0x000006E3 System.String System.Security.Cryptography.X509Certificates.X509ChainStatus::GetInformation(System.Security.Cryptography.X509Certificates.X509ChainStatusFlags)
extern void X509ChainStatus_GetInformation_m2EE262A68356D15E2C691E84DA10B2E8E3EFED26 (void);
// 0x000006E4 System.Void System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::.ctor()
extern void X509EnhancedKeyUsageExtension__ctor_m7ECF7F6EEB96855182FD5466A04B4712B3F4C746 (void);
// 0x000006E5 System.Void System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::.ctor(System.Security.Cryptography.AsnEncodedData,System.Boolean)
extern void X509EnhancedKeyUsageExtension__ctor_mBDA55EB3C6594D4F6A06102B4EAF633F86C3E092 (void);
// 0x000006E6 System.Void System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::.ctor(System.Security.Cryptography.OidCollection,System.Boolean)
extern void X509EnhancedKeyUsageExtension__ctor_mE0CA01F89B6532A7E7B256F248C23B61F0A909CC (void);
// 0x000006E7 System.Void System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::CopyFrom(System.Security.Cryptography.AsnEncodedData)
extern void X509EnhancedKeyUsageExtension_CopyFrom_m9577FF97B8E3074B12B7056BF7E30AA0F84309BF (void);
// 0x000006E8 System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::Decode(System.Byte[])
extern void X509EnhancedKeyUsageExtension_Decode_m1110DD9BD5D717540009DB5D90805458CF07423C (void);
// 0x000006E9 System.Byte[] System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::Encode()
extern void X509EnhancedKeyUsageExtension_Encode_mBE23AB15722B1822172F860B5F7651EA871B5550 (void);
// 0x000006EA System.String System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::ToString(System.Boolean)
extern void X509EnhancedKeyUsageExtension_ToString_m9576793CE590716A3BB5AAC1632F946DF75605DE (void);
// 0x000006EB System.Void System.Security.Cryptography.X509Certificates.X509Extension::.ctor()
extern void X509Extension__ctor_mFC88C10C2C6718E6AE74CE37E10D4EBB8FD67BAC (void);
// 0x000006EC System.Void System.Security.Cryptography.X509Certificates.X509Extension::.ctor(System.String,System.Byte[],System.Boolean)
extern void X509Extension__ctor_m442E6EED0F68D0FA2D7D7B085BEAB11F35B9157F (void);
// 0x000006ED System.Boolean System.Security.Cryptography.X509Certificates.X509Extension::get_Critical()
extern void X509Extension_get_Critical_mEF827F09D95784210C2F8D5FD7BE27BB54803E35 (void);
// 0x000006EE System.Void System.Security.Cryptography.X509Certificates.X509Extension::set_Critical(System.Boolean)
extern void X509Extension_set_Critical_m19FBE6E0C8B9069E5A2C232DAAE06046AF18EFEF (void);
// 0x000006EF System.Void System.Security.Cryptography.X509Certificates.X509Extension::CopyFrom(System.Security.Cryptography.AsnEncodedData)
extern void X509Extension_CopyFrom_m674ABC4ECF701B708C28B546B42B7799CA24D4A9 (void);
// 0x000006F0 System.String System.Security.Cryptography.X509Certificates.X509Extension::FormatUnkownData(System.Byte[])
extern void X509Extension_FormatUnkownData_mD0463F5096A6937E95E3165048299CBA9A1A6F7D (void);
// 0x000006F1 System.Void System.Security.Cryptography.X509Certificates.X509ExtensionCollection::.ctor()
extern void X509ExtensionCollection__ctor_m279F781688D6D8D301D06282132D8A02DC864AE9 (void);
// 0x000006F2 System.Int32 System.Security.Cryptography.X509Certificates.X509ExtensionCollection::get_Count()
extern void X509ExtensionCollection_get_Count_m2708E6751246E4E5E64BAE8A99FEC79CFD61E9A1 (void);
// 0x000006F3 System.Object System.Security.Cryptography.X509Certificates.X509ExtensionCollection::get_SyncRoot()
extern void X509ExtensionCollection_get_SyncRoot_m22D5573E42FAAC75DC4DF16C962183E0CDC6AA0D (void);
// 0x000006F4 System.Security.Cryptography.X509Certificates.X509Extension System.Security.Cryptography.X509Certificates.X509ExtensionCollection::get_Item(System.String)
extern void X509ExtensionCollection_get_Item_m411222FFB9DF3584180BA8A09ED061CFD5774F9D (void);
// 0x000006F5 System.Int32 System.Security.Cryptography.X509Certificates.X509ExtensionCollection::Add(System.Security.Cryptography.X509Certificates.X509Extension)
extern void X509ExtensionCollection_Add_mC02BE13A042962E83E849483642F122AEADD2A79 (void);
// 0x000006F6 System.Void System.Security.Cryptography.X509Certificates.X509ExtensionCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern void X509ExtensionCollection_System_Collections_ICollection_CopyTo_mD9F01B71381CFDD12AF55B244025A99529465472 (void);
// 0x000006F7 System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator System.Security.Cryptography.X509Certificates.X509ExtensionCollection::GetEnumerator()
extern void X509ExtensionCollection_GetEnumerator_m7F4BDDDF50FD335D3F6A2AB490221D27524BD0BF (void);
// 0x000006F8 System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ExtensionCollection::System.Collections.IEnumerable.GetEnumerator()
extern void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m574551E2A07A5E860E812C597544B627EE4C4E55 (void);
// 0x000006F9 System.Void System.Security.Cryptography.X509Certificates.X509ExtensionCollection::.cctor()
extern void X509ExtensionCollection__cctor_m302D04DAABB7985BF831D50E7956ECFC3B2D199F (void);
// 0x000006FA System.Void System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator::.ctor(System.Collections.ArrayList)
extern void X509ExtensionEnumerator__ctor_m64C36BD278E62F11279957CC3A5F58A2D7025976 (void);
// 0x000006FB System.Security.Cryptography.X509Certificates.X509Extension System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator::get_Current()
extern void X509ExtensionEnumerator_get_Current_m6057D68B2D7750669A51630A1561FCE7F597E232 (void);
// 0x000006FC System.Object System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator::System.Collections.IEnumerator.get_Current()
extern void X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m5E7878C8D59799831C4350758873B6E0A358538D (void);
// 0x000006FD System.Boolean System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator::MoveNext()
extern void X509ExtensionEnumerator_MoveNext_mDCF343DCC4699DFB3D6E72D84AF8A6C11AB247C3 (void);
// 0x000006FE System.Void System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator::Reset()
extern void X509ExtensionEnumerator_Reset_m1DF9302CFBB6B288D94EE627E3D4DACD78B94A9C (void);
// 0x000006FF Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Helper2::GetMonoCertificate(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void X509Helper2_GetMonoCertificate_mA77B189F919C6BD9F882A59E3743A3AC2230206C (void);
// 0x00000700 System.Security.Cryptography.X509Certificates.X509ChainImpl System.Security.Cryptography.X509Certificates.X509Helper2::CreateChainImpl(System.Boolean)
extern void X509Helper2_CreateChainImpl_mABBFC689A2A031B8E93070F0485962AA5E8B214A (void);
// 0x00000701 System.Boolean System.Security.Cryptography.X509Certificates.X509Helper2::IsValid(System.Security.Cryptography.X509Certificates.X509ChainImpl)
extern void X509Helper2_IsValid_m2D40E03D265A1B1D53B6B8C726E833358AA4187D (void);
// 0x00000702 System.Void System.Security.Cryptography.X509Certificates.X509Helper2::ThrowIfContextInvalid(System.Security.Cryptography.X509Certificates.X509ChainImpl)
extern void X509Helper2_ThrowIfContextInvalid_m12213152E825627422EBDC2D88B524AE528B70B1 (void);
// 0x00000703 System.Exception System.Security.Cryptography.X509Certificates.X509Helper2::GetInvalidChainContextException()
extern void X509Helper2_GetInvalidChainContextException_m44B5C4113D72A0DA0EE7E1D4C7048FF631CA7D46 (void);
// 0x00000704 System.Void System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::.ctor()
extern void X509KeyUsageExtension__ctor_mE0EE68497EDEDD78213EEDC931D68DD35DF6340C (void);
// 0x00000705 System.Void System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::.ctor(System.Security.Cryptography.AsnEncodedData,System.Boolean)
extern void X509KeyUsageExtension__ctor_m1DE9CD768712A07E5803496501CCFDA81CFB2689 (void);
// 0x00000706 System.Void System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::.ctor(System.Security.Cryptography.X509Certificates.X509KeyUsageFlags,System.Boolean)
extern void X509KeyUsageExtension__ctor_m2FAA526721435935D0B10E0DB916AF56CFD84751 (void);
// 0x00000707 System.Security.Cryptography.X509Certificates.X509KeyUsageFlags System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::get_KeyUsages()
extern void X509KeyUsageExtension_get_KeyUsages_m1BD6C5BC6E3E380982C135B2E81731435345144F (void);
// 0x00000708 System.Void System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::CopyFrom(System.Security.Cryptography.AsnEncodedData)
extern void X509KeyUsageExtension_CopyFrom_mCBCF5CA17676B2030F6C82ED80A008EF029AF733 (void);
// 0x00000709 System.Security.Cryptography.X509Certificates.X509KeyUsageFlags System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::GetValidFlags(System.Security.Cryptography.X509Certificates.X509KeyUsageFlags)
extern void X509KeyUsageExtension_GetValidFlags_m82B1C7DA4F4E0F16090782189E1B4984D133BB45 (void);
// 0x0000070A System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::Decode(System.Byte[])
extern void X509KeyUsageExtension_Decode_mB64239ABDEA862E43E26CF324D3E41ECCF3088BC (void);
// 0x0000070B System.Byte[] System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::Encode()
extern void X509KeyUsageExtension_Encode_m38D20B2FB68EDF3594EDC0D9BA88800ABF305542 (void);
// 0x0000070C System.String System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::ToString(System.Boolean)
extern void X509KeyUsageExtension_ToString_m270131139984275D7571120A2984F0F5E77E79DC (void);
// 0x0000070D System.Void System.Security.Cryptography.X509Certificates.X509Store::.ctor(System.Security.Cryptography.X509Certificates.StoreName,System.Security.Cryptography.X509Certificates.StoreLocation)
extern void X509Store__ctor_m4BC006F268440F969A1E2550A4884AA9FB4FEB1E (void);
// 0x0000070E System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509Store::get_Certificates()
extern void X509Store_get_Certificates_m3605E1FE284AD55C48E874C49CD22C11828D2DA0 (void);
// 0x0000070F Mono.Security.X509.X509Stores System.Security.Cryptography.X509Certificates.X509Store::get_Factory()
extern void X509Store_get_Factory_m78BACD85C7BF54F835A7F9AA9F4F163F9E3F987A (void);
// 0x00000710 Mono.Security.X509.X509Store System.Security.Cryptography.X509Certificates.X509Store::get_Store()
extern void X509Store_get_Store_mF57B442ECFBBBB6980A36A80B64399779C20D253 (void);
// 0x00000711 System.Void System.Security.Cryptography.X509Certificates.X509Store::Close()
extern void X509Store_Close_mE444AB5FE15C355E4EF9369E7EB5F78F345320F0 (void);
// 0x00000712 System.Void System.Security.Cryptography.X509Certificates.X509Store::Dispose()
extern void X509Store_Dispose_mF1DA64DEAEEBAFEECD1E945B30BB8E169984D7E1 (void);
// 0x00000713 System.Void System.Security.Cryptography.X509Certificates.X509Store::Open(System.Security.Cryptography.X509Certificates.OpenFlags)
extern void X509Store_Open_mE65F1CB5311705EB955D576295B048C1155CCED8 (void);
// 0x00000714 System.Void System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::.ctor()
extern void X509SubjectKeyIdentifierExtension__ctor_mFC2A19B1301ABBA5F6C5CE407ABC4F4CA1EB49B1 (void);
// 0x00000715 System.Void System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::.ctor(System.Security.Cryptography.AsnEncodedData,System.Boolean)
extern void X509SubjectKeyIdentifierExtension__ctor_m421293DCB67F3A918098962166B8166FD9657D08 (void);
// 0x00000716 System.Void System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::.ctor(System.Byte[],System.Boolean)
extern void X509SubjectKeyIdentifierExtension__ctor_m610C9C804421B7CB519F3A5C7D66827CC1106EE1 (void);
// 0x00000717 System.Void System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::.ctor(System.String,System.Boolean)
extern void X509SubjectKeyIdentifierExtension__ctor_mECF6398B9841FB4533228FE77F51F93316EADE94 (void);
// 0x00000718 System.Void System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::.ctor(System.Security.Cryptography.X509Certificates.PublicKey,System.Boolean)
extern void X509SubjectKeyIdentifierExtension__ctor_m3DD2C4E1505A6E625499FBD833BFC076D28D83EB (void);
// 0x00000719 System.Void System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::.ctor(System.Security.Cryptography.X509Certificates.PublicKey,System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm,System.Boolean)
extern void X509SubjectKeyIdentifierExtension__ctor_m1858C1B050FAF65946B790737652E874781B4B56 (void);
// 0x0000071A System.String System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::get_SubjectKeyIdentifier()
extern void X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_mFF2A155072C67F430E7F4B178733A80388943CBA (void);
// 0x0000071B System.Void System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::CopyFrom(System.Security.Cryptography.AsnEncodedData)
extern void X509SubjectKeyIdentifierExtension_CopyFrom_m959BB16C6973B1B565D02ED9993AF9069019D77A (void);
// 0x0000071C System.Byte System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::FromHexChar(System.Char)
extern void X509SubjectKeyIdentifierExtension_FromHexChar_m0547F9E771F6BE506DEA650D696F7794F91B59A3 (void);
// 0x0000071D System.Byte System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::FromHexChars(System.Char,System.Char)
extern void X509SubjectKeyIdentifierExtension_FromHexChars_m7143D074BE9451C082422BA3FF118C17D873E5A1 (void);
// 0x0000071E System.Byte[] System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::FromHex(System.String)
extern void X509SubjectKeyIdentifierExtension_FromHex_m7A7DD49DB74BC4020A28007CDD5765765EC9DDB3 (void);
// 0x0000071F System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::Decode(System.Byte[])
extern void X509SubjectKeyIdentifierExtension_Decode_m9E9B87F478F55E202411CBCE3AA314CBF73C7F30 (void);
// 0x00000720 System.Byte[] System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::Encode()
extern void X509SubjectKeyIdentifierExtension_Encode_m8746D45CE4AC46543D3332BC6001FA1BC7A72CB3 (void);
// 0x00000721 System.String System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::ToString(System.Boolean)
extern void X509SubjectKeyIdentifierExtension_ToString_m9AE51334149DE6200E554F9C30E2DC5570D36707 (void);
// 0x00000722 System.Boolean System.Net.TcpValidationHelpers::ValidatePortNumber(System.Int32)
extern void TcpValidationHelpers_ValidatePortNumber_m87E6293214842C630C35F9863ADE5AD844235480 (void);
// 0x00000723 System.Void System.Net.HttpVersion::.cctor()
extern void HttpVersion__cctor_m237BC8431A75E09C3610837B9FECBDD305CF2622 (void);
// 0x00000724 System.Boolean System.Net.IPAddress::get_IsIPv4()
extern void IPAddress_get_IsIPv4_m912994B89DDE21F85E4FE40381CDFF1688BE3F38 (void);
// 0x00000725 System.Boolean System.Net.IPAddress::get_IsIPv6()
extern void IPAddress_get_IsIPv6_m957F80A11C7E40C2CCAE554F12AA547E387B6884 (void);
// 0x00000726 System.UInt32 System.Net.IPAddress::get_PrivateAddress()
extern void IPAddress_get_PrivateAddress_mE1CD072F1E7BDF981FD71CBD984F5FD2A2EC6399 (void);
// 0x00000727 System.Void System.Net.IPAddress::set_PrivateAddress(System.UInt32)
extern void IPAddress_set_PrivateAddress_m73BD8A117538CBDB80F8FF47D31609FA4855D74E (void);
// 0x00000728 System.UInt32 System.Net.IPAddress::get_PrivateScopeId()
extern void IPAddress_get_PrivateScopeId_m3C01EDB8971607BAC0398557E295B5A818191532 (void);
// 0x00000729 System.Void System.Net.IPAddress::set_PrivateScopeId(System.UInt32)
extern void IPAddress_set_PrivateScopeId_m2BA43F20C3B975BDAC6E330C09E09BFD34594A42 (void);
// 0x0000072A System.Void System.Net.IPAddress::.ctor(System.Int64)
extern void IPAddress__ctor_mC240D2060BA0F5A96D286CDE3772FE4354D7030B (void);
// 0x0000072B System.Void System.Net.IPAddress::.ctor(System.Byte[],System.Int64)
extern void IPAddress__ctor_m8D2578858CF6F8E56591F45C8A14150DEFFA44F5 (void);
// 0x0000072C System.Void System.Net.IPAddress::.ctor(System.ReadOnlySpan`1<System.Byte>,System.Int64)
extern void IPAddress__ctor_mFD3C42E1EECC3099D6E347238FB2A0EBE822AF19 (void);
// 0x0000072D System.Void System.Net.IPAddress::.ctor(System.UInt16*,System.Int32,System.UInt32)
extern void IPAddress__ctor_m9673193087826D4DFAA57CE98431390FD47C512F (void);
// 0x0000072E System.Void System.Net.IPAddress::.ctor(System.UInt16[],System.UInt32)
extern void IPAddress__ctor_m16761A05E56D81D4519A8E0FEBB7910E17A1FB7C (void);
// 0x0000072F System.Boolean System.Net.IPAddress::TryParse(System.String,System.Net.IPAddress&)
extern void IPAddress_TryParse_mB8DC9EE090ED3BE8F8C9D419759AA9FF4A498D3B (void);
// 0x00000730 System.Net.IPAddress System.Net.IPAddress::Parse(System.String)
extern void IPAddress_Parse_mD7BEF4D6060D8BE776F559C5F81F195A9917CF1C (void);
// 0x00000731 System.Boolean System.Net.IPAddress::TryWriteBytes(System.Span`1<System.Byte>,System.Int32&)
extern void IPAddress_TryWriteBytes_mB6CA9DA47541FD37A67BCF614983BDBD3581346A (void);
// 0x00000732 System.Void System.Net.IPAddress::WriteIPv6Bytes(System.Span`1<System.Byte>)
extern void IPAddress_WriteIPv6Bytes_m4505E41EC6C2E484293B39735D63DF58BF37539B (void);
// 0x00000733 System.Void System.Net.IPAddress::WriteIPv4Bytes(System.Span`1<System.Byte>)
extern void IPAddress_WriteIPv4Bytes_m4E03460BC962536BD927917BD374A8B1E1C23C40 (void);
// 0x00000734 System.Byte[] System.Net.IPAddress::GetAddressBytes()
extern void IPAddress_GetAddressBytes_m1501E0143C791E3A065E508F5535D8E206652EC9 (void);
// 0x00000735 System.Net.Sockets.AddressFamily System.Net.IPAddress::get_AddressFamily()
extern void IPAddress_get_AddressFamily_m1CE4BCCE499BD70B22F9E37B3F266F9306A98C21 (void);
// 0x00000736 System.Int64 System.Net.IPAddress::get_ScopeId()
extern void IPAddress_get_ScopeId_m464E5C5597F29B05135EDCA6DDB1667EFA70EEF8 (void);
// 0x00000737 System.String System.Net.IPAddress::ToString()
extern void IPAddress_ToString_m91FE727877BEAA0115F497387E2E9CB0B45C3895 (void);
// 0x00000738 System.Boolean System.Net.IPAddress::IsLoopback(System.Net.IPAddress)
extern void IPAddress_IsLoopback_m673C9C63D59BF356AB7964CEE148EE1F3E01CC19 (void);
// 0x00000739 System.Boolean System.Net.IPAddress::Equals(System.Object,System.Boolean)
extern void IPAddress_Equals_mC4514B3B7F9B95B79A1121706C115AECE61636C7 (void);
// 0x0000073A System.Boolean System.Net.IPAddress::Equals(System.Object)
extern void IPAddress_Equals_m5EA56A0CBC4F66012C11628D8CC75E03E5A8C462 (void);
// 0x0000073B System.Int32 System.Net.IPAddress::GetHashCode()
extern void IPAddress_GetHashCode_m243850627E81961E58E3A7A0A39A1F365C47A4BA (void);
// 0x0000073C System.Net.IPAddress System.Net.IPAddress::MapToIPv6()
extern void IPAddress_MapToIPv6_m3D243981B6A8235516A4D8A68FC555B59675DE33 (void);
// 0x0000073D System.Byte[] System.Net.IPAddress::ThrowAddressNullException()
extern void IPAddress_ThrowAddressNullException_mF55B22F51151F17731038C12D118EBEC63C9CC4A (void);
// 0x0000073E System.Void System.Net.IPAddress::.cctor()
extern void IPAddress__cctor_mB1459880D331879EB11466988E78D88EBA0F6735 (void);
// 0x0000073F System.Void System.Net.IPAddress/ReadOnlyIPAddress::.ctor(System.Int64)
extern void ReadOnlyIPAddress__ctor_m0E47AC9070535FCCC4796C165AC8DAB6FA2B5AC5 (void);
// 0x00000740 System.Net.IPAddress System.Net.IPAddressParser::Parse(System.ReadOnlySpan`1<System.Char>,System.Boolean)
extern void IPAddressParser_Parse_m924FDF6137E57D2C205C767578A902FAC6AF9F6C (void);
// 0x00000741 System.String System.Net.IPAddressParser::IPv4AddressToString(System.UInt32)
extern void IPAddressParser_IPv4AddressToString_mED56559517ADF1172DEE5FFBA56B7A3C66060303 (void);
// 0x00000742 System.Void System.Net.IPAddressParser::IPv4AddressToString(System.UInt32,System.Text.StringBuilder)
extern void IPAddressParser_IPv4AddressToString_mC3C2E797E621FC478FCDBADFE9DE20909516D706 (void);
// 0x00000743 System.Int32 System.Net.IPAddressParser::IPv4AddressToStringHelper(System.UInt32,System.Char*)
extern void IPAddressParser_IPv4AddressToStringHelper_m732D3622C399BF418CC039E34BB8EBA0F5B7C581 (void);
// 0x00000744 System.String System.Net.IPAddressParser::IPv6AddressToString(System.UInt16[],System.UInt32)
extern void IPAddressParser_IPv6AddressToString_mA5E8E4CCAA57C745E4CF564CB6CA263661A1BB13 (void);
// 0x00000745 System.Text.StringBuilder System.Net.IPAddressParser::IPv6AddressToStringHelper(System.UInt16[],System.UInt32)
extern void IPAddressParser_IPv6AddressToStringHelper_m5E32EC869045EA58C14FDB598476013D76568A27 (void);
// 0x00000746 System.Void System.Net.IPAddressParser::FormatIPv4AddressNumber(System.Int32,System.Char*,System.Int32&)
extern void IPAddressParser_FormatIPv4AddressNumber_m6C347346B73C32788716D03C3FB4DEA954EE37C2 (void);
// 0x00000747 System.Boolean System.Net.IPAddressParser::Ipv4StringToAddress(System.ReadOnlySpan`1<System.Char>,System.Int64&)
extern void IPAddressParser_Ipv4StringToAddress_m0E8B9B120883B4280B6DB894C47C5527D0A8C6E9 (void);
// 0x00000748 System.Boolean System.Net.IPAddressParser::Ipv6StringToAddress(System.ReadOnlySpan`1<System.Char>,System.UInt16*,System.Int32,System.UInt32&)
extern void IPAddressParser_Ipv6StringToAddress_m2F6666E91E78CB63F69EEDAB23F5DBA65353734A (void);
// 0x00000749 System.Void System.Net.IPAddressParser::AppendSections(System.UInt16[],System.Int32,System.Int32,System.Text.StringBuilder)
extern void IPAddressParser_AppendSections_mB41667569210B39E7A732CE4EDDB684FF7B97C94 (void);
// 0x0000074A System.Void System.Net.IPAddressParser::AppendHex(System.UInt16,System.Text.StringBuilder)
extern void IPAddressParser_AppendHex_m078E3C19C653E140FC8E6943F292CACBB29A13AD (void);
// 0x0000074B System.UInt32 System.Net.IPAddressParser::ExtractIPv4Address(System.UInt16[])
extern void IPAddressParser_ExtractIPv4Address_mF574B7C9D0B4DC4C22DBC0F2A6701FC816A2128D (void);
// 0x0000074C System.UInt16 System.Net.IPAddressParser::Reverse(System.UInt16)
extern void IPAddressParser_Reverse_mD663C9994E59E47531F7D69E576B14D0572676D5 (void);
// 0x0000074D System.Net.Sockets.AddressFamily System.Net.IPEndPoint::get_AddressFamily()
extern void IPEndPoint_get_AddressFamily_m9941B51AEDFC4A65B614DCFFBD6A11644DBA5851 (void);
// 0x0000074E System.Void System.Net.IPEndPoint::.ctor(System.Int64,System.Int32)
extern void IPEndPoint__ctor_m16D9FC6DA967F79B612A59A804E1374B9D324E4A (void);
// 0x0000074F System.Void System.Net.IPEndPoint::.ctor(System.Net.IPAddress,System.Int32)
extern void IPEndPoint__ctor_m902C98F9E3F36B20B3C2E030AA91B62E2BC7A85A (void);
// 0x00000750 System.Net.IPAddress System.Net.IPEndPoint::get_Address()
extern void IPEndPoint_get_Address_m72F783CB76E10E9DBDF680CCC1DAAED201BABB1C (void);
// 0x00000751 System.Int32 System.Net.IPEndPoint::get_Port()
extern void IPEndPoint_get_Port_mFBE1AF1C9CC7E68A46BF46AD3869CC9DC01CF429 (void);
// 0x00000752 System.String System.Net.IPEndPoint::ToString()
extern void IPEndPoint_ToString_mC285E84E3FB9A15B223DD4401DDFC1E4171E766A (void);
// 0x00000753 System.Net.SocketAddress System.Net.IPEndPoint::Serialize()
extern void IPEndPoint_Serialize_m4495698C21946D39519728ADC8D4FB9860B52DD5 (void);
// 0x00000754 System.Net.EndPoint System.Net.IPEndPoint::Create(System.Net.SocketAddress)
extern void IPEndPoint_Create_m3F397111A683D18A38FC7275D55BB94B0FC7350D (void);
// 0x00000755 System.Boolean System.Net.IPEndPoint::Equals(System.Object)
extern void IPEndPoint_Equals_m480BC01DF0A7D8DA996A030546DCECBE2B5D6AF7 (void);
// 0x00000756 System.Int32 System.Net.IPEndPoint::GetHashCode()
extern void IPEndPoint_GetHashCode_m888B6D706296BF20224488C4909807D26655E4F3 (void);
// 0x00000757 System.Void System.Net.IPEndPoint::.cctor()
extern void IPEndPoint__cctor_mF8F48FE436F60E44A928B1967AA49F472E8D1D05 (void);
// 0x00000758 System.Net.ICredentials System.Net.CredentialCache::get_DefaultCredentials()
extern void CredentialCache_get_DefaultCredentials_mFD45861A5151E546B6DF04F6D527E82214EA4143 (void);
// 0x00000759 System.Void System.Net.SystemNetworkCredential::.ctor()
extern void SystemNetworkCredential__ctor_m6C0643A3B325903C6E20EEC9940A6646F4AE77B6 (void);
// 0x0000075A System.Void System.Net.SystemNetworkCredential::.cctor()
extern void SystemNetworkCredential__cctor_mC2950C5826DFEFF2A593B0E00769DCAF75257AF3 (void);
// 0x0000075B System.Net.Sockets.AddressFamily System.Net.EndPoint::get_AddressFamily()
extern void EndPoint_get_AddressFamily_m44AC2CD5C6868280BEBABF4090D3AFCA2E254537 (void);
// 0x0000075C System.Net.SocketAddress System.Net.EndPoint::Serialize()
extern void EndPoint_Serialize_mB045A92E21967EF34B1D5A32EF4E645CC1871BA3 (void);
// 0x0000075D System.Net.EndPoint System.Net.EndPoint::Create(System.Net.SocketAddress)
extern void EndPoint_Create_m09DA8D1A544D556AD0A1A08E3B0CBE9052615983 (void);
// 0x0000075E System.Void System.Net.EndPoint::.ctor()
extern void EndPoint__ctor_m4CD393A3D801B696473199A2D2F7EC80E8036F5D (void);
// 0x0000075F System.Boolean System.Net.ICertificatePolicy::CheckValidationResult(System.Net.ServicePoint,System.Security.Cryptography.X509Certificates.X509Certificate,System.Net.WebRequest,System.Int32)
// 0x00000760 System.String System.Net.IPHostEntry::get_HostName()
extern void IPHostEntry_get_HostName_m04050E3890C11BC5CB02BF48FBDD8AB4938FC390 (void);
// 0x00000761 System.Void System.Net.IPHostEntry::set_HostName(System.String)
extern void IPHostEntry_set_HostName_m4218E75B9A5D143AF361FEC708ED40CD3829920A (void);
// 0x00000762 System.Void System.Net.IPHostEntry::set_Aliases(System.String[])
extern void IPHostEntry_set_Aliases_mCAB3DC9E9B474EA49C1A80E8010FF4932316B266 (void);
// 0x00000763 System.Net.IPAddress[] System.Net.IPHostEntry::get_AddressList()
extern void IPHostEntry_get_AddressList_m9D14D52EFB41C53C9C4A36D438E1333A99B7AA71 (void);
// 0x00000764 System.Void System.Net.IPHostEntry::set_AddressList(System.Net.IPAddress[])
extern void IPHostEntry_set_AddressList_mFC955717B52B0D1DC71059FC7B2ED6659E4729D8 (void);
// 0x00000765 System.Void System.Net.IPHostEntry::.ctor()
extern void IPHostEntry__ctor_mF2D2692AFFD3ED81CAF30F4CD42B7B0843952398 (void);
// 0x00000766 System.Void System.Net.InternalException::.ctor()
extern void InternalException__ctor_m20A09E66E185FB1449C71227304AF40936C8011A (void);
// 0x00000767 System.Boolean System.Net.NclUtilities::IsAddressLocal(System.Net.IPAddress)
extern void NclUtilities_IsAddressLocal_mA952662DF0A2D8BF1AFF9E74B1FF46DBCA90812F (void);
// 0x00000768 System.Net.IPHostEntry System.Net.NclUtilities::GetLocalHost()
extern void NclUtilities_GetLocalHost_m7C77756A35C2F3CC268016902FA61CF88C5AD56F (void);
// 0x00000769 System.Net.IPAddress[] System.Net.NclUtilities::get_LocalAddresses()
extern void NclUtilities_get_LocalAddresses_mC5E3B1D849F83965C3F09ECF963B3CB252A8F22F (void);
// 0x0000076A System.Object System.Net.NclUtilities::get_LocalAddressesLock()
extern void NclUtilities_get_LocalAddressesLock_mDF3BAF033A2D11D70D5703B65302AB09525F7880 (void);
// 0x0000076B System.Boolean System.Net.ValidationHelper::IsBlankString(System.String)
extern void ValidationHelper_IsBlankString_mEE97DDAD061D54175C26B9E51E891C4F346E33B6 (void);
// 0x0000076C System.Boolean System.Net.ValidationHelper::ValidateTcpPort(System.Int32)
extern void ValidationHelper_ValidateTcpPort_m9042E85AFB8EA2756E2940065C116104C008CCB2 (void);
// 0x0000076D System.Void System.Net.ValidationHelper::.cctor()
extern void ValidationHelper__cctor_m24C6635AA32CE592CED801B39532BB4B7445AFC1 (void);
// 0x0000076E System.NotImplementedException System.Net.ExceptionHelper::get_MethodNotImplementedException()
extern void ExceptionHelper_get_MethodNotImplementedException_mF8678EE4622F1BBEEC6CDD54293E4D1B40A6D251 (void);
// 0x0000076F System.NotImplementedException System.Net.ExceptionHelper::get_PropertyNotImplementedException()
extern void ExceptionHelper_get_PropertyNotImplementedException_m5029CA867B083BAEE8D3D4577B1F571FEDDDE590 (void);
// 0x00000770 System.Void System.Net.NetworkCredential::.ctor(System.String,System.String,System.String)
extern void NetworkCredential__ctor_m6AACF996F73DA7CE81D33CD301DF98362E7D3EB2 (void);
// 0x00000771 System.Void System.Net.NetworkCredential::set_UserName(System.String)
extern void NetworkCredential_set_UserName_m1C8F2119945861B5B7A512896A57F5E2C5537B2F (void);
// 0x00000772 System.Void System.Net.NetworkCredential::set_Password(System.String)
extern void NetworkCredential_set_Password_mF0B964234CE60FF322135B014FFD8C28FEF5428B (void);
// 0x00000773 System.Void System.Net.NetworkCredential::set_Domain(System.String)
extern void NetworkCredential_set_Domain_mF7A8246CA8329E6A86A7B2B5FA8C972371F5AABA (void);
// 0x00000774 System.Void System.Net.ProtocolViolationException::.ctor()
extern void ProtocolViolationException__ctor_m0999A59A95131FA8BB4D6685FBF4BC0B74E01E34 (void);
// 0x00000775 System.Void System.Net.ProtocolViolationException::.ctor(System.String)
extern void ProtocolViolationException__ctor_m2C2665583590BF7DB9B53A7AF126FEFDDE364BA5 (void);
// 0x00000776 System.Void System.Net.ProtocolViolationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void ProtocolViolationException__ctor_mD9B5606196795D66E9A101AE92EE2A8D9B9A7440 (void);
// 0x00000777 System.Void System.Net.ProtocolViolationException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void ProtocolViolationException_System_Runtime_Serialization_ISerializable_GetObjectData_mA112E599073DE2CF2AC7F84E6CD354BFEBFA3170 (void);
// 0x00000778 System.Void System.Net.ProtocolViolationException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void ProtocolViolationException_GetObjectData_mF3C17B3E564B658959AF1A026E232D8E2D2CCEB9 (void);
// 0x00000779 System.Net.Sockets.AddressFamily System.Net.SocketAddress::get_Family()
extern void SocketAddress_get_Family_mF66C07BE26931A763DF421310667F1C7EE82EA20 (void);
// 0x0000077A System.Int32 System.Net.SocketAddress::get_Size()
extern void SocketAddress_get_Size_mBBA7758844EE6209A287989E0DBAE534C9A21A11 (void);
// 0x0000077B System.Byte System.Net.SocketAddress::get_Item(System.Int32)
extern void SocketAddress_get_Item_mFF97A6A1AF0D8AB36C662282C477B5DBAFD10FBD (void);
// 0x0000077C System.Void System.Net.SocketAddress::set_Item(System.Int32,System.Byte)
extern void SocketAddress_set_Item_m749FA727E65F8BE8E095BFBCD56091796CFF6B84 (void);
// 0x0000077D System.Void System.Net.SocketAddress::.ctor(System.Net.Sockets.AddressFamily,System.Int32)
extern void SocketAddress__ctor_mD56F67A8077F542F2DA0ABAF693EE192F5910ECB (void);
// 0x0000077E System.Void System.Net.SocketAddress::.ctor(System.Net.IPAddress)
extern void SocketAddress__ctor_mE024ADACE4B62D1FF2969E8D59E3D6A4B567DC30 (void);
// 0x0000077F System.Void System.Net.SocketAddress::.ctor(System.Net.IPAddress,System.Int32)
extern void SocketAddress__ctor_m33DA845BE27401842BFABDF79738F8EFA60B12B6 (void);
// 0x00000780 System.Net.IPAddress System.Net.SocketAddress::GetIPAddress()
extern void SocketAddress_GetIPAddress_m9F349596DE4F881222A989059162AF5880FBAA0D (void);
// 0x00000781 System.Net.IPEndPoint System.Net.SocketAddress::GetIPEndPoint()
extern void SocketAddress_GetIPEndPoint_m1B1532600216A9545A7CAC1EA49F23CBAC5C8C17 (void);
// 0x00000782 System.Boolean System.Net.SocketAddress::Equals(System.Object)
extern void SocketAddress_Equals_m68D7D3DC11E1070E43492BA30FD9AE5AEA51C49D (void);
// 0x00000783 System.Int32 System.Net.SocketAddress::GetHashCode()
extern void SocketAddress_GetHashCode_mA5B524859742D73CF5870F6984360E15F09FA719 (void);
// 0x00000784 System.String System.Net.SocketAddress::ToString()
extern void SocketAddress_ToString_m1C4AEB870E76472C9E3800224F2400A66DF8E46A (void);
// 0x00000785 System.Void System.Net.WebException::.ctor()
extern void WebException__ctor_m1F229FB262E89CA90779094218A3066F532EB8CB (void);
// 0x00000786 System.Void System.Net.WebException::.ctor(System.String,System.Exception)
extern void WebException__ctor_m8075F9F7345CECCBDB276E0B67DE820A9F7D03E9 (void);
// 0x00000787 System.Void System.Net.WebException::.ctor(System.String,System.Net.WebExceptionStatus)
extern void WebException__ctor_m6C829021B5388956F84830FC249915324C1453A1 (void);
// 0x00000788 System.Void System.Net.WebException::.ctor(System.String,System.Exception,System.Net.WebExceptionStatus,System.Net.WebResponse)
extern void WebException__ctor_m937A4670EAF10FD9EFA1E0AEAAE27515293339FD (void);
// 0x00000789 System.Void System.Net.WebException::.ctor(System.String,System.String,System.Exception,System.Net.WebExceptionStatus,System.Net.WebResponse)
extern void WebException__ctor_mA1609CA415DDD5C7C4AF37E9CD2CE77330C43384 (void);
// 0x0000078A System.Void System.Net.WebException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebException__ctor_m6D36D5DBE171207E973C5BDFC9A57BE2A1E5DA7C (void);
// 0x0000078B System.Void System.Net.WebException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebException_System_Runtime_Serialization_ISerializable_GetObjectData_mAEABC5E4A1BAE8AD395EFC404CCEAA00345AD628 (void);
// 0x0000078C System.Void System.Net.WebException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebException_GetObjectData_mF735C851AF332C94DDDAB38C0C24AA1871DC2901 (void);
// 0x0000078D System.String System.Net.WebExceptionMapping::GetWebStatusString(System.Net.WebExceptionStatus)
extern void WebExceptionMapping_GetWebStatusString_mC6F3D28D6DCDD3E8674CBDA5CA684DF603DBCF69 (void);
// 0x0000078E System.Void System.Net.WebExceptionMapping::.cctor()
extern void WebExceptionMapping__cctor_m56C96AB4B5D49C51756576BE119AC1096CDD6C4C (void);
// 0x0000078F System.Void System.Net.WebHeaderCollection::NormalizeCommonHeaders()
extern void WebHeaderCollection_NormalizeCommonHeaders_mE1D83B9D009EB876D33D1EB93CDC65F4F97B5B77 (void);
// 0x00000790 System.Collections.Specialized.NameValueCollection System.Net.WebHeaderCollection::get_InnerCollection()
extern void WebHeaderCollection_get_InnerCollection_m544FF08AD232BC5BDD8C5109DF6CD21C5ECAE2D0 (void);
// 0x00000791 System.Void System.Net.WebHeaderCollection::AddInternal(System.String,System.String)
extern void WebHeaderCollection_AddInternal_m0EE002E2F815150CA5C43EC865F6D193FD4B7777 (void);
// 0x00000792 System.String System.Net.WebHeaderCollection::CheckBadChars(System.String,System.Boolean)
extern void WebHeaderCollection_CheckBadChars_m1F7BCD4BF0DC01AF8C1378AD02862D9900B67D71 (void);
// 0x00000793 System.Boolean System.Net.WebHeaderCollection::ContainsNonAsciiChars(System.String)
extern void WebHeaderCollection_ContainsNonAsciiChars_mAE8D797265C12FBCAC784B15D734914273ED7AFD (void);
// 0x00000794 System.Void System.Net.WebHeaderCollection::ThrowOnRestrictedHeader(System.String)
extern void WebHeaderCollection_ThrowOnRestrictedHeader_mFBE0E21167D9EB5485BD3DC18B505FEC9BF6CF51 (void);
// 0x00000795 System.Void System.Net.WebHeaderCollection::Add(System.String,System.String)
extern void WebHeaderCollection_Add_m3E612375F241E3383BA9EC01972C5A5FCD9384CF (void);
// 0x00000796 System.String System.Net.WebHeaderCollection::ToString()
extern void WebHeaderCollection_ToString_m60D2996FFE881E3D8AAF700E761ECA34B0A24D86 (void);
// 0x00000797 System.String System.Net.WebHeaderCollection::GetAsString(System.Collections.Specialized.NameValueCollection,System.Boolean,System.Boolean)
extern void WebHeaderCollection_GetAsString_m7E801872DAF279F8228F57FF2EA5336CD29F2E4F (void);
// 0x00000798 System.Void System.Net.WebHeaderCollection::.ctor()
extern void WebHeaderCollection__ctor_m9C77B3E5FD0A3637283AE2B7084F5ED5D8CD7369 (void);
// 0x00000799 System.Void System.Net.WebHeaderCollection::.ctor(System.Net.WebHeaderCollectionType)
extern void WebHeaderCollection__ctor_mE6EBDC6DA6CA46B83FD87FA51A93BACCA7FCDB27 (void);
// 0x0000079A System.Void System.Net.WebHeaderCollection::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection__ctor_m7FAAE403639DC42E7FFED6006B1D10BF5A1B4B5F (void);
// 0x0000079B System.Void System.Net.WebHeaderCollection::OnDeserialization(System.Object)
extern void WebHeaderCollection_OnDeserialization_mDF73EE0F5BBF3AEE9C37273B79738A47530BECA4 (void);
// 0x0000079C System.Void System.Net.WebHeaderCollection::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection_GetObjectData_mA9D238943F3E6D4DF3A1156A0EA6729F5CA6F911 (void);
// 0x0000079D System.Void System.Net.WebHeaderCollection::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_mCB9569FEA54BEF369CF3BE5F56F36072D01D712D (void);
// 0x0000079E System.String System.Net.WebHeaderCollection::Get(System.String)
extern void WebHeaderCollection_Get_m7EB2DFD01DAC1EA2CC4CA7290459BF47B06CABED (void);
// 0x0000079F System.Collections.IEnumerator System.Net.WebHeaderCollection::GetEnumerator()
extern void WebHeaderCollection_GetEnumerator_m8A77AE9A2EFFD0F12E60A1DDF51BBBF2C9F5904B (void);
// 0x000007A0 System.Int32 System.Net.WebHeaderCollection::get_Count()
extern void WebHeaderCollection_get_Count_mFD0A0FD946CC97663996F70E67BF64851F3A4B26 (void);
// 0x000007A1 System.String System.Net.WebHeaderCollection::Get(System.Int32)
extern void WebHeaderCollection_Get_m99BCFF243942F451A075C334C2B67767B2332348 (void);
// 0x000007A2 System.String System.Net.WebHeaderCollection::GetKey(System.Int32)
extern void WebHeaderCollection_GetKey_m568730550CD89F6A9C02478A059923E3BEE324BD (void);
// 0x000007A3 System.Void System.Net.WebHeaderCollection::.cctor()
extern void WebHeaderCollection__cctor_m065B6EEB9A340079FA0815B12CF08010D7A5FD81 (void);
// 0x000007A4 System.Int32 System.Net.CaseInsensitiveAscii::GetHashCode(System.Object)
extern void CaseInsensitiveAscii_GetHashCode_m6196C976C5E2C2BF948F9775AF827D80AA0707B2 (void);
// 0x000007A5 System.Int32 System.Net.CaseInsensitiveAscii::Compare(System.Object,System.Object)
extern void CaseInsensitiveAscii_Compare_mDA30468D238E946A8CA36C5D0CBD9C92EBB67B0B (void);
// 0x000007A6 System.Int32 System.Net.CaseInsensitiveAscii::FastGetHashCode(System.String)
extern void CaseInsensitiveAscii_FastGetHashCode_m8185162F020C571335BCFB444577F786E6BB1E06 (void);
// 0x000007A7 System.Boolean System.Net.CaseInsensitiveAscii::Equals(System.Object,System.Object)
extern void CaseInsensitiveAscii_Equals_m2F02CC88964600A235BA5B084B83ADA6CEE2EE5F (void);
// 0x000007A8 System.Void System.Net.CaseInsensitiveAscii::.ctor()
extern void CaseInsensitiveAscii__ctor_mD964E182EFE7238322DE8F7A2CA169657B6C4F4A (void);
// 0x000007A9 System.Void System.Net.CaseInsensitiveAscii::.cctor()
extern void CaseInsensitiveAscii__cctor_m769922E617CDF4088DCC27EDD5C2F0042D2318A3 (void);
// 0x000007AA System.Object System.Net.WebRequest::get_InternalSyncObject()
extern void WebRequest_get_InternalSyncObject_mCCA701B3B01048CC2F26150D9E84D25CD194DFCF (void);
// 0x000007AB System.Void System.Net.WebRequest::.ctor()
extern void WebRequest__ctor_m8A7037980E4A7E78EEF420F0C38A36C10DE3D642 (void);
// 0x000007AC System.Void System.Net.WebRequest::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebRequest__ctor_mFCD491607BF422AED49CB0368CFE19F2E37BE98A (void);
// 0x000007AD System.Void System.Net.WebRequest::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m84834AA790D887F6742649C7D4001AD00089102B (void);
// 0x000007AE System.Void System.Net.WebRequest::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebRequest_GetObjectData_m1809AB7BE2251220AB0F64110CFD815EF882D3E7 (void);
// 0x000007AF System.Net.IWebProxy System.Net.WebRequest::get_InternalDefaultWebProxy()
extern void WebRequest_get_InternalDefaultWebProxy_m5FE1DF46DCB29A82C5AD3017F0D1BAD2A787A75C (void);
// 0x000007B0 System.Void System.Net.WebRequest::.cctor()
extern void WebRequest__cctor_mB67087F945455F2B1502CB55721CBF2ADB0A10F4 (void);
// 0x000007B1 System.Void System.Net.WebRequest/DesignerWebRequestCreate::.ctor()
extern void DesignerWebRequestCreate__ctor_mBFB58FED978B03A3C53AE292C3855EDED4222C88 (void);
// 0x000007B2 System.Void System.Net.WebResponse::.ctor()
extern void WebResponse__ctor_mC11AFFC628F1E49E53431A3129ADC7CEA012306B (void);
// 0x000007B3 System.Void System.Net.WebResponse::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebResponse__ctor_m845789E13F69DA82F94377A29D46186F2A43ADD1 (void);
// 0x000007B4 System.Void System.Net.WebResponse::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebResponse_System_Runtime_Serialization_ISerializable_GetObjectData_m7251108700FDB16B091C135DC5E76053623F4FE6 (void);
// 0x000007B5 System.Void System.Net.WebResponse::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebResponse_GetObjectData_mDE55FE69900B3F6311ED36EC53D1F775E2A634CB (void);
// 0x000007B6 System.Void System.Net.WebResponse::Close()
extern void WebResponse_Close_mDB1366AAB8237FC3D25C47B45FC7FA18055D7ADD (void);
// 0x000007B7 System.Void System.Net.WebResponse::Dispose()
extern void WebResponse_Dispose_mB68E6769F296DE95FF4BAF9A80AA9309EBB4AB4E (void);
// 0x000007B8 System.Void System.Net.WebResponse::Dispose(System.Boolean)
extern void WebResponse_Dispose_mAACC90307104A5FBE9D417E8E40569C2501D5568 (void);
// 0x000007B9 System.Void System.Net.HeaderParser::.ctor(System.Object,System.IntPtr)
extern void HeaderParser__ctor_m3E4BFAA908282AFA1EB023C1B84A03C222A68AAD (void);
// 0x000007BA System.String[] System.Net.HeaderParser::Invoke(System.String)
extern void HeaderParser_Invoke_mCAC8132BC020AD64B0130E50BD0379A312245C8E (void);
// 0x000007BB System.Void System.Net.HeaderInfo::.ctor(System.String,System.Boolean,System.Boolean,System.Boolean,System.Net.HeaderParser)
extern void HeaderInfo__ctor_mB82B9D1D7C19C78B649E9107B0DB316A96120CB8 (void);
// 0x000007BC System.String[] System.Net.HeaderInfoTable::ParseSingleValue(System.String)
extern void HeaderInfoTable_ParseSingleValue_m217B3A467422A5B17876254E9984F2C30E178F9A (void);
// 0x000007BD System.String[] System.Net.HeaderInfoTable::ParseMultiValue(System.String)
extern void HeaderInfoTable_ParseMultiValue_mCACE161A48EB4BC9DD4519BF3EC02CC83DD6C85C (void);
// 0x000007BE System.Void System.Net.HeaderInfoTable::.cctor()
extern void HeaderInfoTable__cctor_mD8B50AECD3CECAAC7E93610FAE44324679088EDC (void);
// 0x000007BF System.Net.HeaderInfo System.Net.HeaderInfoTable::get_Item(System.String)
extern void HeaderInfoTable_get_Item_mE388F21B37BFA30336BB82A1585C744E69A20021 (void);
// 0x000007C0 System.Void System.Net.HeaderInfoTable::.ctor()
extern void HeaderInfoTable__ctor_mED0A17850B2F6AA3A3E6BFACF257E8640A7F3F50 (void);
// 0x000007C1 System.Void System.Net.ICloseEx::CloseEx(System.Net.CloseExState)
// 0x000007C2 System.Net.LazyAsyncResult/ThreadContext System.Net.LazyAsyncResult::get_CurrentThreadContext()
extern void LazyAsyncResult_get_CurrentThreadContext_mC86876C2BE5FFCFB3CCD0B34ACAD7272E72725FD (void);
// 0x000007C3 System.Object System.Net.LazyAsyncResult::get_AsyncObject()
extern void LazyAsyncResult_get_AsyncObject_m599FB00838F916DBE45F359D64D26717EA2DF231 (void);
// 0x000007C4 System.Void System.Net.LazyAsyncResult::ProtectedInvokeCallback(System.Object,System.IntPtr)
extern void LazyAsyncResult_ProtectedInvokeCallback_mAB31EFC1A9671BA58BB38F59F381EAFE9C564410 (void);
// 0x000007C5 System.Void System.Net.LazyAsyncResult::InvokeCallback(System.Object)
extern void LazyAsyncResult_InvokeCallback_m4A1E1A15818F5ED6E6DBAEB48104A06529A11C9A (void);
// 0x000007C6 System.Void System.Net.LazyAsyncResult::Complete(System.IntPtr)
extern void LazyAsyncResult_Complete_m6B165AD8AEBAB7129514C42C4A3277114E0683BC (void);
// 0x000007C7 System.Void System.Net.LazyAsyncResult::WorkerThreadComplete(System.Object)
extern void LazyAsyncResult_WorkerThreadComplete_m50589CB1937E4BA7FF703482ECC24706F0E1A1BF (void);
// 0x000007C8 System.Void System.Net.LazyAsyncResult::Cleanup()
extern void LazyAsyncResult_Cleanup_mDB648575075A8BDAE413E4FB90C35A880A238B02 (void);
// 0x000007C9 System.Void System.Net.LazyAsyncResult/ThreadContext::.ctor()
extern void ThreadContext__ctor_mBFFE2269F22D15B6570BD909390F40F213E9DBF1 (void);
// 0x000007CA System.String System.Net.NetRes::GetWebStatusString(System.String,System.Net.WebExceptionStatus)
extern void NetRes_GetWebStatusString_m35A90AB3B319FDDFAC2A2C4AA534504806B8555B (void);
// 0x000007CB System.Void System.Net.TimerThread::.cctor()
extern void TimerThread__cctor_mC25B3C34109193014AFAA8AE22DEB5A82AE730E4 (void);
// 0x000007CC System.Net.TimerThread/Queue System.Net.TimerThread::CreateQueue(System.Int32)
extern void TimerThread_CreateQueue_mEE7746808FCC711657A10D38DAC9FC01B93B68D8 (void);
// 0x000007CD System.Void System.Net.TimerThread::StopTimerThread()
extern void TimerThread_StopTimerThread_m1E3A2BE926509E8E445BA5C00BA5B83C8283EDCE (void);
// 0x000007CE System.Void System.Net.TimerThread::OnDomainUnload(System.Object,System.EventArgs)
extern void TimerThread_OnDomainUnload_m789618CB693C29737319DBD4437199D0346FAF60 (void);
// 0x000007CF System.Void System.Net.TimerThread/Queue::.ctor(System.Int32)
extern void Queue__ctor_mD131B17E1C13BF9B3E8EBF5A909ACB274930B0A0 (void);
// 0x000007D0 System.Void System.Net.TimerThread/Timer::.ctor(System.Int32)
extern void Timer__ctor_m74E9AEA5FB4F7DDDD8D9089D6F9C9EF7A3A96F3E (void);
// 0x000007D1 System.Boolean System.Net.TimerThread/Timer::Cancel()
// 0x000007D2 System.Void System.Net.TimerThread/Timer::Dispose()
extern void Timer_Dispose_m266A7BD332741D89F4AA204ECEA9BD4784AFC79E (void);
// 0x000007D3 System.Void System.Net.TimerThread/Callback::.ctor(System.Object,System.IntPtr)
extern void Callback__ctor_mE36A85A258851A46EE477BBE1E87B5751C3CC9A9 (void);
// 0x000007D4 System.Void System.Net.TimerThread/Callback::Invoke(System.Net.TimerThread/Timer,System.Int32,System.Object)
extern void Callback_Invoke_mE5F4E02D25CB58B34E93A55CC21418F00B55C1BE (void);
// 0x000007D5 System.Void System.Net.TimerThread/TimerQueue::.ctor(System.Int32)
extern void TimerQueue__ctor_mCB1C60FF084C13E97E4FC8C4E56F6264649285D9 (void);
// 0x000007D6 System.Void System.Net.TimerThread/InfiniteTimerQueue::.ctor()
extern void InfiniteTimerQueue__ctor_mC47879DB811308BFE6D5D4613791A81CCA215501 (void);
// 0x000007D7 System.Void System.Net.TimerThread/TimerNode::.ctor()
extern void TimerNode__ctor_m259A5ECB2BA964982D8CB975EE945899CB14EA6A (void);
// 0x000007D8 System.Net.TimerThread/TimerNode System.Net.TimerThread/TimerNode::get_Next()
extern void TimerNode_get_Next_mB0E6D37D592DC92CBED33A26B0362F00959578D5 (void);
// 0x000007D9 System.Void System.Net.TimerThread/TimerNode::set_Next(System.Net.TimerThread/TimerNode)
extern void TimerNode_set_Next_m77F9833E3DA4B66D565A10F0409C15B6B7677AB0 (void);
// 0x000007DA System.Net.TimerThread/TimerNode System.Net.TimerThread/TimerNode::get_Prev()
extern void TimerNode_get_Prev_m358430B0F3DD4E039E38962A04118F937E6F5E4D (void);
// 0x000007DB System.Void System.Net.TimerThread/TimerNode::set_Prev(System.Net.TimerThread/TimerNode)
extern void TimerNode_set_Prev_m3A7D7FD5A4B654793FB48D0BAF7D8E0638BB1E47 (void);
// 0x000007DC System.Boolean System.Net.TimerThread/TimerNode::Cancel()
extern void TimerNode_Cancel_m6CAF1FDE8F05519F86383C25195AC922D5056489 (void);
// 0x000007DD System.Void System.Net.FileWebRequest::.ctor(System.Uri)
extern void FileWebRequest__ctor_m32AA08E0CE88EE4B4328D724C347D7D3F2B3E700 (void);
// 0x000007DE System.Void System.Net.FileWebRequest::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void FileWebRequest__ctor_m7AF7635D835AAEA13DE9B5F7BF53869032B6E51F (void);
// 0x000007DF System.Void System.Net.FileWebRequest::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m73E50CCB8DA4A87382EE2B23DBA184184C238C67 (void);
// 0x000007E0 System.Void System.Net.FileWebRequest::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void FileWebRequest_GetObjectData_mB0C155782CDEA4ED9B05D66308F6223AB0C4E389 (void);
// 0x000007E1 System.Boolean System.Net.FileWebRequest::get_Aborted()
extern void FileWebRequest_get_Aborted_mDCF5639158777EE8240560751536AF4CFF1FB3BE (void);
// 0x000007E2 System.Void System.Net.FileWebRequest::GetRequestStreamCallback(System.Object)
extern void FileWebRequest_GetRequestStreamCallback_m62CCC32A81F8350AC6975EEEB79B2D61CC428C8C (void);
// 0x000007E3 System.Void System.Net.FileWebRequest::GetResponseCallback(System.Object)
extern void FileWebRequest_GetResponseCallback_m21A70D5427620CCA22552BC8263CEF014B3AF414 (void);
// 0x000007E4 System.Void System.Net.FileWebRequest::UnblockReader()
extern void FileWebRequest_UnblockReader_mC1E7C9618A64BC7785D882EB222D92A95244F8B3 (void);
// 0x000007E5 System.Void System.Net.FileWebRequest::.cctor()
extern void FileWebRequest__cctor_m1034782E644D45E398ED29E1ADE0AA1454DFF5F9 (void);
// 0x000007E6 System.Void System.Net.FileWebRequestCreator::.ctor()
extern void FileWebRequestCreator__ctor_mDF264D86311EA60A56DA2B1FCE048E48F0BF6CAB (void);
// 0x000007E7 System.Net.WebRequest System.Net.FileWebRequestCreator::Create(System.Uri)
extern void FileWebRequestCreator_Create_mB47FFBF665D3B3D20F250AE2396A3C5654637939 (void);
// 0x000007E8 System.Void System.Net.FileWebStream::.ctor(System.Net.FileWebRequest,System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare)
extern void FileWebStream__ctor_m0F3CC822C4DEE7C71B3FA3FFDCF5B25EAEB2CEAE (void);
// 0x000007E9 System.Void System.Net.FileWebStream::.ctor(System.Net.FileWebRequest,System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.Int32,System.Boolean)
extern void FileWebStream__ctor_m9E118415C0F3AD53809A5836C1BD6DFC9913C60B (void);
// 0x000007EA System.Void System.Net.FileWebStream::Dispose(System.Boolean)
extern void FileWebStream_Dispose_m1155DD4853E8E2ADF2EB7E0A940289F18E4F8EF8 (void);
// 0x000007EB System.Void System.Net.FileWebStream::System.Net.ICloseEx.CloseEx(System.Net.CloseExState)
extern void FileWebStream_System_Net_ICloseEx_CloseEx_m68BBF0B85ACACDFB200E96968A6E31E8FCA757C4 (void);
// 0x000007EC System.Int32 System.Net.FileWebStream::Read(System.Byte[],System.Int32,System.Int32)
extern void FileWebStream_Read_mD35B2485DB5F8929EF92A5727FF64A28BF2A5DB0 (void);
// 0x000007ED System.Void System.Net.FileWebStream::Write(System.Byte[],System.Int32,System.Int32)
extern void FileWebStream_Write_m216E33F6ABF32F817381DEF9DB3B133BC0587468 (void);
// 0x000007EE System.IAsyncResult System.Net.FileWebStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void FileWebStream_BeginRead_mAAB039B0A955363303488218A4CBB9845CAEB4CF (void);
// 0x000007EF System.Int32 System.Net.FileWebStream::EndRead(System.IAsyncResult)
extern void FileWebStream_EndRead_m5AEA2D6B64A3CA7E005D9F0929C9D829766F6538 (void);
// 0x000007F0 System.IAsyncResult System.Net.FileWebStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void FileWebStream_BeginWrite_m686EDD328F0ECED1C8C51E819A61E3BD7A44F5B1 (void);
// 0x000007F1 System.Void System.Net.FileWebStream::EndWrite(System.IAsyncResult)
extern void FileWebStream_EndWrite_m5162ABF5383D0F4A52DF6996687868DE6AF9E486 (void);
// 0x000007F2 System.Void System.Net.FileWebStream::CheckError()
extern void FileWebStream_CheckError_m7C73A2B18D870AB5869B1F38F325C32E83305F6B (void);
// 0x000007F3 System.Void System.Net.FileWebResponse::.ctor(System.Net.FileWebRequest,System.Uri,System.IO.FileAccess,System.Boolean)
extern void FileWebResponse__ctor_mDCECF4C7D821F8FCBA18B5FA8DF2AD303A029C30 (void);
// 0x000007F4 System.Void System.Net.FileWebResponse::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void FileWebResponse__ctor_m63E4E435D6A83A6DFCE4FB6560F97FD09C50D973 (void);
// 0x000007F5 System.Void System.Net.FileWebResponse::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void FileWebResponse_System_Runtime_Serialization_ISerializable_GetObjectData_m92E0F46AC675AA9E4EEB159D1B465E085CB4CEC7 (void);
// 0x000007F6 System.Void System.Net.FileWebResponse::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void FileWebResponse_GetObjectData_m8C157E17C06D23AE9B5E8B00FFDDB3F94209667A (void);
// 0x000007F7 System.Void System.Net.FileWebResponse::Close()
extern void FileWebResponse_Close_m03BA89FC13B905EC65468C4AFB8FC44A1AFF3F83 (void);
// 0x000007F8 System.Void System.Net.FileWebResponse::System.Net.ICloseEx.CloseEx(System.Net.CloseExState)
extern void FileWebResponse_System_Net_ICloseEx_CloseEx_m9909306B65ECA8038FB0A1DE1D0E72DEC4EB3D39 (void);
// 0x000007F9 System.Uri System.Net.IWebProxy::GetProxy(System.Uri)
// 0x000007FA System.Boolean System.Net.IWebProxy::IsBypassed(System.Uri)
// 0x000007FB System.Void System.Net.WebProxy::.ctor()
extern void WebProxy__ctor_m513721AC5813F35B2879182BF7558A2966DDBC34 (void);
// 0x000007FC System.Void System.Net.WebProxy::.ctor(System.Uri,System.Boolean,System.String[],System.Net.ICredentials)
extern void WebProxy__ctor_m56D1C9B767710B435DFDCC58891651D915E9E96E (void);
// 0x000007FD System.Net.ICredentials System.Net.WebProxy::get_Credentials()
extern void WebProxy_get_Credentials_m76BB7DE06FD84E20406661BF0D98959DC73B28D9 (void);
// 0x000007FE System.Boolean System.Net.WebProxy::get_UseDefaultCredentials()
extern void WebProxy_get_UseDefaultCredentials_mA938B24C5D5406B88C515CC7450B91FBF3BBA28E (void);
// 0x000007FF System.Void System.Net.WebProxy::set_UseDefaultCredentials(System.Boolean)
extern void WebProxy_set_UseDefaultCredentials_mF60535F31708D6DF7CECC2926EF1634C1333C002 (void);
// 0x00000800 System.Uri System.Net.WebProxy::GetProxy(System.Uri)
extern void WebProxy_GetProxy_mB7D36975267535555BCB1BF449098DB35D01BC8E (void);
// 0x00000801 System.Void System.Net.WebProxy::UpdateRegExList(System.Boolean)
extern void WebProxy_UpdateRegExList_m7B2EE98DAADC48CBDFA6ED2D491B4E4F00857F48 (void);
// 0x00000802 System.Boolean System.Net.WebProxy::IsMatchInBypassList(System.Uri)
extern void WebProxy_IsMatchInBypassList_m4D28B54328E1AFB40D9BE518536EE192CE423F50 (void);
// 0x00000803 System.Boolean System.Net.WebProxy::IsLocal(System.Uri)
extern void WebProxy_IsLocal_mA55F6EF93DD6449B262157E0795507C82356F154 (void);
// 0x00000804 System.Boolean System.Net.WebProxy::IsLocalInProxyHash(System.Uri)
extern void WebProxy_IsLocalInProxyHash_m20B75565C09C609A0A9C000640B562C1D067A99A (void);
// 0x00000805 System.Boolean System.Net.WebProxy::IsBypassed(System.Uri)
extern void WebProxy_IsBypassed_m4919E244B263600121FECB9F693AECBDBA30AE38 (void);
// 0x00000806 System.Boolean System.Net.WebProxy::IsBypassedManual(System.Uri)
extern void WebProxy_IsBypassedManual_m89E6F6F73446B4663DEB837B77ED542A9D5A220B (void);
// 0x00000807 System.Void System.Net.WebProxy::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebProxy__ctor_mCD69F94F99FAE8B45E7983EDDD7C8C8FB82F03B6 (void);
// 0x00000808 System.Void System.Net.WebProxy::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m80D2CE0C8665B8D1E6CBCAD584496A87934DE312 (void);
// 0x00000809 System.Void System.Net.WebProxy::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebProxy_GetObjectData_mB8AB2DF20CBEF664997055463E7C4639E43BF52B (void);
// 0x0000080A System.Net.AutoWebProxyScriptEngine System.Net.WebProxy::get_ScriptEngine()
extern void WebProxy_get_ScriptEngine_m0B6FE570183127FAD115C72CBC6BFFF5020004DF (void);
// 0x0000080B System.Net.IWebProxy System.Net.WebProxy::CreateDefaultProxy()
extern void WebProxy_CreateDefaultProxy_mF54A2CCE4E97B5A463DD71DCEF0E7A2A9C1265F3 (void);
// 0x0000080C System.Void System.Net.WebProxy::.ctor(System.Boolean)
extern void WebProxy__ctor_m0D1F873D2441832F41295266069C0887BBD43641 (void);
// 0x0000080D System.Void System.Net.WebProxy::UnsafeUpdateFromRegistry()
extern void WebProxy_UnsafeUpdateFromRegistry_m450A4700AA84B7A0C467365724AB9926F6881167 (void);
// 0x0000080E System.Boolean System.Net.WebProxy::GetProxyAuto(System.Uri,System.Uri&)
extern void WebProxy_GetProxyAuto_m72DBD46E29CD88F40BC0B5D375BB8D30018029BC (void);
// 0x0000080F System.Boolean System.Net.WebProxy::IsBypassedAuto(System.Uri,System.Boolean&)
extern void WebProxy_IsBypassedAuto_mF4D52FD050AC1CA1672AE503BD23B68794CFD60E (void);
// 0x00000810 System.Boolean System.Net.WebProxy::AreAllBypassed(System.Collections.Generic.IEnumerable`1<System.String>,System.Boolean)
extern void WebProxy_AreAllBypassed_mD0ED2AEA81FAB6B141A13433406C53A9C8F10F59 (void);
// 0x00000811 System.Uri System.Net.WebProxy::ProxyUri(System.String)
extern void WebProxy_ProxyUri_mAC61DB0E0378F9FE1ED4CFB5ABB61331312FB85B (void);
// 0x00000812 System.Boolean System.Net.AutoWebProxyScriptEngine::GetProxies(System.Uri,System.Collections.Generic.IList`1<System.String>&)
extern void AutoWebProxyScriptEngine_GetProxies_m39AEDFB157EFDBE7CE80A569565EAFEA874AE381 (void);
// 0x00000813 System.Boolean System.Net.AutoWebProxyScriptEngine::GetProxies(System.Uri,System.Collections.Generic.IList`1<System.String>&,System.Int32&)
extern void AutoWebProxyScriptEngine_GetProxies_m0EC7B4AB41D5BDC4DCBDF2CAB12EAF6CAFFD56D1 (void);
// 0x00000814 System.Security.SecureString System.Net.UnsafeNclNativeMethods/SecureStringHelper::CreateSecureString(System.String)
extern void SecureStringHelper_CreateSecureString_mEB7FEF36DD2FD2DE189C394068B70EA57F56C6D9 (void);
// 0x00000815 System.Boolean System.Net.Logging::get_On()
extern void Logging_get_On_mE613A75366C51B02DEC967952C3C0DD852156B02 (void);
// 0x00000816 System.Void System.Net.ServerCertValidationCallback::.ctor(System.Net.Security.RemoteCertificateValidationCallback)
extern void ServerCertValidationCallback__ctor_mF3AC814542EED45987465791FE7846DE2A8B5603 (void);
// 0x00000817 System.Void System.Net.ServerCertValidationCallback::Callback(System.Object)
extern void ServerCertValidationCallback_Callback_mF04B81117BD337FC77BDB94B8EAA8B8B99A4D72C (void);
// 0x00000818 System.Boolean System.Net.ServerCertValidationCallback::Invoke(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void ServerCertValidationCallback_Invoke_mDDC4DA8F50134EF50257E51B53BE36732550C7E6 (void);
// 0x00000819 System.Void System.Net.ServerCertValidationCallback/CallbackContext::.ctor(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void CallbackContext__ctor_mF5D13B557A13193C357723E572E78E52F108AEB7 (void);
// 0x0000081A System.Boolean System.Net.Dns::GetHostByName_icall(System.String,System.String&,System.String[]&,System.String[]&,System.Int32)
extern void Dns_GetHostByName_icall_m5FFF0440C4EA238DA14130BD1856BC8C730E6BE1 (void);
// 0x0000081B System.Boolean System.Net.Dns::GetHostByAddr_icall(System.String,System.String&,System.String[]&,System.String[]&,System.Int32)
extern void Dns_GetHostByAddr_icall_m3411CB4B97CE07A5BFE2D02B2A186738C76CFD22 (void);
// 0x0000081C System.Boolean System.Net.Dns::GetHostName_icall(System.String&)
extern void Dns_GetHostName_icall_mB1522212F101A0B89516E5734443BBD361E76FA9 (void);
// 0x0000081D System.Void System.Net.Dns::Error_11001(System.String)
extern void Dns_Error_11001_m23687C2F5E4CB975F023D3775A1E7DC5B61DB83C (void);
// 0x0000081E System.Net.IPHostEntry System.Net.Dns::hostent_to_IPHostEntry(System.String,System.String,System.String[],System.String[])
extern void Dns_hostent_to_IPHostEntry_mC5D77A6DE5F97D1A47AD86CAD4F69FF780A12159 (void);
// 0x0000081F System.Net.IPHostEntry System.Net.Dns::GetHostByAddressFromString(System.String,System.Boolean)
extern void Dns_GetHostByAddressFromString_mE3A65EDABDD9DBBA0879B21F4A545BA7613683A8 (void);
// 0x00000820 System.Net.IPHostEntry System.Net.Dns::GetHostEntry(System.String)
extern void Dns_GetHostEntry_m01156278E5CDAE38B7E1B2EC617F505A4B836D02 (void);
// 0x00000821 System.Net.IPHostEntry System.Net.Dns::GetHostEntry(System.Net.IPAddress)
extern void Dns_GetHostEntry_m09C47D2D4329E24D56DCA5BDA6D4F28FEC45EC87 (void);
// 0x00000822 System.Net.IPAddress[] System.Net.Dns::GetHostAddresses(System.String)
extern void Dns_GetHostAddresses_m0592CB7DB3F5665C412BCBE8297F198748114F90 (void);
// 0x00000823 System.Net.IPHostEntry System.Net.Dns::GetHostByName(System.String)
extern void Dns_GetHostByName_m554E787DE7A847C2730E1BE8F74C56E9594C3D3F (void);
// 0x00000824 System.String System.Net.Dns::GetHostName()
extern void Dns_GetHostName_mF728787FF8A38620054B934FD08E021460A7C14D (void);
// 0x00000825 System.Void System.Net.HttpRequestCreator::.ctor()
extern void HttpRequestCreator__ctor_mA6D07F56561A4C93100584384CCDED5171C45F50 (void);
// 0x00000826 System.Net.WebRequest System.Net.HttpRequestCreator::Create(System.Uri)
extern void HttpRequestCreator_Create_m25D9085B7B59931A1A4DBA18DE828D8FA376A139 (void);
// 0x00000827 System.Void System.Net.HttpWebRequest::.cctor()
extern void HttpWebRequest__cctor_m5B96D0A29F3F6BA4C78CC0DE32C81A78A75767EF (void);
// 0x00000828 System.Void System.Net.HttpWebRequest::.ctor(System.Uri)
extern void HttpWebRequest__ctor_m37C92DEFA8C4C0282FE5B11E065D65C25CF8B91F (void);
// 0x00000829 System.Void System.Net.HttpWebRequest::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void HttpWebRequest__ctor_m94F348B8E4338E5D0404A8425F5F43B339B322BE (void);
// 0x0000082A System.Void System.Net.HttpWebRequest::ResetAuthorization()
extern void HttpWebRequest_ResetAuthorization_mC7AA3C31078EA81423351F8DAC62F68E9F15060E (void);
// 0x0000082B System.Uri System.Net.HttpWebRequest::get_Address()
extern void HttpWebRequest_get_Address_mC0579CE0CED2FDCBF69FAF3232706F7994EAC20D (void);
// 0x0000082C System.Void System.Net.HttpWebRequest::set_ThrowOnError(System.Boolean)
extern void HttpWebRequest_set_ThrowOnError_mB232259B74EC15F9E119944A41934D673A7DE4E7 (void);
// 0x0000082D System.String System.Net.HttpWebRequest::get_Host()
extern void HttpWebRequest_get_Host_mE23F8F15207CEF1E64CF212EEA7643185207923C (void);
// 0x0000082E System.Net.ServicePoint System.Net.HttpWebRequest::get_ServicePoint()
extern void HttpWebRequest_get_ServicePoint_m170B921D095437FC5B7FE5920F327F1AABF532D6 (void);
// 0x0000082F System.Net.ServicePoint System.Net.HttpWebRequest::get_ServicePointNoLock()
extern void HttpWebRequest_get_ServicePointNoLock_m302BC1F3A189D46E652B79981E8D29095E5D5C61 (void);
// 0x00000830 System.Net.ServerCertValidationCallback System.Net.HttpWebRequest::get_ServerCertValidationCallback()
extern void HttpWebRequest_get_ServerCertValidationCallback_m8822E8093BBEE23482561CCB65D6AF3B7B1B464E (void);
// 0x00000831 System.Net.ServicePoint System.Net.HttpWebRequest::GetServicePoint()
extern void HttpWebRequest_GetServicePoint_m936C89220DFB9BE810A223E84E36E6D933510DB7 (void);
// 0x00000832 System.Exception System.Net.HttpWebRequest::FlattenException(System.Exception)
extern void HttpWebRequest_FlattenException_mF0D9565F401231C7F7C6A973F829CCB37E34C3EA (void);
// 0x00000833 System.Void System.Net.HttpWebRequest::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_mDB54A2BCCE38E278C44D3A136F6988CEEA92F5FE (void);
// 0x00000834 System.Void System.Net.HttpWebRequest::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void HttpWebRequest_GetObjectData_m6C3C1BB107DB72006CDF96892EF910400CFA9266 (void);
// 0x00000835 System.Void System.Net.HttpWebRequest::.ctor()
extern void HttpWebRequest__ctor_m1AF72A90B1265EE52584D72B1B225DCC95E62722 (void);
// 0x00000836 System.Void System.Net.HttpWebRequest/AuthorizationState::.ctor(System.Net.HttpWebRequest,System.Boolean)
extern void AuthorizationState__ctor_m6DDA04B33B8A520B93FBDF8AC4BBA3B7CCC03CBE (void);
// 0x00000837 System.String System.Net.HttpWebRequest/AuthorizationState::ToString()
extern void AuthorizationState_ToString_m6426C6029B83D041B9F1B9871A4541F70A2EFC00 (void);
// 0x00000838 System.Void System.Net.ServicePoint::.ctor(System.Net.ServicePointManager/SPKey,System.Uri,System.Int32,System.Int32)
extern void ServicePoint__ctor_m4A7FE526B6A4EF15F51919D425E51B1D67EFD1AE (void);
// 0x00000839 System.Void System.Net.ServicePoint::set_Scheduler(System.Net.ServicePointScheduler)
extern void ServicePoint_set_Scheduler_m7935567ACF1937D07DA99217D517C7946A876126 (void);
// 0x0000083A System.Void System.Net.ServicePoint::set_Expect100Continue(System.Boolean)
extern void ServicePoint_set_Expect100Continue_mDDB9FC522313DDC57B1B348B711CF888516AA4CD (void);
// 0x0000083B System.Void System.Net.ServicePoint::set_UseNagleAlgorithm(System.Boolean)
extern void ServicePoint_set_UseNagleAlgorithm_mC025F081ED6C7014665EB20388747D12476D03C3 (void);
// 0x0000083C System.Void System.Net.ServicePoint::set_SendContinue(System.Boolean)
extern void ServicePoint_set_SendContinue_m5480B6A8C6478BA620030302C861A842DF9F6AD3 (void);
// 0x0000083D System.Void System.Net.ServicePoint::SetTcpKeepAlive(System.Boolean,System.Int32,System.Int32)
extern void ServicePoint_SetTcpKeepAlive_m225D363A70F7971133192A44745C8D284DE19084 (void);
// 0x0000083E System.Void System.Net.ServicePoint::set_UsesProxy(System.Boolean)
extern void ServicePoint_set_UsesProxy_m48157456CF1FF842A098916FE6F485CCA2FB9B62 (void);
// 0x0000083F System.Void System.Net.ServicePoint::set_UseConnect(System.Boolean)
extern void ServicePoint_set_UseConnect_mD34B72F3B4F0532EB58DAB315A58143E5882A81F (void);
// 0x00000840 System.Void System.Net.ServicePoint::UpdateServerCertificate(System.Security.Cryptography.X509Certificates.X509Certificate)
extern void ServicePoint_UpdateServerCertificate_mA5119A83F8A76B1098E4C0B7BEACED9F04288D10 (void);
// 0x00000841 System.Void System.Net.ServicePointManager::.cctor()
extern void ServicePointManager__cctor_m8067E1B281672361100384C9FD668FB793EEE44A (void);
// 0x00000842 System.Net.ICertificatePolicy System.Net.ServicePointManager::GetLegacyCertificatePolicy()
extern void ServicePointManager_GetLegacyCertificatePolicy_mBD5DCFD553FEEE50F78A70BAB63F95ACF21A25FC (void);
// 0x00000843 System.Boolean System.Net.ServicePointManager::get_CheckCertificateRevocationList()
extern void ServicePointManager_get_CheckCertificateRevocationList_mC0CDB007A91C59573B664FAB178B756F78561534 (void);
// 0x00000844 System.Net.ServerCertValidationCallback System.Net.ServicePointManager::get_ServerCertValidationCallback()
extern void ServicePointManager_get_ServerCertValidationCallback_m9436D7CB2C84F88A2F878C700D5B3B34B85B911D (void);
// 0x00000845 System.Net.ServicePoint System.Net.ServicePointManager::FindServicePoint(System.Uri,System.Net.IWebProxy)
extern void ServicePointManager_FindServicePoint_m8B83339FF47FF2FDB7671C114BD56B71743ED2F4 (void);
// 0x00000846 System.Void System.Net.ServicePointManager/SPKey::.ctor(System.Uri,System.Uri,System.Boolean)
extern void SPKey__ctor_m30BE1C73F21DEB53327E47A0EFDC05D39726E2FB (void);
// 0x00000847 System.Boolean System.Net.ServicePointManager/SPKey::get_UsesProxy()
extern void SPKey_get_UsesProxy_mD847C0A3FA96B142882A340E221D4C4AE6BE3E76 (void);
// 0x00000848 System.Int32 System.Net.ServicePointManager/SPKey::GetHashCode()
extern void SPKey_GetHashCode_mEF88EF3AB042F1A3B7BE41171F53155177ED0B4F (void);
// 0x00000849 System.Boolean System.Net.ServicePointManager/SPKey::Equals(System.Object)
extern void SPKey_Equals_mDFDEB4BBCC2EE4F43F4F36CBA81A35DCFE1CA114 (void);
// 0x0000084A System.Void System.Net.ServicePointScheduler::set_ServicePoint(System.Net.ServicePoint)
extern void ServicePointScheduler_set_ServicePoint_mEA9C878A2C3B51F9C6B1D66AB82FC1D81B38289B (void);
// 0x0000084B System.Void System.Net.ServicePointScheduler::.ctor(System.Net.ServicePoint,System.Int32,System.Int32)
extern void ServicePointScheduler__ctor_m31580DC08F3E2491616C5067921AD44899E58004 (void);
// 0x0000084C System.Void System.Net.ServicePointScheduler/ConnectionGroup::.ctor(System.Net.ServicePointScheduler,System.String)
extern void ConnectionGroup__ctor_m374EEC30B4DB339157E1FEFE8D9908F1602B5103 (void);
// 0x0000084D System.Void System.Net.ServicePointScheduler/AsyncManualResetEvent::Set()
extern void AsyncManualResetEvent_Set_m3D541CCC4B5CB8393A63D23B9E5F104C85258549 (void);
// 0x0000084E System.Void System.Net.ServicePointScheduler/AsyncManualResetEvent::.ctor(System.Boolean)
extern void AsyncManualResetEvent__ctor_mC5C973C7ACD4C923325E8B98C3E4DF293C08C757 (void);
// 0x0000084F System.Void System.Net.ServicePointScheduler/AsyncManualResetEvent/<>c::.cctor()
extern void U3CU3Ec__cctor_mF229783494E9F1BE9EB2E4C7D11B94F0AE5FD9C5 (void);
// 0x00000850 System.Void System.Net.ServicePointScheduler/AsyncManualResetEvent/<>c::.ctor()
extern void U3CU3Ec__ctor_m42F5C86E7E31C11A87C17A9FBE7A56B703102A01 (void);
// 0x00000851 System.Boolean System.Net.ServicePointScheduler/AsyncManualResetEvent/<>c::<Set>b__4_0(System.Object)
extern void U3CU3Ec_U3CSetU3Eb__4_0_m7ABBA00DFE7F256F3DD9640FB9A9D68CF5988498 (void);
// 0x00000852 System.Void System.Net.WebConnection::Reset()
extern void WebConnection_Reset_mAE0ED7B4E2139686F19310C905B9C065209E7747 (void);
// 0x00000853 System.Void System.Net.WebConnection::Close(System.Boolean)
extern void WebConnection_Close_mB451C06EF8412B688EE8DCBD0B70951B0415D791 (void);
// 0x00000854 System.Void System.Net.WebConnection::CloseSocket()
extern void WebConnection_CloseSocket_m435216C8E7C8EC2B0F7DBFE95A2A4A9D3E7D7B74 (void);
// 0x00000855 System.Void System.Net.WebConnection::Dispose(System.Boolean)
extern void WebConnection_Dispose_mAA356D7546A523F3FAC8EEC34502DA3EDFD797F2 (void);
// 0x00000856 System.Void System.Net.WebConnection::Dispose()
extern void WebConnection_Dispose_m619217D576EFF59E4939308F24595E8F498175F4 (void);
// 0x00000857 System.Void System.Net.WebConnection::ResetNtlm()
extern void WebConnection_ResetNtlm_mBD715060318622998A51E76EEF7C4106C18779A3 (void);
// 0x00000858 System.Net.NetworkInformation.IPGlobalProperties System.Net.NetworkInformation.IPGlobalProperties::GetIPGlobalProperties()
extern void IPGlobalProperties_GetIPGlobalProperties_m78B851B32A1D963CC616CCA6DC7116F5EAC15753 (void);
// 0x00000859 System.Net.NetworkInformation.IPGlobalProperties System.Net.NetworkInformation.IPGlobalProperties::InternalGetIPGlobalProperties()
extern void IPGlobalProperties_InternalGetIPGlobalProperties_m72EC7CD424FA4463DBF973ECB688CE857A01578E (void);
// 0x0000085A System.String System.Net.NetworkInformation.IPGlobalProperties::get_DomainName()
// 0x0000085B System.Void System.Net.NetworkInformation.IPGlobalProperties::.ctor()
extern void IPGlobalProperties__ctor_mD47A72AA48932214CA1D4736A83CE60D87446DB8 (void);
// 0x0000085C System.Net.NetworkInformation.IPGlobalProperties System.Net.NetworkInformation.IPGlobalPropertiesFactoryPal::Create()
extern void IPGlobalPropertiesFactoryPal_Create_m08DE78C65028109D3F8B3F8C6C67DC3546FD9AC4 (void);
// 0x0000085D System.String System.Net.NetworkInformation.Win32IPGlobalProperties::get_DomainName()
extern void Win32IPGlobalProperties_get_DomainName_m606701D1FC345ED0DEE4776A2EFE5BB69E5AAE45 (void);
// 0x0000085E System.Void System.Net.NetworkInformation.Win32IPGlobalProperties::.ctor()
extern void Win32IPGlobalProperties__ctor_m9B513B7EF6994299CD6F5043C32868D9521264EA (void);
// 0x0000085F System.Net.NetworkInformation.IPGlobalProperties System.Net.NetworkInformation.Win32IPGlobalPropertiesFactoryPal::Create()
extern void Win32IPGlobalPropertiesFactoryPal_Create_m6D31F76384538314B8844A60A44681AD1B38D5D5 (void);
// 0x00000860 System.Int32 System.Net.NetworkInformation.Win32NetworkInterface::GetNetworkParams(System.IntPtr,System.Int32&)
extern void Win32NetworkInterface_GetNetworkParams_mA2E02CD896547407FEF7BACC80FE5B3ECFD57A32 (void);
// 0x00000861 System.Net.NetworkInformation.Win32_FIXED_INFO System.Net.NetworkInformation.Win32NetworkInterface::get_FixedInfo()
extern void Win32NetworkInterface_get_FixedInfo_m90FC6BF37040C5B5548AB86E4532C16CED57F7A4 (void);
// 0x00000862 System.Net.NetworkInformation.IPGlobalProperties System.Net.NetworkInformation.UnixIPGlobalPropertiesFactoryPal::Create()
extern void UnixIPGlobalPropertiesFactoryPal_Create_m3646A4471214BFFD7D35E54EDA955AA1E8D2D468 (void);
// 0x00000863 System.Net.IWebProxy System.Net.Configuration.DefaultProxySectionInternal::GetDefaultProxy_UsingOldMonoCode()
extern void DefaultProxySectionInternal_GetDefaultProxy_UsingOldMonoCode_mE92DE08E220E7F8B610BF188DCD06BE990F8CFB3 (void);
// 0x00000864 System.Net.IWebProxy System.Net.Configuration.DefaultProxySectionInternal::GetSystemWebProxy()
extern void DefaultProxySectionInternal_GetSystemWebProxy_m149FAD86815FA437C5D6FEF7ABCA97CD2C3C94D7 (void);
// 0x00000865 System.Object System.Net.Configuration.DefaultProxySectionInternal::get_ClassSyncObject()
extern void DefaultProxySectionInternal_get_ClassSyncObject_m6C8A102D494E67F287EA10DEACD3EA151C0D085D (void);
// 0x00000866 System.Net.Configuration.DefaultProxySectionInternal System.Net.Configuration.DefaultProxySectionInternal::GetSection()
extern void DefaultProxySectionInternal_GetSection_mC347FAA709106553F94BA8097F9593383BCA22FC (void);
// 0x00000867 System.Net.IWebProxy System.Net.Configuration.DefaultProxySectionInternal::get_WebProxy()
extern void DefaultProxySectionInternal_get_WebProxy_mF4F15C1A7C6088706CA2834D70E8CD52086AFBDA (void);
// 0x00000868 System.Void System.Net.Configuration.DefaultProxySectionInternal::.ctor()
extern void DefaultProxySectionInternal__ctor_m9EDD9E9D9F38DCA0B5260A4F040F27CE1650D430 (void);
// 0x00000869 System.Net.Configuration.SettingsSectionInternal System.Net.Configuration.SettingsSectionInternal::get_Section()
extern void SettingsSectionInternal_get_Section_m2B7B6F6B8C13E9FAD46C76116CCDE663741ECF35 (void);
// 0x0000086A System.Boolean System.Net.Configuration.SettingsSectionInternal::get_Ipv6Enabled()
extern void SettingsSectionInternal_get_Ipv6Enabled_m4DF56EE72066B70B389C21B2B4A30EAFC05D20F0 (void);
// 0x0000086B System.Void System.Net.Configuration.SettingsSectionInternal::.ctor()
extern void SettingsSectionInternal__ctor_mF034B00EA94462E6FA1F906C97BD7B59AA00F1C3 (void);
// 0x0000086C System.Void System.Net.Configuration.SettingsSectionInternal::.cctor()
extern void SettingsSectionInternal__cctor_m8BEE9C7B1B87EC7507FC1D686220BAF2B94087C2 (void);
// 0x0000086D System.Void System.Net.Cache.RequestCachePolicy::.ctor(System.Net.Cache.RequestCacheLevel)
extern void RequestCachePolicy__ctor_mDE54BDF10DC2094F849F2E6A90770189FD664988 (void);
// 0x0000086E System.String System.Net.Cache.RequestCachePolicy::ToString()
extern void RequestCachePolicy_ToString_m7F0E4C10E284F30A563E035DBD56BF855E4E3A28 (void);
// 0x0000086F System.Void System.Net.Sockets.NetworkStream::.ctor(System.Net.Sockets.Socket,System.Boolean)
extern void NetworkStream__ctor_mA44C3DB90F5B64F230CC2176DC7E722F4077A9DE (void);
// 0x00000870 System.Void System.Net.Sockets.NetworkStream::.ctor(System.Net.Sockets.Socket,System.IO.FileAccess,System.Boolean)
extern void NetworkStream__ctor_mC30750A728A682C23581DC0ABEE5B7F1E4246AB5 (void);
// 0x00000871 System.Boolean System.Net.Sockets.NetworkStream::get_CanRead()
extern void NetworkStream_get_CanRead_mE61A13D0F12690A65200D3FD0947A0DC2442643E (void);
// 0x00000872 System.Boolean System.Net.Sockets.NetworkStream::get_CanSeek()
extern void NetworkStream_get_CanSeek_mAF6CE2068CC8DC3DDD922C6C2C187D286A3F5044 (void);
// 0x00000873 System.Boolean System.Net.Sockets.NetworkStream::get_CanWrite()
extern void NetworkStream_get_CanWrite_mD3EBED79804ED2676F028892B84DC7F8F9FDAA6A (void);
// 0x00000874 System.Int64 System.Net.Sockets.NetworkStream::get_Length()
extern void NetworkStream_get_Length_mA738206EC6CF6C8A07FEC82C3702A5F494569496 (void);
// 0x00000875 System.Int64 System.Net.Sockets.NetworkStream::get_Position()
extern void NetworkStream_get_Position_m6DAEE19231A91693081BE120ABCB5B031DC95E61 (void);
// 0x00000876 System.Void System.Net.Sockets.NetworkStream::set_Position(System.Int64)
extern void NetworkStream_set_Position_mACE85A7C1D600E5B6D18CF5FF32D859F4734010D (void);
// 0x00000877 System.Int64 System.Net.Sockets.NetworkStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void NetworkStream_Seek_mD5CE01FBB731FF52A66451C5F688837AC4ADD906 (void);
// 0x00000878 System.Int32 System.Net.Sockets.NetworkStream::Read(System.Byte[],System.Int32,System.Int32)
extern void NetworkStream_Read_mB0EBC9488D9E3291AB4C6412F3AE70E8D6EEDBA7 (void);
// 0x00000879 System.Int32 System.Net.Sockets.NetworkStream::Read(System.Span`1<System.Byte>)
extern void NetworkStream_Read_m3D01BAC21011E126CED7B801A8E40CCEE363F267 (void);
// 0x0000087A System.Int32 System.Net.Sockets.NetworkStream::ReadByte()
extern void NetworkStream_ReadByte_m6C87F0C53F84DE07AF3EAC2E9FC963336642DCF3 (void);
// 0x0000087B System.Void System.Net.Sockets.NetworkStream::Write(System.Byte[],System.Int32,System.Int32)
extern void NetworkStream_Write_mFF7B408A0A6322FDEFB54BD0C759C3C6E6268AFE (void);
// 0x0000087C System.Void System.Net.Sockets.NetworkStream::Write(System.ReadOnlySpan`1<System.Byte>)
extern void NetworkStream_Write_m7EDA66BDCECB18E5642F9FE6EA9501A957A1AA8B (void);
// 0x0000087D System.Void System.Net.Sockets.NetworkStream::WriteByte(System.Byte)
extern void NetworkStream_WriteByte_m08B98BC1C6991DEE0FAE667899EDA2A2EE306E30 (void);
// 0x0000087E System.Void System.Net.Sockets.NetworkStream::Dispose(System.Boolean)
extern void NetworkStream_Dispose_m23F2CD1DBD8BBDCC96B36F93E663F3D5FF20B7F3 (void);
// 0x0000087F System.Void System.Net.Sockets.NetworkStream::Finalize()
extern void NetworkStream_Finalize_mE8FF3552F90C61575174F96CB6EF435268BE792A (void);
// 0x00000880 System.IAsyncResult System.Net.Sockets.NetworkStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void NetworkStream_BeginRead_mCC685D1B566FEC09C2AE76871AB01917D2446144 (void);
// 0x00000881 System.Int32 System.Net.Sockets.NetworkStream::EndRead(System.IAsyncResult)
extern void NetworkStream_EndRead_m53CDED13D3A6DD8D42F8533F2336D0EA6C752701 (void);
// 0x00000882 System.IAsyncResult System.Net.Sockets.NetworkStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void NetworkStream_BeginWrite_mE2F47607B52FC7FA99DD00337DB2AA0F5673E7C0 (void);
// 0x00000883 System.Void System.Net.Sockets.NetworkStream::EndWrite(System.IAsyncResult)
extern void NetworkStream_EndWrite_m843683738E14624DEBEF963D2480C8F3DB5EC848 (void);
// 0x00000884 System.Threading.Tasks.Task`1<System.Int32> System.Net.Sockets.NetworkStream::ReadAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void NetworkStream_ReadAsync_mB629CC680C16D2F2C45CF197D90D30DBD6050518 (void);
// 0x00000885 System.Threading.Tasks.ValueTask`1<System.Int32> System.Net.Sockets.NetworkStream::ReadAsync(System.Memory`1<System.Byte>,System.Threading.CancellationToken)
extern void NetworkStream_ReadAsync_mD7A23469A79B0E7C89FF03F5566997F89D839E0B (void);
// 0x00000886 System.Threading.Tasks.Task System.Net.Sockets.NetworkStream::WriteAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void NetworkStream_WriteAsync_m1829F2663EE9DA443488E4031D103DE4E28F2C73 (void);
// 0x00000887 System.Threading.Tasks.ValueTask System.Net.Sockets.NetworkStream::WriteAsync(System.ReadOnlyMemory`1<System.Byte>,System.Threading.CancellationToken)
extern void NetworkStream_WriteAsync_mF9FEB5879FAB94387395E59D435B94095CE9D59D (void);
// 0x00000888 System.Void System.Net.Sockets.NetworkStream::Flush()
extern void NetworkStream_Flush_m8DAEB9385D9E7B5531733900D507531E0B628F9B (void);
// 0x00000889 System.Void System.Net.Sockets.NetworkStream::SetLength(System.Int64)
extern void NetworkStream_SetLength_m6A2E5648D163C03687A1D3CCCD5C270C5D9FB68F (void);
// 0x0000088A System.Threading.Tasks.ValueTask`1<System.Int32> System.Net.Sockets.Socket::ReceiveAsync(System.Memory`1<System.Byte>,System.Net.Sockets.SocketFlags,System.Boolean,System.Threading.CancellationToken)
extern void Socket_ReceiveAsync_m04BFC87C7857569298C3C39387F24556559BAC36 (void);
// 0x0000088B System.Threading.Tasks.Task`1<System.Int32> System.Net.Sockets.Socket::ReceiveAsyncApm(System.Memory`1<System.Byte>,System.Net.Sockets.SocketFlags)
extern void Socket_ReceiveAsyncApm_mD6A803998C4FF807388FA600EAD6146A601E1709 (void);
// 0x0000088C System.Threading.Tasks.ValueTask System.Net.Sockets.Socket::SendAsyncForNetworkStream(System.ReadOnlyMemory`1<System.Byte>,System.Net.Sockets.SocketFlags,System.Threading.CancellationToken)
extern void Socket_SendAsyncForNetworkStream_m8A5FA8416ED841852D05A8232320F2498A7EABAD (void);
// 0x0000088D System.Threading.Tasks.Task`1<System.Int32> System.Net.Sockets.Socket::SendAsyncApm(System.ReadOnlyMemory`1<System.Byte>,System.Net.Sockets.SocketFlags)
extern void Socket_SendAsyncApm_m1B2DB2B58903CCB8577C99C132896C3F9E7B0724 (void);
// 0x0000088E System.Void System.Net.Sockets.Socket::CompleteAccept(System.Net.Sockets.Socket,System.Net.Sockets.Socket/TaskSocketAsyncEventArgs`1<System.Net.Sockets.Socket>)
extern void Socket_CompleteAccept_m1318F59CE726ED625516D2FE6769493F4D4F9C3D (void);
// 0x0000088F System.Void System.Net.Sockets.Socket::CompleteSendReceive(System.Net.Sockets.Socket,System.Net.Sockets.Socket/Int32TaskSocketAsyncEventArgs,System.Boolean)
extern void Socket_CompleteSendReceive_m56B142FD1AA39E5AFE655A9AAB5B531AA9CDF14C (void);
// 0x00000890 System.Exception System.Net.Sockets.Socket::GetException(System.Net.Sockets.SocketError,System.Boolean)
extern void Socket_GetException_mB0C16F7E32740B9EA9E34144BBE98B5EC002DA06 (void);
// 0x00000891 System.Void System.Net.Sockets.Socket::ReturnSocketAsyncEventArgs(System.Net.Sockets.Socket/Int32TaskSocketAsyncEventArgs,System.Boolean)
extern void Socket_ReturnSocketAsyncEventArgs_mF4A7BE8F1238309B0DA23245D0571CD0DE913913 (void);
// 0x00000892 System.Void System.Net.Sockets.Socket::ReturnSocketAsyncEventArgs(System.Net.Sockets.Socket/TaskSocketAsyncEventArgs`1<System.Net.Sockets.Socket>)
extern void Socket_ReturnSocketAsyncEventArgs_m6A467DCD60E641529A89C3521762C1C9A1482F9A (void);
// 0x00000893 System.Void System.Net.Sockets.Socket::.ctor(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType)
extern void Socket__ctor_m35F1F4B4872E251867DA16460F06E903A30E4595 (void);
// 0x00000894 System.Boolean System.Net.Sockets.Socket::get_OSSupportsIPv4()
extern void Socket_get_OSSupportsIPv4_m779A662683A5FBD8D7B4D1E5E940B04C6AFA0120 (void);
// 0x00000895 System.Boolean System.Net.Sockets.Socket::get_OSSupportsIPv6()
extern void Socket_get_OSSupportsIPv6_m45BC8FD78EDFCB853CA509A8DDD23EF42203B6D4 (void);
// 0x00000896 System.IntPtr System.Net.Sockets.Socket::get_Handle()
extern void Socket_get_Handle_mBF42AC5E9403C23BE532F17EE337BF9F150A0256 (void);
// 0x00000897 System.Net.Sockets.AddressFamily System.Net.Sockets.Socket::get_AddressFamily()
extern void Socket_get_AddressFamily_m42C390D31345314080EC35356ACFBBFF7E1123E5 (void);
// 0x00000898 System.Net.Sockets.SocketType System.Net.Sockets.Socket::get_SocketType()
extern void Socket_get_SocketType_mAFBEF108992F069AF0B8AD26D47EBBEADB8D2661 (void);
// 0x00000899 System.Net.Sockets.ProtocolType System.Net.Sockets.Socket::get_ProtocolType()
extern void Socket_get_ProtocolType_m364A0E823BF3EA78DAFC1ED3A6B39658FB6A23BB (void);
// 0x0000089A System.Void System.Net.Sockets.Socket::set_ExclusiveAddressUse(System.Boolean)
extern void Socket_set_ExclusiveAddressUse_m3F9A655F123086A025AD1736933B0754A5A6DF7F (void);
// 0x0000089B System.Int32 System.Net.Sockets.Socket::get_ReceiveBufferSize()
extern void Socket_get_ReceiveBufferSize_m863A6D0F34BA3B8CD450346B17842AB8C3887A75 (void);
// 0x0000089C System.Void System.Net.Sockets.Socket::set_ReceiveBufferSize(System.Int32)
extern void Socket_set_ReceiveBufferSize_mFCBD973C71C5E4883D3E3C6E822652373ED9CCD6 (void);
// 0x0000089D System.Int32 System.Net.Sockets.Socket::get_SendBufferSize()
extern void Socket_get_SendBufferSize_mC9F6F5099083DEADBFAC760012C883F823BDD706 (void);
// 0x0000089E System.Void System.Net.Sockets.Socket::set_SendBufferSize(System.Int32)
extern void Socket_set_SendBufferSize_mBFC47E7A4581A220FBFE8B113CE828EFD7F4EF33 (void);
// 0x0000089F System.Void System.Net.Sockets.Socket::set_DontFragment(System.Boolean)
extern void Socket_set_DontFragment_mCF6F7D2735A7FD4E327617826C62ED015001DFF6 (void);
// 0x000008A0 System.Boolean System.Net.Sockets.Socket::get_DualMode()
extern void Socket_get_DualMode_m8B45BA120D7A03DB5EF8C3CF9483C829E19CEFCA (void);
// 0x000008A1 System.Void System.Net.Sockets.Socket::set_DualMode(System.Boolean)
extern void Socket_set_DualMode_m08152574129C01AC8941AAB43752C3CD1525C193 (void);
// 0x000008A2 System.Boolean System.Net.Sockets.Socket::get_IsDualMode()
extern void Socket_get_IsDualMode_m1CB3E1554CD046481E3408B20D887C7EC8470CFA (void);
// 0x000008A3 System.Boolean System.Net.Sockets.Socket::CanTryAddressFamily(System.Net.Sockets.AddressFamily)
extern void Socket_CanTryAddressFamily_m11E16D00AD0F3BBA149B4E313D67CC12E2735EDB (void);
// 0x000008A4 System.Int32 System.Net.Sockets.Socket::Send(System.Byte[],System.Int32,System.Net.Sockets.SocketFlags)
extern void Socket_Send_m289DAB977DFA1BBE87E545BC7452464B0C38812E (void);
// 0x000008A5 System.Int32 System.Net.Sockets.Socket::Send(System.Collections.Generic.IList`1<System.ArraySegment`1<System.Byte>>,System.Net.Sockets.SocketFlags)
extern void Socket_Send_m3174031DF8ADFDAF158BEB0D8AC5CBB445F8D21C (void);
// 0x000008A6 System.Int32 System.Net.Sockets.Socket::Send(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags)
extern void Socket_Send_mF846BE4031BE538814BC497FFDC8BB02AC9F5A33 (void);
// 0x000008A7 System.Int32 System.Net.Sockets.Socket::Receive(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags)
extern void Socket_Receive_m29FED7D6FE4F95DD3B71CE6425AE19A3781FADBE (void);
// 0x000008A8 System.Int32 System.Net.Sockets.Socket::Receive(System.Collections.Generic.IList`1<System.ArraySegment`1<System.Byte>>,System.Net.Sockets.SocketFlags)
extern void Socket_Receive_mF4189DF1BB210034B41CC593C0FEC2F21D8DFF47 (void);
// 0x000008A9 System.Int32 System.Net.Sockets.Socket::ReceiveFrom(System.Byte[],System.Net.EndPoint&)
extern void Socket_ReceiveFrom_m0E841C82B5F0DB9CAF3A7C0E72E64C3C22A1A09D (void);
// 0x000008AA System.Void System.Net.Sockets.Socket::SetIPProtectionLevel(System.Net.Sockets.IPProtectionLevel)
extern void Socket_SetIPProtectionLevel_mC243CFB942D4DA0F2A148A0DAD3B5559DB005CDB (void);
// 0x000008AB System.IAsyncResult System.Net.Sockets.Socket::BeginSend(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.AsyncCallback,System.Object)
extern void Socket_BeginSend_m36452399B2D44A215795BFB0F1E06AEFD451E7C3 (void);
// 0x000008AC System.Int32 System.Net.Sockets.Socket::EndSend(System.IAsyncResult)
extern void Socket_EndSend_mC636AB62DC23FA2E8BDB612F5CDB203E6CAE4D17 (void);
// 0x000008AD System.IAsyncResult System.Net.Sockets.Socket::BeginReceive(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.AsyncCallback,System.Object)
extern void Socket_BeginReceive_mCCBDCDF0EE9A2AD7345F93A3931F2C426AFA4738 (void);
// 0x000008AE System.Int32 System.Net.Sockets.Socket::EndReceive(System.IAsyncResult)
extern void Socket_EndReceive_mD86DB01FE084056F0DA43DBB82B6B491CA8F5364 (void);
// 0x000008AF System.Object System.Net.Sockets.Socket::get_InternalSyncObject()
extern void Socket_get_InternalSyncObject_m11CB9CA23D0CDFF801C26121CB479BA162D19ACD (void);
// 0x000008B0 System.Boolean System.Net.Sockets.Socket::get_CleanedUp()
extern void Socket_get_CleanedUp_m8F6EDAE37C56CBC282D3EAF9D93B5726EEC85988 (void);
// 0x000008B1 System.Void System.Net.Sockets.Socket::InitializeSockets()
extern void Socket_InitializeSockets_mF69CCC3B14A53FC7CD95F6E87FC726812ADB3AE2 (void);
// 0x000008B2 System.Void System.Net.Sockets.Socket::Dispose()
extern void Socket_Dispose_mED966D36CA8914F199BCE9497FFC5261AC99C1AC (void);
// 0x000008B3 System.Void System.Net.Sockets.Socket::Finalize()
extern void Socket_Finalize_m1350F4F84C06E9DF703C32E47B1579C84A22B742 (void);
// 0x000008B4 System.Void System.Net.Sockets.Socket::InternalShutdown(System.Net.Sockets.SocketShutdown)
extern void Socket_InternalShutdown_mD0EF08D54151D53369D32CD61C8518454AAAEFE8 (void);
// 0x000008B5 System.Void System.Net.Sockets.Socket::.ctor(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType,System.Net.Sockets.SafeSocketHandle)
extern void Socket__ctor_m91002BA76E3B56D33CDEB6D30EFE923F92822CE4 (void);
// 0x000008B6 System.Void System.Net.Sockets.Socket::SocketDefaults()
extern void Socket_SocketDefaults_mEEA679EDCFED63435F630A133916BD5F97BF1FFB (void);
// 0x000008B7 System.IntPtr System.Net.Sockets.Socket::Socket_icall(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType,System.Int32&)
extern void Socket_Socket_icall_m835339B54AFB8EC75F01DE329512C2DA3FF9A99E (void);
// 0x000008B8 System.Void System.Net.Sockets.Socket::set_EnableBroadcast(System.Boolean)
extern void Socket_set_EnableBroadcast_m8C25D9941F2D9BD40277F8D3395400993F4CF1CF (void);
// 0x000008B9 System.Boolean System.Net.Sockets.Socket::get_IsBound()
extern void Socket_get_IsBound_m6CA0A4232291A7A0B99FCE2E5662A576D31A0A9D (void);
// 0x000008BA System.Void System.Net.Sockets.Socket::set_MulticastLoopback(System.Boolean)
extern void Socket_set_MulticastLoopback_m58B314C4A975622C94B7AE7AD29BB7333B2FF38B (void);
// 0x000008BB System.Boolean System.Net.Sockets.Socket::get_Blocking()
extern void Socket_get_Blocking_mE3DB6D71A54F6376624DDA9522C783B914860ED3 (void);
// 0x000008BC System.Void System.Net.Sockets.Socket::set_Blocking(System.Boolean)
extern void Socket_set_Blocking_mAE329D3DA788B19FDDC6B773F5B0A1C31FBE9A7B (void);
// 0x000008BD System.Void System.Net.Sockets.Socket::Blocking_internal(System.Net.Sockets.SafeSocketHandle,System.Boolean,System.Int32&)
extern void Socket_Blocking_internal_mCD8DDA2B42FCD3694E0DF5977ECDCFB080CA54BA (void);
// 0x000008BE System.Void System.Net.Sockets.Socket::Blocking_icall(System.IntPtr,System.Boolean,System.Int32&)
extern void Socket_Blocking_icall_m822EDC3A6E0C02C7351B93395BFC0D6103997323 (void);
// 0x000008BF System.Boolean System.Net.Sockets.Socket::get_Connected()
extern void Socket_get_Connected_m6E61ADEB0124FB8D46AC1EDFDC268D3C9F7869C5 (void);
// 0x000008C0 System.Void System.Net.Sockets.Socket::set_NoDelay(System.Boolean)
extern void Socket_set_NoDelay_m1F7ACCEF219BB8745AB17F29CEA618088E0B2405 (void);
// 0x000008C1 System.Net.EndPoint System.Net.Sockets.Socket::get_RemoteEndPoint()
extern void Socket_get_RemoteEndPoint_m6A626481D55A640C7654B91BFDE00199C2C95479 (void);
// 0x000008C2 System.Net.SocketAddress System.Net.Sockets.Socket::RemoteEndPoint_internal(System.Net.Sockets.SafeSocketHandle,System.Int32,System.Int32&)
extern void Socket_RemoteEndPoint_internal_mB9463EBBCBDCFD1C6BE384E07C972F3BB6D57FC8 (void);
// 0x000008C3 System.Net.SocketAddress System.Net.Sockets.Socket::RemoteEndPoint_icall(System.IntPtr,System.Int32,System.Int32&)
extern void Socket_RemoteEndPoint_icall_mCD9AE17D25710602FF67C6BC45BA1BB773B0A230 (void);
// 0x000008C4 System.Boolean System.Net.Sockets.Socket::Poll(System.Int32,System.Net.Sockets.SelectMode)
extern void Socket_Poll_m65D8E383FBE5A1D4A115942182620422B6646B98 (void);
// 0x000008C5 System.Boolean System.Net.Sockets.Socket::Poll_internal(System.Net.Sockets.SafeSocketHandle,System.Net.Sockets.SelectMode,System.Int32,System.Int32&)
extern void Socket_Poll_internal_mA9D6DA0F9323BE621CD3ACBEBEA80641F55AAA11 (void);
// 0x000008C6 System.Boolean System.Net.Sockets.Socket::Poll_icall(System.IntPtr,System.Net.Sockets.SelectMode,System.Int32,System.Int32&)
extern void Socket_Poll_icall_m4335903786D9CA9197FF532517CE5BD3BB4D3951 (void);
// 0x000008C7 System.Net.Sockets.Socket System.Net.Sockets.Socket::Accept()
extern void Socket_Accept_m11B031FF408E1899034B993B352178D9D6B37C1A (void);
// 0x000008C8 System.Void System.Net.Sockets.Socket::Accept(System.Net.Sockets.Socket)
extern void Socket_Accept_mAF83623B54FEA675F0F16C29EACE1F24DE6C02FF (void);
// 0x000008C9 System.Net.Sockets.Socket System.Net.Sockets.Socket::EndAccept(System.IAsyncResult)
extern void Socket_EndAccept_m6A7BE15D82A8C8DCA5AD786CC97E0A12FF354D09 (void);
// 0x000008CA System.Net.Sockets.Socket System.Net.Sockets.Socket::EndAccept(System.Byte[]&,System.Int32&,System.IAsyncResult)
extern void Socket_EndAccept_m96B08273E42578EAB0A297A707A1F15956F69FB0 (void);
// 0x000008CB System.Net.Sockets.SafeSocketHandle System.Net.Sockets.Socket::Accept_internal(System.Net.Sockets.SafeSocketHandle,System.Int32&,System.Boolean)
extern void Socket_Accept_internal_m993249D44B5193CB8B5CFFDDEDBCE4D4DFBB9D6A (void);
// 0x000008CC System.IntPtr System.Net.Sockets.Socket::Accept_icall(System.IntPtr,System.Int32&,System.Boolean)
extern void Socket_Accept_icall_m7EF3E68DB5166B5A2F185C50A1DDD23DDF62B851 (void);
// 0x000008CD System.Void System.Net.Sockets.Socket::Bind(System.Net.EndPoint)
extern void Socket_Bind_m137559EBA78A72ED4ADF8B56F5C535CE638165AA (void);
// 0x000008CE System.Void System.Net.Sockets.Socket::Bind_internal(System.Net.Sockets.SafeSocketHandle,System.Net.SocketAddress,System.Int32&)
extern void Socket_Bind_internal_m85DF18886A077D271720DCF0ACDA05AACED2B7C8 (void);
// 0x000008CF System.Void System.Net.Sockets.Socket::Bind_icall(System.IntPtr,System.Net.SocketAddress,System.Int32&)
extern void Socket_Bind_icall_m2FC33FF6853AE0E221EF889F9EB212D461A3D1B8 (void);
// 0x000008D0 System.Void System.Net.Sockets.Socket::Listen(System.Int32)
extern void Socket_Listen_m734803DAC514E64828168AF9212D6A9EC63B32EC (void);
// 0x000008D1 System.Void System.Net.Sockets.Socket::Listen_internal(System.Net.Sockets.SafeSocketHandle,System.Int32,System.Int32&)
extern void Socket_Listen_internal_m2802EDACE64C106C3E87CFE335BE2507486B6DD4 (void);
// 0x000008D2 System.Void System.Net.Sockets.Socket::Listen_icall(System.IntPtr,System.Int32,System.Int32&)
extern void Socket_Listen_icall_m4590ECBAFC507156C673C4D3FA6EDD53DD8F625E (void);
// 0x000008D3 System.Void System.Net.Sockets.Socket::Connect(System.Net.IPAddress,System.Int32)
extern void Socket_Connect_mEE1E8320C097486AFA5432284C1B7FFB0DDC35D6 (void);
// 0x000008D4 System.Void System.Net.Sockets.Socket::Connect(System.Net.EndPoint)
extern void Socket_Connect_m874A510F986F4D91446B2F53688D0BCFFE0A6BE1 (void);
// 0x000008D5 System.Boolean System.Net.Sockets.Socket::BeginMConnect(System.Net.Sockets.SocketAsyncResult)
extern void Socket_BeginMConnect_mACF037091CD1CDEBF28695402C2CE7B8E230A6AE (void);
// 0x000008D6 System.Boolean System.Net.Sockets.Socket::BeginSConnect(System.Net.Sockets.SocketAsyncResult)
extern void Socket_BeginSConnect_m3C0B6709149C420AEAED6A3F124AD2044954F4C6 (void);
// 0x000008D7 System.Void System.Net.Sockets.Socket::EndConnect(System.IAsyncResult)
extern void Socket_EndConnect_m10F5B4F43A68F96891DB3AEF493AB160CD8104D6 (void);
// 0x000008D8 System.Void System.Net.Sockets.Socket::Connect_internal(System.Net.Sockets.SafeSocketHandle,System.Net.SocketAddress,System.Int32&,System.Boolean)
extern void Socket_Connect_internal_mB28CEC91E12F9B621FF2B074CACC575629E33746 (void);
// 0x000008D9 System.Void System.Net.Sockets.Socket::Connect_icall(System.IntPtr,System.Net.SocketAddress,System.Int32&,System.Boolean)
extern void Socket_Connect_icall_m32BE8ABC1B8535BC195F0BEC382408DD0D3B4E26 (void);
// 0x000008DA System.Void System.Net.Sockets.Socket::Disconnect(System.Boolean)
extern void Socket_Disconnect_mFECF870A7A5604A9AFD79DBB6354F1697A96C422 (void);
// 0x000008DB System.Void System.Net.Sockets.Socket::EndDisconnect(System.IAsyncResult)
extern void Socket_EndDisconnect_mCD874CA31D96229FBF311AB941DFB1FAE4897BE8 (void);
// 0x000008DC System.Void System.Net.Sockets.Socket::Disconnect_internal(System.Net.Sockets.SafeSocketHandle,System.Boolean,System.Int32&)
extern void Socket_Disconnect_internal_m78AD76E91C83AE6CE27D4FC952FD1D28EA33FE91 (void);
// 0x000008DD System.Void System.Net.Sockets.Socket::Disconnect_icall(System.IntPtr,System.Boolean,System.Int32&)
extern void Socket_Disconnect_icall_m2BE84F38BC9AB58C78B466C044968413A8DB4289 (void);
// 0x000008DE System.Int32 System.Net.Sockets.Socket::Receive(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.Net.Sockets.SocketError&)
extern void Socket_Receive_mF387A0975239872402A6118D3F196B020060021C (void);
// 0x000008DF System.Int32 System.Net.Sockets.Socket::Receive(System.Memory`1<System.Byte>,System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.Net.Sockets.SocketError&)
extern void Socket_Receive_m42AB6119254FA391A818CADF23C60441ECA29152 (void);
// 0x000008E0 System.Int32 System.Net.Sockets.Socket::Receive(System.Collections.Generic.IList`1<System.ArraySegment`1<System.Byte>>,System.Net.Sockets.SocketFlags,System.Net.Sockets.SocketError&)
extern void Socket_Receive_m51DFAB2284E194E286E5B086F874EFDA80A7C687 (void);
// 0x000008E1 System.Int32 System.Net.Sockets.Socket::Receive(System.Span`1<System.Byte>,System.Net.Sockets.SocketFlags,System.Net.Sockets.SocketError&)
extern void Socket_Receive_m08FAD3EAB5697251B5E6258AFB8C5E2F032BE1E6 (void);
// 0x000008E2 System.Int32 System.Net.Sockets.Socket::Send(System.ReadOnlySpan`1<System.Byte>,System.Net.Sockets.SocketFlags,System.Net.Sockets.SocketError&)
extern void Socket_Send_m4A03A1017A508D72593092EBB9DC6DC713C24F30 (void);
// 0x000008E3 System.Boolean System.Net.Sockets.Socket::ReceiveAsync(System.Net.Sockets.SocketAsyncEventArgs)
extern void Socket_ReceiveAsync_m158A08D073322BE419CDF11B449F07191F7576CA (void);
// 0x000008E4 System.IAsyncResult System.Net.Sockets.Socket::BeginReceive(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.Net.Sockets.SocketError&,System.AsyncCallback,System.Object)
extern void Socket_BeginReceive_m6CE71DB94ED6C33A5B0921F13530283D68979B54 (void);
// 0x000008E5 System.Int32 System.Net.Sockets.Socket::EndReceive(System.IAsyncResult,System.Net.Sockets.SocketError&)
extern void Socket_EndReceive_m080D7964C40358B101AA6AF0B55635B3CBC9DFB3 (void);
// 0x000008E6 System.Int32 System.Net.Sockets.Socket::Receive_internal(System.Net.Sockets.SafeSocketHandle,System.Net.Sockets.Socket/WSABUF*,System.Int32,System.Net.Sockets.SocketFlags,System.Int32&,System.Boolean)
extern void Socket_Receive_internal_mD1316BB323E539121369401B592F3B054551E90A (void);
// 0x000008E7 System.Int32 System.Net.Sockets.Socket::Receive_array_icall(System.IntPtr,System.Net.Sockets.Socket/WSABUF*,System.Int32,System.Net.Sockets.SocketFlags,System.Int32&,System.Boolean)
extern void Socket_Receive_array_icall_m1447B2BB5E3E980D3BA40BBD910C69AF33ED22AF (void);
// 0x000008E8 System.Int32 System.Net.Sockets.Socket::Receive_internal(System.Net.Sockets.SafeSocketHandle,System.Byte*,System.Int32,System.Net.Sockets.SocketFlags,System.Int32&,System.Boolean)
extern void Socket_Receive_internal_mFFD3DE5D5B14A0730765CE470002536254B94789 (void);
// 0x000008E9 System.Int32 System.Net.Sockets.Socket::Receive_icall(System.IntPtr,System.Byte*,System.Int32,System.Net.Sockets.SocketFlags,System.Int32&,System.Boolean)
extern void Socket_Receive_icall_m970B475156F96287ED2C1C919FA6AD982816A85E (void);
// 0x000008EA System.Int32 System.Net.Sockets.Socket::ReceiveFrom(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.Net.EndPoint&)
extern void Socket_ReceiveFrom_m780282028E62CDD6021E3B8917656AC820878E6E (void);
// 0x000008EB System.Int32 System.Net.Sockets.Socket::ReceiveFrom(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.Net.EndPoint&,System.Net.Sockets.SocketError&)
extern void Socket_ReceiveFrom_mFCCD977ED8761DBBF13EC14BEFC4615662CE501C (void);
// 0x000008EC System.Int32 System.Net.Sockets.Socket::ReceiveFrom(System.Memory`1<System.Byte>,System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.Net.EndPoint&,System.Net.Sockets.SocketError&)
extern void Socket_ReceiveFrom_m6C9B4FF70B221818FD01FBE94E5635B169686B70 (void);
// 0x000008ED System.IAsyncResult System.Net.Sockets.Socket::BeginReceiveFrom(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.Net.EndPoint&,System.AsyncCallback,System.Object)
extern void Socket_BeginReceiveFrom_mF3DA6F22C3107DB734B011D045D11AA976B6C84C (void);
// 0x000008EE System.Int32 System.Net.Sockets.Socket::EndReceiveFrom(System.IAsyncResult,System.Net.EndPoint&)
extern void Socket_EndReceiveFrom_mAE3C7B82193AD982F47EE74B8C75CB52E7DB15BA (void);
// 0x000008EF System.Int32 System.Net.Sockets.Socket::EndReceiveFrom_internal(System.Net.Sockets.SocketAsyncResult,System.Net.Sockets.SocketAsyncEventArgs)
extern void Socket_EndReceiveFrom_internal_m460268E3D25241DA2EFCBBCA8B9A553AD07C6590 (void);
// 0x000008F0 System.Int32 System.Net.Sockets.Socket::ReceiveFrom_internal(System.Net.Sockets.SafeSocketHandle,System.Byte*,System.Int32,System.Net.Sockets.SocketFlags,System.Net.SocketAddress&,System.Int32&,System.Boolean)
extern void Socket_ReceiveFrom_internal_m5C01BB7E993A7899973D0FC82A6E2D86F8F813AE (void);
// 0x000008F1 System.Int32 System.Net.Sockets.Socket::ReceiveFrom_icall(System.IntPtr,System.Byte*,System.Int32,System.Net.Sockets.SocketFlags,System.Net.SocketAddress&,System.Int32&,System.Boolean)
extern void Socket_ReceiveFrom_icall_m583CD09AB1E40E32DCA9C7984C58B5CB866AB51E (void);
// 0x000008F2 System.Int32 System.Net.Sockets.Socket::Send(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.Net.Sockets.SocketError&)
extern void Socket_Send_mC5F7C598421E94D9098B7A65073193E7E574A1C7 (void);
// 0x000008F3 System.Int32 System.Net.Sockets.Socket::Send(System.Collections.Generic.IList`1<System.ArraySegment`1<System.Byte>>,System.Net.Sockets.SocketFlags,System.Net.Sockets.SocketError&)
extern void Socket_Send_m6414CA18C059F7262E069F92A17A2B31BB83E2F7 (void);
// 0x000008F4 System.Boolean System.Net.Sockets.Socket::SendAsync(System.Net.Sockets.SocketAsyncEventArgs)
extern void Socket_SendAsync_m0492E6DD02BBA2878FC8BAD3770F619B57370A26 (void);
// 0x000008F5 System.IAsyncResult System.Net.Sockets.Socket::BeginSend(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.Net.Sockets.SocketError&,System.AsyncCallback,System.Object)
extern void Socket_BeginSend_m0BBAE4C48B338F9F6EBFD4F0E6FE236FE4BDE64C (void);
// 0x000008F6 System.Void System.Net.Sockets.Socket::BeginSendCallback(System.Net.Sockets.SocketAsyncResult,System.Int32)
extern void Socket_BeginSendCallback_m7BAA6F54C7D4AEAF6BC6B2552D23C3464C0F787D (void);
// 0x000008F7 System.Int32 System.Net.Sockets.Socket::EndSend(System.IAsyncResult,System.Net.Sockets.SocketError&)
extern void Socket_EndSend_m78EE767A837539CAFEA99FA8A2EB0EF24444123E (void);
// 0x000008F8 System.Int32 System.Net.Sockets.Socket::Send_internal(System.Net.Sockets.SafeSocketHandle,System.Net.Sockets.Socket/WSABUF*,System.Int32,System.Net.Sockets.SocketFlags,System.Int32&,System.Boolean)
extern void Socket_Send_internal_mAED1C3A20DB7EB11A59927CDD3ABC5579D0C43BB (void);
// 0x000008F9 System.Int32 System.Net.Sockets.Socket::Send_array_icall(System.IntPtr,System.Net.Sockets.Socket/WSABUF*,System.Int32,System.Net.Sockets.SocketFlags,System.Int32&,System.Boolean)
extern void Socket_Send_array_icall_mFDA9B09614B64B5143D8FF0BCE40751A6D7B956D (void);
// 0x000008FA System.Int32 System.Net.Sockets.Socket::Send_internal(System.Net.Sockets.SafeSocketHandle,System.Byte*,System.Int32,System.Net.Sockets.SocketFlags,System.Int32&,System.Boolean)
extern void Socket_Send_internal_mAEE2FA94300B035241FCE5C276EAA4B956FA89B2 (void);
// 0x000008FB System.Int32 System.Net.Sockets.Socket::Send_icall(System.IntPtr,System.Byte*,System.Int32,System.Net.Sockets.SocketFlags,System.Int32&,System.Boolean)
extern void Socket_Send_icall_m5FFD8AFD5D12879C21F92A2C56D5DC475F69716A (void);
// 0x000008FC System.Int32 System.Net.Sockets.Socket::SendTo(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.Net.EndPoint)
extern void Socket_SendTo_m07A6D82F7ABD61B6B9C87931035FCF793AA3D6F6 (void);
// 0x000008FD System.Int32 System.Net.Sockets.Socket::SendTo(System.Memory`1<System.Byte>,System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.Net.EndPoint)
extern void Socket_SendTo_m06ACC9DF41CAF14666D8CC00719FCCD48DAE3153 (void);
// 0x000008FE System.IAsyncResult System.Net.Sockets.Socket::BeginSendTo(System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,System.Net.EndPoint,System.AsyncCallback,System.Object)
extern void Socket_BeginSendTo_m0FA2AFB253E22B89BA454BEEB59B403E6DAD134C (void);
// 0x000008FF System.Void System.Net.Sockets.Socket::BeginSendToCallback(System.Net.Sockets.SocketAsyncResult,System.Int32)
extern void Socket_BeginSendToCallback_m304F7045084969E1B54635CFBD812CCA9CADB697 (void);
// 0x00000900 System.Int32 System.Net.Sockets.Socket::EndSendTo(System.IAsyncResult)
extern void Socket_EndSendTo_mC35D110ED10CFAFEF443A1AEFCDEA438DE0101EC (void);
// 0x00000901 System.Int32 System.Net.Sockets.Socket::SendTo_internal(System.Net.Sockets.SafeSocketHandle,System.Byte*,System.Int32,System.Net.Sockets.SocketFlags,System.Net.SocketAddress,System.Int32&,System.Boolean)
extern void Socket_SendTo_internal_m5820CA4730A4ECB2613FA6D9303D74FB668EDC1B (void);
// 0x00000902 System.Int32 System.Net.Sockets.Socket::SendTo_icall(System.IntPtr,System.Byte*,System.Int32,System.Net.Sockets.SocketFlags,System.Net.SocketAddress,System.Int32&,System.Boolean)
extern void Socket_SendTo_icall_m44EC5697BA880F9342179D257EB34600ACA44FB6 (void);
// 0x00000903 System.Object System.Net.Sockets.Socket::GetSocketOption(System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName)
extern void Socket_GetSocketOption_m39C453F9FA4D1EC664C660851CED73271B1162A2 (void);
// 0x00000904 System.Void System.Net.Sockets.Socket::GetSocketOption_obj_internal(System.Net.Sockets.SafeSocketHandle,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,System.Object&,System.Int32&)
extern void Socket_GetSocketOption_obj_internal_m5C6F74BA7CBC5A7AF3FED034A1557A2CD65804EA (void);
// 0x00000905 System.Void System.Net.Sockets.Socket::GetSocketOption_obj_icall(System.IntPtr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,System.Object&,System.Int32&)
extern void Socket_GetSocketOption_obj_icall_m95FC9B4255A7445D549B200032C94E74699CF1FD (void);
// 0x00000906 System.Void System.Net.Sockets.Socket::SetSocketOption(System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,System.Boolean)
extern void Socket_SetSocketOption_mE47F5DEEA190E45317AEEE6F1506940CB8E943A1 (void);
// 0x00000907 System.Void System.Net.Sockets.Socket::SetSocketOption(System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,System.Int32)
extern void Socket_SetSocketOption_m19085C1856DE21260294680B7725610D71D66A58 (void);
// 0x00000908 System.Void System.Net.Sockets.Socket::SetSocketOption_internal(System.Net.Sockets.SafeSocketHandle,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,System.Object,System.Byte[],System.Int32,System.Int32&)
extern void Socket_SetSocketOption_internal_mF5D974153131AD9CA8713FA0F0549DE238C88CD5 (void);
// 0x00000909 System.Void System.Net.Sockets.Socket::SetSocketOption_icall(System.IntPtr,System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,System.Object,System.Byte[],System.Int32,System.Int32&)
extern void Socket_SetSocketOption_icall_m7F1AF1F4E424BB5306FBF5E0DACF35BD9A2650A4 (void);
// 0x0000090A System.Void System.Net.Sockets.Socket::Close()
extern void Socket_Close_m5EBF3D8BE2C42EF8037BC9372CE7760B1717EEE4 (void);
// 0x0000090B System.Void System.Net.Sockets.Socket::Close(System.Int32)
extern void Socket_Close_mE6673231EFBF35BBF5228BC5C7A2BA08DAD5237F (void);
// 0x0000090C System.Void System.Net.Sockets.Socket::Close_icall(System.IntPtr,System.Int32&)
extern void Socket_Close_icall_mBF92507692212E88F568DED543121A71DC710701 (void);
// 0x0000090D System.Void System.Net.Sockets.Socket::Shutdown_internal(System.Net.Sockets.SafeSocketHandle,System.Net.Sockets.SocketShutdown,System.Int32&)
extern void Socket_Shutdown_internal_mEF0F7AAEE1A7ECB909B871A31ABC8A44D08697EC (void);
// 0x0000090E System.Void System.Net.Sockets.Socket::Shutdown_icall(System.IntPtr,System.Net.Sockets.SocketShutdown,System.Int32&)
extern void Socket_Shutdown_icall_m3AD0F420C6DB7C30FBC8741BEA5D1F0D3ED7F14D (void);
// 0x0000090F System.Void System.Net.Sockets.Socket::Dispose(System.Boolean)
extern void Socket_Dispose_mD208C78BC53159B6E12EEAD70784ED7F19B38724 (void);
// 0x00000910 System.Void System.Net.Sockets.Socket::Linger(System.IntPtr)
extern void Socket_Linger_m24D121E76C38D9E302EF059712EE0C90B5312FBF (void);
// 0x00000911 System.Void System.Net.Sockets.Socket::ThrowIfDisposedAndClosed()
extern void Socket_ThrowIfDisposedAndClosed_m93567578970C10A4E56C7DF84D7D18A1E5AB8CE5 (void);
// 0x00000912 System.Void System.Net.Sockets.Socket::ThrowIfBufferNull(System.Byte[])
extern void Socket_ThrowIfBufferNull_m80732B6CF5F7BFA314905627BF1B9D28D4E2A41F (void);
// 0x00000913 System.Void System.Net.Sockets.Socket::ThrowIfBufferOutOfRange(System.Byte[],System.Int32,System.Int32)
extern void Socket_ThrowIfBufferOutOfRange_m22646D99174E99B57B816E260C93D959CE4FB143 (void);
// 0x00000914 System.Void System.Net.Sockets.Socket::ThrowIfUdp()
extern void Socket_ThrowIfUdp_m006AEC31161FCD9D704DBC20A4C8896B719ED6FA (void);
// 0x00000915 System.Net.Sockets.SocketAsyncResult System.Net.Sockets.Socket::ValidateEndIAsyncResult(System.IAsyncResult,System.String,System.String)
extern void Socket_ValidateEndIAsyncResult_m3B62AE8C5CB328C37CE3FB3264B5A0299E1DD3A3 (void);
// 0x00000916 System.Void System.Net.Sockets.Socket::QueueIOSelectorJob(System.Threading.SemaphoreSlim,System.IntPtr,System.IOSelectorJob)
extern void Socket_QueueIOSelectorJob_m7342EB18C3E806FFE6C87FC356C89E23064A7FB4 (void);
// 0x00000917 System.Void System.Net.Sockets.Socket::InitSocketAsyncEventArgs(System.Net.Sockets.SocketAsyncEventArgs,System.AsyncCallback,System.Object,System.Net.Sockets.SocketOperation)
extern void Socket_InitSocketAsyncEventArgs_m32B714DC68442471D169D0EF1CF36138694847C3 (void);
// 0x00000918 System.Net.Sockets.SocketAsyncOperation System.Net.Sockets.Socket::SocketOperationToSocketAsyncOperation(System.Net.Sockets.SocketOperation)
extern void Socket_SocketOperationToSocketAsyncOperation_mA3A0B1DB1F7A834F100915D3507DFFF381A5B6F4 (void);
// 0x00000919 System.Net.IPEndPoint System.Net.Sockets.Socket::RemapIPEndPoint(System.Net.IPEndPoint)
extern void Socket_RemapIPEndPoint_m8B03D3304FF55B1B423F96A2917F34732B258D73 (void);
// 0x0000091A System.Void System.Net.Sockets.Socket::cancel_blocking_socket_operation(System.Threading.Thread)
extern void Socket_cancel_blocking_socket_operation_mF7D69972B1CC9D769A3E5BF9946125C5CC661172 (void);
// 0x0000091B System.Int32 System.Net.Sockets.Socket::get_FamilyHint()
extern void Socket_get_FamilyHint_m133ABCFB7BF5184CFA0AA12E4BC031D9598CBAC8 (void);
// 0x0000091C System.Boolean System.Net.Sockets.Socket::IsProtocolSupported_internal(System.Net.NetworkInformation.NetworkInterfaceComponent)
extern void Socket_IsProtocolSupported_internal_m4C4E9B469DECA875DCDE5AFCA5B522F09C3B44EA (void);
// 0x0000091D System.Boolean System.Net.Sockets.Socket::IsProtocolSupported(System.Net.NetworkInformation.NetworkInterfaceComponent)
extern void Socket_IsProtocolSupported_m063B4A00EC7B0FF7FB37507F33C22BFBD0177F18 (void);
// 0x0000091E System.Void System.Net.Sockets.Socket::.cctor()
extern void Socket__cctor_mD7400247DF81848730C006F1CDFE51353C2F3F16 (void);
// 0x0000091F System.Void System.Net.Sockets.Socket/CachedEventArgs::.ctor()
extern void CachedEventArgs__ctor_mB9F52BB006EBBAD11F9C6E13F14C47971A1F36C9 (void);
// 0x00000920 System.Void System.Net.Sockets.Socket/TaskSocketAsyncEventArgs`1::.ctor()
// 0x00000921 System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<TResult> System.Net.Sockets.Socket/TaskSocketAsyncEventArgs`1::GetCompletionResponsibility(System.Boolean&)
// 0x00000922 System.Void System.Net.Sockets.Socket/Int32TaskSocketAsyncEventArgs::.ctor()
extern void Int32TaskSocketAsyncEventArgs__ctor_m2D28C3F386AE7BDAC321D2E13CA613FCCC9F7D60 (void);
// 0x00000923 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::.ctor()
extern void AwaitableSocketAsyncEventArgs__ctor_m57166E81DE7564E32B7FE2512A7C590A85F75F5C (void);
// 0x00000924 System.Boolean System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::get_WrapExceptionsInIOExceptions()
extern void AwaitableSocketAsyncEventArgs_get_WrapExceptionsInIOExceptions_m417FC84C2DCA30FFE3EF2DBC55646E410BB864A9 (void);
// 0x00000925 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::set_WrapExceptionsInIOExceptions(System.Boolean)
extern void AwaitableSocketAsyncEventArgs_set_WrapExceptionsInIOExceptions_m254606698A8C97790E95BEFE9B11A9AE528227B2 (void);
// 0x00000926 System.Boolean System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::Reserve()
extern void AwaitableSocketAsyncEventArgs_Reserve_m2A356885123F19F592BF5610380A73D7A315A8A7 (void);
// 0x00000927 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::Release()
extern void AwaitableSocketAsyncEventArgs_Release_m968CB82406E80CFDF9F3A77F23B7F1B1A44E7B3D (void);
// 0x00000928 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::OnCompleted(System.Net.Sockets.SocketAsyncEventArgs)
extern void AwaitableSocketAsyncEventArgs_OnCompleted_mC86BEBA0820E4B4BA4482692F5D3CD57960086B9 (void);
// 0x00000929 System.Threading.Tasks.ValueTask`1<System.Int32> System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::ReceiveAsync(System.Net.Sockets.Socket)
extern void AwaitableSocketAsyncEventArgs_ReceiveAsync_mC2BA1C5E24CB4AE2F2E1B69002DBD35FD10EF2BC (void);
// 0x0000092A System.Threading.Tasks.ValueTask System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::SendAsyncForNetworkStream(System.Net.Sockets.Socket)
extern void AwaitableSocketAsyncEventArgs_SendAsyncForNetworkStream_m47C17FAF97A11A8F1CBCBC0DBFB193EF88960D2B (void);
// 0x0000092B System.Threading.Tasks.Sources.ValueTaskSourceStatus System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::GetStatus(System.Int16)
extern void AwaitableSocketAsyncEventArgs_GetStatus_mC7723671714D0561D4240F0422721FE850287C39 (void);
// 0x0000092C System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16,System.Threading.Tasks.Sources.ValueTaskSourceOnCompletedFlags)
extern void AwaitableSocketAsyncEventArgs_OnCompleted_m4CEDC0CE4C641E978968BDAE34A6D9F3AD2C5E06 (void);
// 0x0000092D System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::InvokeContinuation(System.Action`1<System.Object>,System.Object,System.Boolean)
extern void AwaitableSocketAsyncEventArgs_InvokeContinuation_m77BA72041547ECDF9888FED13F15F35A7390BB17 (void);
// 0x0000092E System.Int32 System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::GetResult(System.Int16)
extern void AwaitableSocketAsyncEventArgs_GetResult_mD1285F6C0E77C62371227AEA7461BE494692845E (void);
// 0x0000092F System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::System.Threading.Tasks.Sources.IValueTaskSource.GetResult(System.Int16)
extern void AwaitableSocketAsyncEventArgs_System_Threading_Tasks_Sources_IValueTaskSource_GetResult_m275B7D8B48D925808E1B29EC32B07A2648F63BA1 (void);
// 0x00000930 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::ThrowIncorrectTokenException()
extern void AwaitableSocketAsyncEventArgs_ThrowIncorrectTokenException_mCF33DC63E9EC887CE52A959DE5760ECE9483D407 (void);
// 0x00000931 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::ThrowMultipleContinuationsException()
extern void AwaitableSocketAsyncEventArgs_ThrowMultipleContinuationsException_m23F43A76D940E78011D7B1AAE6170F5532E9BD0E (void);
// 0x00000932 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::ThrowException(System.Net.Sockets.SocketError)
extern void AwaitableSocketAsyncEventArgs_ThrowException_m77AE96856D358D880F32E1FA6323F9F161E214E4 (void);
// 0x00000933 System.Exception System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::CreateException(System.Net.Sockets.SocketError)
extern void AwaitableSocketAsyncEventArgs_CreateException_mCF6E0A048155F41AA05A8847FA0155FCAB47D6F5 (void);
// 0x00000934 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs::.cctor()
extern void AwaitableSocketAsyncEventArgs__cctor_m29C9980065E4ADD89B108387999A94A207CA572B (void);
// 0x00000935 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs/<>c::.cctor()
extern void U3CU3Ec__cctor_mC972E27AEB8CFE1FE7E12EA1E19A9B813715F1B1 (void);
// 0x00000936 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs/<>c::.ctor()
extern void U3CU3Ec__ctor_mE17ED0A084782AF1BB595DEC18E0560D76AEBA70 (void);
// 0x00000937 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs/<>c::<OnCompleted>b__14_0(System.Object)
extern void U3CU3Ec_U3COnCompletedU3Eb__14_0_m5314AD7E71DB2D2A543185D11CB8246091EF8257 (void);
// 0x00000938 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs/<>c::<InvokeContinuation>b__20_0(System.Object)
extern void U3CU3Ec_U3CInvokeContinuationU3Eb__20_0_m3439C73F5A9F402774FC521870BB0F0934E3DDC5 (void);
// 0x00000939 System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs/<>c::<.cctor>b__27_0(System.Object)
extern void U3CU3Ec_U3C_cctorU3Eb__27_0_m7B5E46EB80E344A6ECAD9C772E4E448AF0F5FF90 (void);
// 0x0000093A System.Void System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs/<>c::<.cctor>b__27_1(System.Object)
extern void U3CU3Ec_U3C_cctorU3Eb__27_1_mFB29CBF67405E81046B23B3025E43D9714CD80F9 (void);
// 0x0000093B System.Void System.Net.Sockets.Socket/<>c::.cctor()
extern void U3CU3Ec__cctor_m038CF32946815BC1EC22307A7B49E3CA24C281AF (void);
// 0x0000093C System.Void System.Net.Sockets.Socket/<>c::.ctor()
extern void U3CU3Ec__ctor_mBED1A732D3342D1A48A4088765AA6159F66308A3 (void);
// 0x0000093D System.Net.Sockets.Socket/CachedEventArgs System.Net.Sockets.Socket/<>c::<ReceiveAsync>b__14_0()
extern void U3CU3Ec_U3CReceiveAsyncU3Eb__14_0_mC59A3A03878FD1D393D4345962F6D3E6984EE609 (void);
// 0x0000093E System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs System.Net.Sockets.Socket/<>c::<ReceiveAsync>b__14_1()
extern void U3CU3Ec_U3CReceiveAsyncU3Eb__14_1_m06418F2989013CBF63EA4DE96C8E82F79AA8580D (void);
// 0x0000093F System.Void System.Net.Sockets.Socket/<>c::<ReceiveAsyncApm>b__15_0(System.IAsyncResult)
extern void U3CU3Ec_U3CReceiveAsyncApmU3Eb__15_0_m6059641EBB33F2508CD3F8AD1768816383497446 (void);
// 0x00000940 System.Void System.Net.Sockets.Socket/<>c::<ReceiveAsyncApm>b__15_1(System.IAsyncResult)
extern void U3CU3Ec_U3CReceiveAsyncApmU3Eb__15_1_mD81706B8EDF964E3FF8CF9B7717AF288C9131DEE (void);
// 0x00000941 System.Net.Sockets.Socket/CachedEventArgs System.Net.Sockets.Socket/<>c::<SendAsyncForNetworkStream>b__22_0()
extern void U3CU3Ec_U3CSendAsyncForNetworkStreamU3Eb__22_0_m42A7692C372A4888E5D2070BFD402F5DB136F51D (void);
// 0x00000942 System.Net.Sockets.Socket/AwaitableSocketAsyncEventArgs System.Net.Sockets.Socket/<>c::<SendAsyncForNetworkStream>b__22_1()
extern void U3CU3Ec_U3CSendAsyncForNetworkStreamU3Eb__22_1_mB8B5FE244D7173851FD875760A7D1940A8B60B79 (void);
// 0x00000943 System.Void System.Net.Sockets.Socket/<>c::<SendAsyncApm>b__23_0(System.IAsyncResult)
extern void U3CU3Ec_U3CSendAsyncApmU3Eb__23_0_m3FCE73D18C4ED344F469F82A9C51E477CA1EA0D6 (void);
// 0x00000944 System.Void System.Net.Sockets.Socket/<>c::<SendAsyncApm>b__23_1(System.IAsyncResult)
extern void U3CU3Ec_U3CSendAsyncApmU3Eb__23_1_mF20106872C29B9D38FA321A7F41C1718EBC14B9C (void);
// 0x00000945 System.Void System.Net.Sockets.Socket/<>c::<SendAsync>b__295_0(System.IOAsyncResult)
extern void U3CU3Ec_U3CSendAsyncU3Eb__295_0_mE1575C8E05206B1D48DC45BEE81D0516DECDF2F9 (void);
// 0x00000946 System.Void System.Net.Sockets.Socket/<>c::<BeginSend>b__297_0(System.IOAsyncResult)
extern void U3CU3Ec_U3CBeginSendU3Eb__297_0_mBB4DD495ABB09A05BC74328D8624152A6C8AF91F (void);
// 0x00000947 System.Void System.Net.Sockets.Socket/<>c::<BeginSendTo>b__310_0(System.IOAsyncResult)
extern void U3CU3Ec_U3CBeginSendToU3Eb__310_0_m4FF95FAECEDAC6A1A4C5DA5C1E04B1F0DC88D855 (void);
// 0x00000948 System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_0(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern void U3CU3Ec_U3C_cctorU3Eb__367_0_m374195043B44F151293B98C58D3C36A426D54285 (void);
// 0x00000949 System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_1(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern void U3CU3Ec_U3C_cctorU3Eb__367_1_mB75869C75ACBA247A3EF6650633CC7101DD531BC (void);
// 0x0000094A System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_2(System.Object,System.Net.Sockets.SocketAsyncEventArgs)
extern void U3CU3Ec_U3C_cctorU3Eb__367_2_m02D7CD9BE2BFEC708C85C3B8A9D67BAA4DD92FD8 (void);
// 0x0000094B System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_3(System.IAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_3_m02CCCD8CCE3176AB8A9DAA6BAA575BDE5F2D57D6 (void);
// 0x0000094C System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_4(System.IOAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_4_m802B9387C24EC09B0909D35D8AE9962F52B6C188 (void);
// 0x0000094D System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_5(System.IOAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_5_m695C3A5FBD0C51D71C6D5E1D1699AC01A91BF528 (void);
// 0x0000094E System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_6(System.IAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_6_mFB25C6CBDC7BA018A88FCECD1FB04117D137F8F6 (void);
// 0x0000094F System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_7(System.IOAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_7_m624ACE294D17E142E9B57F7D3DB735CEDEBEFE7C (void);
// 0x00000950 System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_8(System.IAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_8_m71E6A84E783DE7F080A6652C4151F897EA1E32C0 (void);
// 0x00000951 System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_9(System.IOAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_9_mF99985871CC04F99411C3EE2D1354FF13B0EEDB2 (void);
// 0x00000952 System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_10(System.IAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_10_m6C7764F04456B7F5F6AF4C89020F9179B76C8060 (void);
// 0x00000953 System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_11(System.IOAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_11_mF302618690648FDA9BDAEAD4AA9231336E68386A (void);
// 0x00000954 System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_12(System.IOAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_12_m98E24FD18071B79AC0F0557EC4F838AC497892C0 (void);
// 0x00000955 System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_13(System.IAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_13_m5A14C5CE0B6C00EBC98A0D10C2F670E9F8E5FDC7 (void);
// 0x00000956 System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_14(System.IOAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_14_m2502D94E58E7B239017969E7BDD16DAA9473DB87 (void);
// 0x00000957 System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_15(System.IAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_15_mD30BB7B50BADC326CF0BCD23DCDF15B6791C24CB (void);
// 0x00000958 System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_16(System.IOAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_16_m52CF050F4CEBCFEDD8DF4BB9E89D14D69BC554E2 (void);
// 0x00000959 System.Void System.Net.Sockets.Socket/<>c::<.cctor>b__367_17(System.IAsyncResult)
extern void U3CU3Ec_U3C_cctorU3Eb__367_17_m402C11E460133671C680B46BAE5EF0108245E12B (void);
// 0x0000095A System.Void System.Net.Sockets.Socket/<>c__DisplayClass298_0::.ctor()
extern void U3CU3Ec__DisplayClass298_0__ctor_mC8FFE6C7BDBE2A999CF8792C81BD55DF56BF5004 (void);
// 0x0000095B System.Void System.Net.Sockets.Socket/<>c__DisplayClass298_0::<BeginSendCallback>b__0(System.IOAsyncResult)
extern void U3CU3Ec__DisplayClass298_0_U3CBeginSendCallbackU3Eb__0_m2A2487EF9F99DDE2CA0FD582FC353D04BA4FC843 (void);
// 0x0000095C System.Void System.Net.Sockets.Socket/<>c__DisplayClass311_0::.ctor()
extern void U3CU3Ec__DisplayClass311_0__ctor_m0CC0E00F55AC8EAF6B987A3E1DEE7BB3BF228BE2 (void);
// 0x0000095D System.Void System.Net.Sockets.Socket/<>c__DisplayClass311_0::<BeginSendToCallback>b__0(System.IOAsyncResult)
extern void U3CU3Ec__DisplayClass311_0_U3CBeginSendToCallbackU3Eb__0_mC75A3CEA8BA7D7EF3F6CE003D66DA9AF42B7C1B2 (void);
// 0x0000095E System.Void System.Net.Sockets.Socket/<>c__DisplayClass355_0::.ctor()
extern void U3CU3Ec__DisplayClass355_0__ctor_mA97F35DC1143434E4ED8C3A6BFDD83162EA26BDE (void);
// 0x0000095F System.Void System.Net.Sockets.Socket/<>c__DisplayClass355_0::<QueueIOSelectorJob>b__0(System.Threading.Tasks.Task)
extern void U3CU3Ec__DisplayClass355_0_U3CQueueIOSelectorJobU3Eb__0_m7B1B6117A026B2295671986FBAFF0F01C11F94AB (void);
// 0x00000960 System.Int32 System.Net.Sockets.SocketException::WSAGetLastError_icall()
extern void SocketException_WSAGetLastError_icall_m44B3CDD4C14F19AD07549E0FFF2CB9497B5897BD (void);
// 0x00000961 System.Void System.Net.Sockets.SocketException::.ctor()
extern void SocketException__ctor_mDDA59569C2C05AB013AF9670BD6EC070086239AB (void);
// 0x00000962 System.Void System.Net.Sockets.SocketException::.ctor(System.Int32,System.String)
extern void SocketException__ctor_m0F0444CE9F1E2BF7AF1D0A89DC29FD973AE743AC (void);
// 0x00000963 System.Void System.Net.Sockets.SocketException::.ctor(System.Int32)
extern void SocketException__ctor_mC09AA11941DA13359416179634B151A3FE2B9A64 (void);
// 0x00000964 System.Void System.Net.Sockets.SocketException::.ctor(System.Net.Sockets.SocketError)
extern void SocketException__ctor_m5E9DDE94C8009DE11B8CE2EF1611A5CE8A900AC3 (void);
// 0x00000965 System.Void System.Net.Sockets.SocketException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void SocketException__ctor_m830447309D5813D7FDE050DB3A075BDCE787B046 (void);
// 0x00000966 System.String System.Net.Sockets.SocketException::get_Message()
extern void SocketException_get_Message_mA34986089059D346C42DB47FB8BD331856BE0492 (void);
// 0x00000967 System.Net.Sockets.SocketError System.Net.Sockets.SocketException::get_SocketErrorCode()
extern void SocketException_get_SocketErrorCode_m84FB2D308F046A24A1355975F3BF689C988224C6 (void);
// 0x00000968 System.Boolean System.Net.Sockets.IPPacketInformation::Equals(System.Object)
extern void IPPacketInformation_Equals_mF2E8CBE60B68F20394E51286666D25B54F8EC2B7 (void);
// 0x00000969 System.Int32 System.Net.Sockets.IPPacketInformation::GetHashCode()
extern void IPPacketInformation_GetHashCode_mF1429DDA203ED791CB45EDB87A9006E58BB12A81 (void);
// 0x0000096A System.Void System.Net.Sockets.LingerOption::.ctor(System.Boolean,System.Int32)
extern void LingerOption__ctor_m0BECD7DEA12AC52917D065B27818677F4DE7CEBC (void);
// 0x0000096B System.Void System.Net.Sockets.LingerOption::set_Enabled(System.Boolean)
extern void LingerOption_set_Enabled_mEDF2FED046EA58F9C23BEF3BF1184C3A890419DB (void);
// 0x0000096C System.Void System.Net.Sockets.LingerOption::set_LingerTime(System.Int32)
extern void LingerOption_set_LingerTime_mDC6B06F96C04C5A69E19912EBB37DC5BDE5A6DFB (void);
// 0x0000096D System.Void System.Net.Sockets.TcpClient::.ctor()
extern void TcpClient__ctor_m709FAC4D5B9DDB279F9B97D90D3164B92E1F2C87 (void);
// 0x0000096E System.Void System.Net.Sockets.TcpClient::.ctor(System.Net.Sockets.AddressFamily)
extern void TcpClient__ctor_m94DC92A387808F584010C708C071197B52B48B55 (void);
// 0x0000096F System.Void System.Net.Sockets.TcpClient::.ctor(System.Net.Sockets.Socket)
extern void TcpClient__ctor_m8FDA6AE13D95E071F7952F7DB34C729ABCF7AEE8 (void);
// 0x00000970 System.Net.Sockets.Socket System.Net.Sockets.TcpClient::get_Client()
extern void TcpClient_get_Client_m38B7DCD73EB42C6BBE6340B5E4C0A8432C1140C0 (void);
// 0x00000971 System.Void System.Net.Sockets.TcpClient::set_Client(System.Net.Sockets.Socket)
extern void TcpClient_set_Client_m145E6A35F13B52AF10DE077FD19DE79A622E5F5C (void);
// 0x00000972 System.Boolean System.Net.Sockets.TcpClient::get_Connected()
extern void TcpClient_get_Connected_mAE5B631778361C28C783E417C09F87BA102787B5 (void);
// 0x00000973 System.Void System.Net.Sockets.TcpClient::Connect(System.String,System.Int32)
extern void TcpClient_Connect_mED87D733E342BEAF8DCB947F612FD25C505753BB (void);
// 0x00000974 System.Void System.Net.Sockets.TcpClient::Connect(System.Net.IPEndPoint)
extern void TcpClient_Connect_m45A6127922B8ECB5CFBC879DE6A3BAB7BEC8ABBE (void);
// 0x00000975 System.Net.Sockets.NetworkStream System.Net.Sockets.TcpClient::GetStream()
extern void TcpClient_GetStream_mDD54336B17D1267BD593E0A1EB9EDF3E9506AEBA (void);
// 0x00000976 System.Void System.Net.Sockets.TcpClient::Close()
extern void TcpClient_Close_m03E0ED4E4BA87B3F1ED17585AB1327ED76F5FE89 (void);
// 0x00000977 System.Void System.Net.Sockets.TcpClient::Dispose(System.Boolean)
extern void TcpClient_Dispose_mE2C0688BB1E1E9E25CC9E7CA016AE27547DAEC82 (void);
// 0x00000978 System.Void System.Net.Sockets.TcpClient::Dispose()
extern void TcpClient_Dispose_m533DFCEE37B4299BBB5FACD9A98F8181EC2386D0 (void);
// 0x00000979 System.Void System.Net.Sockets.TcpClient::Finalize()
extern void TcpClient_Finalize_mE6E1D5B6566ABF5AFEF53EF025298EE6F33E7AE2 (void);
// 0x0000097A System.Void System.Net.Sockets.TcpClient::set_ReceiveTimeout(System.Int32)
extern void TcpClient_set_ReceiveTimeout_mF205FBAD90301B326420B66DDA8E7E6AD518B27C (void);
// 0x0000097B System.Void System.Net.Sockets.TcpClient::set_SendTimeout(System.Int32)
extern void TcpClient_set_SendTimeout_m0BF2D0CB3C24116A88097885CDB64971D5AF69CB (void);
// 0x0000097C System.Void System.Net.Sockets.TcpClient::set_NoDelay(System.Boolean)
extern void TcpClient_set_NoDelay_mEE5ACC3E8272661337761B25D06A90670012AC85 (void);
// 0x0000097D System.Void System.Net.Sockets.TcpClient::initialize()
extern void TcpClient_initialize_mE26B7664FC1F1DB4D963065A7E47848798697114 (void);
// 0x0000097E System.Void System.Net.Sockets.TcpListener::.ctor(System.Net.IPAddress,System.Int32)
extern void TcpListener__ctor_m6EDEF45E8F8F2872F3828E801806D9FEC3FF003B (void);
// 0x0000097F System.Net.Sockets.TcpListener System.Net.Sockets.TcpListener::Create(System.Int32)
extern void TcpListener_Create_m7A41A8E5A3DF1791B461D21E5E535D4B5D1678C8 (void);
// 0x00000980 System.Net.Sockets.Socket System.Net.Sockets.TcpListener::get_Server()
extern void TcpListener_get_Server_m949D3B055C39A6716571782E1474BC0CD1F72D26 (void);
// 0x00000981 System.Void System.Net.Sockets.TcpListener::Start()
extern void TcpListener_Start_m919D559B138B311CFFBBE4BF66E326EABD8F8712 (void);
// 0x00000982 System.Void System.Net.Sockets.TcpListener::Start(System.Int32)
extern void TcpListener_Start_mB9AB1161D1570AA1D68F020820D00B108E782C09 (void);
// 0x00000983 System.Void System.Net.Sockets.TcpListener::Stop()
extern void TcpListener_Stop_mBF4B354EB52138AC9A0184F186894EDBAE3BA5FD (void);
// 0x00000984 System.Net.Sockets.TcpClient System.Net.Sockets.TcpListener::AcceptTcpClient()
extern void TcpListener_AcceptTcpClient_mD7DFF1228EAB3F886B5BBC6175A0856C84F2B419 (void);
// 0x00000985 System.Void System.Net.Sockets.UdpClient::.ctor()
extern void UdpClient__ctor_m0F676D3E94A3C7B908DE1FBDF08FEB36894F100E (void);
// 0x00000986 System.Void System.Net.Sockets.UdpClient::.ctor(System.Net.Sockets.AddressFamily)
extern void UdpClient__ctor_mBFC4DFC10BDE68B705E8B56B6ABDA631CBA751F9 (void);
// 0x00000987 System.Void System.Net.Sockets.UdpClient::.ctor(System.Int32)
extern void UdpClient__ctor_m6DCF61B7A404114E51B86974B1B4E028F45C3442 (void);
// 0x00000988 System.Void System.Net.Sockets.UdpClient::.ctor(System.Int32,System.Net.Sockets.AddressFamily)
extern void UdpClient__ctor_mCA910885972A14289F4CF94ACB4040ECCC935D7E (void);
// 0x00000989 System.Net.Sockets.Socket System.Net.Sockets.UdpClient::get_Client()
extern void UdpClient_get_Client_m7338E337D9A97F9050FCAD3A29760AB5CA7AFD9A (void);
// 0x0000098A System.Void System.Net.Sockets.UdpClient::set_Client(System.Net.Sockets.Socket)
extern void UdpClient_set_Client_m539B7DB562DA1B5376A2ACB65C1943E55423C46F (void);
// 0x0000098B System.Void System.Net.Sockets.UdpClient::set_MulticastLoopback(System.Boolean)
extern void UdpClient_set_MulticastLoopback_mA183AA4187FFE98B9F7853D7B3BE837E082D2300 (void);
// 0x0000098C System.Void System.Net.Sockets.UdpClient::set_EnableBroadcast(System.Boolean)
extern void UdpClient_set_EnableBroadcast_m688F1E60EF735B1882A69C9AB606225DC2049188 (void);
// 0x0000098D System.Void System.Net.Sockets.UdpClient::set_ExclusiveAddressUse(System.Boolean)
extern void UdpClient_set_ExclusiveAddressUse_m48228EA37AC52DFDF23DDFB06BA5626F7679A919 (void);
// 0x0000098E System.Void System.Net.Sockets.UdpClient::Close()
extern void UdpClient_Close_m5441019910E37A7F7B8610A46769004E614FF5B6 (void);
// 0x0000098F System.Void System.Net.Sockets.UdpClient::FreeResources()
extern void UdpClient_FreeResources_m5E0D30AF8B963F5277A5116C0E1DCAD5C48D5469 (void);
// 0x00000990 System.Void System.Net.Sockets.UdpClient::Dispose()
extern void UdpClient_Dispose_m12D6B00FE05B89B2049F79E847BCA2D0490DD63E (void);
// 0x00000991 System.Void System.Net.Sockets.UdpClient::Dispose(System.Boolean)
extern void UdpClient_Dispose_m8A81C06984179D26D3D9D08B566B8202606CA595 (void);
// 0x00000992 System.Void System.Net.Sockets.UdpClient::Connect(System.Net.IPEndPoint)
extern void UdpClient_Connect_m44A8225451A8B7DB449D6BB31DCD0C3BD758022D (void);
// 0x00000993 System.Void System.Net.Sockets.UdpClient::CheckForBroadcast(System.Net.IPAddress)
extern void UdpClient_CheckForBroadcast_mB7F54E4B304EF71FF92510F82D0CDFEB35C6D9DC (void);
// 0x00000994 System.Boolean System.Net.Sockets.UdpClient::IsBroadcast(System.Net.IPAddress)
extern void UdpClient_IsBroadcast_mF679BE752DA8794D7E7F994D421575AE060CA538 (void);
// 0x00000995 System.Int32 System.Net.Sockets.UdpClient::Send(System.Byte[],System.Int32,System.Net.IPEndPoint)
extern void UdpClient_Send_m4DCD3993D7ED39C174F8FC8F653DD30F1BDF47B9 (void);
// 0x00000996 System.Int32 System.Net.Sockets.UdpClient::Send(System.Byte[],System.Int32)
extern void UdpClient_Send_mF0E2DCA4FF68290820E807BB5794E938E46280B4 (void);
// 0x00000997 System.IAsyncResult System.Net.Sockets.UdpClient::BeginSend(System.Byte[],System.Int32,System.Net.IPEndPoint,System.AsyncCallback,System.Object)
extern void UdpClient_BeginSend_m64EE2621AE1D1454AD282D733781D628080E52FF (void);
// 0x00000998 System.Int32 System.Net.Sockets.UdpClient::EndSend(System.IAsyncResult)
extern void UdpClient_EndSend_mD7D713085090DEB85E7F116791B62EB6FC0112E2 (void);
// 0x00000999 System.IAsyncResult System.Net.Sockets.UdpClient::BeginReceive(System.AsyncCallback,System.Object)
extern void UdpClient_BeginReceive_m5B450DDB7B13BF24E52CD53FB2DF00CE410CC4E3 (void);
// 0x0000099A System.Byte[] System.Net.Sockets.UdpClient::EndReceive(System.IAsyncResult,System.Net.IPEndPoint&)
extern void UdpClient_EndReceive_m1BAA258139FD339A82F427A6D4049DF4A7A82B39 (void);
// 0x0000099B System.Threading.Tasks.Task`1<System.Int32> System.Net.Sockets.UdpClient::SendAsync(System.Byte[],System.Int32,System.Net.IPEndPoint)
extern void UdpClient_SendAsync_mC7A9D2FEBBE4DC07216DD57C2EFD6605C13262FB (void);
// 0x0000099C System.Threading.Tasks.Task`1<System.Net.Sockets.UdpReceiveResult> System.Net.Sockets.UdpClient::ReceiveAsync()
extern void UdpClient_ReceiveAsync_mA40A2896E4589B93AD9B7D9079E271841E27C5A5 (void);
// 0x0000099D System.Void System.Net.Sockets.UdpClient::createClientSocket()
extern void UdpClient_createClientSocket_mD5B15DF98624D542C138F3993826628B6F8E4C31 (void);
// 0x0000099E System.IAsyncResult System.Net.Sockets.UdpClient::<ReceiveAsync>b__65_0(System.AsyncCallback,System.Object)
extern void UdpClient_U3CReceiveAsyncU3Eb__65_0_mEFDD57EC20634B7617653A2AA2CD802E9B897328 (void);
// 0x0000099F System.Net.Sockets.UdpReceiveResult System.Net.Sockets.UdpClient::<ReceiveAsync>b__65_1(System.IAsyncResult)
extern void UdpClient_U3CReceiveAsyncU3Eb__65_1_mC35EF5D9963D628123BABF74DD90F4CB1DB047E9 (void);
// 0x000009A0 System.Void System.Net.Sockets.UdpReceiveResult::.ctor(System.Byte[],System.Net.IPEndPoint)
extern void UdpReceiveResult__ctor_mC2A34DE9634F5B6F2BF61EAA1D8768419B085F26 (void);
// 0x000009A1 System.Byte[] System.Net.Sockets.UdpReceiveResult::get_Buffer()
extern void UdpReceiveResult_get_Buffer_m7C3092CBE979A308878E5B037BF26FDD29DAB8CC (void);
// 0x000009A2 System.Net.IPEndPoint System.Net.Sockets.UdpReceiveResult::get_RemoteEndPoint()
extern void UdpReceiveResult_get_RemoteEndPoint_m79E46C121B64109D4E12D3168C4712DF9FE945E9 (void);
// 0x000009A3 System.Int32 System.Net.Sockets.UdpReceiveResult::GetHashCode()
extern void UdpReceiveResult_GetHashCode_mC25E5C4B721F0FFD9AC58CE2F3788D404F288EB0 (void);
// 0x000009A4 System.Boolean System.Net.Sockets.UdpReceiveResult::Equals(System.Object)
extern void UdpReceiveResult_Equals_mE7B77E1DF28B1084E214343B9B5B881CDDED95DA (void);
// 0x000009A5 System.Boolean System.Net.Sockets.UdpReceiveResult::Equals(System.Net.Sockets.UdpReceiveResult)
extern void UdpReceiveResult_Equals_mDD344E622B6536C7297AE2429C8FAD2FEEC1132E (void);
// 0x000009A6 System.Void System.Net.Sockets.SafeSocketHandle::.ctor(System.IntPtr,System.Boolean)
extern void SafeSocketHandle__ctor_mA17D19F4D9F5F0D3C29A0C46702E85B9C9D87295 (void);
// 0x000009A7 System.Boolean System.Net.Sockets.SafeSocketHandle::ReleaseHandle()
extern void SafeSocketHandle_ReleaseHandle_m6861A54261EEC98F3DB4BB27251AA7ED6BD4DCE3 (void);
// 0x000009A8 System.Void System.Net.Sockets.SafeSocketHandle::RegisterForBlockingSyscall()
extern void SafeSocketHandle_RegisterForBlockingSyscall_mFCDDEAB42C472D9A8E46B76C0BAECEB7D1832A98 (void);
// 0x000009A9 System.Void System.Net.Sockets.SafeSocketHandle::UnRegisterForBlockingSyscall()
extern void SafeSocketHandle_UnRegisterForBlockingSyscall_m806E8BB959EDBB3B78061C5628A6F2F6EB6800B4 (void);
// 0x000009AA System.Void System.Net.Sockets.SafeSocketHandle::.cctor()
extern void SafeSocketHandle__cctor_m7691BE9F87F473365D4973842102E05614970DE8 (void);
// 0x000009AB System.Net.Sockets.Socket System.Net.Sockets.SocketAsyncEventArgs::get_AcceptSocket()
extern void SocketAsyncEventArgs_get_AcceptSocket_mF12CD5B1067314F3970759A5AE7C94BF1070A4CE (void);
// 0x000009AC System.Void System.Net.Sockets.SocketAsyncEventArgs::set_AcceptSocket(System.Net.Sockets.Socket)
extern void SocketAsyncEventArgs_set_AcceptSocket_m1F265F9F17EE63050B267DFE9B5CA0F3E487B93B (void);
// 0x000009AD System.Int32 System.Net.Sockets.SocketAsyncEventArgs::get_BytesTransferred()
extern void SocketAsyncEventArgs_get_BytesTransferred_m18A255B3F69597D990A7287AAC5310DCDE9B77AE (void);
// 0x000009AE System.Void System.Net.Sockets.SocketAsyncEventArgs::set_BytesTransferred(System.Int32)
extern void SocketAsyncEventArgs_set_BytesTransferred_mAB2EB75BE1BB7B97DBEFCAF3DD42E26BC787D625 (void);
// 0x000009AF System.Void System.Net.Sockets.SocketAsyncEventArgs::set_LastOperation(System.Net.Sockets.SocketAsyncOperation)
extern void SocketAsyncEventArgs_set_LastOperation_m7BA8A0B5CB76B4C095BBB9E1124C3F237B51E26A (void);
// 0x000009B0 System.Void System.Net.Sockets.SocketAsyncEventArgs::set_RemoteEndPoint(System.Net.EndPoint)
extern void SocketAsyncEventArgs_set_RemoteEndPoint_m63E183C33AA5B60D8331A1CEC30A008B6AC7206F (void);
// 0x000009B1 System.Net.Sockets.SocketError System.Net.Sockets.SocketAsyncEventArgs::get_SocketError()
extern void SocketAsyncEventArgs_get_SocketError_m0609040BBEA9BB0C0C6161A7B9A0B5EF50A6346B (void);
// 0x000009B2 System.Void System.Net.Sockets.SocketAsyncEventArgs::set_SocketError(System.Net.Sockets.SocketError)
extern void SocketAsyncEventArgs_set_SocketError_m0E7EC931E1FDA4014BCB6A5E7D5E3E216B80BFB2 (void);
// 0x000009B3 System.Void System.Net.Sockets.SocketAsyncEventArgs::set_SocketFlags(System.Net.Sockets.SocketFlags)
extern void SocketAsyncEventArgs_set_SocketFlags_m0ED0ECFCD2A27C9B5D9AD5F6C38B1146685F1EFA (void);
// 0x000009B4 System.Object System.Net.Sockets.SocketAsyncEventArgs::get_UserToken()
extern void SocketAsyncEventArgs_get_UserToken_m7953E8417ECA92D0AE46615B343136763B0CFB12 (void);
// 0x000009B5 System.Void System.Net.Sockets.SocketAsyncEventArgs::set_UserToken(System.Object)
extern void SocketAsyncEventArgs_set_UserToken_m10B93BD715163BE1A9C813413BD5BD3C4209032D (void);
// 0x000009B6 System.Void System.Net.Sockets.SocketAsyncEventArgs::.ctor(System.Boolean)
extern void SocketAsyncEventArgs__ctor_m14B163B655DD0353E9E022305674A77BF5958D79 (void);
// 0x000009B7 System.Void System.Net.Sockets.SocketAsyncEventArgs::Finalize()
extern void SocketAsyncEventArgs_Finalize_m59E3BE36B99D1B090D58EBA606D6750E040D6D01 (void);
// 0x000009B8 System.Void System.Net.Sockets.SocketAsyncEventArgs::Dispose(System.Boolean)
extern void SocketAsyncEventArgs_Dispose_mA5B8D8ADB8521BC9219061C13CB7C07BD02E16DB (void);
// 0x000009B9 System.Void System.Net.Sockets.SocketAsyncEventArgs::Dispose()
extern void SocketAsyncEventArgs_Dispose_m6DCE1086200FEDE70AFC93B221810F02334FCEA1 (void);
// 0x000009BA System.Void System.Net.Sockets.SocketAsyncEventArgs::SetBytesTransferred(System.Int32)
extern void SocketAsyncEventArgs_SetBytesTransferred_m5ACAA13E707A9D5B8F75FD06DD54AB6F21F60585 (void);
// 0x000009BB System.Net.Sockets.Socket System.Net.Sockets.SocketAsyncEventArgs::get_CurrentSocket()
extern void SocketAsyncEventArgs_get_CurrentSocket_m1FA3256E04EA21BDF5B2ACC9813165D21E614F43 (void);
// 0x000009BC System.Void System.Net.Sockets.SocketAsyncEventArgs::SetCurrentSocket(System.Net.Sockets.Socket)
extern void SocketAsyncEventArgs_SetCurrentSocket_m1A43B5217A35F7EB632A0AF19CAC038378E21026 (void);
// 0x000009BD System.Void System.Net.Sockets.SocketAsyncEventArgs::SetLastOperation(System.Net.Sockets.SocketAsyncOperation)
extern void SocketAsyncEventArgs_SetLastOperation_m73C3FAA754E365C569C956149E6E10E9739DBB17 (void);
// 0x000009BE System.Void System.Net.Sockets.SocketAsyncEventArgs::Complete_internal()
extern void SocketAsyncEventArgs_Complete_internal_m80E7F9C012A9FF94E68C09B05314042FA80549B0 (void);
// 0x000009BF System.Void System.Net.Sockets.SocketAsyncEventArgs::OnCompleted(System.Net.Sockets.SocketAsyncEventArgs)
extern void SocketAsyncEventArgs_OnCompleted_mA73590353C7D619F0A57E5D65C7D6CF1EF41AD6C (void);
// 0x000009C0 System.Memory`1<System.Byte> System.Net.Sockets.SocketAsyncEventArgs::get_MemoryBuffer()
extern void SocketAsyncEventArgs_get_MemoryBuffer_m431CDEB0A4D2496B5A4C44232A72FF9DE95FEE1A (void);
// 0x000009C1 System.Int32 System.Net.Sockets.SocketAsyncEventArgs::get_Offset()
extern void SocketAsyncEventArgs_get_Offset_m0D3ADE0DFB4BACC0FC3C85ED389DC9DCE451EA01 (void);
// 0x000009C2 System.Int32 System.Net.Sockets.SocketAsyncEventArgs::get_Count()
extern void SocketAsyncEventArgs_get_Count_m4B67FDD41086AF476925D19F590DDE95579B5313 (void);
// 0x000009C3 System.Collections.Generic.IList`1<System.ArraySegment`1<System.Byte>> System.Net.Sockets.SocketAsyncEventArgs::get_BufferList()
extern void SocketAsyncEventArgs_get_BufferList_mD9E837C74ED11F81EC7076E532FA5067DCCAB173 (void);
// 0x000009C4 System.Void System.Net.Sockets.SocketAsyncEventArgs::SetBuffer(System.Memory`1<System.Byte>)
extern void SocketAsyncEventArgs_SetBuffer_m442BE1EFE47E011A747645F5A1F31F0702852EF7 (void);
// 0x000009C5 System.IntPtr System.Net.Sockets.SocketAsyncResult::get_Handle()
extern void SocketAsyncResult_get_Handle_mBEAD04497226B5110889462C480B2BBF93AAD430 (void);
// 0x000009C6 System.Void System.Net.Sockets.SocketAsyncResult::.ctor()
extern void SocketAsyncResult__ctor_m2123FFF2C2A56DB840E109348FEEA575B5B691E4 (void);
// 0x000009C7 System.Void System.Net.Sockets.SocketAsyncResult::Init(System.Net.Sockets.Socket,System.AsyncCallback,System.Object,System.Net.Sockets.SocketOperation)
extern void SocketAsyncResult_Init_m4FFCC4198F1133FC01DB80D562B59FB35086EC45 (void);
// 0x000009C8 System.Void System.Net.Sockets.SocketAsyncResult::.ctor(System.Net.Sockets.Socket,System.AsyncCallback,System.Object,System.Net.Sockets.SocketOperation)
extern void SocketAsyncResult__ctor_m3C1FE5E660B13BB4C17D1756B918D9EEC6052F79 (void);
// 0x000009C9 System.Net.Sockets.SocketError System.Net.Sockets.SocketAsyncResult::get_ErrorCode()
extern void SocketAsyncResult_get_ErrorCode_m2CFEB18E19F49775804758830C58B1B068CF936C (void);
// 0x000009CA System.Void System.Net.Sockets.SocketAsyncResult::CheckIfThrowDelayedException()
extern void SocketAsyncResult_CheckIfThrowDelayedException_m25275FD1C77F37FB2CFDE46BDC669CF9B31031C9 (void);
// 0x000009CB System.Void System.Net.Sockets.SocketAsyncResult::CompleteDisposed()
extern void SocketAsyncResult_CompleteDisposed_m41B0FF709D71B1B321871B18BCFAAB3005419BAC (void);
// 0x000009CC System.Void System.Net.Sockets.SocketAsyncResult::Complete()
extern void SocketAsyncResult_Complete_mE0BFD66E710CA1E69D16B0283A5C637BDE354CAA (void);
// 0x000009CD System.Void System.Net.Sockets.SocketAsyncResult::Complete(System.Boolean)
extern void SocketAsyncResult_Complete_m218F8233E7ADB1AFFE69406F6337E09726E640D2 (void);
// 0x000009CE System.Void System.Net.Sockets.SocketAsyncResult::Complete(System.Int32)
extern void SocketAsyncResult_Complete_mC5E61EFDC26D56DCFD98A8600B185043FA285B6E (void);
// 0x000009CF System.Void System.Net.Sockets.SocketAsyncResult::Complete(System.Exception,System.Boolean)
extern void SocketAsyncResult_Complete_m6F201277AC561C7A755617A4963735B70D178B12 (void);
// 0x000009D0 System.Void System.Net.Sockets.SocketAsyncResult::Complete(System.Exception)
extern void SocketAsyncResult_Complete_m49FA96276F9EBB207D8194440AE89300DDDAC513 (void);
// 0x000009D1 System.Void System.Net.Sockets.SocketAsyncResult::Complete(System.Net.Sockets.Socket)
extern void SocketAsyncResult_Complete_mD191B47C619B02AB538B7A00721E689AAFBBD3EA (void);
// 0x000009D2 System.Void System.Net.Sockets.SocketAsyncResult::Complete(System.Net.Sockets.Socket,System.Int32)
extern void SocketAsyncResult_Complete_m85EF6089B3B9ECB2A9737E9C694BCCFFEAE48CF1 (void);
// 0x000009D3 System.Void System.Net.Sockets.SocketAsyncResult/<>c::.cctor()
extern void U3CU3Ec__cctor_m0F0A7E1C51C668515103160DCC4D3F3ED8B7C5C6 (void);
// 0x000009D4 System.Void System.Net.Sockets.SocketAsyncResult/<>c::.ctor()
extern void U3CU3Ec__ctor_mAC11B6F52B2C5BA43E6B14CF8338841CAA0ABF26 (void);
// 0x000009D5 System.Void System.Net.Sockets.SocketAsyncResult/<>c::<Complete>b__27_0(System.Object)
extern void U3CU3Ec_U3CCompleteU3Eb__27_0_m267CAA8BCD77C0263EF3DAC0336FC29A937D2C76 (void);
// 0x000009D6 System.Boolean System.Net.Security.SslClientAuthenticationOptions::get_AllowRenegotiation()
extern void SslClientAuthenticationOptions_get_AllowRenegotiation_m3FAAF852D579A7F688961D77D611E0A096791DA9 (void);
// 0x000009D7 System.String System.Net.Security.SslClientAuthenticationOptions::get_TargetHost()
extern void SslClientAuthenticationOptions_get_TargetHost_mC4CF1905E7B4F8193544D4997E305406B976A797 (void);
// 0x000009D8 System.Void System.Net.Security.SslClientAuthenticationOptions::set_TargetHost(System.String)
extern void SslClientAuthenticationOptions_set_TargetHost_m0D8FF94F267DC51F2BDE6E1C94C2CD67783C4019 (void);
// 0x000009D9 System.Security.Cryptography.X509Certificates.X509CertificateCollection System.Net.Security.SslClientAuthenticationOptions::get_ClientCertificates()
extern void SslClientAuthenticationOptions_get_ClientCertificates_mD6898001726E82ADD9E3083D78BD05DB619B313C (void);
// 0x000009DA System.Void System.Net.Security.SslClientAuthenticationOptions::set_ClientCertificates(System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern void SslClientAuthenticationOptions_set_ClientCertificates_m08B053F1C158912EEA43A4A28D37A740041CA78B (void);
// 0x000009DB System.Void System.Net.Security.SslClientAuthenticationOptions::set_CertificateRevocationCheckMode(System.Security.Cryptography.X509Certificates.X509RevocationMode)
extern void SslClientAuthenticationOptions_set_CertificateRevocationCheckMode_m5DB15346690DAC5A8F610AA8C7DEC7F2DFC29DE9 (void);
// 0x000009DC System.Void System.Net.Security.SslClientAuthenticationOptions::set_EncryptionPolicy(System.Net.Security.EncryptionPolicy)
extern void SslClientAuthenticationOptions_set_EncryptionPolicy_m008E6F28D3C4BFEFFB32A9BEA6AEF1C2B53F1C77 (void);
// 0x000009DD System.Security.Authentication.SslProtocols System.Net.Security.SslClientAuthenticationOptions::get_EnabledSslProtocols()
extern void SslClientAuthenticationOptions_get_EnabledSslProtocols_mEC43F31DCFB3322900792809C72AC48070A9D93B (void);
// 0x000009DE System.Void System.Net.Security.SslClientAuthenticationOptions::set_EnabledSslProtocols(System.Security.Authentication.SslProtocols)
extern void SslClientAuthenticationOptions_set_EnabledSslProtocols_mC0D304F69A5DC05292B87E00A73415BC83A632D5 (void);
// 0x000009DF System.Void System.Net.Security.SslClientAuthenticationOptions::.ctor()
extern void SslClientAuthenticationOptions__ctor_mFFA8C3E39592A8376E7C3F6AC6FB89A6B7A702CD (void);
// 0x000009E0 System.Boolean System.Net.Security.SslServerAuthenticationOptions::get_AllowRenegotiation()
extern void SslServerAuthenticationOptions_get_AllowRenegotiation_mCA00A2853276A68C3B547E6BC0E5B1282EA4CEC2 (void);
// 0x000009E1 System.Boolean System.Net.Security.SslServerAuthenticationOptions::get_ClientCertificateRequired()
extern void SslServerAuthenticationOptions_get_ClientCertificateRequired_mD383FFDB61C59FD5568DFE8B6CDED914AB9977B3 (void);
// 0x000009E2 System.Void System.Net.Security.SslServerAuthenticationOptions::set_ClientCertificateRequired(System.Boolean)
extern void SslServerAuthenticationOptions_set_ClientCertificateRequired_m30FD56EF277CCC34618D4BE98CFC4B541A8AF822 (void);
// 0x000009E3 System.Security.Cryptography.X509Certificates.X509Certificate System.Net.Security.SslServerAuthenticationOptions::get_ServerCertificate()
extern void SslServerAuthenticationOptions_get_ServerCertificate_m7B4C5BBA8FE842496FA13E51D45BE6833671E980 (void);
// 0x000009E4 System.Void System.Net.Security.SslServerAuthenticationOptions::set_ServerCertificate(System.Security.Cryptography.X509Certificates.X509Certificate)
extern void SslServerAuthenticationOptions_set_ServerCertificate_mBA59C3095AB36F693A8DDF611E7EBE5D2AAC51E1 (void);
// 0x000009E5 System.Security.Authentication.SslProtocols System.Net.Security.SslServerAuthenticationOptions::get_EnabledSslProtocols()
extern void SslServerAuthenticationOptions_get_EnabledSslProtocols_m31B8C712D5D3A161E9EC3CE855D7FA44F8E98012 (void);
// 0x000009E6 System.Void System.Net.Security.SslServerAuthenticationOptions::set_EnabledSslProtocols(System.Security.Authentication.SslProtocols)
extern void SslServerAuthenticationOptions_set_EnabledSslProtocols_mC66D59B188A010F1B1726EA34BF61E6DFE5A51A0 (void);
// 0x000009E7 System.Void System.Net.Security.SslServerAuthenticationOptions::set_CertificateRevocationCheckMode(System.Security.Cryptography.X509Certificates.X509RevocationMode)
extern void SslServerAuthenticationOptions_set_CertificateRevocationCheckMode_mCB4596F4D3F547C63E3029612C713F90EF28854C (void);
// 0x000009E8 System.Void System.Net.Security.SslServerAuthenticationOptions::set_EncryptionPolicy(System.Net.Security.EncryptionPolicy)
extern void SslServerAuthenticationOptions_set_EncryptionPolicy_m88D26CB33718B5E589647926F4264CFF5AC0E909 (void);
// 0x000009E9 System.Void System.Net.Security.SslServerAuthenticationOptions::.ctor()
extern void SslServerAuthenticationOptions__ctor_mDF5CCA0826DF6EFA4ECEDC6CB49A10B96D0A7EEF (void);
// 0x000009EA System.Void System.Net.Security.AuthenticatedStream::.ctor(System.IO.Stream,System.Boolean)
extern void AuthenticatedStream__ctor_m8B163146A01B938A3E87E1BED07EE07494A3E357 (void);
// 0x000009EB System.IO.Stream System.Net.Security.AuthenticatedStream::get_InnerStream()
extern void AuthenticatedStream_get_InnerStream_m5BDA4857898A90C696DAD90FD61C536FC3EB10C7 (void);
// 0x000009EC System.Void System.Net.Security.AuthenticatedStream::Dispose(System.Boolean)
extern void AuthenticatedStream_Dispose_m031F600EE876A06427FB0713D992FF02B9C3320F (void);
// 0x000009ED System.Boolean System.Net.Security.AuthenticatedStream::get_IsAuthenticated()
// 0x000009EE System.Void System.Net.Security.LocalCertificateSelectionCallback::.ctor(System.Object,System.IntPtr)
extern void LocalCertificateSelectionCallback__ctor_mE4F3A8BFF483881C2634112BB581CA4B07809805 (void);
// 0x000009EF System.Security.Cryptography.X509Certificates.X509Certificate System.Net.Security.LocalCertificateSelectionCallback::Invoke(System.Object,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern void LocalCertificateSelectionCallback_Invoke_mED43EE6E88B8C653C7D68966F86751B70907591C (void);
// 0x000009F0 System.Void System.Net.Security.RemoteCertificateValidationCallback::.ctor(System.Object,System.IntPtr)
extern void RemoteCertificateValidationCallback__ctor_mC5C0650D4E5D29475BDBDA5B43FEA44DB0DBF367 (void);
// 0x000009F1 System.Boolean System.Net.Security.RemoteCertificateValidationCallback::Invoke(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void RemoteCertificateValidationCallback_Invoke_m5256BF7D3B9BFFFAA959191FD5117C393F670D83 (void);
// 0x000009F2 System.Void System.Net.Security.LocalCertSelectionCallback::.ctor(System.Object,System.IntPtr)
extern void LocalCertSelectionCallback__ctor_mA7B36924E69A918A71649CFF95CB361E33317C38 (void);
// 0x000009F3 System.Security.Cryptography.X509Certificates.X509Certificate System.Net.Security.LocalCertSelectionCallback::Invoke(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern void LocalCertSelectionCallback_Invoke_m3AD956B5D53937117BD3E036068B49615337B5B9 (void);
// 0x000009F4 System.Void System.Net.Security.ServerCertSelectionCallback::.ctor(System.Object,System.IntPtr)
extern void ServerCertSelectionCallback__ctor_m06DDCEDF49D7A5F0F8494E3E960C7DFC64C64946 (void);
// 0x000009F5 System.Security.Cryptography.X509Certificates.X509Certificate System.Net.Security.ServerCertSelectionCallback::Invoke(System.String)
extern void ServerCertSelectionCallback_Invoke_mB6A207E1A8CA76F667A242B68943297741CC6EC1 (void);
// 0x000009F6 Mono.Net.Security.MobileAuthenticatedStream System.Net.Security.SslStream::get_Impl()
extern void SslStream_get_Impl_m1356F55D556B6CBE922FB053F79BC930FCADF057 (void);
// 0x000009F7 System.String System.Net.Security.SslStream::get_InternalTargetHost()
extern void SslStream_get_InternalTargetHost_m9434AC1422D463C25B5C5313AC9BE45B095136AE (void);
// 0x000009F8 Mono.Net.Security.MobileTlsProvider System.Net.Security.SslStream::GetProvider()
extern void SslStream_GetProvider_mD8FA7A9739AD24D3525FAEFF94A2C400ABE7E9F3 (void);
// 0x000009F9 System.Void System.Net.Security.SslStream::.ctor(System.IO.Stream,System.Boolean,System.Net.Security.RemoteCertificateValidationCallback)
extern void SslStream__ctor_m9096E6E85E60C4D205A26FA43D121FECEC735484 (void);
// 0x000009FA System.Void System.Net.Security.SslStream::.ctor(System.IO.Stream,System.Boolean,System.Net.Security.RemoteCertificateValidationCallback,System.Net.Security.LocalCertificateSelectionCallback)
extern void SslStream__ctor_mB4497903594E8A33C14B9A4C4DAB7B16BA2E9B2C (void);
// 0x000009FB System.Void System.Net.Security.SslStream::SetAndVerifyValidationCallback(System.Net.Security.RemoteCertificateValidationCallback)
extern void SslStream_SetAndVerifyValidationCallback_m6FA4BE931FA1C7BC5D175B4E1CA2959742FA9C9D (void);
// 0x000009FC System.Void System.Net.Security.SslStream::SetAndVerifySelectionCallback(System.Net.Security.LocalCertificateSelectionCallback)
extern void SslStream_SetAndVerifySelectionCallback_m8B82CA433CF92AD113F606ACE7229D442587DA0B (void);
// 0x000009FD System.Void System.Net.Security.SslStream::AuthenticateAsClient(System.String)
extern void SslStream_AuthenticateAsClient_mD8CF20D4070562AF8344C7AAF3041E420CA8B7A7 (void);
// 0x000009FE System.Void System.Net.Security.SslStream::AuthenticateAsClient(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Authentication.SslProtocols,System.Boolean)
extern void SslStream_AuthenticateAsClient_mB88736B524C5E2095C51B96634E38E38D759853C (void);
// 0x000009FF System.Void System.Net.Security.SslStream::AuthenticateAsServer(System.Security.Cryptography.X509Certificates.X509Certificate,System.Boolean,System.Security.Authentication.SslProtocols,System.Boolean)
extern void SslStream_AuthenticateAsServer_m5573B24BAE2B574D3AA2023E1070198776919130 (void);
// 0x00000A00 System.Boolean System.Net.Security.SslStream::get_IsAuthenticated()
extern void SslStream_get_IsAuthenticated_m5056C714711944DA54B35F6F2A3025A720B49290 (void);
// 0x00000A01 System.Boolean System.Net.Security.SslStream::get_CanSeek()
extern void SslStream_get_CanSeek_mA98FD41EDB70B03DB44F714CE952E128CA9D9B4B (void);
// 0x00000A02 System.Boolean System.Net.Security.SslStream::get_CanRead()
extern void SslStream_get_CanRead_m0B2ED5711B4D7C796529F56D7039AD546571FEF2 (void);
// 0x00000A03 System.Boolean System.Net.Security.SslStream::get_CanWrite()
extern void SslStream_get_CanWrite_mCBCABFAC940539066F34E4480E52E07CA06DA7CE (void);
// 0x00000A04 System.Int64 System.Net.Security.SslStream::get_Length()
extern void SslStream_get_Length_mD6515D77DBB57912C598761F67C321B6B16E129C (void);
// 0x00000A05 System.Int64 System.Net.Security.SslStream::get_Position()
extern void SslStream_get_Position_mD2DBB36BA22F4620B635557B5748E0928F28B6A2 (void);
// 0x00000A06 System.Void System.Net.Security.SslStream::set_Position(System.Int64)
extern void SslStream_set_Position_m70987A87C7DB346D2FD447EA11E96774105EE5D2 (void);
// 0x00000A07 System.Void System.Net.Security.SslStream::SetLength(System.Int64)
extern void SslStream_SetLength_m3B8BAE2F8203BB823F1E934195F50842A46E5643 (void);
// 0x00000A08 System.Int64 System.Net.Security.SslStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void SslStream_Seek_mFEDBF392998DDDB0F7A71F25D1BB821EA1463B25 (void);
// 0x00000A09 System.Void System.Net.Security.SslStream::Flush()
extern void SslStream_Flush_m61B328FCFEB939DE23740823CE3EB7E8DF3B14A4 (void);
// 0x00000A0A System.Void System.Net.Security.SslStream::CheckDisposed()
extern void SslStream_CheckDisposed_m884E18F119B366EE6734CCFA126A1F633687FA95 (void);
// 0x00000A0B System.Void System.Net.Security.SslStream::Dispose(System.Boolean)
extern void SslStream_Dispose_m187B76F54E2471469F5B62B8DDB33E11E40EE397 (void);
// 0x00000A0C System.Int32 System.Net.Security.SslStream::Read(System.Byte[],System.Int32,System.Int32)
extern void SslStream_Read_mEAFC2AA192D9EAF401E2B067395EA08EDC5FB47E (void);
// 0x00000A0D System.Void System.Net.Security.SslStream::Write(System.Byte[],System.Int32,System.Int32)
extern void SslStream_Write_mCF650FDF725E1AAEDC462A016A38E535F7210F3C (void);
// 0x00000A0E System.Threading.Tasks.Task`1<System.Int32> System.Net.Security.SslStream::ReadAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void SslStream_ReadAsync_m0F251A72A8D1C8C41A49CC99AE55D8D0F96A956C (void);
// 0x00000A0F System.Threading.Tasks.Task System.Net.Security.SslStream::WriteAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void SslStream_WriteAsync_m8211FC8F3FF4552E897516BE6CD70923848B89C3 (void);
// 0x00000A10 System.IAsyncResult System.Net.Security.SslStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void SslStream_BeginRead_m716BC242891C530ECB08DA4989AE27EEBAC71C98 (void);
// 0x00000A11 System.Int32 System.Net.Security.SslStream::EndRead(System.IAsyncResult)
extern void SslStream_EndRead_m881FE67FB5376508CBB50A96ED65BC672FB3A7A0 (void);
// 0x00000A12 System.IAsyncResult System.Net.Security.SslStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void SslStream_BeginWrite_m66F3B2CDAAE0E1FD662552A8D1ECAC889F2AD2D0 (void);
// 0x00000A13 System.Void System.Net.Security.SslStream::EndWrite(System.IAsyncResult)
extern void SslStream_EndWrite_m78C915054198C80ECEE67CBB07D782E10686BA61 (void);
// 0x00000A14 System.Void System.Net.Security.SslStream/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mBE4019B5D6D20FC100DAF52B1172ACF497046370 (void);
// 0x00000A15 System.Security.Cryptography.X509Certificates.X509Certificate System.Net.Security.SslStream/<>c__DisplayClass21_0::<SetAndVerifySelectionCallback>b__0(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern void U3CU3Ec__DisplayClass21_0_U3CSetAndVerifySelectionCallbackU3Eb__0_mADCFBC85C66A800C57CD2CECA4CDB6A9E5BB020D (void);
// 0x00000A16 System.Void System.ComponentModel.EditorBrowsableAttribute::.ctor(System.ComponentModel.EditorBrowsableState)
extern void EditorBrowsableAttribute__ctor_mE6105AD9666A4DF03DB2590C687EAC6B12D908CE (void);
// 0x00000A17 System.Boolean System.ComponentModel.EditorBrowsableAttribute::Equals(System.Object)
extern void EditorBrowsableAttribute_Equals_m2DFA6ADDE69D95657A8257840C01FCAF74A6B02F (void);
// 0x00000A18 System.Int32 System.ComponentModel.EditorBrowsableAttribute::GetHashCode()
extern void EditorBrowsableAttribute_GetHashCode_m1AE50FA45B338D71189AA3C249DB56406DEC798C (void);
// 0x00000A19 System.Void System.ComponentModel.BrowsableAttribute::.ctor(System.Boolean)
extern void BrowsableAttribute__ctor_m1828B5114921E3FF83082211F2FBE82517559035 (void);
// 0x00000A1A System.Boolean System.ComponentModel.BrowsableAttribute::get_Browsable()
extern void BrowsableAttribute_get_Browsable_m23594845C49373D8D6AB753D89DB39D8285250F0 (void);
// 0x00000A1B System.Boolean System.ComponentModel.BrowsableAttribute::Equals(System.Object)
extern void BrowsableAttribute_Equals_m8EF756EC88F170513772F14FC08BDAD1EC2B863D (void);
// 0x00000A1C System.Int32 System.ComponentModel.BrowsableAttribute::GetHashCode()
extern void BrowsableAttribute_GetHashCode_m40CEC719AE0A79F4339009D2E473342E59BAEDD1 (void);
// 0x00000A1D System.Void System.ComponentModel.BrowsableAttribute::.cctor()
extern void BrowsableAttribute__cctor_mAA4C6BF30D72D9DB9B0BE59F46140D4E284E15B2 (void);
// 0x00000A1E System.Object System.ComponentModel.ArrayConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern void ArrayConverter_ConvertTo_mC144B3093D2037D3BB5B109D15725ABCC4E985D9 (void);
// 0x00000A1F System.ComponentModel.PropertyDescriptorCollection System.ComponentModel.ArrayConverter::GetProperties(System.ComponentModel.ITypeDescriptorContext,System.Object,System.Attribute[])
extern void ArrayConverter_GetProperties_mC4FE7B6803C8B34BD45ED4DB67B0D3692AF60F55 (void);
// 0x00000A20 System.Boolean System.ComponentModel.ArrayConverter::GetPropertiesSupported(System.ComponentModel.ITypeDescriptorContext)
extern void ArrayConverter_GetPropertiesSupported_m00411C896EB1E0B5155BB701D72A7F690C86C6F8 (void);
// 0x00000A21 System.Void System.ComponentModel.ArrayConverter::.ctor()
extern void ArrayConverter__ctor_mAA6B57C16F167FF104B8E23402DE87ACA5E13A2B (void);
// 0x00000A22 System.Void System.ComponentModel.ArrayConverter/ArrayPropertyDescriptor::.ctor(System.Type,System.Type,System.Int32)
extern void ArrayPropertyDescriptor__ctor_mC12E0FB619504B32AC30C714F3E51B0C3BA813AB (void);
// 0x00000A23 System.Void System.ComponentModel.AttributeCollection::.ctor(System.Attribute[])
extern void AttributeCollection__ctor_m6987DEDDE616B905793F96BC20866FDEC937B313 (void);
// 0x00000A24 System.Attribute[] System.ComponentModel.AttributeCollection::get_Attributes()
extern void AttributeCollection_get_Attributes_m698416274F06ACDAA3A55972D029340253DAD0F6 (void);
// 0x00000A25 System.Int32 System.ComponentModel.AttributeCollection::get_Count()
extern void AttributeCollection_get_Count_mBD37E9F785A4BD5426A67D42F0FA01FC0E2BEA02 (void);
// 0x00000A26 System.Collections.IEnumerator System.ComponentModel.AttributeCollection::GetEnumerator()
extern void AttributeCollection_GetEnumerator_m1464C016A16216F1F87A46940CF97B742AA032C6 (void);
// 0x00000A27 System.Object System.ComponentModel.AttributeCollection::System.Collections.ICollection.get_SyncRoot()
extern void AttributeCollection_System_Collections_ICollection_get_SyncRoot_mE4F31AFB7E65BD84B5D64120F0C68F72753CD409 (void);
// 0x00000A28 System.Int32 System.ComponentModel.AttributeCollection::System.Collections.ICollection.get_Count()
extern void AttributeCollection_System_Collections_ICollection_get_Count_mFBE2F16F7901E3F85F0C4B37032E83E99BDD2300 (void);
// 0x00000A29 System.Collections.IEnumerator System.ComponentModel.AttributeCollection::System.Collections.IEnumerable.GetEnumerator()
extern void AttributeCollection_System_Collections_IEnumerable_GetEnumerator_m5C57499D1B27935C97A88E9B44D571CAA87D93A4 (void);
// 0x00000A2A System.Void System.ComponentModel.AttributeCollection::CopyTo(System.Array,System.Int32)
extern void AttributeCollection_CopyTo_m29A1B7F330A44A3ABD7402CF9863089B917AA7F6 (void);
// 0x00000A2B System.Void System.ComponentModel.AttributeCollection::.cctor()
extern void AttributeCollection__cctor_mCFAD6C1E6356E3CA0CDDA680BDB66D72B0161DF8 (void);
// 0x00000A2C System.Void System.ComponentModel.BaseNumberConverter::.ctor()
extern void BaseNumberConverter__ctor_m697E7E5DB315CE6CDA318F256A634BAEF3C7C76F (void);
// 0x00000A2D System.Boolean System.ComponentModel.BaseNumberConverter::get_AllowHex()
extern void BaseNumberConverter_get_AllowHex_m2A6143CE655C657EDFE8A7DC3897EEA0EEB3A4BD (void);
// 0x00000A2E System.Type System.ComponentModel.BaseNumberConverter::get_TargetType()
// 0x00000A2F System.Object System.ComponentModel.BaseNumberConverter::FromString(System.String,System.Int32)
// 0x00000A30 System.Object System.ComponentModel.BaseNumberConverter::FromString(System.String,System.Globalization.NumberFormatInfo)
// 0x00000A31 System.String System.ComponentModel.BaseNumberConverter::ToString(System.Object,System.Globalization.NumberFormatInfo)
// 0x00000A32 System.Boolean System.ComponentModel.BaseNumberConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void BaseNumberConverter_CanConvertFrom_mA4351F586FC63F67CC1E0013CD68BCC2F89FB916 (void);
// 0x00000A33 System.Object System.ComponentModel.BaseNumberConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern void BaseNumberConverter_ConvertFrom_mAC29686169A55736CD527CB44D3E8DB0D697C7DB (void);
// 0x00000A34 System.Object System.ComponentModel.BaseNumberConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern void BaseNumberConverter_ConvertTo_m46174D69B2F224A17A25C88B162C0270DE801CF2 (void);
// 0x00000A35 System.Boolean System.ComponentModel.BaseNumberConverter::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void BaseNumberConverter_CanConvertTo_m6108C34CF9DF475BF4074181348848BCE6C53F76 (void);
// 0x00000A36 System.Boolean System.ComponentModel.BooleanConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void BooleanConverter_CanConvertFrom_mD1A1ADBAB30E4ED54C20554B62F6E4C38369E6CE (void);
// 0x00000A37 System.Object System.ComponentModel.BooleanConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern void BooleanConverter_ConvertFrom_m828B810B45C660FD12088B64754C92984D043092 (void);
// 0x00000A38 System.ComponentModel.TypeConverter/StandardValuesCollection System.ComponentModel.BooleanConverter::GetStandardValues(System.ComponentModel.ITypeDescriptorContext)
extern void BooleanConverter_GetStandardValues_m748173E319A659C8D26EF251DE71C75C83ED2E0C (void);
// 0x00000A39 System.Boolean System.ComponentModel.BooleanConverter::GetStandardValuesExclusive(System.ComponentModel.ITypeDescriptorContext)
extern void BooleanConverter_GetStandardValuesExclusive_m0C6DCBFBE8CB8099BD7B6CED00D30BCAF375E2BD (void);
// 0x00000A3A System.Boolean System.ComponentModel.BooleanConverter::GetStandardValuesSupported(System.ComponentModel.ITypeDescriptorContext)
extern void BooleanConverter_GetStandardValuesSupported_mDEE5DB63C7EE136CF18FED6BE1DCB90749603D91 (void);
// 0x00000A3B System.Void System.ComponentModel.BooleanConverter::.ctor()
extern void BooleanConverter__ctor_m50990A1CB60D3557199985EF164FF1E8AFCADC87 (void);
// 0x00000A3C System.Boolean System.ComponentModel.DecimalConverter::get_AllowHex()
extern void DecimalConverter_get_AllowHex_m2B657E4B11F10E8F6F3E27F8F24EBD407E68386D (void);
// 0x00000A3D System.Type System.ComponentModel.DecimalConverter::get_TargetType()
extern void DecimalConverter_get_TargetType_m713B317E87EFCE4C62D9252D5C995FCEF1DAEF47 (void);
// 0x00000A3E System.Boolean System.ComponentModel.DecimalConverter::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void DecimalConverter_CanConvertTo_m2C1D96863DB726FCB8566B7B78899A0DD5D85CC4 (void);
// 0x00000A3F System.Object System.ComponentModel.DecimalConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern void DecimalConverter_ConvertTo_m0F245DFB5E97D39B818A6D3A87C6FAE07F57F6BC (void);
// 0x00000A40 System.Object System.ComponentModel.DecimalConverter::FromString(System.String,System.Int32)
extern void DecimalConverter_FromString_m65A0F7D7BFD4A568C416732121878DDA2CDB2423 (void);
// 0x00000A41 System.Object System.ComponentModel.DecimalConverter::FromString(System.String,System.Globalization.NumberFormatInfo)
extern void DecimalConverter_FromString_m4F6FAD944464E3B239B445BE45950CF15E423BFC (void);
// 0x00000A42 System.String System.ComponentModel.DecimalConverter::ToString(System.Object,System.Globalization.NumberFormatInfo)
extern void DecimalConverter_ToString_mA2278E45267D367F8FED73BDE4293021E493499F (void);
// 0x00000A43 System.Void System.ComponentModel.DecimalConverter::.ctor()
extern void DecimalConverter__ctor_mC1E815EF17DE8DBD91FB472C83A4F38514704B46 (void);
// 0x00000A44 System.Void System.ComponentModel.DelegatingTypeDescriptionProvider::.ctor(System.Type)
extern void DelegatingTypeDescriptionProvider__ctor_mF4BAC468855383A4FA4CBFA329DF02F35805ADC5 (void);
// 0x00000A45 System.ComponentModel.TypeDescriptionProvider System.ComponentModel.DelegatingTypeDescriptionProvider::get_Provider()
extern void DelegatingTypeDescriptionProvider_get_Provider_m39F8D32C2826EC53330210A55058A76B06136E74 (void);
// 0x00000A46 System.Type System.ComponentModel.DelegatingTypeDescriptionProvider::GetReflectionType(System.Type,System.Object)
extern void DelegatingTypeDescriptionProvider_GetReflectionType_m130F511F42060A53340620EB0667237EB0F94024 (void);
// 0x00000A47 System.Boolean System.ComponentModel.DoubleConverter::get_AllowHex()
extern void DoubleConverter_get_AllowHex_m7B24D1C1979E1B4B0F32D455B7BADE06AB50DBBB (void);
// 0x00000A48 System.Type System.ComponentModel.DoubleConverter::get_TargetType()
extern void DoubleConverter_get_TargetType_m32CBA966154111137DA7AE087217F3A7BAFECA88 (void);
// 0x00000A49 System.Object System.ComponentModel.DoubleConverter::FromString(System.String,System.Int32)
extern void DoubleConverter_FromString_m205541D548D67CE0C9493E7CC532133BD5B27DC0 (void);
// 0x00000A4A System.Object System.ComponentModel.DoubleConverter::FromString(System.String,System.Globalization.NumberFormatInfo)
extern void DoubleConverter_FromString_m7E7B0719E5299B5DEF5806531A51807A6860050C (void);
// 0x00000A4B System.String System.ComponentModel.DoubleConverter::ToString(System.Object,System.Globalization.NumberFormatInfo)
extern void DoubleConverter_ToString_m9BDB00A75E71F1001A1F555AD9E786C32CF668C3 (void);
// 0x00000A4C System.Void System.ComponentModel.DoubleConverter::.ctor()
extern void DoubleConverter__ctor_m3CA5201BA68AB510BB2112A6755FCFEAB6DE53BA (void);
// 0x00000A4D System.Void System.ComponentModel.EventDescriptorCollection::.ctor(System.ComponentModel.EventDescriptor[])
extern void EventDescriptorCollection__ctor_m5B672594D0220F89AD3DFE0FBCDB34FED5B56CF3 (void);
// 0x00000A4E System.Void System.ComponentModel.EventDescriptorCollection::.ctor(System.ComponentModel.EventDescriptor[],System.Boolean)
extern void EventDescriptorCollection__ctor_m6FEF5B613838F164D7F9FC939EA915FE7B5CF118 (void);
// 0x00000A4F System.Int32 System.ComponentModel.EventDescriptorCollection::get_Count()
extern void EventDescriptorCollection_get_Count_mCBBE573DF8EC096E14170A5039E90107943E5A87 (void);
// 0x00000A50 System.Void System.ComponentModel.EventDescriptorCollection::set_Count(System.Int32)
extern void EventDescriptorCollection_set_Count_mD825B07B7C93EC3A511CD0DE95EFB9F490E146E8 (void);
// 0x00000A51 System.ComponentModel.EventDescriptor System.ComponentModel.EventDescriptorCollection::get_Item(System.Int32)
extern void EventDescriptorCollection_get_Item_mA010043B2DFD4EE474361E4485B7727354BB00ED (void);
// 0x00000A52 System.Int32 System.ComponentModel.EventDescriptorCollection::Add(System.ComponentModel.EventDescriptor)
extern void EventDescriptorCollection_Add_mAA14F6928C91D2A6B81E69723C35E7F43FBA62CC (void);
// 0x00000A53 System.Void System.ComponentModel.EventDescriptorCollection::Clear()
extern void EventDescriptorCollection_Clear_m34F6F48BAA557FBB783F24C33EB2E26002A57C56 (void);
// 0x00000A54 System.Boolean System.ComponentModel.EventDescriptorCollection::Contains(System.ComponentModel.EventDescriptor)
extern void EventDescriptorCollection_Contains_m50DB06B4BB92F9F108E41B0B39C71C687A426F12 (void);
// 0x00000A55 System.Void System.ComponentModel.EventDescriptorCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern void EventDescriptorCollection_System_Collections_ICollection_CopyTo_mFAE7230CA92EB811EA7AC85B9EF758477D9CB58D (void);
// 0x00000A56 System.Void System.ComponentModel.EventDescriptorCollection::EnsureEventsOwned()
extern void EventDescriptorCollection_EnsureEventsOwned_m84EEB041AFCAC7DC00D846999338F998A3B873B4 (void);
// 0x00000A57 System.Void System.ComponentModel.EventDescriptorCollection::EnsureSize(System.Int32)
extern void EventDescriptorCollection_EnsureSize_m9A0437DD4221566FFE953D7D8A9B15CEC82D8F1B (void);
// 0x00000A58 System.Int32 System.ComponentModel.EventDescriptorCollection::IndexOf(System.ComponentModel.EventDescriptor)
extern void EventDescriptorCollection_IndexOf_mFB443D00688BEE960D0AB1EFCF41FD41863D105D (void);
// 0x00000A59 System.Void System.ComponentModel.EventDescriptorCollection::Insert(System.Int32,System.ComponentModel.EventDescriptor)
extern void EventDescriptorCollection_Insert_mC472AA88B6CC60161C15A8B464D4D1A827C45CA7 (void);
// 0x00000A5A System.Void System.ComponentModel.EventDescriptorCollection::Remove(System.ComponentModel.EventDescriptor)
extern void EventDescriptorCollection_Remove_mC7EDCADC4A6630ABA8C38C7F1CE424C622A73A67 (void);
// 0x00000A5B System.Void System.ComponentModel.EventDescriptorCollection::RemoveAt(System.Int32)
extern void EventDescriptorCollection_RemoveAt_m1D8C3C214CDF147C85410AB0F956EA1732261900 (void);
// 0x00000A5C System.Collections.IEnumerator System.ComponentModel.EventDescriptorCollection::GetEnumerator()
extern void EventDescriptorCollection_GetEnumerator_mBBA565367CD4D16306527D637EA37D2BA6949478 (void);
// 0x00000A5D System.Void System.ComponentModel.EventDescriptorCollection::InternalSort(System.String[])
extern void EventDescriptorCollection_InternalSort_mFDAC78BDC8DF8D728C05BF58B24C6B3AAE83E1C9 (void);
// 0x00000A5E System.Void System.ComponentModel.EventDescriptorCollection::InternalSort(System.Collections.IComparer)
extern void EventDescriptorCollection_InternalSort_mC173B2DDBF4F976EC4B3E9A2AC03FFD0D3F115AD (void);
// 0x00000A5F System.Object System.ComponentModel.EventDescriptorCollection::System.Collections.ICollection.get_SyncRoot()
extern void EventDescriptorCollection_System_Collections_ICollection_get_SyncRoot_m929985FDBE786C0523515318A463F1DB1F0EBAA5 (void);
// 0x00000A60 System.Int32 System.ComponentModel.EventDescriptorCollection::System.Collections.ICollection.get_Count()
extern void EventDescriptorCollection_System_Collections_ICollection_get_Count_mF9D4EB7237921F1FDF76F78307CB4C5AE29C5D74 (void);
// 0x00000A61 System.Collections.IEnumerator System.ComponentModel.EventDescriptorCollection::System.Collections.IEnumerable.GetEnumerator()
extern void EventDescriptorCollection_System_Collections_IEnumerable_GetEnumerator_mC73C30221041A865D2AA69E9E57D5D50A89E756E (void);
// 0x00000A62 System.Object System.ComponentModel.EventDescriptorCollection::System.Collections.IList.get_Item(System.Int32)
extern void EventDescriptorCollection_System_Collections_IList_get_Item_m816D7A606BEA8FDB4B8F959E0727A591139AB75E (void);
// 0x00000A63 System.Void System.ComponentModel.EventDescriptorCollection::System.Collections.IList.set_Item(System.Int32,System.Object)
extern void EventDescriptorCollection_System_Collections_IList_set_Item_mEEC2A36C2AD376E350334887290721ECF319F17F (void);
// 0x00000A64 System.Int32 System.ComponentModel.EventDescriptorCollection::System.Collections.IList.Add(System.Object)
extern void EventDescriptorCollection_System_Collections_IList_Add_mC80A1930B52051837C3B553FED53E0A3624609D2 (void);
// 0x00000A65 System.Boolean System.ComponentModel.EventDescriptorCollection::System.Collections.IList.Contains(System.Object)
extern void EventDescriptorCollection_System_Collections_IList_Contains_m7CCEEC883D1D814BAA11C20A2AF4B2ECB25CB670 (void);
// 0x00000A66 System.Void System.ComponentModel.EventDescriptorCollection::System.Collections.IList.Clear()
extern void EventDescriptorCollection_System_Collections_IList_Clear_m5F4F6DC457DC4AC15BF1ECDEB05C9AC37A8EDFAA (void);
// 0x00000A67 System.Int32 System.ComponentModel.EventDescriptorCollection::System.Collections.IList.IndexOf(System.Object)
extern void EventDescriptorCollection_System_Collections_IList_IndexOf_mF92645F7291545162437FAC95787100B4844B4E4 (void);
// 0x00000A68 System.Void System.ComponentModel.EventDescriptorCollection::System.Collections.IList.Insert(System.Int32,System.Object)
extern void EventDescriptorCollection_System_Collections_IList_Insert_mC6AD4196A4F520BB4C0B5866D3D6307A9639A58E (void);
// 0x00000A69 System.Void System.ComponentModel.EventDescriptorCollection::System.Collections.IList.Remove(System.Object)
extern void EventDescriptorCollection_System_Collections_IList_Remove_m04E2ACE9E3B90350007626A227EA9626F133589C (void);
// 0x00000A6A System.Void System.ComponentModel.EventDescriptorCollection::System.Collections.IList.RemoveAt(System.Int32)
extern void EventDescriptorCollection_System_Collections_IList_RemoveAt_m67FB853573FD34FEB23F0763434FBBB1FF8D3389 (void);
// 0x00000A6B System.Boolean System.ComponentModel.EventDescriptorCollection::System.Collections.IList.get_IsReadOnly()
extern void EventDescriptorCollection_System_Collections_IList_get_IsReadOnly_m14ED5EDE6AAD26BC174F00C4F48A16C0874B6160 (void);
// 0x00000A6C System.Boolean System.ComponentModel.EventDescriptorCollection::System.Collections.IList.get_IsFixedSize()
extern void EventDescriptorCollection_System_Collections_IList_get_IsFixedSize_mE80CCD9C034D17CB7E7780BF1FFF1FC92123EA62 (void);
// 0x00000A6D System.Void System.ComponentModel.EventDescriptorCollection::.cctor()
extern void EventDescriptorCollection__cctor_mEB5345968B743898C4EE2AFED371C8C3A7816C80 (void);
// 0x00000A6E System.Void System.ComponentModel.EventDescriptorCollection/ArraySubsetEnumerator::.ctor(System.Array,System.Int32)
extern void ArraySubsetEnumerator__ctor_m76C2057A80D4E3DCEA8B97DD0EF9456AE5D6EA92 (void);
// 0x00000A6F System.Boolean System.ComponentModel.EventDescriptorCollection/ArraySubsetEnumerator::MoveNext()
extern void ArraySubsetEnumerator_MoveNext_m8AD8B4514C227FAFDF85FE66EE18F51EF8E00497 (void);
// 0x00000A70 System.Void System.ComponentModel.EventDescriptorCollection/ArraySubsetEnumerator::Reset()
extern void ArraySubsetEnumerator_Reset_m49967EC2F723F5F2EABD5E780E51B0AE912FCD77 (void);
// 0x00000A71 System.Object System.ComponentModel.EventDescriptorCollection/ArraySubsetEnumerator::get_Current()
extern void ArraySubsetEnumerator_get_Current_mE137EF1ADECA45633866E8723C50E4A07FFA478B (void);
// 0x00000A72 System.Type System.ComponentModel.Int16Converter::get_TargetType()
extern void Int16Converter_get_TargetType_mB268E3FEA2B5BDEBBC628D21A07686856FC451C8 (void);
// 0x00000A73 System.Object System.ComponentModel.Int16Converter::FromString(System.String,System.Int32)
extern void Int16Converter_FromString_mACA7B00848D2EC76B985408DE0D92D84F462D14C (void);
// 0x00000A74 System.Object System.ComponentModel.Int16Converter::FromString(System.String,System.Globalization.NumberFormatInfo)
extern void Int16Converter_FromString_m57535287425AA32CBB51833859D5224F030EC2F3 (void);
// 0x00000A75 System.String System.ComponentModel.Int16Converter::ToString(System.Object,System.Globalization.NumberFormatInfo)
extern void Int16Converter_ToString_mB9DD75EE872A4040BFFC794C4400FBC857A75CCF (void);
// 0x00000A76 System.Void System.ComponentModel.Int16Converter::.ctor()
extern void Int16Converter__ctor_m3803A7772B21F49200FC142C193A40DC286C9DFF (void);
// 0x00000A77 System.Type System.ComponentModel.Int32Converter::get_TargetType()
extern void Int32Converter_get_TargetType_m837B83368B96CE1CDFCF450F7DA3629ECC86DC1A (void);
// 0x00000A78 System.Object System.ComponentModel.Int32Converter::FromString(System.String,System.Int32)
extern void Int32Converter_FromString_m3917F97D4401800516F6F44363CD154F9645AC72 (void);
// 0x00000A79 System.Object System.ComponentModel.Int32Converter::FromString(System.String,System.Globalization.NumberFormatInfo)
extern void Int32Converter_FromString_m54931A645C6C1B4DB770894D5A23F24A03E9B340 (void);
// 0x00000A7A System.String System.ComponentModel.Int32Converter::ToString(System.Object,System.Globalization.NumberFormatInfo)
extern void Int32Converter_ToString_m8B5D811FCF0FBFADCAE25E354FD27FE6256AA36D (void);
// 0x00000A7B System.Void System.ComponentModel.Int32Converter::.ctor()
extern void Int32Converter__ctor_m88220DA12F6ED6FE4D1232B73C20DECE9EFC63FF (void);
// 0x00000A7C System.Type System.ComponentModel.Int64Converter::get_TargetType()
extern void Int64Converter_get_TargetType_m8279327D6F373BEE6E4BF0439215D6F5DF6B0097 (void);
// 0x00000A7D System.Object System.ComponentModel.Int64Converter::FromString(System.String,System.Int32)
extern void Int64Converter_FromString_m4F1E8450F4A60B29297D4AA3D8FE196B09D98102 (void);
// 0x00000A7E System.Object System.ComponentModel.Int64Converter::FromString(System.String,System.Globalization.NumberFormatInfo)
extern void Int64Converter_FromString_m324A7ABADC1B991B6AAB4B600C0A676B289BDF72 (void);
// 0x00000A7F System.String System.ComponentModel.Int64Converter::ToString(System.Object,System.Globalization.NumberFormatInfo)
extern void Int64Converter_ToString_mC4F1CE4731349A078460E190F587DB3276B54B3E (void);
// 0x00000A80 System.Void System.ComponentModel.Int64Converter::.ctor()
extern void Int64Converter__ctor_m3C124DB065F27F6BEAB7ABAF4B3D13E6A9F3E94C (void);
// 0x00000A81 System.Void System.ComponentModel.PropertyDescriptor::.ctor(System.String,System.Attribute[])
extern void PropertyDescriptor__ctor_m20223572362A331B6F0D436BC19AFE2EE87558B8 (void);
// 0x00000A82 System.Type System.ComponentModel.PropertyDescriptor::get_PropertyType()
// 0x00000A83 System.Boolean System.ComponentModel.PropertyDescriptor::Equals(System.Object)
extern void PropertyDescriptor_Equals_m69C3B1D9E4AF806536B7545DAB5FE72883A512F6 (void);
// 0x00000A84 System.Void System.ComponentModel.PropertyDescriptor::FillAttributes(System.Collections.IList)
extern void PropertyDescriptor_FillAttributes_m089944713C5E19B880AC940B07C609BFC3D273D1 (void);
// 0x00000A85 System.Int32 System.ComponentModel.PropertyDescriptor::GetHashCode()
extern void PropertyDescriptor_GetHashCode_m8470877D9BFF977617586AC7E2B900D01B3B4E3F (void);
// 0x00000A86 System.Void System.ComponentModel.PropertyDescriptorCollection::.ctor(System.ComponentModel.PropertyDescriptor[])
extern void PropertyDescriptorCollection__ctor_m3D1AC0D2B3187A895AE8B64AC891251860D8C361 (void);
// 0x00000A87 System.Void System.ComponentModel.PropertyDescriptorCollection::.ctor(System.ComponentModel.PropertyDescriptor[],System.Boolean)
extern void PropertyDescriptorCollection__ctor_mAD196DBB6D71AA12A41715798F3DFACFDA170BAE (void);
// 0x00000A88 System.Void System.ComponentModel.PropertyDescriptorCollection::.ctor(System.ComponentModel.PropertyDescriptor[],System.Int32,System.String[],System.Collections.IComparer)
extern void PropertyDescriptorCollection__ctor_m3452043C947348EB364E043013AB2DB4165B72D7 (void);
// 0x00000A89 System.Int32 System.ComponentModel.PropertyDescriptorCollection::get_Count()
extern void PropertyDescriptorCollection_get_Count_m9121E7236AF94FE7AD4BB3C48ADBBB2A8ACF3508 (void);
// 0x00000A8A System.Void System.ComponentModel.PropertyDescriptorCollection::set_Count(System.Int32)
extern void PropertyDescriptorCollection_set_Count_m035F2DDDDD3FEFC8EF701C3A584CA6BCDAB6D854 (void);
// 0x00000A8B System.ComponentModel.PropertyDescriptor System.ComponentModel.PropertyDescriptorCollection::get_Item(System.Int32)
extern void PropertyDescriptorCollection_get_Item_m5BD52F4CCBEF1D1D9B33887BB373181AD11A4C25 (void);
// 0x00000A8C System.ComponentModel.PropertyDescriptor System.ComponentModel.PropertyDescriptorCollection::get_Item(System.String)
extern void PropertyDescriptorCollection_get_Item_mD34F172015A4AAC759099193BBBCDB1489D51E09 (void);
// 0x00000A8D System.Int32 System.ComponentModel.PropertyDescriptorCollection::Add(System.ComponentModel.PropertyDescriptor)
extern void PropertyDescriptorCollection_Add_mC398A8C807A50565628FA7377622CB29BA08E8A0 (void);
// 0x00000A8E System.Void System.ComponentModel.PropertyDescriptorCollection::Clear()
extern void PropertyDescriptorCollection_Clear_mB8C7EDDBC34045AB39A85F7CA8C2A48FE5E84154 (void);
// 0x00000A8F System.Boolean System.ComponentModel.PropertyDescriptorCollection::Contains(System.ComponentModel.PropertyDescriptor)
extern void PropertyDescriptorCollection_Contains_m61BE63B4CBEBE522952340A5E1AD4EC81561A534 (void);
// 0x00000A90 System.Void System.ComponentModel.PropertyDescriptorCollection::CopyTo(System.Array,System.Int32)
extern void PropertyDescriptorCollection_CopyTo_m96A7340B76D01123FFC2968406157491DA5F2F51 (void);
// 0x00000A91 System.Void System.ComponentModel.PropertyDescriptorCollection::EnsurePropsOwned()
extern void PropertyDescriptorCollection_EnsurePropsOwned_m95EB38CC4CD3A669B5890569BE6EA68DDC0691D1 (void);
// 0x00000A92 System.Void System.ComponentModel.PropertyDescriptorCollection::EnsureSize(System.Int32)
extern void PropertyDescriptorCollection_EnsureSize_mA9CC05191669F9F3899F5A7D42B8DF24C51CC6A2 (void);
// 0x00000A93 System.ComponentModel.PropertyDescriptor System.ComponentModel.PropertyDescriptorCollection::Find(System.String,System.Boolean)
extern void PropertyDescriptorCollection_Find_mCBD6FF2B467342E2385A83851AFEA0D79765D941 (void);
// 0x00000A94 System.Int32 System.ComponentModel.PropertyDescriptorCollection::IndexOf(System.ComponentModel.PropertyDescriptor)
extern void PropertyDescriptorCollection_IndexOf_mD936E02B8B1AACBF21EB129B5D7F3C8CBC07BE15 (void);
// 0x00000A95 System.Void System.ComponentModel.PropertyDescriptorCollection::Insert(System.Int32,System.ComponentModel.PropertyDescriptor)
extern void PropertyDescriptorCollection_Insert_m5294EDBAD3EED9E88B72F8C337F65EE5AB2960EA (void);
// 0x00000A96 System.Void System.ComponentModel.PropertyDescriptorCollection::Remove(System.ComponentModel.PropertyDescriptor)
extern void PropertyDescriptorCollection_Remove_m93B40D4D8FD13CA21ACAF39012043595BB6ED03B (void);
// 0x00000A97 System.Void System.ComponentModel.PropertyDescriptorCollection::RemoveAt(System.Int32)
extern void PropertyDescriptorCollection_RemoveAt_m0A24CA5AB989131D0F6691B289DD9092790E9E07 (void);
// 0x00000A98 System.ComponentModel.PropertyDescriptorCollection System.ComponentModel.PropertyDescriptorCollection::Sort(System.String[])
extern void PropertyDescriptorCollection_Sort_m75E8E60ABA88330A9CE34B6DDE5E6DCFD10C6867 (void);
// 0x00000A99 System.Void System.ComponentModel.PropertyDescriptorCollection::InternalSort(System.String[])
extern void PropertyDescriptorCollection_InternalSort_m976B7A3E0A38A56F3E6C282199722DFC37EE92C9 (void);
// 0x00000A9A System.Void System.ComponentModel.PropertyDescriptorCollection::InternalSort(System.Collections.IComparer)
extern void PropertyDescriptorCollection_InternalSort_m715EF0848576185E9957756222B20A93320A3108 (void);
// 0x00000A9B System.Collections.IEnumerator System.ComponentModel.PropertyDescriptorCollection::GetEnumerator()
extern void PropertyDescriptorCollection_GetEnumerator_m895320E1DA7A7E88D5392EA88C7415FCCE9F9A69 (void);
// 0x00000A9C System.Object System.ComponentModel.PropertyDescriptorCollection::System.Collections.ICollection.get_SyncRoot()
extern void PropertyDescriptorCollection_System_Collections_ICollection_get_SyncRoot_m375B281E77CC48014E87622A8F7E5FF2BB6B23AF (void);
// 0x00000A9D System.Int32 System.ComponentModel.PropertyDescriptorCollection::System.Collections.ICollection.get_Count()
extern void PropertyDescriptorCollection_System_Collections_ICollection_get_Count_mD0FF809D823EB373D8A6E8DB49619ED4141702A2 (void);
// 0x00000A9E System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.Clear()
extern void PropertyDescriptorCollection_System_Collections_IList_Clear_m45872C5426EECF7A39B524076FF749A27FFFD720 (void);
// 0x00000A9F System.Collections.IEnumerator System.ComponentModel.PropertyDescriptorCollection::System.Collections.IEnumerable.GetEnumerator()
extern void PropertyDescriptorCollection_System_Collections_IEnumerable_GetEnumerator_mCA1C26C5D8253A2654911C476DB3E2E4C7E1DED0 (void);
// 0x00000AA0 System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.RemoveAt(System.Int32)
extern void PropertyDescriptorCollection_System_Collections_IList_RemoveAt_mF52F79BDB81434BECEFF023F99EFDDEE21E3D382 (void);
// 0x00000AA1 System.Boolean System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.Contains(System.Object)
extern void PropertyDescriptorCollection_System_Collections_IDictionary_Contains_m0E06B08842DB4FA109B5AB61EBEFB731E3EA94AA (void);
// 0x00000AA2 System.Collections.IDictionaryEnumerator System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.GetEnumerator()
extern void PropertyDescriptorCollection_System_Collections_IDictionary_GetEnumerator_mF6EAE817278709EF0201AB2E677B887CA7520A51 (void);
// 0x00000AA3 System.Object System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.get_Item(System.Object)
extern void PropertyDescriptorCollection_System_Collections_IDictionary_get_Item_mC6EFD0AFD470D71014230D69B656107657929330 (void);
// 0x00000AA4 System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern void PropertyDescriptorCollection_System_Collections_IDictionary_set_Item_mF70D7EA54C026CF6DF6082671C7BB5FA4FA8C4A1 (void);
// 0x00000AA5 System.Int32 System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.Add(System.Object)
extern void PropertyDescriptorCollection_System_Collections_IList_Add_mA83681FD649F472C18C4222547909AB5C0B2636D (void);
// 0x00000AA6 System.Boolean System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.Contains(System.Object)
extern void PropertyDescriptorCollection_System_Collections_IList_Contains_mF4A5AD4B8FB2DE87C3F46804A45A9E5FE061F0A8 (void);
// 0x00000AA7 System.Int32 System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.IndexOf(System.Object)
extern void PropertyDescriptorCollection_System_Collections_IList_IndexOf_mDDE0710A46D8C7F0C812BBB749058633E2B538A0 (void);
// 0x00000AA8 System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.Insert(System.Int32,System.Object)
extern void PropertyDescriptorCollection_System_Collections_IList_Insert_mECCC2CBDA9731E920BDA0D1C5E1B3446D1051D7C (void);
// 0x00000AA9 System.Boolean System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.get_IsReadOnly()
extern void PropertyDescriptorCollection_System_Collections_IList_get_IsReadOnly_mD92E29DD5AF442E8F758EE9C364AA67D8C692429 (void);
// 0x00000AAA System.Boolean System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.get_IsFixedSize()
extern void PropertyDescriptorCollection_System_Collections_IList_get_IsFixedSize_m384E8605F0612E87E1A78F863E28AE7E0D0D6B8A (void);
// 0x00000AAB System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.Remove(System.Object)
extern void PropertyDescriptorCollection_System_Collections_IList_Remove_m6B3B43F181BF42AE15D0EC97F83E16157119767A (void);
// 0x00000AAC System.Object System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.get_Item(System.Int32)
extern void PropertyDescriptorCollection_System_Collections_IList_get_Item_m330B3FFF9A800353E1463DBA60359A5846637632 (void);
// 0x00000AAD System.Void System.ComponentModel.PropertyDescriptorCollection::System.Collections.IList.set_Item(System.Int32,System.Object)
extern void PropertyDescriptorCollection_System_Collections_IList_set_Item_m1C9BF875A752FEAC57320E429DF95D0A11BDD5B8 (void);
// 0x00000AAE System.Void System.ComponentModel.PropertyDescriptorCollection::.cctor()
extern void PropertyDescriptorCollection__cctor_m9FF868A1DA3307692B17BEE0248BE146682D8DA7 (void);
// 0x00000AAF System.Void System.ComponentModel.PropertyDescriptorCollection/PropertyDescriptorEnumerator::.ctor(System.ComponentModel.PropertyDescriptorCollection)
extern void PropertyDescriptorEnumerator__ctor_mEF40577980844F48F837016FC78D6D6C4B656C40 (void);
// 0x00000AB0 System.Object System.ComponentModel.PropertyDescriptorCollection/PropertyDescriptorEnumerator::get_Current()
extern void PropertyDescriptorEnumerator_get_Current_mD139182036930AB6DD0F33E0C311C5D280E8169F (void);
// 0x00000AB1 System.Collections.DictionaryEntry System.ComponentModel.PropertyDescriptorCollection/PropertyDescriptorEnumerator::get_Entry()
extern void PropertyDescriptorEnumerator_get_Entry_m83A1F1F4EC45FCDAB6444FDAE98158CAEA6E17C0 (void);
// 0x00000AB2 System.Object System.ComponentModel.PropertyDescriptorCollection/PropertyDescriptorEnumerator::get_Key()
extern void PropertyDescriptorEnumerator_get_Key_m9AC03BAD80D2C982D93FCF6E7FA77694CDA80583 (void);
// 0x00000AB3 System.Object System.ComponentModel.PropertyDescriptorCollection/PropertyDescriptorEnumerator::get_Value()
extern void PropertyDescriptorEnumerator_get_Value_m5EC049B1D1B61491D697056DD2A5018B3F83DAB2 (void);
// 0x00000AB4 System.Boolean System.ComponentModel.PropertyDescriptorCollection/PropertyDescriptorEnumerator::MoveNext()
extern void PropertyDescriptorEnumerator_MoveNext_m1558DACC789497E2E8BEC2C914F68449B51F35E5 (void);
// 0x00000AB5 System.Void System.ComponentModel.PropertyDescriptorCollection/PropertyDescriptorEnumerator::Reset()
extern void PropertyDescriptorEnumerator_Reset_mF7C0D145E906CE6FCB18646933E580C14A0AE1DE (void);
// 0x00000AB6 System.Void System.ComponentModel.RefreshEventArgs::.ctor(System.Type)
extern void RefreshEventArgs__ctor_mA88DB8D3EBE2EDC16126E5DA09495F31F6E43CC9 (void);
// 0x00000AB7 System.Void System.ComponentModel.RefreshEventHandler::.ctor(System.Object,System.IntPtr)
extern void RefreshEventHandler__ctor_m2A318A5FC4A7829BCC18BE0F9A652DCD1C8C192F (void);
// 0x00000AB8 System.Void System.ComponentModel.RefreshEventHandler::Invoke(System.ComponentModel.RefreshEventArgs)
extern void RefreshEventHandler_Invoke_m64C4651451C458F51B3DB45E9BF2D20E0B1DBCF7 (void);
// 0x00000AB9 System.Boolean System.ComponentModel.SingleConverter::get_AllowHex()
extern void SingleConverter_get_AllowHex_m49756E0A9F6A76035FC8ED207F161A4C28C7B0DF (void);
// 0x00000ABA System.Type System.ComponentModel.SingleConverter::get_TargetType()
extern void SingleConverter_get_TargetType_mC060F5026570A7907B8B1D9EA0402752331C6BD0 (void);
// 0x00000ABB System.Object System.ComponentModel.SingleConverter::FromString(System.String,System.Int32)
extern void SingleConverter_FromString_mAB3584379A90F18A33A61D1913F74D590A17DB30 (void);
// 0x00000ABC System.Object System.ComponentModel.SingleConverter::FromString(System.String,System.Globalization.NumberFormatInfo)
extern void SingleConverter_FromString_m27810AC814274F6507B5D2B3E3D6886B153661DC (void);
// 0x00000ABD System.String System.ComponentModel.SingleConverter::ToString(System.Object,System.Globalization.NumberFormatInfo)
extern void SingleConverter_ToString_m02237BA935CD713251AB84BA7B0E77283E65427E (void);
// 0x00000ABE System.Void System.ComponentModel.SingleConverter::.ctor()
extern void SingleConverter__ctor_m463B63EB8D945D2D43E5B8099D8F0EC24AEBDA7A (void);
// 0x00000ABF System.Boolean System.ComponentModel.StringConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void StringConverter_CanConvertFrom_mCE12DA7BD7ABA631AA3C00EF415C6C650636D00D (void);
// 0x00000AC0 System.Object System.ComponentModel.StringConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern void StringConverter_ConvertFrom_mDC2BBDD289C8D8259AF915CB23765888BBFAB9AC (void);
// 0x00000AC1 System.Void System.ComponentModel.StringConverter::.ctor()
extern void StringConverter__ctor_m074D8686ED6ABC0E64C20D1EFD4406F215E7187A (void);
// 0x00000AC2 System.Boolean System.ComponentModel.TimeSpanConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void TimeSpanConverter_CanConvertFrom_mD6725B6738FA9EC354BD235B49DBEF0A522F1110 (void);
// 0x00000AC3 System.Boolean System.ComponentModel.TimeSpanConverter::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void TimeSpanConverter_CanConvertTo_mA1CF08D25D65F197EAA28F3FD3C4218A27FECCA0 (void);
// 0x00000AC4 System.Object System.ComponentModel.TimeSpanConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern void TimeSpanConverter_ConvertFrom_mF00171D4039FEA734A0FC5ACDB81BB0395A3AFF2 (void);
// 0x00000AC5 System.Object System.ComponentModel.TimeSpanConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern void TimeSpanConverter_ConvertTo_m30C65F425D5F7D46475EC262213EB57255929A6F (void);
// 0x00000AC6 System.Void System.ComponentModel.TimeSpanConverter::.ctor()
extern void TimeSpanConverter__ctor_m7319DCEEDA2578B340AD4FEEE010AE5CEFAC9BD5 (void);
// 0x00000AC7 System.Void System.ComponentModel.TypeConverterAttribute::.ctor()
extern void TypeConverterAttribute__ctor_mEEE3CA7C17555BA93BB76201BA2BCF879E617DA9 (void);
// 0x00000AC8 System.Void System.ComponentModel.TypeConverterAttribute::.ctor(System.Type)
extern void TypeConverterAttribute__ctor_m0E1A55E1BD368F0AABCAF766BBCE1D275777A52A (void);
// 0x00000AC9 System.String System.ComponentModel.TypeConverterAttribute::get_ConverterTypeName()
extern void TypeConverterAttribute_get_ConverterTypeName_mBBE5335F11FC13345D2828866172BB5DDF9F3E45 (void);
// 0x00000ACA System.Boolean System.ComponentModel.TypeConverterAttribute::Equals(System.Object)
extern void TypeConverterAttribute_Equals_mA3BF2C8CFA0AE8B5D0F52E066B1141AA53B4E015 (void);
// 0x00000ACB System.Int32 System.ComponentModel.TypeConverterAttribute::GetHashCode()
extern void TypeConverterAttribute_GetHashCode_m23ACA2A297447E17150BF490188919676FBD8A01 (void);
// 0x00000ACC System.Void System.ComponentModel.TypeConverterAttribute::.cctor()
extern void TypeConverterAttribute__cctor_mB9A8C9E5E62F8A8D81E250D0B53D359860D7C04A (void);
// 0x00000ACD System.Void System.ComponentModel.TypeDescriptionProvider::.ctor()
extern void TypeDescriptionProvider__ctor_m575B3B96D9C9A3B7C0D34A14D9E5FAA63EEE004C (void);
// 0x00000ACE System.Type System.ComponentModel.TypeDescriptionProvider::GetReflectionType(System.Type)
extern void TypeDescriptionProvider_GetReflectionType_m595CDB1DADC484AB12377301C2D93AD8DE2A61C8 (void);
// 0x00000ACF System.Type System.ComponentModel.TypeDescriptionProvider::GetReflectionType(System.Type,System.Object)
extern void TypeDescriptionProvider_GetReflectionType_m370874914F8C84A0B85985491FDBAAC993C3851C (void);
// 0x00000AD0 System.String System.ComponentModel.TypeDescriptionProviderAttribute::get_TypeName()
extern void TypeDescriptionProviderAttribute_get_TypeName_m7E06882E9348A8D4987BB62EA1493B2BD23147E6 (void);
// 0x00000AD1 System.Object System.ComponentModel.CollectionConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern void CollectionConverter_ConvertTo_m79E20F624C9391C76B8081E74BF16EB4C04535E7 (void);
// 0x00000AD2 System.ComponentModel.PropertyDescriptorCollection System.ComponentModel.CollectionConverter::GetProperties(System.ComponentModel.ITypeDescriptorContext,System.Object,System.Attribute[])
extern void CollectionConverter_GetProperties_m5E61C7ABF345599E670E2FD270A35190DA1DBF20 (void);
// 0x00000AD3 System.Boolean System.ComponentModel.CollectionConverter::GetPropertiesSupported(System.ComponentModel.ITypeDescriptorContext)
extern void CollectionConverter_GetPropertiesSupported_m2068FA7A17AA42142E42EBF63151013E0934C728 (void);
// 0x00000AD4 System.Void System.ComponentModel.CollectionConverter::.ctor()
extern void CollectionConverter__ctor_m20ED0C661750EBE427D356F24AED92A407988147 (void);
// 0x00000AD5 System.Void System.ComponentModel.EnumConverter::.ctor(System.Type)
extern void EnumConverter__ctor_m3E3C7D81C8092A5591BAA556288B2A3E22DCA99B (void);
// 0x00000AD6 System.Boolean System.ComponentModel.EnumConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void EnumConverter_CanConvertFrom_m08BA3C1DC3D84B157F087AE1E407A974E3A85D11 (void);
// 0x00000AD7 System.Boolean System.ComponentModel.EnumConverter::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void EnumConverter_CanConvertTo_mF6140EA2D6D701B21DC1EEFAAA2C62DF8CAB690A (void);
// 0x00000AD8 System.Collections.IComparer System.ComponentModel.EnumConverter::get_Comparer()
extern void EnumConverter_get_Comparer_mBB79A346E8150D9554B6FBCD0EF1B981A7696DEC (void);
// 0x00000AD9 System.Object System.ComponentModel.EnumConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern void EnumConverter_ConvertFrom_mD1D0496C621971B0610C92ABC562B26BEA40EC61 (void);
// 0x00000ADA System.Object System.ComponentModel.EnumConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern void EnumConverter_ConvertTo_m95581FDC64EFF404D3646BA0BB09A316C58126D1 (void);
// 0x00000ADB System.ComponentModel.TypeConverter/StandardValuesCollection System.ComponentModel.EnumConverter::GetStandardValues(System.ComponentModel.ITypeDescriptorContext)
extern void EnumConverter_GetStandardValues_mAF14DECB7670119DFE61D9955A5667BC43A1F023 (void);
// 0x00000ADC System.Boolean System.ComponentModel.EnumConverter::GetStandardValuesExclusive(System.ComponentModel.ITypeDescriptorContext)
extern void EnumConverter_GetStandardValuesExclusive_mFFEE5BB5AEA41073002C03D1CB163E3F42619317 (void);
// 0x00000ADD System.Boolean System.ComponentModel.EnumConverter::GetStandardValuesSupported(System.ComponentModel.ITypeDescriptorContext)
extern void EnumConverter_GetStandardValuesSupported_m049732C08BD2A4300C75A9FB12BE6082C2DAA5EA (void);
// 0x00000ADE System.Boolean System.ComponentModel.EnumConverter::IsValid(System.ComponentModel.ITypeDescriptorContext,System.Object)
extern void EnumConverter_IsValid_mF38C2D56D21042513C025A195EBF00C9626CB105 (void);
// 0x00000ADF System.Void System.ComponentModel.MemberDescriptor::.ctor(System.String,System.Attribute[])
extern void MemberDescriptor__ctor_mDCA5ED4D5D9CB14D6D1D81EDB76EA80DB79B8AF4 (void);
// 0x00000AE0 System.String System.ComponentModel.MemberDescriptor::get_Name()
extern void MemberDescriptor_get_Name_mA0D12966D84092E03F4DC16A085176299AC73291 (void);
// 0x00000AE1 System.Int32 System.ComponentModel.MemberDescriptor::get_NameHashCode()
extern void MemberDescriptor_get_NameHashCode_m415B96A35EB826A5A00E0F0EBBDD94980BD5E5F2 (void);
// 0x00000AE2 System.Boolean System.ComponentModel.MemberDescriptor::Equals(System.Object)
extern void MemberDescriptor_Equals_m375ECBF78B86952EC60CDD90301D8C2EF911A620 (void);
// 0x00000AE3 System.Void System.ComponentModel.MemberDescriptor::FillAttributes(System.Collections.IList)
extern void MemberDescriptor_FillAttributes_mD79B898348F5E1F6C98F62F0D4CE719C5547C56C (void);
// 0x00000AE4 System.Void System.ComponentModel.MemberDescriptor::FilterAttributesIfNeeded()
extern void MemberDescriptor_FilterAttributesIfNeeded_mB060A4FB6DE46D17F76E54B43A0772EC623EFDC7 (void);
// 0x00000AE5 System.Int32 System.ComponentModel.MemberDescriptor::GetHashCode()
extern void MemberDescriptor_GetHashCode_m18BBAA42F4FF641C84B5E45802F1FDC290F86860 (void);
// 0x00000AE6 System.Void System.ComponentModel.ReflectTypeDescriptionProvider::.ctor()
extern void ReflectTypeDescriptionProvider__ctor_mD2C527AEF85A636D3E4507604E84FAB79A98DB62 (void);
// 0x00000AE7 System.Type System.ComponentModel.ReflectTypeDescriptionProvider::GetReflectionType(System.Type,System.Object)
extern void ReflectTypeDescriptionProvider_GetReflectionType_m42CCC59A3631DE84FA1BA53F4D0266F5CC7DA92B (void);
// 0x00000AE8 System.ComponentModel.ReflectTypeDescriptionProvider/ReflectedTypeData System.ComponentModel.ReflectTypeDescriptionProvider::GetTypeData(System.Type,System.Boolean)
extern void ReflectTypeDescriptionProvider_GetTypeData_mAB0CA0760FE347023B0CB43F64B800C0E4CF2064 (void);
// 0x00000AE9 System.Boolean System.ComponentModel.ReflectTypeDescriptionProvider::IsPopulated(System.Type)
extern void ReflectTypeDescriptionProvider_IsPopulated_m84F86F476D4C3552D9319BE958418D78CE50C1A8 (void);
// 0x00000AEA System.Void System.ComponentModel.ReflectTypeDescriptionProvider::Refresh(System.Type)
extern void ReflectTypeDescriptionProvider_Refresh_m2E5B6CEEA9ED33154509B1DE82E5BF4E5814CAF0 (void);
// 0x00000AEB System.Void System.ComponentModel.ReflectTypeDescriptionProvider::.cctor()
extern void ReflectTypeDescriptionProvider__cctor_m7F48DC584D9D7017DDCF9AC5F86CDF8193097C7F (void);
// 0x00000AEC System.Void System.ComponentModel.ReflectTypeDescriptionProvider/ReflectedTypeData::.ctor(System.Type)
extern void ReflectedTypeData__ctor_m229206DEEAEC214C249B48F72505C12A2C10F233 (void);
// 0x00000AED System.Boolean System.ComponentModel.ReflectTypeDescriptionProvider/ReflectedTypeData::get_IsPopulated()
extern void ReflectedTypeData_get_IsPopulated_m207FD7DEB577D25B681C31AE2F2A86D64895C76E (void);
// 0x00000AEE System.Void System.ComponentModel.ReflectTypeDescriptionProvider/ReflectedTypeData::Refresh()
extern void ReflectedTypeData_Refresh_m5A18415C807E341603C1A069AA7A3EB76258CAAD (void);
// 0x00000AEF System.Boolean System.ComponentModel.TypeConverter::get_UseCompatibleTypeConversion()
extern void TypeConverter_get_UseCompatibleTypeConversion_mC6FAEA504FE4DAA7723C2B31F7E7C80E237110A1 (void);
// 0x00000AF0 System.Boolean System.ComponentModel.TypeConverter::CanConvertFrom(System.Type)
extern void TypeConverter_CanConvertFrom_mE41048372E94CADCD46A0E450385F4E13011D19B (void);
// 0x00000AF1 System.Boolean System.ComponentModel.TypeConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void TypeConverter_CanConvertFrom_m5E04D8BFD5D73042127767BD65B6434DAAEC2E53 (void);
// 0x00000AF2 System.Boolean System.ComponentModel.TypeConverter::CanConvertTo(System.Type)
extern void TypeConverter_CanConvertTo_m05803F15F053E4DA3CF290D52FC379BB9D8C396A (void);
// 0x00000AF3 System.Boolean System.ComponentModel.TypeConverter::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern void TypeConverter_CanConvertTo_m959B290C863DDFBDD575AF3236B0A17E02E44013 (void);
// 0x00000AF4 System.Object System.ComponentModel.TypeConverter::ConvertFrom(System.Object)
extern void TypeConverter_ConvertFrom_m74D536778D865EA22414730575143153FD749EDD (void);
// 0x00000AF5 System.Object System.ComponentModel.TypeConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern void TypeConverter_ConvertFrom_mDE2D27BF8934184429496B39ED63BD3702868774 (void);
// 0x00000AF6 System.Object System.ComponentModel.TypeConverter::ConvertFromInvariantString(System.String)
extern void TypeConverter_ConvertFromInvariantString_m81DA50DC43E5868C83A5D283FFD227B919C9D0A5 (void);
// 0x00000AF7 System.Object System.ComponentModel.TypeConverter::ConvertFromInvariantString(System.ComponentModel.ITypeDescriptorContext,System.String)
extern void TypeConverter_ConvertFromInvariantString_m990BDB1C582D2E303DAA1DF2B123260C6A55A31C (void);
// 0x00000AF8 System.Object System.ComponentModel.TypeConverter::ConvertFromString(System.String)
extern void TypeConverter_ConvertFromString_mE302BA2C46E1B0D2C310A5E5B32AA71EAA73F3AD (void);
// 0x00000AF9 System.Object System.ComponentModel.TypeConverter::ConvertFromString(System.ComponentModel.ITypeDescriptorContext,System.String)
extern void TypeConverter_ConvertFromString_m6B1F932DB2AE6F4C06B84360B51790FF06C6870B (void);
// 0x00000AFA System.Object System.ComponentModel.TypeConverter::ConvertFromString(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.String)
extern void TypeConverter_ConvertFromString_m84FD8657B6D8327BE563E5A649A1D89A80BF2A32 (void);
// 0x00000AFB System.Object System.ComponentModel.TypeConverter::ConvertTo(System.Object,System.Type)
extern void TypeConverter_ConvertTo_m0171DC87F8C3920E781691F5F8F1145A6DAF9CE8 (void);
// 0x00000AFC System.Object System.ComponentModel.TypeConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern void TypeConverter_ConvertTo_m5D4785A320B7ED351B677E28B77171F70E389B96 (void);
// 0x00000AFD System.String System.ComponentModel.TypeConverter::ConvertToInvariantString(System.Object)
extern void TypeConverter_ConvertToInvariantString_m1E0494D32C84A9F062F9762C8979E223C74D803C (void);
// 0x00000AFE System.String System.ComponentModel.TypeConverter::ConvertToInvariantString(System.ComponentModel.ITypeDescriptorContext,System.Object)
extern void TypeConverter_ConvertToInvariantString_mF902AFB0FE32B8E7014DE226455D3FC79030FF92 (void);
// 0x00000AFF System.String System.ComponentModel.TypeConverter::ConvertToString(System.Object)
extern void TypeConverter_ConvertToString_m124B43934E0022CA836E7258ABAF8F9FA8911093 (void);
// 0x00000B00 System.String System.ComponentModel.TypeConverter::ConvertToString(System.ComponentModel.ITypeDescriptorContext,System.Object)
extern void TypeConverter_ConvertToString_mBE2DD3AF57FBB948F861DBD1DEAE33B18A33BF28 (void);
// 0x00000B01 System.String System.ComponentModel.TypeConverter::ConvertToString(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern void TypeConverter_ConvertToString_m85A19DC8D0117332C304EBEE2911D66F8028E245 (void);
// 0x00000B02 System.Object System.ComponentModel.TypeConverter::CreateInstance(System.Collections.IDictionary)
extern void TypeConverter_CreateInstance_m3431A3634D1048D919FF4B1DE80605A2BC8EA7C2 (void);
// 0x00000B03 System.Object System.ComponentModel.TypeConverter::CreateInstance(System.ComponentModel.ITypeDescriptorContext,System.Collections.IDictionary)
extern void TypeConverter_CreateInstance_mCF1FB466625510C41AF4C31DBF0D6B0D17AD8F34 (void);
// 0x00000B04 System.Exception System.ComponentModel.TypeConverter::GetConvertFromException(System.Object)
extern void TypeConverter_GetConvertFromException_m4CD1BB8AACD489FC7094283F199FC6DB6409F7AE (void);
// 0x00000B05 System.Exception System.ComponentModel.TypeConverter::GetConvertToException(System.Object,System.Type)
extern void TypeConverter_GetConvertToException_m2F0A3FF8E38030AEABCB146D9A65FD27C3F4FE52 (void);
// 0x00000B06 System.Boolean System.ComponentModel.TypeConverter::GetCreateInstanceSupported()
extern void TypeConverter_GetCreateInstanceSupported_mFFBD842063695B04C1D1F918715576E58210B9EA (void);
// 0x00000B07 System.Boolean System.ComponentModel.TypeConverter::GetCreateInstanceSupported(System.ComponentModel.ITypeDescriptorContext)
extern void TypeConverter_GetCreateInstanceSupported_m6F211317A2C58B002A523A637B40EC6E878D031B (void);
// 0x00000B08 System.ComponentModel.PropertyDescriptorCollection System.ComponentModel.TypeConverter::GetProperties(System.Object)
extern void TypeConverter_GetProperties_m469ECD3A977FA29494A2D9E6F6EE957EA921E107 (void);
// 0x00000B09 System.ComponentModel.PropertyDescriptorCollection System.ComponentModel.TypeConverter::GetProperties(System.ComponentModel.ITypeDescriptorContext,System.Object)
extern void TypeConverter_GetProperties_m59D3B36D9259D09DC67C2DA59245BCEBDFA4D6F2 (void);
// 0x00000B0A System.ComponentModel.PropertyDescriptorCollection System.ComponentModel.TypeConverter::GetProperties(System.ComponentModel.ITypeDescriptorContext,System.Object,System.Attribute[])
extern void TypeConverter_GetProperties_mBD67295683221C70AB3253653C06C39F640F4F44 (void);
// 0x00000B0B System.Boolean System.ComponentModel.TypeConverter::GetPropertiesSupported()
extern void TypeConverter_GetPropertiesSupported_m67A5238F90828ECD5ABFFEA50DF3F10DE2324A3D (void);
// 0x00000B0C System.Boolean System.ComponentModel.TypeConverter::GetPropertiesSupported(System.ComponentModel.ITypeDescriptorContext)
extern void TypeConverter_GetPropertiesSupported_mDE66D29A3EED2AC764A8B4F3C91917E2F9732763 (void);
// 0x00000B0D System.Collections.ICollection System.ComponentModel.TypeConverter::GetStandardValues()
extern void TypeConverter_GetStandardValues_m2D0F59931104F725470D67A2DC6167B1980BE390 (void);
// 0x00000B0E System.ComponentModel.TypeConverter/StandardValuesCollection System.ComponentModel.TypeConverter::GetStandardValues(System.ComponentModel.ITypeDescriptorContext)
extern void TypeConverter_GetStandardValues_m9F77FD4C8216D3F06928B418993659F3A0FEF5B6 (void);
// 0x00000B0F System.Boolean System.ComponentModel.TypeConverter::GetStandardValuesExclusive()
extern void TypeConverter_GetStandardValuesExclusive_m41CA2CC09F031BC8A14339DC376FE50D31AE9812 (void);
// 0x00000B10 System.Boolean System.ComponentModel.TypeConverter::GetStandardValuesExclusive(System.ComponentModel.ITypeDescriptorContext)
extern void TypeConverter_GetStandardValuesExclusive_mCA336C7CE58B28DF0B46D5B52E3D0D177718FF42 (void);
// 0x00000B11 System.Boolean System.ComponentModel.TypeConverter::GetStandardValuesSupported()
extern void TypeConverter_GetStandardValuesSupported_mDE36B9244050270D1DC2F6F840C10DA945CFAA23 (void);
// 0x00000B12 System.Boolean System.ComponentModel.TypeConverter::GetStandardValuesSupported(System.ComponentModel.ITypeDescriptorContext)
extern void TypeConverter_GetStandardValuesSupported_m5DFA377B1091BB08E64654FBEE8613EEDE731718 (void);
// 0x00000B13 System.Boolean System.ComponentModel.TypeConverter::IsValid(System.Object)
extern void TypeConverter_IsValid_m54570B0309A0F2735FE3FA162D5F669B8BF434C8 (void);
// 0x00000B14 System.Boolean System.ComponentModel.TypeConverter::IsValid(System.ComponentModel.ITypeDescriptorContext,System.Object)
extern void TypeConverter_IsValid_m991D78815B29EDE7EF8B1CD0114A5E6B84F7328C (void);
// 0x00000B15 System.ComponentModel.PropertyDescriptorCollection System.ComponentModel.TypeConverter::SortProperties(System.ComponentModel.PropertyDescriptorCollection,System.String[])
extern void TypeConverter_SortProperties_m814BFD1763DEB6AD4D3E6B6042777358523AB3B6 (void);
// 0x00000B16 System.Void System.ComponentModel.TypeConverter::.ctor()
extern void TypeConverter__ctor_mA5B1882A94D0491297B903563E8B03D75B2F67A2 (void);
// 0x00000B17 System.Void System.ComponentModel.TypeConverter/SimplePropertyDescriptor::.ctor(System.Type,System.String,System.Type,System.Attribute[])
extern void SimplePropertyDescriptor__ctor_m6DF452C766FA5F610C684119950D96C7105A2B26 (void);
// 0x00000B18 System.Type System.ComponentModel.TypeConverter/SimplePropertyDescriptor::get_PropertyType()
extern void SimplePropertyDescriptor_get_PropertyType_m613F51DD8C6ECFE9C107312A24AAE28F375650A1 (void);
// 0x00000B19 System.Void System.ComponentModel.TypeConverter/StandardValuesCollection::.ctor(System.Collections.ICollection)
extern void StandardValuesCollection__ctor_m9049046DA81FD7B6FE279E063A43DC9B1D3B26B0 (void);
// 0x00000B1A System.Int32 System.ComponentModel.TypeConverter/StandardValuesCollection::get_Count()
extern void StandardValuesCollection_get_Count_mC127EA7E146C2FD58D5740935B45CACCD13D4637 (void);
// 0x00000B1B System.Void System.ComponentModel.TypeConverter/StandardValuesCollection::CopyTo(System.Array,System.Int32)
extern void StandardValuesCollection_CopyTo_mC06C9542D25F3E2D5A1DFDE690C920E8CFF94E10 (void);
// 0x00000B1C System.Collections.IEnumerator System.ComponentModel.TypeConverter/StandardValuesCollection::GetEnumerator()
extern void StandardValuesCollection_GetEnumerator_m0484C08651E32F669A0624D29E44C6CDEB550F6C (void);
// 0x00000B1D System.Int32 System.ComponentModel.TypeConverter/StandardValuesCollection::System.Collections.ICollection.get_Count()
extern void StandardValuesCollection_System_Collections_ICollection_get_Count_m98A89DC263C152B5E1A5B5A6B795D963CBADCD4D (void);
// 0x00000B1E System.Object System.ComponentModel.TypeConverter/StandardValuesCollection::System.Collections.ICollection.get_SyncRoot()
extern void StandardValuesCollection_System_Collections_ICollection_get_SyncRoot_mFFAF4C68B76F26F6816CEAB7D8D86656B1D1AC51 (void);
// 0x00000B1F System.Void System.ComponentModel.TypeConverter/StandardValuesCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern void StandardValuesCollection_System_Collections_ICollection_CopyTo_mF996F3746F028C7EB58F7096E7B7B7FAB911A896 (void);
// 0x00000B20 System.Collections.IEnumerator System.ComponentModel.TypeConverter/StandardValuesCollection::System.Collections.IEnumerable.GetEnumerator()
extern void StandardValuesCollection_System_Collections_IEnumerable_GetEnumerator_m66B4D529F0193F42A3858A1B638372B8A6687672 (void);
// 0x00000B21 System.Type System.ComponentModel.TypeDescriptor::get_InterfaceType()
extern void TypeDescriptor_get_InterfaceType_m468BAAC18190A6D8E4369B3139FB38ECE1B4C5BB (void);
// 0x00000B22 System.Int32 System.ComponentModel.TypeDescriptor::get_MetadataVersion()
extern void TypeDescriptor_get_MetadataVersion_m6E33CD6DCADDEA862C9D9266B664C21DE70D449A (void);
// 0x00000B23 System.Void System.ComponentModel.TypeDescriptor::AddProvider(System.ComponentModel.TypeDescriptionProvider,System.Type)
extern void TypeDescriptor_AddProvider_mCE291812F3ECB4C76537D451403951E926F7EBF4 (void);
// 0x00000B24 System.Void System.ComponentModel.TypeDescriptor::CheckDefaultProvider(System.Type)
extern void TypeDescriptor_CheckDefaultProvider_m1467B92A2B042B2E7F6332210808BB6AA42BABB1 (void);
// 0x00000B25 System.Type System.ComponentModel.TypeDescriptor::GetNodeForBaseType(System.Type)
extern void TypeDescriptor_GetNodeForBaseType_m85BCD83E89EE286F365A1553239C985BEB1D785C (void);
// 0x00000B26 System.ComponentModel.TypeDescriptionProvider System.ComponentModel.TypeDescriptor::GetProviderRecursive(System.Type)
extern void TypeDescriptor_GetProviderRecursive_m371FD85FC01238874F7387FA430260D57646EA3F (void);
// 0x00000B27 System.Type System.ComponentModel.TypeDescriptor::GetReflectionType(System.Type)
extern void TypeDescriptor_GetReflectionType_m2E40168C9752B069A3E430478C46D54463DA6707 (void);
// 0x00000B28 System.ComponentModel.TypeDescriptor/TypeDescriptionNode System.ComponentModel.TypeDescriptor::NodeFor(System.Type)
extern void TypeDescriptor_NodeFor_m99219DF297037E09BCCFD4492FFEDDD7C7D229A1 (void);
// 0x00000B29 System.ComponentModel.TypeDescriptor/TypeDescriptionNode System.ComponentModel.TypeDescriptor::NodeFor(System.Type,System.Boolean)
extern void TypeDescriptor_NodeFor_mE9C1F8845A707769A5A95E338C610FA244DA283C (void);
// 0x00000B2A System.Void System.ComponentModel.TypeDescriptor::RaiseRefresh(System.Type)
extern void TypeDescriptor_RaiseRefresh_m026E52DA15538070CED25BBCBB696318032A6A8C (void);
// 0x00000B2B System.Void System.ComponentModel.TypeDescriptor::Refresh(System.Type)
extern void TypeDescriptor_Refresh_m8F344E720B395FC5493977659FACECE567A6A223 (void);
// 0x00000B2C System.Void System.ComponentModel.TypeDescriptor::SortDescriptorArray(System.Collections.IList)
extern void TypeDescriptor_SortDescriptorArray_mF088401B40E9E2739EC9F16B98F05D60A9B89A47 (void);
// 0x00000B2D System.Void System.ComponentModel.TypeDescriptor::.cctor()
extern void TypeDescriptor__cctor_m3EDDF7A3EA099841E95028726F74677419380E0D (void);
// 0x00000B2E System.Int32 System.ComponentModel.TypeDescriptor/MemberDescriptorComparer::Compare(System.Object,System.Object)
extern void MemberDescriptorComparer_Compare_m33438352E9BA8C1BF6B594E524DC1BF8BC5EFFE1 (void);
// 0x00000B2F System.Void System.ComponentModel.TypeDescriptor/MemberDescriptorComparer::.ctor()
extern void MemberDescriptorComparer__ctor_mDCB73C6EC35AE31BE43CBF4C88053920603B4431 (void);
// 0x00000B30 System.Void System.ComponentModel.TypeDescriptor/MemberDescriptorComparer::.cctor()
extern void MemberDescriptorComparer__cctor_m31A7C495358AAA5B5E72F47BDC54DDC970E3A11C (void);
// 0x00000B31 System.Void System.ComponentModel.TypeDescriptor/TypeDescriptionNode::.ctor(System.ComponentModel.TypeDescriptionProvider)
extern void TypeDescriptionNode__ctor_mD6C85C9088064C16A401309FAFDC62252A5D5526 (void);
// 0x00000B32 System.Type System.ComponentModel.TypeDescriptor/TypeDescriptionNode::GetReflectionType(System.Type,System.Object)
extern void TypeDescriptionNode_GetReflectionType_m54393E311DD22AEF766FC73C95C7059CCD09FBF5 (void);
// 0x00000B33 System.Void System.ComponentModel.Win32Exception::.ctor()
extern void Win32Exception__ctor_m0B20F25EC4A56A122FF766429AF40AF6292E11B1 (void);
// 0x00000B34 System.Void System.ComponentModel.Win32Exception::.ctor(System.Int32)
extern void Win32Exception__ctor_mFC38F1C99031D227C6BB3CA07099866D39994281 (void);
// 0x00000B35 System.Void System.ComponentModel.Win32Exception::.ctor(System.Int32,System.String)
extern void Win32Exception__ctor_mB0380F996944EDFCF9CACC7F28A18874F19B4FD4 (void);
// 0x00000B36 System.Void System.ComponentModel.Win32Exception::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void Win32Exception__ctor_m1B3DF3F51BBF050C18F933B36027A8A4902668C4 (void);
// 0x00000B37 System.Int32 System.ComponentModel.Win32Exception::get_NativeErrorCode()
extern void Win32Exception_get_NativeErrorCode_m33C1D176D9178C36DA32F8FEC251B923EE53CE5E (void);
// 0x00000B38 System.Void System.ComponentModel.Win32Exception::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void Win32Exception_GetObjectData_mB7993FD0BE12222D5518373EBAB84F89651D99D2 (void);
// 0x00000B39 System.String System.ComponentModel.Win32Exception::GetErrorMessage(System.Int32)
extern void Win32Exception_GetErrorMessage_mE857D9093D6D366D960BD8A8FCCF22DF6FB92728 (void);
// 0x00000B3A System.Void System.ComponentModel.WeakHashtable::.ctor()
extern void WeakHashtable__ctor_mBFFE17D4547F681A27EBE60D277625C9F9D0510F (void);
// 0x00000B3B System.Void System.ComponentModel.WeakHashtable::Clear()
extern void WeakHashtable_Clear_m306746F8BC4535C2327FFDF167F23CA06077A6EE (void);
// 0x00000B3C System.Void System.ComponentModel.WeakHashtable::Remove(System.Object)
extern void WeakHashtable_Remove_mC091A16F4775270A1FC840FE4E82917E4F010A03 (void);
// 0x00000B3D System.Void System.ComponentModel.WeakHashtable::.cctor()
extern void WeakHashtable__cctor_mDC0172C1DE43E226990881404CE73D1B64046A16 (void);
// 0x00000B3E System.Boolean System.ComponentModel.WeakHashtable/WeakKeyComparer::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern void WeakKeyComparer_System_Collections_IEqualityComparer_Equals_m2DE66C4A7CF44E5A99DA241D8271C285ABBE56F8 (void);
// 0x00000B3F System.Int32 System.ComponentModel.WeakHashtable/WeakKeyComparer::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern void WeakKeyComparer_System_Collections_IEqualityComparer_GetHashCode_m7E00D4A81998A46FFD9BE51679C6169DF0CCBF63 (void);
// 0x00000B40 System.Void System.ComponentModel.WeakHashtable/WeakKeyComparer::.ctor()
extern void WeakKeyComparer__ctor_m1DB79747A0192C8CB5B45E74743CA7CA7EC09E52 (void);
// 0x00000B41 System.Void System.ComponentModel.Design.Serialization.InstanceDescriptor::.ctor(System.Reflection.MemberInfo,System.Collections.ICollection)
extern void InstanceDescriptor__ctor_m4A88597D681C8A8A9551AB318F8CBB47D49BEC57 (void);
// 0x00000B42 System.Void System.ComponentModel.Design.Serialization.InstanceDescriptor::.ctor(System.Reflection.MemberInfo,System.Collections.ICollection,System.Boolean)
extern void InstanceDescriptor__ctor_m9AA7C372C2EEDA3F07BE3A6CCC21BCBD3814B64F (void);
// 0x00000B43 System.Collections.ICollection System.ComponentModel.Design.Serialization.InstanceDescriptor::get_Arguments()
extern void InstanceDescriptor_get_Arguments_mF64376FB377AA57CC10376452FECFDC55C6EC837 (void);
// 0x00000B44 System.Reflection.MemberInfo System.ComponentModel.Design.Serialization.InstanceDescriptor::get_MemberInfo()
extern void InstanceDescriptor_get_MemberInfo_m58DD58AFB1C49C6B33C3958002E7CB9D61374E9E (void);
// 0x00000B45 System.Object System.ComponentModel.Design.Serialization.InstanceDescriptor::Invoke()
extern void InstanceDescriptor_Invoke_mACDF3C04B3998B38C4A79FCFC66F4760EEEEB2FE (void);
// 0x00000B46 System.Boolean System.Collections.HashtableExtensions::TryGetValue(System.Collections.Hashtable,System.Object,T&)
// 0x00000B47 System.Void System.Collections.Specialized.NameValueCollection::.ctor()
extern void NameValueCollection__ctor_mCA29E5386DACDF19F6F51BC395324F0A8EE05604 (void);
// 0x00000B48 System.Void System.Collections.Specialized.NameValueCollection::.ctor(System.Int32,System.Collections.IEqualityComparer)
extern void NameValueCollection__ctor_m1545E08D22D8E6C1FCDDAE43741AA43D52F4A21E (void);
// 0x00000B49 System.Void System.Collections.Specialized.NameValueCollection::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void NameValueCollection__ctor_mAC66C8064A4AAB73914143C4CDF3BF28543BAEC3 (void);
// 0x00000B4A System.Void System.Collections.Specialized.NameValueCollection::InvalidateCachedArrays()
extern void NameValueCollection_InvalidateCachedArrays_mA9AA2F9B6A67C2576FA30F12208AA6E1F149C44B (void);
// 0x00000B4B System.String System.Collections.Specialized.NameValueCollection::GetAsOneString(System.Collections.ArrayList)
extern void NameValueCollection_GetAsOneString_m6B44F208B557CE92924A116A67838CB4C259B844 (void);
// 0x00000B4C System.Void System.Collections.Specialized.NameValueCollection::Add(System.String,System.String)
extern void NameValueCollection_Add_m68C266151774CD1FC057C9349BAE09BDB15A26A7 (void);
// 0x00000B4D System.String System.Collections.Specialized.NameValueCollection::Get(System.String)
extern void NameValueCollection_Get_m786B25D45B38D7051C696104946F3F0A2DF78106 (void);
// 0x00000B4E System.String System.Collections.Specialized.NameValueCollection::get_Item(System.String)
extern void NameValueCollection_get_Item_mE3ED755803E67BC852123C70EEF8D53774FA635F (void);
// 0x00000B4F System.String System.Collections.Specialized.NameValueCollection::Get(System.Int32)
extern void NameValueCollection_Get_m2C55CAA38BF0F6677AE8F44C22C289A5B59B4B20 (void);
// 0x00000B50 System.String System.Collections.Specialized.NameValueCollection::GetKey(System.Int32)
extern void NameValueCollection_GetKey_m80CFEB6D61F9C3B9D03D7B25457D8B122C15170F (void);
// 0x00000B51 System.Void System.Collections.Specialized.NameValueCollection::.ctor(System.DBNull)
extern void NameValueCollection__ctor_mF894B0B49D20CE92F0CE3BBF8ED67F48E61CDB6A (void);
// 0x00000B52 System.String System.Collections.Specialized.StringCollection::get_Item(System.Int32)
extern void StringCollection_get_Item_m4C46EE5894C7A604638AE2ABF4530C201B1FB88E (void);
// 0x00000B53 System.Void System.Collections.Specialized.StringCollection::set_Item(System.Int32,System.String)
extern void StringCollection_set_Item_mA1AFC86270CD52B6F5017555AD22CF7710D6B425 (void);
// 0x00000B54 System.Int32 System.Collections.Specialized.StringCollection::get_Count()
extern void StringCollection_get_Count_mC6D18D20EEE559A67C72BD37ECA9C0FB93283F3F (void);
// 0x00000B55 System.Boolean System.Collections.Specialized.StringCollection::System.Collections.IList.get_IsReadOnly()
extern void StringCollection_System_Collections_IList_get_IsReadOnly_m4558520882327B88C80FE911E956E0F082872DED (void);
// 0x00000B56 System.Boolean System.Collections.Specialized.StringCollection::System.Collections.IList.get_IsFixedSize()
extern void StringCollection_System_Collections_IList_get_IsFixedSize_m821D69C64C5E4DE6841D4CC95FFC330E63F4D672 (void);
// 0x00000B57 System.Int32 System.Collections.Specialized.StringCollection::Add(System.String)
extern void StringCollection_Add_m03BBFAA1D11499DA68474A73F5DB55460F0D6DAF (void);
// 0x00000B58 System.Void System.Collections.Specialized.StringCollection::Clear()
extern void StringCollection_Clear_m2B917BE0CCA6344B1FD65474C53CE512F0476435 (void);
// 0x00000B59 System.Boolean System.Collections.Specialized.StringCollection::Contains(System.String)
extern void StringCollection_Contains_mB9DCBDDFD850E68D6080D2D7657F399BA6002B10 (void);
// 0x00000B5A System.Void System.Collections.Specialized.StringCollection::CopyTo(System.String[],System.Int32)
extern void StringCollection_CopyTo_mCD7A68178A8F87AC0628B2E92CE466A27512CB05 (void);
// 0x00000B5B System.Int32 System.Collections.Specialized.StringCollection::IndexOf(System.String)
extern void StringCollection_IndexOf_m85C4CA19319F830D14472258D9A3DFD31F0F6BDF (void);
// 0x00000B5C System.Void System.Collections.Specialized.StringCollection::Insert(System.Int32,System.String)
extern void StringCollection_Insert_mB3044A3F3B5C3B364DCEED9058895D978A4410CE (void);
// 0x00000B5D System.Void System.Collections.Specialized.StringCollection::Remove(System.String)
extern void StringCollection_Remove_mC6996622434EE5F503A70F425F0DE6E8D60399DC (void);
// 0x00000B5E System.Void System.Collections.Specialized.StringCollection::RemoveAt(System.Int32)
extern void StringCollection_RemoveAt_m4D0E6E6AA354A0AFFB76AEB56B4CFF24717BB53A (void);
// 0x00000B5F System.Object System.Collections.Specialized.StringCollection::get_SyncRoot()
extern void StringCollection_get_SyncRoot_mC1F8C1DA3C5ED059D89BD483FB1A4A0A31053F86 (void);
// 0x00000B60 System.Object System.Collections.Specialized.StringCollection::System.Collections.IList.get_Item(System.Int32)
extern void StringCollection_System_Collections_IList_get_Item_m749070FBB55E7C9C0C21FE4C1013D154D4635BB2 (void);
// 0x00000B61 System.Void System.Collections.Specialized.StringCollection::System.Collections.IList.set_Item(System.Int32,System.Object)
extern void StringCollection_System_Collections_IList_set_Item_m176A74C64AA22AFEA5808E4C81AA3FB46B0F9F5E (void);
// 0x00000B62 System.Int32 System.Collections.Specialized.StringCollection::System.Collections.IList.Add(System.Object)
extern void StringCollection_System_Collections_IList_Add_m4A71788F0E3068533B66D918B4F29762980052B5 (void);
// 0x00000B63 System.Boolean System.Collections.Specialized.StringCollection::System.Collections.IList.Contains(System.Object)
extern void StringCollection_System_Collections_IList_Contains_mEB0510524C7FD5886FFF9A0F710CFC04BACAFEF7 (void);
// 0x00000B64 System.Int32 System.Collections.Specialized.StringCollection::System.Collections.IList.IndexOf(System.Object)
extern void StringCollection_System_Collections_IList_IndexOf_mFF76DA01812E585BDD7B25ED4453C22BE0FE9A25 (void);
// 0x00000B65 System.Void System.Collections.Specialized.StringCollection::System.Collections.IList.Insert(System.Int32,System.Object)
extern void StringCollection_System_Collections_IList_Insert_m3B0EB6DA5B02CCA0AB270A329B83152592E6D8B1 (void);
// 0x00000B66 System.Void System.Collections.Specialized.StringCollection::System.Collections.IList.Remove(System.Object)
extern void StringCollection_System_Collections_IList_Remove_m042058DA970F09FC6A0465F09F3082FB2B71BA45 (void);
// 0x00000B67 System.Void System.Collections.Specialized.StringCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern void StringCollection_System_Collections_ICollection_CopyTo_m32503816BE4347138894130BD0AD6488643D6AB9 (void);
// 0x00000B68 System.Collections.IEnumerator System.Collections.Specialized.StringCollection::System.Collections.IEnumerable.GetEnumerator()
extern void StringCollection_System_Collections_IEnumerable_GetEnumerator_mF4227CFA7634BA8BFF9F7D25079538A77BD060E1 (void);
// 0x00000B69 System.Void System.Collections.Specialized.StringCollection::.ctor()
extern void StringCollection__ctor_m3EF93D74FEC69C5A0CD7A502539B2CEDD8D02954 (void);
// 0x00000B6A System.Void System.Collections.Specialized.NameObjectCollectionBase::.ctor()
extern void NameObjectCollectionBase__ctor_m6E0DCD48B817A59D0156390A09CD50BE07BF145D (void);
// 0x00000B6B System.Void System.Collections.Specialized.NameObjectCollectionBase::.ctor(System.Collections.IEqualityComparer)
extern void NameObjectCollectionBase__ctor_mB5F581BB65F493F603EE57E1815389E1406D5B61 (void);
// 0x00000B6C System.Void System.Collections.Specialized.NameObjectCollectionBase::.ctor(System.Int32,System.Collections.IEqualityComparer)
extern void NameObjectCollectionBase__ctor_m6B980F9EB155B69FB4C50AD38C2C0C104FE2F261 (void);
// 0x00000B6D System.Void System.Collections.Specialized.NameObjectCollectionBase::.ctor(System.DBNull)
extern void NameObjectCollectionBase__ctor_m113E50DAB246F05DB6302516E683D458250672DB (void);
// 0x00000B6E System.Void System.Collections.Specialized.NameObjectCollectionBase::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void NameObjectCollectionBase__ctor_m7E8E8B56138D44BECF32865A11DAF942AE2BA4DA (void);
// 0x00000B6F System.Void System.Collections.Specialized.NameObjectCollectionBase::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void NameObjectCollectionBase_GetObjectData_m0675A0B0EC79E1DAD6E307764C899EC5E552B32A (void);
// 0x00000B70 System.Void System.Collections.Specialized.NameObjectCollectionBase::OnDeserialization(System.Object)
extern void NameObjectCollectionBase_OnDeserialization_mFF0F15B0E4DB6D9B6043E2B338160B321889C4B6 (void);
// 0x00000B71 System.Void System.Collections.Specialized.NameObjectCollectionBase::Reset()
extern void NameObjectCollectionBase_Reset_m107877B3D64D818F9D4E0936DA9627C4D94DF6DC (void);
// 0x00000B72 System.Void System.Collections.Specialized.NameObjectCollectionBase::Reset(System.Int32)
extern void NameObjectCollectionBase_Reset_mFF34A1D69D7F9BECD9948E748769623EFA50C0D3 (void);
// 0x00000B73 System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry System.Collections.Specialized.NameObjectCollectionBase::FindEntry(System.String)
extern void NameObjectCollectionBase_FindEntry_m1BC4B6ACCE244B544B216D16DB53A769E670876F (void);
// 0x00000B74 System.Boolean System.Collections.Specialized.NameObjectCollectionBase::get_IsReadOnly()
extern void NameObjectCollectionBase_get_IsReadOnly_m1B1072E33B3B42507F822EBB61F75A804A1D503A (void);
// 0x00000B75 System.Void System.Collections.Specialized.NameObjectCollectionBase::BaseAdd(System.String,System.Object)
extern void NameObjectCollectionBase_BaseAdd_m1A8E236F037A35B3FBC7435A120B2E66EE7F563E (void);
// 0x00000B76 System.Object System.Collections.Specialized.NameObjectCollectionBase::BaseGet(System.String)
extern void NameObjectCollectionBase_BaseGet_mA6FA3CE608E74CF1E4C71227EA95F15C0D2BB154 (void);
// 0x00000B77 System.Object System.Collections.Specialized.NameObjectCollectionBase::BaseGet(System.Int32)
extern void NameObjectCollectionBase_BaseGet_mE892E8066502D48BEA2E9760D2BE31FB40B8DEAA (void);
// 0x00000B78 System.String System.Collections.Specialized.NameObjectCollectionBase::BaseGetKey(System.Int32)
extern void NameObjectCollectionBase_BaseGetKey_m4435B786286C5946CFA38206FFE20E33F32A78A6 (void);
// 0x00000B79 System.Collections.IEnumerator System.Collections.Specialized.NameObjectCollectionBase::GetEnumerator()
extern void NameObjectCollectionBase_GetEnumerator_mDEEC7FF01AEF8FB0739964513BAF10C4C3F254BC (void);
// 0x00000B7A System.Int32 System.Collections.Specialized.NameObjectCollectionBase::get_Count()
extern void NameObjectCollectionBase_get_Count_m766C6702F64CD224AB123566BAACB2A81FCBF5F0 (void);
// 0x00000B7B System.Void System.Collections.Specialized.NameObjectCollectionBase::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern void NameObjectCollectionBase_System_Collections_ICollection_CopyTo_mF47F5F43F47C553B071EB7A6AA4A352D95B02E01 (void);
// 0x00000B7C System.Object System.Collections.Specialized.NameObjectCollectionBase::System.Collections.ICollection.get_SyncRoot()
extern void NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_mAC6E9FEF3C7C442481772E568C4FBF552B908624 (void);
// 0x00000B7D System.Void System.Collections.Specialized.NameObjectCollectionBase::.cctor()
extern void NameObjectCollectionBase__cctor_m4D457522AA1259C7AB96A98558FC6B0C38CEEAED (void);
// 0x00000B7E System.Void System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry::.ctor(System.String,System.Object)
extern void NameObjectEntry__ctor_m36EA15EE18936FA78B2E4E29C63311E03221F513 (void);
// 0x00000B7F System.Void System.Collections.Specialized.NameObjectCollectionBase/NameObjectKeysEnumerator::.ctor(System.Collections.Specialized.NameObjectCollectionBase)
extern void NameObjectKeysEnumerator__ctor_mF3026FAD0A80AADFBC11888DCB0FDF7C2DD71801 (void);
// 0x00000B80 System.Boolean System.Collections.Specialized.NameObjectCollectionBase/NameObjectKeysEnumerator::MoveNext()
extern void NameObjectKeysEnumerator_MoveNext_m8800474BD1E5F24E4B88D1E33ABA793CB05DD540 (void);
// 0x00000B81 System.Void System.Collections.Specialized.NameObjectCollectionBase/NameObjectKeysEnumerator::Reset()
extern void NameObjectKeysEnumerator_Reset_m548902A21282FB0766D833231BE700069DA316A5 (void);
// 0x00000B82 System.Object System.Collections.Specialized.NameObjectCollectionBase/NameObjectKeysEnumerator::get_Current()
extern void NameObjectKeysEnumerator_get_Current_m23AC7A2525995AC67EF4002996653748F33F6096 (void);
// 0x00000B83 System.Void System.Collections.Specialized.CompatibleComparer::.ctor(System.Collections.IComparer,System.Collections.IHashCodeProvider)
extern void CompatibleComparer__ctor_m9F601617145FDFCA25983415C4427C47E5C170A7 (void);
// 0x00000B84 System.Boolean System.Collections.Specialized.CompatibleComparer::Equals(System.Object,System.Object)
extern void CompatibleComparer_Equals_m09CF7F036688C2E00C8BF5618C0528BA8D8C40D5 (void);
// 0x00000B85 System.Int32 System.Collections.Specialized.CompatibleComparer::GetHashCode(System.Object)
extern void CompatibleComparer_GetHashCode_mE88BB0E4CC1A615946A43A8DE75B6F1F57B1CDAF (void);
// 0x00000B86 System.Collections.IComparer System.Collections.Specialized.CompatibleComparer::get_Comparer()
extern void CompatibleComparer_get_Comparer_mFA7D1FF54540B95FED7C58DBCDB7ACC037FD3DF9 (void);
// 0x00000B87 System.Collections.IHashCodeProvider System.Collections.Specialized.CompatibleComparer::get_HashCodeProvider()
extern void CompatibleComparer_get_HashCodeProvider_m1085836D796353C24686CF56026EB89D783BBFC4 (void);
// 0x00000B88 System.Collections.IComparer System.Collections.Specialized.CompatibleComparer::get_DefaultComparer()
extern void CompatibleComparer_get_DefaultComparer_m9CC25AF4BD1553D2B2C51FDE9AC8110BABB5C5D5 (void);
// 0x00000B89 System.Collections.IHashCodeProvider System.Collections.Specialized.CompatibleComparer::get_DefaultHashCodeProvider()
extern void CompatibleComparer_get_DefaultHashCodeProvider_m292C48BD6A5A7065925BAF2BFDEBF7217516DC50 (void);
// 0x00000B8A System.Void System.Collections.Generic.ValueListBuilder`1::.ctor(System.Span`1<T>)
// 0x00000B8B System.Int32 System.Collections.Generic.ValueListBuilder`1::get_Length()
// 0x00000B8C T& System.Collections.Generic.ValueListBuilder`1::get_Item(System.Int32)
// 0x00000B8D System.Void System.Collections.Generic.ValueListBuilder`1::Append(T)
// 0x00000B8E System.ReadOnlySpan`1<T> System.Collections.Generic.ValueListBuilder`1::AsSpan()
// 0x00000B8F System.Void System.Collections.Generic.ValueListBuilder`1::Dispose()
// 0x00000B90 System.Void System.Collections.Generic.ValueListBuilder`1::Grow()
// 0x00000B91 T System.Collections.Generic.ValueListBuilder`1::Pop()
// 0x00000B92 System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32*,System.Int32)
extern void BitHelper__ctor_mD4A0F4FB490C57EA8B7C904941BC72AD14697A08 (void);
// 0x00000B93 System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32[],System.Int32)
extern void BitHelper__ctor_m67B3260E6A92B90D770D8CF043A848A477DC4595 (void);
// 0x00000B94 System.Void System.Collections.Generic.BitHelper::MarkBit(System.Int32)
extern void BitHelper_MarkBit_m337CC205369FD7A7E1A199B7210DFFC98AB6C1AA (void);
// 0x00000B95 System.Boolean System.Collections.Generic.BitHelper::IsMarked(System.Int32)
extern void BitHelper_IsMarked_mB9298BD21F493D475C1D71EEE439D99611B4D01C (void);
// 0x00000B96 System.Int32 System.Collections.Generic.BitHelper::ToIntArrayLength(System.Int32)
extern void BitHelper_ToIntArrayLength_m6F27FE06169E9B8829561FC41BB8C81F6A9D0F41 (void);
// 0x00000B97 System.Void System.Collections.Generic.LinkedList`1::.ctor()
// 0x00000B98 System.Void System.Collections.Generic.LinkedList`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000B99 System.Int32 System.Collections.Generic.LinkedList`1::get_Count()
// 0x00000B9A System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::get_First()
// 0x00000B9B System.Boolean System.Collections.Generic.LinkedList`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000B9C System.Void System.Collections.Generic.LinkedList`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000B9D System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::AddLast(T)
// 0x00000B9E System.Void System.Collections.Generic.LinkedList`1::Clear()
// 0x00000B9F System.Boolean System.Collections.Generic.LinkedList`1::Contains(T)
// 0x00000BA0 System.Void System.Collections.Generic.LinkedList`1::CopyTo(T[],System.Int32)
// 0x00000BA1 System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::Find(T)
// 0x00000BA2 System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1::GetEnumerator()
// 0x00000BA3 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000BA4 System.Boolean System.Collections.Generic.LinkedList`1::Remove(T)
// 0x00000BA5 System.Void System.Collections.Generic.LinkedList`1::Remove(System.Collections.Generic.LinkedListNode`1<T>)
// 0x00000BA6 System.Void System.Collections.Generic.LinkedList`1::RemoveFirst()
// 0x00000BA7 System.Void System.Collections.Generic.LinkedList`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000BA8 System.Void System.Collections.Generic.LinkedList`1::OnDeserialization(System.Object)
// 0x00000BA9 System.Void System.Collections.Generic.LinkedList`1::InternalInsertNodeBefore(System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
// 0x00000BAA System.Void System.Collections.Generic.LinkedList`1::InternalInsertNodeToEmptyList(System.Collections.Generic.LinkedListNode`1<T>)
// 0x00000BAB System.Void System.Collections.Generic.LinkedList`1::InternalRemoveNode(System.Collections.Generic.LinkedListNode`1<T>)
// 0x00000BAC System.Void System.Collections.Generic.LinkedList`1::ValidateNode(System.Collections.Generic.LinkedListNode`1<T>)
// 0x00000BAD System.Object System.Collections.Generic.LinkedList`1::System.Collections.ICollection.get_SyncRoot()
// 0x00000BAE System.Void System.Collections.Generic.LinkedList`1::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000BAF System.Collections.IEnumerator System.Collections.Generic.LinkedList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000BB0 System.Void System.Collections.Generic.LinkedList`1/Enumerator::.ctor(System.Collections.Generic.LinkedList`1<T>)
// 0x00000BB1 System.Void System.Collections.Generic.LinkedList`1/Enumerator::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000BB2 T System.Collections.Generic.LinkedList`1/Enumerator::get_Current()
// 0x00000BB3 System.Object System.Collections.Generic.LinkedList`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000BB4 System.Boolean System.Collections.Generic.LinkedList`1/Enumerator::MoveNext()
// 0x00000BB5 System.Void System.Collections.Generic.LinkedList`1/Enumerator::System.Collections.IEnumerator.Reset()
// 0x00000BB6 System.Void System.Collections.Generic.LinkedList`1/Enumerator::Dispose()
// 0x00000BB7 System.Void System.Collections.Generic.LinkedList`1/Enumerator::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000BB8 System.Void System.Collections.Generic.LinkedList`1/Enumerator::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
// 0x00000BB9 System.Void System.Collections.Generic.LinkedListNode`1::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
// 0x00000BBA System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::get_Next()
// 0x00000BBB T System.Collections.Generic.LinkedListNode`1::get_Value()
// 0x00000BBC System.Void System.Collections.Generic.LinkedListNode`1::Invalidate()
// 0x00000BBD System.Void System.Collections.Generic.SortedList`2::.ctor()
// 0x00000BBE System.Void System.Collections.Generic.SortedList`2::Add(TKey,TValue)
// 0x00000BBF System.Void System.Collections.Generic.SortedList`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000BC0 System.Boolean System.Collections.Generic.SortedList`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000BC1 System.Boolean System.Collections.Generic.SortedList`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000BC2 System.Void System.Collections.Generic.SortedList`2::set_Capacity(System.Int32)
// 0x00000BC3 System.Int32 System.Collections.Generic.SortedList`2::get_Count()
// 0x00000BC4 System.Collections.Generic.IList`1<TKey> System.Collections.Generic.SortedList`2::get_Keys()
// 0x00000BC5 System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.SortedList`2::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
// 0x00000BC6 System.Collections.Generic.IEnumerable`1<TKey> System.Collections.Generic.SortedList`2::System.Collections.Generic.IReadOnlyDictionary<TKey,TValue>.get_Keys()
// 0x00000BC7 System.Collections.Generic.IList`1<TValue> System.Collections.Generic.SortedList`2::get_Values()
// 0x00000BC8 System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.SortedList`2::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
// 0x00000BC9 System.Collections.Generic.IEnumerable`1<TValue> System.Collections.Generic.SortedList`2::System.Collections.Generic.IReadOnlyDictionary<TKey,TValue>.get_Values()
// 0x00000BCA System.Collections.Generic.SortedList`2/KeyList<TKey,TValue> System.Collections.Generic.SortedList`2::GetKeyListHelper()
// 0x00000BCB System.Collections.Generic.SortedList`2/ValueList<TKey,TValue> System.Collections.Generic.SortedList`2::GetValueListHelper()
// 0x00000BCC System.Boolean System.Collections.Generic.SortedList`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
// 0x00000BCD System.Object System.Collections.Generic.SortedList`2::System.Collections.ICollection.get_SyncRoot()
// 0x00000BCE System.Void System.Collections.Generic.SortedList`2::Clear()
// 0x00000BCF System.Boolean System.Collections.Generic.SortedList`2::System.Collections.IDictionary.Contains(System.Object)
// 0x00000BD0 System.Boolean System.Collections.Generic.SortedList`2::ContainsKey(TKey)
// 0x00000BD1 System.Boolean System.Collections.Generic.SortedList`2::ContainsValue(TValue)
// 0x00000BD2 System.Void System.Collections.Generic.SortedList`2::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
// 0x00000BD3 System.Void System.Collections.Generic.SortedList`2::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000BD4 System.Void System.Collections.Generic.SortedList`2::EnsureCapacity(System.Int32)
// 0x00000BD5 TValue System.Collections.Generic.SortedList`2::GetByIndex(System.Int32)
// 0x00000BD6 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2::GetEnumerator()
// 0x00000BD7 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
// 0x00000BD8 System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedList`2::System.Collections.IDictionary.GetEnumerator()
// 0x00000BD9 System.Collections.IEnumerator System.Collections.Generic.SortedList`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000BDA TKey System.Collections.Generic.SortedList`2::GetKey(System.Int32)
// 0x00000BDB TValue System.Collections.Generic.SortedList`2::get_Item(TKey)
// 0x00000BDC System.Void System.Collections.Generic.SortedList`2::set_Item(TKey,TValue)
// 0x00000BDD System.Object System.Collections.Generic.SortedList`2::System.Collections.IDictionary.get_Item(System.Object)
// 0x00000BDE System.Void System.Collections.Generic.SortedList`2::System.Collections.IDictionary.set_Item(System.Object,System.Object)
// 0x00000BDF System.Int32 System.Collections.Generic.SortedList`2::IndexOfKey(TKey)
// 0x00000BE0 System.Int32 System.Collections.Generic.SortedList`2::IndexOfValue(TValue)
// 0x00000BE1 System.Void System.Collections.Generic.SortedList`2::Insert(System.Int32,TKey,TValue)
// 0x00000BE2 System.Boolean System.Collections.Generic.SortedList`2::TryGetValue(TKey,TValue&)
// 0x00000BE3 System.Void System.Collections.Generic.SortedList`2::RemoveAt(System.Int32)
// 0x00000BE4 System.Boolean System.Collections.Generic.SortedList`2::Remove(TKey)
// 0x00000BE5 System.Boolean System.Collections.Generic.SortedList`2::IsCompatibleKey(System.Object)
// 0x00000BE6 System.Void System.Collections.Generic.SortedList`2/Enumerator::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>,System.Int32)
// 0x00000BE7 System.Void System.Collections.Generic.SortedList`2/Enumerator::Dispose()
// 0x00000BE8 System.Object System.Collections.Generic.SortedList`2/Enumerator::System.Collections.IDictionaryEnumerator.get_Key()
// 0x00000BE9 System.Boolean System.Collections.Generic.SortedList`2/Enumerator::MoveNext()
// 0x00000BEA System.Collections.DictionaryEntry System.Collections.Generic.SortedList`2/Enumerator::System.Collections.IDictionaryEnumerator.get_Entry()
// 0x00000BEB System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/Enumerator::get_Current()
// 0x00000BEC System.Object System.Collections.Generic.SortedList`2/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000BED System.Object System.Collections.Generic.SortedList`2/Enumerator::System.Collections.IDictionaryEnumerator.get_Value()
// 0x00000BEE System.Void System.Collections.Generic.SortedList`2/Enumerator::System.Collections.IEnumerator.Reset()
// 0x00000BEF System.Void System.Collections.Generic.SortedList`2/SortedListKeyEnumerator::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
// 0x00000BF0 System.Void System.Collections.Generic.SortedList`2/SortedListKeyEnumerator::Dispose()
// 0x00000BF1 System.Boolean System.Collections.Generic.SortedList`2/SortedListKeyEnumerator::MoveNext()
// 0x00000BF2 TKey System.Collections.Generic.SortedList`2/SortedListKeyEnumerator::get_Current()
// 0x00000BF3 System.Object System.Collections.Generic.SortedList`2/SortedListKeyEnumerator::System.Collections.IEnumerator.get_Current()
// 0x00000BF4 System.Void System.Collections.Generic.SortedList`2/SortedListKeyEnumerator::System.Collections.IEnumerator.Reset()
// 0x00000BF5 System.Void System.Collections.Generic.SortedList`2/SortedListValueEnumerator::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
// 0x00000BF6 System.Void System.Collections.Generic.SortedList`2/SortedListValueEnumerator::Dispose()
// 0x00000BF7 System.Boolean System.Collections.Generic.SortedList`2/SortedListValueEnumerator::MoveNext()
// 0x00000BF8 TValue System.Collections.Generic.SortedList`2/SortedListValueEnumerator::get_Current()
// 0x00000BF9 System.Object System.Collections.Generic.SortedList`2/SortedListValueEnumerator::System.Collections.IEnumerator.get_Current()
// 0x00000BFA System.Void System.Collections.Generic.SortedList`2/SortedListValueEnumerator::System.Collections.IEnumerator.Reset()
// 0x00000BFB System.Void System.Collections.Generic.SortedList`2/KeyList::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
// 0x00000BFC System.Int32 System.Collections.Generic.SortedList`2/KeyList::get_Count()
// 0x00000BFD System.Boolean System.Collections.Generic.SortedList`2/KeyList::get_IsReadOnly()
// 0x00000BFE System.Object System.Collections.Generic.SortedList`2/KeyList::System.Collections.ICollection.get_SyncRoot()
// 0x00000BFF System.Void System.Collections.Generic.SortedList`2/KeyList::Add(TKey)
// 0x00000C00 System.Void System.Collections.Generic.SortedList`2/KeyList::Clear()
// 0x00000C01 System.Boolean System.Collections.Generic.SortedList`2/KeyList::Contains(TKey)
// 0x00000C02 System.Void System.Collections.Generic.SortedList`2/KeyList::CopyTo(TKey[],System.Int32)
// 0x00000C03 System.Void System.Collections.Generic.SortedList`2/KeyList::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000C04 System.Void System.Collections.Generic.SortedList`2/KeyList::Insert(System.Int32,TKey)
// 0x00000C05 TKey System.Collections.Generic.SortedList`2/KeyList::get_Item(System.Int32)
// 0x00000C06 System.Void System.Collections.Generic.SortedList`2/KeyList::set_Item(System.Int32,TKey)
// 0x00000C07 System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedList`2/KeyList::GetEnumerator()
// 0x00000C08 System.Collections.IEnumerator System.Collections.Generic.SortedList`2/KeyList::System.Collections.IEnumerable.GetEnumerator()
// 0x00000C09 System.Int32 System.Collections.Generic.SortedList`2/KeyList::IndexOf(TKey)
// 0x00000C0A System.Boolean System.Collections.Generic.SortedList`2/KeyList::Remove(TKey)
// 0x00000C0B System.Void System.Collections.Generic.SortedList`2/KeyList::RemoveAt(System.Int32)
// 0x00000C0C System.Void System.Collections.Generic.SortedList`2/ValueList::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
// 0x00000C0D System.Int32 System.Collections.Generic.SortedList`2/ValueList::get_Count()
// 0x00000C0E System.Boolean System.Collections.Generic.SortedList`2/ValueList::get_IsReadOnly()
// 0x00000C0F System.Object System.Collections.Generic.SortedList`2/ValueList::System.Collections.ICollection.get_SyncRoot()
// 0x00000C10 System.Void System.Collections.Generic.SortedList`2/ValueList::Add(TValue)
// 0x00000C11 System.Void System.Collections.Generic.SortedList`2/ValueList::Clear()
// 0x00000C12 System.Boolean System.Collections.Generic.SortedList`2/ValueList::Contains(TValue)
// 0x00000C13 System.Void System.Collections.Generic.SortedList`2/ValueList::CopyTo(TValue[],System.Int32)
// 0x00000C14 System.Void System.Collections.Generic.SortedList`2/ValueList::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000C15 System.Void System.Collections.Generic.SortedList`2/ValueList::Insert(System.Int32,TValue)
// 0x00000C16 TValue System.Collections.Generic.SortedList`2/ValueList::get_Item(System.Int32)
// 0x00000C17 System.Void System.Collections.Generic.SortedList`2/ValueList::set_Item(System.Int32,TValue)
// 0x00000C18 System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.SortedList`2/ValueList::GetEnumerator()
// 0x00000C19 System.Collections.IEnumerator System.Collections.Generic.SortedList`2/ValueList::System.Collections.IEnumerable.GetEnumerator()
// 0x00000C1A System.Int32 System.Collections.Generic.SortedList`2/ValueList::IndexOf(TValue)
// 0x00000C1B System.Boolean System.Collections.Generic.SortedList`2/ValueList::Remove(TValue)
// 0x00000C1C System.Void System.Collections.Generic.SortedList`2/ValueList::RemoveAt(System.Int32)
// 0x00000C1D System.Void System.Collections.Generic.SortedSet`1::.ctor()
// 0x00000C1E System.Void System.Collections.Generic.SortedSet`1::.ctor(System.Collections.Generic.IComparer`1<T>)
// 0x00000C1F System.Void System.Collections.Generic.SortedSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000C20 System.Boolean System.Collections.Generic.SortedSet`1::ContainsAllElements(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C21 System.Boolean System.Collections.Generic.SortedSet`1::InOrderTreeWalk(System.Collections.Generic.TreeWalkPredicate`1<T>)
// 0x00000C22 System.Boolean System.Collections.Generic.SortedSet`1::BreadthFirstTreeWalk(System.Collections.Generic.TreeWalkPredicate`1<T>)
// 0x00000C23 System.Int32 System.Collections.Generic.SortedSet`1::get_Count()
// 0x00000C24 System.Collections.Generic.IComparer`1<T> System.Collections.Generic.SortedSet`1::get_Comparer()
// 0x00000C25 System.Boolean System.Collections.Generic.SortedSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000C26 System.Object System.Collections.Generic.SortedSet`1::System.Collections.ICollection.get_SyncRoot()
// 0x00000C27 System.Void System.Collections.Generic.SortedSet`1::VersionCheck()
// 0x00000C28 System.Boolean System.Collections.Generic.SortedSet`1::IsWithinRange(T)
// 0x00000C29 System.Boolean System.Collections.Generic.SortedSet`1::Add(T)
// 0x00000C2A System.Void System.Collections.Generic.SortedSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000C2B System.Boolean System.Collections.Generic.SortedSet`1::AddIfNotPresent(T)
// 0x00000C2C System.Boolean System.Collections.Generic.SortedSet`1::Remove(T)
// 0x00000C2D System.Boolean System.Collections.Generic.SortedSet`1::DoRemove(T)
// 0x00000C2E System.Void System.Collections.Generic.SortedSet`1::Clear()
// 0x00000C2F System.Boolean System.Collections.Generic.SortedSet`1::Contains(T)
// 0x00000C30 System.Void System.Collections.Generic.SortedSet`1::CopyTo(T[],System.Int32)
// 0x00000C31 System.Void System.Collections.Generic.SortedSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000C32 System.Void System.Collections.Generic.SortedSet`1::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000C33 System.Collections.Generic.SortedSet`1/Enumerator<T> System.Collections.Generic.SortedSet`1::GetEnumerator()
// 0x00000C34 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.SortedSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000C35 System.Collections.IEnumerator System.Collections.Generic.SortedSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000C36 System.Void System.Collections.Generic.SortedSet`1::InsertionBalance(System.Collections.Generic.SortedSet`1/Node<T>,System.Collections.Generic.SortedSet`1/Node<T>&,System.Collections.Generic.SortedSet`1/Node<T>,System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C37 System.Void System.Collections.Generic.SortedSet`1::ReplaceChildOrRoot(System.Collections.Generic.SortedSet`1/Node<T>,System.Collections.Generic.SortedSet`1/Node<T>,System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C38 System.Void System.Collections.Generic.SortedSet`1::ReplaceNode(System.Collections.Generic.SortedSet`1/Node<T>,System.Collections.Generic.SortedSet`1/Node<T>,System.Collections.Generic.SortedSet`1/Node<T>,System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C39 System.Collections.Generic.SortedSet`1/Node<T> System.Collections.Generic.SortedSet`1::FindNode(T)
// 0x00000C3A System.Int32 System.Collections.Generic.SortedSet`1::InternalIndexOf(T)
// 0x00000C3B System.Collections.Generic.SortedSet`1/Node<T> System.Collections.Generic.SortedSet`1::FindRange(T,T,System.Boolean,System.Boolean)
// 0x00000C3C System.Boolean System.Collections.Generic.SortedSet`1::HasEqualComparer(System.Collections.Generic.SortedSet`1<T>)
// 0x00000C3D System.Boolean System.Collections.Generic.SortedSet`1::IsSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C3E System.Boolean System.Collections.Generic.SortedSet`1::IsSubsetOfSortedSetWithSameComparer(System.Collections.Generic.SortedSet`1<T>)
// 0x00000C3F System.Boolean System.Collections.Generic.SortedSet`1::IsProperSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C40 System.Boolean System.Collections.Generic.SortedSet`1::IsSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C41 System.Boolean System.Collections.Generic.SortedSet`1::IsProperSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C42 System.Boolean System.Collections.Generic.SortedSet`1::SetEquals(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C43 System.Boolean System.Collections.Generic.SortedSet`1::Overlaps(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C44 System.Collections.Generic.SortedSet`1/ElementCount<T> System.Collections.Generic.SortedSet`1::CheckUniqueAndUnfoundElements(System.Collections.Generic.IEnumerable`1<T>,System.Boolean)
// 0x00000C45 T System.Collections.Generic.SortedSet`1::get_Min()
// 0x00000C46 T System.Collections.Generic.SortedSet`1::get_MinInternal()
// 0x00000C47 T System.Collections.Generic.SortedSet`1::get_Max()
// 0x00000C48 T System.Collections.Generic.SortedSet`1::get_MaxInternal()
// 0x00000C49 System.Collections.Generic.SortedSet`1<T> System.Collections.Generic.SortedSet`1::GetViewBetween(T,T)
// 0x00000C4A System.Void System.Collections.Generic.SortedSet`1::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000C4B System.Void System.Collections.Generic.SortedSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000C4C System.Void System.Collections.Generic.SortedSet`1::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
// 0x00000C4D System.Void System.Collections.Generic.SortedSet`1::OnDeserialization(System.Object)
// 0x00000C4E System.Int32 System.Collections.Generic.SortedSet`1::Log2(System.Int32)
// 0x00000C4F System.Void System.Collections.Generic.SortedSet`1/TreeSubSet::.ctor(System.Collections.Generic.SortedSet`1<T>,T,T,System.Boolean,System.Boolean)
// 0x00000C50 System.Boolean System.Collections.Generic.SortedSet`1/TreeSubSet::AddIfNotPresent(T)
// 0x00000C51 System.Boolean System.Collections.Generic.SortedSet`1/TreeSubSet::Contains(T)
// 0x00000C52 System.Boolean System.Collections.Generic.SortedSet`1/TreeSubSet::DoRemove(T)
// 0x00000C53 System.Void System.Collections.Generic.SortedSet`1/TreeSubSet::Clear()
// 0x00000C54 System.Boolean System.Collections.Generic.SortedSet`1/TreeSubSet::IsWithinRange(T)
// 0x00000C55 T System.Collections.Generic.SortedSet`1/TreeSubSet::get_MinInternal()
// 0x00000C56 T System.Collections.Generic.SortedSet`1/TreeSubSet::get_MaxInternal()
// 0x00000C57 System.Boolean System.Collections.Generic.SortedSet`1/TreeSubSet::InOrderTreeWalk(System.Collections.Generic.TreeWalkPredicate`1<T>)
// 0x00000C58 System.Boolean System.Collections.Generic.SortedSet`1/TreeSubSet::BreadthFirstTreeWalk(System.Collections.Generic.TreeWalkPredicate`1<T>)
// 0x00000C59 System.Collections.Generic.SortedSet`1/Node<T> System.Collections.Generic.SortedSet`1/TreeSubSet::FindNode(T)
// 0x00000C5A System.Int32 System.Collections.Generic.SortedSet`1/TreeSubSet::InternalIndexOf(T)
// 0x00000C5B System.Void System.Collections.Generic.SortedSet`1/TreeSubSet::VersionCheck()
// 0x00000C5C System.Void System.Collections.Generic.SortedSet`1/TreeSubSet::VersionCheckImpl()
// 0x00000C5D System.Collections.Generic.SortedSet`1<T> System.Collections.Generic.SortedSet`1/TreeSubSet::GetViewBetween(T,T)
// 0x00000C5E System.Void System.Collections.Generic.SortedSet`1/TreeSubSet::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000C5F System.Void System.Collections.Generic.SortedSet`1/TreeSubSet::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000C60 System.Void System.Collections.Generic.SortedSet`1/TreeSubSet::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
// 0x00000C61 System.Void System.Collections.Generic.SortedSet`1/TreeSubSet::OnDeserialization(System.Object)
// 0x00000C62 System.Boolean System.Collections.Generic.SortedSet`1/TreeSubSet::<VersionCheckImpl>b__20_0(System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C63 System.Void System.Collections.Generic.SortedSet`1/TreeSubSet/<>c__DisplayClass9_0::.ctor()
// 0x00000C64 System.Boolean System.Collections.Generic.SortedSet`1/TreeSubSet/<>c__DisplayClass9_0::<Clear>b__0(System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C65 System.Void System.Collections.Generic.SortedSet`1/Node::.ctor(T,System.Collections.Generic.NodeColor)
// 0x00000C66 System.Boolean System.Collections.Generic.SortedSet`1/Node::IsNonNullRed(System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C67 System.Boolean System.Collections.Generic.SortedSet`1/Node::IsNullOrBlack(System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C68 T System.Collections.Generic.SortedSet`1/Node::get_Item()
// 0x00000C69 System.Void System.Collections.Generic.SortedSet`1/Node::set_Item(T)
// 0x00000C6A System.Collections.Generic.SortedSet`1/Node<T> System.Collections.Generic.SortedSet`1/Node::get_Left()
// 0x00000C6B System.Void System.Collections.Generic.SortedSet`1/Node::set_Left(System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C6C System.Collections.Generic.SortedSet`1/Node<T> System.Collections.Generic.SortedSet`1/Node::get_Right()
// 0x00000C6D System.Void System.Collections.Generic.SortedSet`1/Node::set_Right(System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C6E System.Collections.Generic.NodeColor System.Collections.Generic.SortedSet`1/Node::get_Color()
// 0x00000C6F System.Void System.Collections.Generic.SortedSet`1/Node::set_Color(System.Collections.Generic.NodeColor)
// 0x00000C70 System.Boolean System.Collections.Generic.SortedSet`1/Node::get_IsBlack()
// 0x00000C71 System.Boolean System.Collections.Generic.SortedSet`1/Node::get_IsRed()
// 0x00000C72 System.Boolean System.Collections.Generic.SortedSet`1/Node::get_Is2Node()
// 0x00000C73 System.Boolean System.Collections.Generic.SortedSet`1/Node::get_Is4Node()
// 0x00000C74 System.Void System.Collections.Generic.SortedSet`1/Node::ColorBlack()
// 0x00000C75 System.Void System.Collections.Generic.SortedSet`1/Node::ColorRed()
// 0x00000C76 System.Collections.Generic.TreeRotation System.Collections.Generic.SortedSet`1/Node::GetRotation(System.Collections.Generic.SortedSet`1/Node<T>,System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C77 System.Collections.Generic.SortedSet`1/Node<T> System.Collections.Generic.SortedSet`1/Node::GetSibling(System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C78 System.Void System.Collections.Generic.SortedSet`1/Node::Split4Node()
// 0x00000C79 System.Collections.Generic.SortedSet`1/Node<T> System.Collections.Generic.SortedSet`1/Node::Rotate(System.Collections.Generic.TreeRotation)
// 0x00000C7A System.Collections.Generic.SortedSet`1/Node<T> System.Collections.Generic.SortedSet`1/Node::RotateLeft()
// 0x00000C7B System.Collections.Generic.SortedSet`1/Node<T> System.Collections.Generic.SortedSet`1/Node::RotateLeftRight()
// 0x00000C7C System.Collections.Generic.SortedSet`1/Node<T> System.Collections.Generic.SortedSet`1/Node::RotateRight()
// 0x00000C7D System.Collections.Generic.SortedSet`1/Node<T> System.Collections.Generic.SortedSet`1/Node::RotateRightLeft()
// 0x00000C7E System.Void System.Collections.Generic.SortedSet`1/Node::Merge2Nodes()
// 0x00000C7F System.Void System.Collections.Generic.SortedSet`1/Node::ReplaceChild(System.Collections.Generic.SortedSet`1/Node<T>,System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C80 System.Void System.Collections.Generic.SortedSet`1/Enumerator::.ctor(System.Collections.Generic.SortedSet`1<T>)
// 0x00000C81 System.Void System.Collections.Generic.SortedSet`1/Enumerator::.ctor(System.Collections.Generic.SortedSet`1<T>,System.Boolean)
// 0x00000C82 System.Void System.Collections.Generic.SortedSet`1/Enumerator::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000C83 System.Void System.Collections.Generic.SortedSet`1/Enumerator::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
// 0x00000C84 System.Void System.Collections.Generic.SortedSet`1/Enumerator::Initialize()
// 0x00000C85 System.Boolean System.Collections.Generic.SortedSet`1/Enumerator::MoveNext()
// 0x00000C86 System.Void System.Collections.Generic.SortedSet`1/Enumerator::Dispose()
// 0x00000C87 T System.Collections.Generic.SortedSet`1/Enumerator::get_Current()
// 0x00000C88 System.Object System.Collections.Generic.SortedSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000C89 System.Void System.Collections.Generic.SortedSet`1/Enumerator::Reset()
// 0x00000C8A System.Void System.Collections.Generic.SortedSet`1/Enumerator::System.Collections.IEnumerator.Reset()
// 0x00000C8B System.Void System.Collections.Generic.SortedSet`1/Enumerator::.cctor()
// 0x00000C8C System.Void System.Collections.Generic.SortedSet`1/<>c__DisplayClass52_0::.ctor()
// 0x00000C8D System.Boolean System.Collections.Generic.SortedSet`1/<>c__DisplayClass52_0::<CopyTo>b__0(System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C8E System.Void System.Collections.Generic.SortedSet`1/<>c__DisplayClass53_0::.ctor()
// 0x00000C8F System.Boolean System.Collections.Generic.SortedSet`1/<>c__DisplayClass53_0::<System.Collections.ICollection.CopyTo>b__0(System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C90 System.Void System.Collections.Generic.TreeWalkPredicate`1::.ctor(System.Object,System.IntPtr)
// 0x00000C91 System.Boolean System.Collections.Generic.TreeWalkPredicate`1::Invoke(System.Collections.Generic.SortedSet`1/Node<T>)
// 0x00000C92 System.Boolean System.Collections.Generic.ISet`1::Add(T)
// 0x00000C93 System.Boolean System.Collections.Generic.ISet`1::IsSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C94 System.Boolean System.Collections.Generic.ISet`1::IsSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C95 System.Boolean System.Collections.Generic.ISet`1::IsProperSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C96 System.Boolean System.Collections.Generic.ISet`1::IsProperSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C97 System.Boolean System.Collections.Generic.ISet`1::Overlaps(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C98 System.Boolean System.Collections.Generic.ISet`1::SetEquals(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000C99 System.Void System.IO.InvalidDataException::.ctor()
extern void InvalidDataException__ctor_mE85BD9741C8A2C6D03128821898A587169DA0C41 (void);
// 0x00000C9A System.Void System.IO.InvalidDataException::.ctor(System.String)
extern void InvalidDataException__ctor_m514879053D89E3C8229A01AA8970237FA5409532 (void);
// 0x00000C9B System.Void System.IO.InvalidDataException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void InvalidDataException__ctor_m076999CA4741F42158DAC11C78DDA17F00FCDE2B (void);
// 0x00000C9C System.Void System.IO.Compression.GZipStream::.ctor(System.IO.Stream,System.IO.Compression.CompressionMode)
extern void GZipStream__ctor_m25053634B041374B528EB0F01612B072A9AF2E05 (void);
// 0x00000C9D System.Void System.IO.Compression.GZipStream::.ctor(System.IO.Stream,System.IO.Compression.CompressionMode,System.Boolean)
extern void GZipStream__ctor_mFF3ECD9074856457C500AC6B4F80D833CCE487CF (void);
// 0x00000C9E System.Boolean System.IO.Compression.GZipStream::get_CanRead()
extern void GZipStream_get_CanRead_mFF4A941C8BD1D9C3A249A084C4335EFF8FAD47BB (void);
// 0x00000C9F System.Boolean System.IO.Compression.GZipStream::get_CanWrite()
extern void GZipStream_get_CanWrite_m0A3EC64CE819F978DE34A8477FAAA82889AE7248 (void);
// 0x00000CA0 System.Boolean System.IO.Compression.GZipStream::get_CanSeek()
extern void GZipStream_get_CanSeek_m530B997FC6D2144F7B73E708903E6E9F39E304DD (void);
// 0x00000CA1 System.Int64 System.IO.Compression.GZipStream::get_Length()
extern void GZipStream_get_Length_mCBA17CDD8925825AC2EB07E04CDE391B8E46C583 (void);
// 0x00000CA2 System.Int64 System.IO.Compression.GZipStream::get_Position()
extern void GZipStream_get_Position_m934BD5709034CAAEE05A6BCF382D1FEB0314C420 (void);
// 0x00000CA3 System.Void System.IO.Compression.GZipStream::set_Position(System.Int64)
extern void GZipStream_set_Position_m7FEEE9F3845C130340DC5E1DF984ACB2CD53E957 (void);
// 0x00000CA4 System.Void System.IO.Compression.GZipStream::Flush()
extern void GZipStream_Flush_m8D38670B3BE0472E83AA60F783A8EF3E1C1F01A0 (void);
// 0x00000CA5 System.Int64 System.IO.Compression.GZipStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void GZipStream_Seek_m72909F60549EAB072B86484A1E80BA15D4352E66 (void);
// 0x00000CA6 System.Void System.IO.Compression.GZipStream::SetLength(System.Int64)
extern void GZipStream_SetLength_m5DCF13005B2AB45961D14230670698CBAF93D0C1 (void);
// 0x00000CA7 System.Int32 System.IO.Compression.GZipStream::ReadByte()
extern void GZipStream_ReadByte_m486ACD72754C08B02CF1B90A71A744EE92F4D42C (void);
// 0x00000CA8 System.IAsyncResult System.IO.Compression.GZipStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void GZipStream_BeginRead_mDD211249BF8242D28889645A05453C3D89C6B0F1 (void);
// 0x00000CA9 System.Int32 System.IO.Compression.GZipStream::EndRead(System.IAsyncResult)
extern void GZipStream_EndRead_m8245FEE8D2B21A169DD1A40A67B5D398DD6005D2 (void);
// 0x00000CAA System.Int32 System.IO.Compression.GZipStream::Read(System.Byte[],System.Int32,System.Int32)
extern void GZipStream_Read_m41EC5D087FD227D05C8435E8704C9F0F6A7E2F9D (void);
// 0x00000CAB System.Int32 System.IO.Compression.GZipStream::Read(System.Span`1<System.Byte>)
extern void GZipStream_Read_mBC83A9A584B5A2DC2B08B044E96C0E8AF33723D5 (void);
// 0x00000CAC System.IAsyncResult System.IO.Compression.GZipStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void GZipStream_BeginWrite_mB80B01F99925CF4A6B201E77B54E7656A40474B8 (void);
// 0x00000CAD System.Void System.IO.Compression.GZipStream::EndWrite(System.IAsyncResult)
extern void GZipStream_EndWrite_m7FC15F1CD67B5C6977FEB16E630545AD6E020C91 (void);
// 0x00000CAE System.Void System.IO.Compression.GZipStream::Write(System.Byte[],System.Int32,System.Int32)
extern void GZipStream_Write_mCA30092CA9BEFD7425587337A5C4451866E1EB34 (void);
// 0x00000CAF System.Void System.IO.Compression.GZipStream::Write(System.ReadOnlySpan`1<System.Byte>)
extern void GZipStream_Write_mF25D52D7EC30B244830E14477D047FC5028E51D3 (void);
// 0x00000CB0 System.Void System.IO.Compression.GZipStream::Dispose(System.Boolean)
extern void GZipStream_Dispose_mD369BCB8951329CEE518D159BB1D16BBC0C709E3 (void);
// 0x00000CB1 System.Threading.Tasks.Task`1<System.Int32> System.IO.Compression.GZipStream::ReadAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void GZipStream_ReadAsync_mACECFB99609D4696156F98AE738B2038E6663471 (void);
// 0x00000CB2 System.Threading.Tasks.ValueTask`1<System.Int32> System.IO.Compression.GZipStream::ReadAsync(System.Memory`1<System.Byte>,System.Threading.CancellationToken)
extern void GZipStream_ReadAsync_mCFF5156AFDFF16B472A323F2D1BA86BFE13C7358 (void);
// 0x00000CB3 System.Threading.Tasks.Task System.IO.Compression.GZipStream::WriteAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)
extern void GZipStream_WriteAsync_m2CB67E3DB87DFC01588B604C8E2D60EE1AE27EF7 (void);
// 0x00000CB4 System.Threading.Tasks.ValueTask System.IO.Compression.GZipStream::WriteAsync(System.ReadOnlyMemory`1<System.Byte>,System.Threading.CancellationToken)
extern void GZipStream_WriteAsync_m43C1DEA3F232AFD6036CB0637831887E93488000 (void);
// 0x00000CB5 System.Void System.IO.Compression.GZipStream::CheckDeflateStream()
extern void GZipStream_CheckDeflateStream_m691CE26A65C042DAFBA84076906015A8B3CBE264 (void);
// 0x00000CB6 System.Void System.IO.Compression.GZipStream::ThrowStreamClosedException()
extern void GZipStream_ThrowStreamClosedException_m7B905CA279182188E2191C564DC9415234005D5F (void);
// 0x00000CB7 System.Void System.IO.Compression.DeflateStream::.ctor(System.IO.Stream,System.IO.Compression.CompressionMode,System.Boolean,System.Int32)
extern void DeflateStream__ctor_mFA0036CA1C1411FBED2DDFB2B563635F4755078C (void);
// 0x00000CB8 System.Void System.IO.Compression.DeflateStream::.ctor(System.IO.Stream,System.IO.Compression.CompressionMode,System.Boolean,System.Boolean)
extern void DeflateStream__ctor_m67AEE7C38BCFD323CD25D3FB36674E6A5010CD36 (void);
// 0x00000CB9 System.Void System.IO.Compression.DeflateStream::Finalize()
extern void DeflateStream_Finalize_mAFF7BCEF197A6549D13572A0AFD326CD995EA7AC (void);
// 0x00000CBA System.Void System.IO.Compression.DeflateStream::Dispose(System.Boolean)
extern void DeflateStream_Dispose_m85096A7176C94248E02CE7E9DA78338C45B2F746 (void);
// 0x00000CBB System.Int32 System.IO.Compression.DeflateStream::ReadInternal(System.Byte[],System.Int32,System.Int32)
extern void DeflateStream_ReadInternal_m4FCD4EB1534E24E85A9E5D933D6AF6EABF5F223C (void);
// 0x00000CBC System.Threading.Tasks.ValueTask`1<System.Int32> System.IO.Compression.DeflateStream::ReadAsyncMemory(System.Memory`1<System.Byte>,System.Threading.CancellationToken)
extern void DeflateStream_ReadAsyncMemory_m1FC9EB7B2284773AA527913018FEA9EB82965327 (void);
// 0x00000CBD System.Int32 System.IO.Compression.DeflateStream::ReadCore(System.Span`1<System.Byte>)
extern void DeflateStream_ReadCore_m58E605C082DCF3455C4D56EBEBB8F2ABECCC9B40 (void);
// 0x00000CBE System.Int32 System.IO.Compression.DeflateStream::Read(System.Byte[],System.Int32,System.Int32)
extern void DeflateStream_Read_m46316C432282671BF001324B75881E50CC2B7FA3 (void);
// 0x00000CBF System.Void System.IO.Compression.DeflateStream::WriteInternal(System.Byte[],System.Int32,System.Int32)
extern void DeflateStream_WriteInternal_m5BD64441B31FC3D10F20A273FF45380EAAB97845 (void);
// 0x00000CC0 System.Threading.Tasks.ValueTask System.IO.Compression.DeflateStream::WriteAsyncMemory(System.ReadOnlyMemory`1<System.Byte>,System.Threading.CancellationToken)
extern void DeflateStream_WriteAsyncMemory_mDEFCEC8FAA595B11419D022F351AD825C01AFCE6 (void);
// 0x00000CC1 System.Void System.IO.Compression.DeflateStream::WriteCore(System.ReadOnlySpan`1<System.Byte>)
extern void DeflateStream_WriteCore_mC6ADF328D0A66CAFEEC9B23F89A22C223DFF3730 (void);
// 0x00000CC2 System.Void System.IO.Compression.DeflateStream::Write(System.Byte[],System.Int32,System.Int32)
extern void DeflateStream_Write_mA09DDD17D3B00089F808F9C7C587226666DB141A (void);
// 0x00000CC3 System.Void System.IO.Compression.DeflateStream::Flush()
extern void DeflateStream_Flush_mE12E917486F3EA41FE453945AA5B5617F80E6D0C (void);
// 0x00000CC4 System.IAsyncResult System.IO.Compression.DeflateStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void DeflateStream_BeginRead_m5FD78CDEDE42221D07255D38C4E8BBB3FFAD0235 (void);
// 0x00000CC5 System.IAsyncResult System.IO.Compression.DeflateStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void DeflateStream_BeginWrite_m00FB9CAB52A95FA92911DBB50200D8AB81F4F4BA (void);
// 0x00000CC6 System.Int32 System.IO.Compression.DeflateStream::EndRead(System.IAsyncResult)
extern void DeflateStream_EndRead_m3A49F7660DAE5121AC48BD7409580CD46E3D8D9B (void);
// 0x00000CC7 System.Void System.IO.Compression.DeflateStream::EndWrite(System.IAsyncResult)
extern void DeflateStream_EndWrite_m304251B2932DD464A16C7306E862BF86BA516A6A (void);
// 0x00000CC8 System.Int64 System.IO.Compression.DeflateStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void DeflateStream_Seek_mCABEFA32513E0858BC7A98F05AAA217A09C65B16 (void);
// 0x00000CC9 System.Void System.IO.Compression.DeflateStream::SetLength(System.Int64)
extern void DeflateStream_SetLength_mC18B3CCD1225B3C3A15EF54830C44236670F63F3 (void);
// 0x00000CCA System.Boolean System.IO.Compression.DeflateStream::get_CanRead()
extern void DeflateStream_get_CanRead_mDDA3C3CEE73CFCD62DE14F5DE9F27C7A5615976E (void);
// 0x00000CCB System.Boolean System.IO.Compression.DeflateStream::get_CanSeek()
extern void DeflateStream_get_CanSeek_m93DEADA4FB38CBBAB266F8B7DBAFE508AEC222E4 (void);
// 0x00000CCC System.Boolean System.IO.Compression.DeflateStream::get_CanWrite()
extern void DeflateStream_get_CanWrite_m95EB501342F11B287A529DE15089097EDF7C215F (void);
// 0x00000CCD System.Int64 System.IO.Compression.DeflateStream::get_Length()
extern void DeflateStream_get_Length_m8C7CE98029153C5065D28496ACE3F99CB644897C (void);
// 0x00000CCE System.Int64 System.IO.Compression.DeflateStream::get_Position()
extern void DeflateStream_get_Position_mA6B4FE66DBE953C5504A3561AD75FCC87718F376 (void);
// 0x00000CCF System.Void System.IO.Compression.DeflateStream::set_Position(System.Int64)
extern void DeflateStream_set_Position_m9BA570C4BF91C325EFE8E6BEAF40E6AB54BCC885 (void);
// 0x00000CD0 System.Void System.IO.Compression.DeflateStream/ReadMethod::.ctor(System.Object,System.IntPtr)
extern void ReadMethod__ctor_mA65629D1C063B5E57095F952077323B5CE96C396 (void);
// 0x00000CD1 System.Int32 System.IO.Compression.DeflateStream/ReadMethod::Invoke(System.Byte[],System.Int32,System.Int32)
extern void ReadMethod_Invoke_m0FA726BC164CDBF0ACA713BDD0EBA3187D0562F8 (void);
// 0x00000CD2 System.IAsyncResult System.IO.Compression.DeflateStream/ReadMethod::BeginInvoke(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void ReadMethod_BeginInvoke_mE465A26187BE3F1829ECA0C02AFAEFCA6D2F6EEF (void);
// 0x00000CD3 System.Int32 System.IO.Compression.DeflateStream/ReadMethod::EndInvoke(System.IAsyncResult)
extern void ReadMethod_EndInvoke_mBCA4D1483C0504BF239C2305B7201E1EC72ED6F7 (void);
// 0x00000CD4 System.Void System.IO.Compression.DeflateStream/WriteMethod::.ctor(System.Object,System.IntPtr)
extern void WriteMethod__ctor_mBE4CCCE38227B2D866CB539879255AD8B0C2B51A (void);
// 0x00000CD5 System.Void System.IO.Compression.DeflateStream/WriteMethod::Invoke(System.Byte[],System.Int32,System.Int32)
extern void WriteMethod_Invoke_m8754DB7443BBDB1F7553BFA43DBBD024C0E268F1 (void);
// 0x00000CD6 System.IAsyncResult System.IO.Compression.DeflateStream/WriteMethod::BeginInvoke(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void WriteMethod_BeginInvoke_m4A29F5255900687782F0A92A0A4D043AEA2C982C (void);
// 0x00000CD7 System.Void System.IO.Compression.DeflateStream/WriteMethod::EndInvoke(System.IAsyncResult)
extern void WriteMethod_EndInvoke_mFDA332678A1FD46BAE42F3A0DF2073FD9CC6628C (void);
// 0x00000CD8 System.Void System.IO.Compression.DeflateStreamNative::.ctor()
extern void DeflateStreamNative__ctor_mCDA4F9E1FF67EFCE84EBB8889DDDD30F4A12BB0E (void);
// 0x00000CD9 System.IO.Compression.DeflateStreamNative System.IO.Compression.DeflateStreamNative::Create(System.IO.Stream,System.IO.Compression.CompressionMode,System.Boolean)
extern void DeflateStreamNative_Create_m628D0A7F7EE62339C0D867FBF4319093C82018A7 (void);
// 0x00000CDA System.Void System.IO.Compression.DeflateStreamNative::Finalize()
extern void DeflateStreamNative_Finalize_mE02A29EE58E329CFC439284070FF87A31B18C701 (void);
// 0x00000CDB System.Void System.IO.Compression.DeflateStreamNative::Dispose(System.Boolean)
extern void DeflateStreamNative_Dispose_m4FB7DA2D3E50360ECCE3B81A77CF25B5C6722C8E (void);
// 0x00000CDC System.Void System.IO.Compression.DeflateStreamNative::Flush()
extern void DeflateStreamNative_Flush_m60D8D1C3DEBB720E402625ECFA9A0CFCA69E8F6F (void);
// 0x00000CDD System.Int32 System.IO.Compression.DeflateStreamNative::ReadZStream(System.IntPtr,System.Int32)
extern void DeflateStreamNative_ReadZStream_m64072AF17150906ED32D871778DC5EF313FADD9F (void);
// 0x00000CDE System.Void System.IO.Compression.DeflateStreamNative::WriteZStream(System.IntPtr,System.Int32)
extern void DeflateStreamNative_WriteZStream_m4B066C7A2E031A2CA6EE0FE876A9611242630F18 (void);
// 0x00000CDF System.Int32 System.IO.Compression.DeflateStreamNative::UnmanagedRead(System.IntPtr,System.Int32,System.IntPtr)
extern void DeflateStreamNative_UnmanagedRead_m79E9628F6FF91A3995491997242DD83046C9B704 (void);
// 0x00000CE0 System.Int32 System.IO.Compression.DeflateStreamNative::UnmanagedRead(System.IntPtr,System.Int32)
extern void DeflateStreamNative_UnmanagedRead_mC17A13844F4560E954F816037B0A5C04FFEDECB1 (void);
// 0x00000CE1 System.Int32 System.IO.Compression.DeflateStreamNative::UnmanagedWrite(System.IntPtr,System.Int32,System.IntPtr)
extern void DeflateStreamNative_UnmanagedWrite_m509AE153ECB916CDF755432335FBAB2B51CA1486 (void);
// 0x00000CE2 System.Int32 System.IO.Compression.DeflateStreamNative::UnmanagedWrite(System.IntPtr,System.Int32)
extern void DeflateStreamNative_UnmanagedWrite_m73ADBE62E8371FFDFFB380F568A8510E8644B1BD (void);
// 0x00000CE3 System.Void System.IO.Compression.DeflateStreamNative::CheckResult(System.Int32,System.String)
extern void DeflateStreamNative_CheckResult_m9354BDD0FA5B71935FFD628698412B5380329C47 (void);
// 0x00000CE4 System.IO.Compression.DeflateStreamNative/SafeDeflateStreamHandle System.IO.Compression.DeflateStreamNative::CreateZStream(System.IO.Compression.CompressionMode,System.Boolean,System.IO.Compression.DeflateStreamNative/UnmanagedReadOrWrite,System.IntPtr)
extern void DeflateStreamNative_CreateZStream_m5075DD90424301C1C82A2142AA530FDC6F345006 (void);
// 0x00000CE5 System.Int32 System.IO.Compression.DeflateStreamNative::CloseZStream(System.IntPtr)
extern void DeflateStreamNative_CloseZStream_mEA426722FFB221614DC4AFEC80BBB4382B375187 (void);
// 0x00000CE6 System.Int32 System.IO.Compression.DeflateStreamNative::Flush(System.IO.Compression.DeflateStreamNative/SafeDeflateStreamHandle)
extern void DeflateStreamNative_Flush_mAC7D59B136EB2C8B1512123F21B91976CDE33D5F (void);
// 0x00000CE7 System.Int32 System.IO.Compression.DeflateStreamNative::ReadZStream(System.IO.Compression.DeflateStreamNative/SafeDeflateStreamHandle,System.IntPtr,System.Int32)
extern void DeflateStreamNative_ReadZStream_mC4E36C2FDE3E7850B154FE6A09825D7B055E1519 (void);
// 0x00000CE8 System.Int32 System.IO.Compression.DeflateStreamNative::WriteZStream(System.IO.Compression.DeflateStreamNative/SafeDeflateStreamHandle,System.IntPtr,System.Int32)
extern void DeflateStreamNative_WriteZStream_m95704830E739FD66C207ED91323852F7E0FCC35B (void);
// 0x00000CE9 System.Void System.IO.Compression.DeflateStreamNative/UnmanagedReadOrWrite::.ctor(System.Object,System.IntPtr)
extern void UnmanagedReadOrWrite__ctor_mF122921041A4F277205DD17182C31BBE9B1DEEF6 (void);
// 0x00000CEA System.Int32 System.IO.Compression.DeflateStreamNative/UnmanagedReadOrWrite::Invoke(System.IntPtr,System.Int32,System.IntPtr)
extern void UnmanagedReadOrWrite_Invoke_m595EDC2E3BDD8B54CA699C245FAA1003994A5B22 (void);
// 0x00000CEB System.Boolean System.IO.Compression.DeflateStreamNative/SafeDeflateStreamHandle::get_IsInvalid()
extern void SafeDeflateStreamHandle_get_IsInvalid_m8220E0B23A6DE1C8171ADDD78B30FBC346F1D1D1 (void);
// 0x00000CEC System.Void System.IO.Compression.DeflateStreamNative/SafeDeflateStreamHandle::.ctor()
extern void SafeDeflateStreamHandle__ctor_mBDF78EC52524A938914F593AF5C198C9F7632E41 (void);
// 0x00000CED System.Boolean System.IO.Compression.DeflateStreamNative/SafeDeflateStreamHandle::ReleaseHandle()
extern void SafeDeflateStreamHandle_ReleaseHandle_m8A0A695A4A1C2F46C8BFBE3BD1526BCDC6066704 (void);
// 0x00000CEE System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_mC09A19935970846B38570D03608F0E5F25E2020C (void);
// 0x00000CEF System.Void System.Net.Configuration.BypassElementCollection::.ctor()
extern void BypassElementCollection__ctor_m344EC8CD2EACA36E1213C5914037BB133E87FE54 (void);
// 0x00000CF0 System.Void System.Net.Configuration.ConnectionManagementElementCollection::.ctor()
extern void ConnectionManagementElementCollection__ctor_m4698342A715032A762FA04B2745BFCA46263BCB3 (void);
// 0x00000CF1 System.Void System.Net.Configuration.ConnectionManagementSection::.ctor()
extern void ConnectionManagementSection__ctor_m2FA1D29F887BD6498FBEF692BBC0AA727E38E802 (void);
// 0x00000CF2 System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.ConnectionManagementSection::get_Properties()
extern void ConnectionManagementSection_get_Properties_mFCE0F9418E9E8CD2E1301815CC4D548261F58E95 (void);
// 0x00000CF3 System.Void System.Net.Configuration.DefaultProxySection::.ctor()
extern void DefaultProxySection__ctor_m962B68CEFCDE7DFB7646EF16297CA4BE0097284D (void);
// 0x00000CF4 System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.DefaultProxySection::get_Properties()
extern void DefaultProxySection_get_Properties_m5186F1A2734EB96F065C7BC14A215D17F8325418 (void);
// 0x00000CF5 System.Void System.Net.Configuration.DefaultProxySection::Reset(System.Configuration.ConfigurationElement)
extern void DefaultProxySection_Reset_m0B9A1155840E7C55FC24AEDB23FB17A2815EEB77 (void);
// 0x00000CF6 System.Void System.Net.Configuration.ProxyElement::.ctor()
extern void ProxyElement__ctor_mC1BDF426F97F4F84CB5A34B423F59EC8748EBD70 (void);
// 0x00000CF7 System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.ProxyElement::get_Properties()
extern void ProxyElement_get_Properties_mBFE6E283FE7E9329B586ACAADC8F2E047C7ABB2A (void);
// 0x00000CF8 System.Void System.Net.Configuration.HttpWebRequestElement::.ctor()
extern void HttpWebRequestElement__ctor_mB9DDAC7FF431D4AF4CAFCBC96E1A3572A1F83717 (void);
// 0x00000CF9 System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.HttpWebRequestElement::get_Properties()
extern void HttpWebRequestElement_get_Properties_mAA0F11EA73B5397C50809EAC679F2C564F9EC7CA (void);
// 0x00000CFA System.Void System.Net.Configuration.Ipv6Element::.ctor()
extern void Ipv6Element__ctor_m869AC261E0994A8B3D83CEE25C6CB99E6A5EA4C1 (void);
// 0x00000CFB System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.Ipv6Element::get_Properties()
extern void Ipv6Element_get_Properties_m382D5B5B098EFF19C2A4AC1889CD1C8EAB918322 (void);
// 0x00000CFC System.Void System.Net.Configuration.NetSectionGroup::.ctor()
extern void NetSectionGroup__ctor_mCD2F0DC2B9F0C6C346BC91BD8750C7111A9528AC (void);
// 0x00000CFD System.Void System.Net.Configuration.SettingsSection::.ctor()
extern void SettingsSection__ctor_mBD94E7E098221DC09775A3AE1713930F6820D242 (void);
// 0x00000CFE System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.SettingsSection::get_Properties()
extern void SettingsSection_get_Properties_mA774C9C884F4AD5DEC2BBE31DF7E35CAC71EBF8F (void);
// 0x00000CFF System.Void System.Net.Configuration.PerformanceCountersElement::.ctor()
extern void PerformanceCountersElement__ctor_m9537195CBC7DC607F7253D06E7B6B1090456E68F (void);
// 0x00000D00 System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.PerformanceCountersElement::get_Properties()
extern void PerformanceCountersElement_get_Properties_m7C16FBC3E915E8384FB4409209628737B1537C9B (void);
// 0x00000D01 System.Void System.Net.Configuration.ServicePointManagerElement::.ctor()
extern void ServicePointManagerElement__ctor_m0F1CF21589CA283DA734A472930A9665698B2D36 (void);
// 0x00000D02 System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.ServicePointManagerElement::get_Properties()
extern void ServicePointManagerElement_get_Properties_m70469523AAD1A2C2E9D4F90DDA1BA619FCE68B58 (void);
// 0x00000D03 System.Void System.Net.Configuration.SocketElement::.ctor()
extern void SocketElement__ctor_m4FB4C851F1BB20CED74C206EFB1BA9DDFBE024D2 (void);
// 0x00000D04 System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.SocketElement::get_Properties()
extern void SocketElement_get_Properties_m8AF94DEEDA0EB4092F59FBDC361592E1B9EBDEC3 (void);
// 0x00000D05 System.Void System.Net.Configuration.WebProxyScriptElement::.ctor()
extern void WebProxyScriptElement__ctor_mB613D71DE569317E40726CD8DB9A6ABA0A302821 (void);
// 0x00000D06 System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.WebProxyScriptElement::get_Properties()
extern void WebProxyScriptElement_get_Properties_mF5188225C0C1D64BC136465BB4B0E962FFE9240B (void);
// 0x00000D07 System.Void System.Net.Configuration.WebRequestModulesSection::.ctor()
extern void WebRequestModulesSection__ctor_mF25CD80A5111A4F87B7FBF7663A3442470B97C0E (void);
// 0x00000D08 System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.WebRequestModulesSection::get_Properties()
extern void WebRequestModulesSection_get_Properties_mBDBDED569E677717FB617CD36B94AE34BF4208F6 (void);
// 0x00000D09 System.Void System.Net.Configuration.WebRequestModuleElementCollection::.ctor()
extern void WebRequestModuleElementCollection__ctor_m62C156D78336B2E37EA6F10D477FDEAE4161F99E (void);
// 0x00000D0A System.Void System.Diagnostics.DiagnosticsConfigurationHandler::.ctor()
extern void DiagnosticsConfigurationHandler__ctor_mDFB0000E6218840F66D21BB6C9BF3F647CCBC5A3 (void);
// 0x00000D0B System.Object System.Diagnostics.DiagnosticsConfigurationHandler::Create(System.Object,System.Object,System.Xml.XmlNode)
extern void DiagnosticsConfigurationHandler_Create_mF21B1CC68B87DE078690B2B493FCDC5F728CBAAB (void);
// 0x00000D0C System.Void Unity.ThrowStub::ThrowNotSupportedException()
extern void ThrowStub_ThrowNotSupportedException_m0E6C3891D0501FEF5BF1A72B7AA4D6310CE082DD (void);
static Il2CppMethodPointer s_methodPointers[3340] = 
{
	SystemCertificateProvider_GetX509Pal_m25337E8316D705CEC31B70783EFE75F0F72256D4,
	SystemCertificateProvider_EnsureInitialized_m5270DDA49FA6EA014CBA47A3AD13FB9A4C99B206,
	SystemCertificateProvider_get_X509Pal_m0E8EA8BF5F1C65CA33C3C4E0FA723E93DFF74B97,
	SystemCertificateProvider_Import_m3349082A12B7F5513069393B9EE77CF5264AB0FA,
	SystemCertificateProvider_Mono_ISystemCertificateProvider_Import_m4D2602C705772487B0FF370D6CB704324FDBE98F,
	SystemCertificateProvider_Import_m530888F3CEA00BE89B9AF47DAFCF03E319A5AD73,
	SystemCertificateProvider_Mono_ISystemCertificateProvider_Import_mD7F4BAB2B4498500CD48A645D674188BFC30F036,
	SystemCertificateProvider_Import_mC81F89F5BDFCF15698FDCEFAAEF6ACE1F6CB585C,
	SystemCertificateProvider__ctor_m28122065D2285B598B1F145BC122693E14F722AC,
	SystemCertificateProvider__cctor_m03BD266351742E571EA0605CB05A4842B1999803,
	SystemDependencyProvider_get_Instance_m410F141F466665CEBA34A72F26D78C77A8B66719,
	SystemDependencyProvider_Initialize_mC10ED912B2EC028539CA83BE2BAE4EE1E237E835,
	SystemDependencyProvider_Mono_ISystemDependencyProvider_get_CertificateProvider_mEEA1F11DB63C54CCFAE6EA976D5B349719FEACCF,
	SystemDependencyProvider_get_CertificateProvider_mEC19F34D14EB2B7BF5F8973DBC3BF3EAD5B1A19D,
	SystemDependencyProvider_get_X509Pal_m4982FB6EE530F77FB076AD4D21A57BBEDCA8D961,
	SystemDependencyProvider__ctor_mB851EC1408E241AB573D319D5BFCC1E95CF40343,
	SystemDependencyProvider__cctor_mFFD9148E4D54BCAB5E5244CA21AE22F8032D4D5A,
	X509Pal_get_Instance_mBA83FEDE77C97026665ED6DCDDBDC047CBA632C3,
	X509PalImplMono_Import_m72547BAC14066F54342B6D09539D358F6F0F045B,
	X509PalImplMono_Import_m3AC55B3BC7FEDCC4872F8D51A9229333470AA19E,
	X509PalImplMono_Import_m152053C1A07898B4096D6E1537AFF88EE8AB9633,
	X509PalImplMono__ctor_m8DEF93E123E01393942BB5A18755A221C588DD47,
	NULL,
	NULL,
	NULL,
	X509PalImpl_PEM_m21975D897B5389B6BA20C629AAFF37691DEE76A2,
	X509PalImpl_ConvertData_mD638DA0765DA08BFD8FF87884A2BFCC9883131DF,
	X509PalImpl_ImportFallback_m7BF753657B715800F1F182A3B7198578D197DAC3,
	X509PalImpl_ImportFallback_mA21AA40BF7FE8E3E3F62ADBEBBBF415DC9BC588E,
	X509PalImpl_get_SupportsLegacyBasicConstraintsExtension_mA7FF5FA4ABFF9B8A028FD3937636C6E4D7AE2F38,
	X509PalImpl_GetCertContentType_m41DC755893135DB1ACF78832BDAB201C46AC6F99,
	X509PalImpl__ctor_m29605E82202DAC5123F4B35D87719454190299C0,
	X509PalImpl__cctor_mDA462E335386A1004AE32139C485AA0D6474DA27,
	MonoPInvokeCallbackAttribute__ctor_m8B720CC4ADB295EF6F98ECB4B14AB5010CBC8C28,
	CertHelper_AddCertificatesToNativeChain_m6A078F5FFEA16184F19AEEDD4F9C8E00F68BDD48,
	CertHelper_AddCertificateToNativeChain_m04D6F01E00DF4D0C134704DF9403167E1878675A,
	Debug_CheckAndThrow_m0DC4C61C1A5FA2DF49E11420953CD3032843D59F,
	Debug_CheckAndThrow_m47BE0492786EEE30351AF6662609556CABAB0A80,
	UnityTls_GetUnityTlsInterface_m47FB014BA17E899E2AE904DE3F59A84763D924F0,
	UnityTls_get_IsSupported_m7A43EE578D088D942479C11C7A1718432ED1F048,
	UnityTls_get_NativeInterface_m48CD53391B9897F81D41D180A899D4C48F9B374B,
	unitytls_x509verify_callback__ctor_mEDC6857514FD696562638B3F1DFE959920D1EB26,
	unitytls_x509verify_callback_Invoke_mB53E808E78AE5BD02CECCA0BE198295ECAB83B9A,
	unitytls_tlsctx_write_callback__ctor_m1A216E476EA91B415C0FD89765E6560410A7F7F5,
	unitytls_tlsctx_write_callback_Invoke_mF1F1D27A9BE14950A905685370EDAE5EAB051712,
	unitytls_tlsctx_read_callback__ctor_mEAC57775C6AB86A2E9E6DDC440E27C845BFAA4C0,
	unitytls_tlsctx_read_callback_Invoke_m0B21AFD4EACBDBEC6EEFBEB61E142051F338194B,
	unitytls_tlsctx_trace_callback__ctor_mB9CC97640B55911851D544F3F85CAFB5B85B647A,
	unitytls_tlsctx_trace_callback_Invoke_m9511E6D873C0FFE4EBDA45E52CA47228A483A01F,
	unitytls_tlsctx_certificate_callback__ctor_m9A3F05E07D9A69B8FF419421DF8167DF9CE7E77A,
	unitytls_tlsctx_certificate_callback_Invoke_m37CB366C6D6E0A27CE4E1E4943DF2DE1F32E2BF1,
	unitytls_tlsctx_x509verify_callback__ctor_m8583619DBB13F6EDF2A385C37DFAE8796EB89C0F,
	unitytls_tlsctx_x509verify_callback_Invoke_m1F7A6DA53EF61AC64F2DD9A33AF40541F97C6874,
	unitytls_interface_struct__ctor_mCBFC829FE6CEA8E6545EA7B61E756FFA2EC4A22D,
	unitytls_errorstate_create_t__ctor_m291CCEF12E6C9729662E0BBECB5D4FA6BDBA2F02,
	unitytls_errorstate_create_t_Invoke_m8B9A9835EA7C6A11E1627CA8904F3F853FB61BC0,
	unitytls_errorstate_raise_error_t__ctor_mF30A64F84A8FC246DD062993A45C038B30FC6009,
	unitytls_errorstate_raise_error_t_Invoke_m9B2B3433846A318340EDF0ED0E33C45030CCEA02,
	unitytls_key_get_ref_t__ctor_mEDA7937D1EC76342E6CE92AF394E7E1A37F664B1,
	unitytls_key_get_ref_t_Invoke_mF5D3582922E868C42BCF32EB14471F8C800BB66E,
	unitytls_key_parse_der_t__ctor_m25AD8C01A87CF5F737E649F1FB9CA340A9A832A8,
	unitytls_key_parse_der_t_Invoke_m244086296EBDE0C0B0F072B33AAD5BA39A030DB6,
	unitytls_key_parse_pem_t__ctor_m8F4D69BD7B0988BF7682B3E5EA5934141BFEE440,
	unitytls_key_parse_pem_t_Invoke_mE842DC1436977A2982224694DB5BCC2FF8A9B331,
	unitytls_key_free_t__ctor_m935A5571CE60C369950D58C79432FB1C50231C37,
	unitytls_key_free_t_Invoke_m860A3C396228B7ADF9406AE78D439BDCD12FB98E,
	unitytls_x509_export_der_t__ctor_m9BF2B0B567856AE42AB0ED274CD26ACE5FC0186B,
	unitytls_x509_export_der_t_Invoke_m13DB2E205EF9ED37B75F5B763C5080E7077841AD,
	unitytls_x509list_get_ref_t__ctor_m6977636DC288FB0809D7C0CE8D9D31D25795F77C,
	unitytls_x509list_get_ref_t_Invoke_mE7C675B7847FFEF96C25AE757D34CE920AA16EC2,
	unitytls_x509list_get_x509_t__ctor_mFF7BC99AB91FE7F9CCB586988B246F597ADF53DD,
	unitytls_x509list_get_x509_t_Invoke_m9080273A34FE768B80024B96001D5B7451A46EC0,
	unitytls_x509list_create_t__ctor_m41F2B1E6C767CD3A39F4E0FAD41B678AF61A14A6,
	unitytls_x509list_create_t_Invoke_m05D8719D913109E4997FF913015E66455BC3C7D3,
	unitytls_x509list_append_t__ctor_m22FAC52BA432FBCF2048DA6236633970E4E18CB0,
	unitytls_x509list_append_t_Invoke_mEF1811CFAD02743F745D8A8E09E2E84D68154976,
	unitytls_x509list_append_der_t__ctor_mB33965F4253393C8B17E44DACA1ECDBCC17EB673,
	unitytls_x509list_append_der_t_Invoke_m3D900F0F07C8747B9F046512DAC7579D0C7CD193,
	unitytls_x509list_free_t__ctor_mA156855741A0AA67C20B87507BE55F188ECBFFD4,
	unitytls_x509list_free_t_Invoke_m0974FBCD7B6920A4EC57BDCB080E1A7307EEEA02,
	unitytls_x509verify_default_ca_t__ctor_m936B931A7A5A10B4A514630F84CF136A799AFA23,
	unitytls_x509verify_default_ca_t_Invoke_m1D112FD2010361D2DB08F08A598000ADCD0875C3,
	unitytls_x509verify_explicit_ca_t__ctor_m1812F92F4C92CE54233697CE380177EBA629A1F1,
	unitytls_x509verify_explicit_ca_t_Invoke_m4028A9AE8FD6B69DB36BAE68CC3A3362A825738B,
	unitytls_tlsctx_create_server_t__ctor_m035F9FCC9C4B71D3BA4F1610FBCFB7E87E28EDD7,
	unitytls_tlsctx_create_server_t_Invoke_mCEB3E8CADCB316F4DF606A68182A4E5C9EC4D3B9,
	unitytls_tlsctx_create_client_t__ctor_m898460A0D30C6EEF737F445DFBA96076E4D22130,
	unitytls_tlsctx_create_client_t_Invoke_m728BCBF32A45326A1AD8B1E17933372F7D9E01EE,
	unitytls_tlsctx_server_require_client_authentication_t__ctor_m3E84F3D43AA2928DA825256C6179E2D0CF926C60,
	unitytls_tlsctx_server_require_client_authentication_t_Invoke_m0B6DAC7CA95263D1245C9B0A466AF00D073059EB,
	unitytls_tlsctx_set_certificate_callback_t__ctor_m31E0497908A2C272B9AB80B4659D1517F9647E20,
	unitytls_tlsctx_set_certificate_callback_t_Invoke_mFC277320977BC310A0EB484EE505E25B6E2C7CA3,
	unitytls_tlsctx_set_trace_callback_t__ctor_m262F20124214D62819468C226E925221D2527D96,
	unitytls_tlsctx_set_trace_callback_t_Invoke_mC98468DD35AA05DE628FEAE7669B4F71205CCBD7,
	unitytls_tlsctx_set_x509verify_callback_t__ctor_mE6C3A39FA8A1903C74826A5BFF748DFEB1A4D06D,
	unitytls_tlsctx_set_x509verify_callback_t_Invoke_mA9AE094A7DB71B4B7B051BB3D67CB98A7F48D3CE,
	unitytls_tlsctx_set_supported_ciphersuites_t__ctor_m856B5362E3479A5C75709F62B5A316C0A0F913FF,
	unitytls_tlsctx_set_supported_ciphersuites_t_Invoke_m8E153461EFCEAAAF59D1E918F7C06B14358B43E2,
	unitytls_tlsctx_get_ciphersuite_t__ctor_mFD8DF58601BB4A8F00C3A56F881F2E92D7536CA9,
	unitytls_tlsctx_get_ciphersuite_t_Invoke_m2DF155D62800CFFD66AC63538F69DB2275EC3FFD,
	unitytls_tlsctx_get_protocol_t__ctor_mAA5710F913B95E0964AD90EDF59FD71E984DD00E,
	unitytls_tlsctx_get_protocol_t_Invoke_m3D931DDD241F0B3EDA8F6F1EFA159C0669E7E3F0,
	unitytls_tlsctx_process_handshake_t__ctor_m163A6D53CE45B5161FD5586DBBD8BB90B9EDDA45,
	unitytls_tlsctx_process_handshake_t_Invoke_mD8F2422C5C6C9FF0ADDAB17AECB998C2E225CF7A,
	unitytls_tlsctx_read_t__ctor_m55B52EA0C5ED94A8030D80A9B7967F34850AC799,
	unitytls_tlsctx_read_t_Invoke_m0FB34952D7592128A211AA0DA5536A325494FA04,
	unitytls_tlsctx_write_t__ctor_mD229A38085F7CAE944D6CF0D505C6F167C709E96,
	unitytls_tlsctx_write_t_Invoke_mC6A06FBF8E4C5652830248B521C83865B3F2DBBC,
	unitytls_tlsctx_notify_close_t__ctor_m700B77B204CFF6F4113F3E3F4CF6673CF7ACC0FB,
	unitytls_tlsctx_notify_close_t_Invoke_m6E5688DDA03F313A1C470E4C30263C5B4A174138,
	unitytls_tlsctx_free_t__ctor_m46E06E2335A31589194453E910356B6456160E7E,
	unitytls_tlsctx_free_t_Invoke_mC5D5191740D2507A83D95B4B3E6CE4C4A9FBAFD2,
	unitytls_random_generate_bytes_t__ctor_mFAC4CCD07765DE0827186AEC35DCCFC2E66BE066,
	unitytls_random_generate_bytes_t_Invoke_mF0A5B5E7279FF32FBFF48DBB98EB07C88C63EF92,
	UnityTlsContext__ctor_m4B8251E305EF5AC2CCB5D963BCB2F61D0F18408C,
	UnityTlsContext_ExtractNativeKeyAndChainFromManagedCertificate_mAAD3045A2D64C314FD288C3468D41767DD47205A,
	UnityTlsContext_get_IsAuthenticated_mA566E1D02B55C267233595651ACBDE0AB14D0361,
	UnityTlsContext_get_RemoteCertificate_m01F83F7747AEA21356AF35E31870F03909474FA7,
	UnityTlsContext_Flush_mBE19169C723528F75B3A13B17ECB1A36690E27C6,
	UnityTlsContext_Read_m461E85BF4B42B72297B42A6E7EE5A756A25259FD,
	UnityTlsContext_Write_m11529FF555E48CA82577431E6856FEE3E4E0FA28,
	UnityTlsContext_Renegotiate_m5E5182B59BAA90319DC59CD711DDE930C39BEB75,
	UnityTlsContext_PendingRenegotiation_m9F5831ECA8A6BBA97585A08ED9F51CA08F502187,
	UnityTlsContext_Shutdown_m6DD342ACE3206195381E15E5CD5BB4845CDBDBE6,
	UnityTlsContext_Dispose_m5E0A4A0D9AF08F03E82CBF45BD14477E7D2D34DB,
	UnityTlsContext_StartHandshake_m56718DFC2F7D4A03C3B1AD6C9ABE502EDCB6BC0A,
	UnityTlsContext_ProcessHandshake_m098B5D0081E137B05DB4FB7B7F74805EF749C09D,
	UnityTlsContext_FinishHandshake_m4F1BF52160E1BFCF223717415D5F1DB96B5F9556,
	UnityTlsContext_WriteCallback_m53DD70115C97432A676F3E437E8FB42F9FC068F5,
	UnityTlsContext_WriteCallback_m28708B36ED5744CED29D543B86A2E117DFF661BA,
	UnityTlsContext_ReadCallback_m15F3A217E44D480CAED06EB5A88503FB6259D7EC,
	UnityTlsContext_ReadCallback_mE4765DDC1755E4A030F1D3B59B255072FF656ED1,
	UnityTlsContext_VerifyCallback_m75D7C072718405EBBF8A2A9C794C4DDFB2595BD0,
	UnityTlsContext_VerifyCallback_m2498A768960C1A792A1B60A1F47D16624E543537,
	UnityTlsContext_CertificateCallback_mF5E626BA2545CFFA64428622678E409702C2045A,
	UnityTlsContext_CertificateCallback_m5A4C050D5D49709202B72A50F33533E7C5C6B113,
	UnityTlsConversions_GetMinProtocol_m0E691D4227CAB79BDC7F6E30E5CF2698D2F5813F,
	UnityTlsConversions_GetMaxProtocol_mC7381248C4DA2168508E3A4C5DE049BE58C7F250,
	UnityTlsConversions_ConvertProtocolVersion_mB9C0E5A59CC1564EDF56D06C396461DA678CAD0B,
	UnityTlsConversions_VerifyResultToAlertDescription_m23B95B4CEFBE8FB8329DF4A53E744F9592C40DA6,
	UnityTlsConversions_VerifyResultToPolicyErrror_m6D9F0BBA8B599BF2133E28C4D804472CEDA5AFCF,
	UnityTlsConversions_VerifyResultToChainStatus_m266E87DEE12D19BFD385BCCE2F5F30C527D6DB74,
	UnityTlsProvider_get_Name_m24407BD8B3C23C4DCF2EDF41F9056463763E7389,
	UnityTlsProvider_get_ID_mE43288EBB589EFFC6EE6E49209FBE8295BE577F1,
	UnityTlsProvider_get_SupportsSslStream_mBB478D61296FAF8CAC3145C816E27AA7A1CE8CC8,
	UnityTlsProvider_get_SupportsMonoExtensions_mC34E3757CB6273E3838F8DF33503CF6E4E4ADC01,
	UnityTlsProvider_get_SupportsConnectionInfo_mF18DD7A3E1806F86AB471A580EEAD68AB736FB73,
	UnityTlsProvider_get_SupportsCleanShutdown_mBB7DCD0ACF8B5239D89F4695859B90D2F5275D50,
	UnityTlsProvider_get_SupportedProtocols_m59C751B343CAC7A9692B1C12FC986450EF700C88,
	UnityTlsProvider_CreateSslStream_m0B98A540CD6B4AE49FFD79ACCC226E69F21DE848,
	UnityTlsProvider_x509verify_callback_m47823254E133F7304ABFAE5A1F0D218402BC5B45,
	UnityTlsProvider_ValidateCertificate_mE2F233ED2EDE0D248D79D8133BE8F1C9133856A6,
	UnityTlsProvider__ctor_m0C11027324407A76EB81EB51C37FF5C8F6F219A8,
	UnityTlsStream__ctor_mEE3913BCF1FE718A3C5209FC2AD90D44D312F593,
	UnityTlsStream_CreateContext_m113223853E0216291BEE53F8DCEACC7E3BECE451,
	X509ChainImplUnityTls__ctor_mD653A56ED8AD47C0A46DB3D89749A055D4BB1ABC,
	X509ChainImplUnityTls_get_IsValid_mC01A80C0AF85FC18A5C790EF35586C8503D6B36F,
	X509ChainImplUnityTls_get_NativeCertificateChain_mBFE146631BDB57BB66B00EBB21414AC7AFF73E72,
	X509ChainImplUnityTls_get_ChainElements_m88A7F21DC30467829291CF89448C65025D7A15A4,
	X509ChainImplUnityTls_AddStatus_mF8C26841864A662C616E5A94A762EA3A19EF71D4,
	X509ChainImplUnityTls_get_ChainPolicy_m6FF8496FAA1CCC30DA4691A4CFCC1C6F8E7939B6,
	X509ChainImplUnityTls_Build_m9B33FDCF42AE118B28749FDE388231A6E6316768,
	X509ChainImplUnityTls_Reset_m1AD27C4C44348EADFBF5C6CF802F8AF60B562474,
	X509ChainImplUnityTls_Dispose_m8E23D2EF6C322FF694E5E6098A52FB54388FC446,
	BufferOffsetSize_get_EndOffset_m114650CE0C422157F8EDCD8C0F1B89C5FFE7F9C9,
	BufferOffsetSize_get_Remaining_mB3CB4287B2AAAD7976374B914E95ABFCE71B1A4E,
	BufferOffsetSize__ctor_mB62BB1F461998563FB6793F509FF26FB79B81955,
	BufferOffsetSize_ToString_m2C7B9A215EDE5C22ED8C51534365E110BB56C083,
	BufferOffsetSize2__ctor_m773DC687399F95F23BE42179B942303C0B52FF07,
	BufferOffsetSize2_Reset_m1B80AE03ED0C2BAC447F3BDEC8F827CEE6053E7F,
	BufferOffsetSize2_MakeRoom_mCE9261D07E745C72E3032A9FDC19FA54FEF0BE28,
	BufferOffsetSize2_AppendData_mA85DD811BF73EB8F153F9DB2668C11E907F258A5,
	AsyncProtocolResult_get_UserResult_mEDF85928DEE138CA072E0BADB697024616981ABF,
	AsyncProtocolResult_get_Error_m2A01329BA71151C59E17E3D8812932AD02EF5C05,
	AsyncProtocolResult__ctor_m2999944493F7C75F19D5BA3811118ED10A889305,
	AsyncProtocolResult__ctor_m80187A7036019F9768BA1941362CB6785F10E99F,
	AsyncProtocolRequest_get_Parent_m20DA5526115969E9D32B61999A044FF0618AFF92,
	AsyncProtocolRequest_get_RunSynchronously_m5E10561209866EE44A27D35A1F4762008063B4BF,
	AsyncProtocolRequest_get_Name_m3D5DC63A43FFCA24B1E5A9825B76BD391D790F84,
	AsyncProtocolRequest_get_UserResult_mA77AFFB0D9A5FD69E5EEC51CB0F80C25E31D0759,
	AsyncProtocolRequest_set_UserResult_mD84274B3348783A6E03B9000BB82E0737E1EC974,
	AsyncProtocolRequest__ctor_mDF1F675DC3E001867DFFD9B1C14CF5559BE3035A,
	AsyncProtocolRequest_RequestRead_mD55DC015A50E42EF2FA7DB75FE1E7E3FCFA3CFA0,
	AsyncProtocolRequest_RequestWrite_mA6AB8ACEA16A14CD6A232DA256C417CD183EE69F,
	AsyncProtocolRequest_StartOperation_m3BFF3E84B25BBE13881D3592FF4CAEC3736FBEDD,
	AsyncProtocolRequest_ProcessOperation_m7210D0C2351D69FF9FF798860C85FAA7F91C050F,
	AsyncProtocolRequest_InnerRead_mF418FD1D9403BFD08A44D00EEAF8E9F3ECD25A02,
	NULL,
	AsyncProtocolRequest_ToString_m76F3449064B096829BE4288E38F10DF76B124F1F,
	U3CStartOperationU3Ed__23_MoveNext_mDE591A3806D9523FC8D2E26ED510CEE7963F8FF2,
	U3CStartOperationU3Ed__23_SetStateMachine_mA0C8C029457FAAE036C6098FDC66C98574D0BD03,
	U3CProcessOperationU3Ed__24_MoveNext_mA73F0FD80CE65D2E3E12390E49E3F34CFF47E98B,
	U3CProcessOperationU3Ed__24_SetStateMachine_mA9912D191EE4A99536A61DEED059DC89B765A823,
	U3CInnerReadU3Ed__25_MoveNext_mC4567E370C71DA9DA0F704C570308D106D4DDB2C,
	U3CInnerReadU3Ed__25_SetStateMachine_m15DB2E97371A46CBCCD53DCAE7A4FE6F5FF84DCE,
	AsyncHandshakeRequest__ctor_m741ADA23A424389AD20579B23AE2DBE8FA00CFF7,
	AsyncHandshakeRequest_Run_m345A76BCFDDE5E054B97BD4F5FE238A05D3F4C26,
	AsyncReadOrWriteRequest_get_UserBuffer_mD158BF301713ADC8899B3F3CFF7D18B76A956417,
	AsyncReadOrWriteRequest_get_CurrentSize_m4230AD05749403B840042BB7531124C6A96980E6,
	AsyncReadOrWriteRequest_set_CurrentSize_mBCD300A1CD6FF110A3527E80711A820FEFC2F9E1,
	AsyncReadOrWriteRequest__ctor_m4DCAC7C20097985A9A4FDCB73719DD2E508CF07D,
	AsyncReadOrWriteRequest_ToString_m1D1115D6B97BD767A3482863B6B64642D0733696,
	AsyncReadRequest__ctor_mE67D11354154F1844761E0FFB869BBF05F1AE741,
	AsyncReadRequest_Run_m2D98252200B926F53624E90AE9649767F0C110ED,
	AsyncWriteRequest__ctor_m627E554957F61C80484B10AD0D85849C1E9A0548,
	AsyncWriteRequest_Run_m10D193A545B0D8D5B68FED13056B0EE6099A92A0,
	ChainValidationHelper_GetInternalValidator_m9BF3791525A447EBA2283618A85A3E3C346205B7,
	ChainValidationHelper__ctor_m57CDE5571532D21835801745135CEE1ACF96A092,
	ChainValidationHelper_GetValidationCallback_m48153D51B837278F4F2E6DADE32FC5460C5F7935,
	ChainValidationHelper_DefaultSelectionCallback_m9662623B23235E63F7081F6632A3305D3C6C0220,
	ChainValidationHelper_get_Settings_m94E2DC79CB696A4F54956D812983E62FEB33D22E,
	ChainValidationHelper_SelectClientCertificate_m77B577FD626B93B7018F62E6843727168F7876EA,
	ChainValidationHelper_ValidateCertificate_m859E258B2CF6F570984C618241A26AAFE2B7104D,
	ChainValidationHelper_ValidateChain_m60FB1695D7C8F3A0C01C864FDFEC2EC3CF648F79,
	ChainValidationHelper_ValidateChain_m6E88B46B65F70D52BDD9438D218D7E3BE55BB1C7,
	ChainValidationHelper_InvokeCallback_m1E6AABFB2535B6E22428BDDC3252CE6ADB4D78CE,
	U3CU3Ec__DisplayClass11_0__ctor_m33705296E35208F621BECF6D7957DED9C7317D78,
	U3CU3Ec__DisplayClass11_0_U3CGetValidationCallbackU3Eb__0_m02FE972124C9B2EE3AE5CF1F1D4952D8DEF21D22,
	MobileAuthenticatedStream__ctor_mFA70F7AAAE52C6559DC554D363E5FFD4DE665CED,
	MobileAuthenticatedStream_get_SslStream_mAF94411F0FF2126C8D8C1E9AABBC8CDA7EB73087,
	MobileAuthenticatedStream_get_Settings_m21B268EC66FBAF762EC443B8FC784F1251444A96,
	MobileAuthenticatedStream_get_Provider_m9AE08411140D0CC27ADC979E0E290ECD70A47359,
	MobileAuthenticatedStream_get_TargetHost_m572DA701CB3CE3FA4006D5745F9A3B9A52739CAB,
	MobileAuthenticatedStream_set_TargetHost_m490855169728C94C8FEAD0689D2FA3E578046BB6,
	MobileAuthenticatedStream_CheckThrow_m9E35E722A793A807809AD00C9756997AB7561396,
	MobileAuthenticatedStream_GetSSPIException_m04F38FF55AEEC19087840F3A722D2C225050F427,
	MobileAuthenticatedStream_GetIOException_m80A6F9660142728B73A169E1B09AB8BE268DD517,
	MobileAuthenticatedStream_GetInternalError_m59D75502C4CAC329B87974927FF39AF718D15BDC,
	MobileAuthenticatedStream_GetInvalidNestedCallException_mC7EBE64F717F86F3D399AA54CC8C60F18876725D,
	MobileAuthenticatedStream_SetException_m490704BA8728AB01E7BA158FE545FD4D7C080448,
	MobileAuthenticatedStream_AuthenticateAsClient_mEB1237B52A4AB1FF3D7D3E52F61CE78A744973FC,
	MobileAuthenticatedStream_AuthenticateAsServer_m38A5035F798D754F9C460A3C4E2B90B673DD9091,
	MobileAuthenticatedStream_ProcessAuthentication_m0449087259B996D81ED6AC7A06ED19D46FAB0AF7,
	NULL,
	MobileAuthenticatedStream_Read_m6EC3DEF7CDD081863F58DC0E16120040DEB79622,
	MobileAuthenticatedStream_Write_mD339F8D2C127E4A82A7C22A0EE9AF82F0A07A942,
	MobileAuthenticatedStream_ReadAsync_mFA908C90B562D2C958DC1762A90F883DB3AC5F13,
	MobileAuthenticatedStream_WriteAsync_m764C5294A8B64C33B593CD388DC0BD9FFE120753,
	MobileAuthenticatedStream_StartOperation_m8E632D323D991C8393DE86E4EC3E02DCA5AB31AF,
	MobileAuthenticatedStream_InternalRead_m90FE0F2015227506EF5E6EA18E49EEA5E3A1C319,
	MobileAuthenticatedStream_InternalRead_m5989243EA8E865C58630A151F50FD9286501238A,
	MobileAuthenticatedStream_InternalWrite_mDCC759D5D81736625897B956239DBEEE8F7AE743,
	MobileAuthenticatedStream_InternalWrite_m0B6A441B8194AAE474EECE582DA3627AB26CFB54,
	MobileAuthenticatedStream_InnerRead_m62766E47172B5468B6934BED507A4EF4054396ED,
	MobileAuthenticatedStream_InnerWrite_m137287DEDB6C2BC5E8DDDAC999787FA5A0B4B570,
	MobileAuthenticatedStream_ProcessHandshake_m30F67A233EAB7CE44A6B6E8E7F4E958DD888F63F,
	MobileAuthenticatedStream_ProcessRead_mB8FC6C872B55E0631A80824636220F3B73270441,
	MobileAuthenticatedStream_ProcessWrite_mC26A594E550853A399B5AD111CE704D06C500992,
	MobileAuthenticatedStream_get_IsAuthenticated_m46A9D2671C2864D1F5C559FC6C5284701BA8AB1E,
	MobileAuthenticatedStream_Dispose_m0D094AAD63D7E66A3F88EA02B58CC039BF9D0F52,
	MobileAuthenticatedStream_Flush_mCD0E572BA47A644E4B22167074AC8B4B926E6C60,
	MobileAuthenticatedStream_Seek_mC6C077D712D53180D6D3676CCAB67810A57041DE,
	MobileAuthenticatedStream_SetLength_m46CDB4C9A73652B8DF812B305342C6874B063A60,
	MobileAuthenticatedStream_get_CanRead_m206F7DA5CC6E445B2E23678344AD847C72929BED,
	MobileAuthenticatedStream_get_CanWrite_m6FBD6254C19E3391BAC2EFFDE46005BB0CDDEA84,
	MobileAuthenticatedStream_get_CanSeek_mF7A4E5EC7EAFCAA07CBDD7D35B1A764177612503,
	MobileAuthenticatedStream_get_Length_mF8ACA717EFBD5E1E15B9ABBEFC9F5EC41127469C,
	MobileAuthenticatedStream_get_Position_mDC516AFB4C932E61DA6DBFEF056CD9D798129035,
	MobileAuthenticatedStream_set_Position_mDA69F8B3575A0D6E3B11621BCB334371E80B9D56,
	MobileAuthenticatedStream__cctor_mAE00872557415DE85E6CB57122CAD50E9F3080FE,
	MobileAuthenticatedStream_U3CInnerWriteU3Eb__67_0_mEC1BC0A19130E44F6E542F229CE3FFAC0CD78697,
	U3CProcessAuthenticationU3Ed__48_MoveNext_m982934120035A46C0FB96F7DF55F7FA0822B4940,
	U3CProcessAuthenticationU3Ed__48_SetStateMachine_mC87BA3C30B7501EF0D0435E4C2C08EE79238576F,
	U3CStartOperationU3Ed__57_MoveNext_m2CD8CDF02548ACC068670F3F670590EEC77F7853,
	U3CStartOperationU3Ed__57_SetStateMachine_m1EC071633621977A0623BF79A53028A2A766BED9,
	U3CU3Ec__DisplayClass66_0__ctor_m0649BF5458CDCAB6017C443988C36967E3000993,
	U3CU3Ec__DisplayClass66_0_U3CInnerReadU3Eb__0_m3F3293E1DD48D7532F3B8D84D7C8402698D60F36,
	U3CInnerReadU3Ed__66_MoveNext_m5DFDC2139602BBC2F86D14BDA64B4A49ECDD90EB,
	U3CInnerReadU3Ed__66_SetStateMachine_mBA42C0B565B5D3F43D109D2A8CC66EE8CE9BC920,
	U3CInnerWriteU3Ed__67_MoveNext_mDF6F0FEA0FCAE97EEB2E5A2546446C00674CCEA3,
	U3CInnerWriteU3Ed__67_SetStateMachine_m7448CF503BCD3FEC38241FB22DBA3AB4B91FF5B1,
	MobileTlsContext__ctor_mAB891D57AD3DD87C68B97E458DCE7C03634B6404,
	MobileTlsContext_get_Options_m27C5391B99A94D7411FE2732BE3A49C4525862D4,
	MobileTlsContext_get_Parent_mC509F430FE5581A104C62FB3A08AE9D97DE19098,
	MobileTlsContext_get_Settings_m68D8D1488C1778311BE2EE65F549081B0E7D362C,
	NULL,
	MobileTlsContext_get_IsServer_m79E765F4FA8DAF1C98322134D48F6CACBEC824B7,
	MobileTlsContext_get_TargetHost_m82FF5791372BEF41AC5E73E368824231CC4F5528,
	MobileTlsContext_get_ServerName_m3CF7B9F7D249B9B2F553B5A72F439B5502229BF3,
	MobileTlsContext_get_AskForClientCertificate_mABDEDBD5B9CA52FCAD16A9C271C73FA05218B767,
	MobileTlsContext_get_EnabledProtocols_mE69073C19E4863B1EE7AA959E0804B18E169C85B,
	MobileTlsContext_get_ClientCertificates_m675A2D5E4F7FCB85B4B412A5659953184065C9EE,
	MobileTlsContext_GetProtocolVersions_m96D2EDF568161025E00CAEC740905545998BB36E,
	NULL,
	NULL,
	NULL,
	MobileTlsContext_get_LocalServerCertificate_m20FDC85F1F18C42644D48872EBAF9A6247624E09,
	MobileTlsContext_set_LocalServerCertificate_m32343DBF091AD80FA29DD8184C0C99C72FE43FEF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MobileTlsContext_ValidateCertificate_m7A7661E339AC2252062B3F7224621C9FACFEA234,
	MobileTlsContext_SelectServerCertificate_m393E610B8E9273895A4E691DC8C78295FD563F2B,
	MobileTlsContext_SelectClientCertificate_mF12113474A7EF6BDC8F7119D418722173A4EBDC1,
	NULL,
	MobileTlsContext_Dispose_m7C7D11F95AC2F7663D397D6FA4ECF82ECC7C0F9A,
	MobileTlsContext_Dispose_m020A795EE75CF99916490B91697E0EB31C5A2F94,
	MobileTlsContext_Finalize_mC58200C1813B8458AD3DB37BC15338F2AC750DF9,
	NULL,
	NULL,
	MobileTlsProvider__ctor_m31D6E547C8B388F136D323088D376FD54FE69B68,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MonoSslAuthenticationOptions_get_ServerCertSelectionDelegate_m8F35626050A015130D823BA68862416889F75A42,
	MonoSslAuthenticationOptions__ctor_mF6A5D221635C5E1ACF430108C1A11538E9F5349C,
	MonoSslClientAuthenticationOptions_get_Options_m67D736F913351C90F780A66F1A7C40832825D268,
	MonoSslClientAuthenticationOptions_get_ServerMode_m7D7A5D3F111161ABF85FBFA44615F184880FBC44,
	MonoSslClientAuthenticationOptions__ctor_mFF45688A74504467AD6CC5CC1F416EAB53DDDDE1,
	MonoSslClientAuthenticationOptions_get_AllowRenegotiation_mA6C88EE0477C658289190FD7E50A71990BD8AEEF,
	MonoSslClientAuthenticationOptions_set_CertificateRevocationCheckMode_m99D8AF1B0AC110FA167DF8614E485EABC97E35B0,
	MonoSslClientAuthenticationOptions_set_EncryptionPolicy_mA3DBA3D924CD48EFE642EC7AE41D194500803F95,
	MonoSslClientAuthenticationOptions_get_EnabledSslProtocols_m03F598BB877EDC3B07069EC3D5CD5D51464355F1,
	MonoSslClientAuthenticationOptions_set_EnabledSslProtocols_m5D27CFC1927D070DAC713CE4F933E3565E753149,
	MonoSslClientAuthenticationOptions_get_TargetHost_m200D056C2694EC25E422728E35388E345889DB7B,
	MonoSslClientAuthenticationOptions_set_TargetHost_m069D8AB5AFF0FEE83E1C44770D3F3DE2D3CA8795,
	MonoSslClientAuthenticationOptions_get_ClientCertificateRequired_m9AE71A7C9696A450375687F64B4B8E69DEF0BD3F,
	MonoSslClientAuthenticationOptions_set_ClientCertificateRequired_m8768F1FD719CB1CE9970684B1CEF731DAF5FA266,
	MonoSslClientAuthenticationOptions_get_ClientCertificates_m57BF4F92084EAAFFEF541E89D7ED519532DAEC0E,
	MonoSslClientAuthenticationOptions_set_ClientCertificates_mA5CD1A0990D617B249AE37CD4B49C53DB37B59A5,
	MonoSslClientAuthenticationOptions_get_ServerCertificate_m7CD9CC5BF55B0C7749CDF0C813C9ACA4852B2985,
	MonoSslClientAuthenticationOptions_set_ServerCertificate_m1806D490852347DFAD35312C92722765F98311F6,
	MonoSslServerAuthenticationOptions_get_Options_m2D07A52747C99A865F99FCE5BE8BBE439064CF8D,
	MonoSslServerAuthenticationOptions_get_ServerMode_mB4B5BBB94D83E6C0CBF7A609EA1E41295261A788,
	MonoSslServerAuthenticationOptions__ctor_m211250606F22155D74CF67A2B7C22BC2D8FD6D09,
	MonoSslServerAuthenticationOptions_get_AllowRenegotiation_mFDE89EBE20D491413C6DA1725BE48C8A6D122692,
	MonoSslServerAuthenticationOptions_set_CertificateRevocationCheckMode_m6791C99760134F30DC6FAFD2AA4A5ED4BE22F67C,
	MonoSslServerAuthenticationOptions_set_EncryptionPolicy_m92DE237BB4391DCDCD2BC2EBB645C544A0F10D9A,
	MonoSslServerAuthenticationOptions_get_EnabledSslProtocols_m09F833488BD7B306A97E09715877C024C6E4FCD5,
	MonoSslServerAuthenticationOptions_set_EnabledSslProtocols_m3D5F51C998613D004EDC32998C26E895AF60CA5A,
	MonoSslServerAuthenticationOptions_get_ClientCertificateRequired_m02CC06C33754C266C56A2FBB4D88BC3E16A3E7C1,
	MonoSslServerAuthenticationOptions_set_ClientCertificateRequired_m40CFC42CA71E7FDED3293CA213CE0187EDD908D2,
	MonoSslServerAuthenticationOptions_get_TargetHost_mF15AC1BA010ADB337151222436D0DAE9F6DD1B63,
	MonoSslServerAuthenticationOptions_set_TargetHost_m7FB1CB6CB8135731D73500E4BC59EADDBB311132,
	MonoSslServerAuthenticationOptions_get_ServerCertificate_mAC142235AC40CA569A058861AF65CB5B744C6EE1,
	MonoSslServerAuthenticationOptions_set_ServerCertificate_mBC83A44B22D50324A5F4CC3AFAC4158148FE3674,
	MonoSslServerAuthenticationOptions_get_ClientCertificates_m4451AAD3DDD09C5FA0A1466C358F07EBDFD81130,
	MonoSslServerAuthenticationOptions_set_ClientCertificates_m222B1EDF6597135FFB4CDDB78C7246F34C4D4089,
	MonoTlsProviderFactory_GetProviderInternal_m87C87ACEF272F007119C23121CD0D2E183785546,
	MonoTlsProviderFactory_InitializeInternal_mB0843306BA58C8CBDE17485CBFCE45BAD8CFA356,
	MonoTlsProviderFactory_LookupProvider_m842219B5A2493A9AA3B4D1911A1D77EB2F260DD0,
	MonoTlsProviderFactory_InitializeProviderRegistration_mDC8AEDEE018FFC5374E746A72B55A337B675CB82,
	MonoTlsProviderFactory_PopulateUnityProviders_m5A63191876243DF78B41ACDFA473F61C8B2243F3,
	MonoTlsProviderFactory_PopulateProviders_mF2AB544C94407E541767C11141D5876CB079F838,
	MonoTlsProviderFactory_IsBtlsSupported_m3AC62A64C40E98295932BFB36ABF44776241D780,
	MonoTlsProviderFactory_CreateDefaultProviderImpl_m674088AF2FC01E08CEE18728A7BBACDD9A7C2182,
	MonoTlsProviderFactory_GetProvider_m8CDA0A014E43C98804347DB5384F9C05BBA26FEE,
	MonoTlsProviderFactory__cctor_mA2C103AE727F17D8706F964DB378FB0644CA70D1,
	MonoTlsStream_get_Request_m5F3CDE9EA1921F1A45C0FAE20DD4BA634D0D8D83,
	MonoTlsStream_set_CertificateValidationFailed_m225E1AA3894B292682A2BF06B886693A11DEAFEC,
	MonoTlsStream_Dispose_m955C2295BC9317F9E6F756164528EE530E0032FC,
	MonoTlsStream_CloseSslStream_m82584A00391A5ACA6EC81C4848BBC7F92F801278,
	NoReflectionHelper_GetProvider_m07746C3FA62A325DCC22169E40CC5353A93F2A23,
	SystemCertificateValidator__cctor_m6C2E43AD95D74AF470013B88B823825B13937E30,
	SystemCertificateValidator_NeedsChain_mEA1D649FC72CD6D61ACECF098C7CC871AEC5A215,
	CallbackHelpers_PublicToMono_m4B0ED4D1F9D6AC15E14D09F55642889B57CAE950,
	CallbackHelpers_MonoToInternal_m895AC89217D5876DB6CC95D7C656C9BA13D4D618,
	U3CU3Ec__DisplayClass0_0__ctor_m31154BF78DAACBD45BE85524DB60A0C96A1B2A12,
	U3CU3Ec__DisplayClass0_0_U3CPublicToMonoU3Eb__0_m1C478ECA2AAAF7CF24DE679072777B23441DEFC8,
	U3CU3Ec__DisplayClass6_0__ctor_mD1E04FD28115DBD711A881B26347464C27140E23,
	U3CU3Ec__DisplayClass6_0_U3CMonoToInternalU3Eb__0_mE37EA1777B5D0BCD42B36A717CF6FC0E18CD3AE9,
	MonoBtlsBio__ctor_m27999B362D4460C8CF038BF555FA4895B901AA5E,
	MonoBtlsBio_get_Handle_m447149554862449317739255B72B29685EF522C7,
	MonoBtlsBio_mono_btls_bio_free_mD35E178BE5716B6F65A35065550F53CDF569A0AB,
	BoringBioHandle__ctor_m94DB76BFFBAFAAC740B93B8FEB2B9F26489725EA,
	BoringBioHandle_ReleaseHandle_mFC55F3883FC1B9759C03679E032DB8A967785FE9,
	MonoBtlsBioMemory_mono_btls_bio_mem_new_m888889BBAAFF564D6D030981355733781B45FE40,
	MonoBtlsBioMemory_mono_btls_bio_mem_get_data_m68ECECE7237880D38B52B934CD42A2D64AD2FA59,
	MonoBtlsBioMemory__ctor_mA8389F42E3A775CB226FE64F143169BCE25018A7,
	MonoBtlsBioMemory_GetData_m3DA5A5971C27CAF5694AA01DA1A9136379A8147A,
	NULL,
	NULL,
	NULL,
	NULL,
	MonoBtlsBioMono__ctor_m1D9C5AB7F2108D6C7E9E817181EF0516F2BFD965,
	MonoBtlsBioMono_mono_btls_bio_mono_new_mF53600E459C525834E106EE628F6DE06DF1A8D1A,
	MonoBtlsBioMono_mono_btls_bio_mono_initialize_m0607739C885E2566FCAF4CD6153A745992F80102,
	MonoBtlsBioMono_Control_mF7578B78CC76CA1B0D2FE532B658EFE092BFB533,
	MonoBtlsBioMono_OnRead_mB3B07BB23F84F8FA457FC186112435B1206D48F1,
	MonoBtlsBioMono_OnRead_mE0FF0BFC134CD0F923BBC555E346B7DD7CAD8988,
	MonoBtlsBioMono_OnWrite_m13ED5B856803BC1AECF385A8A8DDE3728131240D,
	MonoBtlsBioMono_OnWrite_m605A12A8B0CE9517927656981580BFA330BED613,
	MonoBtlsBioMono_Control_mE6F92D680A96B8B446D566D221BF925954FF43BB,
	MonoBtlsBioMono_Close_m5A7EC104436F0CE6C30C116126361AD3E9EB0388,
	BioReadFunc__ctor_mF67045947F464E6421342B8F8C33D087121071EE,
	BioReadFunc_Invoke_m744CB0CD8BE679279438EF430FB0E2FD6DDDB116,
	BioWriteFunc__ctor_m3A5F86F45AF72BC45CEEDCB0CB4F8D6CAE430677,
	BioWriteFunc_Invoke_mBA1D9291830C4AC61989002AE28AA19329279027,
	BioControlFunc__ctor_mECCABB988AD4AB5F618BA15CBFA2E8038FAF47B8,
	BioControlFunc_Invoke_m9A7F407B786D5DBE70CE79098FE7A2C3C50F77B5,
	MonoBtlsContext__ctor_mFF1354F7D4DC6FF9884B01D72E440171BEB484E6,
	MonoBtlsContext_GetPrivateCertificate_m4DA83A2AA6134658390AAEEDD22D497527D69498,
	MonoBtlsContext_VerifyCallback_m72C449E20B2F9F5DC5EB079F1114FE71E5BD4D50,
	MonoBtlsContext_SelectCallback_mCC262DE516D940AD768EE975B0AB5E8E9A41A696,
	MonoBtlsContext_ServerNameCallback_mEC03DB70C67F21082D38A723593413619C6923E3,
	MonoBtlsContext_StartHandshake_mC61D033B4D1D8E6D19422FDAAC610D8ADAED2366,
	MonoBtlsContext_SetPrivateCertificate_m282944AF213BD8B0FB26C98622D3CB1503B81E3B,
	MonoBtlsContext_GetException_m9711314F100FA6FF002F56ABF4F4B7EC088D44CC,
	MonoBtlsContext_ProcessHandshake_m57F7BFA5557487E87D55C0F7A03001679850E4D1,
	MonoBtlsContext_DoProcessHandshake_m58BE2AD5EA649B65151234993D39D114EC04C7EF,
	MonoBtlsContext_FinishHandshake_m27B7D33BE2074849236F0CBB1741CF5D461361A5,
	MonoBtlsContext_InitializeConnection_m50A6DBB41A93C09A6CC23D6EB749368A6B0ED562,
	MonoBtlsContext_GetPeerCertificate_m4EE3A44FE9F0445EE728F46AC7D7C9D9AE2847EC,
	MonoBtlsContext_InitializeSession_m884FFEF19AC79E255090EBC4BF3A26FCB8167CF5,
	MonoBtlsContext_GetProtocol_mA09A372EDAD446DE4C74137BD9A496A6432B076E,
	MonoBtlsContext_Flush_m461CB922D21FF484E3D8C4605174EEB608AABB5E,
	MonoBtlsContext_Read_m7C39CAEF7B9AEA3B7A7119B959ED5D53B884797C,
	MonoBtlsContext_Write_mAC097BE01F8D6C0A5DF058E406DF4FC0312941E7,
	MonoBtlsContext_Renegotiate_m7D619E3469DE30895949BA0C4C5B4A87F359B070,
	MonoBtlsContext_Shutdown_m60EE94F649A4AD3CBA73373E073346AA27B90E9C,
	MonoBtlsContext_PendingRenegotiation_mF614A7BE29C70CCB70F7E84BEDA19A1A858F87B0,
	NULL,
	MonoBtlsContext_Dispose_m01F050EFECECB8198D614D51FF0123B70C82B8B0,
	MonoBtlsContext_Mono_Btls_IMonoBtlsBioMono_Read_mBADC4C379D8B6FB3ACA5068AE8147111DE1B5DEC,
	MonoBtlsContext_Mono_Btls_IMonoBtlsBioMono_Write_m67C5F3168753CEDA8CBB506A8A206D688D980C1C,
	MonoBtlsContext_Mono_Btls_IMonoBtlsBioMono_Flush_mC00CC6A40B9E148F9D63F8629D11F1DC0DA2A135,
	MonoBtlsContext_Mono_Btls_IMonoBtlsBioMono_Close_m830DA02726C9ADEB02B5AF73E39C22D43A847127,
	MonoBtlsContext_get_IsAuthenticated_m51942004793D9BC3D95DB7ABFD53CA2D4204D380,
	MonoBtlsContext_get_RemoteCertificate_mD114E4893BF27ED2FD87A9D86C36AA915DF7D4C9,
	MonoBtlsError_mono_btls_error_clear_error_mF5ADFE111AF96E5591ABBD14A06504A90C4B1B45,
	MonoBtlsError_mono_btls_error_get_error_line_m66FDF6BA20A7A1B7F0736F3C271BF4BE5959261C,
	MonoBtlsError_mono_btls_error_get_error_string_n_m8BA6D140E84E1F6E3FA891B1B86B44D10FDB5A3D,
	MonoBtlsError_mono_btls_error_get_reason_mA810B6689B39DC83767CD59EA99165B15A917D4D,
	MonoBtlsError_ClearError_m5D1732687E88DAAA64369CCA7FC40D5A71AC6878,
	MonoBtlsError_GetErrorString_m947E5B74ADAD69852AD20AEB8FDAD187157C2315,
	MonoBtlsError_GetError_m008CE301168688B62B435460D9727EEC4BA17314,
	MonoBtlsError_GetErrorReason_mAF26D002DE639B5AAC6D396B0ED09C70CA25FF9E,
	MonoBtlsException__ctor_m5DCEFB95BE77CDFF5A8D0DA19B45F41E46188E67,
	MonoBtlsException__ctor_mE99D10E7FF7F36DB79481CB71E6B1B3AF0EA2539,
	MonoBtlsException__ctor_m057F82E928B82D01EE415555AD059886A83808B0,
	MonoBtlsException__ctor_m86D66CEBF74847200F5205293C6B9421B7BBDB42,
	MonoBtlsKey_mono_btls_key_new_mFF76A62C86DFE90713115D98A2D6B9CAA93167FC,
	MonoBtlsKey_mono_btls_key_free_m09DC429805222FC2E7A9850A32F2674120E313EE,
	MonoBtlsKey_mono_btls_key_up_ref_m4DFC176C712A62F199FC933AF29A1F0B3A86E818,
	MonoBtlsKey_mono_btls_key_get_bytes_mCA5AF65D687CD9E0E172CAB83AFF419F66590369,
	MonoBtlsKey_mono_btls_key_assign_rsa_private_key_m589A698F64930277A6D0EF4993D041FC2759B7F4,
	MonoBtlsKey_get_Handle_m3F0EC3F04D1B2886BBD7896BEB3DE91E6D255E6C,
	MonoBtlsKey__ctor_m41A5A6E3975D3CD98F64CB8B83325D87DEB01DDA,
	MonoBtlsKey_GetBytes_mD3A4E69C8BF5D40AB6BD6C7CF1A4650DF9BF5097,
	MonoBtlsKey_Copy_m27EC5A8D495C066523DB4558D7D148D9F2C5436E,
	MonoBtlsKey_CreateFromRSAPrivateKey_mC544AA4E5CC589D60B6F12445782A0ECCBC9D4CF,
	BoringKeyHandle__ctor_m61DE09782CCD143D59C1FC4A29A3B3D83631A5AD,
	BoringKeyHandle_ReleaseHandle_mA9996A7F374DECC94D48660290157AB69179ADDD,
	MonoBtlsObject__ctor_m357D70EACFD7EF183FA3F59B02746ED649B8B36F,
	MonoBtlsObject_get_Handle_m929F55AA1CC6397BB81B37E33E6C295DF0F4B13E,
	MonoBtlsObject_get_IsValid_m81F9484EE0BB31515AC50158B182C87B8AB62B5F,
	MonoBtlsObject_CheckThrow_m3B61D9B98C8D6CF068CEEC067854D5A32C462FAD,
	MonoBtlsObject_SetException_mC444E69A432F5EBC62D37F29C5AF1E7F6480C01A,
	MonoBtlsObject_CheckError_m64C6CD56095272D4EC8E67D2807460800CB6B877,
	MonoBtlsObject_CheckError_mC13A0B4648920B0530835D424FC350B1D0351DEF,
	MonoBtlsObject_CheckLastError_m7E83AFAD66B4A04C528A019C790A7EA8F03D5CB4,
	MonoBtlsObject_mono_btls_free_mC1D61AA77F5B78D22DEF4D0DC0AD151DB26FF087,
	MonoBtlsObject_FreeDataPtr_m762822A138134A35091FFAE34129CD0596901E0D,
	MonoBtlsObject_Close_m721FFCCBC5320FF91B8F88651EC1013978B50A52,
	MonoBtlsObject_Dispose_m5D56F90D3CDD44E63432AEB0F41F67DD6EB3E353,
	MonoBtlsObject_Dispose_m8E78236AF0BB3D6848B882BFAA2D574D24E5EB1A,
	MonoBtlsObject_Finalize_mC56D7B0A1FEA92DDE1DBA0AF6C7715575CEBCE6A,
	MonoBtlsHandle__ctor_mFE1DF2FF7208BD4481638701BDEF9F2B556B652D,
	MonoBtlsHandle_get_IsInvalid_mAEC6C53A163B00D46C9C36BD2196230616F6F43C,
	MonoBtlsPkcs12_get_Handle_m7AA7DEEA89783B8B9ECFE1B9351677A9BE4AD11C,
	MonoBtlsPkcs12_mono_btls_pkcs12_free_mEEED21018C033E7E6438617B6ABC241CFD86B853,
	MonoBtlsPkcs12_mono_btls_pkcs12_new_m49A58BD1E7CF96008821E9AE0A7AAAB196C52D4E,
	MonoBtlsPkcs12_mono_btls_pkcs12_get_count_mDE026ECA3212C9690582648FBDFBC00887779186,
	MonoBtlsPkcs12_mono_btls_pkcs12_get_cert_m424422207D2723F4FAA1926E634D734F2E945574,
	MonoBtlsPkcs12_mono_btls_pkcs12_import_m1A9199FB8C08152D7420A0DDFCE061D7CB86530D,
	MonoBtlsPkcs12_mono_btls_pkcs12_has_private_key_mF5CCCADB1D9E03972CC913034C8010412021AB1B,
	MonoBtlsPkcs12_mono_btls_pkcs12_get_private_key_m2D7ABD2B2FBA68CB1D7FDC4EE993ECB95678537B,
	MonoBtlsPkcs12__ctor_m0443539C1F555E7933C9C11738803D4C4A19F7A9,
	MonoBtlsPkcs12_get_Count_m3B3AA72AB596F6F8D82B96F7B787C9AA718DF342,
	MonoBtlsPkcs12_GetCertificate_m3EF3675C309529933E88E3D52E695B5E81AB5C78,
	MonoBtlsPkcs12_Import_m7AE3F61C4E65DB51D4E6A266D42B80375B74C388,
	MonoBtlsPkcs12_get_HasPrivateKey_m18B31C7E511B1C69BFD4F0C24207AA197C370033,
	MonoBtlsPkcs12_GetPrivateKey_m4B69EE9C50095AD4E0F9D224F52973AFA6F4982E,
	BoringPkcs12Handle__ctor_m29FBAECE2230F319C049BEFD64157C107BDD5FF3,
	BoringPkcs12Handle_ReleaseHandle_m428BB05A57EF7DDCC5C35E185C1E869E06C9F8F4,
	MonoBtlsProvider_get_ID_mCA08C6D8B652E685A654B072C3901E5D7C17482A,
	MonoBtlsProvider_get_Name_mB84D1E1A27CFAEC85FCDE48160773A36EF10D544,
	MonoBtlsProvider__ctor_m3BAD59FA2D9BCA7A2B1F029FE689A56A07B0810E,
	MonoBtlsProvider_get_SupportsSslStream_mCA509641BE1177D1E2E5F774269A50A2124B28E4,
	MonoBtlsProvider_get_SupportsMonoExtensions_mD63E3CF2CDBBFD5B03541937C90670D5AC5D3F49,
	MonoBtlsProvider_get_SupportsConnectionInfo_m4EED7BD9FAA3ADD5D523AC635D0535C965DF2658,
	MonoBtlsProvider_get_SupportsCleanShutdown_m5489F01B86092220D84D502AF105E5034FE4A88E,
	MonoBtlsProvider_get_SupportedProtocols_m5D344FBA9F458952B8CB4FCFB609F4AEED56D80C,
	MonoBtlsProvider_CreateSslStream_m36F5514259018235AD66D7DB0C84E5D7E35428C2,
	MonoBtlsProvider_GetNativeCertificate_m5277ABC9E56ED50DA43CB9E20AC005C4A31F392A,
	MonoBtlsProvider_GetNativeCertificate_mFD2CBE90C78213D12B4F6A77BCA7E356D508F522,
	MonoBtlsProvider_GetNativeCertificate_m8CBBFBFC50A27D9AE00A738CDCE742A63ED40724,
	MonoBtlsProvider_GetVerifyParam_m4E0754CE4B0B9F9604E9996698168D5C8715D775,
	MonoBtlsProvider_ValidateCertificate_m0272E85178C08F977C093FEE3251CE5DFA9161E9,
	MonoBtlsProvider_ValidateCertificate_mDFB6088BC65546798D540127BDD4872C57D96E12,
	MonoBtlsProvider_CheckValidationResult_mADAFCE1E140B64E2BC7D6E233469960A3124C113,
	MonoBtlsProvider_MapVerifyErrorToChainStatus_m59B25FB5D5804BBC06AFFF9430D933BB75AA5143,
	MonoBtlsProvider_SetupCertificateStore_mB0E6DC01DE3C8D962A6B352C022B67FE95648640,
	MonoBtlsProvider_SetupDefaultCertificateStore_m1DC6267AC90D4E0C16C36219C2D3A29C5A8410E7,
	MonoBtlsProvider_AddUserStore_mFB218F3546B995CB7F72B94656ADDBC213F449A6,
	MonoBtlsProvider_AddMachineStore_m0D472A8BBDC3EFC8CFF14F1A487C0C1C45DBA28E,
	MonoBtlsProvider_AddTrustedRoots_m2BD630627150BDA01BBD22D12D9ED0E08115CD44,
	MonoBtlsProvider_CreateCertificate_m99776D7CD36710CDACE0FD72B86D4C0B1B8150D3,
	MonoBtlsProvider_GetManagedChain_m3A7BF034BF541D0232C8F6B40C2191D34FD5DD25,
	MonoBtlsProvider_GetBtlsCertificate_mAE23E9D7430A7F9FEE847269DFC37B4489C95EFD,
	MonoBtlsProvider_GetNativeChain_m2C514D3E480513FB201FF510A5EA3B769FF67A75,
	MonoBtlsVerifyCallback__ctor_m8F1128CB88D8E552034C35F1115F9642DB2A00CB,
	MonoBtlsVerifyCallback_Invoke_m825965B6B4C69CA0287ED84C20688B62BC5009DB,
	MonoBtlsSelectCallback__ctor_m7B7D62D2596DC675786AA7FA342F7188BB6D9834,
	MonoBtlsSelectCallback_Invoke_m758D66BCE6FFA5CF23148236694A0ADE7835F833,
	MonoBtlsServerNameCallback__ctor_m245B337F77B4B68D1DD967C5AC999F6950AFA039,
	MonoBtlsServerNameCallback_Invoke_m4AA0CCFC7FEEACD68818537DC6D829872A2B619C,
	MonoBtlsSsl_mono_btls_ssl_destroy_m0AFA7E41AC0D4502C2DED1D4F43D179B992E8D8F,
	MonoBtlsSsl_mono_btls_ssl_new_m5114FC79FE517C0A825E7EE033891A68DB098784,
	MonoBtlsSsl_mono_btls_ssl_use_certificate_m5F43AA1BA2388D702700762D2D619ED674650D4E,
	MonoBtlsSsl_mono_btls_ssl_use_private_key_mFE842927801E542794BA55118A94ACDB74415FE1,
	MonoBtlsSsl_mono_btls_ssl_add_chain_certificate_mAC4BEB909E6E286B069E36DA0F50D8A3C0C43C0E,
	MonoBtlsSsl_mono_btls_ssl_accept_mEF4E97286CC13D00C5B92E146EA886E258A892DC,
	MonoBtlsSsl_mono_btls_ssl_connect_m47EC7A445C611BE5ED51827D177D099AFFD05F10,
	MonoBtlsSsl_mono_btls_ssl_handshake_m7E16FC5F7F1800C74B54C1D78651FFF8D3800BFF,
	MonoBtlsSsl_mono_btls_ssl_close_mA473DEE6ED19B3D23DD0D63D82E4213D339E0484,
	MonoBtlsSsl_mono_btls_ssl_shutdown_mA15CB14FB516D07D3B7F358C076599E135417450,
	MonoBtlsSsl_mono_btls_ssl_set_quiet_shutdown_m5A553D7EC20BCFF8D5653C2777B679AFBD54E237,
	MonoBtlsSsl_mono_btls_ssl_set_bio_mF5A8F4790AA61B1FCA72D063F6A689957A72EF01,
	MonoBtlsSsl_mono_btls_ssl_read_m47C6ACDE0165F28162E26DEA766353AD12FFABA2,
	MonoBtlsSsl_mono_btls_ssl_write_m8BEF50C4A00E2E386021FF5BA4DB26BF26F94D8E,
	MonoBtlsSsl_mono_btls_ssl_get_error_mA76FBC1696B6F7A315998918EE8662B00850A84A,
	MonoBtlsSsl_mono_btls_ssl_get_version_m9D689AB91D22F5B7F086B27D6F662716AC1C3F46,
	MonoBtlsSsl_mono_btls_ssl_get_cipher_mD0F85F9EC48100506E63309B68931B087CB4B447,
	MonoBtlsSsl_mono_btls_ssl_get_peer_certificate_m125C12698C7C59D398513DBD2FB9FE11F543FC6D,
	MonoBtlsSsl_mono_btls_ssl_print_errors_cb_m96A1A5C32FDDFA460C49DEF5A46A965204E575DE,
	MonoBtlsSsl_mono_btls_ssl_set_server_name_mBA32BA41B58854F039990D3CDFD4E1C7B1592E51,
	MonoBtlsSsl_mono_btls_ssl_get_server_name_m0954C61331CCFEBCD971F088F8007D15E072071C,
	MonoBtlsSsl_mono_btls_ssl_set_renegotiate_mode_mA7D9D649F08A5DEFDAD0E8531FDB9B22BEDF202F,
	MonoBtlsSsl_mono_btls_ssl_renegotiate_pending_m581B2265253163ACCE6A50515544A30CCA3EB255,
	MonoBtlsSsl_Create_internal_mB7DBB50C4AE91B6347E11600F4BC53C94739FBED,
	MonoBtlsSsl__ctor_mE665FFA57F03F58B3516A2E113031E24E3AEB57C,
	MonoBtlsSsl_get_Handle_mD7D81179837A024646AF720D8E390C67ADFF2453,
	MonoBtlsSsl_SetBio_m12B1F338F813EEDFE846539A618CB47009BCC8A1,
	MonoBtlsSsl_ThrowError_m433E4D0C73EBC76A2DEE8A5DBC78AFCE07F40CCC,
	MonoBtlsSsl_GetError_mCD0707D0B720A563FA21F804ACC411922B79F994,
	MonoBtlsSsl_SetCertificate_mEB04028459613ADE7035551AAB5C015319D8568D,
	MonoBtlsSsl_SetPrivateKey_m73596A1183D40F136A5539E41F0269C264EC1182,
	MonoBtlsSsl_AddIntermediateCertificate_mE4967C4FC58AF0D78CB6F887667AFC6BFBC78028,
	MonoBtlsSsl_Accept_m2FFF1FA57235A2DCB0C04E3B5A11CC41D90F0485,
	MonoBtlsSsl_Connect_mFE6BEB2E52F8E6FFC8DCBBCE0405E0ED62553A77,
	MonoBtlsSsl_Handshake_m99EE68C3129DD273EBE5138ACC0D288F5D862BBB,
	MonoBtlsSsl_PrintErrorsCallback_mF161ADF7DBDB81C47BDD587C87E1275DEE5B7A45,
	MonoBtlsSsl_GetErrors_m608539CB7B246064023EE5BD33D782401D07DC40,
	MonoBtlsSsl_PrintErrors_mC20509AB8D7DEE3B6779B92C4DDF55CC211E95DC,
	MonoBtlsSsl_Read_mE903D7417006C8A9D77AACD96A73863DDF7050BB,
	MonoBtlsSsl_Write_mB6524341838C199A948FD6DA6B81DE3591655B5D,
	MonoBtlsSsl_GetVersion_m3EE8DEF9D9F0187848EFE277A29120D7F2C49726,
	MonoBtlsSsl_GetCipher_m394E4C612F94448B5C24F0EB0D6FAAACB85EE554,
	MonoBtlsSsl_GetPeerCertificate_mD0C0DB8E9C3B7E577EF88B1EB522364B3FC78B1B,
	MonoBtlsSsl_SetServerName_mB31156F4873EB863AC0172FE329DA0464FC55F47,
	MonoBtlsSsl_GetServerName_mE4D22C3884972C938F097D6B15AADDCA25B29048,
	MonoBtlsSsl_Shutdown_m703B0B4E90128785E6412DCF184BC4A9B01EF169,
	MonoBtlsSsl_SetQuietShutdown_m73C451BE677698E14C633EDC8F33A30F0E4B326D,
	MonoBtlsSsl_Close_mB59931062F42A522BD0281DFA0169FC724ABEC27,
	MonoBtlsSsl_SetRenegotiateMode_m3D1E1B6E4E1460680B43E35CB032C8571E0705DC,
	MonoBtlsSsl_RenegotiatePending_mF3477EE7E3F6DF2222F987F3F5889A120A45915D,
	BoringSslHandle__ctor_m0DA0F6EDC111D843B46B27029C94E5B1DEFB780C,
	BoringSslHandle_ReleaseHandle_m9701A56FF4D5ADFAE0624ABBB9157D20EF3DF5A8,
	PrintErrorsCallbackFunc__ctor_mD64624B7F3BC980A50DA2D168E8B354DBF173D20,
	PrintErrorsCallbackFunc_Invoke_m1C7DE7EF19681297B4451713E2706DB4414B57E6,
	MonoBtlsSslCtx_get_Handle_mB22E16E1A65DD17AE1714D71366C945B5C4616C5,
	MonoBtlsSslCtx_mono_btls_ssl_ctx_new_mA803FADA54CA1ED023AF761296ACC291F1CDACF3,
	MonoBtlsSslCtx_mono_btls_ssl_ctx_free_m1A7BE8FC9A5BA191A7265FACFCDC5F05CABCBB5B,
	MonoBtlsSslCtx_mono_btls_ssl_ctx_initialize_m9FC92A37AE4BCA9B6365082A39203A8B128BD1FF,
	MonoBtlsSslCtx_mono_btls_ssl_ctx_set_cert_verify_callback_m821C35D7B4DBC48ADD2629297367FDEA9FDCA8AB,
	MonoBtlsSslCtx_mono_btls_ssl_ctx_set_cert_select_callback_mE6BA9666ECC96DC257D1AF16706624202CF9C928,
	MonoBtlsSslCtx_mono_btls_ssl_ctx_set_min_version_mB656FCEDFCC04D6C49FA4DCB873046DD5B8B99A4,
	MonoBtlsSslCtx_mono_btls_ssl_ctx_set_max_version_mEF4057F269BFFD8B495C52AF53207BA5F669C890,
	MonoBtlsSslCtx_mono_btls_ssl_ctx_set_ciphers_m5264971DF99AE39A29279ED15A9429014660491F,
	MonoBtlsSslCtx_mono_btls_ssl_ctx_set_verify_param_m1DEFE9109A85CB4A8D1C889C530C17B9A4738999,
	MonoBtlsSslCtx_mono_btls_ssl_ctx_set_client_ca_list_mB1FB9F45EC726867A26E76423A6C92F51DF008F6,
	MonoBtlsSslCtx_mono_btls_ssl_ctx_set_server_name_callback_mCBCDD78039946B3C1DEF05EF8383C55247CF4C2B,
	MonoBtlsSslCtx__ctor_m3E69692A614EBED4C22B0E942121CBB38198D12B,
	MonoBtlsSslCtx__ctor_m5576AC303A8B0D1C2360CC2147579F35748BFF2D,
	MonoBtlsSslCtx_get_CertificateStore_m70BC1E033FDC7C6E1E57301670787F89EC20AE76,
	MonoBtlsSslCtx_VerifyCallback_mB0FE711DA675DBB6992CA8D4A9C7832458B48FDA,
	MonoBtlsSslCtx_NativeVerifyCallback_mFDF10D01CAAD040E50EE6F53FC1D4155EF696218,
	MonoBtlsSslCtx_NativeSelectCallback_m6CA34F71A6093C7260A9F066D6C9FB3CD0CC86DB,
	MonoBtlsSslCtx_CopyIssuers_m1EEE67A4577682033606F576823D8E6BC7795330,
	MonoBtlsSslCtx_SetVerifyCallback_m0D163F264758B0F20DEEE8218DD4D9E727AFD799,
	MonoBtlsSslCtx_SetSelectCallback_mF70092A4C30087EF4DCC62D13D0F4879DD6F3803,
	MonoBtlsSslCtx_SetMinVersion_m7853260977C5BF2F2B51D35C8E1E8A423EE6EC09,
	MonoBtlsSslCtx_SetMaxVersion_m51CF8D0C80F0CBE225C23B08D8222894E609CB6B,
	MonoBtlsSslCtx_SetCiphers_mC3EF46AFD033E9C4C62772C17E983B8240C9FCB5,
	MonoBtlsSslCtx_SetVerifyParam_m4D65EAA326696C938B22E9D96AEE91A02A294F64,
	MonoBtlsSslCtx_SetClientCertificateIssuers_m2E5E54CC451A210F235833240AC2B7A3989554CC,
	MonoBtlsSslCtx_SetServerNameCallback_m93CCA4BE25A5BE978B79EE6DC2150A447FE66D9D,
	MonoBtlsSslCtx_NativeServerNameCallback_mFF311CE931F6A075940D1B6828D1FB517AF03838,
	MonoBtlsSslCtx_Close_mB3C805DE72F5DD81D7F035E5F869412D870EF711,
	BoringSslCtxHandle__ctor_mF43CD26E987DABD205A2383B900C30A14BCF3C0F,
	BoringSslCtxHandle_ReleaseHandle_m1D261E3DA2C2B52CE8C0EA15587DB7193A56346C,
	NativeVerifyFunc__ctor_m57E97D28673DE3AA35DB5F421B50E4DEFA8CE5BE,
	NativeVerifyFunc_Invoke_m83D46A16DA70BD1D0BDEFE8B94F360B42C27ABED,
	NativeSelectFunc__ctor_mCC793EE6C123121F715F3CFD8C23A9A331169608,
	NativeSelectFunc_Invoke_m92191EA94B7513F04CEB52D1D2EEEB1DA3E78063,
	NativeServerNameFunc__ctor_mB04711B6D7A98DB9A773CC0041F6099EB3CBF171,
	NativeServerNameFunc_Invoke_mC6F38842460FFECBD5545EBD2B0115151283F0B4,
	MonoBtlsStream__ctor_mFF14FECC3A05B0FEA3779DDEE2B2486F4ADF9BCE,
	MonoBtlsStream_CreateContext_m738A83D66948E39D29AE299287E9E9D6695FD512,
	MonoBtlsUtils_Compare_mE8D3A803B86EBCF644E1B535FD3E64B881AA26E9,
	MonoBtlsUtils_AppendEntry_mFBB6CD4FB64E595EABD27AD9F0F764732B1FBA96,
	MonoBtlsUtils_FormatName_mA05387DD7121DE816001E82D3AD3DA73BCD00EC4,
	MonoBtlsUtils__cctor_m7677185E4B3BCD92B665FDAB6E014D98317D8E2E,
	MonoBtlsX509_get_Handle_m3FD0B08C08DC8EB7CAB3F19C54B14AA4A228B3B8,
	MonoBtlsX509__ctor_m05B9938DCE1DE8F04B7405D3EB10C63BF895BE6B,
	MonoBtlsX509_mono_btls_x509_up_ref_mCC6CE3CA7AE784DD9F1FB6583F1CB8B5351571DE,
	MonoBtlsX509_mono_btls_x509_from_data_m9EC8E0AEE86C66F5A3AF364275D1F2D1DEC3DB2A,
	MonoBtlsX509_mono_btls_x509_get_subject_name_mF3AD0FD1FD33BB61A230459BEF4FF5492A65871E,
	MonoBtlsX509_mono_btls_x509_get_raw_data_m2522AB2F57663D4193FD2D9E0D32F3200D7BC418,
	MonoBtlsX509_mono_btls_x509_cmp_mAE0EFAB10B563BB9BE09CA6EE557C8B205C01D24,
	MonoBtlsX509_mono_btls_x509_free_m61E30328A4F0976CAA4E8616417D7B9EF424A808,
	MonoBtlsX509_mono_btls_x509_add_explicit_trust_m1B974EF4F14629491F9478D5BBBBC491D43A7C69,
	MonoBtlsX509_Copy_mE36DCC64F4761F579F1A6C28996101B23506BB1B,
	MonoBtlsX509_LoadFromData_m2FACD8CC31EF32F86A20C2D6C25F750F3807D171,
	MonoBtlsX509_GetSubjectName_m6E405347CF8D3ED807EBD1C46CD38767C34556D3,
	MonoBtlsX509_GetSubjectNameHash_mDDEF7BAE05A848383436D739D74460B82D5B47FF,
	MonoBtlsX509_GetRawData_m4B52A8FF5917F7B0DB9B2E355623F315AC735906,
	MonoBtlsX509_Compare_m7197975E987FC0C0F66B6F5289B6BA48EEDFEF41,
	MonoBtlsX509_AddExplicitTrust_m328F124CDBFB8D5649F58868425B2D9617398466,
	BoringX509Handle__ctor_mBA555CCB295495BD93AEEEBC93DADB85CA4C1597,
	BoringX509Handle_ReleaseHandle_m44A35C0FD8E3302AFD84F0276BDD7566C9708A33,
	BoringX509Handle_StealHandle_m096A11B8EC57A9AF11BA983B31E152F4A70E45C7,
	MonoBtlsX509Chain_get_Handle_m4E499FE6819CFFF133A5BA29C8D5EBD81430A225,
	MonoBtlsX509Chain_mono_btls_x509_chain_new_mEC13DFD06CB21A809B1B94BF578E74890050A1C1,
	MonoBtlsX509Chain_mono_btls_x509_chain_get_count_m612614A1FA864F9329D7F6307E0291B9332A8314,
	MonoBtlsX509Chain_mono_btls_x509_chain_get_cert_m35035B0600C9CB527F96969A351A0A4350D078DA,
	MonoBtlsX509Chain_mono_btls_x509_chain_add_cert_mE706CEB043F75C9E1764D4105EB059C5748756AA,
	MonoBtlsX509Chain_mono_btls_x509_chain_up_ref_m24821A143013BA4DE2B09C5214A19B441D104723,
	MonoBtlsX509Chain_mono_btls_x509_chain_free_m2C99947F6A69FC697E3DBCB02DB632128A4E092E,
	MonoBtlsX509Chain__ctor_m68C61C0629C37B9A65BABBCDBCBA69707487E0D8,
	MonoBtlsX509Chain__ctor_m75184464AF4022BCD4D85FD3FF11C1F5B88791CE,
	MonoBtlsX509Chain_get_Count_mAF59CA42465F37BC94EB371C25430B0044C01ED0,
	MonoBtlsX509Chain_GetCertificate_m6432C0F179B74CCC54D9965C406491287DFC9036,
	MonoBtlsX509Chain_AddCertificate_mF7444AA20B0EA6C6E697DEC2316ACC9BD69CB518,
	MonoBtlsX509Chain_Copy_m91814D7F915489E580CA83905085ADC1BA386E9B,
	BoringX509ChainHandle__ctor_m5A09F89FBAB71A1FBC8731056BF9DF188B5EA1C3,
	BoringX509ChainHandle_ReleaseHandle_m6F1169066011A81E31BE0F33DD22F2174420715A,
	MonoBtlsX509Lookup_get_Handle_m919AF42E0D90C34EB73A118066CA6E101E50FB31,
	MonoBtlsX509Lookup_mono_btls_x509_lookup_new_m9950505CB930A051CA8C4F8C95E7253FAC17479C,
	MonoBtlsX509Lookup_mono_btls_x509_lookup_add_dir_m8EA72A40759E7762D0425FA77106F1AEC235D552,
	MonoBtlsX509Lookup_mono_btls_x509_lookup_add_mono_m2E25E7B16639FA61D02302C122AD05D0A408CAD4,
	MonoBtlsX509Lookup_mono_btls_x509_lookup_free_m53C7B263B1EB29CAFAF262D22956331A92085B67,
	MonoBtlsX509Lookup_mono_btls_x509_lookup_peek_lookup_m0E1B1395362841F47F3C1906A055961E4583DCE4,
	MonoBtlsX509Lookup_Create_internal_m8B83B63221A5E987780682B7E36DBC2E55A4F866,
	MonoBtlsX509Lookup__ctor_mAA8D65D64981E5E421EEDE651D7089AE5BE891A2,
	MonoBtlsX509Lookup_GetNativeLookup_m226D773FE350EA76156B6817E867F11DE5EC7F79,
	MonoBtlsX509Lookup_AddDirectory_mD3C4A2781FFAFCDDE7A5954EA18E6255D0327916,
	MonoBtlsX509Lookup_AddMono_m225B3DD90B16F4C7224057DBF7F71247B0718967,
	MonoBtlsX509Lookup_AddCertificate_m032B8F408E8C48D5A77E3F55FDA3EF0F13F20941,
	MonoBtlsX509Lookup_Close_mF6F5AFB74D46E995DDC9F5DED9D8C40D680FCA31,
	BoringX509LookupHandle__ctor_mC3377C6FDC82C379081CC89A03D3E66DBC28760E,
	BoringX509LookupHandle_ReleaseHandle_mA8F8F14635553D481B3581699FB1DFF5A60C43AD,
	MonoBtlsX509LookupMono_get_Handle_mD34446E542FA06D76191F4A5758C319B08186001,
	MonoBtlsX509LookupMono_mono_btls_x509_lookup_mono_new_m93F177FB5C5229037B85E61CA05A78F9FB2ABF20,
	MonoBtlsX509LookupMono_mono_btls_x509_lookup_mono_init_m53607769F147658FA2E4DD7729E52EBC01E6DB21,
	MonoBtlsX509LookupMono_mono_btls_x509_lookup_mono_free_m9BAE11DA94E995D2C61ECEBCB49AE66E668CA495,
	MonoBtlsX509LookupMono__ctor_m833FBC2503ABDCE001FC83F9BF5C03F3B8B51064,
	MonoBtlsX509LookupMono_Install_m06ED46E5FD3761842A9B7750D5693003155F7016,
	MonoBtlsX509LookupMono_AddCertificate_m2216A9F856CD64B208A17F228F1CDD39971FABC2,
	NULL,
	MonoBtlsX509LookupMono_OnGetBySubject_m15FE6FBEE9849C324E8E099172A581EABEB83FB3,
	MonoBtlsX509LookupMono_Close_mF6E1290CAC54656E312429B47DE52D1107E830F0,
	BoringX509LookupMonoHandle__ctor_mC6AC306800774EB0EEBE877AFF028335B3B8EAAE,
	BoringX509LookupMonoHandle_ReleaseHandle_m3167A5697F5E0975C6CCE2F8F18D96DC91CE65BD,
	BySubjectFunc__ctor_m716B9DBA0E032168201B3F2604C5AD2625F10561,
	BySubjectFunc_Invoke_m27F18158D124F9C4EEAE02223A4B2372B3301B96,
	MonoBtlsX509LookupMonoCollection__ctor_m997D5DCDBC344D684111875A5F99D4FD5991182C,
	MonoBtlsX509LookupMonoCollection_Initialize_mB847A317C0B5C871C79126EBE63E1B7174325C38,
	MonoBtlsX509LookupMonoCollection_OnGetBySubject_m119F2F82E11008A9688EA9ACE83A64FA6FD57067,
	MonoBtlsX509LookupMonoCollection_Close_m131578243929C528FD4E660CAF442AFF47AF3706,
	MonoBtlsX509Name_mono_btls_x509_name_hash_mAA7A940CB0BF150FB73225F53EBA9D427E22A155,
	MonoBtlsX509Name_mono_btls_x509_name_get_entry_count_mB15EEB87BD5FA234A4FA843C0AC4170E97726026,
	MonoBtlsX509Name_mono_btls_x509_name_get_entry_type_mD7DFF5C6D95DF744939AEB9BF2F1B4C2BDE37F58,
	MonoBtlsX509Name_mono_btls_x509_name_get_entry_oid_mBD127D617FEB7CB420D9216A2A99154DA0C47270,
	MonoBtlsX509Name_mono_btls_x509_name_get_entry_oid_data_m85F7C59ACC9FE93F146C628D838288799A7AE1C7,
	MonoBtlsX509Name_mono_btls_x509_name_get_entry_value_m7DA2D337402CF9E6FC97B31C7861483324971D41,
	MonoBtlsX509Name_mono_btls_x509_name_from_data_m6B0A715456411D4727EAFF338CBB74400925490E,
	MonoBtlsX509Name_mono_btls_x509_name_free_m4D32CE033146B665264781920D133F289BB8A52D,
	MonoBtlsX509Name_get_Handle_m6F6F858ABCFF67031A986F782E2945FDDEEA89E8,
	MonoBtlsX509Name__ctor_mE79B982F5B1CD10752351373FBE3CB981FC84629,
	MonoBtlsX509Name_GetHash_m951517C1A1EBC07ED7213E1EE1293D977A36163E,
	MonoBtlsX509Name_GetEntryCount_mC03F2E7C9DB042167E9AB04AEDB298B4F5E20C62,
	MonoBtlsX509Name_GetEntryType_mAECDAFFD3F52321C02ED185056C0B42FC62835BC,
	MonoBtlsX509Name_GetEntryOid_mFB2561A2D361280DB134B7C1C426FECB541061AA,
	MonoBtlsX509Name_GetEntryOidData_m7B4A22FC80FC24A0FCA68BAE76B2818B48EC8303,
	MonoBtlsX509Name_GetEntryValue_m9048C928E51BD319F44E55953584BB4A1403C8FF,
	MonoBtlsX509Name_CreateFromData_mF6906700D30B086DEE6D9A25ADFCFA1ECBA58D64,
	BoringX509NameHandle__ctor_m47C6D29BC27537D2A061E1EB231416E79AF08F6C,
	BoringX509NameHandle_ReleaseHandle_mD2170A02B59FFD9CE229236533ED39CF6A5D282D,
	MonoBtlsX509Store_get_Handle_m89AE801D5B269E2E046407E0635740F404FABFE8,
	MonoBtlsX509Store_mono_btls_x509_store_new_mF66BDE64C8126E1A047CD1A674D76740FD13FF1B,
	MonoBtlsX509Store_mono_btls_x509_store_from_ssl_ctx_m181AD147AC11B5F5805A36E58A4008AE4AAA5864,
	MonoBtlsX509Store_mono_btls_x509_store_add_cert_mF7B1C07406B8793DBF5E6D450CBB9A9D7BAA755C,
	MonoBtlsX509Store_mono_btls_x509_store_free_mD78608C3A44172D4A17295D6C5D74A5A673B55B7,
	MonoBtlsX509Store_Create_internal_m900BB0AE58DB23F957753C74B489997C7B8A89E5,
	MonoBtlsX509Store_Create_internal_mEC0FA7ED8C7DA3E3D167AD510AA4EAC7B8CA9068,
	MonoBtlsX509Store__ctor_m848B0907C7C945C9AC29E2DC67BB49D4AF29122D,
	MonoBtlsX509Store__ctor_mDCA08A7A262CE8FD4CB0C275A147FFB9CE67C82F,
	MonoBtlsX509Store_AddCertificate_mC8C028325E40B61A290CF9ED06E1624B78BB139B,
	MonoBtlsX509Store_AddLookup_m9C6FA4053BF3D69EB22FE35A30C7F5FB182CCD24,
	MonoBtlsX509Store_AddDirectoryLookup_m1CB4E7CA7977A369F224404306002170BF2CA3E1,
	MonoBtlsX509Store_AddCollection_m7E48F9B4620415231973398B2C5CB6860F1D13B1,
	MonoBtlsX509Store_Close_mFC090B8A071362ECED1DB24D3471824C0B018402,
	BoringX509StoreHandle__ctor_mB3C1BCAB5FF6B1292D07D5E04B32C0B176CA1E40,
	BoringX509StoreHandle_ReleaseHandle_mB2A57AB29100FC7A72B074E9FCC069E1ECB2A59C,
	MonoBtlsX509StoreCtx_get_Handle_m2E8C152DA7370B3EAA9CB2126ECA5CF2B30124C2,
	MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_new_m148D71307C5C2F68C623D0865BF6604169DFE608,
	MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_from_ptr_m0E4A355742DC4F59FE6ED1B5D49D65D3FAE9726E,
	MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_get_error_mBA2BD5503DDCF594F5C1BBB195615B8A32363B64,
	MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_get_chain_m09FE29F1E29A7FAA0F1A020991ACF5CEA1577F8C,
	MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_init_m21FC604D97D34897243452A3F5C4DC696195CB3E,
	MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_set_param_m0CE4AF656DA1350EB4BE3BCA238B3D7738D6C65F,
	MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_verify_cert_mF847C34B563D76E11C4281528F90D940C70025EC,
	MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_get_untrusted_m90312DE8BB483B0DF0F4BDCA8126C7733832BF4A,
	MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_up_ref_m1EF1553C379B5B37305016D6A9D20CFB6743986A,
	MonoBtlsX509StoreCtx_mono_btls_x509_store_ctx_free_mF805084F9C02FD101E3C92384556E877D797B6A2,
	MonoBtlsX509StoreCtx__ctor_m546652793A7C67EDD01661ECF54FD0ED3B50B75B,
	MonoBtlsX509StoreCtx_Create_internal_m0AF6A7EFECDD27A17CFA824DAC752038D242FFB8,
	MonoBtlsX509StoreCtx__ctor_m48FB15F8DD6681E5271FB29421DEAA89A1B6F196,
	MonoBtlsX509StoreCtx__ctor_mFD0079D6C6BD47357CD3E19BD121FD336AC17BFD,
	MonoBtlsX509StoreCtx_GetError_m0BDF8083E134E7F9B3DBB8D862B2422F36133D46,
	MonoBtlsX509StoreCtx_GetChain_m589378381689FF4870F149D405B03F6FBB8EEA4B,
	MonoBtlsX509StoreCtx_GetUntrusted_m97D215DD2183B70E764FC20D9A3EDFC1329F5B97,
	MonoBtlsX509StoreCtx_Initialize_mC15C81CDFF21089D13BE8383602CCFE10075D093,
	MonoBtlsX509StoreCtx_SetVerifyParam_mD1F431EC644C7400B85CB25323D8A63AAD195BFE,
	MonoBtlsX509StoreCtx_get_VerifyResult_m46002D2AE1A138E54EBE5D5C002D8F58E690B732,
	MonoBtlsX509StoreCtx_Verify_mB035E88C6FD9D5A45B28A9494A4EAB860136725B,
	MonoBtlsX509StoreCtx_Copy_mEC93A3593445A8306AFCAE0864DF39682684407B,
	BoringX509StoreCtxHandle__ctor_mB110C57F474AE661F51673C31DE8C452AB565778,
	BoringX509StoreCtxHandle_ReleaseHandle_m387B2DFF84D204300011659B9F77E62CB1215C3E,
	MonoBtlsX509StoreManager_Initialize_m8219904DEF9F837D89908552B7A1E4861E7DBF72,
	MonoBtlsX509StoreManager_DoInitialize_m1E82BF3BADFA373C88A766912843EE7F1D96B6A5,
	MonoBtlsX509StoreManager_GetStorePath_mC53DDD09FADBA1DC7A382D97EA648C6B69E12635,
	MonoBtlsX509VerifyParam_get_Handle_m484BD6CF89C2F173000B8E2FC4442B13EEEC481B,
	MonoBtlsX509VerifyParam_mono_btls_x509_verify_param_copy_mF05B4613E5AA97EF7506064B95AD47B8DB5BB953,
	MonoBtlsX509VerifyParam_mono_btls_x509_verify_param_lookup_m914360F88B58BE56E7DDD807D705339131C9E296,
	MonoBtlsX509VerifyParam_mono_btls_x509_verify_param_can_modify_m857577AAE12C4CC171CC2EF3F720F6FA5CA9A2FD,
	MonoBtlsX509VerifyParam_mono_btls_x509_verify_param_set_host_m0F7B4789B71C7B327F7D703F50BF6C7460E147A2,
	MonoBtlsX509VerifyParam_mono_btls_x509_verify_param_set_time_m9C770883C0CB7DCFB55DC96B9A163A02F36528F4,
	MonoBtlsX509VerifyParam_mono_btls_x509_verify_param_free_m1A6DEE2E9AFCFE8A0A4E661F35BADF8A658C10B3,
	MonoBtlsX509VerifyParam__ctor_m4211A69035B76E9BCDA406F56407CB6F7B2F64FC,
	MonoBtlsX509VerifyParam_Copy_m8B543C8389E6D5D6DFB6AB4E1D1D91AA76D6876B,
	MonoBtlsX509VerifyParam_GetSslClient_mF8B59DC573161A26D2F9057A2001DD49E55168BC,
	MonoBtlsX509VerifyParam_GetSslServer_mA2B54BD4497A2E7734930BB4874E23F5F70DCE8D,
	MonoBtlsX509VerifyParam_Lookup_mFC83C380CCDE6B1358C61F092E8239B7CA2AD793,
	MonoBtlsX509VerifyParam_get_CanModify_m2B36671A3BFD3F534F2587C90C36C97449E6E8D9,
	MonoBtlsX509VerifyParam_WantToModify_m339A1A65C4A81F5C5D33B17C7C0AC40386A1692F,
	MonoBtlsX509VerifyParam_SetHost_m8FFFFEFD9A58FF78B8A02DA6BF41FD83496EF883,
	MonoBtlsX509VerifyParam_SetTime_m7CC7C7BB09B44E22991F3D1DC7945616B66C7C36,
	BoringX509VerifyParamHandle__ctor_mC7D9DD61778AE4EFE7B63B271F758122B9CD65E9,
	BoringX509VerifyParamHandle_ReleaseHandle_m22C5C9F57FA8D3072B3702A738447437FA970B56,
	X509CertificateImplBtls__ctor_m9B766C510B621586A897818C904AEA16DCEA1622,
	X509CertificateImplBtls__ctor_mBD1BDF7B84B633622363B567E28C52FC0F0BDE65,
	X509CertificateImplBtls__ctor_mCD39E4EB7778DC3DF261DC7DB368FC8D10161C74,
	X509CertificateImplBtls__ctor_mF19371D8B0AB5381EF0D30574D126293018CA24D,
	X509CertificateImplBtls_get_IsValid_mB7EF3B8732696CB911745EC8FD923D251860268B,
	X509CertificateImplBtls_get_X509_m96B07D0BA825233DE2E209EDD7970363605F87C7,
	X509CertificateImplBtls_get_NativePrivateKey_mD4E6CD38DAE4CBEC361BFFA6A62237453F178E43,
	X509CertificateImplBtls_Clone_mA1954F8E9B803C677836D3B3FD8E32FC5D2C0205,
	X509CertificateImplBtls_GetRawCertData_m0C76E3E3B3E7C1C3DD58F326C4D2DC73669415BE,
	X509CertificateImplBtls_get_IntermediateCertificates_mFEBC8F779F0638037FF604AE4B1CF7543B8755A2,
	X509CertificateImplBtls_Dispose_m4475832886CC96446D1EE4B314D962B8406980A0,
	X509CertificateImplBtls_get_HasPrivateKey_m33195867F1591B1CCEAB3BF987B81FBA0591C83D,
	X509CertificateImplBtls_get_PrivateKey_m59B1F91805D97572E85200AF66E405C6318F6FEF,
	X509CertificateImplBtls_set_PrivateKey_mD271BE07E8EC167512E651AB7D1142AA2ED21715,
	X509CertificateImplBtls_GetRSAPrivateKey_m73D73BEDF6FD28A6B0041A27ED1286003A626303,
	X509CertificateImplBtls_GetDSAPrivateKey_m23B3F54E2CD14841ABFFC18F39D7C3279819F4FA,
	X509CertificateImplBtls_Import_m5C23917D44D1E1BCF37A6957BA8102DC263D7D10,
	X509CertificateImplBtls_ImportPkcs12_m349710DD0FBBFDCC59064187312A21BA4ABC9FA6,
	X509CertificateImplBtls_ImportAuthenticode_mB6EC779B036A5BF1691B767D6F4BE9B6C9087506,
	X509CertificateImplBtls_Verify_mE17AABFD574B56D52916106F61BDAFAC98EAB249,
	X509ChainImplBtls__ctor_m9D5552CEA49BDD9BD694E3F3286F46214991EAC9,
	X509ChainImplBtls__ctor_m128255CC7901CDF5BFD9BE0D50087DC98E2F0E45,
	X509ChainImplBtls_get_IsValid_m32F89F48120D92C96E9708BD898F760FC7362F32,
	X509ChainImplBtls_get_StoreCtx_m4B1A68C79457CDA59D832677790DA780DCE6915E,
	X509ChainImplBtls_get_ChainElements_mEA430114878721A40F550DB39D35AEF0734D217D,
	X509ChainImplBtls_get_ChainPolicy_mF5DB0339D3EAE7F69964D1B6CA34A513A43B6408,
	X509ChainImplBtls_AddStatus_mE854AF70220C73FA841B6CC72A68EB99774CB536,
	X509ChainImplBtls_Build_m04F5DC966431BF7612DFBACC8A03DDB1B9B15BE5,
	X509ChainImplBtls_Reset_m024486185BBFECB79D652EDA88DF16590C25F6A9,
	X509ChainImplBtls_Dispose_mE4F62C67069BD1F3477E4C0FBB1198E4BDE46364,
	X509PalImplBtls__ctor_m3AF8288F8E0AF64AC97C373346A56E63DA8658AD,
	X509PalImplBtls_get_Provider_mE31DF32B169BC4E69C99349B93A70EF246EB89CD,
	X509PalImplBtls_Import_mE864AD400885ABD4939F7813FF41F872B95CD3F5,
	X509PalImplBtls_Import_m1582D613CDCAE1CB791B811B2E81EB3AEB2E021B,
	X509PalImplBtls_Import_m1EB2C0587BAD69FAAA8FCEF5AC0D4CDC8517B288,
	OidLookup_ToFriendlyName_m34B7595764843A001047D30E77B0DB1987653103,
	OidLookup_ToOid_mE362E659C0A532B8D2E1638D8B50FBACFEBFCF2D,
	OidLookup_ShouldUseCache_mB50BE42B791BF8314979BCF725BB50F5C1DF0461,
	OidLookup_NativeOidToFriendlyName_m54826DC2A46712C51D3BBE397372E01D0BCBDB34,
	OidLookup_NativeFriendlyNameToOid_m1CEF74ADB4771FEAFE0C491AC36DF806517C55AC,
	OidLookup__cctor_m315F5D4656A8C145CD154D9DF653F5207E12AEBC,
	U3CU3Ec__cctor_m2E929BF5DE0AAD59E0C0E2AA6875DFF57F976962,
	U3CU3Ec__ctor_m381C6259C7B5579501CBFB1E4D1DF201D5928470,
	U3CU3Ec_U3C_cctorU3Eb__10_0_m6CCA3946EAC08544EB57D764DBF2D23E12DE30A1,
	U3CU3Ec_U3C_cctorU3Eb__10_1_m7F86444F2B083758A76759BD54D31A3538101A4E,
	CertificateData__ctor_m623A235F902E7262CB9DA2CEB1BF5D3E64A6D23F,
	CertificateData_GetNameInfo_m8DC5068DE35A32AC641DCBA45C409D05C4CE9D72,
	CertificateData_GetSimpleNameInfo_mA631A4BFF5FA74EED7E44B2398323213F4505511,
	CertificateData_FindAltNameMatch_mE725F81D068254CD717F42AC769E934327BA6E13,
	CertificateData_ReadReverseRdns_mCC9E641803F520C0B6ABE2CF0D5D087F85E921D1,
	U3CReadReverseRdnsU3Ed__21__ctor_m5A6BC49C73B7B87A9A4400C6B15A6B08990A46D4,
	U3CReadReverseRdnsU3Ed__21_System_IDisposable_Dispose_mDB6F5E4B3207652BEAED7B42745D734D10C5249C,
	U3CReadReverseRdnsU3Ed__21_MoveNext_m9D224CA977CE0147564D800919EBB3CA3584DAC4,
	U3CReadReverseRdnsU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_get_Current_mF0FA23744F644CF1371214864DD2A43728064474,
	U3CReadReverseRdnsU3Ed__21_System_Collections_IEnumerator_Reset_m14F8B978364B8DDAC56367B68EEA3D7C06732564,
	U3CReadReverseRdnsU3Ed__21_System_Collections_IEnumerator_get_Current_m3CA95125EAEEBACD8685B90E65831D0B4B69EAA7,
	U3CReadReverseRdnsU3Ed__21_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_GetEnumerator_m19CB560BFA57F0DD069DCCA4F99AA3A67CEB5427,
	U3CReadReverseRdnsU3Ed__21_System_Collections_IEnumerable_GetEnumerator_mA9604E14B40705140F1AD16409F6716BDF3BFF08,
	IPv4AddressHelper_ParseHostNumber_mA778BE5D8E7B36AFF00BC3A042D532C5E4718B74,
	IPv4AddressHelper_IsValid_m1E626B5740F8C0F4F4AEA9D8D9E06933BD79C7AE,
	IPv4AddressHelper_ParseCanonical_m7BE948BF36EC5AD70DFFD320BD9EE8A343DA795E,
	IPv4AddressHelper_IsValidCanonical_mBE62564C4A9F8F584C9FBC0E6A5D5159071BE23E,
	IPv4AddressHelper_ParseNonCanonical_m4ACD00B972D7DBEB80BC3D7A9C36BB2CEA81638E,
	IPv4AddressHelper_ParseCanonicalName_m25D822E5AC4B56E40C2404C8DE1C03E70ABCF72C,
	IPv4AddressHelper_Parse_m61983BF8F1D7D827BABB3BD87AE4A2CE2BA92906,
	IPv6AddressHelper_FindCompressionRange_m52DF5ADCE15CB8AE6F141E829F1252EBB436A39F,
	IPv6AddressHelper_ShouldHaveIpv4Embedded_m59F598D7493A037117972E6D4AF8F233B7D87ABC,
	IPv6AddressHelper_IsValidStrict_m63AA667FE446A0F20109E4680BE64B9B60F73123,
	IPv6AddressHelper_Parse_mE09CDE68CBB5BE9195B68D52D79D81591C270CAF,
	IPv6AddressHelper_ParseCanonicalName_m148CB0071D79C29D97FE7E537B0BFA2CB27B709C,
	IPv6AddressHelper_IsLoopback_mE5795ECD1129E9EB2368E4A7D4EB1433CF51A3F4,
	IPv6AddressHelper_InternalIsValid_m23A4A563DF5766AC8391628217ADE8670A740D3D,
	IPv6AddressHelper_IsValid_mEA13C6DB64BF812706785DA99E9AFFBC95C1C3E7,
	NotImplemented_ByDesignWithMessage_m91D3B27BDC19FF6AAEDD420C9A8E3A43FB3B9DE3,
	UriBuilder__ctor_m1CBBB001D2542111D5B3334356E2AC65AFB80D27,
	UriBuilder__ctor_mD45945E23BE6C1AA8C56E64DEC181E36EE826C25,
	UriBuilder_Init_m5A6628A92532D3CAACD649C7528C15261C17A99D,
	UriBuilder_set_Host_m7CD9C7B0B9CACAF15A57ACFC363C766F19178930,
	UriBuilder_set_Port_mC6C31D266AA5790B8ED038C31489BCC2DE55DE3D,
	UriBuilder_set_Scheme_m02C71DED53B5BDB8CA03E7484B8B554D0EF4B3BA,
	UriBuilder_get_Uri_mD0DF3F7DDF7C56CC99E2D29E06C959F80A4171A1,
	UriBuilder_Equals_m2318FB925C63380B75D92D199A9C2AE4AA98F322,
	UriBuilder_GetHashCode_m8ECFACD3573A5E927B335A3443481248118C8310,
	UriBuilder_SetFieldsFromUri_mB6015DF5AA0D123A1B2749378D88FA26D409788E,
	UriBuilder_ToString_m0DEFF1C8981338B9D30B81620E839DCF2CFF4713,
	InvariantComparer__ctor_m42B33CC43C194F792B70B96258F930ECF46970A5,
	InvariantComparer_Compare_m74CBFBF1B02F2E9C149AC405D969DB2F4A802889,
	InvariantComparer__cctor_m319780C01DB316EE447344855F394630AF6F3C6F,
	IriHelper_CheckIriUnicodeRange_m47EF9FBD5FE075A464AE2A242FB43BEBC7C4F775,
	IriHelper_CheckIriUnicodeRange_m76D54508F06B0BD021CF86889C71C44578B495FB,
	IriHelper_CheckIsReserved_mFF947D4AEDC2D811E2B5379B9706901ED2B29868,
	IriHelper_EscapeUnescapeIri_mE7C6E85447A41E32A050856BC24B40279EDFF6E5,
	Uri_get_IsImplicitFile_m8D1A522158E4EB8AD8C077376D023ED1262998F6,
	Uri_get_IsUncOrDosPath_m33F2D8808AB44816518C1C464152D4B7872374F0,
	Uri_get_IsDosPath_m16357B61F5D111C45EF7D40276B96429E414F0D2,
	Uri_get_IsUncPath_m2D9E7E2DB4EA57EF268B9BA167A1389E18AE9835,
	Uri_get_HostType_mA30DC51045EE5FE94B3CB84B0D361A9C28EE572E,
	Uri_get_Syntax_mCBAC18B1DC71D269BF6C98417D923674DE258DC2,
	Uri_get_IsNotAbsoluteUri_m374185EB37A209B6102F955427913168C5543028,
	Uri_IriParsingStatic_m5FACD38C4EDB61BC6AC465759CDB56F24C34C3B8,
	Uri_get_AllowIdn_m612B747CE2F03FCA867D81F14710758B561209E7,
	Uri_AllowIdnStatic_m0B61B2A586F2BF870D185A1BD65151F52EFBC457,
	Uri_IsIntranet_m3AC6DBC7C35F309DFAD0951ABC7D95A8000548EE,
	Uri_get_UserDrivenParsing_mF2426529046B3DF3292A7AA2F4885F8D6591BBBD,
	Uri_SetUserDrivenParsing_mDE80D352D6343F50F152EB65EF142E84F7F79D69,
	Uri_get_SecuredPathIndex_m575B7B28B17F8F7EE3FA563ACF6390B0926732AD,
	Uri_NotAny_mB88A38A602D4A784CADBD43FDF4CDA0235371E81,
	Uri_InFact_mEEE8ADE0780C0712A3DEB8935BD6FC7B23F38A7B,
	Uri_StaticNotAny_mC49F6501AFE849E383AC6777CEB63D2B409B264E,
	Uri_StaticInFact_m85C3E521AD9916CC030AC01E311F79D90E981B05,
	Uri_EnsureUriInfo_m6FE387306AF82CDCFC56E2E9C2171C406D1FA982,
	Uri_EnsureParseRemaining_mDF7D4313A56047EB22457E12330C138156D54783,
	Uri_EnsureHostString_m7F5F3ED8488F83170A857CD51F0CFEB8322AA055,
	Uri__ctor_m6CA436E6AD2768A121FA851CBEEFA3623E849D3A,
	Uri__ctor_m24549041BC5661EAC10BA8CB35B60AD6512AF69B,
	Uri__ctor_mD7EC916948CBFE71F80298DDA6E4209724958B09,
	Uri_CreateUri_m9D81A5F3B332C44FDD875893EF619631708791F2,
	Uri__ctor_m5AA91BCDE21E8AD62B9A48948CF3661B2F00E38F,
	Uri_GetCombinedString_mA3D01108BC32A4594A578A4345BA0F012F554521,
	Uri_GetException_m9BD1AF317D6DE516C8D3909A0ECD460BA35996F7,
	Uri__ctor_mE1AB9FB0563073B8DFB381A69A54E9E0459D583F,
	Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m39D9B260E68B8715E9B693A125A2116CE14D760D,
	Uri_GetObjectData_m0D16C2BC060F0B88503DCC0EEACB151D43FA16CA,
	Uri_get_AbsolutePath_mABB93DD30D4C0F11948DE5C117650B1C3A9925CA,
	Uri_get_PrivateAbsolutePath_mF7B63FC5EBFAB1EE164BF868900B548962C5A546,
	Uri_get_AbsoluteUri_m080934F4F2E2160EBEABDF00F8B6D59888EA63AE,
	Uri_get_LocalPath_m60EE736E6F601238CBC20CE7DBB647C61F16DB0E,
	Uri_get_Authority_m5BE92DE43B1B4BCD6849FEBDE5530AB97E0C31EB,
	Uri_get_IsDefaultPort_m1BFC89B27306852E9AFDEF92897FD52B0B3ECD06,
	Uri_get_IsFile_m760E384ED177DC9197D5010B763F64196552DF5F,
	Uri_get_IsLoopback_m0E865DCC324F483C0C60E7D8E6FEF7AD0F46059A,
	Uri_get_PathAndQuery_m33B64A4AAF08510C0199CF5A85918B91D1008E5C,
	Uri_get_IsUnc_mB190B452D6A7B09FC8BDD967A5C38F32E87E0FCA,
	Uri_get_Host_m2C0E258C7DFF7A340049BE9BC08FF45E90988D8C,
	Uri_StaticIsFile_mDA17D91238AB2D1C549C55C524F67C714145D9CF,
	Uri_get_InitializeLock_mBC92A79EFBC775C9192BB5992F88AB868576D9CF,
	Uri_InitializeUriConfig_mADB8B77C49B053970C363E91FE4BDD8315401364,
	Uri_GetLocalPath_mDC900B37A0BEA02CAD7298582C2042221F9859DE,
	Uri_get_Port_m7A1413AC9D9A2FC5DC8A7F89DF4A01ACA0241A02,
	Uri_get_Query_m17DEC663263C486C8C8177AF9C4BFE2FFBE51178,
	Uri_get_Fragment_m3CB7AD14DA9618ADE406039D4713F67D6951D835,
	Uri_get_Scheme_m29106D5109538220B22FC49DE7B44040E51B0F6F,
	Uri_get_OriginalStringSwitched_mBE7152D5DA7A1D922B728B9EA8C64815B0469FB7,
	Uri_get_OriginalString_m3031F9054CA10F2C55C0E2415CC19810D360A5D6,
	Uri_get_DnsSafeHost_mE5001E7AE7F1879A2059024EA06C9031F6320A2B,
	Uri_get_IsAbsoluteUri_m2F9B759B85D295639D7959A616E1FBC203D756DF,
	Uri_get_UserEscaped_m5A376B8E2DDB815CAA2E34AF8890FBA943DC2085,
	Uri_get_UserInfo_mC0189C851F71ACF0B48A462E74C7F3EB86501C8D,
	Uri_IsGenDelim_mB6256291D6D90DAFB8B923D197BBD48A371F4265,
	Uri_CheckSchemeName_m00DF91AC5BC363425D403696C6251A6BCC5E9E62,
	Uri_IsHexDigit_m65CBC0215C67105D79C7B49FAF5DD4676B86ED20,
	Uri_FromHex_m16EF28AD07AF8D499C8157124A9E7C01D6FFFAA8,
	Uri_GetHashCode_m41959A208E51019C57C20014B2505993FB94B70C,
	Uri_ToString_mDD98375BF8CEBEF6E3708BBCA6C4AB0B54165DA2,
	Uri_op_Equality_mB299FA02A76FD12A781BCBAD53D65B73C1768682,
	Uri_op_Inequality_mC32A3382EF16D80BF39005BDD2AB452203D76D6E,
	Uri_Equals_m1BD19791DD8A42FEFF5844C29623626C2849C452,
	Uri_ParseScheme_m97A1B445A9528F29FC3007B02F2F8789AC201E82,
	Uri_ParseMinimal_m24D824EC77216C64F4B8B9175915CD6C3C6DB7B5,
	Uri_PrivateParseMinimal_m4D4C3CB26AC0DE8B0E1F7DC4143F2004D9D25B17,
	Uri_PrivateParseMinimalIri_mD8C7CA7EE14D994E915B48FB6A2CDE62253D4F1E,
	Uri_CreateUriInfo_m3B4F804AB6CB010246F2A52F123D6E44DECDE998,
	Uri_CreateHostString_mBBAF0ACD4E2EE673F8F7F4E416D87517D94245CF,
	Uri_CreateHostStringHelper_mB7076A86E12D57D5722AA4AC0993B38332A21C8D,
	Uri_GetHostViaCustomSyntax_m6632DAD1F66901E82C60A47D626A9140A586C802,
	Uri_GetParts_m432C93EAC8684C7410585D36DA00B04F6DC34357,
	Uri_GetEscapedParts_m0C33275AFEA210E5CAD22649E8B61B49FB43971E,
	Uri_GetUnescapedParts_m57F83B75B05C5D8EAB6B8ACCB6C8E4F0922CB7ED,
	Uri_ReCreateParts_mB622FFDF74676902D184FD96C4E6B740B65140AF,
	Uri_GetUriPartsFromUserString_m3D6EEB76609CCC54A0ABE65911CC24603A184B29,
	Uri_ParseRemaining_m9134D26360C9EEDFAFAFEE1ECA1A5CEE66BE5A3A,
	Uri_ParseSchemeCheckImplicitFile_m8244C40705A553AA88036339B231B1BB279580DC,
	Uri_CheckKnownSchemes_mB4E8E77DD15C70FACC9DA5078083E670B0EC9FA1,
	Uri_CheckSchemeSyntax_m7D9517CBE8B09F13A24AF4EEF0AE2D185C3E1C2D,
	Uri_CheckAuthorityHelper_m7946931160B7EC50E7167A11E570A4AF8AEB5B2E,
	Uri_CheckAuthorityHelperHandleDnsIri_mB737EC35D3A477668E6E289D09DFB43EAD4D4D62,
	Uri_CheckAuthorityHelperHandleAnyHostIri_mE38845CEFA41AB52AB949CAE2A43D77FB397C08D,
	Uri_FindEndOfComponent_m59CB1EFDA410B42014EA7DCDBA0C9FC9F5996E02,
	Uri_FindEndOfComponent_m198F7AC80BEA08E8DAA6B4577D8A872B41C519B1,
	Uri_CheckCanonical_m54AFA85AA00A9909C2EC01F026873F12532AD7F7,
	Uri_GetCanonicalPath_m66FA22F6E85D79E360A7EE86C0E03F1A009F6F99,
	Uri_UnescapeOnly_m16364432252C3908D378B0BC2BC37F9AD5CED9A6,
	Uri_Compress_m0A24E0C5DD7555EA7723C37FF428B0B1CA6B976D,
	Uri_CalculateCaseInsensitiveHashCode_mC84DC2B6AE39DB00143C7DE8BD0CDA05872635A5,
	Uri_CombineUri_m0A99C4A1F80FF7E6C8747545BB4610ABC4B4F9D1,
	Uri_get_HasAuthority_m2180FC17FB6FE7A11EBA0BDE1EF5EB9EF808A909,
	Uri_IsLWS_m16D38DDA4D0A7CD3899DEEE31CEF160EAF2C3A44,
	Uri_IsAsciiLetter_mFDC33D2D538AFF6A5819B73CB555709865ADE841,
	Uri_IsAsciiLetterOrDigit_m44901130AFF9E14663C5F9E6437BE44484A82BB8,
	Uri_IsBidiControlCharacter_m17A486D6ED9263488C6F4B3F3EDF1BF95352765E,
	Uri_StripBidiControlCharacter_m259E2D3AA84B455ADD63DFBC4EC802241E4317FA,
	Uri_CreateThis_m522F36D576D9A8803410439127DB32837331BA3A,
	Uri_InitializeUri_m044EA7C6188B132874B4522D5CA41EEFDB9A1718,
	Uri_CheckForConfigLoad_mC7297EBC00884B1793FDB16F79BF0FCFD53E9FA7,
	Uri_CheckForUnicode_mC763EF0DDC61EE359A6256B95209736EC0353AA1,
	Uri_CheckForEscapedUnreserved_mCEF702F733C72BE825E9FEBBEA751540A6A78D11,
	Uri_TryCreate_mBDD7F98C3BEBD9C3936DDF7960533CBB56478FDF,
	Uri_GetComponents_m30E3EAD4E9A39939456E764FEF94AFD4D6B7532A,
	Uri_UnescapeDataString_mD0A4732F7ED9CA5DB9E46D802971B5406094CC77,
	Uri_EscapeUnescapeIri_m14C0CD1A4C14E2458685E5A10049204B1A15C16C,
	Uri__ctor_m6EA3AF49C103A3D56EBBB24BCAEE8DE7E8676AF4,
	Uri_CreateHelper_mC4FC141C7E569879417FFC24BCB47E22CE09163B,
	Uri_ResolveHelper_m8FD2A09E63507DB5178AE3C182C323E8EA4B8752,
	Uri_GetRelativeSerializationString_mDEB0AD1094EC2842C2F0830C1E43FF2FFDEB6F5B,
	Uri_GetComponentsHelper_m83D8E4A027E180AFB34BA5290A8FF381EBCBCB75,
	Uri_CreateThisFromUri_m986BB8F1CF8D75EEFA12D9D0963BA927FA394BE5,
	Uri__cctor_mED6507CF356205205DE18F0D9D2A660D4C094F23,
	UriInfo__ctor_m88A6E3E7164B78B18C70D83CA5AD734517D308B3,
	MoreInfo__ctor_m4AC1286A4D4DAD0AB0EE99A2C952D54BFA5F44A7,
	UriFormatException__ctor_m4D218C92C093CD0CDDDC012911A1E10CB327BB70,
	UriFormatException__ctor_m718A5DEB020FAAD1F654CD05E63C3988AA7712D3,
	UriFormatException__ctor_m15F8C1205D9A648F72DD5D94DB92FD1FA1397FE0,
	UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_mF5C6CA057915A5F7F4E9D8FBE5D6EB2595A4FD80,
	UriHelper_EscapeString_m616525587FFF13665945B9173F3C2EC41C9EDDA8,
	UriHelper_EnsureDestinationSize_m4CAB570AAAC5145D6DAF9AC4362812693C324887,
	UriHelper_UnescapeString_m978B253231CE4F7BA6F6DC7778D679C19F77CB54,
	UriHelper_UnescapeString_mD137E2B3F648504A96E7F244796CDCEBE6BB9BE1,
	UriHelper_MatchUTF8Sequence_m0E963F060C446C4BBBBD60E4D54CAE9A36D5A3F7,
	UriHelper_EscapeAsciiChar_m3B740B3721EF4D93E9B9DFB2F881138AF4DE4452,
	UriHelper_EscapedAscii_mC7BE08D5B7803E83C57B8269F298B6DFA9EC5120,
	UriHelper_IsNotSafeForUnescape_mD11825AB8FE6E410A5C0D6D7850C607DBF697D7E,
	UriHelper_IsReservedUnreservedOrHash_mEAD756DAD5DB17F71960CC501BBB7CF9AB5EF74F,
	UriHelper_IsUnreserved_m3171C456B602B4F7052B4E865F2DF04C71553797,
	UriHelper_Is3986Unreserved_m06ED29AEFC563547A29E1DAA6A55760ADD9FCC30,
	UriHelper__cctor_mEBD15B6A7F8E49B4188075DC2EF1A2956E7E3185,
	UriParser_get_SchemeName_m3BD969B4E8EEA3769D070FA42CE340CDC766F47D,
	UriParser_get_DefaultPort_m38684C86B40F59960533D8F812B311ACBBEB7AA5,
	UriParser_OnNewUri_m752BC0174A2E3F7AA99F1088E1D539E5B8F7B5B2,
	UriParser_InitializeAndValidate_m5B38A10B57DC49B1CE13F361ABD78D7269EDA886,
	UriParser_Resolve_m7CC03524A2848FBD24BAB57CC62A19C751937182,
	UriParser_GetComponents_m230F3341901B4B90DC4650093272BC15A1161FAF,
	UriParser_get_ShouldUseLegacyV2Quirks_m2BDF10B430FDA291AA6962104CD838776F31D20E,
	UriParser__cctor_m9E43C905DCA4EE1855EE2C0236A1FCB6075841C5,
	UriParser_get_Flags_mBE183D7CBEEEBEC670CCF1DF21D56B354796D6C3,
	UriParser_NotAny_mCE6CF6736414BE547215F6E44C3E1283DEA05F56,
	UriParser_InFact_m3C07C3050DE1E69F6B0BC816EC34AF610373E532,
	UriParser_IsAllSet_mEE811F1E7BBCC34D7A32BC30A18AD5D34E690813,
	UriParser_IsFullMatch_mFC1C9EEDD31F040997931B26EF54CE7988495550,
	UriParser__ctor_m4FA54685DFA00FC6AEA73E94CD59C5FF2872E55A,
	UriParser_FindOrFetchAsUnknownV1Syntax_m29D45A9BF93C7C4FC7C819658043B6E753E7F01A,
	UriParser_GetSyntax_mD610CEF6784E137AF251EBC41629E2FE1D344615,
	UriParser_get_IsSimple_m8ED850D585BA3BA0FDC239496065E6D2D973C7E5,
	UriParser_InternalOnNewUri_m93B84E04949FAE8FE3B6919DA9F933B682D4B2F8,
	UriParser_InternalValidate_m69157B3E80381F3FC104788160F2213B3FE8F39D,
	UriParser_InternalResolve_mD1963080EBCF493DA78656AA5AEA458BF5138F5B,
	UriParser_InternalGetComponents_m52F450C5283A1820D5AFD07505E5FE82D4F476B0,
	BuiltInUriParser__ctor_m6E9775075E7360DD524B7E3E456A3FFA58F89E79,
	DomainNameHelper_ParseCanonicalName_m12865C6C2427B6ED804747CB1117D0B80E332F4D,
	DomainNameHelper_IsValid_m9E1909F6CA353F469ACF623BCBE1E3B545164752,
	DomainNameHelper_IsValidByIri_mE5772C5B7528885976ACB5AB11881B79A410CE24,
	DomainNameHelper_IdnEquivalent_m14CEBD0BD7B1BF46F8806E4EB5BB1901F7F29E2A,
	DomainNameHelper_IdnEquivalent_mD8EBF17C7A0A50FFAA03EAA9CA737899C9A0881E,
	DomainNameHelper_IsIdnAce_m063EE4431000F1D566D2D62B3BBB947A00CDA8EF,
	DomainNameHelper_IsIdnAce_mC429C00971921E7C357FA9A57D14AAB9A0CF3566,
	DomainNameHelper_UnicodeEquivalent_mD229718EC61E3ECBFA7AF5C894D22ADEEDE1B4ED,
	DomainNameHelper_UnicodeEquivalent_m62B17C161795C1C6AC76D49A3FB50B16693CC9DB,
	DomainNameHelper_IsASCIILetterOrDigit_mFAF590F20333B1D787428537FCD1CFF3993FE1F6,
	DomainNameHelper_IsValidDomainLabelCharacter_m957B15160BE378C49B6E25CC4F0D99BBA1FE06C6,
	UncNameHelper_ParseCanonicalName_mB07DBCB21F6996E1A2BC9D4D4DD427AE0E5D36E2,
	UncNameHelper_IsValid_m677F95E73B0278AC062F862BEA6BE646048127D4,
	IOAsyncCallback__ctor_m58A7C732444005CB72C5B352096C2DB252A6F113,
	IOAsyncCallback_Invoke_m70CC21061AD0965DBC87CB382AD395A35E1788B2,
	IOAsyncResult__ctor_mAA7211B9E6FDF23D0012354FEE350B6B5EB786F4,
	IOAsyncResult_Init_m6580DFCCB89EF7CFF91F7040C62ECC3811A3A98D,
	IOAsyncResult__ctor_mCDC93C97872E9DAB6A1D9F27C1FA14DDFC6A400D,
	IOAsyncResult_get_AsyncCallback_mC6150F5FC2CEE8FDC22EACD9B9BC72544FE801CC,
	IOAsyncResult_get_AsyncState_m8F7E705BB64B9C38D1F9EE7C746AAD46DDF1616A,
	IOAsyncResult_get_AsyncWaitHandle_mA0350FACA25DDD429DBE840269101E8168DCD42E,
	IOAsyncResult_get_CompletedSynchronously_m26279BC02AFFE83009A7EEABFFF58EEB2D12CD24,
	IOAsyncResult_set_CompletedSynchronously_m29ECF9F6388C1F9A12AF83DF8CD19D9FFB4B4D6D,
	IOAsyncResult_get_IsCompleted_m0EAD0BE09029EE4AFC303B76DC5E1EBE4269BEE8,
	IOAsyncResult_set_IsCompleted_m72D907352B8AC32DF47864E4F4EDFADEC825A6F9,
	NULL,
	IOSelectorJob__ctor_mA67864E470C66B78F683571EBD6A6315D9247DE1,
	IOSelectorJob_System_Threading_IThreadPoolWorkItem_ExecuteWorkItem_m896BBDBDBB91D3771569204789FEF8E700123D79,
	IOSelectorJob_System_Threading_IThreadPoolWorkItem_MarkAborted_m3F59E62D7927340D04264BAA081F7FAE0533C9CE,
	IOSelectorJob_MarkDisposed_mBCCE13D70B97BB7AE46D976AE207DF642C4531F0,
	IOSelector_Add_m170DDEAFEE849601BFE7ADBCFD630A7F2BA96057,
	UriTypeConverter__ctor_m02CB01E1305031BE5A45FB54601E520748B6EDB6,
	UriTypeConverter_CanConvert_m970377ED30FF1B6875CCDA64FDC87EFE7D381DE3,
	UriTypeConverter_CanConvertFrom_mD5D419C5B80FC3A5D175655A6278E54FC80928E7,
	UriTypeConverter_CanConvertTo_m119145C6F2FDE221E99ACDAF0EF723003388E8EE,
	UriTypeConverter_ConvertFrom_m079712540B92F2B6AF1780A5793F36EA77CA32ED,
	UriTypeConverter_ConvertTo_mA975FACF630714EBEA487EFC770EA0742756A764,
	Capture__ctor_mDC1197B1E29487F3D086091CF3D5B1029D593402,
	Capture_get_Index_m35E8D105E5E2142415F85116BFD4BA369AF7A17A,
	Capture_set_Index_mD5E578A0833A03602FBDAA2ABF77C9CC849CDABE,
	Capture_get_Length_mA3E97C9DF116CD4B4C20EBFB70A1312F33DA7A92,
	Capture_set_Length_mF9069396819D5D0282F38E70F347CDC625BE016E,
	Capture_get_Text_mDD394681E5FC39FFDCCD9552C0BF09DF8A7054ED,
	Capture_set_Text_m99AE1BB9AF9D7744D0E6F747DCF5A378591C353C,
	Capture_get_Value_m1AB4193C2FC4B0D08AA34FECF10D03876D848BDC,
	Capture_ToString_mC843EDDC990DBE2696BA08D08B1081CA12228F2C,
	Capture_GetLeftSubstring_mBFF01B80F38DA0FF4E56EFEF9FD9EE8899AE57DC,
	Capture_GetRightSubstring_m1F1F896A999BC08AEEBD1BB28616F12FC593D1C3,
	Group__ctor_mFDE74CF068520CBE797B563B419B8817E60CB58C,
	Group_get_Success_m4E0238EE4B1E7F927E2AF13E2E5901BCA92BE62F,
	Group__cctor_mC3032FEBEBFB00282A4485A8D95E47649CE2B881,
	Group__ctor_mEB6936CDFD191A4C92766CE3F3301161D7505C5E,
	GroupCollection_get_IsReadOnly_mF1DE2BBB4E573C6EDE85B787281F90951AAB8C4A,
	GroupCollection_get_Count_mE8775E53B4F5DA6D520BEF5ECEED2E0C08A26B39,
	GroupCollection_get_Item_m40EC174D4AC8FDD68F8819C35B779C79A44322F3,
	GroupCollection_GetEnumerator_m47E269C5A8C902948812AF4F52D0C96759550EE9,
	GroupCollection_System_Collections_Generic_IEnumerableU3CSystem_Text_RegularExpressions_GroupU3E_GetEnumerator_m593F2B44A71E46606D2E8C0816E626675EE2087D,
	GroupCollection_GetGroup_m8EDFDB8D85D8BEC6EB98B68CA69EE0022FC16B13,
	GroupCollection_GetGroupImpl_m27CE16949BE0A9CFC61CC6CBF3FB179F63654D92,
	GroupCollection_get_SyncRoot_m7505B29FFFE40E5319BAD3F3B3332F8A6299570A,
	GroupCollection_CopyTo_m122BEB82372AC34C575FCB8C71188A2A49CCC482,
	GroupCollection_CopyTo_mCA848040E42611E6A969C9E4F632FD81FC27B52A,
	GroupCollection_System_Collections_Generic_IListU3CSystem_Text_RegularExpressions_GroupU3E_IndexOf_m63AB69A8D37D49B1AFFE8CDEED6442D25A25F94A,
	GroupCollection_System_Collections_Generic_IListU3CSystem_Text_RegularExpressions_GroupU3E_Insert_m515A74F3095C972A28657B89FE6B65A3EC5324FA,
	GroupCollection_System_Collections_Generic_IListU3CSystem_Text_RegularExpressions_GroupU3E_RemoveAt_m981151BB2C68E7F2DC0BF4DD61D4AD4AB5B5AE3A,
	GroupCollection_System_Collections_Generic_IListU3CSystem_Text_RegularExpressions_GroupU3E_get_Item_m7FD6BE83AFD3DCC2F9ABB2AB4B938F552DF1D45D,
	GroupCollection_System_Collections_Generic_IListU3CSystem_Text_RegularExpressions_GroupU3E_set_Item_mB5DBB39B4E6F360B8105064E6659EFC49E8B1C85,
	GroupCollection_System_Collections_Generic_ICollectionU3CSystem_Text_RegularExpressions_GroupU3E_Add_m96D9B7FCEC33784ED88BAC3C291146E64A921D8D,
	GroupCollection_System_Collections_Generic_ICollectionU3CSystem_Text_RegularExpressions_GroupU3E_Clear_m67DAFAB787617D8A9B720AAF612436587E2C3172,
	GroupCollection_System_Collections_Generic_ICollectionU3CSystem_Text_RegularExpressions_GroupU3E_Contains_m260CC86A4FD94F5682B73C9C8BE03551C14B3568,
	GroupCollection_System_Collections_Generic_ICollectionU3CSystem_Text_RegularExpressions_GroupU3E_Remove_m008F559DB848823129DDCB7B832C731F0D4AFF83,
	GroupCollection_System_Collections_IList_Add_m08D9178D05AE66397CBCF2184448AE712741E4D1,
	GroupCollection_System_Collections_IList_Clear_mCFF90C2303D7106D722E8B574ED3261C80F250CE,
	GroupCollection_System_Collections_IList_Contains_m9E34B033D0BB903B2491336D7F70BE5A59D46208,
	GroupCollection_System_Collections_IList_IndexOf_mA974FC7CFC23310A1CAAD8A750823D5E0ED762AC,
	GroupCollection_System_Collections_IList_Insert_m3CDAAB43F292FD0ABD2D9595F02233FB55577623,
	GroupCollection_System_Collections_IList_get_IsFixedSize_m5AA2CBFB17E2617597BDFBB56CCA1E435A763014,
	GroupCollection_System_Collections_IList_Remove_m32982B763D442200E71B8A5E6C85C9501A555F23,
	GroupCollection_System_Collections_IList_RemoveAt_m32BF4A16212F4849AD46F680E5D0D14268CB7C58,
	GroupCollection_System_Collections_IList_get_Item_m969EA1758BE453F49B700FABF29940C5C9800573,
	GroupCollection_System_Collections_IList_set_Item_mDDFE5D350953DB61758FE64D556670EA5A402C48,
	GroupCollection__ctor_mB178F93E8DB52AA6412FF28E5FE50943E2BABFFB,
	Enumerator__ctor_m17B823D35315CED0509F6B4C5F04C148B9388DDD,
	Enumerator_MoveNext_m1DDDE3FC6B83FF15ED8AA895DE1357C37B7241C5,
	Enumerator_get_Current_m6201905A1315214B04F5001307F52887F9161F43,
	Enumerator_System_Collections_IEnumerator_get_Current_m16F94921184B09EDC3D45E9CFE3F3FAA1E5D862D,
	Enumerator_System_Collections_IEnumerator_Reset_m5B7067CCF9E8139F34641618492A2DAACA5419B0,
	Enumerator_System_IDisposable_Dispose_m1C77D8E3DFF1A034E9329E11854F11012F20AA69,
	Match__ctor_m2A6BD37E98680890114AC62CDD23E1821E80A53A,
	Match_get_Empty_m9565EFA0F267BAF2886447CB8169A654C5471918,
	Match_Reset_m53A78F16760B7CD5CC8A104F6CD95CA255D888AA,
	Match_NextMatch_m4B7BA2B21E09FA0937806F41DDE3EE0F052C4052,
	Match_GroupToStringImpl_mF3F2A3BC4EC33DD1FD35244E2412E05151AF6A1A,
	Match_LastGroupToStringImpl_m78C890F3547E00454BEB81BC939FE680AAEBB29A,
	Match_AddMatch_m5F28F3255C64D67D56C1542CE32A9D324F96780E,
	Match_BalanceMatch_m85EA44453E637DEBE24ABC7562BF91EE1222DBFD,
	Match_RemoveMatch_m154DB10030027B3738903FFB50DE511D71656B04,
	Match_IsMatched_m9D6E0C0AB1F3E1E8E49E77E3A0A0ECCD7C2C4FBA,
	Match_MatchIndex_m97F06DECDDA8BD3E4D8DF6B9224B8E24C0764F35,
	Match_MatchLength_m5C93EA60B018A79D9D05C8E69D25835848471B01,
	Match_Tidy_mF0A46109EAF0E91CBCA732985265E1DC20CA3F51,
	Match__cctor_mAB49EF64F5132A336358E3F1922FF173EA10CE3C,
	Match__ctor_mBABEF8F11A3BC6DB713519819D135CC87AC1B4CD,
	MatchSparse__ctor_m7CEA8A7F538C706527778E8E75003DD74AC2FD41,
	ExclusiveReference_Get_mCC2B9161946C9532001438C65A38E8D8FD56FB3C,
	ExclusiveReference_Release_m7A911FBF52436AA832188BF89CC558155BE09018,
	ExclusiveReference__ctor_m41EB96FC1FA261CB792752B89F15E6513FEA1722,
	Regex_GetCachedCode_m2039E73548B61FE55B6C8D85C00076DBA50332E6,
	Regex_GetCachedCodeEntryInternal_m9FE175C47BF909F7F223013004C0EA0DAD660C06,
	Regex_FillCacheDictionary_mC47F317967E556D1F03A344B78B215356D604755,
	Regex_TryGetCacheValue_m8785E636E24F8F0C3E5A7CFE516978A434C99DEB,
	Regex_TryGetCacheValueSmall_mEA1FE67593D84A849F8DDFD9BB4144FF5BE18A64,
	Regex_LookupCachedAndPromote_m41760A936BAED48327B5E239894DA154B7832DD3,
	Regex_IsMatch_m967E98137B140DFD845C5E1699D1BC8EF08CC76C,
	Regex_IsMatch_m3709329406E39A80F61F4DD2CC9AB672835A3E81,
	Regex_IsMatch_m7E96E666FBE7259D7638A3A6A21BE824D2406F49,
	Regex_IsMatch_mC358D80573E765ECC19F9F5882664E3B1FA0966E,
	Regex_Match_m58565ECF23ACCD2CA77D6F10A6A182B03CF0FF84,
	Regex_Match_mDF110993CAA26CD41AE9F5DB7F0C307B2680CED3,
	Regex_Replace_mA40B00220816EB1850F503AAB876812C726D8D66,
	Regex_Replace_m09B7AAE77B831CF7E8B1F469D62EF719069370EB,
	Regex_Replace_m8E802C10B5A591BEA61FC00EA8E4AF398D815A90,
	Regex_Replace_mD86A837354847D86EC562069D67567F9F707C9C4,
	Regex__cctor_mF29CE2FFB1941E7609C5B67C0236AC98003F675F,
	Regex_ValidateMatchTimeout_mA50CD0CB49AB9B42B548900335CA6EABC30BD18F,
	Regex_InitDefaultMatchTimeout_mC441DFFACCC15C2710E85B95820DAB49AE9C47A7,
	Regex__ctor_m082970AA73B8236360F0CA651FA24A8D1EBF89CD,
	Regex__ctor_mE3996C71B04A4A6845745D01C93B1D27423D0621,
	Regex_System_Runtime_Serialization_ISerializable_GetObjectData_mD788EE38607BF5D618FDA920048707DF4CA4B99C,
	Regex__ctor_mA8EBF2F80CC00D741BFD2FFD7DC9DB26FBCEE2A4,
	Regex_get_RightToLeft_m52C4DEE1DC94FC9531785B74111022F3BCC1DF4B,
	Regex_ToString_m4982DE4C344F7D529666314646F071580A450D5B,
	Regex_GroupNameFromNumber_mC99A37419C4C0C9C3869BCDF055EDC72782D53FF,
	Regex_InitializeReferences_m55385AD102411A38BF4426C08F6FEC6E55E4D4D6,
	Regex_Run_m4DCD2202C599273C00D6052A77BDD2F39E2F5BF8,
	Regex_UseOptionR_mFAB8780E9F998BE60C4B1127D57DD627A3B5629C,
	Regex_UseOptionInvariant_mD51BCBA0BF4C1B5CEBE9423B4AAA93CD83569B60,
	CachedCodeEntryKey__ctor_mE505E40C97820B3FD140F6212F0B4ECF8745585F,
	CachedCodeEntryKey_Equals_m59F40708B97179EA5C9019EA6A958D7AB8BC8CA2,
	CachedCodeEntryKey_Equals_m55F9ED97CB9AF357034A75C2FDB8498FC55D3AF0,
	CachedCodeEntryKey_op_Equality_m5C48CEBA5D7F893F4FE88D06917EB738615DDCC6,
	CachedCodeEntryKey_GetHashCode_m253B53C100F840B987A487020F02184019D776DE,
	CachedCodeEntry__ctor_m14145ADFF67BA7C888673D806C24E28C96B83B2A,
	RegexBoyerMoore__ctor_mFDCB1AB420E718548B67DE42C233402955BBB189,
	RegexBoyerMoore_MatchPattern_m0E84EAABBFCDA85484466A41A7B90187D3489E8C,
	RegexBoyerMoore_IsMatch_mC09692237823CC6C74D4F5D8900D82CEC840FB47,
	RegexBoyerMoore_Scan_m86EB5A209D33DA3F4C7335F8FE133238666FA57B,
	RegexCharClass__ctor_m165680E6EA10035E0630220C967BD772F5D6F52E,
	RegexCharClass__ctor_mFAD2AF3D7B0D4D3DC8BC850B8899C5C4D55F8B49,
	RegexCharClass_get_CanMerge_mC093FE4BE7C1933BA9F146CF2A33CD729A965172,
	RegexCharClass_set_Negate_m8EA71CB8F671E99EF0B388CB520F44B8B6653A6B,
	RegexCharClass_AddChar_mED269B31D5C606032B3394A9B9305ED172D16746,
	RegexCharClass_AddCharClass_mDE8C70A6A99B2223DE4EA36A4753A13B5EEDE7BB,
	RegexCharClass_AddSet_mFC202CC46631017C7D64ED17EAA9C786C5C7CE83,
	RegexCharClass_AddSubtraction_m4EF34AFB93C001C6A4521E1F1C5F4B54833E8A12,
	RegexCharClass_AddRange_mB1188A62712FE4ABB7F5DB28645351E375B5C6A7,
	RegexCharClass_AddCategoryFromName_m032AED661665AE30D7F3F712914E2937F6E4FF97,
	RegexCharClass_AddCategory_m3A3C78511F279FD0CF67A3EE36F59A548A37269A,
	RegexCharClass_AddLowercase_mB87FC245CC837E582370E5410A6B40B23BEAC0BE,
	RegexCharClass_AddLowercaseRange_m75E07C55A082FE5B2926997F2564B611DF61A343,
	RegexCharClass_AddWord_mAED995BB6613893E0950C5895515204F1E95D152,
	RegexCharClass_AddSpace_mD1BF3AD215D69EC6CF0E49A359F7FC709FD6FBB6,
	RegexCharClass_AddDigit_m04C1001F7E8DA85CB2D262ED403BA9BD215A7A8E,
	RegexCharClass_SingletonChar_m11683F48B3AA3E0B5CD5849C40ADDCECC91BFF10,
	RegexCharClass_IsMergeable_mD017475B43D0621F15ECAF5006410D4D0CCC46AD,
	RegexCharClass_IsEmpty_mDD55987C8F4B887716E46869730F5E1E31593A44,
	RegexCharClass_IsSingleton_mF23CB7003B555ADB4C5035A1B3A218A8ED847D12,
	RegexCharClass_IsSingletonInverse_m62E7E31998BEBC824A4EED3CEE5A2D3FB3BF5831,
	RegexCharClass_IsSubtraction_m069CC20713C5757A8AAD9BA556F07C8E042F3BE8,
	RegexCharClass_IsNegated_m66E1348EBCAAD253AEEA5EDC3F9A93E84A433947,
	RegexCharClass_IsECMAWordChar_m3A2920E6309D58F982B8B7AE20D9166601DDCA0B,
	RegexCharClass_IsWordChar_m9FEFF0FC22277053D7A4404FD919C7F41733DB94,
	RegexCharClass_CharInClass_mAF6EE8C936383EB630F17C8C268F10BB8C10BC83,
	RegexCharClass_CharInClassRecursive_m7B2DC6EF52CFA494C26DC8AA976F3A06DDAF22FC,
	RegexCharClass_CharInClassInternal_m166B4DDEB0553E88B58A2F3E859CA2D29995CF54,
	RegexCharClass_CharInCategory_m1B20968374CE9C35FE81D172C3325E37433897E1,
	RegexCharClass_CharInCategoryGroup_mDDAA3F1975AC1A7527137AE6951121F381703D48,
	RegexCharClass_NegateCategory_m1C615694A5F8EECBBE4C09BA293454D856F52F50,
	RegexCharClass_Parse_m4766E7E53BF572FD9815DEEBD9831380C33C796A,
	RegexCharClass_ParseRecursive_m6ABF2F2B8B8C5A559866EBF3FBA305247DE4CCF6,
	RegexCharClass_RangeCount_m36A38A50CD2E3E61B5F55D95562BD92923F981F1,
	RegexCharClass_ToStringClass_m6B0B92A0A525001CD78A6C2A766295268E0FED57,
	RegexCharClass_GetRangeAt_m67B8198B09F344044D8F742C5C9869034C4A3128,
	RegexCharClass_Canonicalize_m77EEBAD046B3EAD5110DD472434BDE16851C7A9E,
	RegexCharClass_SetFromProperty_m75FD04D8F9EA5FD996E4C8B4A2DF45BEEE6BABC5,
	RegexCharClass__cctor_m70495E564EC18CDEF2A2687F8012C96E571A0631,
	LowerCaseMapping__ctor_m06D0A21D5D72E39535F82076EF046F98C62D7D5A,
	SingleRangeComparer__ctor_m9B430EFE70974D964D73913D81E7C6C9236BBCCE,
	SingleRangeComparer_Compare_m917FDE86E5F31A755D07077EA095F8B7E27E130B,
	SingleRangeComparer__cctor_m512F77BDA73732EB8D39BB056D00CF9B200D5D1C,
	SingleRange__ctor_m14FD490AFB119E412298554D31FADDCB9465511E,
	RegexCode__ctor_mA50EE1CFA1A050BFC45E135DAB7E2704049512E0,
	RegexCode_OpcodeBacktracks_m77D0680FD94F152F85C6CF8724A5EBFFE07A1397,
	RegexFCD__ctor_mE1C0A21180069D25EEF6F3702938ABB087F35C0C,
	RegexFCD_FirstChars_mD151F8EF8DD64C9E1960D6C4E79E65A6766322CA,
	RegexFCD_Prefix_m6D786604F799DF1EEF509BEA577E2DC50C75FB6E,
	RegexFCD_Anchors_mF19C2B50200C6D7E38566E5C4854DB04EADFF5CE,
	RegexFCD_AnchorFromType_mF944273F420E8D43F4F9EC93950E3797B4C10A47,
	RegexFCD_PushInt_m0B38AFDAF0D14FF1E41AC90838B40C4C866D8875,
	RegexFCD_IntIsEmpty_m05E3C6FF40B69E98D90CF1C12DF3F32AEC800B5E,
	RegexFCD_PopInt_m1DF4F5E6B4BA9E15AFF2B3C44D1A233EA46EFCD0,
	RegexFCD_PushFC_m1396C7FD5ACE3D28076FAFF667D9A0D169F8B675,
	RegexFCD_FCIsEmpty_m749E6423466EAD51E38445AC91E76DE31258574F,
	RegexFCD_PopFC_mB83F2100E8EBACF17904BD5CACB57525D2966628,
	RegexFCD_TopFC_mAE41171B96938FD879E0C0AE7C0D06645AAA84CD,
	RegexFCD_Dispose_m0B04A2F3B819FA7BFAF0AECCBF6D22E6D9789C07,
	RegexFCD_RegexFCFromRegexTree_mD5A53219905969B914CE27404553A4360DA78500,
	RegexFCD_SkipChild_m5ACE619BAC10DD5DFE755655DB6151E7DB0885FD,
	RegexFCD_CalculateFC_m16F7EB523C02B530A741A7F6364F2E46865DCC91,
	RegexFC__ctor_m18E889B95C8C6014582FD2C6991F789190519F0A,
	RegexFC__ctor_m073D88DF033828EB626DBEA191FE78296CADB57D,
	RegexFC__ctor_m69E0058CD2F4A36F14589250B3F5CBAD7025D6F5,
	RegexFC_AddFC_m61D0F8CDE635ED3A48AEA069A7700B100D2AE851,
	RegexFC_get_CaseInsensitive_m4FCD7C7545925463ADA4055E9FE62D5A452E2102,
	RegexFC_set_CaseInsensitive_mF9A3745D1575160C2F2B1F7F9EB5541BD0C9FF4B,
	RegexFC_GetFirstChars_m6BAC398ABDCA5AF58CBE678143C9792611A2A3E8,
	RegexInterpreter__ctor_mB81896A8292574F7532CF707F74A1AC1DECEA9FF,
	RegexInterpreter_InitTrackCount_mFFFBE9294C6FCB854D4502362473C40A5ABBBFC4,
	RegexInterpreter_Advance_m31E2455F84B61536DE535BE51D4D0B42DC33857B,
	RegexInterpreter_Goto_m9BF559085E75D4AA7457FAD60E879AB96BB4AEC9,
	RegexInterpreter_Textto_m66DE4B949DBAA02672CFDE27162BCED542038850,
	RegexInterpreter_Trackto_mDFDFF0401BC8F2B2D26AEC65B54B963F84A089AE,
	RegexInterpreter_Textstart_mDF185C6B3E245A2307A8B0B9F5FE5DF8C8C9936E,
	RegexInterpreter_Textpos_m6E20619B1C7F8B77D7A57A8A6939C24011808DEC,
	RegexInterpreter_Trackpos_m6BDE792A7333FADB2533591B88211FD77E8F0401,
	RegexInterpreter_TrackPush_mC82959F407482A4713C419296455A18F5D861E93,
	RegexInterpreter_TrackPush_mA225A4A0302F57CE82842189DCB8895A9AAEDB3D,
	RegexInterpreter_TrackPush_m94D69F6CC8E122478F4EADE04FB7FC9482CEC878,
	RegexInterpreter_TrackPush_m606F28BD60B479F0FD3073F8B5627836DB89230E,
	RegexInterpreter_TrackPush2_m5131F1DCBCA4D4D2231B44A85B08DE319BE31C9F,
	RegexInterpreter_TrackPush2_m18FF10B704A6586A18D9AB21D28737DC32DA2797,
	RegexInterpreter_Backtrack_m8D227FAE1EFD91174D43FF070D9200291941E593,
	RegexInterpreter_SetOperator_m693B374C6827D36CE755EFFDA53E245FB04652EC,
	RegexInterpreter_TrackPop_m2FE356634C8337D8B657D30790588E4A4D7DECC1,
	RegexInterpreter_TrackPop_mD3125258D278581E337C822F3C9847233E06224B,
	RegexInterpreter_TrackPeek_m8D2CDC071ADBBC028FBAA364E2C18BD19FD6D861,
	RegexInterpreter_TrackPeek_m45C4F44554EC086C01B03C054BF6A94105D39699,
	RegexInterpreter_StackPush_m9BC14D7DD0FAF99AF230B436F32557E056F8FB60,
	RegexInterpreter_StackPush_m7741DC1BAEE69DFB68E1D60A9FC39C718829DF0F,
	RegexInterpreter_StackPop_m3284B40674B96F317D05BD2616AAA80181E0AEFF,
	RegexInterpreter_StackPop_mAEBB71FEDEADC065C5FA19E7C2C0FF0E82CEE681,
	RegexInterpreter_StackPeek_m16057890EE30C3383227F21632C02C6BEF981991,
	RegexInterpreter_StackPeek_mAFBD38869C8A829D6ACF534643B8A6C9863A10D8,
	RegexInterpreter_Operator_m3BD9AD7DCDA4E84DE4821DA786E1B1483E677B58,
	RegexInterpreter_Operand_m09F048ED7BBB36BEFC5023D420323E37A97C9560,
	RegexInterpreter_Leftchars_m6FD5AA399927D1767633B24AE234D596CBDD4002,
	RegexInterpreter_Rightchars_m0D684BDADC810F61B803D30D121506833AD07CD7,
	RegexInterpreter_Bump_mD5E6B4A80D6C620EDD37A5C04156ADF7C27EE9E1,
	RegexInterpreter_Forwardchars_mE97F35195F994160F951E3C035F9E0C35EB739C6,
	RegexInterpreter_Forwardcharnext_m78266C74DAAFD2D09050F148FFDA0A50656AC4C1,
	RegexInterpreter_Stringmatch_m89AE288DB294AC0146C7772193CAD3B702849E54,
	RegexInterpreter_Refmatch_m457B37C3E3B0671E7E89B7F6C16396D36EB32F68,
	RegexInterpreter_Backwardnext_m0C573AFAD3FAEBED37991618A232ACB169F524FF,
	RegexInterpreter_CharAt_m9EFCB7CE5FB311B6C061AA996705CD3D402F8E8C,
	RegexInterpreter_FindFirstChar_m87D986F0502AEEFC4F13EBD0EAD891C495C4845F,
	RegexInterpreter_Go_m1CF26F8D95231F3B2235EA98AFB286C3F1629BED,
	RegexMatchTimeoutException__ctor_m24286DF7231B227E3A8B3C31AE33447361971005,
	RegexMatchTimeoutException__ctor_mA8C497C6700E1233B4953A8AA7B8F57C8682C410,
	RegexMatchTimeoutException__ctor_m7DE41C173C1A57CEA5D015FF63DDFCB70CFF4FBF,
	RegexMatchTimeoutException_System_Runtime_Serialization_ISerializable_GetObjectData_m6F530DBA34FE5CBECBCD322C8FBD4AE28054688C,
	RegexMatchTimeoutException_get_Input_mA4DCA4F3C9692ACDC45FBEB683EE4165C84E4073,
	RegexMatchTimeoutException_get_Pattern_m8F12503E2B66F34B8E8EFE46971C169EA1BFBAB2,
	RegexMatchTimeoutException_get_MatchTimeout_m12D9D88EFFA06EAC0FF06C81ECF29BAE0F906087,
	RegexNode__ctor_mAE4AC160F21E5DF26B99B74753BCFB9EF0951B2B,
	RegexNode__ctor_mFA693535F5B9610F075D30F19626A8AFFA879D7B,
	RegexNode__ctor_m00D0AE9AB07549681FB13D96CC0AA20FED8CE6EA,
	RegexNode__ctor_m3352B98BE0DAD220AB653F929BD6D95689B5E07F,
	RegexNode__ctor_m5ECC62B9C3918CD043542E5BEAAD39A06158E27A,
	RegexNode_UseOptionR_mEBD872419C386D7C7802D2A6D2187E7B6D1D1641,
	RegexNode_ReverseLeft_m251846220299FD022068361A09B9A7A4CF67A586,
	RegexNode_MakeRep_mFDCFC9F5DA5DCBFC66D6F00A8E08A602F00C5F48,
	RegexNode_Reduce_m9D14953F47CD1D924C3EEB9D0287D7F257AD2D98,
	RegexNode_StripEnation_m2BACBC29E58CEC452B91534BCFA2A21639712B61,
	RegexNode_ReduceGroup_m62F5B20C564AC8A8E8C1C9E00684A8BB9A988E3B,
	RegexNode_ReduceRep_m85B3760500D732A04B60CD3F247CE8ECA1E6F8C1,
	RegexNode_ReduceSet_mD307DD149936830074467A0F24C2E20032F708E9,
	RegexNode_ReduceAlternation_m193F3C5A073401BA987E5AB7D35F59B1FC902341,
	RegexNode_ReduceConcatenation_m95EE4F324360AC71D52EDEC1C53F6F81D73C67C3,
	RegexNode_MakeQuantifier_mA6A10E1AEF5AA80F82BD1BE6D28E62259208B5BC,
	RegexNode_AddChild_m1A2B3A7445927E39341C233C0E533D77FC9517A8,
	RegexNode_Child_m6F5C25EE5349D1BE0D65C89475FB0C42A15BFF27,
	RegexNode_ChildCount_m833AF69D2711D8EEC5E50FD666A61F428A12396D,
	RegexNode_Type_mDE9685FD99646016D0D6038C4509C6392B10F019,
	RegexParser_Parse_m5AE448041BC21B17EF68A2039991EC85F91AA9BB,
	RegexParser_ParseReplacement_m721243B3049725DF4FCCBE8F1861CC47BAF380CD,
	RegexParser__ctor_m99DC2B2C4B64E7298BBF49AD862BFD12007E38E4,
	RegexParser_SetPattern_m852D668CE45FBCAA3A6ACC415B94BDD0A7190265,
	RegexParser_Reset_m426A8C90F4D8065486802AE7AC9FB07FC599EB01,
	RegexParser_ScanRegex_m3CD95B8EAE4F2E8ECF3461F199B04A021C77231D,
	RegexParser_ScanReplacement_mC27972ADC38D857F44851CE642162928ED4D12D6,
	RegexParser_ScanCharClass_m01448F1178653B15083659F29014BB87A6E2295D,
	RegexParser_ScanGroupOpen_m5ABFBB28982A5097B2D560091D2AC12EA8B028F7,
	RegexParser_ScanBlank_m4C1B18EC7A5DF860363CA17E69B20A00023B5E82,
	RegexParser_ScanBackslash_mAAE11106CB78789803468679894E1D7AE6B0C92E,
	RegexParser_ScanBasicBackslash_mB0F1728AD89C8F196601B3110F0BA807FBC8FCEC,
	RegexParser_ScanDollar_mB8FD395274F630278543211B2BB583AA5B03D96A,
	RegexParser_ScanCapname_mC9524DCBE266D677078207375663506AC32BE3F7,
	RegexParser_ScanOctal_m95EA0AE4B61D5418914DDD3815C6277471B4EDFD,
	RegexParser_ScanDecimal_m1BFFE19E0698D0EC97D81C655F3D04B608897161,
	RegexParser_ScanHex_mDD96A112E30DABDB5BC0C558373DA5607F8ACF95,
	RegexParser_HexDigit_mD112E7ACEDAD4A29DEA1A771854CA34CEB259053,
	RegexParser_ScanControl_m26274BE5776CF5AC03E8EAC7B5972C0A8AF2A95B,
	RegexParser_IsOnlyTopOption_mAA13B95864B8B314B8B6689B47FA4BFE3B433280,
	RegexParser_ScanOptions_m56F01464F53558D3AF3BF1CD874A2656930AF31C,
	RegexParser_ScanCharEscape_mA872DDDD24E165F5B5C5F62DCFAE3B02CCF97C98,
	RegexParser_ParseProperty_m3B765A1B30A38A723CAFE63AF9D6F3D08C7C6F38,
	RegexParser_TypeFromCode_m11B176B0B9A6EF391370E021B111101AE56F4CC6,
	RegexParser_OptionFromCode_mD6CDD96F9917C0518A437DB77BC53130AE702627,
	RegexParser_CountCaptures_m2ECDC9ED534F245A12224038BB86DE5D66DB56A3,
	RegexParser_NoteCaptureSlot_m21BD1F90FF2C62FFA445303D574D271757D62222,
	RegexParser_NoteCaptureName_m5CDAF94FD43B665DE067B6DC02BD72A315D48FFD,
	RegexParser_NoteCaptures_mC02396D9D7C522A5B701A2C1C88E742C98EAAF17,
	RegexParser_AssignNameSlots_mC5738B99DE328E7D1FA7279A717640EC2044857E,
	RegexParser_CaptureSlotFromName_mE18BA49BCA1CE4B972DC804E52C78E48905FEFAB,
	RegexParser_IsCaptureSlot_m1EE21960AA0BB1C072BD9D765AFF8A6DFC3192C3,
	RegexParser_IsCaptureName_m5DC0D7BF3AD01C929CB982048722BCD43797CEAE,
	RegexParser_UseOptionN_mD441B71AC8368C644B8D9A0CCB3DB8BD681639B3,
	RegexParser_UseOptionI_mB3FB0740ED596E6ABC827E903DCE8AA4258BA701,
	RegexParser_UseOptionM_mAE3FC2655A6E80EA35D5CEBEE1D82B5B07529118,
	RegexParser_UseOptionS_m070AD4458172FB14FF21E4502DCA6ECEE8938994,
	RegexParser_UseOptionX_m0BFB48EDC8A2F38A29A8F6CEC85080D9E04010FC,
	RegexParser_UseOptionE_mE0B7314EAB95CAC9B9762CD3AB9FBC71BFD410D9,
	RegexParser_IsSpecial_m4AC43B42D2727513B568B534C6FF09093ACB2754,
	RegexParser_IsStopperX_mF8FCEDFDBE9E5ED5469A04463782B951552BBB2F,
	RegexParser_IsQuantifier_m7FFA04954A7E4C39D443D790483F2126A5119E25,
	RegexParser_IsTrueQuantifier_m22CCE851B85E0760270178DF1184746040E68AEF,
	RegexParser_IsSpace_m4E37D473B564502A80E5C968E630AA2D4A56231A,
	RegexParser_AddConcatenate_mDDD0350FE254D75D6707668332D0F3088A5572AF,
	RegexParser_PushGroup_m2D76CB852430A3B3F7454DDC3322B2A5E4CE398F,
	RegexParser_PopGroup_mEC525DC83E5BF2F8DA03129AE3DF06834EB76552,
	RegexParser_EmptyStack_m756AC5BB66D0F2077EC7DD655A3D57681200F5CA,
	RegexParser_StartGroup_mBF861FCFB0FB9ED26744CAE2297537DAB6DF37C1,
	RegexParser_AddAlternate_m77BCF63DD4E1AB89B70F72821CAE4B67936CD847,
	RegexParser_AddConcatenate_m77A3D9F6B1484C7B656BDF3CC5659248F9DEA1EA,
	RegexParser_AddConcatenate_m3B10D68AE09CC6A2F06AC65E49B918D11D5375FF,
	RegexParser_Unit_m6C984A797C4A92AB0A9985E5A8207DB9CA9AE87F,
	RegexParser_AddUnitOne_m991DCA85D3432B8F7F74D18125A69FFE25108F47,
	RegexParser_AddUnitNotone_m4F79DE175B95FDB88323D7FE9DC3132E24F8F0C0,
	RegexParser_AddUnitSet_mA262F57A7FAB27996D8115F66BC84A5C58317D7F,
	RegexParser_AddUnitNode_mE8DC7CD039C933AE881097E041BA620AA325FDE8,
	RegexParser_AddUnitType_mEBDFA2325ABDA3FDA74184DBFB706824F6540553,
	RegexParser_AddGroup_m8989D6A9CD4B700522D722DEF5FEFC267C4DBFB7,
	RegexParser_PushOptions_m6E501EE3C5D6FE990ED74935A9AC9A6E834FD968,
	RegexParser_PopOptions_mAE4848419A71162B8E563781AC817A8D6ACEB91B,
	RegexParser_EmptyOptionsStack_mC16A4D18503FF296911710732A95062A8279163B,
	RegexParser_PopKeepOptions_m911F83DFA290ED90B06D3D3166553E6205C256C1,
	RegexParser_MakeException_m385FDF5B69D02A0CE93393E92AFAD928A9CCE3FC,
	RegexParser_Textpos_mBF50F3EB06FDA6C68AF4BF0D0C8D4FC09FDCFC88,
	RegexParser_Textto_mA45B325CA6CCBC0012F1CBB0282AE114B34AEBE2,
	RegexParser_RightCharMoveRight_m4386CDEFA7DC102FCA0E6E00E522E1712AE3AFE9,
	RegexParser_MoveRight_m5F6B031B292FC0F81DF5E7039EDA6C9D0706A0A9,
	RegexParser_MoveRight_m921C1D96B625B4C8AF1D658E514D57CC44E40B61,
	RegexParser_MoveLeft_m57BB7A0146D0E8834D715667048B75A517761AD5,
	RegexParser_CharAt_mF46179762DDF46FB78E32CCA3058950244CFCB70,
	RegexParser_RightChar_m23D21ECC3D6999A466C0A91BDF589BFE4801CF91,
	RegexParser_RightChar_m6BEE1FBF048A2DFC53235447BB2F6AC8C56A4147,
	RegexParser_CharsRight_m6F83CD819B532331F5778143B6A95B31C8734A79,
	RegexParser__cctor_m4F6AA7D64102443052B58896EDF95DCC21C2C0C7,
	RegexPrefix__ctor_m05CD44F32CB0B5F8B2C080EAB94886F9743E86A4,
	RegexPrefix_get_CaseInsensitive_m4DAE2D9E91916BFFAB928F7DCAA20A5ABD4560E6,
	RegexPrefix_get_Empty_m3D63F7F64529D4CA3C0929F94E035EEED7D9E0F9,
	RegexPrefix_get_Prefix_m844D02262C0720B0DC488AD1541713AD37BC518E,
	RegexPrefix__cctor_mF7EA052790930134AC69C755D0066C70CA86363E,
	RegexReplacement__ctor_m9CE743F0908A59F98461440AE5B842BE1BC72EA5,
	RegexReplacement_GetOrCreate_m3DABA1D0A9D785394FC1B3E7373EBFD0A2FEB3B5,
	RegexReplacement_get_Pattern_m146EA30FB574587E88DD056AC22F79375C3F55E6,
	RegexReplacement_ReplacementImpl_m60E4B82702C44912E56FED402AC793E7B7CCC7E8,
	RegexReplacement_ReplacementImplRTL_m343AE6575EE1B8810D049CA7F8203237920C84E0,
	RegexReplacement_Replace_m54375FA094C20D81305C8DDC17AB973BF1B461F9,
	RegexRunner__ctor_mBC777F351AD6D1CDD5F20C408B936F7D0F36ED69,
	RegexRunner_Scan_m903691F4DFB68C0267F240D61A6F166BB592CE53,
	RegexRunner_StartTimeoutWatch_mA17870A15492787CA1384132267054AB09BE8BE9,
	RegexRunner_CheckTimeout_m20BF0CD8BC16738ED6E7F20AAF5B4EE695E7E120,
	RegexRunner_DoCheckTimeout_mC26B9239FA1CD3DC98F11A82800F61852793CEA5,
	NULL,
	NULL,
	NULL,
	RegexRunner_InitMatch_mCCF94A1E7CCE89275E47BB218C4DD58DF5D5593D,
	RegexRunner_TidyMatch_m0EEEF0CFBB55AFF485B4D9F9FD21834B9B3A01FA,
	RegexRunner_EnsureStorage_mB3DB043396771740A05B2AABC293DAAB1250B024,
	RegexRunner_IsBoundary_mCF4AE83F6FE3CC46C71C4558DBCDEC255BD422B8,
	RegexRunner_IsECMABoundary_m56C692126DF69F0933BBFCBF122F764C6093DA26,
	RegexRunner_DoubleTrack_m15E35365FAF5F438C7BD0BB0C37594F765949F1F,
	RegexRunner_DoubleStack_m89C12DF438D7BE4ADA2A4D1DFA36F274F292E1F4,
	RegexRunner_DoubleCrawl_m7B67CFD3071825C9F01BC0ACE0E27B1C4EC6DED5,
	RegexRunner_Crawl_mBE60CC8F17E6D85E1A87F0A86BC684F32ACE7CA5,
	RegexRunner_Popcrawl_m2C994BE22A9A4DE0D897691A1CEF87189DB43E67,
	RegexRunner_Crawlpos_m4EC84964D3AD3DB49724BFC28CE89FC34478D532,
	RegexRunner_Capture_m3EB42B55A19BD7776156F04CBE9367C6EBA59F9C,
	RegexRunner_TransferCapture_m642CC595E9FA641B913CCBE02E233DBB319A6E8A,
	RegexRunner_Uncapture_m70B0FFB308F48B4673F73311330BFF640F47B7D9,
	RegexRunner_IsMatched_m7E64237D020BFFA74813A8B6455D5F35671A876C,
	RegexRunner_MatchIndex_m150A8B71C07022C485630C71B2A9FA6FD195F88F,
	RegexRunner_MatchLength_m19C9B24F4F6CAC56A391A0AC47A4358C61615BF7,
	NULL,
	RegexTree__ctor_m89DA1335AAB55B0707774E978E6078CE42DB0DA6,
	RegexWriter__ctor_m37DF964F59213D287C9D812A3D0945744BF341EE,
	RegexWriter_Write_mCDBE8B9E7703439F2F27C082808594B1056BEB99,
	RegexWriter_Dispose_mD34F8BFC41D28F22FBC65E3F1A211DFE85625E04,
	RegexWriter_RegexCodeFromRegexTree_m3352CAF05A1B1753D32D768B83BB778B3E2D4568,
	RegexWriter_PatchJump_mC1090F37107CAF7F4B52B020E070BDE9284118A6,
	RegexWriter_Emit_m5884414A6EA7431DA9C5708E650B20BEDF982CAC,
	RegexWriter_Emit_m41C2B9DF58542C452CA4FC77A317884578757D75,
	RegexWriter_Emit_mDED2545E64CE95D941A1232DBA24EB6C81A7DB0C,
	RegexWriter_StringCode_m5D4A227BBAA0674B820D70BEA1A2F7D0B29635FE,
	RegexWriter_MapCapnum_m5EA7BA45A1C4D51DC20C281246D3997A3001A0B6,
	RegexWriter_EmitFragment_m0B35F7C3BDC22B5DE82735F2652C7948AE8494A5,
	BooleanSwitch__ctor_mA9232C7B57B18DBDDFD4F14DA26483509C0FFD53,
	Switch__ctor_mA295E111B6A7F4B8D2B6AF796926017759669EA7,
	Switch__ctor_mE69262A89E3F19ACB4610B1EBFB35C763BB3EA48,
	Switch__pruneCachedSwitches_mB732689A566AFB2C1DD9693451B9DE80125CBEFC,
	Switch__cctor_m73E1B17596C50266B4FD30170C74991462125626,
	SwitchLevelAttribute__ctor_mB0C0910F9AA6FC3C568CD3DCEF69731CC528B53A,
	SwitchLevelAttribute_set_SwitchLevelType_m4757AB3B3D77487056F17495239C40B56FC2E9AF,
	Stopwatch_GetTimestamp_mD6D582A3E30369F05C829A5650BE2AE511EC807F,
	Stopwatch__ctor_mAFE6B2F45CF1C3469EF6D5307972BC098B473D0A,
	Stopwatch_get_Elapsed_mF16CCA2F9B7E2EAEFCAF8D81F44370DBAC1B2178,
	Stopwatch_get_ElapsedMilliseconds_m8D8E60ADEB47D008C06D8E57D28D41EAE07AECBF,
	Stopwatch_get_ElapsedTicks_m3F4040FBF8C7CCDC69E0E04824019DEBB25AA410,
	Stopwatch_Start_m35906A38C7D336CF9DD35854C7E170D762A55915,
	Stopwatch__cctor_mB40A9951DF8D540C2432D56CBC9643E62C40039E,
	ExcludeFromCodeCoverageAttribute__ctor_m2CB8196E20519167E842D03BE4CFBCC84BEFC9E2,
	AuthenticationException__ctor_m7A29E86FE3299D40A63064FF6DB61D0E1A70C1F6,
	AuthenticationException__ctor_mF08E9BCF31EFCDBECB34BB56D05EEF158B200896,
	AuthenticationException__ctor_m6EDDEE3840629AC06E70CDF7878F955FE1ED538A,
	AuthenticationException__ctor_m091E6A6612916774EDF1C0F7544B70A5FA97A542,
	DerSequenceReader_set_ContentLength_mA2B091D517ABF0AFDF9C54477720668E09F47BCA,
	DerSequenceReader__ctor_m899D7771F7F469E94812F8EB9BB0A75A0BE69CBD,
	DerSequenceReader__ctor_mAA932343BB190BC44E41A6D8FBA75EC976D0D09A,
	DerSequenceReader__ctor_m09BDCF87C8D4558CABCC96159ADDBB6E49361141,
	DerSequenceReader_get_HasData_mD6CE45854665F68A82771427FF58DDF7632D6A31,
	DerSequenceReader_PeekTag_mC22E34353BE4B4C5CE3579AE64B220263C159C8B,
	DerSequenceReader_SkipValue_m0F715CE70FF6EDD8575359FDC2E8ADBAC72FFACE,
	DerSequenceReader_ReadNextEncodedValue_mB9D376F1ED7428BFE7DE86B327E1B80E52E91CB7,
	DerSequenceReader_ReadBoolean_m568EA640EA9967B8785075997D5161FC7C6C5EBD,
	DerSequenceReader_ReadInteger_mEFB23332B798605A0F474C5B43C59D70543B93BC,
	DerSequenceReader_ReadIntegerBytes_mBF491D99194CDD9FC70F58CF07E66B535C3F63EB,
	DerSequenceReader_ReadBitString_m6519951E58B9642238778071AF7926A6948F0387,
	DerSequenceReader_ReadOctetString_mDF80CEA6892B5E2BAA457C153DEBE8C411EC70FE,
	DerSequenceReader_ReadOidAsString_m53E76684261C012927DE60E2A8A5A4ECC6B33198,
	DerSequenceReader_ReadUtf8String_m695D8EC313847638A5E9FCB3B036B3AD4A73DB83,
	DerSequenceReader_ReadCollectionWithTag_mBF55466089C1EAABB9D84B29A9A6193525AC412F,
	DerSequenceReader_ReadSequence_m97DF1A79453D57FB5DB012273F34213180921AB4,
	DerSequenceReader_ReadSet_mB2770ADE8AF6F651659562AEF5B8F247619641FF,
	DerSequenceReader_ReadPrintableString_m9ED84D1BD26B7DB18C047355BD2011CAFC2E72FE,
	DerSequenceReader_ReadIA5String_m0C23F1CCD96F9F4BD97A43459D1839BDFD1EAB40,
	DerSequenceReader_ReadT61String_m30F888297CE3E176BE97EE993BA1FA945607999E,
	DerSequenceReader_ReadX509Date_m4B50094324675B16BCF2F77BCB1CD6827F04A568,
	DerSequenceReader_ReadUtcTime_m16083186C07F2771CB7B4EA9EA68248FCFA9B66B,
	DerSequenceReader_ReadGeneralizedTime_m24100E18CD48B8F2E7415BEA4095466A0DBF3042,
	DerSequenceReader_ReadBMPString_m6AD51BC4D16BF2659A5F0D197A0A2F184D36375E,
	DerSequenceReader_TrimTrailingNulls_m867B3CCF1312220E709B95AC95169DD059B6EC21,
	DerSequenceReader_ReadTime_m492DB6BB481B1CFC586644EE3D233145A5486F32,
	DerSequenceReader_ReadContentAsBytes_m1DA571FBC10F30D7D68D49DAADF32C320A2B9907,
	DerSequenceReader_EatTag_mF2658A324EBFAF16DAEB9762EA7DFE9036D4FB8E,
	DerSequenceReader_CheckTag_mC092A1AF116BB243A90001F1611B11D638D66675,
	DerSequenceReader_EatLength_m2D1533B44A7CE4E4C97B206F704CF0BEE6D2A6A5,
	DerSequenceReader_ScanContentLength_mB11C032022638AF8CE558C71D72AC7E51DC69ED8,
	U3CU3Ec__cctor_mD983523B317973CF165D528473020E47EBC3CDDA,
	U3CU3Ec__ctor_mF050E3B4FEBB43DB13E684C3B309D33A66DCC861,
	U3CU3Ec_U3CReadT61StringU3Eb__45_0_m53F555E4EFBF56385A389ACA1681D68920A18735,
	U3CU3Ec_U3CReadT61StringU3Eb__45_1_m2DE60562D90243EC4880800B7AAE0463F5E8B557,
	U3CU3Ec_U3CReadTimeU3Eb__51_0_mA023CCE123BD5D9DF2635970BA3E5D664BCA33EC,
	Oid__ctor_m9032299ACEB6DC66674CB2D8889D875A1A9D308F,
	Oid__ctor_m4AF81B9D51BED48AE505251E0874EC654BA07B72,
	Oid__ctor_m533C0126C7C8D382D3E702287A9AD0CED2D0377D,
	Oid__ctor_m3973A5FE5482D45FABC8642E59B655274B0B5D59,
	Oid_FromOidValue_mFB93729052DE2FBAB456718907F3B64765D833E4,
	Oid_get_Value_m59D678A83ED4DE7D87FB7450C8CF4084DB506FD3,
	Oid_set_Value_m122282994E6EE138CE4284B052EF8BE2086CA72F,
	Oid_get_FriendlyName_m1B7B5D80B6B533A33DAB2B28DCD0B53927D1BD17,
	Oid__ctor_m29BE61A97566E5BE4D5AE6D0A5C31FE30885AADC,
	OidCollection__ctor_m9B8C252AE048962DAD7419597007ABA2793F9D98,
	OidCollection_Add_m1D4822DF4EA0B912A1DED6AA102D41F7FAEDC8F5,
	OidCollection_get_Item_m0AEC71C746E735035A05C50EE2721C1E8FE08F7A,
	OidCollection_get_Count_m9560D70809087591935904924A15FBEC1FEFEB11,
	OidCollection_GetEnumerator_mBB791F48E305B3355A1BCE624E0BB367331F82AC,
	OidCollection_System_Collections_IEnumerable_GetEnumerator_m4624AA2C7F0693698228803D9B59EFE6AAD6AFE4,
	OidCollection_System_Collections_ICollection_CopyTo_m4EF1FB9A18E822BA196BF3F0204B7C06178B9CFC,
	OidCollection_get_SyncRoot_mD310358F3BF4454604AD96C734BA6EC180CE7A85,
	OidEnumerator__ctor_m2168FEE96614FB7460C84762ED3728A5413F42E6,
	OidEnumerator_get_Current_mCED077699FA00ED57FA82F8B460C28B86F1B5C9F,
	OidEnumerator_System_Collections_IEnumerator_get_Current_m0B627B9F510EF4F283F40EBE17164B9231BB7735,
	OidEnumerator_MoveNext_mD42201944C163E81DABDE08B1535C4AB9A5778DD,
	OidEnumerator_Reset_m24F1F25812A7BFE9B02B312BA9E670C930622F27,
	AsnEncodedData__ctor_m90028EC56A5E36272EAA4A9279AE23358D46F547,
	AsnEncodedData__ctor_m90834437DE68932EB0BE8BB434DDA1DB896DA67E,
	AsnEncodedData__ctor_m3E8C1D1E0CE464176742B847DC99BF8EAABF15E6,
	AsnEncodedData__ctor_mA80B22E2D7D98198B55D04474F6926EF80CBABF6,
	AsnEncodedData_get_Oid_mBDB4803B2ADEE3BD40596E82FF738C2DC339BC28,
	AsnEncodedData_set_Oid_m877771219F651EA3FF834952300180274531C9C7,
	AsnEncodedData_get_RawData_m629D858D7D20E72C1F46124E5032FE0D3B32AADF,
	AsnEncodedData_set_RawData_m05F9F17765AD384ECE3A333DCBF6A0572099346B,
	AsnEncodedData_CopyFrom_mA5DD4A419C5FC29D82DBE9B69C9F8CBC2872A1F2,
	AsnEncodedData_Format_m557790DA037FAFD8C7FE28D4579487863DCB3346,
	AsnEncodedData_ToString_m669772943B78D9242BC5F176A4BD857326F95021,
	AsnEncodedData_Default_m883444CBD5238793D1B94A6DE273FF2F9C47EA70,
	AsnEncodedData_BasicConstraintsExtension_m74BFC192666755E9D8A088880789AB6EA84F1FCC,
	AsnEncodedData_EnhancedKeyUsageExtension_m3C95D5D2D8F81BEA5E33B264767C8A5E5575D7CA,
	AsnEncodedData_KeyUsageExtension_m9A9C72CF2A831A1849A55336FC8D6DEE097E28D3,
	AsnEncodedData_SubjectKeyIdentifierExtension_m3CEDDCED26AD5621407DAA401F385269348BB789,
	AsnEncodedData_SubjectAltName_mDE2F39E1A43C1E13D89319F221517EE54B1A6A14,
	AsnEncodedData_NetscapeCertType_m55E43EBDA7882304E8FE5F3DC134A2719F519373,
	PublicKey__ctor_m810A1EF4D8A0BAE8A3CB36EB8CC0BBA99D799782,
	PublicKey_get_EncodedKeyValue_m153EF781728D471C55002A40BBDA435F1B3A1213,
	PublicKey_get_EncodedParameters_mE338A4A69A15907E38A644D624A07DDD55C521E0,
	PublicKey_get_Key_mC0A4379DAA42B565878EE09310C43A4CCB45F273,
	PublicKey_get_Oid_mB5796F569C136759CB740F747C6A3B6F8976401A,
	PublicKey_GetUnsignedBigInteger_mDC1A423F4649E751E2F44C534F52F1103F1E8CA2,
	PublicKey_DecodeDSA_m32E09E73807395C6F5B0DD37E72874DD1FD1215A,
	PublicKey_DecodeRSA_m462116935D07EA1121C50801E6CCB6B21F0C5DC1,
	PublicKey__cctor_mBA35606057B7AD6687CD52BFED47441593DC5004,
	X500DistinguishedName__ctor_mF36480D37651E6FC6662E90ED7795F9A5C1C3DD2,
	X500DistinguishedName__ctor_m880E4D05D355F1B494990EFA3B9C7D0DFA0C8679,
	X500DistinguishedName__ctor_m95199AB45F165A1B774CA88B45F08889BFA5E162,
	X500DistinguishedName_get_Name_m57B2708AAA2B4E15AB6F6B513D88C4468898B5A9,
	X500DistinguishedName_Decode_mEC580A9699C0384B0196C80CD5ECFAA2151243BD,
	X500DistinguishedName_Format_mD7DD9D893081C391AA74C66CEC456324EDC36969,
	X500DistinguishedName_GetSeparator_m2317B62BFC2A082692A05C18874763719D2E5BF8,
	X500DistinguishedName_DecodeRawData_mB465AEF2F370353BB809B6B310064FF67DC2D970,
	X500DistinguishedName_Canonize_m7B0365D90F71C238DAA5795422320E1E4A63EB98,
	X500DistinguishedName_AreEqual_m91FA80CFEC70C71A5E3C73D5D40BFA220427588C,
	X509BasicConstraintsExtension__ctor_m672BE9B41107A8C30454BF4CAC120D70512BA89A,
	X509BasicConstraintsExtension__ctor_m76C88E8D4C7CCEC7B2C3D56CC58A56352F7729E4,
	X509BasicConstraintsExtension__ctor_m02334D8CAA280B339CA41E2019CF1F02C8B2E7DE,
	X509BasicConstraintsExtension_get_CertificateAuthority_mB4ECEF43A245E97DA80C6323BE339BE2DF99FF5C,
	X509BasicConstraintsExtension_get_HasPathLengthConstraint_m8F7C371EFF99BD110663DB106BEE889955255BC7,
	X509BasicConstraintsExtension_get_PathLengthConstraint_m4D3F016E42C243887D07DD955518A818DC14BBC5,
	X509BasicConstraintsExtension_CopyFrom_mCC603D99CEC828BDF86FDEB3C4650CF4D3753B8E,
	X509BasicConstraintsExtension_Decode_m7C50097B6238FEF4DAD6A9A09A449DB0CAFFEBEB,
	X509BasicConstraintsExtension_Encode_mD7FA31A70B025F67062C3844E4FEB09B14A4DE2B,
	X509BasicConstraintsExtension_ToString_mBFC61BA07CD2BB8EACCC19A5AE72EA81C11D02E4,
	X509Certificate2_Reset_mE779D76DC57C54B674D7588861324F02DD784C3E,
	X509Certificate2__ctor_m84519718D6C34074E7037786ADAE51290460C2B1,
	X509Certificate2__ctor_m34CFFC999D3A152729A5C59DBE80AB709547DA19,
	X509Certificate2__ctor_m196538E20CE749EB7220F5AEF93FEBF0C7072350,
	X509Certificate2__ctor_m7DA4EE40349835A87AAA8E5717489DFD6C44D0A2,
	X509Certificate2__ctor_m909B8F5F1E15BC0006AF21031FF21AB2CDE8266C,
	X509Certificate2__ctor_m2C7F482BFC1A0BAAABDD8BE9BBB2026DA5F3E600,
	X509Certificate2__ctor_mB1BC3DC26B71A5D5D3753B04DCA16AA49627EC15,
	X509Certificate2_get_Extensions_m3746E7EB004789DF5AF56AD36209676860901579,
	X509Certificate2_get_HasPrivateKey_m561EE07579AC86B96856A73367961AD01DAD8567,
	X509Certificate2_get_PrivateKey_mB2D1370AFF6F005B4A18A2223363C353B8D24A4A,
	X509Certificate2_get_IssuerName_m47CB829F5388F2E68A20D473F586FA847739B83D,
	X509Certificate2_get_NotAfter_m925CA3901B7856D0692C6DED4F6428A40A726787,
	X509Certificate2_get_NotBefore_m656CC9913A0E6F6F0C06185F3B63F3A2EDCEB801,
	X509Certificate2_get_PublicKey_mF1813BEE1EE04C593FBAA44590E2AEBC23A72304,
	X509Certificate2_get_RawData_m34518B0CB70515321029A2265AA89EFEB50177C6,
	X509Certificate2_get_SerialNumber_m12F610EEBE485A29FD49A06E8A33BB690476309B,
	X509Certificate2_get_SignatureAlgorithm_mF3FE8751A82E00E38E315EA893575D125F4F7354,
	X509Certificate2_get_SubjectName_m0A767E93883BD9D984903B6753819CB7E02D90BC,
	X509Certificate2_get_Thumbprint_m0A33F62238AF002C1B93A0F93A361B890E61DE8C,
	X509Certificate2_get_Version_m6A4FB2125DA64437D184A64E731F2B58738F0B09,
	X509Certificate2_GetCertContentType_mDEFBB0D6E87233AE62662CA74E7142FD1379D3FA,
	X509Certificate2_GetNameInfo_mDFE32FF38933F2B9F652B27B1EA857AD463222DE,
	X509Certificate2_ToString_mAC732FED5427D5551E2CC9B06C145128E7F6563B,
	X509Certificate2_ToString_mA115124642674F646AF34E6B8E2FB54870026342,
	X509Certificate2_Verify_m5BDB9791EDBA2F518184D4AFC2FD1B656F4C4591,
	X509Certificate2_CreateCustomExtensionIfAny_m2290FD40B2CB727A1504C2F0B34D1FE0E5B11000,
	X509Certificate2_get_Impl_m21C56437F9DD096FA112C2DF07F04EB2E381015F,
	X509Certificate2Collection__ctor_mC7A17C695E7DB906D549A0BF60943D2490F4C2B9,
	X509Certificate2Collection__ctor_mD3765F49CF526B51256B004DDA856A291EFEF74C,
	X509Certificate2Collection_get_Item_m46D8DA4B824642F03A2E448CB52AB591850DE9C4,
	X509Certificate2Collection_Add_m6C2C142C7BF143E83388F8793A64890F44996ABB,
	X509Certificate2Collection_AddRange_m764C331A677795DCA45A496DF9752B58F2FDA2A8,
	X509Certificate2Collection_Contains_m71ADE296B2581756ED2484BC42CB4D51A9172688,
	X509Certificate2Collection_GetKeyIdentifier_mA7189361FD18F2665E09354BA391E96B189B44EA,
	X509Certificate2Collection_Find_mA13F604A4AAF8C5EFFA436EFA7785B7E27C0471C,
	X509Certificate2Collection_GetEnumerator_m2D2A261B2F68A857C3C8B0037C5E05F10A6761FC,
	X509Certificate2Collection__cctor_mC3757CA5D10515491E20F8CEBAEE82745C49F7FF,
	X509Certificate2Enumerator__ctor_mBF306968F1D046A5084D7E70CE7A7A3E0412357B,
	X509Certificate2Enumerator_get_Current_m7D19172490F76C81A7E7DB5AF8D172400BC95B53,
	X509Certificate2Enumerator_MoveNext_m824241D32F6E1B3FF4CAE0E2B370BB5E32238F19,
	X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m11E9442F6F2EB7E5829832ABAD721E4B7E4A1CC5,
	X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m28901EEB56D14B1402463992A26EE714EA1B2B85,
	X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m84371FC9208871CE46B7D09A77AF055C52938FC6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	X509Certificate2Impl__ctor_mE6FF68AD589010E57C86555935DB601680AC1839,
	X509Certificate2ImplMono_get_IsValid_mA2D572B4E2382A93FEB9F88940269684D433439B,
	X509Certificate2ImplMono__ctor_m3F6BBB4381EB9A720E11577B9F663DC10B3B93A7,
	X509Certificate2ImplMono__ctor_m622D5FDEB76F561EA4AE9C8F4597D8E6AE1C6C28,
	X509Certificate2ImplMono__ctor_m1B2FB6BBB7558184F07C2E42ECE23C70075A9B7D,
	X509Certificate2ImplMono_Clone_mE74EE518CD4FAD43F670E3B9140350A0C9766385,
	X509Certificate2ImplMono_get_Cert_m456536A0FC9BB84D859CC7ECAA421203FDCE6E00,
	X509Certificate2ImplMono_GetRawCertData_mF4AB5CCF83D23AFE455D75CC7545423221565CC0,
	X509Certificate2ImplMono_get_HasPrivateKey_mE1EE233BC55F71002EA56248BCCADF483BE07B22,
	X509Certificate2ImplMono_get_PrivateKey_m2B92DBE7562BBE32D2DC2F5ADC6E96FCD2497F60,
	X509Certificate2ImplMono_set_PrivateKey_m6F611F15AA315BB5F2B21984CDCB7A955855D205,
	X509Certificate2ImplMono_GetRSAPrivateKey_m88696965DA58E0E6EDAC4E6397F13A54031359A9,
	X509Certificate2ImplMono_GetDSAPrivateKey_mFDE83C8B6DB600DE9FC20E791EDF3F1FA57DB77B,
	X509Certificate2ImplMono_ImportPkcs12_mE10A545B13FB03F48E6F4E7210F6FE86BEAD77B0,
	X509Certificate2ImplMono_ImportPkcs12_mC29B89A6EDBB108185C4A91028F6EB496ACB2C98,
	X509Certificate2ImplMono_Verify_m2DEACB934D750CA4071F66F4A3067B9A2F24C9E8,
	X509Certificate2ImplMono_get_IntermediateCertificates_m304680821DC498B386CF82B8B82DD1248DE52C85,
	X509Certificate2ImplMono_get_MonoCertificate_mE723B3BB0523A176164138FE73F1F34C28E3EB2A,
	X509Certificate2ImplMono__cctor_mAA23F03A760725B059D7DE6FB55EFDA78FFFD328,
	X509Certificate2ImplUnix_EnsureCertData_m4BD3C65CDE138D7239A18652543C7A93BFDD4E62,
	NULL,
	X509Certificate2ImplUnix_get_KeyAlgorithm_m91A6386635D6554DFE8D3ABE9BE166A2B39965DF,
	X509Certificate2ImplUnix_get_KeyAlgorithmParameters_m97B958D5098C698F869D9C9C9BE13AD43AABCFD7,
	X509Certificate2ImplUnix_get_PublicKeyValue_m0C7862B1E1A55C681109592D4BD09439F43F301C,
	X509Certificate2ImplUnix_get_SerialNumber_m5388A1C777603B896EFE49885A320520AE8BFE71,
	X509Certificate2ImplUnix_get_SignatureAlgorithm_mC5519532C05449E473FDE2BCAB54F2429AE8A1C4,
	X509Certificate2ImplUnix_get_Version_mCE7533BFE8F2E54A4386855F1288E87CB431BA6D,
	X509Certificate2ImplUnix_get_SubjectName_m1080CB4AA55DEF3052FABE2681FCD500AEC2A786,
	X509Certificate2ImplUnix_get_IssuerName_mE929B42F7137A781DBAE7BA1DB8704D773DF50FF,
	X509Certificate2ImplUnix_get_Subject_m32692DEEF247791D84F4F69D487D11A1243919DE,
	X509Certificate2ImplUnix_get_Issuer_mD59B5CFCA2DABBDCA2B68A1FA13CD037BDEC5A35,
	X509Certificate2ImplUnix_get_RawData_m113719E0C35A6A4917249A7F94D9146563ED6713,
	X509Certificate2ImplUnix_get_Thumbprint_m493D3178B247696830F366F95EDEEF7619D7A174,
	X509Certificate2ImplUnix_GetNameInfo_m9F4BB6FBEE9CD1465008C26862D4C69854FAD653,
	X509Certificate2ImplUnix_get_Extensions_m216C3BA296F656BC33797000BAD6B06DD871AF3F,
	X509Certificate2ImplUnix_get_NotAfter_mC7CE817FB14A0858D6FF56B92BEB9C62A54E3709,
	X509Certificate2ImplUnix_get_NotBefore_mD0D40AAD63C151332B3A3F50B7856BAAE0D2A313,
	X509Certificate2ImplUnix_AppendPrivateKeyInfo_m0A248F4E075D601150745C99FAA3DE8131A52FDF,
	X509Certificate2ImplUnix_Export_m59E685C8306695FBCA2725F56D6127CDE6B0CA10,
	X509Certificate2ImplUnix_ExportPkcs12_m936EF9B64E83A0660C321DD43CFBCBFE5807AD9E,
	X509Certificate2ImplUnix_ExportPkcs12_mC956F970A09A223AAD1B163E787BEF552623FB0E,
	X509Certificate2ImplUnix__ctor_mF3392E6B555FBA22CBDC93DF0C53479586EBF327,
	X509CertificateCollection__ctor_m237A27DC157785FBC50EBAF3562AAF0907BF97EB,
	X509CertificateCollection__ctor_mE486AD558BE6F0D8FD89AB88DE922DFA684C4AA7,
	X509CertificateCollection_get_Item_m9EC79D4B62FB412278F92F3641969385252F6572,
	X509CertificateCollection_Add_mE6123E24D942801BC5A41196F92B0552015E110F,
	X509CertificateCollection_AddRange_m45914B11721824E8BE7FC2B2FC4ADD28F2C45EDB,
	X509CertificateCollection_GetEnumerator_mBD26239EE292204789D87CA73DBA5C898C2F4F14,
	X509CertificateCollection_GetHashCode_m3F1BB9203AB722A6A9CF574390B0244F47485111,
	X509CertificateEnumerator__ctor_m7FEB0CE4B71CB9CE860AAF29554279392249D237,
	X509CertificateEnumerator_get_Current_mF256C5D7194A99B5C567F6E62806C153DA8864F1,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_mBBB3E8D212FD598C9C90F880D31B8227852F5075,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m297DB4F7F1CB60E4B3E0BB2BBF1B80279E5E6936,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_mC0D277DD2410455AE4EBCC14C4258F5F5F8677CC,
	X509CertificateEnumerator_MoveNext_m2DCBF7B544E0E410B90DF3FD96B473B11A37E8DA,
	X509CertificateImplCollection__ctor_m9A1E10EE2FC833E8F8C938A91AD907BC437452E2,
	X509CertificateImplCollection__ctor_mC7C4FAB75F44B53B8FE8D9CA3443F1411CD0BA11,
	X509CertificateImplCollection_get_Count_mB81B621804D7FB640FDD4177610CEEDF0484378D,
	X509CertificateImplCollection_get_Item_mA250EDFA1ACBE344A52C5CD818A6578CC07763A1,
	X509CertificateImplCollection_Add_mDF2DF7D94CEB84BCCC7292831C838FCDB1322B8C,
	X509CertificateImplCollection_Clone_m2AC54E741E6AB673D07464C05D27A3FF9F4112B0,
	X509CertificateImplCollection_Dispose_m84BFDFFCD3B9DEC80EAFEBBFD156F1E246DBA617,
	X509CertificateImplCollection_Dispose_m77FC31F821A4066580E5F99685EB915466F034AC,
	X509CertificateImplCollection_Finalize_mE46191767109F329A19BBD0CC4BC1AA6E2C28A95,
	X509Chain_get_Impl_m4B712BC99067069BCE090957082F5FE17EE237A6,
	X509Chain__ctor_mE938759A3C3CA007FB171DDD375312F03394D3F6,
	X509Chain__ctor_mF765A0057950154FC61FF1D112A8EFCB73314EC4,
	X509Chain__ctor_m6F2404400D7192FDA4CCB37DC67F18F12616000B,
	X509Chain__ctor_m2B6C93313DDBB62BE6E2AE073958B7662C5FBB7D,
	X509Chain_get_ChainElements_m656A4A5EEA69BB8C4F5995DEF7C88AD9EB961AED,
	X509Chain_get_ChainPolicy_m947CBD207A65DB28D0EE7909004E44CBEDF34348,
	X509Chain_Build_mF368D6BAE5925C8311B41BFBA08C5D91277C67E5,
	X509Chain_Reset_mE9CFDF83BD0B08994DEF29C30556429B2FD55860,
	X509Chain_Create_mA8CE0BB7C2DD4A66D217213F5D94B9EF21851E64,
	X509Chain_Dispose_mC10F7CE7DAF0799F13C798184330C046222E305D,
	X509Chain_Dispose_m24B2B8C5A29F06A8A1F8564EB0B39820616B3930,
	X509Chain_Finalize_m76B5ABE85F3D5596788618FFD03C4831DB0EDB39,
	X509ChainElement__ctor_m7088FBE9A69384945CA1A40CD973E1846FBD720F,
	X509ChainElement_get_Certificate_m6AF210BCAD2D94C19DFEF475BDAA84E5FF4AF105,
	X509ChainElement_get_ChainElementStatus_m24E928225ABCAD112A7D1D5A4ECDC2B89F3AF705,
	X509ChainElement_get_StatusFlags_m8ECBC4E3E1149987BA1A837775A8AA68496B87D6,
	X509ChainElement_set_StatusFlags_m7DF3FEE1B3B197C198FBA57215414725119B9B1F,
	X509ChainElement_Count_m2E6A6ADB8C118E8879B3C6E29093AE0172A4A0F6,
	X509ChainElement_Set_m7DB50EE91B2762D0623FA12E7A7B361602CE3D21,
	X509ChainElement_UncompressFlags_mB26BFCDCC458958F4A0A398DDA7516E7A54DC004,
	X509ChainElementCollection__ctor_mAEF26718ED02C8480A7041E3C6B66500950E013D,
	X509ChainElementCollection_get_Count_m5448D972C3009FA67B3F4E13061B6012D561B78B,
	X509ChainElementCollection_get_Item_mC2A5AAFBAEF74FB085685620066058ACDC71D69A,
	X509ChainElementCollection_get_SyncRoot_m435D18A280E3D7609FB490021AC2E1436BE61568,
	X509ChainElementCollection_System_Collections_ICollection_CopyTo_m4909BF1B409D795DDD3D8D2206FA17B88AEA5B29,
	X509ChainElementCollection_GetEnumerator_m7C2AE0FD14790330E1C365116C6654B137E83817,
	X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m25F89A23A06D302469D28DDA337A0B0C5C463269,
	X509ChainElementCollection_Add_mA032CE8B536713B0E070C9A211B084706D04F6C8,
	X509ChainElementCollection_Clear_mF0C2AE76F73C525E29E808615576C7EE11409659,
	X509ChainElementCollection_Contains_m9B8619202762237150DD4DEDE83ECF8EA73C25FE,
	X509ChainElementEnumerator__ctor_m552B42A7693729672B7EF7F95B4211F6BD69AE6D,
	X509ChainElementEnumerator_get_Current_m0D089910DA01219AA1079B5C014349B28C106E68,
	X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_mCE2DFBF81D34976171E7A936A04F87C0A625B025,
	X509ChainElementEnumerator_MoveNext_mA8C7DDBB648F9B1F15FA3E169D4361374BE2B995,
	X509ChainElementEnumerator_Reset_m4E0A51B45009F95A6E9B64C15F87CC9E48444A40,
	NULL,
	X509ChainImpl_ThrowIfContextInvalid_m85AF480D1B87D8DA3D780040385CBB88D9B173CD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	X509ChainImpl_Dispose_mB7A6D60B308E90F0173FA8160F7FF0A00709A25A,
	X509ChainImpl_Dispose_m9F8B96FFCB317F7F8C8E1ACA82061EA7404B732D,
	X509ChainImpl_Finalize_mB70B742567D0E59AE522AA9F80800B308D87BC6F,
	X509ChainImpl__ctor_m16F671EB135FB6E7E3CF6C42715D7106E6FAAF18,
	X509ChainImplMono__ctor_m0ACF8D6927E705577989597DAAF5CFE6346B1A76,
	X509ChainImplMono_get_IsValid_m1140F06D0030BA5A55753B46FF61AE846B845F54,
	X509ChainImplMono_get_ChainElements_m7EF05738D8235376B4B24F8AD95C59104C2ADAA7,
	X509ChainImplMono_get_ChainPolicy_m7E8861BDCB501BCD6371583BE75CBD4E0C754209,
	X509ChainImplMono_AddStatus_mA336CCFEEB670FDF921E26F14C7CF32021A7D838,
	X509ChainImplMono_Build_mE56172C044107FDA771CD1A9654948E079B2E6F9,
	X509ChainImplMono_Reset_m1155C3B608B1BCEE6F5BD6351A3D63013F1CB292,
	X509ChainImplMono_get_Roots_m9F02570D4FEDCC596E09D548969BD82D1D6ACCE4,
	X509ChainImplMono_get_CertificateAuthorities_mA0B07CBA55DB93E0EA463F834358CB231F740282,
	X509ChainImplMono_get_LMRootStore_m45317F8F6713CAB17096C8CC557A5660130D78A6,
	X509ChainImplMono_get_UserRootStore_m7D16C83BB80412C6697AB2A39D0AE8575659BFE1,
	X509ChainImplMono_get_LMCAStore_mC5519D87A02C14345D45110E5431A32E34D4E048,
	X509ChainImplMono_get_UserCAStore_m025075F5390E1CD388809D3857FF03FBB3CF12AB,
	X509ChainImplMono_get_CertificateCollection_m65D90AB33B635CD388E73D20EF02B3C1EDB57292,
	X509ChainImplMono_BuildChainFrom_m240087A2D01469F43D36A194637FAB93DE7D574F,
	X509ChainImplMono_SelectBestFromCollection_m24FBA3CA0A492C7708FF11E763D8B1099F36BC06,
	X509ChainImplMono_FindParent_mA64A7D939DE5F34944AFF6D58E2391BF47D3D0A6,
	X509ChainImplMono_IsChainComplete_m1FA60E2128868B2FDCB258B5D16F15411C540823,
	X509ChainImplMono_IsSelfIssued_mDF746101F4634DE86C55A2AEB1936D8E68F751CA,
	X509ChainImplMono_ValidateChain_mFC6C8BB61FC9DF99BDB656ACFACD0BB4BD57A177,
	X509ChainImplMono_Process_m469792B377599FD4347315913B28356AE5C57D0E,
	X509ChainImplMono_PrepareForNextCertificate_mD651DA79F6A64548599D510B1157DA0F44386872,
	X509ChainImplMono_WrapUp_m1546AB65C518C3E3A175A1C907FB5641AB8D0233,
	X509ChainImplMono_ProcessCertificateExtensions_m24B4A9B57A31DED6492E65D47108DE0F0609990E,
	X509ChainImplMono_IsSignedWith_m841918EE1C928C94D6788FC5F446772123515913,
	X509ChainImplMono_GetSubjectKeyIdentifier_m0D8BA44577CA7AE09AA6E2998E8C77FCA7CC0BD0,
	X509ChainImplMono_GetAuthorityKeyIdentifier_mB0CD4B03B79394A27F337CDE31F3E1A7DD0BD0B1,
	X509ChainImplMono_GetAuthorityKeyIdentifier_m63F1F5346CE98F07873A731826508D8E7E53B8B3,
	X509ChainImplMono_GetAuthorityKeyIdentifier_m8815C38DD46B58C0322D8F4E217DB785637F12FA,
	X509ChainImplMono_CheckRevocationOnChain_m8178244CB6B50DF10CBB3669ECAF005D649B2D01,
	X509ChainImplMono_CheckRevocation_m8E1EDE1A95C4A09E39086F871B9D80C22D68BE35,
	X509ChainImplMono_CheckRevocation_m73491E913ED7EB66E573B727C552A3012A229962,
	X509ChainImplMono_CheckCrls_mD2938D2C74DD5E9EA76205425E74A9EEB6C4C65E,
	X509ChainImplMono_FindCrl_m10BA0B67DD7779475981229634AB40F239B60373,
	X509ChainImplMono_ProcessCrlExtensions_mD92E0695E51E62A17AF7326E905C2592AD3F7072,
	X509ChainImplMono_ProcessCrlEntryExtensions_mF1426E8193BC2553983F697EEB8F3035B70FE43E,
	X509ChainImplMono__cctor_mA05EEC443B725B1B59B85DB2A6C9D4380D4CDCB2,
	X509ChainPolicy__ctor_m069F83BFFF3E79ED4CEBE8CF553CC65576F8D3D9,
	X509ChainPolicy_get_ExtraStore_mAC7E1235E5CB8DD373B859F2B5C142638D7BBD2D,
	X509ChainPolicy_set_ExtraStore_mECC7CF51491C5A6E20F21CCD65F6EC66FF0BAF3E,
	X509ChainPolicy_get_RevocationFlag_mB2DEC5907796536DB5304CAC972EA7B34C49CDAD,
	X509ChainPolicy_get_RevocationMode_mDC8382C2ABF29DFE3972E40343F025A52488AF29,
	X509ChainPolicy_set_RevocationMode_mD31CF4CA6A09443829F91980F8AC98E21D5888AE,
	X509ChainPolicy_get_VerificationFlags_m66FC764D76DC6AF35F600BFFA710D826B2EF9B80,
	X509ChainPolicy_set_VerificationFlags_mA0C6910C20FCC1FB1DA1A35ED2E7429FA527A69E,
	X509ChainPolicy_get_VerificationTime_mDC1D5996C7077FECCB41D38974815178627EE82D,
	X509ChainPolicy_Reset_mEE02175020B69CE1732C0E89A2AE60F0BA5B1417,
	X509ChainStatus__ctor_m5AD96E11604F8DB96673677AC2FFF68EBA550859,
	X509ChainStatus_get_Status_m58AB369B6D5CDF2436644C1936953058FBBDEF99,
	X509ChainStatus_set_Status_m55D0302200912F7B74F7805470551EB33C28246D,
	X509ChainStatus_set_StatusInformation_m0A4B88F2DEF120DB1D38E3EFF86170C9D272DB3F,
	X509ChainStatus_GetInformation_m2EE262A68356D15E2C691E84DA10B2E8E3EFED26,
	X509EnhancedKeyUsageExtension__ctor_m7ECF7F6EEB96855182FD5466A04B4712B3F4C746,
	X509EnhancedKeyUsageExtension__ctor_mBDA55EB3C6594D4F6A06102B4EAF633F86C3E092,
	X509EnhancedKeyUsageExtension__ctor_mE0CA01F89B6532A7E7B256F248C23B61F0A909CC,
	X509EnhancedKeyUsageExtension_CopyFrom_m9577FF97B8E3074B12B7056BF7E30AA0F84309BF,
	X509EnhancedKeyUsageExtension_Decode_m1110DD9BD5D717540009DB5D90805458CF07423C,
	X509EnhancedKeyUsageExtension_Encode_mBE23AB15722B1822172F860B5F7651EA871B5550,
	X509EnhancedKeyUsageExtension_ToString_m9576793CE590716A3BB5AAC1632F946DF75605DE,
	X509Extension__ctor_mFC88C10C2C6718E6AE74CE37E10D4EBB8FD67BAC,
	X509Extension__ctor_m442E6EED0F68D0FA2D7D7B085BEAB11F35B9157F,
	X509Extension_get_Critical_mEF827F09D95784210C2F8D5FD7BE27BB54803E35,
	X509Extension_set_Critical_m19FBE6E0C8B9069E5A2C232DAAE06046AF18EFEF,
	X509Extension_CopyFrom_m674ABC4ECF701B708C28B546B42B7799CA24D4A9,
	X509Extension_FormatUnkownData_mD0463F5096A6937E95E3165048299CBA9A1A6F7D,
	X509ExtensionCollection__ctor_m279F781688D6D8D301D06282132D8A02DC864AE9,
	X509ExtensionCollection_get_Count_m2708E6751246E4E5E64BAE8A99FEC79CFD61E9A1,
	X509ExtensionCollection_get_SyncRoot_m22D5573E42FAAC75DC4DF16C962183E0CDC6AA0D,
	X509ExtensionCollection_get_Item_m411222FFB9DF3584180BA8A09ED061CFD5774F9D,
	X509ExtensionCollection_Add_mC02BE13A042962E83E849483642F122AEADD2A79,
	X509ExtensionCollection_System_Collections_ICollection_CopyTo_mD9F01B71381CFDD12AF55B244025A99529465472,
	X509ExtensionCollection_GetEnumerator_m7F4BDDDF50FD335D3F6A2AB490221D27524BD0BF,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m574551E2A07A5E860E812C597544B627EE4C4E55,
	X509ExtensionCollection__cctor_m302D04DAABB7985BF831D50E7956ECFC3B2D199F,
	X509ExtensionEnumerator__ctor_m64C36BD278E62F11279957CC3A5F58A2D7025976,
	X509ExtensionEnumerator_get_Current_m6057D68B2D7750669A51630A1561FCE7F597E232,
	X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m5E7878C8D59799831C4350758873B6E0A358538D,
	X509ExtensionEnumerator_MoveNext_mDCF343DCC4699DFB3D6E72D84AF8A6C11AB247C3,
	X509ExtensionEnumerator_Reset_m1DF9302CFBB6B288D94EE627E3D4DACD78B94A9C,
	X509Helper2_GetMonoCertificate_mA77B189F919C6BD9F882A59E3743A3AC2230206C,
	X509Helper2_CreateChainImpl_mABBFC689A2A031B8E93070F0485962AA5E8B214A,
	X509Helper2_IsValid_m2D40E03D265A1B1D53B6B8C726E833358AA4187D,
	X509Helper2_ThrowIfContextInvalid_m12213152E825627422EBDC2D88B524AE528B70B1,
	X509Helper2_GetInvalidChainContextException_m44B5C4113D72A0DA0EE7E1D4C7048FF631CA7D46,
	X509KeyUsageExtension__ctor_mE0EE68497EDEDD78213EEDC931D68DD35DF6340C,
	X509KeyUsageExtension__ctor_m1DE9CD768712A07E5803496501CCFDA81CFB2689,
	X509KeyUsageExtension__ctor_m2FAA526721435935D0B10E0DB916AF56CFD84751,
	X509KeyUsageExtension_get_KeyUsages_m1BD6C5BC6E3E380982C135B2E81731435345144F,
	X509KeyUsageExtension_CopyFrom_mCBCF5CA17676B2030F6C82ED80A008EF029AF733,
	X509KeyUsageExtension_GetValidFlags_m82B1C7DA4F4E0F16090782189E1B4984D133BB45,
	X509KeyUsageExtension_Decode_mB64239ABDEA862E43E26CF324D3E41ECCF3088BC,
	X509KeyUsageExtension_Encode_m38D20B2FB68EDF3594EDC0D9BA88800ABF305542,
	X509KeyUsageExtension_ToString_m270131139984275D7571120A2984F0F5E77E79DC,
	X509Store__ctor_m4BC006F268440F969A1E2550A4884AA9FB4FEB1E,
	X509Store_get_Certificates_m3605E1FE284AD55C48E874C49CD22C11828D2DA0,
	X509Store_get_Factory_m78BACD85C7BF54F835A7F9AA9F4F163F9E3F987A,
	X509Store_get_Store_mF57B442ECFBBBB6980A36A80B64399779C20D253,
	X509Store_Close_mE444AB5FE15C355E4EF9369E7EB5F78F345320F0,
	X509Store_Dispose_mF1DA64DEAEEBAFEECD1E945B30BB8E169984D7E1,
	X509Store_Open_mE65F1CB5311705EB955D576295B048C1155CCED8,
	X509SubjectKeyIdentifierExtension__ctor_mFC2A19B1301ABBA5F6C5CE407ABC4F4CA1EB49B1,
	X509SubjectKeyIdentifierExtension__ctor_m421293DCB67F3A918098962166B8166FD9657D08,
	X509SubjectKeyIdentifierExtension__ctor_m610C9C804421B7CB519F3A5C7D66827CC1106EE1,
	X509SubjectKeyIdentifierExtension__ctor_mECF6398B9841FB4533228FE77F51F93316EADE94,
	X509SubjectKeyIdentifierExtension__ctor_m3DD2C4E1505A6E625499FBD833BFC076D28D83EB,
	X509SubjectKeyIdentifierExtension__ctor_m1858C1B050FAF65946B790737652E874781B4B56,
	X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_mFF2A155072C67F430E7F4B178733A80388943CBA,
	X509SubjectKeyIdentifierExtension_CopyFrom_m959BB16C6973B1B565D02ED9993AF9069019D77A,
	X509SubjectKeyIdentifierExtension_FromHexChar_m0547F9E771F6BE506DEA650D696F7794F91B59A3,
	X509SubjectKeyIdentifierExtension_FromHexChars_m7143D074BE9451C082422BA3FF118C17D873E5A1,
	X509SubjectKeyIdentifierExtension_FromHex_m7A7DD49DB74BC4020A28007CDD5765765EC9DDB3,
	X509SubjectKeyIdentifierExtension_Decode_m9E9B87F478F55E202411CBCE3AA314CBF73C7F30,
	X509SubjectKeyIdentifierExtension_Encode_m8746D45CE4AC46543D3332BC6001FA1BC7A72CB3,
	X509SubjectKeyIdentifierExtension_ToString_m9AE51334149DE6200E554F9C30E2DC5570D36707,
	TcpValidationHelpers_ValidatePortNumber_m87E6293214842C630C35F9863ADE5AD844235480,
	HttpVersion__cctor_m237BC8431A75E09C3610837B9FECBDD305CF2622,
	IPAddress_get_IsIPv4_m912994B89DDE21F85E4FE40381CDFF1688BE3F38,
	IPAddress_get_IsIPv6_m957F80A11C7E40C2CCAE554F12AA547E387B6884,
	IPAddress_get_PrivateAddress_mE1CD072F1E7BDF981FD71CBD984F5FD2A2EC6399,
	IPAddress_set_PrivateAddress_m73BD8A117538CBDB80F8FF47D31609FA4855D74E,
	IPAddress_get_PrivateScopeId_m3C01EDB8971607BAC0398557E295B5A818191532,
	IPAddress_set_PrivateScopeId_m2BA43F20C3B975BDAC6E330C09E09BFD34594A42,
	IPAddress__ctor_mC240D2060BA0F5A96D286CDE3772FE4354D7030B,
	IPAddress__ctor_m8D2578858CF6F8E56591F45C8A14150DEFFA44F5,
	IPAddress__ctor_mFD3C42E1EECC3099D6E347238FB2A0EBE822AF19,
	IPAddress__ctor_m9673193087826D4DFAA57CE98431390FD47C512F,
	IPAddress__ctor_m16761A05E56D81D4519A8E0FEBB7910E17A1FB7C,
	IPAddress_TryParse_mB8DC9EE090ED3BE8F8C9D419759AA9FF4A498D3B,
	IPAddress_Parse_mD7BEF4D6060D8BE776F559C5F81F195A9917CF1C,
	IPAddress_TryWriteBytes_mB6CA9DA47541FD37A67BCF614983BDBD3581346A,
	IPAddress_WriteIPv6Bytes_m4505E41EC6C2E484293B39735D63DF58BF37539B,
	IPAddress_WriteIPv4Bytes_m4E03460BC962536BD927917BD374A8B1E1C23C40,
	IPAddress_GetAddressBytes_m1501E0143C791E3A065E508F5535D8E206652EC9,
	IPAddress_get_AddressFamily_m1CE4BCCE499BD70B22F9E37B3F266F9306A98C21,
	IPAddress_get_ScopeId_m464E5C5597F29B05135EDCA6DDB1667EFA70EEF8,
	IPAddress_ToString_m91FE727877BEAA0115F497387E2E9CB0B45C3895,
	IPAddress_IsLoopback_m673C9C63D59BF356AB7964CEE148EE1F3E01CC19,
	IPAddress_Equals_mC4514B3B7F9B95B79A1121706C115AECE61636C7,
	IPAddress_Equals_m5EA56A0CBC4F66012C11628D8CC75E03E5A8C462,
	IPAddress_GetHashCode_m243850627E81961E58E3A7A0A39A1F365C47A4BA,
	IPAddress_MapToIPv6_m3D243981B6A8235516A4D8A68FC555B59675DE33,
	IPAddress_ThrowAddressNullException_mF55B22F51151F17731038C12D118EBEC63C9CC4A,
	IPAddress__cctor_mB1459880D331879EB11466988E78D88EBA0F6735,
	ReadOnlyIPAddress__ctor_m0E47AC9070535FCCC4796C165AC8DAB6FA2B5AC5,
	IPAddressParser_Parse_m924FDF6137E57D2C205C767578A902FAC6AF9F6C,
	IPAddressParser_IPv4AddressToString_mED56559517ADF1172DEE5FFBA56B7A3C66060303,
	IPAddressParser_IPv4AddressToString_mC3C2E797E621FC478FCDBADFE9DE20909516D706,
	IPAddressParser_IPv4AddressToStringHelper_m732D3622C399BF418CC039E34BB8EBA0F5B7C581,
	IPAddressParser_IPv6AddressToString_mA5E8E4CCAA57C745E4CF564CB6CA263661A1BB13,
	IPAddressParser_IPv6AddressToStringHelper_m5E32EC869045EA58C14FDB598476013D76568A27,
	IPAddressParser_FormatIPv4AddressNumber_m6C347346B73C32788716D03C3FB4DEA954EE37C2,
	IPAddressParser_Ipv4StringToAddress_m0E8B9B120883B4280B6DB894C47C5527D0A8C6E9,
	IPAddressParser_Ipv6StringToAddress_m2F6666E91E78CB63F69EEDAB23F5DBA65353734A,
	IPAddressParser_AppendSections_mB41667569210B39E7A732CE4EDDB684FF7B97C94,
	IPAddressParser_AppendHex_m078E3C19C653E140FC8E6943F292CACBB29A13AD,
	IPAddressParser_ExtractIPv4Address_mF574B7C9D0B4DC4C22DBC0F2A6701FC816A2128D,
	IPAddressParser_Reverse_mD663C9994E59E47531F7D69E576B14D0572676D5,
	IPEndPoint_get_AddressFamily_m9941B51AEDFC4A65B614DCFFBD6A11644DBA5851,
	IPEndPoint__ctor_m16D9FC6DA967F79B612A59A804E1374B9D324E4A,
	IPEndPoint__ctor_m902C98F9E3F36B20B3C2E030AA91B62E2BC7A85A,
	IPEndPoint_get_Address_m72F783CB76E10E9DBDF680CCC1DAAED201BABB1C,
	IPEndPoint_get_Port_mFBE1AF1C9CC7E68A46BF46AD3869CC9DC01CF429,
	IPEndPoint_ToString_mC285E84E3FB9A15B223DD4401DDFC1E4171E766A,
	IPEndPoint_Serialize_m4495698C21946D39519728ADC8D4FB9860B52DD5,
	IPEndPoint_Create_m3F397111A683D18A38FC7275D55BB94B0FC7350D,
	IPEndPoint_Equals_m480BC01DF0A7D8DA996A030546DCECBE2B5D6AF7,
	IPEndPoint_GetHashCode_m888B6D706296BF20224488C4909807D26655E4F3,
	IPEndPoint__cctor_mF8F48FE436F60E44A928B1967AA49F472E8D1D05,
	CredentialCache_get_DefaultCredentials_mFD45861A5151E546B6DF04F6D527E82214EA4143,
	SystemNetworkCredential__ctor_m6C0643A3B325903C6E20EEC9940A6646F4AE77B6,
	SystemNetworkCredential__cctor_mC2950C5826DFEFF2A593B0E00769DCAF75257AF3,
	EndPoint_get_AddressFamily_m44AC2CD5C6868280BEBABF4090D3AFCA2E254537,
	EndPoint_Serialize_mB045A92E21967EF34B1D5A32EF4E645CC1871BA3,
	EndPoint_Create_m09DA8D1A544D556AD0A1A08E3B0CBE9052615983,
	EndPoint__ctor_m4CD393A3D801B696473199A2D2F7EC80E8036F5D,
	NULL,
	IPHostEntry_get_HostName_m04050E3890C11BC5CB02BF48FBDD8AB4938FC390,
	IPHostEntry_set_HostName_m4218E75B9A5D143AF361FEC708ED40CD3829920A,
	IPHostEntry_set_Aliases_mCAB3DC9E9B474EA49C1A80E8010FF4932316B266,
	IPHostEntry_get_AddressList_m9D14D52EFB41C53C9C4A36D438E1333A99B7AA71,
	IPHostEntry_set_AddressList_mFC955717B52B0D1DC71059FC7B2ED6659E4729D8,
	IPHostEntry__ctor_mF2D2692AFFD3ED81CAF30F4CD42B7B0843952398,
	InternalException__ctor_m20A09E66E185FB1449C71227304AF40936C8011A,
	NclUtilities_IsAddressLocal_mA952662DF0A2D8BF1AFF9E74B1FF46DBCA90812F,
	NclUtilities_GetLocalHost_m7C77756A35C2F3CC268016902FA61CF88C5AD56F,
	NclUtilities_get_LocalAddresses_mC5E3B1D849F83965C3F09ECF963B3CB252A8F22F,
	NclUtilities_get_LocalAddressesLock_mDF3BAF033A2D11D70D5703B65302AB09525F7880,
	ValidationHelper_IsBlankString_mEE97DDAD061D54175C26B9E51E891C4F346E33B6,
	ValidationHelper_ValidateTcpPort_m9042E85AFB8EA2756E2940065C116104C008CCB2,
	ValidationHelper__cctor_m24C6635AA32CE592CED801B39532BB4B7445AFC1,
	ExceptionHelper_get_MethodNotImplementedException_mF8678EE4622F1BBEEC6CDD54293E4D1B40A6D251,
	ExceptionHelper_get_PropertyNotImplementedException_m5029CA867B083BAEE8D3D4577B1F571FEDDDE590,
	NetworkCredential__ctor_m6AACF996F73DA7CE81D33CD301DF98362E7D3EB2,
	NetworkCredential_set_UserName_m1C8F2119945861B5B7A512896A57F5E2C5537B2F,
	NetworkCredential_set_Password_mF0B964234CE60FF322135B014FFD8C28FEF5428B,
	NetworkCredential_set_Domain_mF7A8246CA8329E6A86A7B2B5FA8C972371F5AABA,
	ProtocolViolationException__ctor_m0999A59A95131FA8BB4D6685FBF4BC0B74E01E34,
	ProtocolViolationException__ctor_m2C2665583590BF7DB9B53A7AF126FEFDDE364BA5,
	ProtocolViolationException__ctor_mD9B5606196795D66E9A101AE92EE2A8D9B9A7440,
	ProtocolViolationException_System_Runtime_Serialization_ISerializable_GetObjectData_mA112E599073DE2CF2AC7F84E6CD354BFEBFA3170,
	ProtocolViolationException_GetObjectData_mF3C17B3E564B658959AF1A026E232D8E2D2CCEB9,
	SocketAddress_get_Family_mF66C07BE26931A763DF421310667F1C7EE82EA20,
	SocketAddress_get_Size_mBBA7758844EE6209A287989E0DBAE534C9A21A11,
	SocketAddress_get_Item_mFF97A6A1AF0D8AB36C662282C477B5DBAFD10FBD,
	SocketAddress_set_Item_m749FA727E65F8BE8E095BFBCD56091796CFF6B84,
	SocketAddress__ctor_mD56F67A8077F542F2DA0ABAF693EE192F5910ECB,
	SocketAddress__ctor_mE024ADACE4B62D1FF2969E8D59E3D6A4B567DC30,
	SocketAddress__ctor_m33DA845BE27401842BFABDF79738F8EFA60B12B6,
	SocketAddress_GetIPAddress_m9F349596DE4F881222A989059162AF5880FBAA0D,
	SocketAddress_GetIPEndPoint_m1B1532600216A9545A7CAC1EA49F23CBAC5C8C17,
	SocketAddress_Equals_m68D7D3DC11E1070E43492BA30FD9AE5AEA51C49D,
	SocketAddress_GetHashCode_mA5B524859742D73CF5870F6984360E15F09FA719,
	SocketAddress_ToString_m1C4AEB870E76472C9E3800224F2400A66DF8E46A,
	WebException__ctor_m1F229FB262E89CA90779094218A3066F532EB8CB,
	WebException__ctor_m8075F9F7345CECCBDB276E0B67DE820A9F7D03E9,
	WebException__ctor_m6C829021B5388956F84830FC249915324C1453A1,
	WebException__ctor_m937A4670EAF10FD9EFA1E0AEAAE27515293339FD,
	WebException__ctor_mA1609CA415DDD5C7C4AF37E9CD2CE77330C43384,
	WebException__ctor_m6D36D5DBE171207E973C5BDFC9A57BE2A1E5DA7C,
	WebException_System_Runtime_Serialization_ISerializable_GetObjectData_mAEABC5E4A1BAE8AD395EFC404CCEAA00345AD628,
	WebException_GetObjectData_mF735C851AF332C94DDDAB38C0C24AA1871DC2901,
	WebExceptionMapping_GetWebStatusString_mC6F3D28D6DCDD3E8674CBDA5CA684DF603DBCF69,
	WebExceptionMapping__cctor_m56C96AB4B5D49C51756576BE119AC1096CDD6C4C,
	WebHeaderCollection_NormalizeCommonHeaders_mE1D83B9D009EB876D33D1EB93CDC65F4F97B5B77,
	WebHeaderCollection_get_InnerCollection_m544FF08AD232BC5BDD8C5109DF6CD21C5ECAE2D0,
	WebHeaderCollection_AddInternal_m0EE002E2F815150CA5C43EC865F6D193FD4B7777,
	WebHeaderCollection_CheckBadChars_m1F7BCD4BF0DC01AF8C1378AD02862D9900B67D71,
	WebHeaderCollection_ContainsNonAsciiChars_mAE8D797265C12FBCAC784B15D734914273ED7AFD,
	WebHeaderCollection_ThrowOnRestrictedHeader_mFBE0E21167D9EB5485BD3DC18B505FEC9BF6CF51,
	WebHeaderCollection_Add_m3E612375F241E3383BA9EC01972C5A5FCD9384CF,
	WebHeaderCollection_ToString_m60D2996FFE881E3D8AAF700E761ECA34B0A24D86,
	WebHeaderCollection_GetAsString_m7E801872DAF279F8228F57FF2EA5336CD29F2E4F,
	WebHeaderCollection__ctor_m9C77B3E5FD0A3637283AE2B7084F5ED5D8CD7369,
	WebHeaderCollection__ctor_mE6EBDC6DA6CA46B83FD87FA51A93BACCA7FCDB27,
	WebHeaderCollection__ctor_m7FAAE403639DC42E7FFED6006B1D10BF5A1B4B5F,
	WebHeaderCollection_OnDeserialization_mDF73EE0F5BBF3AEE9C37273B79738A47530BECA4,
	WebHeaderCollection_GetObjectData_mA9D238943F3E6D4DF3A1156A0EA6729F5CA6F911,
	WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_mCB9569FEA54BEF369CF3BE5F56F36072D01D712D,
	WebHeaderCollection_Get_m7EB2DFD01DAC1EA2CC4CA7290459BF47B06CABED,
	WebHeaderCollection_GetEnumerator_m8A77AE9A2EFFD0F12E60A1DDF51BBBF2C9F5904B,
	WebHeaderCollection_get_Count_mFD0A0FD946CC97663996F70E67BF64851F3A4B26,
	WebHeaderCollection_Get_m99BCFF243942F451A075C334C2B67767B2332348,
	WebHeaderCollection_GetKey_m568730550CD89F6A9C02478A059923E3BEE324BD,
	WebHeaderCollection__cctor_m065B6EEB9A340079FA0815B12CF08010D7A5FD81,
	CaseInsensitiveAscii_GetHashCode_m6196C976C5E2C2BF948F9775AF827D80AA0707B2,
	CaseInsensitiveAscii_Compare_mDA30468D238E946A8CA36C5D0CBD9C92EBB67B0B,
	CaseInsensitiveAscii_FastGetHashCode_m8185162F020C571335BCFB444577F786E6BB1E06,
	CaseInsensitiveAscii_Equals_m2F02CC88964600A235BA5B084B83ADA6CEE2EE5F,
	CaseInsensitiveAscii__ctor_mD964E182EFE7238322DE8F7A2CA169657B6C4F4A,
	CaseInsensitiveAscii__cctor_m769922E617CDF4088DCC27EDD5C2F0042D2318A3,
	WebRequest_get_InternalSyncObject_mCCA701B3B01048CC2F26150D9E84D25CD194DFCF,
	WebRequest__ctor_m8A7037980E4A7E78EEF420F0C38A36C10DE3D642,
	WebRequest__ctor_mFCD491607BF422AED49CB0368CFE19F2E37BE98A,
	WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m84834AA790D887F6742649C7D4001AD00089102B,
	WebRequest_GetObjectData_m1809AB7BE2251220AB0F64110CFD815EF882D3E7,
	WebRequest_get_InternalDefaultWebProxy_m5FE1DF46DCB29A82C5AD3017F0D1BAD2A787A75C,
	WebRequest__cctor_mB67087F945455F2B1502CB55721CBF2ADB0A10F4,
	DesignerWebRequestCreate__ctor_mBFB58FED978B03A3C53AE292C3855EDED4222C88,
	WebResponse__ctor_mC11AFFC628F1E49E53431A3129ADC7CEA012306B,
	WebResponse__ctor_m845789E13F69DA82F94377A29D46186F2A43ADD1,
	WebResponse_System_Runtime_Serialization_ISerializable_GetObjectData_m7251108700FDB16B091C135DC5E76053623F4FE6,
	WebResponse_GetObjectData_mDE55FE69900B3F6311ED36EC53D1F775E2A634CB,
	WebResponse_Close_mDB1366AAB8237FC3D25C47B45FC7FA18055D7ADD,
	WebResponse_Dispose_mB68E6769F296DE95FF4BAF9A80AA9309EBB4AB4E,
	WebResponse_Dispose_mAACC90307104A5FBE9D417E8E40569C2501D5568,
	HeaderParser__ctor_m3E4BFAA908282AFA1EB023C1B84A03C222A68AAD,
	HeaderParser_Invoke_mCAC8132BC020AD64B0130E50BD0379A312245C8E,
	HeaderInfo__ctor_mB82B9D1D7C19C78B649E9107B0DB316A96120CB8,
	HeaderInfoTable_ParseSingleValue_m217B3A467422A5B17876254E9984F2C30E178F9A,
	HeaderInfoTable_ParseMultiValue_mCACE161A48EB4BC9DD4519BF3EC02CC83DD6C85C,
	HeaderInfoTable__cctor_mD8B50AECD3CECAAC7E93610FAE44324679088EDC,
	HeaderInfoTable_get_Item_mE388F21B37BFA30336BB82A1585C744E69A20021,
	HeaderInfoTable__ctor_mED0A17850B2F6AA3A3E6BFACF257E8640A7F3F50,
	NULL,
	LazyAsyncResult_get_CurrentThreadContext_mC86876C2BE5FFCFB3CCD0B34ACAD7272E72725FD,
	LazyAsyncResult_get_AsyncObject_m599FB00838F916DBE45F359D64D26717EA2DF231,
	LazyAsyncResult_ProtectedInvokeCallback_mAB31EFC1A9671BA58BB38F59F381EAFE9C564410,
	LazyAsyncResult_InvokeCallback_m4A1E1A15818F5ED6E6DBAEB48104A06529A11C9A,
	LazyAsyncResult_Complete_m6B165AD8AEBAB7129514C42C4A3277114E0683BC,
	LazyAsyncResult_WorkerThreadComplete_m50589CB1937E4BA7FF703482ECC24706F0E1A1BF,
	LazyAsyncResult_Cleanup_mDB648575075A8BDAE413E4FB90C35A880A238B02,
	ThreadContext__ctor_mBFFE2269F22D15B6570BD909390F40F213E9DBF1,
	NetRes_GetWebStatusString_m35A90AB3B319FDDFAC2A2C4AA534504806B8555B,
	TimerThread__cctor_mC25B3C34109193014AFAA8AE22DEB5A82AE730E4,
	TimerThread_CreateQueue_mEE7746808FCC711657A10D38DAC9FC01B93B68D8,
	TimerThread_StopTimerThread_m1E3A2BE926509E8E445BA5C00BA5B83C8283EDCE,
	TimerThread_OnDomainUnload_m789618CB693C29737319DBD4437199D0346FAF60,
	Queue__ctor_mD131B17E1C13BF9B3E8EBF5A909ACB274930B0A0,
	Timer__ctor_m74E9AEA5FB4F7DDDD8D9089D6F9C9EF7A3A96F3E,
	NULL,
	Timer_Dispose_m266A7BD332741D89F4AA204ECEA9BD4784AFC79E,
	Callback__ctor_mE36A85A258851A46EE477BBE1E87B5751C3CC9A9,
	Callback_Invoke_mE5F4E02D25CB58B34E93A55CC21418F00B55C1BE,
	TimerQueue__ctor_mCB1C60FF084C13E97E4FC8C4E56F6264649285D9,
	InfiniteTimerQueue__ctor_mC47879DB811308BFE6D5D4613791A81CCA215501,
	TimerNode__ctor_m259A5ECB2BA964982D8CB975EE945899CB14EA6A,
	TimerNode_get_Next_mB0E6D37D592DC92CBED33A26B0362F00959578D5,
	TimerNode_set_Next_m77F9833E3DA4B66D565A10F0409C15B6B7677AB0,
	TimerNode_get_Prev_m358430B0F3DD4E039E38962A04118F937E6F5E4D,
	TimerNode_set_Prev_m3A7D7FD5A4B654793FB48D0BAF7D8E0638BB1E47,
	TimerNode_Cancel_m6CAF1FDE8F05519F86383C25195AC922D5056489,
	FileWebRequest__ctor_m32AA08E0CE88EE4B4328D724C347D7D3F2B3E700,
	FileWebRequest__ctor_m7AF7635D835AAEA13DE9B5F7BF53869032B6E51F,
	FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m73E50CCB8DA4A87382EE2B23DBA184184C238C67,
	FileWebRequest_GetObjectData_mB0C155782CDEA4ED9B05D66308F6223AB0C4E389,
	FileWebRequest_get_Aborted_mDCF5639158777EE8240560751536AF4CFF1FB3BE,
	FileWebRequest_GetRequestStreamCallback_m62CCC32A81F8350AC6975EEEB79B2D61CC428C8C,
	FileWebRequest_GetResponseCallback_m21A70D5427620CCA22552BC8263CEF014B3AF414,
	FileWebRequest_UnblockReader_mC1E7C9618A64BC7785D882EB222D92A95244F8B3,
	FileWebRequest__cctor_m1034782E644D45E398ED29E1ADE0AA1454DFF5F9,
	FileWebRequestCreator__ctor_mDF264D86311EA60A56DA2B1FCE048E48F0BF6CAB,
	FileWebRequestCreator_Create_mB47FFBF665D3B3D20F250AE2396A3C5654637939,
	FileWebStream__ctor_m0F3CC822C4DEE7C71B3FA3FFDCF5B25EAEB2CEAE,
	FileWebStream__ctor_m9E118415C0F3AD53809A5836C1BD6DFC9913C60B,
	FileWebStream_Dispose_m1155DD4853E8E2ADF2EB7E0A940289F18E4F8EF8,
	FileWebStream_System_Net_ICloseEx_CloseEx_m68BBF0B85ACACDFB200E96968A6E31E8FCA757C4,
	FileWebStream_Read_mD35B2485DB5F8929EF92A5727FF64A28BF2A5DB0,
	FileWebStream_Write_m216E33F6ABF32F817381DEF9DB3B133BC0587468,
	FileWebStream_BeginRead_mAAB039B0A955363303488218A4CBB9845CAEB4CF,
	FileWebStream_EndRead_m5AEA2D6B64A3CA7E005D9F0929C9D829766F6538,
	FileWebStream_BeginWrite_m686EDD328F0ECED1C8C51E819A61E3BD7A44F5B1,
	FileWebStream_EndWrite_m5162ABF5383D0F4A52DF6996687868DE6AF9E486,
	FileWebStream_CheckError_m7C73A2B18D870AB5869B1F38F325C32E83305F6B,
	FileWebResponse__ctor_mDCECF4C7D821F8FCBA18B5FA8DF2AD303A029C30,
	FileWebResponse__ctor_m63E4E435D6A83A6DFCE4FB6560F97FD09C50D973,
	FileWebResponse_System_Runtime_Serialization_ISerializable_GetObjectData_m92E0F46AC675AA9E4EEB159D1B465E085CB4CEC7,
	FileWebResponse_GetObjectData_m8C157E17C06D23AE9B5E8B00FFDDB3F94209667A,
	FileWebResponse_Close_m03BA89FC13B905EC65468C4AFB8FC44A1AFF3F83,
	FileWebResponse_System_Net_ICloseEx_CloseEx_m9909306B65ECA8038FB0A1DE1D0E72DEC4EB3D39,
	NULL,
	NULL,
	WebProxy__ctor_m513721AC5813F35B2879182BF7558A2966DDBC34,
	WebProxy__ctor_m56D1C9B767710B435DFDCC58891651D915E9E96E,
	WebProxy_get_Credentials_m76BB7DE06FD84E20406661BF0D98959DC73B28D9,
	WebProxy_get_UseDefaultCredentials_mA938B24C5D5406B88C515CC7450B91FBF3BBA28E,
	WebProxy_set_UseDefaultCredentials_mF60535F31708D6DF7CECC2926EF1634C1333C002,
	WebProxy_GetProxy_mB7D36975267535555BCB1BF449098DB35D01BC8E,
	WebProxy_UpdateRegExList_m7B2EE98DAADC48CBDFA6ED2D491B4E4F00857F48,
	WebProxy_IsMatchInBypassList_m4D28B54328E1AFB40D9BE518536EE192CE423F50,
	WebProxy_IsLocal_mA55F6EF93DD6449B262157E0795507C82356F154,
	WebProxy_IsLocalInProxyHash_m20B75565C09C609A0A9C000640B562C1D067A99A,
	WebProxy_IsBypassed_m4919E244B263600121FECB9F693AECBDBA30AE38,
	WebProxy_IsBypassedManual_m89E6F6F73446B4663DEB837B77ED542A9D5A220B,
	WebProxy__ctor_mCD69F94F99FAE8B45E7983EDDD7C8C8FB82F03B6,
	WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m80D2CE0C8665B8D1E6CBCAD584496A87934DE312,
	WebProxy_GetObjectData_mB8AB2DF20CBEF664997055463E7C4639E43BF52B,
	WebProxy_get_ScriptEngine_m0B6FE570183127FAD115C72CBC6BFFF5020004DF,
	WebProxy_CreateDefaultProxy_mF54A2CCE4E97B5A463DD71DCEF0E7A2A9C1265F3,
	WebProxy__ctor_m0D1F873D2441832F41295266069C0887BBD43641,
	WebProxy_UnsafeUpdateFromRegistry_m450A4700AA84B7A0C467365724AB9926F6881167,
	WebProxy_GetProxyAuto_m72DBD46E29CD88F40BC0B5D375BB8D30018029BC,
	WebProxy_IsBypassedAuto_mF4D52FD050AC1CA1672AE503BD23B68794CFD60E,
	WebProxy_AreAllBypassed_mD0ED2AEA81FAB6B141A13433406C53A9C8F10F59,
	WebProxy_ProxyUri_mAC61DB0E0378F9FE1ED4CFB5ABB61331312FB85B,
	AutoWebProxyScriptEngine_GetProxies_m39AEDFB157EFDBE7CE80A569565EAFEA874AE381,
	AutoWebProxyScriptEngine_GetProxies_m0EC7B4AB41D5BDC4DCBDF2CAB12EAF6CAFFD56D1,
	SecureStringHelper_CreateSecureString_mEB7FEF36DD2FD2DE189C394068B70EA57F56C6D9,
	Logging_get_On_mE613A75366C51B02DEC967952C3C0DD852156B02,
	ServerCertValidationCallback__ctor_mF3AC814542EED45987465791FE7846DE2A8B5603,
	ServerCertValidationCallback_Callback_mF04B81117BD337FC77BDB94B8EAA8B8B99A4D72C,
	ServerCertValidationCallback_Invoke_mDDC4DA8F50134EF50257E51B53BE36732550C7E6,
	CallbackContext__ctor_mF5D13B557A13193C357723E572E78E52F108AEB7,
	Dns_GetHostByName_icall_m5FFF0440C4EA238DA14130BD1856BC8C730E6BE1,
	Dns_GetHostByAddr_icall_m3411CB4B97CE07A5BFE2D02B2A186738C76CFD22,
	Dns_GetHostName_icall_mB1522212F101A0B89516E5734443BBD361E76FA9,
	Dns_Error_11001_m23687C2F5E4CB975F023D3775A1E7DC5B61DB83C,
	Dns_hostent_to_IPHostEntry_mC5D77A6DE5F97D1A47AD86CAD4F69FF780A12159,
	Dns_GetHostByAddressFromString_mE3A65EDABDD9DBBA0879B21F4A545BA7613683A8,
	Dns_GetHostEntry_m01156278E5CDAE38B7E1B2EC617F505A4B836D02,
	Dns_GetHostEntry_m09C47D2D4329E24D56DCA5BDA6D4F28FEC45EC87,
	Dns_GetHostAddresses_m0592CB7DB3F5665C412BCBE8297F198748114F90,
	Dns_GetHostByName_m554E787DE7A847C2730E1BE8F74C56E9594C3D3F,
	Dns_GetHostName_mF728787FF8A38620054B934FD08E021460A7C14D,
	HttpRequestCreator__ctor_mA6D07F56561A4C93100584384CCDED5171C45F50,
	HttpRequestCreator_Create_m25D9085B7B59931A1A4DBA18DE828D8FA376A139,
	HttpWebRequest__cctor_m5B96D0A29F3F6BA4C78CC0DE32C81A78A75767EF,
	HttpWebRequest__ctor_m37C92DEFA8C4C0282FE5B11E065D65C25CF8B91F,
	HttpWebRequest__ctor_m94F348B8E4338E5D0404A8425F5F43B339B322BE,
	HttpWebRequest_ResetAuthorization_mC7AA3C31078EA81423351F8DAC62F68E9F15060E,
	HttpWebRequest_get_Address_mC0579CE0CED2FDCBF69FAF3232706F7994EAC20D,
	HttpWebRequest_set_ThrowOnError_mB232259B74EC15F9E119944A41934D673A7DE4E7,
	HttpWebRequest_get_Host_mE23F8F15207CEF1E64CF212EEA7643185207923C,
	HttpWebRequest_get_ServicePoint_m170B921D095437FC5B7FE5920F327F1AABF532D6,
	HttpWebRequest_get_ServicePointNoLock_m302BC1F3A189D46E652B79981E8D29095E5D5C61,
	HttpWebRequest_get_ServerCertValidationCallback_m8822E8093BBEE23482561CCB65D6AF3B7B1B464E,
	HttpWebRequest_GetServicePoint_m936C89220DFB9BE810A223E84E36E6D933510DB7,
	HttpWebRequest_FlattenException_mF0D9565F401231C7F7C6A973F829CCB37E34C3EA,
	HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_mDB54A2BCCE38E278C44D3A136F6988CEEA92F5FE,
	HttpWebRequest_GetObjectData_m6C3C1BB107DB72006CDF96892EF910400CFA9266,
	HttpWebRequest__ctor_m1AF72A90B1265EE52584D72B1B225DCC95E62722,
	AuthorizationState__ctor_m6DDA04B33B8A520B93FBDF8AC4BBA3B7CCC03CBE,
	AuthorizationState_ToString_m6426C6029B83D041B9F1B9871A4541F70A2EFC00,
	ServicePoint__ctor_m4A7FE526B6A4EF15F51919D425E51B1D67EFD1AE,
	ServicePoint_set_Scheduler_m7935567ACF1937D07DA99217D517C7946A876126,
	ServicePoint_set_Expect100Continue_mDDB9FC522313DDC57B1B348B711CF888516AA4CD,
	ServicePoint_set_UseNagleAlgorithm_mC025F081ED6C7014665EB20388747D12476D03C3,
	ServicePoint_set_SendContinue_m5480B6A8C6478BA620030302C861A842DF9F6AD3,
	ServicePoint_SetTcpKeepAlive_m225D363A70F7971133192A44745C8D284DE19084,
	ServicePoint_set_UsesProxy_m48157456CF1FF842A098916FE6F485CCA2FB9B62,
	ServicePoint_set_UseConnect_mD34B72F3B4F0532EB58DAB315A58143E5882A81F,
	ServicePoint_UpdateServerCertificate_mA5119A83F8A76B1098E4C0B7BEACED9F04288D10,
	ServicePointManager__cctor_m8067E1B281672361100384C9FD668FB793EEE44A,
	ServicePointManager_GetLegacyCertificatePolicy_mBD5DCFD553FEEE50F78A70BAB63F95ACF21A25FC,
	ServicePointManager_get_CheckCertificateRevocationList_mC0CDB007A91C59573B664FAB178B756F78561534,
	ServicePointManager_get_ServerCertValidationCallback_m9436D7CB2C84F88A2F878C700D5B3B34B85B911D,
	ServicePointManager_FindServicePoint_m8B83339FF47FF2FDB7671C114BD56B71743ED2F4,
	SPKey__ctor_m30BE1C73F21DEB53327E47A0EFDC05D39726E2FB,
	SPKey_get_UsesProxy_mD847C0A3FA96B142882A340E221D4C4AE6BE3E76,
	SPKey_GetHashCode_mEF88EF3AB042F1A3B7BE41171F53155177ED0B4F,
	SPKey_Equals_mDFDEB4BBCC2EE4F43F4F36CBA81A35DCFE1CA114,
	ServicePointScheduler_set_ServicePoint_mEA9C878A2C3B51F9C6B1D66AB82FC1D81B38289B,
	ServicePointScheduler__ctor_m31580DC08F3E2491616C5067921AD44899E58004,
	ConnectionGroup__ctor_m374EEC30B4DB339157E1FEFE8D9908F1602B5103,
	AsyncManualResetEvent_Set_m3D541CCC4B5CB8393A63D23B9E5F104C85258549,
	AsyncManualResetEvent__ctor_mC5C973C7ACD4C923325E8B98C3E4DF293C08C757,
	U3CU3Ec__cctor_mF229783494E9F1BE9EB2E4C7D11B94F0AE5FD9C5,
	U3CU3Ec__ctor_m42F5C86E7E31C11A87C17A9FBE7A56B703102A01,
	U3CU3Ec_U3CSetU3Eb__4_0_m7ABBA00DFE7F256F3DD9640FB9A9D68CF5988498,
	WebConnection_Reset_mAE0ED7B4E2139686F19310C905B9C065209E7747,
	WebConnection_Close_mB451C06EF8412B688EE8DCBD0B70951B0415D791,
	WebConnection_CloseSocket_m435216C8E7C8EC2B0F7DBFE95A2A4A9D3E7D7B74,
	WebConnection_Dispose_mAA356D7546A523F3FAC8EEC34502DA3EDFD797F2,
	WebConnection_Dispose_m619217D576EFF59E4939308F24595E8F498175F4,
	WebConnection_ResetNtlm_mBD715060318622998A51E76EEF7C4106C18779A3,
	IPGlobalProperties_GetIPGlobalProperties_m78B851B32A1D963CC616CCA6DC7116F5EAC15753,
	IPGlobalProperties_InternalGetIPGlobalProperties_m72EC7CD424FA4463DBF973ECB688CE857A01578E,
	NULL,
	IPGlobalProperties__ctor_mD47A72AA48932214CA1D4736A83CE60D87446DB8,
	IPGlobalPropertiesFactoryPal_Create_m08DE78C65028109D3F8B3F8C6C67DC3546FD9AC4,
	Win32IPGlobalProperties_get_DomainName_m606701D1FC345ED0DEE4776A2EFE5BB69E5AAE45,
	Win32IPGlobalProperties__ctor_m9B513B7EF6994299CD6F5043C32868D9521264EA,
	Win32IPGlobalPropertiesFactoryPal_Create_m6D31F76384538314B8844A60A44681AD1B38D5D5,
	Win32NetworkInterface_GetNetworkParams_mA2E02CD896547407FEF7BACC80FE5B3ECFD57A32,
	Win32NetworkInterface_get_FixedInfo_m90FC6BF37040C5B5548AB86E4532C16CED57F7A4,
	UnixIPGlobalPropertiesFactoryPal_Create_m3646A4471214BFFD7D35E54EDA955AA1E8D2D468,
	DefaultProxySectionInternal_GetDefaultProxy_UsingOldMonoCode_mE92DE08E220E7F8B610BF188DCD06BE990F8CFB3,
	DefaultProxySectionInternal_GetSystemWebProxy_m149FAD86815FA437C5D6FEF7ABCA97CD2C3C94D7,
	DefaultProxySectionInternal_get_ClassSyncObject_m6C8A102D494E67F287EA10DEACD3EA151C0D085D,
	DefaultProxySectionInternal_GetSection_mC347FAA709106553F94BA8097F9593383BCA22FC,
	DefaultProxySectionInternal_get_WebProxy_mF4F15C1A7C6088706CA2834D70E8CD52086AFBDA,
	DefaultProxySectionInternal__ctor_m9EDD9E9D9F38DCA0B5260A4F040F27CE1650D430,
	SettingsSectionInternal_get_Section_m2B7B6F6B8C13E9FAD46C76116CCDE663741ECF35,
	SettingsSectionInternal_get_Ipv6Enabled_m4DF56EE72066B70B389C21B2B4A30EAFC05D20F0,
	SettingsSectionInternal__ctor_mF034B00EA94462E6FA1F906C97BD7B59AA00F1C3,
	SettingsSectionInternal__cctor_m8BEE9C7B1B87EC7507FC1D686220BAF2B94087C2,
	RequestCachePolicy__ctor_mDE54BDF10DC2094F849F2E6A90770189FD664988,
	RequestCachePolicy_ToString_m7F0E4C10E284F30A563E035DBD56BF855E4E3A28,
	NetworkStream__ctor_mA44C3DB90F5B64F230CC2176DC7E722F4077A9DE,
	NetworkStream__ctor_mC30750A728A682C23581DC0ABEE5B7F1E4246AB5,
	NetworkStream_get_CanRead_mE61A13D0F12690A65200D3FD0947A0DC2442643E,
	NetworkStream_get_CanSeek_mAF6CE2068CC8DC3DDD922C6C2C187D286A3F5044,
	NetworkStream_get_CanWrite_mD3EBED79804ED2676F028892B84DC7F8F9FDAA6A,
	NetworkStream_get_Length_mA738206EC6CF6C8A07FEC82C3702A5F494569496,
	NetworkStream_get_Position_m6DAEE19231A91693081BE120ABCB5B031DC95E61,
	NetworkStream_set_Position_mACE85A7C1D600E5B6D18CF5FF32D859F4734010D,
	NetworkStream_Seek_mD5CE01FBB731FF52A66451C5F688837AC4ADD906,
	NetworkStream_Read_mB0EBC9488D9E3291AB4C6412F3AE70E8D6EEDBA7,
	NetworkStream_Read_m3D01BAC21011E126CED7B801A8E40CCEE363F267,
	NetworkStream_ReadByte_m6C87F0C53F84DE07AF3EAC2E9FC963336642DCF3,
	NetworkStream_Write_mFF7B408A0A6322FDEFB54BD0C759C3C6E6268AFE,
	NetworkStream_Write_m7EDA66BDCECB18E5642F9FE6EA9501A957A1AA8B,
	NetworkStream_WriteByte_m08B98BC1C6991DEE0FAE667899EDA2A2EE306E30,
	NetworkStream_Dispose_m23F2CD1DBD8BBDCC96B36F93E663F3D5FF20B7F3,
	NetworkStream_Finalize_mE8FF3552F90C61575174F96CB6EF435268BE792A,
	NetworkStream_BeginRead_mCC685D1B566FEC09C2AE76871AB01917D2446144,
	NetworkStream_EndRead_m53CDED13D3A6DD8D42F8533F2336D0EA6C752701,
	NetworkStream_BeginWrite_mE2F47607B52FC7FA99DD00337DB2AA0F5673E7C0,
	NetworkStream_EndWrite_m843683738E14624DEBEF963D2480C8F3DB5EC848,
	NetworkStream_ReadAsync_mB629CC680C16D2F2C45CF197D90D30DBD6050518,
	NetworkStream_ReadAsync_mD7A23469A79B0E7C89FF03F5566997F89D839E0B,
	NetworkStream_WriteAsync_m1829F2663EE9DA443488E4031D103DE4E28F2C73,
	NetworkStream_WriteAsync_mF9FEB5879FAB94387395E59D435B94095CE9D59D,
	NetworkStream_Flush_m8DAEB9385D9E7B5531733900D507531E0B628F9B,
	NetworkStream_SetLength_m6A2E5648D163C03687A1D3CCCD5C270C5D9FB68F,
	Socket_ReceiveAsync_m04BFC87C7857569298C3C39387F24556559BAC36,
	Socket_ReceiveAsyncApm_mD6A803998C4FF807388FA600EAD6146A601E1709,
	Socket_SendAsyncForNetworkStream_m8A5FA8416ED841852D05A8232320F2498A7EABAD,
	Socket_SendAsyncApm_m1B2DB2B58903CCB8577C99C132896C3F9E7B0724,
	Socket_CompleteAccept_m1318F59CE726ED625516D2FE6769493F4D4F9C3D,
	Socket_CompleteSendReceive_m56B142FD1AA39E5AFE655A9AAB5B531AA9CDF14C,
	Socket_GetException_mB0C16F7E32740B9EA9E34144BBE98B5EC002DA06,
	Socket_ReturnSocketAsyncEventArgs_mF4A7BE8F1238309B0DA23245D0571CD0DE913913,
	Socket_ReturnSocketAsyncEventArgs_m6A467DCD60E641529A89C3521762C1C9A1482F9A,
	Socket__ctor_m35F1F4B4872E251867DA16460F06E903A30E4595,
	Socket_get_OSSupportsIPv4_m779A662683A5FBD8D7B4D1E5E940B04C6AFA0120,
	Socket_get_OSSupportsIPv6_m45BC8FD78EDFCB853CA509A8DDD23EF42203B6D4,
	Socket_get_Handle_mBF42AC5E9403C23BE532F17EE337BF9F150A0256,
	Socket_get_AddressFamily_m42C390D31345314080EC35356ACFBBFF7E1123E5,
	Socket_get_SocketType_mAFBEF108992F069AF0B8AD26D47EBBEADB8D2661,
	Socket_get_ProtocolType_m364A0E823BF3EA78DAFC1ED3A6B39658FB6A23BB,
	Socket_set_ExclusiveAddressUse_m3F9A655F123086A025AD1736933B0754A5A6DF7F,
	Socket_get_ReceiveBufferSize_m863A6D0F34BA3B8CD450346B17842AB8C3887A75,
	Socket_set_ReceiveBufferSize_mFCBD973C71C5E4883D3E3C6E822652373ED9CCD6,
	Socket_get_SendBufferSize_mC9F6F5099083DEADBFAC760012C883F823BDD706,
	Socket_set_SendBufferSize_mBFC47E7A4581A220FBFE8B113CE828EFD7F4EF33,
	Socket_set_DontFragment_mCF6F7D2735A7FD4E327617826C62ED015001DFF6,
	Socket_get_DualMode_m8B45BA120D7A03DB5EF8C3CF9483C829E19CEFCA,
	Socket_set_DualMode_m08152574129C01AC8941AAB43752C3CD1525C193,
	Socket_get_IsDualMode_m1CB3E1554CD046481E3408B20D887C7EC8470CFA,
	Socket_CanTryAddressFamily_m11E16D00AD0F3BBA149B4E313D67CC12E2735EDB,
	Socket_Send_m289DAB977DFA1BBE87E545BC7452464B0C38812E,
	Socket_Send_m3174031DF8ADFDAF158BEB0D8AC5CBB445F8D21C,
	Socket_Send_mF846BE4031BE538814BC497FFDC8BB02AC9F5A33,
	Socket_Receive_m29FED7D6FE4F95DD3B71CE6425AE19A3781FADBE,
	Socket_Receive_mF4189DF1BB210034B41CC593C0FEC2F21D8DFF47,
	Socket_ReceiveFrom_m0E841C82B5F0DB9CAF3A7C0E72E64C3C22A1A09D,
	Socket_SetIPProtectionLevel_mC243CFB942D4DA0F2A148A0DAD3B5559DB005CDB,
	Socket_BeginSend_m36452399B2D44A215795BFB0F1E06AEFD451E7C3,
	Socket_EndSend_mC636AB62DC23FA2E8BDB612F5CDB203E6CAE4D17,
	Socket_BeginReceive_mCCBDCDF0EE9A2AD7345F93A3931F2C426AFA4738,
	Socket_EndReceive_mD86DB01FE084056F0DA43DBB82B6B491CA8F5364,
	Socket_get_InternalSyncObject_m11CB9CA23D0CDFF801C26121CB479BA162D19ACD,
	Socket_get_CleanedUp_m8F6EDAE37C56CBC282D3EAF9D93B5726EEC85988,
	Socket_InitializeSockets_mF69CCC3B14A53FC7CD95F6E87FC726812ADB3AE2,
	Socket_Dispose_mED966D36CA8914F199BCE9497FFC5261AC99C1AC,
	Socket_Finalize_m1350F4F84C06E9DF703C32E47B1579C84A22B742,
	Socket_InternalShutdown_mD0EF08D54151D53369D32CD61C8518454AAAEFE8,
	Socket__ctor_m91002BA76E3B56D33CDEB6D30EFE923F92822CE4,
	Socket_SocketDefaults_mEEA679EDCFED63435F630A133916BD5F97BF1FFB,
	Socket_Socket_icall_m835339B54AFB8EC75F01DE329512C2DA3FF9A99E,
	Socket_set_EnableBroadcast_m8C25D9941F2D9BD40277F8D3395400993F4CF1CF,
	Socket_get_IsBound_m6CA0A4232291A7A0B99FCE2E5662A576D31A0A9D,
	Socket_set_MulticastLoopback_m58B314C4A975622C94B7AE7AD29BB7333B2FF38B,
	Socket_get_Blocking_mE3DB6D71A54F6376624DDA9522C783B914860ED3,
	Socket_set_Blocking_mAE329D3DA788B19FDDC6B773F5B0A1C31FBE9A7B,
	Socket_Blocking_internal_mCD8DDA2B42FCD3694E0DF5977ECDCFB080CA54BA,
	Socket_Blocking_icall_m822EDC3A6E0C02C7351B93395BFC0D6103997323,
	Socket_get_Connected_m6E61ADEB0124FB8D46AC1EDFDC268D3C9F7869C5,
	Socket_set_NoDelay_m1F7ACCEF219BB8745AB17F29CEA618088E0B2405,
	Socket_get_RemoteEndPoint_m6A626481D55A640C7654B91BFDE00199C2C95479,
	Socket_RemoteEndPoint_internal_mB9463EBBCBDCFD1C6BE384E07C972F3BB6D57FC8,
	Socket_RemoteEndPoint_icall_mCD9AE17D25710602FF67C6BC45BA1BB773B0A230,
	Socket_Poll_m65D8E383FBE5A1D4A115942182620422B6646B98,
	Socket_Poll_internal_mA9D6DA0F9323BE621CD3ACBEBEA80641F55AAA11,
	Socket_Poll_icall_m4335903786D9CA9197FF532517CE5BD3BB4D3951,
	Socket_Accept_m11B031FF408E1899034B993B352178D9D6B37C1A,
	Socket_Accept_mAF83623B54FEA675F0F16C29EACE1F24DE6C02FF,
	Socket_EndAccept_m6A7BE15D82A8C8DCA5AD786CC97E0A12FF354D09,
	Socket_EndAccept_m96B08273E42578EAB0A297A707A1F15956F69FB0,
	Socket_Accept_internal_m993249D44B5193CB8B5CFFDDEDBCE4D4DFBB9D6A,
	Socket_Accept_icall_m7EF3E68DB5166B5A2F185C50A1DDD23DDF62B851,
	Socket_Bind_m137559EBA78A72ED4ADF8B56F5C535CE638165AA,
	Socket_Bind_internal_m85DF18886A077D271720DCF0ACDA05AACED2B7C8,
	Socket_Bind_icall_m2FC33FF6853AE0E221EF889F9EB212D461A3D1B8,
	Socket_Listen_m734803DAC514E64828168AF9212D6A9EC63B32EC,
	Socket_Listen_internal_m2802EDACE64C106C3E87CFE335BE2507486B6DD4,
	Socket_Listen_icall_m4590ECBAFC507156C673C4D3FA6EDD53DD8F625E,
	Socket_Connect_mEE1E8320C097486AFA5432284C1B7FFB0DDC35D6,
	Socket_Connect_m874A510F986F4D91446B2F53688D0BCFFE0A6BE1,
	Socket_BeginMConnect_mACF037091CD1CDEBF28695402C2CE7B8E230A6AE,
	Socket_BeginSConnect_m3C0B6709149C420AEAED6A3F124AD2044954F4C6,
	Socket_EndConnect_m10F5B4F43A68F96891DB3AEF493AB160CD8104D6,
	Socket_Connect_internal_mB28CEC91E12F9B621FF2B074CACC575629E33746,
	Socket_Connect_icall_m32BE8ABC1B8535BC195F0BEC382408DD0D3B4E26,
	Socket_Disconnect_mFECF870A7A5604A9AFD79DBB6354F1697A96C422,
	Socket_EndDisconnect_mCD874CA31D96229FBF311AB941DFB1FAE4897BE8,
	Socket_Disconnect_internal_m78AD76E91C83AE6CE27D4FC952FD1D28EA33FE91,
	Socket_Disconnect_icall_m2BE84F38BC9AB58C78B466C044968413A8DB4289,
	Socket_Receive_mF387A0975239872402A6118D3F196B020060021C,
	Socket_Receive_m42AB6119254FA391A818CADF23C60441ECA29152,
	Socket_Receive_m51DFAB2284E194E286E5B086F874EFDA80A7C687,
	Socket_Receive_m08FAD3EAB5697251B5E6258AFB8C5E2F032BE1E6,
	Socket_Send_m4A03A1017A508D72593092EBB9DC6DC713C24F30,
	Socket_ReceiveAsync_m158A08D073322BE419CDF11B449F07191F7576CA,
	Socket_BeginReceive_m6CE71DB94ED6C33A5B0921F13530283D68979B54,
	Socket_EndReceive_m080D7964C40358B101AA6AF0B55635B3CBC9DFB3,
	Socket_Receive_internal_mD1316BB323E539121369401B592F3B054551E90A,
	Socket_Receive_array_icall_m1447B2BB5E3E980D3BA40BBD910C69AF33ED22AF,
	Socket_Receive_internal_mFFD3DE5D5B14A0730765CE470002536254B94789,
	Socket_Receive_icall_m970B475156F96287ED2C1C919FA6AD982816A85E,
	Socket_ReceiveFrom_m780282028E62CDD6021E3B8917656AC820878E6E,
	Socket_ReceiveFrom_mFCCD977ED8761DBBF13EC14BEFC4615662CE501C,
	Socket_ReceiveFrom_m6C9B4FF70B221818FD01FBE94E5635B169686B70,
	Socket_BeginReceiveFrom_mF3DA6F22C3107DB734B011D045D11AA976B6C84C,
	Socket_EndReceiveFrom_mAE3C7B82193AD982F47EE74B8C75CB52E7DB15BA,
	Socket_EndReceiveFrom_internal_m460268E3D25241DA2EFCBBCA8B9A553AD07C6590,
	Socket_ReceiveFrom_internal_m5C01BB7E993A7899973D0FC82A6E2D86F8F813AE,
	Socket_ReceiveFrom_icall_m583CD09AB1E40E32DCA9C7984C58B5CB866AB51E,
	Socket_Send_mC5F7C598421E94D9098B7A65073193E7E574A1C7,
	Socket_Send_m6414CA18C059F7262E069F92A17A2B31BB83E2F7,
	Socket_SendAsync_m0492E6DD02BBA2878FC8BAD3770F619B57370A26,
	Socket_BeginSend_m0BBAE4C48B338F9F6EBFD4F0E6FE236FE4BDE64C,
	Socket_BeginSendCallback_m7BAA6F54C7D4AEAF6BC6B2552D23C3464C0F787D,
	Socket_EndSend_m78EE767A837539CAFEA99FA8A2EB0EF24444123E,
	Socket_Send_internal_mAED1C3A20DB7EB11A59927CDD3ABC5579D0C43BB,
	Socket_Send_array_icall_mFDA9B09614B64B5143D8FF0BCE40751A6D7B956D,
	Socket_Send_internal_mAEE2FA94300B035241FCE5C276EAA4B956FA89B2,
	Socket_Send_icall_m5FFD8AFD5D12879C21F92A2C56D5DC475F69716A,
	Socket_SendTo_m07A6D82F7ABD61B6B9C87931035FCF793AA3D6F6,
	Socket_SendTo_m06ACC9DF41CAF14666D8CC00719FCCD48DAE3153,
	Socket_BeginSendTo_m0FA2AFB253E22B89BA454BEEB59B403E6DAD134C,
	Socket_BeginSendToCallback_m304F7045084969E1B54635CFBD812CCA9CADB697,
	Socket_EndSendTo_mC35D110ED10CFAFEF443A1AEFCDEA438DE0101EC,
	Socket_SendTo_internal_m5820CA4730A4ECB2613FA6D9303D74FB668EDC1B,
	Socket_SendTo_icall_m44EC5697BA880F9342179D257EB34600ACA44FB6,
	Socket_GetSocketOption_m39C453F9FA4D1EC664C660851CED73271B1162A2,
	Socket_GetSocketOption_obj_internal_m5C6F74BA7CBC5A7AF3FED034A1557A2CD65804EA,
	Socket_GetSocketOption_obj_icall_m95FC9B4255A7445D549B200032C94E74699CF1FD,
	Socket_SetSocketOption_mE47F5DEEA190E45317AEEE6F1506940CB8E943A1,
	Socket_SetSocketOption_m19085C1856DE21260294680B7725610D71D66A58,
	Socket_SetSocketOption_internal_mF5D974153131AD9CA8713FA0F0549DE238C88CD5,
	Socket_SetSocketOption_icall_m7F1AF1F4E424BB5306FBF5E0DACF35BD9A2650A4,
	Socket_Close_m5EBF3D8BE2C42EF8037BC9372CE7760B1717EEE4,
	Socket_Close_mE6673231EFBF35BBF5228BC5C7A2BA08DAD5237F,
	Socket_Close_icall_mBF92507692212E88F568DED543121A71DC710701,
	Socket_Shutdown_internal_mEF0F7AAEE1A7ECB909B871A31ABC8A44D08697EC,
	Socket_Shutdown_icall_m3AD0F420C6DB7C30FBC8741BEA5D1F0D3ED7F14D,
	Socket_Dispose_mD208C78BC53159B6E12EEAD70784ED7F19B38724,
	Socket_Linger_m24D121E76C38D9E302EF059712EE0C90B5312FBF,
	Socket_ThrowIfDisposedAndClosed_m93567578970C10A4E56C7DF84D7D18A1E5AB8CE5,
	Socket_ThrowIfBufferNull_m80732B6CF5F7BFA314905627BF1B9D28D4E2A41F,
	Socket_ThrowIfBufferOutOfRange_m22646D99174E99B57B816E260C93D959CE4FB143,
	Socket_ThrowIfUdp_m006AEC31161FCD9D704DBC20A4C8896B719ED6FA,
	Socket_ValidateEndIAsyncResult_m3B62AE8C5CB328C37CE3FB3264B5A0299E1DD3A3,
	Socket_QueueIOSelectorJob_m7342EB18C3E806FFE6C87FC356C89E23064A7FB4,
	Socket_InitSocketAsyncEventArgs_m32B714DC68442471D169D0EF1CF36138694847C3,
	Socket_SocketOperationToSocketAsyncOperation_mA3A0B1DB1F7A834F100915D3507DFFF381A5B6F4,
	Socket_RemapIPEndPoint_m8B03D3304FF55B1B423F96A2917F34732B258D73,
	Socket_cancel_blocking_socket_operation_mF7D69972B1CC9D769A3E5BF9946125C5CC661172,
	Socket_get_FamilyHint_m133ABCFB7BF5184CFA0AA12E4BC031D9598CBAC8,
	Socket_IsProtocolSupported_internal_m4C4E9B469DECA875DCDE5AFCA5B522F09C3B44EA,
	Socket_IsProtocolSupported_m063B4A00EC7B0FF7FB37507F33C22BFBD0177F18,
	Socket__cctor_mD7400247DF81848730C006F1CDFE51353C2F3F16,
	CachedEventArgs__ctor_mB9F52BB006EBBAD11F9C6E13F14C47971A1F36C9,
	NULL,
	NULL,
	Int32TaskSocketAsyncEventArgs__ctor_m2D28C3F386AE7BDAC321D2E13CA613FCCC9F7D60,
	AwaitableSocketAsyncEventArgs__ctor_m57166E81DE7564E32B7FE2512A7C590A85F75F5C,
	AwaitableSocketAsyncEventArgs_get_WrapExceptionsInIOExceptions_m417FC84C2DCA30FFE3EF2DBC55646E410BB864A9,
	AwaitableSocketAsyncEventArgs_set_WrapExceptionsInIOExceptions_m254606698A8C97790E95BEFE9B11A9AE528227B2,
	AwaitableSocketAsyncEventArgs_Reserve_m2A356885123F19F592BF5610380A73D7A315A8A7,
	AwaitableSocketAsyncEventArgs_Release_m968CB82406E80CFDF9F3A77F23B7F1B1A44E7B3D,
	AwaitableSocketAsyncEventArgs_OnCompleted_mC86BEBA0820E4B4BA4482692F5D3CD57960086B9,
	AwaitableSocketAsyncEventArgs_ReceiveAsync_mC2BA1C5E24CB4AE2F2E1B69002DBD35FD10EF2BC,
	AwaitableSocketAsyncEventArgs_SendAsyncForNetworkStream_m47C17FAF97A11A8F1CBCBC0DBFB193EF88960D2B,
	AwaitableSocketAsyncEventArgs_GetStatus_mC7723671714D0561D4240F0422721FE850287C39,
	AwaitableSocketAsyncEventArgs_OnCompleted_m4CEDC0CE4C641E978968BDAE34A6D9F3AD2C5E06,
	AwaitableSocketAsyncEventArgs_InvokeContinuation_m77BA72041547ECDF9888FED13F15F35A7390BB17,
	AwaitableSocketAsyncEventArgs_GetResult_mD1285F6C0E77C62371227AEA7461BE494692845E,
	AwaitableSocketAsyncEventArgs_System_Threading_Tasks_Sources_IValueTaskSource_GetResult_m275B7D8B48D925808E1B29EC32B07A2648F63BA1,
	AwaitableSocketAsyncEventArgs_ThrowIncorrectTokenException_mCF33DC63E9EC887CE52A959DE5760ECE9483D407,
	AwaitableSocketAsyncEventArgs_ThrowMultipleContinuationsException_m23F43A76D940E78011D7B1AAE6170F5532E9BD0E,
	AwaitableSocketAsyncEventArgs_ThrowException_m77AE96856D358D880F32E1FA6323F9F161E214E4,
	AwaitableSocketAsyncEventArgs_CreateException_mCF6E0A048155F41AA05A8847FA0155FCAB47D6F5,
	AwaitableSocketAsyncEventArgs__cctor_m29C9980065E4ADD89B108387999A94A207CA572B,
	U3CU3Ec__cctor_mC972E27AEB8CFE1FE7E12EA1E19A9B813715F1B1,
	U3CU3Ec__ctor_mE17ED0A084782AF1BB595DEC18E0560D76AEBA70,
	U3CU3Ec_U3COnCompletedU3Eb__14_0_m5314AD7E71DB2D2A543185D11CB8246091EF8257,
	U3CU3Ec_U3CInvokeContinuationU3Eb__20_0_m3439C73F5A9F402774FC521870BB0F0934E3DDC5,
	U3CU3Ec_U3C_cctorU3Eb__27_0_m7B5E46EB80E344A6ECAD9C772E4E448AF0F5FF90,
	U3CU3Ec_U3C_cctorU3Eb__27_1_mFB29CBF67405E81046B23B3025E43D9714CD80F9,
	U3CU3Ec__cctor_m038CF32946815BC1EC22307A7B49E3CA24C281AF,
	U3CU3Ec__ctor_mBED1A732D3342D1A48A4088765AA6159F66308A3,
	U3CU3Ec_U3CReceiveAsyncU3Eb__14_0_mC59A3A03878FD1D393D4345962F6D3E6984EE609,
	U3CU3Ec_U3CReceiveAsyncU3Eb__14_1_m06418F2989013CBF63EA4DE96C8E82F79AA8580D,
	U3CU3Ec_U3CReceiveAsyncApmU3Eb__15_0_m6059641EBB33F2508CD3F8AD1768816383497446,
	U3CU3Ec_U3CReceiveAsyncApmU3Eb__15_1_mD81706B8EDF964E3FF8CF9B7717AF288C9131DEE,
	U3CU3Ec_U3CSendAsyncForNetworkStreamU3Eb__22_0_m42A7692C372A4888E5D2070BFD402F5DB136F51D,
	U3CU3Ec_U3CSendAsyncForNetworkStreamU3Eb__22_1_mB8B5FE244D7173851FD875760A7D1940A8B60B79,
	U3CU3Ec_U3CSendAsyncApmU3Eb__23_0_m3FCE73D18C4ED344F469F82A9C51E477CA1EA0D6,
	U3CU3Ec_U3CSendAsyncApmU3Eb__23_1_mF20106872C29B9D38FA321A7F41C1718EBC14B9C,
	U3CU3Ec_U3CSendAsyncU3Eb__295_0_mE1575C8E05206B1D48DC45BEE81D0516DECDF2F9,
	U3CU3Ec_U3CBeginSendU3Eb__297_0_mBB4DD495ABB09A05BC74328D8624152A6C8AF91F,
	U3CU3Ec_U3CBeginSendToU3Eb__310_0_m4FF95FAECEDAC6A1A4C5DA5C1E04B1F0DC88D855,
	U3CU3Ec_U3C_cctorU3Eb__367_0_m374195043B44F151293B98C58D3C36A426D54285,
	U3CU3Ec_U3C_cctorU3Eb__367_1_mB75869C75ACBA247A3EF6650633CC7101DD531BC,
	U3CU3Ec_U3C_cctorU3Eb__367_2_m02D7CD9BE2BFEC708C85C3B8A9D67BAA4DD92FD8,
	U3CU3Ec_U3C_cctorU3Eb__367_3_m02CCCD8CCE3176AB8A9DAA6BAA575BDE5F2D57D6,
	U3CU3Ec_U3C_cctorU3Eb__367_4_m802B9387C24EC09B0909D35D8AE9962F52B6C188,
	U3CU3Ec_U3C_cctorU3Eb__367_5_m695C3A5FBD0C51D71C6D5E1D1699AC01A91BF528,
	U3CU3Ec_U3C_cctorU3Eb__367_6_mFB25C6CBDC7BA018A88FCECD1FB04117D137F8F6,
	U3CU3Ec_U3C_cctorU3Eb__367_7_m624ACE294D17E142E9B57F7D3DB735CEDEBEFE7C,
	U3CU3Ec_U3C_cctorU3Eb__367_8_m71E6A84E783DE7F080A6652C4151F897EA1E32C0,
	U3CU3Ec_U3C_cctorU3Eb__367_9_mF99985871CC04F99411C3EE2D1354FF13B0EEDB2,
	U3CU3Ec_U3C_cctorU3Eb__367_10_m6C7764F04456B7F5F6AF4C89020F9179B76C8060,
	U3CU3Ec_U3C_cctorU3Eb__367_11_mF302618690648FDA9BDAEAD4AA9231336E68386A,
	U3CU3Ec_U3C_cctorU3Eb__367_12_m98E24FD18071B79AC0F0557EC4F838AC497892C0,
	U3CU3Ec_U3C_cctorU3Eb__367_13_m5A14C5CE0B6C00EBC98A0D10C2F670E9F8E5FDC7,
	U3CU3Ec_U3C_cctorU3Eb__367_14_m2502D94E58E7B239017969E7BDD16DAA9473DB87,
	U3CU3Ec_U3C_cctorU3Eb__367_15_mD30BB7B50BADC326CF0BCD23DCDF15B6791C24CB,
	U3CU3Ec_U3C_cctorU3Eb__367_16_m52CF050F4CEBCFEDD8DF4BB9E89D14D69BC554E2,
	U3CU3Ec_U3C_cctorU3Eb__367_17_m402C11E460133671C680B46BAE5EF0108245E12B,
	U3CU3Ec__DisplayClass298_0__ctor_mC8FFE6C7BDBE2A999CF8792C81BD55DF56BF5004,
	U3CU3Ec__DisplayClass298_0_U3CBeginSendCallbackU3Eb__0_m2A2487EF9F99DDE2CA0FD582FC353D04BA4FC843,
	U3CU3Ec__DisplayClass311_0__ctor_m0CC0E00F55AC8EAF6B987A3E1DEE7BB3BF228BE2,
	U3CU3Ec__DisplayClass311_0_U3CBeginSendToCallbackU3Eb__0_mC75A3CEA8BA7D7EF3F6CE003D66DA9AF42B7C1B2,
	U3CU3Ec__DisplayClass355_0__ctor_mA97F35DC1143434E4ED8C3A6BFDD83162EA26BDE,
	U3CU3Ec__DisplayClass355_0_U3CQueueIOSelectorJobU3Eb__0_m7B1B6117A026B2295671986FBAFF0F01C11F94AB,
	SocketException_WSAGetLastError_icall_m44B3CDD4C14F19AD07549E0FFF2CB9497B5897BD,
	SocketException__ctor_mDDA59569C2C05AB013AF9670BD6EC070086239AB,
	SocketException__ctor_m0F0444CE9F1E2BF7AF1D0A89DC29FD973AE743AC,
	SocketException__ctor_mC09AA11941DA13359416179634B151A3FE2B9A64,
	SocketException__ctor_m5E9DDE94C8009DE11B8CE2EF1611A5CE8A900AC3,
	SocketException__ctor_m830447309D5813D7FDE050DB3A075BDCE787B046,
	SocketException_get_Message_mA34986089059D346C42DB47FB8BD331856BE0492,
	SocketException_get_SocketErrorCode_m84FB2D308F046A24A1355975F3BF689C988224C6,
	IPPacketInformation_Equals_mF2E8CBE60B68F20394E51286666D25B54F8EC2B7,
	IPPacketInformation_GetHashCode_mF1429DDA203ED791CB45EDB87A9006E58BB12A81,
	LingerOption__ctor_m0BECD7DEA12AC52917D065B27818677F4DE7CEBC,
	LingerOption_set_Enabled_mEDF2FED046EA58F9C23BEF3BF1184C3A890419DB,
	LingerOption_set_LingerTime_mDC6B06F96C04C5A69E19912EBB37DC5BDE5A6DFB,
	TcpClient__ctor_m709FAC4D5B9DDB279F9B97D90D3164B92E1F2C87,
	TcpClient__ctor_m94DC92A387808F584010C708C071197B52B48B55,
	TcpClient__ctor_m8FDA6AE13D95E071F7952F7DB34C729ABCF7AEE8,
	TcpClient_get_Client_m38B7DCD73EB42C6BBE6340B5E4C0A8432C1140C0,
	TcpClient_set_Client_m145E6A35F13B52AF10DE077FD19DE79A622E5F5C,
	TcpClient_get_Connected_mAE5B631778361C28C783E417C09F87BA102787B5,
	TcpClient_Connect_mED87D733E342BEAF8DCB947F612FD25C505753BB,
	TcpClient_Connect_m45A6127922B8ECB5CFBC879DE6A3BAB7BEC8ABBE,
	TcpClient_GetStream_mDD54336B17D1267BD593E0A1EB9EDF3E9506AEBA,
	TcpClient_Close_m03E0ED4E4BA87B3F1ED17585AB1327ED76F5FE89,
	TcpClient_Dispose_mE2C0688BB1E1E9E25CC9E7CA016AE27547DAEC82,
	TcpClient_Dispose_m533DFCEE37B4299BBB5FACD9A98F8181EC2386D0,
	TcpClient_Finalize_mE6E1D5B6566ABF5AFEF53EF025298EE6F33E7AE2,
	TcpClient_set_ReceiveTimeout_mF205FBAD90301B326420B66DDA8E7E6AD518B27C,
	TcpClient_set_SendTimeout_m0BF2D0CB3C24116A88097885CDB64971D5AF69CB,
	TcpClient_set_NoDelay_mEE5ACC3E8272661337761B25D06A90670012AC85,
	TcpClient_initialize_mE26B7664FC1F1DB4D963065A7E47848798697114,
	TcpListener__ctor_m6EDEF45E8F8F2872F3828E801806D9FEC3FF003B,
	TcpListener_Create_m7A41A8E5A3DF1791B461D21E5E535D4B5D1678C8,
	TcpListener_get_Server_m949D3B055C39A6716571782E1474BC0CD1F72D26,
	TcpListener_Start_m919D559B138B311CFFBBE4BF66E326EABD8F8712,
	TcpListener_Start_mB9AB1161D1570AA1D68F020820D00B108E782C09,
	TcpListener_Stop_mBF4B354EB52138AC9A0184F186894EDBAE3BA5FD,
	TcpListener_AcceptTcpClient_mD7DFF1228EAB3F886B5BBC6175A0856C84F2B419,
	UdpClient__ctor_m0F676D3E94A3C7B908DE1FBDF08FEB36894F100E,
	UdpClient__ctor_mBFC4DFC10BDE68B705E8B56B6ABDA631CBA751F9,
	UdpClient__ctor_m6DCF61B7A404114E51B86974B1B4E028F45C3442,
	UdpClient__ctor_mCA910885972A14289F4CF94ACB4040ECCC935D7E,
	UdpClient_get_Client_m7338E337D9A97F9050FCAD3A29760AB5CA7AFD9A,
	UdpClient_set_Client_m539B7DB562DA1B5376A2ACB65C1943E55423C46F,
	UdpClient_set_MulticastLoopback_mA183AA4187FFE98B9F7853D7B3BE837E082D2300,
	UdpClient_set_EnableBroadcast_m688F1E60EF735B1882A69C9AB606225DC2049188,
	UdpClient_set_ExclusiveAddressUse_m48228EA37AC52DFDF23DDFB06BA5626F7679A919,
	UdpClient_Close_m5441019910E37A7F7B8610A46769004E614FF5B6,
	UdpClient_FreeResources_m5E0D30AF8B963F5277A5116C0E1DCAD5C48D5469,
	UdpClient_Dispose_m12D6B00FE05B89B2049F79E847BCA2D0490DD63E,
	UdpClient_Dispose_m8A81C06984179D26D3D9D08B566B8202606CA595,
	UdpClient_Connect_m44A8225451A8B7DB449D6BB31DCD0C3BD758022D,
	UdpClient_CheckForBroadcast_mB7F54E4B304EF71FF92510F82D0CDFEB35C6D9DC,
	UdpClient_IsBroadcast_mF679BE752DA8794D7E7F994D421575AE060CA538,
	UdpClient_Send_m4DCD3993D7ED39C174F8FC8F653DD30F1BDF47B9,
	UdpClient_Send_mF0E2DCA4FF68290820E807BB5794E938E46280B4,
	UdpClient_BeginSend_m64EE2621AE1D1454AD282D733781D628080E52FF,
	UdpClient_EndSend_mD7D713085090DEB85E7F116791B62EB6FC0112E2,
	UdpClient_BeginReceive_m5B450DDB7B13BF24E52CD53FB2DF00CE410CC4E3,
	UdpClient_EndReceive_m1BAA258139FD339A82F427A6D4049DF4A7A82B39,
	UdpClient_SendAsync_mC7A9D2FEBBE4DC07216DD57C2EFD6605C13262FB,
	UdpClient_ReceiveAsync_mA40A2896E4589B93AD9B7D9079E271841E27C5A5,
	UdpClient_createClientSocket_mD5B15DF98624D542C138F3993826628B6F8E4C31,
	UdpClient_U3CReceiveAsyncU3Eb__65_0_mEFDD57EC20634B7617653A2AA2CD802E9B897328,
	UdpClient_U3CReceiveAsyncU3Eb__65_1_mC35EF5D9963D628123BABF74DD90F4CB1DB047E9,
	UdpReceiveResult__ctor_mC2A34DE9634F5B6F2BF61EAA1D8768419B085F26,
	UdpReceiveResult_get_Buffer_m7C3092CBE979A308878E5B037BF26FDD29DAB8CC,
	UdpReceiveResult_get_RemoteEndPoint_m79E46C121B64109D4E12D3168C4712DF9FE945E9,
	UdpReceiveResult_GetHashCode_mC25E5C4B721F0FFD9AC58CE2F3788D404F288EB0,
	UdpReceiveResult_Equals_mE7B77E1DF28B1084E214343B9B5B881CDDED95DA,
	UdpReceiveResult_Equals_mDD344E622B6536C7297AE2429C8FAD2FEEC1132E,
	SafeSocketHandle__ctor_mA17D19F4D9F5F0D3C29A0C46702E85B9C9D87295,
	SafeSocketHandle_ReleaseHandle_m6861A54261EEC98F3DB4BB27251AA7ED6BD4DCE3,
	SafeSocketHandle_RegisterForBlockingSyscall_mFCDDEAB42C472D9A8E46B76C0BAECEB7D1832A98,
	SafeSocketHandle_UnRegisterForBlockingSyscall_m806E8BB959EDBB3B78061C5628A6F2F6EB6800B4,
	SafeSocketHandle__cctor_m7691BE9F87F473365D4973842102E05614970DE8,
	SocketAsyncEventArgs_get_AcceptSocket_mF12CD5B1067314F3970759A5AE7C94BF1070A4CE,
	SocketAsyncEventArgs_set_AcceptSocket_m1F265F9F17EE63050B267DFE9B5CA0F3E487B93B,
	SocketAsyncEventArgs_get_BytesTransferred_m18A255B3F69597D990A7287AAC5310DCDE9B77AE,
	SocketAsyncEventArgs_set_BytesTransferred_mAB2EB75BE1BB7B97DBEFCAF3DD42E26BC787D625,
	SocketAsyncEventArgs_set_LastOperation_m7BA8A0B5CB76B4C095BBB9E1124C3F237B51E26A,
	SocketAsyncEventArgs_set_RemoteEndPoint_m63E183C33AA5B60D8331A1CEC30A008B6AC7206F,
	SocketAsyncEventArgs_get_SocketError_m0609040BBEA9BB0C0C6161A7B9A0B5EF50A6346B,
	SocketAsyncEventArgs_set_SocketError_m0E7EC931E1FDA4014BCB6A5E7D5E3E216B80BFB2,
	SocketAsyncEventArgs_set_SocketFlags_m0ED0ECFCD2A27C9B5D9AD5F6C38B1146685F1EFA,
	SocketAsyncEventArgs_get_UserToken_m7953E8417ECA92D0AE46615B343136763B0CFB12,
	SocketAsyncEventArgs_set_UserToken_m10B93BD715163BE1A9C813413BD5BD3C4209032D,
	SocketAsyncEventArgs__ctor_m14B163B655DD0353E9E022305674A77BF5958D79,
	SocketAsyncEventArgs_Finalize_m59E3BE36B99D1B090D58EBA606D6750E040D6D01,
	SocketAsyncEventArgs_Dispose_mA5B8D8ADB8521BC9219061C13CB7C07BD02E16DB,
	SocketAsyncEventArgs_Dispose_m6DCE1086200FEDE70AFC93B221810F02334FCEA1,
	SocketAsyncEventArgs_SetBytesTransferred_m5ACAA13E707A9D5B8F75FD06DD54AB6F21F60585,
	SocketAsyncEventArgs_get_CurrentSocket_m1FA3256E04EA21BDF5B2ACC9813165D21E614F43,
	SocketAsyncEventArgs_SetCurrentSocket_m1A43B5217A35F7EB632A0AF19CAC038378E21026,
	SocketAsyncEventArgs_SetLastOperation_m73C3FAA754E365C569C956149E6E10E9739DBB17,
	SocketAsyncEventArgs_Complete_internal_m80E7F9C012A9FF94E68C09B05314042FA80549B0,
	SocketAsyncEventArgs_OnCompleted_mA73590353C7D619F0A57E5D65C7D6CF1EF41AD6C,
	SocketAsyncEventArgs_get_MemoryBuffer_m431CDEB0A4D2496B5A4C44232A72FF9DE95FEE1A,
	SocketAsyncEventArgs_get_Offset_m0D3ADE0DFB4BACC0FC3C85ED389DC9DCE451EA01,
	SocketAsyncEventArgs_get_Count_m4B67FDD41086AF476925D19F590DDE95579B5313,
	SocketAsyncEventArgs_get_BufferList_mD9E837C74ED11F81EC7076E532FA5067DCCAB173,
	SocketAsyncEventArgs_SetBuffer_m442BE1EFE47E011A747645F5A1F31F0702852EF7,
	SocketAsyncResult_get_Handle_mBEAD04497226B5110889462C480B2BBF93AAD430,
	SocketAsyncResult__ctor_m2123FFF2C2A56DB840E109348FEEA575B5B691E4,
	SocketAsyncResult_Init_m4FFCC4198F1133FC01DB80D562B59FB35086EC45,
	SocketAsyncResult__ctor_m3C1FE5E660B13BB4C17D1756B918D9EEC6052F79,
	SocketAsyncResult_get_ErrorCode_m2CFEB18E19F49775804758830C58B1B068CF936C,
	SocketAsyncResult_CheckIfThrowDelayedException_m25275FD1C77F37FB2CFDE46BDC669CF9B31031C9,
	SocketAsyncResult_CompleteDisposed_m41B0FF709D71B1B321871B18BCFAAB3005419BAC,
	SocketAsyncResult_Complete_mE0BFD66E710CA1E69D16B0283A5C637BDE354CAA,
	SocketAsyncResult_Complete_m218F8233E7ADB1AFFE69406F6337E09726E640D2,
	SocketAsyncResult_Complete_mC5E61EFDC26D56DCFD98A8600B185043FA285B6E,
	SocketAsyncResult_Complete_m6F201277AC561C7A755617A4963735B70D178B12,
	SocketAsyncResult_Complete_m49FA96276F9EBB207D8194440AE89300DDDAC513,
	SocketAsyncResult_Complete_mD191B47C619B02AB538B7A00721E689AAFBBD3EA,
	SocketAsyncResult_Complete_m85EF6089B3B9ECB2A9737E9C694BCCFFEAE48CF1,
	U3CU3Ec__cctor_m0F0A7E1C51C668515103160DCC4D3F3ED8B7C5C6,
	U3CU3Ec__ctor_mAC11B6F52B2C5BA43E6B14CF8338841CAA0ABF26,
	U3CU3Ec_U3CCompleteU3Eb__27_0_m267CAA8BCD77C0263EF3DAC0336FC29A937D2C76,
	SslClientAuthenticationOptions_get_AllowRenegotiation_m3FAAF852D579A7F688961D77D611E0A096791DA9,
	SslClientAuthenticationOptions_get_TargetHost_mC4CF1905E7B4F8193544D4997E305406B976A797,
	SslClientAuthenticationOptions_set_TargetHost_m0D8FF94F267DC51F2BDE6E1C94C2CD67783C4019,
	SslClientAuthenticationOptions_get_ClientCertificates_mD6898001726E82ADD9E3083D78BD05DB619B313C,
	SslClientAuthenticationOptions_set_ClientCertificates_m08B053F1C158912EEA43A4A28D37A740041CA78B,
	SslClientAuthenticationOptions_set_CertificateRevocationCheckMode_m5DB15346690DAC5A8F610AA8C7DEC7F2DFC29DE9,
	SslClientAuthenticationOptions_set_EncryptionPolicy_m008E6F28D3C4BFEFFB32A9BEA6AEF1C2B53F1C77,
	SslClientAuthenticationOptions_get_EnabledSslProtocols_mEC43F31DCFB3322900792809C72AC48070A9D93B,
	SslClientAuthenticationOptions_set_EnabledSslProtocols_mC0D304F69A5DC05292B87E00A73415BC83A632D5,
	SslClientAuthenticationOptions__ctor_mFFA8C3E39592A8376E7C3F6AC6FB89A6B7A702CD,
	SslServerAuthenticationOptions_get_AllowRenegotiation_mCA00A2853276A68C3B547E6BC0E5B1282EA4CEC2,
	SslServerAuthenticationOptions_get_ClientCertificateRequired_mD383FFDB61C59FD5568DFE8B6CDED914AB9977B3,
	SslServerAuthenticationOptions_set_ClientCertificateRequired_m30FD56EF277CCC34618D4BE98CFC4B541A8AF822,
	SslServerAuthenticationOptions_get_ServerCertificate_m7B4C5BBA8FE842496FA13E51D45BE6833671E980,
	SslServerAuthenticationOptions_set_ServerCertificate_mBA59C3095AB36F693A8DDF611E7EBE5D2AAC51E1,
	SslServerAuthenticationOptions_get_EnabledSslProtocols_m31B8C712D5D3A161E9EC3CE855D7FA44F8E98012,
	SslServerAuthenticationOptions_set_EnabledSslProtocols_mC66D59B188A010F1B1726EA34BF61E6DFE5A51A0,
	SslServerAuthenticationOptions_set_CertificateRevocationCheckMode_mCB4596F4D3F547C63E3029612C713F90EF28854C,
	SslServerAuthenticationOptions_set_EncryptionPolicy_m88D26CB33718B5E589647926F4264CFF5AC0E909,
	SslServerAuthenticationOptions__ctor_mDF5CCA0826DF6EFA4ECEDC6CB49A10B96D0A7EEF,
	AuthenticatedStream__ctor_m8B163146A01B938A3E87E1BED07EE07494A3E357,
	AuthenticatedStream_get_InnerStream_m5BDA4857898A90C696DAD90FD61C536FC3EB10C7,
	AuthenticatedStream_Dispose_m031F600EE876A06427FB0713D992FF02B9C3320F,
	NULL,
	LocalCertificateSelectionCallback__ctor_mE4F3A8BFF483881C2634112BB581CA4B07809805,
	LocalCertificateSelectionCallback_Invoke_mED43EE6E88B8C653C7D68966F86751B70907591C,
	RemoteCertificateValidationCallback__ctor_mC5C0650D4E5D29475BDBDA5B43FEA44DB0DBF367,
	RemoteCertificateValidationCallback_Invoke_m5256BF7D3B9BFFFAA959191FD5117C393F670D83,
	LocalCertSelectionCallback__ctor_mA7B36924E69A918A71649CFF95CB361E33317C38,
	LocalCertSelectionCallback_Invoke_m3AD956B5D53937117BD3E036068B49615337B5B9,
	ServerCertSelectionCallback__ctor_m06DDCEDF49D7A5F0F8494E3E960C7DFC64C64946,
	ServerCertSelectionCallback_Invoke_mB6A207E1A8CA76F667A242B68943297741CC6EC1,
	SslStream_get_Impl_m1356F55D556B6CBE922FB053F79BC930FCADF057,
	SslStream_get_InternalTargetHost_m9434AC1422D463C25B5C5313AC9BE45B095136AE,
	SslStream_GetProvider_mD8FA7A9739AD24D3525FAEFF94A2C400ABE7E9F3,
	SslStream__ctor_m9096E6E85E60C4D205A26FA43D121FECEC735484,
	SslStream__ctor_mB4497903594E8A33C14B9A4C4DAB7B16BA2E9B2C,
	SslStream_SetAndVerifyValidationCallback_m6FA4BE931FA1C7BC5D175B4E1CA2959742FA9C9D,
	SslStream_SetAndVerifySelectionCallback_m8B82CA433CF92AD113F606ACE7229D442587DA0B,
	SslStream_AuthenticateAsClient_mD8CF20D4070562AF8344C7AAF3041E420CA8B7A7,
	SslStream_AuthenticateAsClient_mB88736B524C5E2095C51B96634E38E38D759853C,
	SslStream_AuthenticateAsServer_m5573B24BAE2B574D3AA2023E1070198776919130,
	SslStream_get_IsAuthenticated_m5056C714711944DA54B35F6F2A3025A720B49290,
	SslStream_get_CanSeek_mA98FD41EDB70B03DB44F714CE952E128CA9D9B4B,
	SslStream_get_CanRead_m0B2ED5711B4D7C796529F56D7039AD546571FEF2,
	SslStream_get_CanWrite_mCBCABFAC940539066F34E4480E52E07CA06DA7CE,
	SslStream_get_Length_mD6515D77DBB57912C598761F67C321B6B16E129C,
	SslStream_get_Position_mD2DBB36BA22F4620B635557B5748E0928F28B6A2,
	SslStream_set_Position_m70987A87C7DB346D2FD447EA11E96774105EE5D2,
	SslStream_SetLength_m3B8BAE2F8203BB823F1E934195F50842A46E5643,
	SslStream_Seek_mFEDBF392998DDDB0F7A71F25D1BB821EA1463B25,
	SslStream_Flush_m61B328FCFEB939DE23740823CE3EB7E8DF3B14A4,
	SslStream_CheckDisposed_m884E18F119B366EE6734CCFA126A1F633687FA95,
	SslStream_Dispose_m187B76F54E2471469F5B62B8DDB33E11E40EE397,
	SslStream_Read_mEAFC2AA192D9EAF401E2B067395EA08EDC5FB47E,
	SslStream_Write_mCF650FDF725E1AAEDC462A016A38E535F7210F3C,
	SslStream_ReadAsync_m0F251A72A8D1C8C41A49CC99AE55D8D0F96A956C,
	SslStream_WriteAsync_m8211FC8F3FF4552E897516BE6CD70923848B89C3,
	SslStream_BeginRead_m716BC242891C530ECB08DA4989AE27EEBAC71C98,
	SslStream_EndRead_m881FE67FB5376508CBB50A96ED65BC672FB3A7A0,
	SslStream_BeginWrite_m66F3B2CDAAE0E1FD662552A8D1ECAC889F2AD2D0,
	SslStream_EndWrite_m78C915054198C80ECEE67CBB07D782E10686BA61,
	U3CU3Ec__DisplayClass21_0__ctor_mBE4019B5D6D20FC100DAF52B1172ACF497046370,
	U3CU3Ec__DisplayClass21_0_U3CSetAndVerifySelectionCallbackU3Eb__0_mADCFBC85C66A800C57CD2CECA4CDB6A9E5BB020D,
	EditorBrowsableAttribute__ctor_mE6105AD9666A4DF03DB2590C687EAC6B12D908CE,
	EditorBrowsableAttribute_Equals_m2DFA6ADDE69D95657A8257840C01FCAF74A6B02F,
	EditorBrowsableAttribute_GetHashCode_m1AE50FA45B338D71189AA3C249DB56406DEC798C,
	BrowsableAttribute__ctor_m1828B5114921E3FF83082211F2FBE82517559035,
	BrowsableAttribute_get_Browsable_m23594845C49373D8D6AB753D89DB39D8285250F0,
	BrowsableAttribute_Equals_m8EF756EC88F170513772F14FC08BDAD1EC2B863D,
	BrowsableAttribute_GetHashCode_m40CEC719AE0A79F4339009D2E473342E59BAEDD1,
	BrowsableAttribute__cctor_mAA4C6BF30D72D9DB9B0BE59F46140D4E284E15B2,
	ArrayConverter_ConvertTo_mC144B3093D2037D3BB5B109D15725ABCC4E985D9,
	ArrayConverter_GetProperties_mC4FE7B6803C8B34BD45ED4DB67B0D3692AF60F55,
	ArrayConverter_GetPropertiesSupported_m00411C896EB1E0B5155BB701D72A7F690C86C6F8,
	ArrayConverter__ctor_mAA6B57C16F167FF104B8E23402DE87ACA5E13A2B,
	ArrayPropertyDescriptor__ctor_mC12E0FB619504B32AC30C714F3E51B0C3BA813AB,
	AttributeCollection__ctor_m6987DEDDE616B905793F96BC20866FDEC937B313,
	AttributeCollection_get_Attributes_m698416274F06ACDAA3A55972D029340253DAD0F6,
	AttributeCollection_get_Count_mBD37E9F785A4BD5426A67D42F0FA01FC0E2BEA02,
	AttributeCollection_GetEnumerator_m1464C016A16216F1F87A46940CF97B742AA032C6,
	AttributeCollection_System_Collections_ICollection_get_SyncRoot_mE4F31AFB7E65BD84B5D64120F0C68F72753CD409,
	AttributeCollection_System_Collections_ICollection_get_Count_mFBE2F16F7901E3F85F0C4B37032E83E99BDD2300,
	AttributeCollection_System_Collections_IEnumerable_GetEnumerator_m5C57499D1B27935C97A88E9B44D571CAA87D93A4,
	AttributeCollection_CopyTo_m29A1B7F330A44A3ABD7402CF9863089B917AA7F6,
	AttributeCollection__cctor_mCFAD6C1E6356E3CA0CDDA680BDB66D72B0161DF8,
	BaseNumberConverter__ctor_m697E7E5DB315CE6CDA318F256A634BAEF3C7C76F,
	BaseNumberConverter_get_AllowHex_m2A6143CE655C657EDFE8A7DC3897EEA0EEB3A4BD,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseNumberConverter_CanConvertFrom_mA4351F586FC63F67CC1E0013CD68BCC2F89FB916,
	BaseNumberConverter_ConvertFrom_mAC29686169A55736CD527CB44D3E8DB0D697C7DB,
	BaseNumberConverter_ConvertTo_m46174D69B2F224A17A25C88B162C0270DE801CF2,
	BaseNumberConverter_CanConvertTo_m6108C34CF9DF475BF4074181348848BCE6C53F76,
	BooleanConverter_CanConvertFrom_mD1A1ADBAB30E4ED54C20554B62F6E4C38369E6CE,
	BooleanConverter_ConvertFrom_m828B810B45C660FD12088B64754C92984D043092,
	BooleanConverter_GetStandardValues_m748173E319A659C8D26EF251DE71C75C83ED2E0C,
	BooleanConverter_GetStandardValuesExclusive_m0C6DCBFBE8CB8099BD7B6CED00D30BCAF375E2BD,
	BooleanConverter_GetStandardValuesSupported_mDEE5DB63C7EE136CF18FED6BE1DCB90749603D91,
	BooleanConverter__ctor_m50990A1CB60D3557199985EF164FF1E8AFCADC87,
	DecimalConverter_get_AllowHex_m2B657E4B11F10E8F6F3E27F8F24EBD407E68386D,
	DecimalConverter_get_TargetType_m713B317E87EFCE4C62D9252D5C995FCEF1DAEF47,
	DecimalConverter_CanConvertTo_m2C1D96863DB726FCB8566B7B78899A0DD5D85CC4,
	DecimalConverter_ConvertTo_m0F245DFB5E97D39B818A6D3A87C6FAE07F57F6BC,
	DecimalConverter_FromString_m65A0F7D7BFD4A568C416732121878DDA2CDB2423,
	DecimalConverter_FromString_m4F6FAD944464E3B239B445BE45950CF15E423BFC,
	DecimalConverter_ToString_mA2278E45267D367F8FED73BDE4293021E493499F,
	DecimalConverter__ctor_mC1E815EF17DE8DBD91FB472C83A4F38514704B46,
	DelegatingTypeDescriptionProvider__ctor_mF4BAC468855383A4FA4CBFA329DF02F35805ADC5,
	DelegatingTypeDescriptionProvider_get_Provider_m39F8D32C2826EC53330210A55058A76B06136E74,
	DelegatingTypeDescriptionProvider_GetReflectionType_m130F511F42060A53340620EB0667237EB0F94024,
	DoubleConverter_get_AllowHex_m7B24D1C1979E1B4B0F32D455B7BADE06AB50DBBB,
	DoubleConverter_get_TargetType_m32CBA966154111137DA7AE087217F3A7BAFECA88,
	DoubleConverter_FromString_m205541D548D67CE0C9493E7CC532133BD5B27DC0,
	DoubleConverter_FromString_m7E7B0719E5299B5DEF5806531A51807A6860050C,
	DoubleConverter_ToString_m9BDB00A75E71F1001A1F555AD9E786C32CF668C3,
	DoubleConverter__ctor_m3CA5201BA68AB510BB2112A6755FCFEAB6DE53BA,
	EventDescriptorCollection__ctor_m5B672594D0220F89AD3DFE0FBCDB34FED5B56CF3,
	EventDescriptorCollection__ctor_m6FEF5B613838F164D7F9FC939EA915FE7B5CF118,
	EventDescriptorCollection_get_Count_mCBBE573DF8EC096E14170A5039E90107943E5A87,
	EventDescriptorCollection_set_Count_mD825B07B7C93EC3A511CD0DE95EFB9F490E146E8,
	EventDescriptorCollection_get_Item_mA010043B2DFD4EE474361E4485B7727354BB00ED,
	EventDescriptorCollection_Add_mAA14F6928C91D2A6B81E69723C35E7F43FBA62CC,
	EventDescriptorCollection_Clear_m34F6F48BAA557FBB783F24C33EB2E26002A57C56,
	EventDescriptorCollection_Contains_m50DB06B4BB92F9F108E41B0B39C71C687A426F12,
	EventDescriptorCollection_System_Collections_ICollection_CopyTo_mFAE7230CA92EB811EA7AC85B9EF758477D9CB58D,
	EventDescriptorCollection_EnsureEventsOwned_m84EEB041AFCAC7DC00D846999338F998A3B873B4,
	EventDescriptorCollection_EnsureSize_m9A0437DD4221566FFE953D7D8A9B15CEC82D8F1B,
	EventDescriptorCollection_IndexOf_mFB443D00688BEE960D0AB1EFCF41FD41863D105D,
	EventDescriptorCollection_Insert_mC472AA88B6CC60161C15A8B464D4D1A827C45CA7,
	EventDescriptorCollection_Remove_mC7EDCADC4A6630ABA8C38C7F1CE424C622A73A67,
	EventDescriptorCollection_RemoveAt_m1D8C3C214CDF147C85410AB0F956EA1732261900,
	EventDescriptorCollection_GetEnumerator_mBBA565367CD4D16306527D637EA37D2BA6949478,
	EventDescriptorCollection_InternalSort_mFDAC78BDC8DF8D728C05BF58B24C6B3AAE83E1C9,
	EventDescriptorCollection_InternalSort_mC173B2DDBF4F976EC4B3E9A2AC03FFD0D3F115AD,
	EventDescriptorCollection_System_Collections_ICollection_get_SyncRoot_m929985FDBE786C0523515318A463F1DB1F0EBAA5,
	EventDescriptorCollection_System_Collections_ICollection_get_Count_mF9D4EB7237921F1FDF76F78307CB4C5AE29C5D74,
	EventDescriptorCollection_System_Collections_IEnumerable_GetEnumerator_mC73C30221041A865D2AA69E9E57D5D50A89E756E,
	EventDescriptorCollection_System_Collections_IList_get_Item_m816D7A606BEA8FDB4B8F959E0727A591139AB75E,
	EventDescriptorCollection_System_Collections_IList_set_Item_mEEC2A36C2AD376E350334887290721ECF319F17F,
	EventDescriptorCollection_System_Collections_IList_Add_mC80A1930B52051837C3B553FED53E0A3624609D2,
	EventDescriptorCollection_System_Collections_IList_Contains_m7CCEEC883D1D814BAA11C20A2AF4B2ECB25CB670,
	EventDescriptorCollection_System_Collections_IList_Clear_m5F4F6DC457DC4AC15BF1ECDEB05C9AC37A8EDFAA,
	EventDescriptorCollection_System_Collections_IList_IndexOf_mF92645F7291545162437FAC95787100B4844B4E4,
	EventDescriptorCollection_System_Collections_IList_Insert_mC6AD4196A4F520BB4C0B5866D3D6307A9639A58E,
	EventDescriptorCollection_System_Collections_IList_Remove_m04E2ACE9E3B90350007626A227EA9626F133589C,
	EventDescriptorCollection_System_Collections_IList_RemoveAt_m67FB853573FD34FEB23F0763434FBBB1FF8D3389,
	EventDescriptorCollection_System_Collections_IList_get_IsReadOnly_m14ED5EDE6AAD26BC174F00C4F48A16C0874B6160,
	EventDescriptorCollection_System_Collections_IList_get_IsFixedSize_mE80CCD9C034D17CB7E7780BF1FFF1FC92123EA62,
	EventDescriptorCollection__cctor_mEB5345968B743898C4EE2AFED371C8C3A7816C80,
	ArraySubsetEnumerator__ctor_m76C2057A80D4E3DCEA8B97DD0EF9456AE5D6EA92,
	ArraySubsetEnumerator_MoveNext_m8AD8B4514C227FAFDF85FE66EE18F51EF8E00497,
	ArraySubsetEnumerator_Reset_m49967EC2F723F5F2EABD5E780E51B0AE912FCD77,
	ArraySubsetEnumerator_get_Current_mE137EF1ADECA45633866E8723C50E4A07FFA478B,
	Int16Converter_get_TargetType_mB268E3FEA2B5BDEBBC628D21A07686856FC451C8,
	Int16Converter_FromString_mACA7B00848D2EC76B985408DE0D92D84F462D14C,
	Int16Converter_FromString_m57535287425AA32CBB51833859D5224F030EC2F3,
	Int16Converter_ToString_mB9DD75EE872A4040BFFC794C4400FBC857A75CCF,
	Int16Converter__ctor_m3803A7772B21F49200FC142C193A40DC286C9DFF,
	Int32Converter_get_TargetType_m837B83368B96CE1CDFCF450F7DA3629ECC86DC1A,
	Int32Converter_FromString_m3917F97D4401800516F6F44363CD154F9645AC72,
	Int32Converter_FromString_m54931A645C6C1B4DB770894D5A23F24A03E9B340,
	Int32Converter_ToString_m8B5D811FCF0FBFADCAE25E354FD27FE6256AA36D,
	Int32Converter__ctor_m88220DA12F6ED6FE4D1232B73C20DECE9EFC63FF,
	Int64Converter_get_TargetType_m8279327D6F373BEE6E4BF0439215D6F5DF6B0097,
	Int64Converter_FromString_m4F1E8450F4A60B29297D4AA3D8FE196B09D98102,
	Int64Converter_FromString_m324A7ABADC1B991B6AAB4B600C0A676B289BDF72,
	Int64Converter_ToString_mC4F1CE4731349A078460E190F587DB3276B54B3E,
	Int64Converter__ctor_m3C124DB065F27F6BEAB7ABAF4B3D13E6A9F3E94C,
	PropertyDescriptor__ctor_m20223572362A331B6F0D436BC19AFE2EE87558B8,
	NULL,
	PropertyDescriptor_Equals_m69C3B1D9E4AF806536B7545DAB5FE72883A512F6,
	PropertyDescriptor_FillAttributes_m089944713C5E19B880AC940B07C609BFC3D273D1,
	PropertyDescriptor_GetHashCode_m8470877D9BFF977617586AC7E2B900D01B3B4E3F,
	PropertyDescriptorCollection__ctor_m3D1AC0D2B3187A895AE8B64AC891251860D8C361,
	PropertyDescriptorCollection__ctor_mAD196DBB6D71AA12A41715798F3DFACFDA170BAE,
	PropertyDescriptorCollection__ctor_m3452043C947348EB364E043013AB2DB4165B72D7,
	PropertyDescriptorCollection_get_Count_m9121E7236AF94FE7AD4BB3C48ADBBB2A8ACF3508,
	PropertyDescriptorCollection_set_Count_m035F2DDDDD3FEFC8EF701C3A584CA6BCDAB6D854,
	PropertyDescriptorCollection_get_Item_m5BD52F4CCBEF1D1D9B33887BB373181AD11A4C25,
	PropertyDescriptorCollection_get_Item_mD34F172015A4AAC759099193BBBCDB1489D51E09,
	PropertyDescriptorCollection_Add_mC398A8C807A50565628FA7377622CB29BA08E8A0,
	PropertyDescriptorCollection_Clear_mB8C7EDDBC34045AB39A85F7CA8C2A48FE5E84154,
	PropertyDescriptorCollection_Contains_m61BE63B4CBEBE522952340A5E1AD4EC81561A534,
	PropertyDescriptorCollection_CopyTo_m96A7340B76D01123FFC2968406157491DA5F2F51,
	PropertyDescriptorCollection_EnsurePropsOwned_m95EB38CC4CD3A669B5890569BE6EA68DDC0691D1,
	PropertyDescriptorCollection_EnsureSize_mA9CC05191669F9F3899F5A7D42B8DF24C51CC6A2,
	PropertyDescriptorCollection_Find_mCBD6FF2B467342E2385A83851AFEA0D79765D941,
	PropertyDescriptorCollection_IndexOf_mD936E02B8B1AACBF21EB129B5D7F3C8CBC07BE15,
	PropertyDescriptorCollection_Insert_m5294EDBAD3EED9E88B72F8C337F65EE5AB2960EA,
	PropertyDescriptorCollection_Remove_m93B40D4D8FD13CA21ACAF39012043595BB6ED03B,
	PropertyDescriptorCollection_RemoveAt_m0A24CA5AB989131D0F6691B289DD9092790E9E07,
	PropertyDescriptorCollection_Sort_m75E8E60ABA88330A9CE34B6DDE5E6DCFD10C6867,
	PropertyDescriptorCollection_InternalSort_m976B7A3E0A38A56F3E6C282199722DFC37EE92C9,
	PropertyDescriptorCollection_InternalSort_m715EF0848576185E9957756222B20A93320A3108,
	PropertyDescriptorCollection_GetEnumerator_m895320E1DA7A7E88D5392EA88C7415FCCE9F9A69,
	PropertyDescriptorCollection_System_Collections_ICollection_get_SyncRoot_m375B281E77CC48014E87622A8F7E5FF2BB6B23AF,
	PropertyDescriptorCollection_System_Collections_ICollection_get_Count_mD0FF809D823EB373D8A6E8DB49619ED4141702A2,
	PropertyDescriptorCollection_System_Collections_IList_Clear_m45872C5426EECF7A39B524076FF749A27FFFD720,
	PropertyDescriptorCollection_System_Collections_IEnumerable_GetEnumerator_mCA1C26C5D8253A2654911C476DB3E2E4C7E1DED0,
	PropertyDescriptorCollection_System_Collections_IList_RemoveAt_mF52F79BDB81434BECEFF023F99EFDDEE21E3D382,
	PropertyDescriptorCollection_System_Collections_IDictionary_Contains_m0E06B08842DB4FA109B5AB61EBEFB731E3EA94AA,
	PropertyDescriptorCollection_System_Collections_IDictionary_GetEnumerator_mF6EAE817278709EF0201AB2E677B887CA7520A51,
	PropertyDescriptorCollection_System_Collections_IDictionary_get_Item_mC6EFD0AFD470D71014230D69B656107657929330,
	PropertyDescriptorCollection_System_Collections_IDictionary_set_Item_mF70D7EA54C026CF6DF6082671C7BB5FA4FA8C4A1,
	PropertyDescriptorCollection_System_Collections_IList_Add_mA83681FD649F472C18C4222547909AB5C0B2636D,
	PropertyDescriptorCollection_System_Collections_IList_Contains_mF4A5AD4B8FB2DE87C3F46804A45A9E5FE061F0A8,
	PropertyDescriptorCollection_System_Collections_IList_IndexOf_mDDE0710A46D8C7F0C812BBB749058633E2B538A0,
	PropertyDescriptorCollection_System_Collections_IList_Insert_mECCC2CBDA9731E920BDA0D1C5E1B3446D1051D7C,
	PropertyDescriptorCollection_System_Collections_IList_get_IsReadOnly_mD92E29DD5AF442E8F758EE9C364AA67D8C692429,
	PropertyDescriptorCollection_System_Collections_IList_get_IsFixedSize_m384E8605F0612E87E1A78F863E28AE7E0D0D6B8A,
	PropertyDescriptorCollection_System_Collections_IList_Remove_m6B3B43F181BF42AE15D0EC97F83E16157119767A,
	PropertyDescriptorCollection_System_Collections_IList_get_Item_m330B3FFF9A800353E1463DBA60359A5846637632,
	PropertyDescriptorCollection_System_Collections_IList_set_Item_m1C9BF875A752FEAC57320E429DF95D0A11BDD5B8,
	PropertyDescriptorCollection__cctor_m9FF868A1DA3307692B17BEE0248BE146682D8DA7,
	PropertyDescriptorEnumerator__ctor_mEF40577980844F48F837016FC78D6D6C4B656C40,
	PropertyDescriptorEnumerator_get_Current_mD139182036930AB6DD0F33E0C311C5D280E8169F,
	PropertyDescriptorEnumerator_get_Entry_m83A1F1F4EC45FCDAB6444FDAE98158CAEA6E17C0,
	PropertyDescriptorEnumerator_get_Key_m9AC03BAD80D2C982D93FCF6E7FA77694CDA80583,
	PropertyDescriptorEnumerator_get_Value_m5EC049B1D1B61491D697056DD2A5018B3F83DAB2,
	PropertyDescriptorEnumerator_MoveNext_m1558DACC789497E2E8BEC2C914F68449B51F35E5,
	PropertyDescriptorEnumerator_Reset_mF7C0D145E906CE6FCB18646933E580C14A0AE1DE,
	RefreshEventArgs__ctor_mA88DB8D3EBE2EDC16126E5DA09495F31F6E43CC9,
	RefreshEventHandler__ctor_m2A318A5FC4A7829BCC18BE0F9A652DCD1C8C192F,
	RefreshEventHandler_Invoke_m64C4651451C458F51B3DB45E9BF2D20E0B1DBCF7,
	SingleConverter_get_AllowHex_m49756E0A9F6A76035FC8ED207F161A4C28C7B0DF,
	SingleConverter_get_TargetType_mC060F5026570A7907B8B1D9EA0402752331C6BD0,
	SingleConverter_FromString_mAB3584379A90F18A33A61D1913F74D590A17DB30,
	SingleConverter_FromString_m27810AC814274F6507B5D2B3E3D6886B153661DC,
	SingleConverter_ToString_m02237BA935CD713251AB84BA7B0E77283E65427E,
	SingleConverter__ctor_m463B63EB8D945D2D43E5B8099D8F0EC24AEBDA7A,
	StringConverter_CanConvertFrom_mCE12DA7BD7ABA631AA3C00EF415C6C650636D00D,
	StringConverter_ConvertFrom_mDC2BBDD289C8D8259AF915CB23765888BBFAB9AC,
	StringConverter__ctor_m074D8686ED6ABC0E64C20D1EFD4406F215E7187A,
	TimeSpanConverter_CanConvertFrom_mD6725B6738FA9EC354BD235B49DBEF0A522F1110,
	TimeSpanConverter_CanConvertTo_mA1CF08D25D65F197EAA28F3FD3C4218A27FECCA0,
	TimeSpanConverter_ConvertFrom_mF00171D4039FEA734A0FC5ACDB81BB0395A3AFF2,
	TimeSpanConverter_ConvertTo_m30C65F425D5F7D46475EC262213EB57255929A6F,
	TimeSpanConverter__ctor_m7319DCEEDA2578B340AD4FEEE010AE5CEFAC9BD5,
	TypeConverterAttribute__ctor_mEEE3CA7C17555BA93BB76201BA2BCF879E617DA9,
	TypeConverterAttribute__ctor_m0E1A55E1BD368F0AABCAF766BBCE1D275777A52A,
	TypeConverterAttribute_get_ConverterTypeName_mBBE5335F11FC13345D2828866172BB5DDF9F3E45,
	TypeConverterAttribute_Equals_mA3BF2C8CFA0AE8B5D0F52E066B1141AA53B4E015,
	TypeConverterAttribute_GetHashCode_m23ACA2A297447E17150BF490188919676FBD8A01,
	TypeConverterAttribute__cctor_mB9A8C9E5E62F8A8D81E250D0B53D359860D7C04A,
	TypeDescriptionProvider__ctor_m575B3B96D9C9A3B7C0D34A14D9E5FAA63EEE004C,
	TypeDescriptionProvider_GetReflectionType_m595CDB1DADC484AB12377301C2D93AD8DE2A61C8,
	TypeDescriptionProvider_GetReflectionType_m370874914F8C84A0B85985491FDBAAC993C3851C,
	TypeDescriptionProviderAttribute_get_TypeName_m7E06882E9348A8D4987BB62EA1493B2BD23147E6,
	CollectionConverter_ConvertTo_m79E20F624C9391C76B8081E74BF16EB4C04535E7,
	CollectionConverter_GetProperties_m5E61C7ABF345599E670E2FD270A35190DA1DBF20,
	CollectionConverter_GetPropertiesSupported_m2068FA7A17AA42142E42EBF63151013E0934C728,
	CollectionConverter__ctor_m20ED0C661750EBE427D356F24AED92A407988147,
	EnumConverter__ctor_m3E3C7D81C8092A5591BAA556288B2A3E22DCA99B,
	EnumConverter_CanConvertFrom_m08BA3C1DC3D84B157F087AE1E407A974E3A85D11,
	EnumConverter_CanConvertTo_mF6140EA2D6D701B21DC1EEFAAA2C62DF8CAB690A,
	EnumConverter_get_Comparer_mBB79A346E8150D9554B6FBCD0EF1B981A7696DEC,
	EnumConverter_ConvertFrom_mD1D0496C621971B0610C92ABC562B26BEA40EC61,
	EnumConverter_ConvertTo_m95581FDC64EFF404D3646BA0BB09A316C58126D1,
	EnumConverter_GetStandardValues_mAF14DECB7670119DFE61D9955A5667BC43A1F023,
	EnumConverter_GetStandardValuesExclusive_mFFEE5BB5AEA41073002C03D1CB163E3F42619317,
	EnumConverter_GetStandardValuesSupported_m049732C08BD2A4300C75A9FB12BE6082C2DAA5EA,
	EnumConverter_IsValid_mF38C2D56D21042513C025A195EBF00C9626CB105,
	MemberDescriptor__ctor_mDCA5ED4D5D9CB14D6D1D81EDB76EA80DB79B8AF4,
	MemberDescriptor_get_Name_mA0D12966D84092E03F4DC16A085176299AC73291,
	MemberDescriptor_get_NameHashCode_m415B96A35EB826A5A00E0F0EBBDD94980BD5E5F2,
	MemberDescriptor_Equals_m375ECBF78B86952EC60CDD90301D8C2EF911A620,
	MemberDescriptor_FillAttributes_mD79B898348F5E1F6C98F62F0D4CE719C5547C56C,
	MemberDescriptor_FilterAttributesIfNeeded_mB060A4FB6DE46D17F76E54B43A0772EC623EFDC7,
	MemberDescriptor_GetHashCode_m18BBAA42F4FF641C84B5E45802F1FDC290F86860,
	ReflectTypeDescriptionProvider__ctor_mD2C527AEF85A636D3E4507604E84FAB79A98DB62,
	ReflectTypeDescriptionProvider_GetReflectionType_m42CCC59A3631DE84FA1BA53F4D0266F5CC7DA92B,
	ReflectTypeDescriptionProvider_GetTypeData_mAB0CA0760FE347023B0CB43F64B800C0E4CF2064,
	ReflectTypeDescriptionProvider_IsPopulated_m84F86F476D4C3552D9319BE958418D78CE50C1A8,
	ReflectTypeDescriptionProvider_Refresh_m2E5B6CEEA9ED33154509B1DE82E5BF4E5814CAF0,
	ReflectTypeDescriptionProvider__cctor_m7F48DC584D9D7017DDCF9AC5F86CDF8193097C7F,
	ReflectedTypeData__ctor_m229206DEEAEC214C249B48F72505C12A2C10F233,
	ReflectedTypeData_get_IsPopulated_m207FD7DEB577D25B681C31AE2F2A86D64895C76E,
	ReflectedTypeData_Refresh_m5A18415C807E341603C1A069AA7A3EB76258CAAD,
	TypeConverter_get_UseCompatibleTypeConversion_mC6FAEA504FE4DAA7723C2B31F7E7C80E237110A1,
	TypeConverter_CanConvertFrom_mE41048372E94CADCD46A0E450385F4E13011D19B,
	TypeConverter_CanConvertFrom_m5E04D8BFD5D73042127767BD65B6434DAAEC2E53,
	TypeConverter_CanConvertTo_m05803F15F053E4DA3CF290D52FC379BB9D8C396A,
	TypeConverter_CanConvertTo_m959B290C863DDFBDD575AF3236B0A17E02E44013,
	TypeConverter_ConvertFrom_m74D536778D865EA22414730575143153FD749EDD,
	TypeConverter_ConvertFrom_mDE2D27BF8934184429496B39ED63BD3702868774,
	TypeConverter_ConvertFromInvariantString_m81DA50DC43E5868C83A5D283FFD227B919C9D0A5,
	TypeConverter_ConvertFromInvariantString_m990BDB1C582D2E303DAA1DF2B123260C6A55A31C,
	TypeConverter_ConvertFromString_mE302BA2C46E1B0D2C310A5E5B32AA71EAA73F3AD,
	TypeConverter_ConvertFromString_m6B1F932DB2AE6F4C06B84360B51790FF06C6870B,
	TypeConverter_ConvertFromString_m84FD8657B6D8327BE563E5A649A1D89A80BF2A32,
	TypeConverter_ConvertTo_m0171DC87F8C3920E781691F5F8F1145A6DAF9CE8,
	TypeConverter_ConvertTo_m5D4785A320B7ED351B677E28B77171F70E389B96,
	TypeConverter_ConvertToInvariantString_m1E0494D32C84A9F062F9762C8979E223C74D803C,
	TypeConverter_ConvertToInvariantString_mF902AFB0FE32B8E7014DE226455D3FC79030FF92,
	TypeConverter_ConvertToString_m124B43934E0022CA836E7258ABAF8F9FA8911093,
	TypeConverter_ConvertToString_mBE2DD3AF57FBB948F861DBD1DEAE33B18A33BF28,
	TypeConverter_ConvertToString_m85A19DC8D0117332C304EBEE2911D66F8028E245,
	TypeConverter_CreateInstance_m3431A3634D1048D919FF4B1DE80605A2BC8EA7C2,
	TypeConverter_CreateInstance_mCF1FB466625510C41AF4C31DBF0D6B0D17AD8F34,
	TypeConverter_GetConvertFromException_m4CD1BB8AACD489FC7094283F199FC6DB6409F7AE,
	TypeConverter_GetConvertToException_m2F0A3FF8E38030AEABCB146D9A65FD27C3F4FE52,
	TypeConverter_GetCreateInstanceSupported_mFFBD842063695B04C1D1F918715576E58210B9EA,
	TypeConverter_GetCreateInstanceSupported_m6F211317A2C58B002A523A637B40EC6E878D031B,
	TypeConverter_GetProperties_m469ECD3A977FA29494A2D9E6F6EE957EA921E107,
	TypeConverter_GetProperties_m59D3B36D9259D09DC67C2DA59245BCEBDFA4D6F2,
	TypeConverter_GetProperties_mBD67295683221C70AB3253653C06C39F640F4F44,
	TypeConverter_GetPropertiesSupported_m67A5238F90828ECD5ABFFEA50DF3F10DE2324A3D,
	TypeConverter_GetPropertiesSupported_mDE66D29A3EED2AC764A8B4F3C91917E2F9732763,
	TypeConverter_GetStandardValues_m2D0F59931104F725470D67A2DC6167B1980BE390,
	TypeConverter_GetStandardValues_m9F77FD4C8216D3F06928B418993659F3A0FEF5B6,
	TypeConverter_GetStandardValuesExclusive_m41CA2CC09F031BC8A14339DC376FE50D31AE9812,
	TypeConverter_GetStandardValuesExclusive_mCA336C7CE58B28DF0B46D5B52E3D0D177718FF42,
	TypeConverter_GetStandardValuesSupported_mDE36B9244050270D1DC2F6F840C10DA945CFAA23,
	TypeConverter_GetStandardValuesSupported_m5DFA377B1091BB08E64654FBEE8613EEDE731718,
	TypeConverter_IsValid_m54570B0309A0F2735FE3FA162D5F669B8BF434C8,
	TypeConverter_IsValid_m991D78815B29EDE7EF8B1CD0114A5E6B84F7328C,
	TypeConverter_SortProperties_m814BFD1763DEB6AD4D3E6B6042777358523AB3B6,
	TypeConverter__ctor_mA5B1882A94D0491297B903563E8B03D75B2F67A2,
	SimplePropertyDescriptor__ctor_m6DF452C766FA5F610C684119950D96C7105A2B26,
	SimplePropertyDescriptor_get_PropertyType_m613F51DD8C6ECFE9C107312A24AAE28F375650A1,
	StandardValuesCollection__ctor_m9049046DA81FD7B6FE279E063A43DC9B1D3B26B0,
	StandardValuesCollection_get_Count_mC127EA7E146C2FD58D5740935B45CACCD13D4637,
	StandardValuesCollection_CopyTo_mC06C9542D25F3E2D5A1DFDE690C920E8CFF94E10,
	StandardValuesCollection_GetEnumerator_m0484C08651E32F669A0624D29E44C6CDEB550F6C,
	StandardValuesCollection_System_Collections_ICollection_get_Count_m98A89DC263C152B5E1A5B5A6B795D963CBADCD4D,
	StandardValuesCollection_System_Collections_ICollection_get_SyncRoot_mFFAF4C68B76F26F6816CEAB7D8D86656B1D1AC51,
	StandardValuesCollection_System_Collections_ICollection_CopyTo_mF996F3746F028C7EB58F7096E7B7B7FAB911A896,
	StandardValuesCollection_System_Collections_IEnumerable_GetEnumerator_m66B4D529F0193F42A3858A1B638372B8A6687672,
	TypeDescriptor_get_InterfaceType_m468BAAC18190A6D8E4369B3139FB38ECE1B4C5BB,
	TypeDescriptor_get_MetadataVersion_m6E33CD6DCADDEA862C9D9266B664C21DE70D449A,
	TypeDescriptor_AddProvider_mCE291812F3ECB4C76537D451403951E926F7EBF4,
	TypeDescriptor_CheckDefaultProvider_m1467B92A2B042B2E7F6332210808BB6AA42BABB1,
	TypeDescriptor_GetNodeForBaseType_m85BCD83E89EE286F365A1553239C985BEB1D785C,
	TypeDescriptor_GetProviderRecursive_m371FD85FC01238874F7387FA430260D57646EA3F,
	TypeDescriptor_GetReflectionType_m2E40168C9752B069A3E430478C46D54463DA6707,
	TypeDescriptor_NodeFor_m99219DF297037E09BCCFD4492FFEDDD7C7D229A1,
	TypeDescriptor_NodeFor_mE9C1F8845A707769A5A95E338C610FA244DA283C,
	TypeDescriptor_RaiseRefresh_m026E52DA15538070CED25BBCBB696318032A6A8C,
	TypeDescriptor_Refresh_m8F344E720B395FC5493977659FACECE567A6A223,
	TypeDescriptor_SortDescriptorArray_mF088401B40E9E2739EC9F16B98F05D60A9B89A47,
	TypeDescriptor__cctor_m3EDDF7A3EA099841E95028726F74677419380E0D,
	MemberDescriptorComparer_Compare_m33438352E9BA8C1BF6B594E524DC1BF8BC5EFFE1,
	MemberDescriptorComparer__ctor_mDCB73C6EC35AE31BE43CBF4C88053920603B4431,
	MemberDescriptorComparer__cctor_m31A7C495358AAA5B5E72F47BDC54DDC970E3A11C,
	TypeDescriptionNode__ctor_mD6C85C9088064C16A401309FAFDC62252A5D5526,
	TypeDescriptionNode_GetReflectionType_m54393E311DD22AEF766FC73C95C7059CCD09FBF5,
	Win32Exception__ctor_m0B20F25EC4A56A122FF766429AF40AF6292E11B1,
	Win32Exception__ctor_mFC38F1C99031D227C6BB3CA07099866D39994281,
	Win32Exception__ctor_mB0380F996944EDFCF9CACC7F28A18874F19B4FD4,
	Win32Exception__ctor_m1B3DF3F51BBF050C18F933B36027A8A4902668C4,
	Win32Exception_get_NativeErrorCode_m33C1D176D9178C36DA32F8FEC251B923EE53CE5E,
	Win32Exception_GetObjectData_mB7993FD0BE12222D5518373EBAB84F89651D99D2,
	Win32Exception_GetErrorMessage_mE857D9093D6D366D960BD8A8FCCF22DF6FB92728,
	WeakHashtable__ctor_mBFFE17D4547F681A27EBE60D277625C9F9D0510F,
	WeakHashtable_Clear_m306746F8BC4535C2327FFDF167F23CA06077A6EE,
	WeakHashtable_Remove_mC091A16F4775270A1FC840FE4E82917E4F010A03,
	WeakHashtable__cctor_mDC0172C1DE43E226990881404CE73D1B64046A16,
	WeakKeyComparer_System_Collections_IEqualityComparer_Equals_m2DE66C4A7CF44E5A99DA241D8271C285ABBE56F8,
	WeakKeyComparer_System_Collections_IEqualityComparer_GetHashCode_m7E00D4A81998A46FFD9BE51679C6169DF0CCBF63,
	WeakKeyComparer__ctor_m1DB79747A0192C8CB5B45E74743CA7CA7EC09E52,
	InstanceDescriptor__ctor_m4A88597D681C8A8A9551AB318F8CBB47D49BEC57,
	InstanceDescriptor__ctor_m9AA7C372C2EEDA3F07BE3A6CCC21BCBD3814B64F,
	InstanceDescriptor_get_Arguments_mF64376FB377AA57CC10376452FECFDC55C6EC837,
	InstanceDescriptor_get_MemberInfo_m58DD58AFB1C49C6B33C3958002E7CB9D61374E9E,
	InstanceDescriptor_Invoke_mACDF3C04B3998B38C4A79FCFC66F4760EEEEB2FE,
	NULL,
	NameValueCollection__ctor_mCA29E5386DACDF19F6F51BC395324F0A8EE05604,
	NameValueCollection__ctor_m1545E08D22D8E6C1FCDDAE43741AA43D52F4A21E,
	NameValueCollection__ctor_mAC66C8064A4AAB73914143C4CDF3BF28543BAEC3,
	NameValueCollection_InvalidateCachedArrays_mA9AA2F9B6A67C2576FA30F12208AA6E1F149C44B,
	NameValueCollection_GetAsOneString_m6B44F208B557CE92924A116A67838CB4C259B844,
	NameValueCollection_Add_m68C266151774CD1FC057C9349BAE09BDB15A26A7,
	NameValueCollection_Get_m786B25D45B38D7051C696104946F3F0A2DF78106,
	NameValueCollection_get_Item_mE3ED755803E67BC852123C70EEF8D53774FA635F,
	NameValueCollection_Get_m2C55CAA38BF0F6677AE8F44C22C289A5B59B4B20,
	NameValueCollection_GetKey_m80CFEB6D61F9C3B9D03D7B25457D8B122C15170F,
	NameValueCollection__ctor_mF894B0B49D20CE92F0CE3BBF8ED67F48E61CDB6A,
	StringCollection_get_Item_m4C46EE5894C7A604638AE2ABF4530C201B1FB88E,
	StringCollection_set_Item_mA1AFC86270CD52B6F5017555AD22CF7710D6B425,
	StringCollection_get_Count_mC6D18D20EEE559A67C72BD37ECA9C0FB93283F3F,
	StringCollection_System_Collections_IList_get_IsReadOnly_m4558520882327B88C80FE911E956E0F082872DED,
	StringCollection_System_Collections_IList_get_IsFixedSize_m821D69C64C5E4DE6841D4CC95FFC330E63F4D672,
	StringCollection_Add_m03BBFAA1D11499DA68474A73F5DB55460F0D6DAF,
	StringCollection_Clear_m2B917BE0CCA6344B1FD65474C53CE512F0476435,
	StringCollection_Contains_mB9DCBDDFD850E68D6080D2D7657F399BA6002B10,
	StringCollection_CopyTo_mCD7A68178A8F87AC0628B2E92CE466A27512CB05,
	StringCollection_IndexOf_m85C4CA19319F830D14472258D9A3DFD31F0F6BDF,
	StringCollection_Insert_mB3044A3F3B5C3B364DCEED9058895D978A4410CE,
	StringCollection_Remove_mC6996622434EE5F503A70F425F0DE6E8D60399DC,
	StringCollection_RemoveAt_m4D0E6E6AA354A0AFFB76AEB56B4CFF24717BB53A,
	StringCollection_get_SyncRoot_mC1F8C1DA3C5ED059D89BD483FB1A4A0A31053F86,
	StringCollection_System_Collections_IList_get_Item_m749070FBB55E7C9C0C21FE4C1013D154D4635BB2,
	StringCollection_System_Collections_IList_set_Item_m176A74C64AA22AFEA5808E4C81AA3FB46B0F9F5E,
	StringCollection_System_Collections_IList_Add_m4A71788F0E3068533B66D918B4F29762980052B5,
	StringCollection_System_Collections_IList_Contains_mEB0510524C7FD5886FFF9A0F710CFC04BACAFEF7,
	StringCollection_System_Collections_IList_IndexOf_mFF76DA01812E585BDD7B25ED4453C22BE0FE9A25,
	StringCollection_System_Collections_IList_Insert_m3B0EB6DA5B02CCA0AB270A329B83152592E6D8B1,
	StringCollection_System_Collections_IList_Remove_m042058DA970F09FC6A0465F09F3082FB2B71BA45,
	StringCollection_System_Collections_ICollection_CopyTo_m32503816BE4347138894130BD0AD6488643D6AB9,
	StringCollection_System_Collections_IEnumerable_GetEnumerator_mF4227CFA7634BA8BFF9F7D25079538A77BD060E1,
	StringCollection__ctor_m3EF93D74FEC69C5A0CD7A502539B2CEDD8D02954,
	NameObjectCollectionBase__ctor_m6E0DCD48B817A59D0156390A09CD50BE07BF145D,
	NameObjectCollectionBase__ctor_mB5F581BB65F493F603EE57E1815389E1406D5B61,
	NameObjectCollectionBase__ctor_m6B980F9EB155B69FB4C50AD38C2C0C104FE2F261,
	NameObjectCollectionBase__ctor_m113E50DAB246F05DB6302516E683D458250672DB,
	NameObjectCollectionBase__ctor_m7E8E8B56138D44BECF32865A11DAF942AE2BA4DA,
	NameObjectCollectionBase_GetObjectData_m0675A0B0EC79E1DAD6E307764C899EC5E552B32A,
	NameObjectCollectionBase_OnDeserialization_mFF0F15B0E4DB6D9B6043E2B338160B321889C4B6,
	NameObjectCollectionBase_Reset_m107877B3D64D818F9D4E0936DA9627C4D94DF6DC,
	NameObjectCollectionBase_Reset_mFF34A1D69D7F9BECD9948E748769623EFA50C0D3,
	NameObjectCollectionBase_FindEntry_m1BC4B6ACCE244B544B216D16DB53A769E670876F,
	NameObjectCollectionBase_get_IsReadOnly_m1B1072E33B3B42507F822EBB61F75A804A1D503A,
	NameObjectCollectionBase_BaseAdd_m1A8E236F037A35B3FBC7435A120B2E66EE7F563E,
	NameObjectCollectionBase_BaseGet_mA6FA3CE608E74CF1E4C71227EA95F15C0D2BB154,
	NameObjectCollectionBase_BaseGet_mE892E8066502D48BEA2E9760D2BE31FB40B8DEAA,
	NameObjectCollectionBase_BaseGetKey_m4435B786286C5946CFA38206FFE20E33F32A78A6,
	NameObjectCollectionBase_GetEnumerator_mDEEC7FF01AEF8FB0739964513BAF10C4C3F254BC,
	NameObjectCollectionBase_get_Count_m766C6702F64CD224AB123566BAACB2A81FCBF5F0,
	NameObjectCollectionBase_System_Collections_ICollection_CopyTo_mF47F5F43F47C553B071EB7A6AA4A352D95B02E01,
	NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_mAC6E9FEF3C7C442481772E568C4FBF552B908624,
	NameObjectCollectionBase__cctor_m4D457522AA1259C7AB96A98558FC6B0C38CEEAED,
	NameObjectEntry__ctor_m36EA15EE18936FA78B2E4E29C63311E03221F513,
	NameObjectKeysEnumerator__ctor_mF3026FAD0A80AADFBC11888DCB0FDF7C2DD71801,
	NameObjectKeysEnumerator_MoveNext_m8800474BD1E5F24E4B88D1E33ABA793CB05DD540,
	NameObjectKeysEnumerator_Reset_m548902A21282FB0766D833231BE700069DA316A5,
	NameObjectKeysEnumerator_get_Current_m23AC7A2525995AC67EF4002996653748F33F6096,
	CompatibleComparer__ctor_m9F601617145FDFCA25983415C4427C47E5C170A7,
	CompatibleComparer_Equals_m09CF7F036688C2E00C8BF5618C0528BA8D8C40D5,
	CompatibleComparer_GetHashCode_mE88BB0E4CC1A615946A43A8DE75B6F1F57B1CDAF,
	CompatibleComparer_get_Comparer_mFA7D1FF54540B95FED7C58DBCDB7ACC037FD3DF9,
	CompatibleComparer_get_HashCodeProvider_m1085836D796353C24686CF56026EB89D783BBFC4,
	CompatibleComparer_get_DefaultComparer_m9CC25AF4BD1553D2B2C51FDE9AC8110BABB5C5D5,
	CompatibleComparer_get_DefaultHashCodeProvider_m292C48BD6A5A7065925BAF2BFDEBF7217516DC50,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BitHelper__ctor_mD4A0F4FB490C57EA8B7C904941BC72AD14697A08,
	BitHelper__ctor_m67B3260E6A92B90D770D8CF043A848A477DC4595,
	BitHelper_MarkBit_m337CC205369FD7A7E1A199B7210DFFC98AB6C1AA,
	BitHelper_IsMarked_mB9298BD21F493D475C1D71EEE439D99611B4D01C,
	BitHelper_ToIntArrayLength_m6F27FE06169E9B8829561FC41BB8C81F6A9D0F41,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InvalidDataException__ctor_mE85BD9741C8A2C6D03128821898A587169DA0C41,
	InvalidDataException__ctor_m514879053D89E3C8229A01AA8970237FA5409532,
	InvalidDataException__ctor_m076999CA4741F42158DAC11C78DDA17F00FCDE2B,
	GZipStream__ctor_m25053634B041374B528EB0F01612B072A9AF2E05,
	GZipStream__ctor_mFF3ECD9074856457C500AC6B4F80D833CCE487CF,
	GZipStream_get_CanRead_mFF4A941C8BD1D9C3A249A084C4335EFF8FAD47BB,
	GZipStream_get_CanWrite_m0A3EC64CE819F978DE34A8477FAAA82889AE7248,
	GZipStream_get_CanSeek_m530B997FC6D2144F7B73E708903E6E9F39E304DD,
	GZipStream_get_Length_mCBA17CDD8925825AC2EB07E04CDE391B8E46C583,
	GZipStream_get_Position_m934BD5709034CAAEE05A6BCF382D1FEB0314C420,
	GZipStream_set_Position_m7FEEE9F3845C130340DC5E1DF984ACB2CD53E957,
	GZipStream_Flush_m8D38670B3BE0472E83AA60F783A8EF3E1C1F01A0,
	GZipStream_Seek_m72909F60549EAB072B86484A1E80BA15D4352E66,
	GZipStream_SetLength_m5DCF13005B2AB45961D14230670698CBAF93D0C1,
	GZipStream_ReadByte_m486ACD72754C08B02CF1B90A71A744EE92F4D42C,
	GZipStream_BeginRead_mDD211249BF8242D28889645A05453C3D89C6B0F1,
	GZipStream_EndRead_m8245FEE8D2B21A169DD1A40A67B5D398DD6005D2,
	GZipStream_Read_m41EC5D087FD227D05C8435E8704C9F0F6A7E2F9D,
	GZipStream_Read_mBC83A9A584B5A2DC2B08B044E96C0E8AF33723D5,
	GZipStream_BeginWrite_mB80B01F99925CF4A6B201E77B54E7656A40474B8,
	GZipStream_EndWrite_m7FC15F1CD67B5C6977FEB16E630545AD6E020C91,
	GZipStream_Write_mCA30092CA9BEFD7425587337A5C4451866E1EB34,
	GZipStream_Write_mF25D52D7EC30B244830E14477D047FC5028E51D3,
	GZipStream_Dispose_mD369BCB8951329CEE518D159BB1D16BBC0C709E3,
	GZipStream_ReadAsync_mACECFB99609D4696156F98AE738B2038E6663471,
	GZipStream_ReadAsync_mCFF5156AFDFF16B472A323F2D1BA86BFE13C7358,
	GZipStream_WriteAsync_m2CB67E3DB87DFC01588B604C8E2D60EE1AE27EF7,
	GZipStream_WriteAsync_m43C1DEA3F232AFD6036CB0637831887E93488000,
	GZipStream_CheckDeflateStream_m691CE26A65C042DAFBA84076906015A8B3CBE264,
	GZipStream_ThrowStreamClosedException_m7B905CA279182188E2191C564DC9415234005D5F,
	DeflateStream__ctor_mFA0036CA1C1411FBED2DDFB2B563635F4755078C,
	DeflateStream__ctor_m67AEE7C38BCFD323CD25D3FB36674E6A5010CD36,
	DeflateStream_Finalize_mAFF7BCEF197A6549D13572A0AFD326CD995EA7AC,
	DeflateStream_Dispose_m85096A7176C94248E02CE7E9DA78338C45B2F746,
	DeflateStream_ReadInternal_m4FCD4EB1534E24E85A9E5D933D6AF6EABF5F223C,
	DeflateStream_ReadAsyncMemory_m1FC9EB7B2284773AA527913018FEA9EB82965327,
	DeflateStream_ReadCore_m58E605C082DCF3455C4D56EBEBB8F2ABECCC9B40,
	DeflateStream_Read_m46316C432282671BF001324B75881E50CC2B7FA3,
	DeflateStream_WriteInternal_m5BD64441B31FC3D10F20A273FF45380EAAB97845,
	DeflateStream_WriteAsyncMemory_mDEFCEC8FAA595B11419D022F351AD825C01AFCE6,
	DeflateStream_WriteCore_mC6ADF328D0A66CAFEEC9B23F89A22C223DFF3730,
	DeflateStream_Write_mA09DDD17D3B00089F808F9C7C587226666DB141A,
	DeflateStream_Flush_mE12E917486F3EA41FE453945AA5B5617F80E6D0C,
	DeflateStream_BeginRead_m5FD78CDEDE42221D07255D38C4E8BBB3FFAD0235,
	DeflateStream_BeginWrite_m00FB9CAB52A95FA92911DBB50200D8AB81F4F4BA,
	DeflateStream_EndRead_m3A49F7660DAE5121AC48BD7409580CD46E3D8D9B,
	DeflateStream_EndWrite_m304251B2932DD464A16C7306E862BF86BA516A6A,
	DeflateStream_Seek_mCABEFA32513E0858BC7A98F05AAA217A09C65B16,
	DeflateStream_SetLength_mC18B3CCD1225B3C3A15EF54830C44236670F63F3,
	DeflateStream_get_CanRead_mDDA3C3CEE73CFCD62DE14F5DE9F27C7A5615976E,
	DeflateStream_get_CanSeek_m93DEADA4FB38CBBAB266F8B7DBAFE508AEC222E4,
	DeflateStream_get_CanWrite_m95EB501342F11B287A529DE15089097EDF7C215F,
	DeflateStream_get_Length_m8C7CE98029153C5065D28496ACE3F99CB644897C,
	DeflateStream_get_Position_mA6B4FE66DBE953C5504A3561AD75FCC87718F376,
	DeflateStream_set_Position_m9BA570C4BF91C325EFE8E6BEAF40E6AB54BCC885,
	ReadMethod__ctor_mA65629D1C063B5E57095F952077323B5CE96C396,
	ReadMethod_Invoke_m0FA726BC164CDBF0ACA713BDD0EBA3187D0562F8,
	ReadMethod_BeginInvoke_mE465A26187BE3F1829ECA0C02AFAEFCA6D2F6EEF,
	ReadMethod_EndInvoke_mBCA4D1483C0504BF239C2305B7201E1EC72ED6F7,
	WriteMethod__ctor_mBE4CCCE38227B2D866CB539879255AD8B0C2B51A,
	WriteMethod_Invoke_m8754DB7443BBDB1F7553BFA43DBBD024C0E268F1,
	WriteMethod_BeginInvoke_m4A29F5255900687782F0A92A0A4D043AEA2C982C,
	WriteMethod_EndInvoke_mFDA332678A1FD46BAE42F3A0DF2073FD9CC6628C,
	DeflateStreamNative__ctor_mCDA4F9E1FF67EFCE84EBB8889DDDD30F4A12BB0E,
	DeflateStreamNative_Create_m628D0A7F7EE62339C0D867FBF4319093C82018A7,
	DeflateStreamNative_Finalize_mE02A29EE58E329CFC439284070FF87A31B18C701,
	DeflateStreamNative_Dispose_m4FB7DA2D3E50360ECCE3B81A77CF25B5C6722C8E,
	DeflateStreamNative_Flush_m60D8D1C3DEBB720E402625ECFA9A0CFCA69E8F6F,
	DeflateStreamNative_ReadZStream_m64072AF17150906ED32D871778DC5EF313FADD9F,
	DeflateStreamNative_WriteZStream_m4B066C7A2E031A2CA6EE0FE876A9611242630F18,
	DeflateStreamNative_UnmanagedRead_m79E9628F6FF91A3995491997242DD83046C9B704,
	DeflateStreamNative_UnmanagedRead_mC17A13844F4560E954F816037B0A5C04FFEDECB1,
	DeflateStreamNative_UnmanagedWrite_m509AE153ECB916CDF755432335FBAB2B51CA1486,
	DeflateStreamNative_UnmanagedWrite_m73ADBE62E8371FFDFFB380F568A8510E8644B1BD,
	DeflateStreamNative_CheckResult_m9354BDD0FA5B71935FFD628698412B5380329C47,
	DeflateStreamNative_CreateZStream_m5075DD90424301C1C82A2142AA530FDC6F345006,
	DeflateStreamNative_CloseZStream_mEA426722FFB221614DC4AFEC80BBB4382B375187,
	DeflateStreamNative_Flush_mAC7D59B136EB2C8B1512123F21B91976CDE33D5F,
	DeflateStreamNative_ReadZStream_mC4E36C2FDE3E7850B154FE6A09825D7B055E1519,
	DeflateStreamNative_WriteZStream_m95704830E739FD66C207ED91323852F7E0FCC35B,
	UnmanagedReadOrWrite__ctor_mF122921041A4F277205DD17182C31BBE9B1DEEF6,
	UnmanagedReadOrWrite_Invoke_m595EDC2E3BDD8B54CA699C245FAA1003994A5B22,
	SafeDeflateStreamHandle_get_IsInvalid_m8220E0B23A6DE1C8171ADDD78B30FBC346F1D1D1,
	SafeDeflateStreamHandle__ctor_mBDF78EC52524A938914F593AF5C198C9F7632E41,
	SafeDeflateStreamHandle_ReleaseHandle_m8A0A695A4A1C2F46C8BFBE3BD1526BCDC6066704,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_mC09A19935970846B38570D03608F0E5F25E2020C,
	BypassElementCollection__ctor_m344EC8CD2EACA36E1213C5914037BB133E87FE54,
	ConnectionManagementElementCollection__ctor_m4698342A715032A762FA04B2745BFCA46263BCB3,
	ConnectionManagementSection__ctor_m2FA1D29F887BD6498FBEF692BBC0AA727E38E802,
	ConnectionManagementSection_get_Properties_mFCE0F9418E9E8CD2E1301815CC4D548261F58E95,
	DefaultProxySection__ctor_m962B68CEFCDE7DFB7646EF16297CA4BE0097284D,
	DefaultProxySection_get_Properties_m5186F1A2734EB96F065C7BC14A215D17F8325418,
	DefaultProxySection_Reset_m0B9A1155840E7C55FC24AEDB23FB17A2815EEB77,
	ProxyElement__ctor_mC1BDF426F97F4F84CB5A34B423F59EC8748EBD70,
	ProxyElement_get_Properties_mBFE6E283FE7E9329B586ACAADC8F2E047C7ABB2A,
	HttpWebRequestElement__ctor_mB9DDAC7FF431D4AF4CAFCBC96E1A3572A1F83717,
	HttpWebRequestElement_get_Properties_mAA0F11EA73B5397C50809EAC679F2C564F9EC7CA,
	Ipv6Element__ctor_m869AC261E0994A8B3D83CEE25C6CB99E6A5EA4C1,
	Ipv6Element_get_Properties_m382D5B5B098EFF19C2A4AC1889CD1C8EAB918322,
	NetSectionGroup__ctor_mCD2F0DC2B9F0C6C346BC91BD8750C7111A9528AC,
	SettingsSection__ctor_mBD94E7E098221DC09775A3AE1713930F6820D242,
	SettingsSection_get_Properties_mA774C9C884F4AD5DEC2BBE31DF7E35CAC71EBF8F,
	PerformanceCountersElement__ctor_m9537195CBC7DC607F7253D06E7B6B1090456E68F,
	PerformanceCountersElement_get_Properties_m7C16FBC3E915E8384FB4409209628737B1537C9B,
	ServicePointManagerElement__ctor_m0F1CF21589CA283DA734A472930A9665698B2D36,
	ServicePointManagerElement_get_Properties_m70469523AAD1A2C2E9D4F90DDA1BA619FCE68B58,
	SocketElement__ctor_m4FB4C851F1BB20CED74C206EFB1BA9DDFBE024D2,
	SocketElement_get_Properties_m8AF94DEEDA0EB4092F59FBDC361592E1B9EBDEC3,
	WebProxyScriptElement__ctor_mB613D71DE569317E40726CD8DB9A6ABA0A302821,
	WebProxyScriptElement_get_Properties_mF5188225C0C1D64BC136465BB4B0E962FFE9240B,
	WebRequestModulesSection__ctor_mF25CD80A5111A4F87B7FBF7663A3442470B97C0E,
	WebRequestModulesSection_get_Properties_mBDBDED569E677717FB617CD36B94AE34BF4208F6,
	WebRequestModuleElementCollection__ctor_m62C156D78336B2E37EA6F10D477FDEAE4161F99E,
	DiagnosticsConfigurationHandler__ctor_mDFB0000E6218840F66D21BB6C9BF3F647CCBC5A3,
	DiagnosticsConfigurationHandler_Create_mF21B1CC68B87DE078690B2B493FCDC5F728CBAAB,
	ThrowStub_ThrowNotSupportedException_m0E6C3891D0501FEF5BF1A72B7AA4D6310CE082DD,
};
extern void U3CStartOperationU3Ed__23_MoveNext_mDE591A3806D9523FC8D2E26ED510CEE7963F8FF2_AdjustorThunk (void);
extern void U3CStartOperationU3Ed__23_SetStateMachine_mA0C8C029457FAAE036C6098FDC66C98574D0BD03_AdjustorThunk (void);
extern void U3CProcessOperationU3Ed__24_MoveNext_mA73F0FD80CE65D2E3E12390E49E3F34CFF47E98B_AdjustorThunk (void);
extern void U3CProcessOperationU3Ed__24_SetStateMachine_mA9912D191EE4A99536A61DEED059DC89B765A823_AdjustorThunk (void);
extern void U3CInnerReadU3Ed__25_MoveNext_mC4567E370C71DA9DA0F704C570308D106D4DDB2C_AdjustorThunk (void);
extern void U3CInnerReadU3Ed__25_SetStateMachine_m15DB2E97371A46CBCCD53DCAE7A4FE6F5FF84DCE_AdjustorThunk (void);
extern void U3CProcessAuthenticationU3Ed__48_MoveNext_m982934120035A46C0FB96F7DF55F7FA0822B4940_AdjustorThunk (void);
extern void U3CProcessAuthenticationU3Ed__48_SetStateMachine_mC87BA3C30B7501EF0D0435E4C2C08EE79238576F_AdjustorThunk (void);
extern void U3CStartOperationU3Ed__57_MoveNext_m2CD8CDF02548ACC068670F3F670590EEC77F7853_AdjustorThunk (void);
extern void U3CStartOperationU3Ed__57_SetStateMachine_m1EC071633621977A0623BF79A53028A2A766BED9_AdjustorThunk (void);
extern void U3CInnerReadU3Ed__66_MoveNext_m5DFDC2139602BBC2F86D14BDA64B4A49ECDD90EB_AdjustorThunk (void);
extern void U3CInnerReadU3Ed__66_SetStateMachine_mBA42C0B565B5D3F43D109D2A8CC66EE8CE9BC920_AdjustorThunk (void);
extern void U3CInnerWriteU3Ed__67_MoveNext_mDF6F0FEA0FCAE97EEB2E5A2546446C00674CCEA3_AdjustorThunk (void);
extern void U3CInnerWriteU3Ed__67_SetStateMachine_m7448CF503BCD3FEC38241FB22DBA3AB4B91FF5B1_AdjustorThunk (void);
extern void CertificateData__ctor_m623A235F902E7262CB9DA2CEB1BF5D3E64A6D23F_AdjustorThunk (void);
extern void CertificateData_GetNameInfo_m8DC5068DE35A32AC641DCBA45C409D05C4CE9D72_AdjustorThunk (void);
extern void CachedCodeEntryKey__ctor_mE505E40C97820B3FD140F6212F0B4ECF8745585F_AdjustorThunk (void);
extern void CachedCodeEntryKey_Equals_m59F40708B97179EA5C9019EA6A958D7AB8BC8CA2_AdjustorThunk (void);
extern void CachedCodeEntryKey_Equals_m55F9ED97CB9AF357034A75C2FDB8498FC55D3AF0_AdjustorThunk (void);
extern void CachedCodeEntryKey_GetHashCode_m253B53C100F840B987A487020F02184019D776DE_AdjustorThunk (void);
extern void LowerCaseMapping__ctor_m06D0A21D5D72E39535F82076EF046F98C62D7D5A_AdjustorThunk (void);
extern void SingleRange__ctor_m14FD490AFB119E412298554D31FADDCB9465511E_AdjustorThunk (void);
extern void RegexPrefix__ctor_m05CD44F32CB0B5F8B2C080EAB94886F9743E86A4_AdjustorThunk (void);
extern void RegexPrefix_get_CaseInsensitive_m4DAE2D9E91916BFFAB928F7DCAA20A5ABD4560E6_AdjustorThunk (void);
extern void RegexPrefix_get_Prefix_m844D02262C0720B0DC488AD1541713AD37BC518E_AdjustorThunk (void);
extern void X509ChainStatus__ctor_m5AD96E11604F8DB96673677AC2FFF68EBA550859_AdjustorThunk (void);
extern void X509ChainStatus_get_Status_m58AB369B6D5CDF2436644C1936953058FBBDEF99_AdjustorThunk (void);
extern void X509ChainStatus_set_Status_m55D0302200912F7B74F7805470551EB33C28246D_AdjustorThunk (void);
extern void X509ChainStatus_set_StatusInformation_m0A4B88F2DEF120DB1D38E3EFF86170C9D272DB3F_AdjustorThunk (void);
extern void AuthorizationState__ctor_m6DDA04B33B8A520B93FBDF8AC4BBA3B7CCC03CBE_AdjustorThunk (void);
extern void AuthorizationState_ToString_m6426C6029B83D041B9F1B9871A4541F70A2EFC00_AdjustorThunk (void);
extern void IPPacketInformation_Equals_mF2E8CBE60B68F20394E51286666D25B54F8EC2B7_AdjustorThunk (void);
extern void IPPacketInformation_GetHashCode_mF1429DDA203ED791CB45EDB87A9006E58BB12A81_AdjustorThunk (void);
extern void UdpReceiveResult__ctor_mC2A34DE9634F5B6F2BF61EAA1D8768419B085F26_AdjustorThunk (void);
extern void UdpReceiveResult_get_Buffer_m7C3092CBE979A308878E5B037BF26FDD29DAB8CC_AdjustorThunk (void);
extern void UdpReceiveResult_get_RemoteEndPoint_m79E46C121B64109D4E12D3168C4712DF9FE945E9_AdjustorThunk (void);
extern void UdpReceiveResult_GetHashCode_mC25E5C4B721F0FFD9AC58CE2F3788D404F288EB0_AdjustorThunk (void);
extern void UdpReceiveResult_Equals_mE7B77E1DF28B1084E214343B9B5B881CDDED95DA_AdjustorThunk (void);
extern void UdpReceiveResult_Equals_mDD344E622B6536C7297AE2429C8FAD2FEEC1132E_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[39] = 
{
	{ 0x060000BE, U3CStartOperationU3Ed__23_MoveNext_mDE591A3806D9523FC8D2E26ED510CEE7963F8FF2_AdjustorThunk },
	{ 0x060000BF, U3CStartOperationU3Ed__23_SetStateMachine_mA0C8C029457FAAE036C6098FDC66C98574D0BD03_AdjustorThunk },
	{ 0x060000C0, U3CProcessOperationU3Ed__24_MoveNext_mA73F0FD80CE65D2E3E12390E49E3F34CFF47E98B_AdjustorThunk },
	{ 0x060000C1, U3CProcessOperationU3Ed__24_SetStateMachine_mA9912D191EE4A99536A61DEED059DC89B765A823_AdjustorThunk },
	{ 0x060000C2, U3CInnerReadU3Ed__25_MoveNext_mC4567E370C71DA9DA0F704C570308D106D4DDB2C_AdjustorThunk },
	{ 0x060000C3, U3CInnerReadU3Ed__25_SetStateMachine_m15DB2E97371A46CBCCD53DCAE7A4FE6F5FF84DCE_AdjustorThunk },
	{ 0x06000106, U3CProcessAuthenticationU3Ed__48_MoveNext_m982934120035A46C0FB96F7DF55F7FA0822B4940_AdjustorThunk },
	{ 0x06000107, U3CProcessAuthenticationU3Ed__48_SetStateMachine_mC87BA3C30B7501EF0D0435E4C2C08EE79238576F_AdjustorThunk },
	{ 0x06000108, U3CStartOperationU3Ed__57_MoveNext_m2CD8CDF02548ACC068670F3F670590EEC77F7853_AdjustorThunk },
	{ 0x06000109, U3CStartOperationU3Ed__57_SetStateMachine_m1EC071633621977A0623BF79A53028A2A766BED9_AdjustorThunk },
	{ 0x0600010C, U3CInnerReadU3Ed__66_MoveNext_m5DFDC2139602BBC2F86D14BDA64B4A49ECDD90EB_AdjustorThunk },
	{ 0x0600010D, U3CInnerReadU3Ed__66_SetStateMachine_mBA42C0B565B5D3F43D109D2A8CC66EE8CE9BC920_AdjustorThunk },
	{ 0x0600010E, U3CInnerWriteU3Ed__67_MoveNext_mDF6F0FEA0FCAE97EEB2E5A2546446C00674CCEA3_AdjustorThunk },
	{ 0x0600010F, U3CInnerWriteU3Ed__67_SetStateMachine_m7448CF503BCD3FEC38241FB22DBA3AB4B91FF5B1_AdjustorThunk },
	{ 0x0600032C, CertificateData__ctor_m623A235F902E7262CB9DA2CEB1BF5D3E64A6D23F_AdjustorThunk },
	{ 0x0600032D, CertificateData_GetNameInfo_m8DC5068DE35A32AC641DCBA45C409D05C4CE9D72_AdjustorThunk },
	{ 0x0600047F, CachedCodeEntryKey__ctor_mE505E40C97820B3FD140F6212F0B4ECF8745585F_AdjustorThunk },
	{ 0x06000480, CachedCodeEntryKey_Equals_m59F40708B97179EA5C9019EA6A958D7AB8BC8CA2_AdjustorThunk },
	{ 0x06000481, CachedCodeEntryKey_Equals_m55F9ED97CB9AF357034A75C2FDB8498FC55D3AF0_AdjustorThunk },
	{ 0x06000483, CachedCodeEntryKey_GetHashCode_m253B53C100F840B987A487020F02184019D776DE_AdjustorThunk },
	{ 0x060004B0, LowerCaseMapping__ctor_m06D0A21D5D72E39535F82076EF046F98C62D7D5A_AdjustorThunk },
	{ 0x060004B4, SingleRange__ctor_m14FD490AFB119E412298554D31FADDCB9465511E_AdjustorThunk },
	{ 0x0600055C, RegexPrefix__ctor_m05CD44F32CB0B5F8B2C080EAB94886F9743E86A4_AdjustorThunk },
	{ 0x0600055D, RegexPrefix_get_CaseInsensitive_m4DAE2D9E91916BFFAB928F7DCAA20A5ABD4560E6_AdjustorThunk },
	{ 0x0600055F, RegexPrefix_get_Prefix_m844D02262C0720B0DC488AD1541713AD37BC518E_AdjustorThunk },
	{ 0x060006DF, X509ChainStatus__ctor_m5AD96E11604F8DB96673677AC2FFF68EBA550859_AdjustorThunk },
	{ 0x060006E0, X509ChainStatus_get_Status_m58AB369B6D5CDF2436644C1936953058FBBDEF99_AdjustorThunk },
	{ 0x060006E1, X509ChainStatus_set_Status_m55D0302200912F7B74F7805470551EB33C28246D_AdjustorThunk },
	{ 0x060006E2, X509ChainStatus_set_StatusInformation_m0A4B88F2DEF120DB1D38E3EFF86170C9D272DB3F_AdjustorThunk },
	{ 0x06000836, AuthorizationState__ctor_m6DDA04B33B8A520B93FBDF8AC4BBA3B7CCC03CBE_AdjustorThunk },
	{ 0x06000837, AuthorizationState_ToString_m6426C6029B83D041B9F1B9871A4541F70A2EFC00_AdjustorThunk },
	{ 0x06000968, IPPacketInformation_Equals_mF2E8CBE60B68F20394E51286666D25B54F8EC2B7_AdjustorThunk },
	{ 0x06000969, IPPacketInformation_GetHashCode_mF1429DDA203ED791CB45EDB87A9006E58BB12A81_AdjustorThunk },
	{ 0x060009A0, UdpReceiveResult__ctor_mC2A34DE9634F5B6F2BF61EAA1D8768419B085F26_AdjustorThunk },
	{ 0x060009A1, UdpReceiveResult_get_Buffer_m7C3092CBE979A308878E5B037BF26FDD29DAB8CC_AdjustorThunk },
	{ 0x060009A2, UdpReceiveResult_get_RemoteEndPoint_m79E46C121B64109D4E12D3168C4712DF9FE945E9_AdjustorThunk },
	{ 0x060009A3, UdpReceiveResult_GetHashCode_mC25E5C4B721F0FFD9AC58CE2F3788D404F288EB0_AdjustorThunk },
	{ 0x060009A4, UdpReceiveResult_Equals_mE7B77E1DF28B1084E214343B9B5B881CDDED95DA_AdjustorThunk },
	{ 0x060009A5, UdpReceiveResult_Equals_mDD344E622B6536C7297AE2429C8FAD2FEEC1132E_AdjustorThunk },
};
static const int32_t s_InvokerIndices[3340] = 
{
	12322,
	12362,
	6765,
	2381,
	935,
	935,
	2381,
	2381,
	6908,
	12362,
	12322,
	12362,
	6765,
	6765,
	6765,
	6908,
	12362,
	12322,
	4903,
	1366,
	4903,
	6908,
	0,
	0,
	0,
	9704,
	11219,
	4903,
	1366,
	6666,
	4648,
	6908,
	12362,
	5521,
	9002,
	9002,
	9135,
	8364,
	12317,
	12297,
	12322,
	3059,
	953,
	3059,
	908,
	3059,
	908,
	3059,
	963,
	3059,
	51,
	3059,
	1400,
	6908,
	3059,
	7033,
	3059,
	2477,
	3059,
	3269,
	3059,
	331,
	3059,
	331,
	3059,
	5406,
	3059,
	910,
	3059,
	3270,
	3059,
	1733,
	3059,
	3548,
	3059,
	1456,
	3059,
	965,
	3059,
	5406,
	3059,
	256,
	3059,
	163,
	3059,
	333,
	3059,
	332,
	3059,
	1457,
	3059,
	974,
	3059,
	974,
	3059,
	974,
	3059,
	965,
	3059,
	2430,
	3059,
	2430,
	3059,
	2430,
	3059,
	908,
	3059,
	908,
	3059,
	2469,
	3059,
	5406,
	3059,
	1442,
	3068,
	8292,
	6666,
	6765,
	6908,
	1184,
	1184,
	6908,
	6666,
	6908,
	5418,
	6908,
	6666,
	6908,
	8138,
	1331,
	8138,
	1331,
	8824,
	2435,
	7117,
	105,
	11382,
	11382,
	11099,
	9480,
	11099,
	11099,
	6765,
	6722,
	6666,
	6666,
	6666,
	6666,
	6727,
	933,
	8215,
	94,
	6908,
	545,
	4903,
	3253,
	6666,
	7034,
	6765,
	5485,
	6765,
	3982,
	6908,
	5418,
	6727,
	6727,
	1587,
	6765,
	5485,
	6908,
	5485,
	1587,
	6727,
	6765,
	5485,
	5521,
	6765,
	6666,
	6765,
	6727,
	5485,
	3041,
	5485,
	6908,
	4894,
	4894,
	4894,
	0,
	6765,
	6908,
	5521,
	6908,
	5521,
	6908,
	5521,
	3041,
	4618,
	6765,
	6727,
	5485,
	544,
	6765,
	544,
	4618,
	544,
	4618,
	8753,
	568,
	11219,
	8186,
	6765,
	345,
	923,
	245,
	244,
	1249,
	6908,
	631,
	545,
	6765,
	6765,
	6765,
	6765,
	5521,
	2487,
	11219,
	9704,
	12322,
	12322,
	4903,
	1096,
	1047,
	1340,
	0,
	1318,
	1587,
	926,
	926,
	1347,
	785,
	330,
	1245,
	344,
	1338,
	2365,
	2155,
	3536,
	3536,
	6666,
	5418,
	6908,
	2344,
	5486,
	6666,
	6666,
	6666,
	6728,
	6728,
	5486,
	12362,
	6908,
	6908,
	5521,
	6908,
	5521,
	6908,
	6727,
	6908,
	5521,
	6908,
	5521,
	3068,
	6765,
	6765,
	6765,
	0,
	6666,
	6765,
	6765,
	6666,
	6727,
	6765,
	2469,
	0,
	0,
	0,
	6765,
	5521,
	0,
	0,
	0,
	0,
	0,
	0,
	1922,
	4903,
	4903,
	0,
	6908,
	5418,
	6908,
	0,
	0,
	6908,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	6765,
	6908,
	6765,
	6666,
	6908,
	6666,
	5485,
	5485,
	6727,
	5485,
	6765,
	5521,
	6666,
	5418,
	6765,
	5521,
	6765,
	5521,
	6765,
	6666,
	6908,
	6666,
	5485,
	5485,
	6727,
	5485,
	6666,
	5418,
	6765,
	5521,
	6765,
	5521,
	6765,
	5521,
	12322,
	12362,
	9693,
	12362,
	12362,
	12362,
	12297,
	12322,
	12322,
	12362,
	6765,
	5418,
	6908,
	6908,
	12322,
	12362,
	10965,
	11219,
	11219,
	6908,
	631,
	6908,
	938,
	5521,
	6765,
	11513,
	5488,
	6666,
	12317,
	9584,
	6908,
	6765,
	0,
	0,
	0,
	0,
	5521,
	12317,
	7774,
	2343,
	1308,
	7997,
	2166,
	8635,
	8682,
	6908,
	3059,
	746,
	3059,
	1311,
	3059,
	1329,
	3068,
	11219,
	4648,
	4648,
	6727,
	6908,
	5521,
	11216,
	6666,
	6727,
	6908,
	6908,
	6908,
	6908,
	11087,
	6908,
	1184,
	1184,
	6908,
	6908,
	6666,
	0,
	5418,
	785,
	1245,
	6908,
	6908,
	6666,
	6765,
	12362,
	9569,
	9038,
	11088,
	12362,
	11216,
	9569,
	11088,
	6908,
	5485,
	5521,
	3068,
	12317,
	11513,
	11138,
	7992,
	8638,
	6765,
	5521,
	4893,
	6765,
	11219,
	5488,
	6666,
	5521,
	6765,
	6666,
	6908,
	4903,
	2489,
	2799,
	5521,
	11513,
	5488,
	6908,
	5418,
	6908,
	6908,
	2995,
	6666,
	6765,
	11513,
	12317,
	11090,
	9641,
	7993,
	11090,
	11138,
	6908,
	6727,
	4899,
	3068,
	6666,
	6765,
	5488,
	6666,
	6722,
	6765,
	6908,
	6666,
	6666,
	6666,
	6666,
	6727,
	933,
	1366,
	4903,
	1366,
	8751,
	94,
	9420,
	44,
	11088,
	9083,
	11516,
	11516,
	11516,
	9083,
	11219,
	11219,
	11219,
	11219,
	3059,
	4648,
	3059,
	4648,
	3059,
	6727,
	11513,
	11138,
	9587,
	9587,
	9587,
	11090,
	11090,
	11090,
	11513,
	11090,
	9995,
	9996,
	8635,
	8635,
	9585,
	11090,
	11090,
	11138,
	9996,
	9587,
	11138,
	9995,
	11090,
	11219,
	5521,
	6765,
	5521,
	4903,
	4618,
	5521,
	5521,
	5521,
	6727,
	6727,
	6727,
	8636,
	6765,
	6908,
	2165,
	2165,
	6727,
	6727,
	6765,
	5521,
	6765,
	6908,
	6908,
	6908,
	5485,
	6666,
	5488,
	6666,
	3059,
	1312,
	6765,
	12317,
	11090,
	9996,
	9048,
	9996,
	9995,
	9995,
	7995,
	9587,
	7996,
	9996,
	6908,
	5521,
	6765,
	2131,
	8633,
	7996,
	8727,
	3041,
	5521,
	5485,
	5485,
	3041,
	5521,
	5521,
	5521,
	11090,
	6908,
	5488,
	6666,
	3059,
	1309,
	3059,
	745,
	3059,
	4621,
	545,
	4903,
	9420,
	7458,
	8172,
	12362,
	6765,
	5521,
	11138,
	8695,
	11138,
	8635,
	9587,
	11513,
	9585,
	6765,
	9698,
	6765,
	6728,
	4899,
	9591,
	5485,
	5488,
	6666,
	6730,
	6765,
	12317,
	11090,
	9641,
	9587,
	11138,
	11513,
	6908,
	5521,
	6727,
	4899,
	5521,
	6765,
	5488,
	6666,
	6765,
	9641,
	8635,
	9587,
	11513,
	11138,
	9698,
	3056,
	6730,
	3056,
	5521,
	5521,
	6908,
	5488,
	6666,
	6765,
	12317,
	9049,
	11090,
	6908,
	5521,
	5521,
	0,
	8634,
	6908,
	5488,
	6666,
	3059,
	1310,
	3056,
	6908,
	4903,
	6908,
	11120,
	11090,
	9585,
	7995,
	8632,
	7994,
	8685,
	11513,
	6765,
	5521,
	6728,
	6727,
	4618,
	4899,
	4899,
	2368,
	9693,
	2995,
	6666,
	6765,
	12317,
	11138,
	9587,
	11513,
	12322,
	11219,
	6908,
	5521,
	5521,
	4899,
	3056,
	3056,
	6908,
	5488,
	6666,
	6765,
	12317,
	11138,
	9584,
	11138,
	8636,
	9587,
	11090,
	11138,
	11138,
	11513,
	6908,
	11218,
	2772,
	3017,
	6727,
	6765,
	6765,
	3068,
	5521,
	6727,
	6727,
	6765,
	2995,
	6666,
	12362,
	12362,
	11216,
	6765,
	11138,
	11138,
	11090,
	8635,
	9586,
	11513,
	5521,
	6765,
	12322,
	12322,
	9693,
	6666,
	6908,
	5521,
	5441,
	5488,
	6666,
	5521,
	5521,
	3056,
	1610,
	6666,
	6765,
	6765,
	6765,
	6765,
	6765,
	5418,
	6666,
	6765,
	5521,
	6765,
	6765,
	5521,
	3068,
	5521,
	3982,
	5521,
	5521,
	6666,
	6765,
	6765,
	6765,
	5485,
	3982,
	6908,
	5418,
	5521,
	6765,
	4903,
	1366,
	4903,
	8745,
	8745,
	10962,
	8745,
	8745,
	12362,
	12362,
	6908,
	4885,
	4885,
	5521,
	2369,
	11219,
	8747,
	11219,
	5485,
	6908,
	6666,
	6518,
	6908,
	6765,
	6765,
	6765,
	8603,
	7278,
	7906,
	7436,
	8129,
	8176,
	7933,
	10900,
	10954,
	8513,
	8228,
	8173,
	10954,
	7915,
	8513,
	11219,
	6908,
	5521,
	5521,
	5521,
	5485,
	5521,
	6765,
	3982,
	6727,
	5521,
	6765,
	6908,
	2180,
	12362,
	9475,
	7959,
	9476,
	8157,
	6666,
	6666,
	6666,
	6666,
	6892,
	6765,
	6666,
	10965,
	6666,
	1931,
	3982,
	6666,
	6908,
	6890,
	4097,
	4097,
	9484,
	9484,
	6765,
	6908,
	5418,
	5521,
	3056,
	3068,
	1607,
	3068,
	8038,
	11216,
	3094,
	3094,
	3094,
	6765,
	6765,
	6765,
	6765,
	6765,
	6666,
	6666,
	6666,
	6765,
	6666,
	6765,
	10965,
	12322,
	12362,
	6765,
	6727,
	6765,
	6765,
	6765,
	6666,
	6765,
	6765,
	6666,
	6666,
	6765,
	10976,
	10965,
	10976,
	11098,
	6727,
	6765,
	9420,
	9420,
	3982,
	8640,
	6765,
	6727,
	3098,
	5643,
	6908,
	7635,
	6908,
	2371,
	4899,
	2371,
	1350,
	4899,
	6908,
	7645,
	8517,
	8613,
	162,
	18,
	52,
	1041,
	967,
	639,
	1355,
	7353,
	8190,
	11092,
	8752,
	6666,
	10976,
	10976,
	10976,
	10976,
	8713,
	1572,
	1520,
	3982,
	3982,
	3982,
	8536,
	2371,
	11219,
	927,
	1688,
	8171,
	7624,
	4899,
	2371,
	5521,
	12362,
	6908,
	6908,
	6908,
	5521,
	3094,
	3094,
	7112,
	7228,
	7088,
	7087,
	7119,
	9114,
	9824,
	10976,
	10976,
	10976,
	10976,
	12362,
	6765,
	6727,
	6765,
	3038,
	1362,
	1360,
	12297,
	12362,
	6727,
	3946,
	3946,
	3946,
	1879,
	5485,
	11219,
	11219,
	6666,
	6765,
	3038,
	1362,
	1360,
	1587,
	8176,
	7437,
	7437,
	7609,
	7609,
	9418,
	9353,
	8168,
	7609,
	9474,
	9474,
	8176,
	7920,
	3059,
	5521,
	6908,
	3068,
	3068,
	6765,
	6765,
	6765,
	6666,
	5418,
	6666,
	5418,
	0,
	1538,
	6908,
	5521,
	6908,
	9997,
	6908,
	3982,
	1922,
	1922,
	1368,
	938,
	1587,
	6727,
	5485,
	6727,
	5485,
	6765,
	5521,
	6765,
	6765,
	6577,
	6577,
	1098,
	6666,
	12362,
	6908,
	6666,
	6727,
	4899,
	6765,
	6765,
	4899,
	4899,
	6765,
	3056,
	3056,
	4648,
	2799,
	5485,
	4899,
	2799,
	5521,
	6908,
	3982,
	3982,
	4648,
	6908,
	3982,
	4648,
	2799,
	6666,
	5521,
	5485,
	4899,
	2799,
	6908,
	5521,
	6666,
	6765,
	6765,
	6908,
	6908,
	296,
	12322,
	562,
	6765,
	3475,
	6577,
	1525,
	5485,
	5485,
	3946,
	4618,
	4618,
	5485,
	12362,
	6908,
	189,
	6765,
	5521,
	6908,
	2398,
	2398,
	6908,
	9500,
	9500,
	11239,
	9420,
	7948,
	3982,
	1920,
	4903,
	2381,
	8753,
	7628,
	2383,
	935,
	12362,
	11530,
	12351,
	5521,
	3056,
	3094,
	1083,
	6666,
	6765,
	4899,
	6908,
	239,
	6666,
	6666,
	1538,
	3982,
	4234,
	9501,
	6727,
	138,
	1046,
	1920,
	627,
	787,
	6908,
	995,
	6666,
	5418,
	5641,
	5521,
	5521,
	5521,
	3167,
	1046,
	5521,
	5521,
	1678,
	2487,
	2487,
	1464,
	11367,
	10965,
	10965,
	10965,
	10965,
	10965,
	10965,
	10976,
	10976,
	9477,
	8559,
	7467,
	7467,
	7958,
	11219,
	11219,
	9698,
	6727,
	6765,
	5944,
	6908,
	8742,
	12362,
	1142,
	6908,
	2303,
	12362,
	3167,
	62,
	10962,
	5348,
	10856,
	11266,
	11092,
	11088,
	5485,
	6666,
	6727,
	5521,
	6666,
	6765,
	6765,
	6908,
	4903,
	6908,
	1536,
	5418,
	1141,
	1570,
	1915,
	6666,
	5418,
	4903,
	3068,
	6908,
	5485,
	5485,
	5485,
	5485,
	6727,
	6727,
	6727,
	6908,
	5485,
	2769,
	1525,
	5485,
	2769,
	6908,
	5485,
	6908,
	5485,
	6727,
	4618,
	5485,
	2769,
	6908,
	5485,
	6727,
	4618,
	6727,
	4618,
	6727,
	6727,
	6727,
	6727,
	6890,
	3982,
	1879,
	6908,
	5093,
	6666,
	6908,
	1617,
	6908,
	3094,
	3094,
	6765,
	6765,
	6879,
	2769,
	1530,
	1526,
	1525,
	1017,
	6666,
	6765,
	1525,
	6765,
	4899,
	6765,
	6765,
	6765,
	6765,
	6765,
	1339,
	5521,
	4899,
	6727,
	6727,
	9698,
	7625,
	5521,
	5521,
	5485,
	6765,
	6765,
	2364,
	6765,
	6908,
	4893,
	4893,
	6765,
	6765,
	6890,
	6727,
	5093,
	11098,
	6890,
	3946,
	6908,
	6890,
	6765,
	4725,
	11098,
	6908,
	2769,
	3056,
	1590,
	6908,
	4648,
	3946,
	3982,
	6666,
	6666,
	6666,
	6666,
	6666,
	6666,
	10976,
	10976,
	10976,
	6666,
	10976,
	1521,
	6908,
	6908,
	6666,
	5521,
	6908,
	6908,
	1468,
	6765,
	5641,
	5641,
	5521,
	5521,
	5485,
	6908,
	6908,
	6908,
	6666,
	6908,
	4903,
	6727,
	5485,
	6890,
	6908,
	5485,
	6908,
	5093,
	6890,
	5093,
	6727,
	12362,
	3041,
	6666,
	12334,
	6765,
	12362,
	1613,
	7324,
	6765,
	3068,
	3068,
	935,
	6908,
	99,
	6908,
	6908,
	6908,
	0,
	0,
	0,
	6908,
	4893,
	6908,
	1209,
	1209,
	6908,
	6908,
	6908,
	5485,
	6727,
	6727,
	1525,
	1017,
	6908,
	3946,
	4618,
	4618,
	0,
	191,
	2465,
	11219,
	6908,
	4903,
	2769,
	5485,
	2769,
	1525,
	4648,
	4618,
	1536,
	3068,
	3068,
	1613,
	12362,
	12362,
	5521,
	5521,
	12316,
	6908,
	6879,
	6728,
	6728,
	6908,
	12362,
	6908,
	6908,
	5521,
	3068,
	3094,
	5485,
	5521,
	1587,
	993,
	6666,
	6666,
	6908,
	6765,
	6666,
	6727,
	6765,
	6765,
	6765,
	6765,
	6765,
	4893,
	6765,
	6765,
	6765,
	6765,
	6765,
	6684,
	6684,
	6684,
	6765,
	11219,
	2087,
	6765,
	5418,
	9016,
	6727,
	8023,
	12362,
	6908,
	6765,
	6765,
	6765,
	6908,
	5521,
	3068,
	5521,
	9698,
	6765,
	5521,
	6765,
	1610,
	6908,
	4648,
	4899,
	6727,
	6765,
	6765,
	3056,
	6765,
	5521,
	6765,
	6765,
	6666,
	6908,
	6908,
	3068,
	3068,
	5521,
	6765,
	5521,
	6765,
	5521,
	5521,
	4893,
	4893,
	4893,
	4893,
	4893,
	4893,
	4893,
	4893,
	4893,
	1613,
	6765,
	6765,
	6765,
	6765,
	11219,
	9704,
	11219,
	12362,
	5521,
	5521,
	3056,
	6765,
	4899,
	4893,
	11216,
	6908,
	11219,
	9420,
	6908,
	3041,
	986,
	6666,
	6666,
	6727,
	5521,
	4648,
	6765,
	4893,
	6908,
	6908,
	5521,
	3068,
	5521,
	3068,
	5521,
	3094,
	6765,
	6666,
	6765,
	6765,
	6684,
	6684,
	6765,
	6765,
	6765,
	6765,
	6765,
	6765,
	6727,
	11092,
	2369,
	6765,
	4893,
	6666,
	11219,
	6765,
	6908,
	5521,
	4899,
	4648,
	5521,
	3982,
	4903,
	1346,
	6765,
	12362,
	5521,
	6765,
	6666,
	6765,
	6666,
	6908,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	6908,
	6666,
	5521,
	5521,
	1610,
	6765,
	6765,
	6765,
	6666,
	6765,
	5521,
	6765,
	6765,
	2383,
	2383,
	3982,
	6765,
	6765,
	12362,
	6908,
	0,
	6765,
	6765,
	6765,
	6765,
	6765,
	6727,
	6765,
	6765,
	6765,
	6765,
	6765,
	6765,
	2369,
	6765,
	6684,
	6684,
	5521,
	2372,
	4903,
	4903,
	6908,
	6908,
	5521,
	4899,
	4648,
	5521,
	6765,
	6727,
	5521,
	6765,
	6765,
	6666,
	6908,
	6666,
	6908,
	5521,
	6727,
	4899,
	3041,
	6765,
	6908,
	5418,
	6908,
	6765,
	6908,
	5418,
	5521,
	5488,
	6765,
	6765,
	3982,
	6908,
	12322,
	6908,
	5418,
	6908,
	5521,
	6765,
	6765,
	6727,
	5485,
	4618,
	1038,
	6908,
	6908,
	6727,
	4899,
	6765,
	3056,
	6765,
	6765,
	5521,
	6908,
	3982,
	5521,
	6765,
	6765,
	6666,
	6908,
	0,
	6908,
	0,
	0,
	0,
	0,
	0,
	6908,
	5418,
	6908,
	6908,
	5418,
	6666,
	6765,
	6765,
	5485,
	3982,
	6908,
	6765,
	6765,
	6765,
	6765,
	6765,
	6765,
	6765,
	4648,
	2383,
	4903,
	3982,
	3982,
	5485,
	5485,
	5485,
	6908,
	5521,
	1922,
	4903,
	11219,
	11219,
	11219,
	5485,
	1317,
	1320,
	8753,
	4903,
	3982,
	3982,
	12362,
	6908,
	6765,
	5521,
	6727,
	6727,
	5485,
	6727,
	5485,
	6684,
	6908,
	5485,
	6727,
	5485,
	5521,
	11216,
	6908,
	3041,
	3041,
	5521,
	4648,
	6765,
	4893,
	6908,
	1607,
	6666,
	5418,
	5521,
	4903,
	6908,
	6727,
	6765,
	4903,
	4648,
	3056,
	6765,
	6765,
	12362,
	5521,
	6765,
	6765,
	6666,
	6908,
	11219,
	11205,
	10965,
	11516,
	12322,
	6908,
	3041,
	2731,
	6727,
	5521,
	4618,
	4648,
	6765,
	4893,
	2769,
	6765,
	6765,
	6765,
	6908,
	6908,
	5485,
	6908,
	3041,
	3041,
	3041,
	3041,
	1584,
	6765,
	5521,
	10976,
	9478,
	11219,
	4648,
	6765,
	4893,
	10962,
	12362,
	6666,
	6666,
	6891,
	5642,
	6891,
	5642,
	5486,
	3057,
	2454,
	1441,
	3099,
	9413,
	11219,
	1824,
	5346,
	5346,
	6765,
	6727,
	6728,
	6765,
	10965,
	1915,
	3982,
	6727,
	6765,
	12322,
	12362,
	5486,
	9672,
	11232,
	10167,
	9605,
	9710,
	9710,
	9032,
	9335,
	7905,
	8299,
	10165,
	11384,
	11370,
	6727,
	2989,
	3056,
	6765,
	6727,
	6765,
	6765,
	4903,
	3982,
	6727,
	12362,
	12322,
	6908,
	12362,
	6727,
	6765,
	4903,
	6908,
	0,
	6765,
	5521,
	5521,
	6765,
	5521,
	6908,
	6908,
	10965,
	12322,
	12322,
	12322,
	10965,
	10962,
	12362,
	12322,
	12322,
	1613,
	5521,
	5521,
	5521,
	6908,
	5521,
	3094,
	3094,
	3094,
	6727,
	6727,
	3946,
	2731,
	2769,
	5521,
	3056,
	6765,
	6765,
	3982,
	6727,
	6765,
	6908,
	3068,
	3056,
	1098,
	570,
	3094,
	3094,
	3094,
	11216,
	12362,
	6908,
	6765,
	3068,
	9693,
	10965,
	5521,
	3068,
	6765,
	8741,
	6908,
	5641,
	3094,
	5521,
	3094,
	3094,
	4903,
	6765,
	6727,
	4899,
	4899,
	12362,
	4648,
	2180,
	4648,
	1922,
	6908,
	12362,
	12322,
	6908,
	3094,
	3094,
	3094,
	12322,
	12362,
	6908,
	6908,
	3094,
	3094,
	3094,
	6908,
	6908,
	5418,
	3059,
	4903,
	543,
	11219,
	11219,
	12362,
	4903,
	6908,
	0,
	12322,
	6765,
	3059,
	5521,
	5488,
	5521,
	6908,
	6908,
	9698,
	12362,
	11216,
	12362,
	10106,
	5485,
	5485,
	0,
	6908,
	3059,
	1590,
	5485,
	6908,
	6908,
	6765,
	5521,
	6765,
	5521,
	6666,
	5521,
	3094,
	3094,
	3094,
	6666,
	11516,
	11516,
	6908,
	12362,
	6908,
	4903,
	562,
	187,
	5418,
	5485,
	1318,
	1587,
	482,
	4648,
	482,
	5521,
	6908,
	1096,
	3094,
	3094,
	3094,
	6908,
	5485,
	0,
	0,
	6908,
	1048,
	6765,
	6666,
	5418,
	4903,
	5418,
	3982,
	3982,
	3982,
	3982,
	3982,
	3094,
	3094,
	3094,
	6765,
	12322,
	5418,
	6908,
	1913,
	1913,
	9415,
	11219,
	1913,
	1232,
	11219,
	12297,
	5521,
	5521,
	631,
	1103,
	7451,
	7451,
	10955,
	11516,
	8186,
	9693,
	11219,
	11219,
	11219,
	11219,
	12322,
	6908,
	4903,
	12362,
	5521,
	3094,
	6908,
	6765,
	5418,
	6765,
	6765,
	6765,
	6765,
	6765,
	11219,
	3094,
	3094,
	6908,
	3041,
	6765,
	1097,
	5521,
	5418,
	5418,
	5418,
	1468,
	5418,
	5418,
	5521,
	12362,
	12322,
	12297,
	12322,
	9704,
	1607,
	6666,
	6727,
	3982,
	5521,
	1587,
	3068,
	6908,
	5418,
	12362,
	6908,
	3982,
	6908,
	5418,
	6908,
	5418,
	6908,
	6908,
	12322,
	12322,
	0,
	6908,
	12322,
	6765,
	6908,
	12322,
	9584,
	12363,
	12322,
	12322,
	12322,
	12322,
	12322,
	6765,
	6908,
	12322,
	6666,
	6908,
	12362,
	5485,
	6765,
	3041,
	1584,
	6666,
	6666,
	6666,
	6728,
	6728,
	5486,
	2344,
	1318,
	4518,
	6727,
	1587,
	5333,
	5418,
	5418,
	6908,
	482,
	4648,
	482,
	5521,
	926,
	1804,
	926,
	2436,
	6908,
	5486,
	604,
	2358,
	1402,
	2360,
	10106,
	9083,
	9684,
	3041,
	5521,
	1525,
	12297,
	12297,
	6730,
	6727,
	6727,
	6727,
	5418,
	6727,
	5485,
	6727,
	5485,
	5418,
	6666,
	5418,
	6666,
	3946,
	1318,
	2179,
	787,
	787,
	2179,
	2176,
	5485,
	246,
	4648,
	246,
	4648,
	12322,
	6666,
	12362,
	6908,
	6908,
	5485,
	1019,
	6908,
	8140,
	5418,
	6666,
	5418,
	6666,
	5418,
	9065,
	9043,
	6666,
	5418,
	6765,
	8744,
	8734,
	1879,
	7936,
	7930,
	6765,
	5521,
	4903,
	1336,
	8740,
	8693,
	5521,
	9082,
	9052,
	5485,
	9071,
	9044,
	3056,
	5521,
	10965,
	10965,
	5521,
	8304,
	8284,
	5418,
	5521,
	9065,
	9043,
	377,
	348,
	1316,
	1297,
	1294,
	3982,
	157,
	2176,
	7302,
	7301,
	7302,
	7301,
	377,
	234,
	233,
	157,
	2176,
	2180,
	7223,
	7221,
	377,
	1316,
	3982,
	157,
	10095,
	2176,
	7302,
	7301,
	7302,
	7301,
	378,
	349,
	158,
	10095,
	4648,
	7224,
	7222,
	2371,
	7827,
	7771,
	1521,
	1525,
	7258,
	7252,
	6908,
	5485,
	9993,
	9071,
	9044,
	5418,
	5488,
	6908,
	5521,
	1587,
	6908,
	1368,
	1600,
	1103,
	4618,
	4903,
	11516,
	12315,
	10962,
	10962,
	12362,
	6908,
	0,
	0,
	6908,
	6908,
	6666,
	5418,
	6666,
	6908,
	5521,
	3534,
	5107,
	4617,
	1095,
	1607,
	4617,
	5484,
	6908,
	6908,
	5485,
	4899,
	12362,
	12362,
	6908,
	5521,
	5521,
	5521,
	5521,
	12362,
	6908,
	6765,
	6765,
	5521,
	5521,
	6765,
	6765,
	5521,
	5521,
	5521,
	5521,
	5521,
	3068,
	3068,
	3068,
	5521,
	5521,
	5521,
	5521,
	5521,
	5521,
	5521,
	5521,
	5521,
	5521,
	5521,
	5521,
	5521,
	5521,
	5521,
	6908,
	5521,
	6908,
	5521,
	6908,
	5521,
	12315,
	6908,
	2799,
	5485,
	5485,
	3094,
	6765,
	6727,
	3982,
	6727,
	2488,
	5418,
	5485,
	6908,
	5485,
	5521,
	6765,
	5521,
	6666,
	3056,
	5521,
	6765,
	6908,
	5418,
	6908,
	6908,
	5485,
	5485,
	5418,
	6908,
	3056,
	11216,
	6765,
	6908,
	5485,
	6908,
	6765,
	6908,
	5485,
	5485,
	2769,
	6765,
	5521,
	5418,
	5418,
	5418,
	6908,
	6908,
	6908,
	5418,
	5521,
	5521,
	10965,
	1319,
	2179,
	484,
	4648,
	2383,
	2377,
	1361,
	6765,
	6908,
	2383,
	5106,
	3068,
	6765,
	6765,
	6727,
	3982,
	4099,
	2995,
	6666,
	6908,
	6908,
	12362,
	6765,
	5521,
	6727,
	5485,
	5485,
	5521,
	6727,
	5485,
	5485,
	6765,
	5521,
	5418,
	6908,
	5418,
	6908,
	5485,
	6765,
	5521,
	5485,
	6908,
	5521,
	6538,
	6727,
	6727,
	6765,
	5286,
	6730,
	6908,
	1103,
	1103,
	6727,
	6908,
	6908,
	6908,
	5418,
	5485,
	3041,
	5521,
	5521,
	3056,
	12362,
	6908,
	5521,
	6666,
	6765,
	5521,
	6765,
	5521,
	5485,
	5485,
	6727,
	5485,
	6908,
	6666,
	6666,
	5418,
	6765,
	5521,
	6727,
	5485,
	5485,
	5485,
	6908,
	3041,
	6765,
	5418,
	0,
	3059,
	491,
	3059,
	631,
	3059,
	938,
	3059,
	4903,
	6765,
	6765,
	12322,
	1573,
	1048,
	5521,
	5521,
	5521,
	1096,
	1047,
	6666,
	6666,
	6666,
	6666,
	6728,
	6728,
	5486,
	5486,
	2344,
	6908,
	6908,
	5418,
	1318,
	1587,
	926,
	926,
	482,
	4648,
	482,
	5521,
	6908,
	938,
	5485,
	3982,
	6727,
	5418,
	6666,
	3982,
	6727,
	12362,
	938,
	1368,
	3982,
	6908,
	1610,
	5521,
	6765,
	6727,
	6765,
	6765,
	6727,
	6765,
	3056,
	12362,
	6908,
	6666,
	0,
	0,
	0,
	0,
	1922,
	1368,
	938,
	1922,
	1922,
	1368,
	4903,
	3982,
	3982,
	6908,
	6666,
	6765,
	1922,
	938,
	2381,
	2383,
	2383,
	6908,
	5521,
	6765,
	2383,
	6666,
	6765,
	2381,
	2383,
	2383,
	6908,
	5521,
	3041,
	6727,
	5485,
	4899,
	4648,
	6908,
	3982,
	3056,
	6908,
	5485,
	4648,
	2799,
	5521,
	5485,
	6765,
	5521,
	5521,
	6765,
	6727,
	6765,
	4899,
	2799,
	4648,
	3982,
	6908,
	4648,
	2799,
	5521,
	5485,
	6666,
	6666,
	12362,
	3056,
	6666,
	6908,
	6765,
	6765,
	2381,
	2383,
	2383,
	6908,
	6765,
	2381,
	2383,
	2383,
	6908,
	6765,
	2381,
	2383,
	2383,
	6908,
	3068,
	0,
	3982,
	5521,
	6727,
	5521,
	3041,
	1080,
	6727,
	5485,
	4899,
	4903,
	4648,
	6908,
	3982,
	3056,
	6908,
	5485,
	2378,
	4648,
	2799,
	5521,
	5485,
	4903,
	5521,
	5521,
	6765,
	6765,
	6727,
	6908,
	6765,
	5485,
	3982,
	6765,
	4903,
	3068,
	4648,
	3982,
	4648,
	2799,
	6666,
	6666,
	5521,
	4899,
	2799,
	12362,
	5521,
	6765,
	6690,
	6765,
	6765,
	6666,
	6908,
	5521,
	3059,
	5521,
	6666,
	6765,
	2381,
	2383,
	2383,
	6908,
	1922,
	1368,
	6908,
	1922,
	1922,
	1368,
	938,
	6908,
	6908,
	5521,
	6765,
	3982,
	6727,
	12362,
	6908,
	4903,
	2383,
	6765,
	938,
	1368,
	3982,
	6908,
	5521,
	1922,
	1922,
	6765,
	1368,
	938,
	4903,
	3982,
	3982,
	1922,
	3068,
	6765,
	6727,
	3982,
	5521,
	6908,
	6727,
	6908,
	2383,
	2378,
	3982,
	5521,
	12362,
	5521,
	6666,
	6908,
	12297,
	3982,
	1922,
	3982,
	1922,
	4903,
	1368,
	4903,
	2383,
	4903,
	2383,
	1368,
	2383,
	938,
	4903,
	2383,
	4903,
	2383,
	1368,
	4903,
	2383,
	4903,
	2383,
	6666,
	3982,
	4903,
	2383,
	1368,
	6666,
	3982,
	6765,
	4903,
	6666,
	3982,
	6666,
	3982,
	3982,
	1922,
	2383,
	6908,
	1105,
	6765,
	5521,
	6727,
	3056,
	6765,
	6727,
	6765,
	3056,
	6765,
	12322,
	12315,
	10106,
	11516,
	11219,
	11219,
	11219,
	11219,
	9693,
	11516,
	11516,
	11516,
	12362,
	2180,
	6908,
	12362,
	5521,
	2383,
	6908,
	5485,
	2799,
	3094,
	6727,
	3094,
	11216,
	6908,
	6908,
	5521,
	12362,
	1922,
	4648,
	6908,
	3068,
	1607,
	6765,
	6765,
	6765,
	0,
	6908,
	2799,
	3094,
	6908,
	11219,
	3068,
	4903,
	4903,
	4899,
	4899,
	5521,
	4899,
	2799,
	6727,
	6666,
	6666,
	4648,
	6908,
	3982,
	3056,
	4648,
	2799,
	5521,
	5485,
	6765,
	4899,
	2799,
	4648,
	3982,
	4648,
	2799,
	5521,
	3056,
	6765,
	6908,
	6908,
	5521,
	2799,
	5521,
	3094,
	3094,
	5521,
	6908,
	5485,
	4903,
	6666,
	3068,
	4903,
	4899,
	4899,
	6765,
	6727,
	3056,
	6765,
	12362,
	3068,
	5521,
	6666,
	6908,
	6765,
	3068,
	1922,
	4648,
	6765,
	6765,
	12322,
	12322,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	2474,
	3056,
	5485,
	3946,
	11088,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	6908,
	5521,
	3094,
	3056,
	1584,
	6666,
	6666,
	6666,
	6728,
	6728,
	5486,
	6908,
	2344,
	5486,
	6727,
	482,
	4648,
	1318,
	4518,
	482,
	5521,
	1587,
	5333,
	5418,
	926,
	1804,
	926,
	2436,
	6908,
	12362,
	1074,
	1073,
	6908,
	5418,
	1318,
	1804,
	4518,
	1318,
	1587,
	2436,
	5333,
	1587,
	6908,
	482,
	482,
	4648,
	5521,
	2344,
	5486,
	6666,
	6666,
	6666,
	6728,
	6728,
	5486,
	3059,
	1318,
	482,
	4648,
	3059,
	1587,
	482,
	5521,
	6908,
	8745,
	6908,
	5418,
	6908,
	2166,
	2996,
	8633,
	2166,
	8633,
	2166,
	2799,
	8163,
	11090,
	11092,
	8649,
	8649,
	3059,
	1309,
	6666,
	6908,
	6666,
	11384,
	6908,
	6908,
	6908,
	6765,
	6908,
	6765,
	5521,
	6908,
	6765,
	6908,
	6765,
	6908,
	6765,
	6908,
	6908,
	6765,
	6908,
	6765,
	6908,
	6765,
	6908,
	6765,
	6908,
	6765,
	6908,
	6765,
	6908,
	6908,
	1368,
	12362,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[15] = 
{
	{ 0x06000081, 29,  (void**)&UnityTlsContext_WriteCallback_m53DD70115C97432A676F3E437E8FB42F9FC068F5_RuntimeMethod_var, 0 },
	{ 0x06000083, 27,  (void**)&UnityTlsContext_ReadCallback_m15F3A217E44D480CAED06EB5A88503FB6259D7EC_RuntimeMethod_var, 0 },
	{ 0x06000085, 28,  (void**)&UnityTlsContext_VerifyCallback_m75D7C072718405EBBF8A2A9C794C4DDFB2595BD0_RuntimeMethod_var, 0 },
	{ 0x06000087, 26,  (void**)&UnityTlsContext_CertificateCallback_mF5E626BA2545CFFA64428622678E409702C2045A_RuntimeMethod_var, 0 },
	{ 0x06000097, 30,  (void**)&UnityTlsProvider_x509verify_callback_m47823254E133F7304ABFAE5A1F0D218402BC5B45_RuntimeMethod_var, 0 },
	{ 0x0600018A, 14,  (void**)&MonoBtlsBioMono_OnRead_mE0FF0BFC134CD0F923BBC555E346B7DD7CAD8988_RuntimeMethod_var, 0 },
	{ 0x0600018C, 15,  (void**)&MonoBtlsBioMono_OnWrite_m605A12A8B0CE9517927656981580BFA330BED613_RuntimeMethod_var, 0 },
	{ 0x0600018D, 13,  (void**)&MonoBtlsBioMono_Control_mE6F92D680A96B8B446D566D221BF925954FF43BB_RuntimeMethod_var, 0 },
	{ 0x0600022D, 16,  (void**)&MonoBtlsSsl_PrintErrorsCallback_mF161ADF7DBDB81C47BDD587C87E1275DEE5B7A45_RuntimeMethod_var, 0 },
	{ 0x06000250, 19,  (void**)&MonoBtlsSslCtx_NativeVerifyCallback_mFDF10D01CAAD040E50EE6F53FC1D4155EF696218_RuntimeMethod_var, 0 },
	{ 0x06000251, 17,  (void**)&MonoBtlsSslCtx_NativeSelectCallback_m6CA34F71A6093C7260A9F066D6C9FB3CD0CC86DB_RuntimeMethod_var, 0 },
	{ 0x0600025B, 18,  (void**)&MonoBtlsSslCtx_NativeServerNameCallback_mFF311CE931F6A075940D1B6828D1FB517AF03838_RuntimeMethod_var, 0 },
	{ 0x060002A4, 20,  (void**)&MonoBtlsX509LookupMono_OnGetBySubject_m15FE6FBEE9849C324E8E099172A581EABEB83FB3_RuntimeMethod_var, 0 },
	{ 0x06000CDF, 1,  (void**)&DeflateStreamNative_UnmanagedRead_m79E9628F6FF91A3995491997242DD83046C9B704_RuntimeMethod_var, 0 },
	{ 0x06000CE1, 2,  (void**)&DeflateStreamNative_UnmanagedWrite_m509AE153ECB916CDF755432335FBAB2B51CA1486_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[19] = 
{
	{ 0x0200017D, { 2, 1 } },
	{ 0x020001E0, { 4, 12 } },
	{ 0x020001E6, { 16, 23 } },
	{ 0x020001E7, { 39, 3 } },
	{ 0x020001E9, { 42, 41 } },
	{ 0x020001EA, { 83, 6 } },
	{ 0x020001EB, { 89, 3 } },
	{ 0x020001EC, { 92, 3 } },
	{ 0x020001ED, { 95, 8 } },
	{ 0x020001EE, { 103, 7 } },
	{ 0x020001EF, { 110, 96 } },
	{ 0x020001F0, { 206, 51 } },
	{ 0x020001F1, { 257, 4 } },
	{ 0x020001F2, { 261, 19 } },
	{ 0x020001F3, { 280, 22 } },
	{ 0x020001F5, { 302, 2 } },
	{ 0x020001F6, { 304, 3 } },
	{ 0x060001AA, { 0, 2 } },
	{ 0x06000B46, { 3, 1 } },
};
extern const uint32_t g_rgctx_T_t1739A0EEC47DD5827127A388AFD3B44C5F19D673;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_t1739A0EEC47DD5827127A388AFD3B44C5F19D673_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_AsyncTaskMethodBuilder_1_get_Task_m2743D6DA411C78BAE83E326226A7C801AE5046A8;
extern const uint32_t g_rgctx_T_tD61B3019D1869264718D125EAEE4311D26728AB7;
extern const uint32_t g_rgctx_Span_1_get_Length_m839FCF6D0CEBC13A23B4913B4B3BE1D2EF497540;
extern const uint32_t g_rgctx_ValueListBuilder_1_Grow_m64DA028E30FA837F9D8AA340D3A9344AA8CAEB90;
extern const uint32_t g_rgctx_Span_1_Slice_mD195CE80DA136235424FC8460CCC1F444AA8F4C1;
extern const uint32_t g_rgctx_Span_1_op_Implicit_mB474EB18DE57762E3A0772E2DE27ED5252766862;
extern const uint32_t g_rgctx_Span_1_t44C40F6E179DE0B4F52D7DA2E1884C0EE6DE3FB3;
extern const uint32_t g_rgctx_ArrayPool_1_get_Shared_m1992B72A302318B5A74633D26273A877D27EB4EF;
extern const uint32_t g_rgctx_ArrayPool_1_t58F3AC60669102F215C83BF8491B2CFFADF20417;
extern const uint32_t g_rgctx_ArrayPool_1_t58F3AC60669102F215C83BF8491B2CFFADF20417;
extern const uint32_t g_rgctx_ArrayPool_1_Return_mFE24FCB54D3C02CA684BD71EA9152AED02E0B440;
extern const uint32_t g_rgctx_ArrayPool_1_Rent_m5EF306341F1ED22323168043C5B68D869254CC35;
extern const uint32_t g_rgctx_Span_1_op_Implicit_mC53ACE9FE1E7E5E9F3A0C098C37C5A4DCDE2FE82;
extern const uint32_t g_rgctx_Span_1_TryCopyTo_m42CCEFF44F507F716B350717B93A19A5C5529A78;
extern const uint32_t g_rgctx_LinkedList_1_AddLast_m92E9EBD6A9A39368C44BA4FA908B1C7A20FBB251;
extern const uint32_t g_rgctx_LinkedListNode_1_t9E9525296AF0CCB2ED20655A790C2E4CD8622A56;
extern const uint32_t g_rgctx_LinkedListNode_1__ctor_m20FB5915DCE814C5B5ABFF83B3DB1D13FD2002B2;
extern const uint32_t g_rgctx_LinkedList_1_InternalInsertNodeToEmptyList_m260D83D9B9C9EA04987CB40AE53195CB13DF8DB9;
extern const uint32_t g_rgctx_LinkedList_1_InternalInsertNodeBefore_m05B6CF176F09A50E6D93416C84B856EC3E4BAF89;
extern const uint32_t g_rgctx_LinkedListNode_1_get_Next_mB1A01A4FC5490DBA9D6A301ED56851E198C9B4BE;
extern const uint32_t g_rgctx_LinkedListNode_1_Invalidate_m92AA7C26274AB3F1B2C90AAD36ED50BA53479F31;
extern const uint32_t g_rgctx_LinkedList_1_Find_m05423F53A6D38259BBFB8C5A4DC487E2C2182B00;
extern const uint32_t g_rgctx_LinkedList_1_get_Count_m9716A6902AA14DDA5DD9C943647E5A06A1DBB6E9;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_m78B05B517FFE11DE4D4E7F722CC8488A24712D57;
extern const uint32_t g_rgctx_EqualityComparer_1_t392B3893C4126DDA49A6D8C4161C74BC1ECB9674;
extern const uint32_t g_rgctx_T_tB8CC012B03969318B6B5424CAE7A44EDE0AA6665;
extern const uint32_t g_rgctx_EqualityComparer_1_t392B3893C4126DDA49A6D8C4161C74BC1ECB9674;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m6B7ECAC5C3E3877985F42ACB5093D9C5A28CCEBF;
extern const uint32_t g_rgctx_Enumerator_t82440D780591BF278D7D247E299FBB6BA82EFC94;
extern const uint32_t g_rgctx_Enumerator__ctor_mE01A798BE61864D38C425E7B6BE7935FB7850D7D;
extern const uint32_t g_rgctx_LinkedList_1_GetEnumerator_m8083B3D2C3FAE458314291AD1BA07D81465BE251;
extern const uint32_t g_rgctx_LinkedList_1_InternalRemoveNode_mDA777B72523F891BA292ADFA7EEB3ED1C7889689;
extern const uint32_t g_rgctx_LinkedList_1_ValidateNode_m131AE8D054B03E0FBCE81CEED06D9D1EB793E9C5;
extern const uint32_t g_rgctx_TU5BU5D_tB7945C667DD94B711E283C8F803CF9F9F3DF9C5E;
extern const uint32_t g_rgctx_LinkedList_1_CopyTo_m7F21A67D5C258A0B54301C4A6A402621CC82D581;
extern const uint32_t g_rgctx_TU5BU5D_tB7945C667DD94B711E283C8F803CF9F9F3DF9C5E;
extern const uint32_t g_rgctx_TU5BU5D_tB7945C667DD94B711E283C8F803CF9F9F3DF9C5E;
extern const uint32_t g_rgctx_LinkedList_1_t2BE688DBB804B0F90493D85355AF0536C5697D2D;
extern const uint32_t g_rgctx_LinkedList_1_get_Count_m00D92A248A5513B9C324C4F8A7EEAAE4A1AB9A85;
extern const uint32_t g_rgctx_T_t40ED5F405E4C66916984D6B18DAD11FB5460B70E;
extern const uint32_t g_rgctx_Array_Empty_TisTKey_t1FD6F73134BEFD3DA81B6A46436C50FA38749EC7_m962F8D7E72A72B5866FC788E97F3050DBF2D2EB6;
extern const uint32_t g_rgctx_Array_Empty_TisTValue_t05B39B62795218B9FB2F0D6E1C002FB430DD6D0C_mDCBE7E1087DB97D086F71B6F5758E1ED70F712F2;
extern const uint32_t g_rgctx_Comparer_1_get_Default_mCA970F420D07D5E98EB02857280D3EEBCA160863;
extern const uint32_t g_rgctx_Comparer_1_tB1C14895A92A1CFDFEC8C2B1B3A8AAB2774915B1;
extern const uint32_t g_rgctx_TKey_t1FD6F73134BEFD3DA81B6A46436C50FA38749EC7;
extern const uint32_t g_rgctx_Array_BinarySearch_TisTKey_t1FD6F73134BEFD3DA81B6A46436C50FA38749EC7_mB622ACDC6278218422284DF9F8C56A15C318A6D0;
extern const uint32_t g_rgctx_SortedList_2_Insert_mCC6515C2D0BC090B63A93434CB515DA7B30ACA96;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_m5E4415016462D57B44F58AD0F77CF776F27E7AE5;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Value_m4D424FA5AA51495D1B7C4303F24F504CABFCCFD7;
extern const uint32_t g_rgctx_SortedList_2_Add_m5FFD3B1C5235EC7DE6A9C6D8D9910CC27A9AC1DA;
extern const uint32_t g_rgctx_SortedList_2_IndexOfKey_m6481FD095B461438A1FE2B37991097DDA1D815DD;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_mE82F170302E1DA99A2944801AD9A2DFC2F663119;
extern const uint32_t g_rgctx_EqualityComparer_1_t0A97056FB9DAAF5FD6C7ED54906FF1D95D3B49E8;
extern const uint32_t g_rgctx_EqualityComparer_1_t0A97056FB9DAAF5FD6C7ED54906FF1D95D3B49E8;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m633EB6F895BE1F9A3BCD62EFADBB54D45AFC4591;
extern const uint32_t g_rgctx_SortedList_2_RemoveAt_m8EA545535EE75D4319AC2A2221F053E68D6A6F86;
extern const uint32_t g_rgctx_TKeyU5BU5D_t2AEF08C06DBDD8A7D8816C365C3F304B53414C64;
extern const uint32_t g_rgctx_TValueU5BU5D_t9D4C3EA64452F51826D853179BD45565DDE079D9;
extern const uint32_t g_rgctx_SortedList_2_GetKeyListHelper_m11D88DA06F385E4C81657ECE8534B2D59DC983FF;
extern const uint32_t g_rgctx_SortedList_2_GetValueListHelper_mB1BDECD9A5E6318EF46360981E69224312CB4835;
extern const uint32_t g_rgctx_KeyList_tBF0BA35A8E56C2CA6B0D132E81B49AAC9D037ABE;
extern const uint32_t g_rgctx_KeyList__ctor_m9E800CDF1F22A710C1B85E30E2CE048FC4F42188;
extern const uint32_t g_rgctx_ValueList_t0BD3E7798079666543FFC993879C507993BFFEE7;
extern const uint32_t g_rgctx_ValueList__ctor_m2C2D0F1A586CC2C94A51E4AD4078C4E81AAE88BE;
extern const uint32_t g_rgctx_SortedList_2_IsCompatibleKey_m292C7E9BFADCEEBC3D94DC79A480ED45C9EFCD25;
extern const uint32_t g_rgctx_SortedList_2_t93A9CE8FB30B3C390C759153CE3225118DD18523;
extern const uint32_t g_rgctx_SortedList_2_ContainsKey_mD43E2CDBF2912D4CF756124303F08E7877325A0D;
extern const uint32_t g_rgctx_SortedList_2_IndexOfValue_m0C27E1E3CA435B9541143BED59421495454F76CC;
extern const uint32_t g_rgctx_SortedList_2_get_Count_m70084C51D5559257162A8D6731A683D970CF165D;
extern const uint32_t g_rgctx_KeyValuePair_2__ctor_mBF3AA820F6A5A5EA96CE7D4F04AFA2421C066B26;
extern const uint32_t g_rgctx_KeyValuePair_2U5BU5D_t69B802D18DD5300031ED91A70D5E8EDCD78EF679;
extern const uint32_t g_rgctx_KeyValuePair_2_t26603B18F49F78166CF21E4D82F3A76FFAF80D97;
extern const uint32_t g_rgctx_SortedList_2_set_Capacity_m16837BB431C0E2C7CF5FCEA5F6B3CB051C4EC3EA;
extern const uint32_t g_rgctx_Enumerator_t26F0DFAF5C765B8DF9CB0D8D7A6FB770C29F0DA4;
extern const uint32_t g_rgctx_Enumerator__ctor_mAA4F2E43EE68D2CD181E9A0D6CC81FFC7591D301;
extern const Il2CppRGCTXConstrainedData g_rgctx_TKey_t1FD6F73134BEFD3DA81B6A46436C50FA38749EC7_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_TValue_t05B39B62795218B9FB2F0D6E1C002FB430DD6D0C;
extern const uint32_t g_rgctx_SortedList_2_set_Item_m44BAA94EE58C1BD2E91CDABEBFF3F62D0702E4A7;
extern const uint32_t g_rgctx_TValue_t05B39B62795218B9FB2F0D6E1C002FB430DD6D0C;
extern const uint32_t g_rgctx_Array_IndexOf_TisTValue_t05B39B62795218B9FB2F0D6E1C002FB430DD6D0C_m50CBD31A2C8613FCF3A49C18828951D925CB3A0E;
extern const uint32_t g_rgctx_SortedList_2_EnsureCapacity_m9AFA49D189786022026E89A084CD32F0ED866E92;
extern const uint32_t g_rgctx_SortedList_2_tC6344DFC770CE08ABFDA57B902B4868D9F62EF3C;
extern const uint32_t g_rgctx_SortedList_2_get_Count_mCE323B5DCB7E363E70AF36EC76133C1D587B8EC8;
extern const uint32_t g_rgctx_TKey_tFD55C737F5BFBDA0AB5E5CCF01864765DD83FFA9;
extern const uint32_t g_rgctx_TValue_tB72A46C4943C2725B8BC470E058E8E390260E574;
extern const uint32_t g_rgctx_KeyValuePair_2_t28C5B701CDAEA1E9D5ABF27CE326C6E45F61F839;
extern const uint32_t g_rgctx_KeyValuePair_2__ctor_m8F2D8B7678D0CFB7534B3E9F456DABCBC7D516D0;
extern const uint32_t g_rgctx_SortedList_2_tD5CDAF17B1E37289C3F51E62B7B25339B0E1C593;
extern const uint32_t g_rgctx_SortedList_2_get_Count_mD33CE88877A87FEB37A6995D59C4F8F680D1E95B;
extern const uint32_t g_rgctx_TKey_tAFE4038E78BD9702B4A7F65620EDE61B56661C62;
extern const uint32_t g_rgctx_SortedList_2_t451D95A193728D5626E4C2CA2CCC24125C4FE651;
extern const uint32_t g_rgctx_SortedList_2_get_Count_m18B7E97F1D1E1482208D6592D480F8DDCFB765E5;
extern const uint32_t g_rgctx_TValue_tD97A06AA0F2E68B3B8DD39F7B451DF1FCB143DD1;
extern const uint32_t g_rgctx_SortedList_2_t61AEDECC6DF909FAB02D0487A698CA9EE9EB5063;
extern const uint32_t g_rgctx_SortedList_2_ContainsKey_m4A88F19F2A995D960523ADB86539A226F96270D2;
extern const uint32_t g_rgctx_SortedList_2_get_Count_mEEFDA802B250F387AF6DA9968FE4F23319DE7145;
extern const uint32_t g_rgctx_SortedList_2_GetKey_m2D600345BA732A25B665312432E50E3DF78A6AA3;
extern const uint32_t g_rgctx_SortedListKeyEnumerator_tD25E4501C93F8996465E698CF2E30942795FF24F;
extern const uint32_t g_rgctx_SortedListKeyEnumerator__ctor_m28CBE46D4F471D562C6E9BF643EEBEE97FAC49A2;
extern const uint32_t g_rgctx_TKey_t299F6A80A11873E87035AB041EBF0A5F15103B73;
extern const uint32_t g_rgctx_Array_BinarySearch_TisTKey_t299F6A80A11873E87035AB041EBF0A5F15103B73_mF0629A26CAB34F85B81363205BE184CBD08961E5;
extern const uint32_t g_rgctx_SortedList_2_tEF86C98A7E9ABE5CE49C66355C8C437FA26BC3CD;
extern const uint32_t g_rgctx_SortedList_2_ContainsValue_m27B9FF070EEA5DBA0145190F4D3F7A2F9B31C62D;
extern const uint32_t g_rgctx_SortedList_2_get_Count_mD3233393E8C9A5AA14992E1D63A185032E77D8DC;
extern const uint32_t g_rgctx_SortedList_2_GetByIndex_m53C32801F4EA95F9423C85DC10D062A6F118F43A;
extern const uint32_t g_rgctx_SortedListValueEnumerator_t2C333B1C34817C77FE472AF6C5C121C216A1E0BC;
extern const uint32_t g_rgctx_SortedListValueEnumerator__ctor_m5888F52C713CBACC1D3039B513BB385DCCFFAC84;
extern const uint32_t g_rgctx_Array_IndexOf_TisTValue_tEB96A977159351D0F21BC37F853A4450F0186BAF_m2A579BC9630F279050FFC0C41214B3CAFD81E0AF;
extern const uint32_t g_rgctx_Comparer_1_get_Default_m81161DF68C8FBA662C333FE110750998123D884A;
extern const uint32_t g_rgctx_Comparer_1_t5039FF605105E34CA6CD5BDA36586D613105B2FD;
extern const uint32_t g_rgctx_IEnumerable_1_t6B95CE12042047894ECBA60DB0E7A0452A6251AD;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_mD40A8AD1F465F0E302A1F112989CA720FF5ECE93;
extern const uint32_t g_rgctx_IEnumerator_1_tDA871FF7BB1C2D3DC55FE1D1EAA2F93315EA2E52;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m287FD06D0787C3CA29DBF21E2FDB02908B8092EF;
extern const uint32_t g_rgctx_SortedSet_1_t1907F34FCD326D284C3E76A5B27C1FBEFF766F07;
extern const uint32_t g_rgctx_SortedSet_1_Contains_m2219DF60D3DD8333C6D005CB75C808584CDCD618;
extern const uint32_t g_rgctx_SortedSet_1_get_Count_mC104C70642BAE4409B1320CC4CCEBEB190E3AF09;
extern const uint32_t g_rgctx_SortedSet_1_Log2_m1650455008CF18578C3DE966B4D8F93659241712;
extern const uint32_t g_rgctx_SortedSet_1_t1907F34FCD326D284C3E76A5B27C1FBEFF766F07;
extern const uint32_t g_rgctx_Stack_1_tB9949F83B69CE69B76144CEDBB176334EB1A729F;
extern const uint32_t g_rgctx_Stack_1__ctor_m942E509B7108B6D8E0EDD22464AB3282ADA43783;
extern const uint32_t g_rgctx_Stack_1_Push_m2508C7B1C3E4AF0FA5500C42363499A4B8C5D0F8;
extern const uint32_t g_rgctx_Node_t82E36EC90A91286397D275B6DF030BB9AC0A8928;
extern const uint32_t g_rgctx_Node_get_Left_m1B81AE120F05F79696BC09346383D9569F5B405F;
extern const uint32_t g_rgctx_Stack_1_Pop_mC5CDB6147755218C228C81F58089586C1417D5B1;
extern const uint32_t g_rgctx_TreeWalkPredicate_1_tFE86EA1E52CCF1469E47E49C2ECEBD6853E01DD4;
extern const uint32_t g_rgctx_TreeWalkPredicate_1_Invoke_m359AF12F4621B26E63B5A91D6326BF826604571B;
extern const uint32_t g_rgctx_Node_get_Right_mE6DC74A026D29D98A62CFF9A993D06DB21CEB39D;
extern const uint32_t g_rgctx_Stack_1_get_Count_mF3C9F422F43D522282E45CE41A28CF23083410DF;
extern const uint32_t g_rgctx_Queue_1_t5B11105E5DAFD5ED8D1EE047AE9383A72B2525FD;
extern const uint32_t g_rgctx_Queue_1__ctor_m0636156497B54BD8AA8EA1C4B616BC5B293D0C14;
extern const uint32_t g_rgctx_Queue_1_Enqueue_m7C06EEC26B9085D133DB3C9E2914B6376C29686C;
extern const uint32_t g_rgctx_Queue_1_Dequeue_mC9FDECD303283ACEDB593E45DD2D51865173C5B9;
extern const uint32_t g_rgctx_Queue_1_get_Count_m1E085883C454F700E75F0355F94ED7B40D18D9EF;
extern const uint32_t g_rgctx_SortedSet_1_VersionCheck_mA8C684355B26D37C895062307E92E5254608FD39;
extern const uint32_t g_rgctx_SortedSet_1_AddIfNotPresent_m71C0A08495E6C7474FCA314874B2E28925A136C2;
extern const uint32_t g_rgctx_SortedSet_1_Add_m2F7713BF3F81075944513ECD5227300094B847D6;
extern const uint32_t g_rgctx_Node__ctor_mB33B037BE5D30C7C75983A1243CAA6EEA0B63D24;
extern const uint32_t g_rgctx_Node_get_Item_m3714B8239BBE992251A9854F8BAD29394C4C4DCC;
extern const uint32_t g_rgctx_IComparer_1_tA4CC6C6451C9A880FD54FD0B9F099C5E2450F0B5;
extern const uint32_t g_rgctx_IComparer_1_Compare_m3E7FCB9431321AC8AC4E01B5D9BE553F4AD5F72A;
extern const uint32_t g_rgctx_Node_ColorBlack_mAE1C842B0ADCAD9278516541675B2C60DD16E747;
extern const uint32_t g_rgctx_Node_get_Is4Node_m0AEE6A8C6C200AD1297E911305DF47A4E57E9DE4;
extern const uint32_t g_rgctx_Node_Split4Node_m20540150E17B57106BC79DCBCC53A54125D5DF33;
extern const uint32_t g_rgctx_Node_IsNonNullRed_m7F5B238C17B99A71527461C226121250060C78C8;
extern const uint32_t g_rgctx_Node_t82E36EC90A91286397D275B6DF030BB9AC0A8928;
extern const uint32_t g_rgctx_SortedSet_1_InsertionBalance_m2E7CB6DD9BFAF675D452DEC11247565782178B38;
extern const uint32_t g_rgctx_Node_set_Right_m7FF10BDB5E9DF2CB50B3A6D0EC707C0F9563252A;
extern const uint32_t g_rgctx_Node_set_Left_mDEE0FF492989054F7B4DD2543B632078BEDBDA8C;
extern const uint32_t g_rgctx_Node_get_IsRed_m6B0C04892AFBD6D97CFEEDC02895334C7BF92209;
extern const uint32_t g_rgctx_SortedSet_1_DoRemove_mB639696863EB5BBCB3984C43F013301857858F36;
extern const uint32_t g_rgctx_Node_get_Is2Node_m5A6A7791B4E02625C06349371FEDB90A42FDCF8F;
extern const uint32_t g_rgctx_Node_ColorRed_m1D853D12EB4E90AFFD4B0CCB4640E1F54C0D3FDC;
extern const uint32_t g_rgctx_Node_GetSibling_mB9A6C2A662F51A326E0C477D8656D0020A93F01E;
extern const uint32_t g_rgctx_Node_RotateLeft_mCAAF2FF045CBA2B4CB660A666415EDC94D8CA0B3;
extern const uint32_t g_rgctx_Node_RotateRight_mB0EAF0E8375AFCEC0D188E169772F90544C401E6;
extern const uint32_t g_rgctx_SortedSet_1_ReplaceChildOrRoot_mCAE4C51F4A83F9A37E6EA0CC0823A27F6B986526;
extern const uint32_t g_rgctx_Node_Merge2Nodes_mBA890F94A5A184D2EE899E7DD9693B835F3FFC51;
extern const uint32_t g_rgctx_Node_GetRotation_m5881ECAF411C9157256F7968E41E0A00DC7BFBC6;
extern const uint32_t g_rgctx_Node_Rotate_mA1A9DE28DAE9F321D7BBD43D4E4AF87A01A05E22;
extern const uint32_t g_rgctx_Node_get_Color_m8FBAA51EEE4B8FB8C94DB92EC36F834200EC17E6;
extern const uint32_t g_rgctx_Node_set_Color_mF6C582CB62F168A13D7FD5543EBEAB080AC306F6;
extern const uint32_t g_rgctx_SortedSet_1_ReplaceNode_m6AD1593223B0FD0FEC01FCBBFBD0D3DCBC6A00E8;
extern const uint32_t g_rgctx_SortedSet_1_FindNode_m76DF155903D2383BF56389E0646D05C3B6AA64DE;
extern const uint32_t g_rgctx_SortedSet_1_CopyTo_m2DC7417BEC6FFBE4DA465554BF83BB48BE4FEC5F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass52_0_tCEBD2F97696032E4FBE349AA1210B23D96506149;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass52_0__ctor_m8AC1A17E0080C18E282D3F42B792C905598843A1;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass52_0_U3CCopyToU3Eb__0_m50120C9456BC4D9DDBDDAD9B36128CD275866496;
extern const uint32_t g_rgctx_TreeWalkPredicate_1__ctor_m2EC81102633E7571439783E0F3D6CC9188D8F478;
extern const uint32_t g_rgctx_SortedSet_1_InOrderTreeWalk_mCA504DEF419D02C3724C1F99408A028A080CAA05;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass53_0_t8088ED0364B7C35699D0B5A61069F7A81898D400;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass53_0__ctor_mA919A8E6EA3A46A93D247ACFE5CB565C263CE1EF;
extern const uint32_t g_rgctx_TU5BU5D_t38D4E7F7E8DBB01B0EBAAD3562A9C876C9A1AE90;
extern const uint32_t g_rgctx_SortedSet_1_CopyTo_mB673D121FF323A52F849ED9579DE271A611404EF;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass53_0_U3CSystem_Collections_ICollection_CopyToU3Eb__0_m45B1ED8D9DCDFD3FC34DDD3B54CAA5EEDC06FE6E;
extern const uint32_t g_rgctx_Enumerator_t8BC4FE1AD604723F9025C54884ECB41F02B3A5FA;
extern const uint32_t g_rgctx_Enumerator__ctor_m9AA50DEE926AE516D473C8C14E0B89C64DF1D67E;
extern const uint32_t g_rgctx_SortedSet_1_GetEnumerator_mE9EC394A6E10A0F03A8C874130ED9DB8B3E78C80;
extern const uint32_t g_rgctx_Node_RotateRightLeft_mF29D9CBA32EC9A41A3D597EE27E4EE55238036A7;
extern const uint32_t g_rgctx_Node_RotateLeftRight_mAFF20EF80EF7845FF2C2B54CB07E16E9A3C01023;
extern const uint32_t g_rgctx_Node_ReplaceChild_m0905C8DEE7675F2007B79F11BE4540E5AD0B96D8;
extern const uint32_t g_rgctx_SortedSet_1_get_Comparer_m3FCFEA52D823FB8D002097FDD335FE32084B2BCA;
extern const uint32_t g_rgctx_SortedSet_1_HasEqualComparer_m5614B1B0D497A8B85F60D94676E7F60B6F6EF460;
extern const uint32_t g_rgctx_SortedSet_1_IsSubsetOfSortedSetWithSameComparer_mD8E8014C3DB9349C02F15B554D179C9C46E16F6C;
extern const uint32_t g_rgctx_SortedSet_1_CheckUniqueAndUnfoundElements_m5A8E968A794EE22740ABBF2821268A04AAE4A17F;
extern const uint32_t g_rgctx_SortedSet_1_get_Min_mAA1072E42B73ED568E924E47CD55552EA76991C6;
extern const uint32_t g_rgctx_SortedSet_1_get_Max_mDD55BB8934DCBEE0CC220676C3E6CBC0AD7A1A25;
extern const uint32_t g_rgctx_SortedSet_1_GetViewBetween_mE1BA349B377820B7BF85E132B4CAFE0B0F97F47E;
extern const uint32_t g_rgctx_Enumerator_get_Current_m2F5948D8C91CCAD7712F19D96F8EFF7862BA54EF;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m459316E149C41B62A241289E5AB75C9859B482E4;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_t8BC4FE1AD604723F9025C54884ECB41F02B3A5FA_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_SortedSet_1_ContainsAllElements_mE9F8F523D36D3E0917C9F1DF560728BD813B469A;
extern const uint32_t g_rgctx_ICollection_1_t0FD4FD68B67F09E5275B4BEA388B206C21808678;
extern const uint32_t g_rgctx_ICollection_1_get_Count_m29B767DD800FB48F84AAF3C2200C6D76A2387BD1;
extern const uint32_t g_rgctx_SortedSet_1_InternalIndexOf_m432B2692A04D1A65A78C87BDACC3226E2C5A63D3;
extern const uint32_t g_rgctx_SortedSet_1_get_MinInternal_mCAA7B77A011C15215195ED5FF87CD45CCA5DAC75;
extern const uint32_t g_rgctx_SortedSet_1_get_MaxInternal_mEE7A1C1F172F7E97304B79AE9ECC2893CCF75BFB;
extern const uint32_t g_rgctx_TreeSubSet_t11031D0C3F3BD38BA7D1B987A577956EACDF3722;
extern const uint32_t g_rgctx_TreeSubSet__ctor_m5912F6D1C36A0E2A19F011A46C1C20A46122CFE7;
extern const uint32_t g_rgctx_SortedSet_1_GetObjectData_m194013DE3BE7EF3670E5E242F117EBB37405C0C0;
extern const uint32_t g_rgctx_IComparer_1_tA4CC6C6451C9A880FD54FD0B9F099C5E2450F0B5;
extern const uint32_t g_rgctx_TU5BU5D_t38D4E7F7E8DBB01B0EBAAD3562A9C876C9A1AE90;
extern const uint32_t g_rgctx_TU5BU5D_t38D4E7F7E8DBB01B0EBAAD3562A9C876C9A1AE90;
extern const uint32_t g_rgctx_SortedSet_1_OnDeserialization_m56D5CCDACFBD2FA545DED35FF75E40199A8CEA2F;
extern const uint32_t g_rgctx_SortedSet_1_t43A6E6F1EE955185EE7346752FDF6FBEF32E977E;
extern const uint32_t g_rgctx_SortedSet_1_get_Comparer_mFE128A5B4F49C2EA60B963B62687C3EF1CC4357F;
extern const uint32_t g_rgctx_SortedSet_1_t43A6E6F1EE955185EE7346752FDF6FBEF32E977E;
extern const uint32_t g_rgctx_SortedSet_1__ctor_mCC54033B6994BE0C0436B2A4E2E1E9E54F518909;
extern const uint32_t g_rgctx_SortedSet_1_FindRange_m6C2E0847E0A1D93E9305D4D6FA0966756336A93F;
extern const uint32_t g_rgctx_TreeSubSet_VersionCheckImpl_m11741508F15663C215ADE5B3ACC1FC96AE79D008;
extern const uint32_t g_rgctx_SortedSet_1_IsWithinRange_m2A01B405926FFD5E5657731B8243D2A28D2A5A1E;
extern const uint32_t g_rgctx_SortedSet_1_AddIfNotPresent_m4598BD090A7927D25E15D3B9A215C59B8BE0C679;
extern const uint32_t g_rgctx_SortedSet_1_VersionCheck_mC854A48934AC64C9FC9C547AE6E0C7410EC5C11E;
extern const uint32_t g_rgctx_SortedSet_1_Contains_mE01367F9BA3002F5F27AEB8D78600FE69FADEB49;
extern const uint32_t g_rgctx_SortedSet_1_Remove_m47EEA0987A62D2814A5D4DBF556103AF1225531A;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass9_0_tB5B1D770456865055F9E8C7106606A9E032581F3;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass9_0__ctor_mA3541BF8A43702221A4EBC8E04607606F814451C;
extern const uint32_t g_rgctx_List_1_tE50A1BC80D4D5D592B4FE3EA9D11D5169BC485FE;
extern const uint32_t g_rgctx_List_1__ctor_mB0D7186913E3B8D1736FB8F0D8D05496B0C44FB1;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass9_0_U3CClearU3Eb__0_mA73D115A4A7511C9160E735933A28A997F231724;
extern const uint32_t g_rgctx_TreeWalkPredicate_1_tAB63AA224A61328019B75B973C6FE345BFDD0965;
extern const uint32_t g_rgctx_TreeWalkPredicate_1__ctor_mF38B1A004478BADD63F9045E439FC96B695B2D44;
extern const uint32_t g_rgctx_SortedSet_1_BreadthFirstTreeWalk_m01201BDA6434C8D3B3A4A7E85332497E78A29438;
extern const uint32_t g_rgctx_List_1_get_Count_m643271998633167ADF6CC1FF093CD68E5DF4856A;
extern const uint32_t g_rgctx_List_1_get_Item_mEBA579E5EA942E62B03C8DF2E4996CAE5CD0C51E;
extern const uint32_t g_rgctx_List_1_RemoveAt_mFF8B8E0BC795E162CCE29463F20F55006590ADD0;
extern const uint32_t g_rgctx_IComparer_1_t56DDBA8A8572C6FA3107D49727EB234650E07074;
extern const uint32_t g_rgctx_IComparer_1_Compare_m893082C4F4E348460B821DC9C8B0D00D1AE98F9E;
extern const uint32_t g_rgctx_Node_t913BCFA91C36149C5EBE9E8F8E79C07A4A569C49;
extern const uint32_t g_rgctx_Node_get_Item_m0865E8B1EE3EB1EF3BB092C0A959464F05C52A72;
extern const uint32_t g_rgctx_Node_get_Right_mAAED6C6C5634B2CD39AB505EFB39FABBBDACE5B7;
extern const uint32_t g_rgctx_Node_get_Left_m015F38662504C89419622BDFD1CCB466F15F28B5;
extern const uint32_t g_rgctx_SortedSet_1_Log2_mBD3DFE2D3846B591246C9A61D3CB87CB768EA7C2;
extern const uint32_t g_rgctx_Stack_1_tC64AC7B73A5B2C2DAE7F1C8F0B5E73AF1FB50DE4;
extern const uint32_t g_rgctx_Stack_1__ctor_m94C5FDACA0AD705E8951E8DED135A9AE73A832C6;
extern const uint32_t g_rgctx_Stack_1_Push_m391FB8AC34C956640A7B76E2556B4C2ACA5C2C59;
extern const uint32_t g_rgctx_Stack_1_Pop_m7DC8E301CCF53A8A8A28BB38CFECCFAD88D65B4D;
extern const uint32_t g_rgctx_TreeWalkPredicate_1_Invoke_m8271CF1F5A69C9B6E4A0947E6C1D0CFE3919B8D5;
extern const uint32_t g_rgctx_Stack_1_get_Count_m18348F72DB5AA9ED8C5E77CF8A860FC9159D1E13;
extern const uint32_t g_rgctx_Queue_1_t69155CF6A7D53685D085C20AF7F3FC5B61F62B6B;
extern const uint32_t g_rgctx_Queue_1__ctor_m612569F5AEF7FCB678587D85C5DA08D57FA07F17;
extern const uint32_t g_rgctx_Queue_1_Enqueue_mE1ECB0BA1B5DBEB29CBC9AB36CABA4E8A8B9A6C6;
extern const uint32_t g_rgctx_Queue_1_Dequeue_m2DBFF9D78655F1693614797A90DA1404A54D3122;
extern const uint32_t g_rgctx_Queue_1_get_Count_m342BDD91C8D4B202D5D665CABFD3E7E9CD8FB3D3;
extern const uint32_t g_rgctx_SortedSet_1_FindNode_m255D4CECBEF2766935E09CF3DBCDCD1A7F119DAC;
extern const uint32_t g_rgctx_SortedSet_1_GetEnumerator_m85D1ED2641B279AC27F2F6F8A89F72ED8C1E001A;
extern const uint32_t g_rgctx_Enumerator_get_Current_m571F36276E19A196EFB3F0E1318579877BD1C48E;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m4DA58684CA02D9C6D05CB40D4079B1BF16A6FFC6;
extern const uint32_t g_rgctx_Enumerator_tA292E74B09FB96746A52917C0EE2776A5E4E7351;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_tA292E74B09FB96746A52917C0EE2776A5E4E7351_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_TreeSubSet_U3CVersionCheckImplU3Eb__20_0_m21F05956E412EEF05BC4F514894400FE3876A98E;
extern const uint32_t g_rgctx_SortedSet_1_InOrderTreeWalk_m2A46E038A8F7EF23E7EEFA0346BF8137E016F9C5;
extern const uint32_t g_rgctx_SortedSet_1_GetViewBetween_m64D00640776AB63026EC8EF223EC773733459F6B;
extern const uint32_t g_rgctx_TreeSubSet_tA7068D58BDA25F6A8E6756DE8EDC3F41153BF041;
extern const uint32_t g_rgctx_SortedSet_1_GetObjectData_mE2BC6A2D2A4266F880CE3056AA774E6981AE78F3;
extern const uint32_t g_rgctx_Node_tE7F92C0B789BFDF3C8CA7FC08903B10773AE9952;
extern const uint32_t g_rgctx_Node_get_Item_mFE1783AE419A4F13230B7833B6770E3742AF4A15;
extern const uint32_t g_rgctx_List_1_t634977CF38B2AC837D3A5E2FF97542FAA6D24D4F;
extern const uint32_t g_rgctx_List_1_Add_m673EDC30EE2AFE1D3A5AF4A537E836BD11B365CD;
extern const uint32_t g_rgctx_Node_set_Item_mBDEA632B81A444A208273CACB31B451B0E4E91C2;
extern const uint32_t g_rgctx_Node_set_Color_m3B08B60B56F2738AD25411041B8918C741CCACC8;
extern const uint32_t g_rgctx_Node_tA753FBEAFB7C0E9F30B03C4E16C2CAEA64439C50;
extern const uint32_t g_rgctx_Node_get_IsRed_m69D3CDCED0355BC1AE9004E605E9814B29948B1E;
extern const uint32_t g_rgctx_Node_get_IsBlack_m6D6B36CEEE64BCC6D20DCD3E27168CC5AFA93C62;
extern const uint32_t g_rgctx_Node_get_Color_mFB7CE6196D794811BB602F746371034BF004212A;
extern const uint32_t g_rgctx_Node_get_Left_m151DC7DFEA0F0FC03A5E144C64D0B201C3AA4188;
extern const uint32_t g_rgctx_Node_IsNullOrBlack_mAC38C6192A2289B4EC04AB5AD75A89AD37281471;
extern const uint32_t g_rgctx_Node_tA753FBEAFB7C0E9F30B03C4E16C2CAEA64439C50;
extern const uint32_t g_rgctx_Node_get_Right_mEC2BB6CD2C52DA7D536E1D3264307AF21BB6A304;
extern const uint32_t g_rgctx_Node_IsNonNullRed_m8726A1EB92D947D9C662369FBAD54196DEE5A6C5;
extern const uint32_t g_rgctx_Node_ColorRed_mFD53D03A936EC08EEAB5CC40649F8BF8AFDECD88;
extern const uint32_t g_rgctx_Node_ColorBlack_mE73E1C5DE0C000C8F70340E7E12E582D24B70D39;
extern const uint32_t g_rgctx_Node_RotateRight_mDF0183FB5D16BB7A3BDA76DC367ED7B0DCCC0969;
extern const uint32_t g_rgctx_Node_RotateLeft_m070AA41D31D10BAE5BB783265AB98A9E5B2B49BA;
extern const uint32_t g_rgctx_Node_RotateRightLeft_m2BCE860A84B10C33D17E18AC29821A3933F70B3D;
extern const uint32_t g_rgctx_Node_RotateLeftRight_m060152F4001B6F4D92B29516F6C7D7C0E63B48F1;
extern const uint32_t g_rgctx_Node_set_Right_m5DFBA8D32B594D5517CD13462D4E2920DC8F5081;
extern const uint32_t g_rgctx_Node_set_Left_m9CC6AA74944B09D6AA015952BDF4847017A164F7;
extern const uint32_t g_rgctx_Enumerator__ctor_m954D8F8F6F3981A96AAEC85FD47550644D465C8B;
extern const uint32_t g_rgctx_SortedSet_1_t175785C97E60DE6415DF7BC21055FDAA39137573;
extern const uint32_t g_rgctx_SortedSet_1_VersionCheck_m6DCAC4CABF454C0D3B668EDB8C59FCA547023A63;
extern const uint32_t g_rgctx_SortedSet_1_get_Count_mC69B3F3F4D830F7343E5132962C6A7769115CED6;
extern const uint32_t g_rgctx_SortedSet_1_Log2_m9B33E9CBCAE0BE849D996F4718152EF9F33E8B60;
extern const uint32_t g_rgctx_SortedSet_1_t175785C97E60DE6415DF7BC21055FDAA39137573;
extern const uint32_t g_rgctx_Stack_1_t721167C43AC5B7BAE2AD13C232E9818E61C89EC8;
extern const uint32_t g_rgctx_Stack_1__ctor_mE763684B8DA1F87BC06FA5051A88C512BC58A7E6;
extern const uint32_t g_rgctx_Enumerator_Initialize_m034333F910E78A165DDA546B9D5144616DC1F737;
extern const uint32_t g_rgctx_Node_t73DAB7B67962EBCFB9C2284DC57069617B37FC2F;
extern const uint32_t g_rgctx_Node_get_Left_mEB7832AF38FD9A15937D357D24C21E1E7455E98C;
extern const uint32_t g_rgctx_Node_get_Right_m787994C7F76FFA37162A51EAE03E7E98C3C44008;
extern const uint32_t g_rgctx_Node_get_Item_mC576D187E3FF5326BEE6ACDC9E0AF4223F91C76A;
extern const uint32_t g_rgctx_SortedSet_1_IsWithinRange_m54A107906ADD96D310473D218B293476BD8FEC3C;
extern const uint32_t g_rgctx_Stack_1_Push_m382D5229B87DB29B93033AE700C8C1E3FB21DDAE;
extern const uint32_t g_rgctx_Stack_1_get_Count_mC21782095E13FDCDFCB44605E042165E6FDC8D7D;
extern const uint32_t g_rgctx_Stack_1_Pop_m03513D8B029C7F9FDAFA3CB705269C83B815B9D9;
extern const uint32_t g_rgctx_T_tEC3BE4C57633DA8C6D5DF9ED465A3464446E99D6;
extern const uint32_t g_rgctx_Stack_1_Clear_m9395F1CE63727E0D740BEDC5FC25DCBB2F17BAEE;
extern const uint32_t g_rgctx_Enumerator_Reset_m47B84BB420EC2F188C6AB7621BE24CA4BCC59089;
extern const uint32_t g_rgctx_Node__ctor_m9A2F8B51D5C271526F24E09BB206B3A603609ECF;
extern const uint32_t g_rgctx_Enumerator_tE3D13471153F10D89BD41169FDA0185F869BA148;
extern const uint32_t g_rgctx_Node_t48CF9788D57BFC1765748046686145B53ECFF84E;
extern const uint32_t g_rgctx_Node_get_Item_m6E6BE8CF5D41B5B19D132ECCBAD45D82731896D5;
extern const uint32_t g_rgctx_Node_tB3A7E96D93AD78B79150B79C6766B43BE112C7F0;
extern const uint32_t g_rgctx_Node_get_Item_m82AACF3F97AC21E7C7D8F3F4837FEDE501BDA6CB;
extern const uint32_t g_rgctx_T_t19EA9B2E4149762388891475B99799D0C0AE1535;
static const Il2CppRGCTXDefinition s_rgctxValues[307] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t1739A0EEC47DD5827127A388AFD3B44C5F19D673 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_t1739A0EEC47DD5827127A388AFD3B44C5F19D673_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncTaskMethodBuilder_1_get_Task_m2743D6DA411C78BAE83E326226A7C801AE5046A8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tD61B3019D1869264718D125EAEE4311D26728AB7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Span_1_get_Length_m839FCF6D0CEBC13A23B4913B4B3BE1D2EF497540 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ValueListBuilder_1_Grow_m64DA028E30FA837F9D8AA340D3A9344AA8CAEB90 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Span_1_Slice_mD195CE80DA136235424FC8460CCC1F444AA8F4C1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Span_1_op_Implicit_mB474EB18DE57762E3A0772E2DE27ED5252766862 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Span_1_t44C40F6E179DE0B4F52D7DA2E1884C0EE6DE3FB3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ArrayPool_1_get_Shared_m1992B72A302318B5A74633D26273A877D27EB4EF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ArrayPool_1_t58F3AC60669102F215C83BF8491B2CFFADF20417 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ArrayPool_1_t58F3AC60669102F215C83BF8491B2CFFADF20417 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ArrayPool_1_Return_mFE24FCB54D3C02CA684BD71EA9152AED02E0B440 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ArrayPool_1_Rent_m5EF306341F1ED22323168043C5B68D869254CC35 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Span_1_op_Implicit_mC53ACE9FE1E7E5E9F3A0C098C37C5A4DCDE2FE82 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Span_1_TryCopyTo_m42CCEFF44F507F716B350717B93A19A5C5529A78 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedList_1_AddLast_m92E9EBD6A9A39368C44BA4FA908B1C7A20FBB251 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_LinkedListNode_1_t9E9525296AF0CCB2ED20655A790C2E4CD8622A56 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedListNode_1__ctor_m20FB5915DCE814C5B5ABFF83B3DB1D13FD2002B2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedList_1_InternalInsertNodeToEmptyList_m260D83D9B9C9EA04987CB40AE53195CB13DF8DB9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedList_1_InternalInsertNodeBefore_m05B6CF176F09A50E6D93416C84B856EC3E4BAF89 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedListNode_1_get_Next_mB1A01A4FC5490DBA9D6A301ED56851E198C9B4BE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedListNode_1_Invalidate_m92AA7C26274AB3F1B2C90AAD36ED50BA53479F31 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedList_1_Find_m05423F53A6D38259BBFB8C5A4DC487E2C2182B00 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedList_1_get_Count_m9716A6902AA14DDA5DD9C943647E5A06A1DBB6E9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_m78B05B517FFE11DE4D4E7F722CC8488A24712D57 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t392B3893C4126DDA49A6D8C4161C74BC1ECB9674 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tB8CC012B03969318B6B5424CAE7A44EDE0AA6665 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t392B3893C4126DDA49A6D8C4161C74BC1ECB9674 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m6B7ECAC5C3E3877985F42ACB5093D9C5A28CCEBF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t82440D780591BF278D7D247E299FBB6BA82EFC94 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator__ctor_mE01A798BE61864D38C425E7B6BE7935FB7850D7D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedList_1_GetEnumerator_m8083B3D2C3FAE458314291AD1BA07D81465BE251 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedList_1_InternalRemoveNode_mDA777B72523F891BA292ADFA7EEB3ED1C7889689 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedList_1_ValidateNode_m131AE8D054B03E0FBCE81CEED06D9D1EB793E9C5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_tB7945C667DD94B711E283C8F803CF9F9F3DF9C5E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedList_1_CopyTo_m7F21A67D5C258A0B54301C4A6A402621CC82D581 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TU5BU5D_tB7945C667DD94B711E283C8F803CF9F9F3DF9C5E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_tB7945C667DD94B711E283C8F803CF9F9F3DF9C5E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_LinkedList_1_t2BE688DBB804B0F90493D85355AF0536C5697D2D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LinkedList_1_get_Count_m00D92A248A5513B9C324C4F8A7EEAAE4A1AB9A85 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t40ED5F405E4C66916984D6B18DAD11FB5460B70E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Array_Empty_TisTKey_t1FD6F73134BEFD3DA81B6A46436C50FA38749EC7_m962F8D7E72A72B5866FC788E97F3050DBF2D2EB6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Array_Empty_TisTValue_t05B39B62795218B9FB2F0D6E1C002FB430DD6D0C_mDCBE7E1087DB97D086F71B6F5758E1ED70F712F2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Comparer_1_get_Default_mCA970F420D07D5E98EB02857280D3EEBCA160863 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Comparer_1_tB1C14895A92A1CFDFEC8C2B1B3A8AAB2774915B1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TKey_t1FD6F73134BEFD3DA81B6A46436C50FA38749EC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Array_BinarySearch_TisTKey_t1FD6F73134BEFD3DA81B6A46436C50FA38749EC7_mB622ACDC6278218422284DF9F8C56A15C318A6D0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_Insert_mCC6515C2D0BC090B63A93434CB515DA7B30ACA96 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Key_m5E4415016462D57B44F58AD0F77CF776F27E7AE5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Value_m4D424FA5AA51495D1B7C4303F24F504CABFCCFD7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_Add_m5FFD3B1C5235EC7DE6A9C6D8D9910CC27A9AC1DA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_IndexOfKey_m6481FD095B461438A1FE2B37991097DDA1D815DD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_mE82F170302E1DA99A2944801AD9A2DFC2F663119 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t0A97056FB9DAAF5FD6C7ED54906FF1D95D3B49E8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t0A97056FB9DAAF5FD6C7ED54906FF1D95D3B49E8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m633EB6F895BE1F9A3BCD62EFADBB54D45AFC4591 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_RemoveAt_m8EA545535EE75D4319AC2A2221F053E68D6A6F86 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TKeyU5BU5D_t2AEF08C06DBDD8A7D8816C365C3F304B53414C64 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TValueU5BU5D_t9D4C3EA64452F51826D853179BD45565DDE079D9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_GetKeyListHelper_m11D88DA06F385E4C81657ECE8534B2D59DC983FF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_GetValueListHelper_mB1BDECD9A5E6318EF46360981E69224312CB4835 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_KeyList_tBF0BA35A8E56C2CA6B0D132E81B49AAC9D037ABE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyList__ctor_m9E800CDF1F22A710C1B85E30E2CE048FC4F42188 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ValueList_t0BD3E7798079666543FFC993879C507993BFFEE7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ValueList__ctor_m2C2D0F1A586CC2C94A51E4AD4078C4E81AAE88BE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_IsCompatibleKey_m292C7E9BFADCEEBC3D94DC79A480ED45C9EFCD25 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedList_2_t93A9CE8FB30B3C390C759153CE3225118DD18523 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_ContainsKey_mD43E2CDBF2912D4CF756124303F08E7877325A0D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_IndexOfValue_m0C27E1E3CA435B9541143BED59421495454F76CC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_get_Count_m70084C51D5559257162A8D6731A683D970CF165D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2__ctor_mBF3AA820F6A5A5EA96CE7D4F04AFA2421C066B26 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_KeyValuePair_2U5BU5D_t69B802D18DD5300031ED91A70D5E8EDCD78EF679 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_KeyValuePair_2_t26603B18F49F78166CF21E4D82F3A76FFAF80D97 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_set_Capacity_m16837BB431C0E2C7CF5FCEA5F6B3CB051C4EC3EA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t26F0DFAF5C765B8DF9CB0D8D7A6FB770C29F0DA4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator__ctor_mAA4F2E43EE68D2CD181E9A0D6CC81FFC7591D301 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TKey_t1FD6F73134BEFD3DA81B6A46436C50FA38749EC7_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TValue_t05B39B62795218B9FB2F0D6E1C002FB430DD6D0C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_set_Item_m44BAA94EE58C1BD2E91CDABEBFF3F62D0702E4A7 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TValue_t05B39B62795218B9FB2F0D6E1C002FB430DD6D0C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Array_IndexOf_TisTValue_t05B39B62795218B9FB2F0D6E1C002FB430DD6D0C_m50CBD31A2C8613FCF3A49C18828951D925CB3A0E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_EnsureCapacity_m9AFA49D189786022026E89A084CD32F0ED866E92 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedList_2_tC6344DFC770CE08ABFDA57B902B4868D9F62EF3C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_get_Count_mCE323B5DCB7E363E70AF36EC76133C1D587B8EC8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TKey_tFD55C737F5BFBDA0AB5E5CCF01864765DD83FFA9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TValue_tB72A46C4943C2725B8BC470E058E8E390260E574 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_KeyValuePair_2_t28C5B701CDAEA1E9D5ABF27CE326C6E45F61F839 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2__ctor_m8F2D8B7678D0CFB7534B3E9F456DABCBC7D516D0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedList_2_tD5CDAF17B1E37289C3F51E62B7B25339B0E1C593 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_get_Count_mD33CE88877A87FEB37A6995D59C4F8F680D1E95B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TKey_tAFE4038E78BD9702B4A7F65620EDE61B56661C62 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedList_2_t451D95A193728D5626E4C2CA2CCC24125C4FE651 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_get_Count_m18B7E97F1D1E1482208D6592D480F8DDCFB765E5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TValue_tD97A06AA0F2E68B3B8DD39F7B451DF1FCB143DD1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedList_2_t61AEDECC6DF909FAB02D0487A698CA9EE9EB5063 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_ContainsKey_m4A88F19F2A995D960523ADB86539A226F96270D2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_get_Count_mEEFDA802B250F387AF6DA9968FE4F23319DE7145 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_GetKey_m2D600345BA732A25B665312432E50E3DF78A6AA3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedListKeyEnumerator_tD25E4501C93F8996465E698CF2E30942795FF24F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedListKeyEnumerator__ctor_m28CBE46D4F471D562C6E9BF643EEBEE97FAC49A2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TKey_t299F6A80A11873E87035AB041EBF0A5F15103B73 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Array_BinarySearch_TisTKey_t299F6A80A11873E87035AB041EBF0A5F15103B73_mF0629A26CAB34F85B81363205BE184CBD08961E5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedList_2_tEF86C98A7E9ABE5CE49C66355C8C437FA26BC3CD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_ContainsValue_m27B9FF070EEA5DBA0145190F4D3F7A2F9B31C62D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_get_Count_mD3233393E8C9A5AA14992E1D63A185032E77D8DC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_GetByIndex_m53C32801F4EA95F9423C85DC10D062A6F118F43A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedListValueEnumerator_t2C333B1C34817C77FE472AF6C5C121C216A1E0BC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedListValueEnumerator__ctor_m5888F52C713CBACC1D3039B513BB385DCCFFAC84 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Array_IndexOf_TisTValue_tEB96A977159351D0F21BC37F853A4450F0186BAF_m2A579BC9630F279050FFC0C41214B3CAFD81E0AF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Comparer_1_get_Default_m81161DF68C8FBA662C333FE110750998123D884A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Comparer_1_t5039FF605105E34CA6CD5BDA36586D613105B2FD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_t6B95CE12042047894ECBA60DB0E7A0452A6251AD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerable_1_GetEnumerator_mD40A8AD1F465F0E302A1F112989CA720FF5ECE93 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_tDA871FF7BB1C2D3DC55FE1D1EAA2F93315EA2E52 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m287FD06D0787C3CA29DBF21E2FDB02908B8092EF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedSet_1_t1907F34FCD326D284C3E76A5B27C1FBEFF766F07 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_Contains_m2219DF60D3DD8333C6D005CB75C808584CDCD618 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_get_Count_mC104C70642BAE4409B1320CC4CCEBEB190E3AF09 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_Log2_m1650455008CF18578C3DE966B4D8F93659241712 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedSet_1_t1907F34FCD326D284C3E76A5B27C1FBEFF766F07 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Stack_1_tB9949F83B69CE69B76144CEDBB176334EB1A729F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1__ctor_m942E509B7108B6D8E0EDD22464AB3282ADA43783 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_Push_m2508C7B1C3E4AF0FA5500C42363499A4B8C5D0F8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Node_t82E36EC90A91286397D275B6DF030BB9AC0A8928 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Left_m1B81AE120F05F79696BC09346383D9569F5B405F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_Pop_mC5CDB6147755218C228C81F58089586C1417D5B1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TreeWalkPredicate_1_tFE86EA1E52CCF1469E47E49C2ECEBD6853E01DD4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeWalkPredicate_1_Invoke_m359AF12F4621B26E63B5A91D6326BF826604571B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Right_mE6DC74A026D29D98A62CFF9A993D06DB21CEB39D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_get_Count_mF3C9F422F43D522282E45CE41A28CF23083410DF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Queue_1_t5B11105E5DAFD5ED8D1EE047AE9383A72B2525FD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Queue_1__ctor_m0636156497B54BD8AA8EA1C4B616BC5B293D0C14 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Queue_1_Enqueue_m7C06EEC26B9085D133DB3C9E2914B6376C29686C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Queue_1_Dequeue_mC9FDECD303283ACEDB593E45DD2D51865173C5B9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Queue_1_get_Count_m1E085883C454F700E75F0355F94ED7B40D18D9EF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_VersionCheck_mA8C684355B26D37C895062307E92E5254608FD39 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_AddIfNotPresent_m71C0A08495E6C7474FCA314874B2E28925A136C2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_Add_m2F7713BF3F81075944513ECD5227300094B847D6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node__ctor_mB33B037BE5D30C7C75983A1243CAA6EEA0B63D24 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Item_m3714B8239BBE992251A9854F8BAD29394C4C4DCC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IComparer_1_tA4CC6C6451C9A880FD54FD0B9F099C5E2450F0B5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IComparer_1_Compare_m3E7FCB9431321AC8AC4E01B5D9BE553F4AD5F72A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_ColorBlack_mAE1C842B0ADCAD9278516541675B2C60DD16E747 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Is4Node_m0AEE6A8C6C200AD1297E911305DF47A4E57E9DE4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_Split4Node_m20540150E17B57106BC79DCBCC53A54125D5DF33 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_IsNonNullRed_m7F5B238C17B99A71527461C226121250060C78C8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Node_t82E36EC90A91286397D275B6DF030BB9AC0A8928 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_InsertionBalance_m2E7CB6DD9BFAF675D452DEC11247565782178B38 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_set_Right_m7FF10BDB5E9DF2CB50B3A6D0EC707C0F9563252A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_set_Left_mDEE0FF492989054F7B4DD2543B632078BEDBDA8C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_IsRed_m6B0C04892AFBD6D97CFEEDC02895334C7BF92209 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_DoRemove_mB639696863EB5BBCB3984C43F013301857858F36 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Is2Node_m5A6A7791B4E02625C06349371FEDB90A42FDCF8F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_ColorRed_m1D853D12EB4E90AFFD4B0CCB4640E1F54C0D3FDC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_GetSibling_mB9A6C2A662F51A326E0C477D8656D0020A93F01E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_RotateLeft_mCAAF2FF045CBA2B4CB660A666415EDC94D8CA0B3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_RotateRight_mB0EAF0E8375AFCEC0D188E169772F90544C401E6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_ReplaceChildOrRoot_mCAE4C51F4A83F9A37E6EA0CC0823A27F6B986526 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_Merge2Nodes_mBA890F94A5A184D2EE899E7DD9693B835F3FFC51 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_GetRotation_m5881ECAF411C9157256F7968E41E0A00DC7BFBC6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_Rotate_mA1A9DE28DAE9F321D7BBD43D4E4AF87A01A05E22 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Color_m8FBAA51EEE4B8FB8C94DB92EC36F834200EC17E6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_set_Color_mF6C582CB62F168A13D7FD5543EBEAB080AC306F6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_ReplaceNode_m6AD1593223B0FD0FEC01FCBBFBD0D3DCBC6A00E8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_FindNode_m76DF155903D2383BF56389E0646D05C3B6AA64DE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_CopyTo_m2DC7417BEC6FFBE4DA465554BF83BB48BE4FEC5F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass52_0_tCEBD2F97696032E4FBE349AA1210B23D96506149 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass52_0__ctor_m8AC1A17E0080C18E282D3F42B792C905598843A1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass52_0_U3CCopyToU3Eb__0_m50120C9456BC4D9DDBDDAD9B36128CD275866496 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeWalkPredicate_1__ctor_m2EC81102633E7571439783E0F3D6CC9188D8F478 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_InOrderTreeWalk_mCA504DEF419D02C3724C1F99408A028A080CAA05 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass53_0_t8088ED0364B7C35699D0B5A61069F7A81898D400 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass53_0__ctor_mA919A8E6EA3A46A93D247ACFE5CB565C263CE1EF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_t38D4E7F7E8DBB01B0EBAAD3562A9C876C9A1AE90 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_CopyTo_mB673D121FF323A52F849ED9579DE271A611404EF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass53_0_U3CSystem_Collections_ICollection_CopyToU3Eb__0_m45B1ED8D9DCDFD3FC34DDD3B54CAA5EEDC06FE6E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t8BC4FE1AD604723F9025C54884ECB41F02B3A5FA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator__ctor_m9AA50DEE926AE516D473C8C14E0B89C64DF1D67E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_GetEnumerator_mE9EC394A6E10A0F03A8C874130ED9DB8B3E78C80 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_RotateRightLeft_mF29D9CBA32EC9A41A3D597EE27E4EE55238036A7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_RotateLeftRight_mAFF20EF80EF7845FF2C2B54CB07E16E9A3C01023 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_ReplaceChild_m0905C8DEE7675F2007B79F11BE4540E5AD0B96D8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_get_Comparer_m3FCFEA52D823FB8D002097FDD335FE32084B2BCA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_HasEqualComparer_m5614B1B0D497A8B85F60D94676E7F60B6F6EF460 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_IsSubsetOfSortedSetWithSameComparer_mD8E8014C3DB9349C02F15B554D179C9C46E16F6C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_CheckUniqueAndUnfoundElements_m5A8E968A794EE22740ABBF2821268A04AAE4A17F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_get_Min_mAA1072E42B73ED568E924E47CD55552EA76991C6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_get_Max_mDD55BB8934DCBEE0CC220676C3E6CBC0AD7A1A25 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_GetViewBetween_mE1BA349B377820B7BF85E132B4CAFE0B0F97F47E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_m2F5948D8C91CCAD7712F19D96F8EFF7862BA54EF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m459316E149C41B62A241289E5AB75C9859B482E4 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_t8BC4FE1AD604723F9025C54884ECB41F02B3A5FA_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_ContainsAllElements_mE9F8F523D36D3E0917C9F1DF560728BD813B469A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ICollection_1_t0FD4FD68B67F09E5275B4BEA388B206C21808678 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_get_Count_m29B767DD800FB48F84AAF3C2200C6D76A2387BD1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_InternalIndexOf_m432B2692A04D1A65A78C87BDACC3226E2C5A63D3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_get_MinInternal_mCAA7B77A011C15215195ED5FF87CD45CCA5DAC75 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_get_MaxInternal_mEE7A1C1F172F7E97304B79AE9ECC2893CCF75BFB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TreeSubSet_t11031D0C3F3BD38BA7D1B987A577956EACDF3722 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeSubSet__ctor_m5912F6D1C36A0E2A19F011A46C1C20A46122CFE7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_GetObjectData_m194013DE3BE7EF3670E5E242F117EBB37405C0C0 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_IComparer_1_tA4CC6C6451C9A880FD54FD0B9F099C5E2450F0B5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_t38D4E7F7E8DBB01B0EBAAD3562A9C876C9A1AE90 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TU5BU5D_t38D4E7F7E8DBB01B0EBAAD3562A9C876C9A1AE90 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_OnDeserialization_m56D5CCDACFBD2FA545DED35FF75E40199A8CEA2F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedSet_1_t43A6E6F1EE955185EE7346752FDF6FBEF32E977E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_get_Comparer_mFE128A5B4F49C2EA60B963B62687C3EF1CC4357F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedSet_1_t43A6E6F1EE955185EE7346752FDF6FBEF32E977E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1__ctor_mCC54033B6994BE0C0436B2A4E2E1E9E54F518909 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_FindRange_m6C2E0847E0A1D93E9305D4D6FA0966756336A93F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeSubSet_VersionCheckImpl_m11741508F15663C215ADE5B3ACC1FC96AE79D008 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_IsWithinRange_m2A01B405926FFD5E5657731B8243D2A28D2A5A1E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_AddIfNotPresent_m4598BD090A7927D25E15D3B9A215C59B8BE0C679 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_VersionCheck_mC854A48934AC64C9FC9C547AE6E0C7410EC5C11E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_Contains_mE01367F9BA3002F5F27AEB8D78600FE69FADEB49 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_Remove_m47EEA0987A62D2814A5D4DBF556103AF1225531A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass9_0_tB5B1D770456865055F9E8C7106606A9E032581F3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass9_0__ctor_mA3541BF8A43702221A4EBC8E04607606F814451C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tE50A1BC80D4D5D592B4FE3EA9D11D5169BC485FE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mB0D7186913E3B8D1736FB8F0D8D05496B0C44FB1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass9_0_U3CClearU3Eb__0_mA73D115A4A7511C9160E735933A28A997F231724 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TreeWalkPredicate_1_tAB63AA224A61328019B75B973C6FE345BFDD0965 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeWalkPredicate_1__ctor_mF38B1A004478BADD63F9045E439FC96B695B2D44 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_BreadthFirstTreeWalk_m01201BDA6434C8D3B3A4A7E85332497E78A29438 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m643271998633167ADF6CC1FF093CD68E5DF4856A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_mEBA579E5EA942E62B03C8DF2E4996CAE5CD0C51E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_RemoveAt_mFF8B8E0BC795E162CCE29463F20F55006590ADD0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IComparer_1_t56DDBA8A8572C6FA3107D49727EB234650E07074 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IComparer_1_Compare_m893082C4F4E348460B821DC9C8B0D00D1AE98F9E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Node_t913BCFA91C36149C5EBE9E8F8E79C07A4A569C49 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Item_m0865E8B1EE3EB1EF3BB092C0A959464F05C52A72 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Right_mAAED6C6C5634B2CD39AB505EFB39FABBBDACE5B7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Left_m015F38662504C89419622BDFD1CCB466F15F28B5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_Log2_mBD3DFE2D3846B591246C9A61D3CB87CB768EA7C2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Stack_1_tC64AC7B73A5B2C2DAE7F1C8F0B5E73AF1FB50DE4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1__ctor_m94C5FDACA0AD705E8951E8DED135A9AE73A832C6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_Push_m391FB8AC34C956640A7B76E2556B4C2ACA5C2C59 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_Pop_m7DC8E301CCF53A8A8A28BB38CFECCFAD88D65B4D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeWalkPredicate_1_Invoke_m8271CF1F5A69C9B6E4A0947E6C1D0CFE3919B8D5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_get_Count_m18348F72DB5AA9ED8C5E77CF8A860FC9159D1E13 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Queue_1_t69155CF6A7D53685D085C20AF7F3FC5B61F62B6B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Queue_1__ctor_m612569F5AEF7FCB678587D85C5DA08D57FA07F17 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Queue_1_Enqueue_mE1ECB0BA1B5DBEB29CBC9AB36CABA4E8A8B9A6C6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Queue_1_Dequeue_m2DBFF9D78655F1693614797A90DA1404A54D3122 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Queue_1_get_Count_m342BDD91C8D4B202D5D665CABFD3E7E9CD8FB3D3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_FindNode_m255D4CECBEF2766935E09CF3DBCDCD1A7F119DAC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_GetEnumerator_m85D1ED2641B279AC27F2F6F8A89F72ED8C1E001A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_m571F36276E19A196EFB3F0E1318579877BD1C48E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m4DA58684CA02D9C6D05CB40D4079B1BF16A6FFC6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_tA292E74B09FB96746A52917C0EE2776A5E4E7351 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_tA292E74B09FB96746A52917C0EE2776A5E4E7351_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TreeSubSet_U3CVersionCheckImplU3Eb__20_0_m21F05956E412EEF05BC4F514894400FE3876A98E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_InOrderTreeWalk_m2A46E038A8F7EF23E7EEFA0346BF8137E016F9C5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_GetViewBetween_m64D00640776AB63026EC8EF223EC773733459F6B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TreeSubSet_tA7068D58BDA25F6A8E6756DE8EDC3F41153BF041 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_GetObjectData_mE2BC6A2D2A4266F880CE3056AA774E6981AE78F3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Node_tE7F92C0B789BFDF3C8CA7FC08903B10773AE9952 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Item_mFE1783AE419A4F13230B7833B6770E3742AF4A15 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t634977CF38B2AC837D3A5E2FF97542FAA6D24D4F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_m673EDC30EE2AFE1D3A5AF4A537E836BD11B365CD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_set_Item_mBDEA632B81A444A208273CACB31B451B0E4E91C2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_set_Color_m3B08B60B56F2738AD25411041B8918C741CCACC8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Node_tA753FBEAFB7C0E9F30B03C4E16C2CAEA64439C50 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_IsRed_m69D3CDCED0355BC1AE9004E605E9814B29948B1E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_IsBlack_m6D6B36CEEE64BCC6D20DCD3E27168CC5AFA93C62 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Color_mFB7CE6196D794811BB602F746371034BF004212A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Left_m151DC7DFEA0F0FC03A5E144C64D0B201C3AA4188 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_IsNullOrBlack_mAC38C6192A2289B4EC04AB5AD75A89AD37281471 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Node_tA753FBEAFB7C0E9F30B03C4E16C2CAEA64439C50 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Right_mEC2BB6CD2C52DA7D536E1D3264307AF21BB6A304 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_IsNonNullRed_m8726A1EB92D947D9C662369FBAD54196DEE5A6C5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_ColorRed_mFD53D03A936EC08EEAB5CC40649F8BF8AFDECD88 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_ColorBlack_mE73E1C5DE0C000C8F70340E7E12E582D24B70D39 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_RotateRight_mDF0183FB5D16BB7A3BDA76DC367ED7B0DCCC0969 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_RotateLeft_m070AA41D31D10BAE5BB783265AB98A9E5B2B49BA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_RotateRightLeft_m2BCE860A84B10C33D17E18AC29821A3933F70B3D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_RotateLeftRight_m060152F4001B6F4D92B29516F6C7D7C0E63B48F1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_set_Right_m5DFBA8D32B594D5517CD13462D4E2920DC8F5081 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_set_Left_m9CC6AA74944B09D6AA015952BDF4847017A164F7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator__ctor_m954D8F8F6F3981A96AAEC85FD47550644D465C8B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedSet_1_t175785C97E60DE6415DF7BC21055FDAA39137573 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_VersionCheck_m6DCAC4CABF454C0D3B668EDB8C59FCA547023A63 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_get_Count_mC69B3F3F4D830F7343E5132962C6A7769115CED6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_Log2_m9B33E9CBCAE0BE849D996F4718152EF9F33E8B60 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedSet_1_t175785C97E60DE6415DF7BC21055FDAA39137573 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Stack_1_t721167C43AC5B7BAE2AD13C232E9818E61C89EC8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1__ctor_mE763684B8DA1F87BC06FA5051A88C512BC58A7E6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_Initialize_m034333F910E78A165DDA546B9D5144616DC1F737 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Node_t73DAB7B67962EBCFB9C2284DC57069617B37FC2F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Left_mEB7832AF38FD9A15937D357D24C21E1E7455E98C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Right_m787994C7F76FFA37162A51EAE03E7E98C3C44008 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Item_mC576D187E3FF5326BEE6ACDC9E0AF4223F91C76A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_IsWithinRange_m54A107906ADD96D310473D218B293476BD8FEC3C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_Push_m382D5229B87DB29B93033AE700C8C1E3FB21DDAE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_get_Count_mC21782095E13FDCDFCB44605E042165E6FDC8D7D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_Pop_m03513D8B029C7F9FDAFA3CB705269C83B815B9D9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tEC3BE4C57633DA8C6D5DF9ED465A3464446E99D6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_Clear_m9395F1CE63727E0D740BEDC5FC25DCBB2F17BAEE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_Reset_m47B84BB420EC2F188C6AB7621BE24CA4BCC59089 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node__ctor_m9A2F8B51D5C271526F24E09BB206B3A603609ECF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_tE3D13471153F10D89BD41169FDA0185F869BA148 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Node_t48CF9788D57BFC1765748046686145B53ECFF84E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Item_m6E6BE8CF5D41B5B19D132ECCBAD45D82731896D5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Node_tB3A7E96D93AD78B79150B79C6766B43BE112C7F0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Node_get_Item_m82AACF3F97AC21E7C7D8F3F4837FEDE501BDA6CB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t19EA9B2E4149762388891475B99799D0C0AE1535 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_CodeGenModule;
const Il2CppCodeGenModule g_System_CodeGenModule = 
{
	"System.dll",
	3340,
	s_methodPointers,
	39,
	s_adjustorThunks,
	s_InvokerIndices,
	15,
	s_reversePInvokeIndices,
	19,
	s_rgctxIndices,
	307,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

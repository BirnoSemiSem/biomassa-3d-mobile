﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.String UnityEngine.PhysicsScene2D::ToString()
extern void PhysicsScene2D_ToString_mACA22EF575F7544176360B16D431F2BB1DEBB307 (void);
// 0x00000002 System.Boolean UnityEngine.PhysicsScene2D::op_Inequality(UnityEngine.PhysicsScene2D,UnityEngine.PhysicsScene2D)
extern void PhysicsScene2D_op_Inequality_m7B70D9A8FA4E5ACC382E83862AE6BF7EE4CD22BB (void);
// 0x00000003 System.Int32 UnityEngine.PhysicsScene2D::GetHashCode()
extern void PhysicsScene2D_GetHashCode_mD45B3437D088C66A35AE20066AD632D1D0858B1E (void);
// 0x00000004 System.Boolean UnityEngine.PhysicsScene2D::Equals(System.Object)
extern void PhysicsScene2D_Equals_m4A19DE0675BD596A1B5AC0F7138A9A6F4D6029B3 (void);
// 0x00000005 System.Boolean UnityEngine.PhysicsScene2D::Equals(UnityEngine.PhysicsScene2D)
extern void PhysicsScene2D_Equals_mA7C243A71CFDBFA905F057CE3E9C5E61B34216FB (void);
// 0x00000006 System.Boolean UnityEngine.PhysicsScene2D::IsValid()
extern void PhysicsScene2D_IsValid_m3C273A4AC45E6E9994DCBC66D11E003200FAF50C (void);
// 0x00000007 System.Boolean UnityEngine.PhysicsScene2D::IsValid_Internal(UnityEngine.PhysicsScene2D)
extern void PhysicsScene2D_IsValid_Internal_m18461F0649F3666D082886E1785B99AD1BC6380A (void);
// 0x00000008 System.Boolean UnityEngine.PhysicsScene2D::Simulate(System.Single)
extern void PhysicsScene2D_Simulate_m2210AE79B5D4713DA5BEFD4EB5857778651DCE62 (void);
// 0x00000009 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern void PhysicsScene2D_Raycast_m5A2D66F6E7E8F34B6CF5B82099EFA4F69155F25D (void);
// 0x0000000A UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void PhysicsScene2D_Raycast_m74A71D9DBCC2CCD7454240AE784CEE5720E55EA0 (void);
// 0x0000000B UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void PhysicsScene2D_Raycast_Internal_mDD401F1DAD381C0725F0F9CB65E6B23DC78D3C5A (void);
// 0x0000000C System.Int32 UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_Raycast_m004884696543F60917C1ED72374C1528207229C3 (void);
// 0x0000000D System.Int32 UnityEngine.PhysicsScene2D::RaycastArray_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_RaycastArray_Internal_m910B194258C3609328EC0DAD1C288F67B81FD030 (void);
// 0x0000000E System.Int32 UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_Raycast_m541841D244633BA234ED72B01204161686D6B3B9 (void);
// 0x0000000F System.Int32 UnityEngine.PhysicsScene2D::RaycastList_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_RaycastList_Internal_m990B80E682F006388ADFE1FEC61165FB21A58138 (void);
// 0x00000010 System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersection(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit2D[],System.Int32)
extern void PhysicsScene2D_GetRayIntersection_mF3E0EC0D4F5A4B8C063E735979C851ED5B4B4C2E (void);
// 0x00000011 System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersectionArray_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_GetRayIntersectionArray_Internal_mF48B0A8D9804F7938C2513B937602347DA4768AA (void);
// 0x00000012 System.Boolean UnityEngine.PhysicsScene2D::IsValid_Internal_Injected(UnityEngine.PhysicsScene2D&)
extern void PhysicsScene2D_IsValid_Internal_Injected_m0E74DF97D3BAE7CA3FE64D5A8429C496072569F1 (void);
// 0x00000013 System.Void UnityEngine.PhysicsScene2D::Raycast_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern void PhysicsScene2D_Raycast_Internal_Injected_mD6B513DC491DA368725681DDC4E4104C72650B77 (void);
// 0x00000014 System.Int32 UnityEngine.PhysicsScene2D::RaycastArray_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_RaycastArray_Internal_Injected_mE5A33C2FE2873FABD52129DC539856AD762FA3EE (void);
// 0x00000015 System.Int32 UnityEngine.PhysicsScene2D::RaycastList_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_RaycastList_Internal_Injected_mA190F786687B287B75E904E96C25DCE43422CF18 (void);
// 0x00000016 System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersectionArray_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_GetRayIntersectionArray_Internal_Injected_mCD438768C11300F4CA7444621467016833562054 (void);
// 0x00000017 UnityEngine.PhysicsScene2D UnityEngine.PhysicsSceneExtensions2D::GetPhysicsScene2D(UnityEngine.SceneManagement.Scene)
extern void PhysicsSceneExtensions2D_GetPhysicsScene2D_m578B450FE4359087F086A575817F7FDCC6EC0AD7 (void);
// 0x00000018 UnityEngine.PhysicsScene2D UnityEngine.PhysicsSceneExtensions2D::GetPhysicsScene_Internal(UnityEngine.SceneManagement.Scene)
extern void PhysicsSceneExtensions2D_GetPhysicsScene_Internal_mFC821A94DE88F39B0FE9854D858C7273265072D7 (void);
// 0x00000019 System.Void UnityEngine.PhysicsSceneExtensions2D::GetPhysicsScene_Internal_Injected(UnityEngine.SceneManagement.Scene&,UnityEngine.PhysicsScene2D&)
extern void PhysicsSceneExtensions2D_GetPhysicsScene_Internal_Injected_mA3EE076447D7497740B95821A47B41BF8DF0417B (void);
// 0x0000001A UnityEngine.PhysicsScene2D UnityEngine.Physics2D::get_defaultPhysicsScene()
extern void Physics2D_get_defaultPhysicsScene_m10876DD92CFE85E080A574F23FF30B044C03C9BD (void);
// 0x0000001B System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
extern void Physics2D_get_queriesHitTriggers_m43C69B7B5475256AD329817C09D9D6C00EFB540E (void);
// 0x0000001C System.Boolean UnityEngine.Physics2D::Simulate_Internal(UnityEngine.PhysicsScene2D,System.Single)
extern void Physics2D_Simulate_Internal_m3A1A3BFA605D5E39DB9A49EEEF572AC7A62221D7 (void);
// 0x0000001D UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Physics2D_Raycast_m8D056DA6FCBB18FD6F64BA247C16E050B0DBBF18 (void);
// 0x0000001E UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void Physics2D_Raycast_m274780A393AF3EEAB93D4DAF2A5DD95706D5A2CB (void);
// 0x0000001F UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern void Physics2D_Raycast_m6B89CCCAF549D1917B95F663E007382899E66BA7 (void);
// 0x00000020 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern void Physics2D_Raycast_mED174EF64683E4E98190E82039AF753933585A5E (void);
// 0x00000021 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern void Physics2D_Raycast_m4F22AD6D93E283DCBFF1DF2F3B67988BFE092DAA (void);
// 0x00000022 System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void Physics2D_Raycast_mB3DEDF9E9B738EAB8ABB4CD6AF28E169EE329D46 (void);
// 0x00000023 System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[],System.Single)
extern void Physics2D_Raycast_mCD80B702BD6EB5FEBC8B4D3B066DDD8ED0FE4652 (void);
// 0x00000024 System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>,System.Single)
extern void Physics2D_Raycast_mCC46614BD92F9889D2F27AD6DFC24DB76131155D (void);
// 0x00000025 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void Physics2D_RaycastAll_mC9E86E526CD2A3B7A7CB4C1F29F1F482694F77D6 (void);
// 0x00000026 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void Physics2D_RaycastAll_Internal_m55AC316F374C07D90B550B231D135BEA8F8C31ED (void);
// 0x00000027 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray)
extern void Physics2D_GetRayIntersectionAll_m0DF776894E1E3A8C03644D001938AD8B435C6C65 (void);
// 0x00000028 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single)
extern void Physics2D_GetRayIntersectionAll_m0B3510E637D87DA64270E0B699C9E3AA28384368 (void);
// 0x00000029 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_m313BB4020C5DE25EDE1E23460C15CD5F44171C9A (void);
// 0x0000002A UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_Internal_mD38082338EF336C1B44572D8D9E608CFEE0C7A50 (void);
// 0x0000002B System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[])
extern void Physics2D_GetRayIntersectionNonAlloc_m931B8F65C30CFB7234D3CF51F296DE158422B12D (void);
// 0x0000002C System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single)
extern void Physics2D_GetRayIntersectionNonAlloc_mE7B60A6CA6A5513B95D4714050DBA185D2F6FC52 (void);
// 0x0000002D System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionNonAlloc_mBE2332B21D30A74DCB161D48A629B32D747FB755 (void);
// 0x0000002E System.Void UnityEngine.Physics2D::.cctor()
extern void Physics2D__cctor_m538A4AED462ACB3482F9C397DE0B72BCDA2D3CFB (void);
// 0x0000002F System.Boolean UnityEngine.Physics2D::Simulate_Internal_Injected(UnityEngine.PhysicsScene2D&,System.Single)
extern void Physics2D_Simulate_Internal_Injected_m96924B9A71DFF35B53F205A54C89F7494C8FCC3F (void);
// 0x00000030 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
extern void Physics2D_RaycastAll_Internal_Injected_m1DBAE3FDFBFDB3E8CCD49E2D4F286265196FEC0B (void);
// 0x00000031 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_Internal_Injected_m64C09D534E1B44E2B742C13EA712DE97FEB5EC57 (void);
// 0x00000032 System.Void UnityEngine.ContactFilter2D::CheckConsistency()
extern void ContactFilter2D_CheckConsistency_mD918F11F977EA35E87CF491F7AE8794F5D01DF72 (void);
// 0x00000033 System.Void UnityEngine.ContactFilter2D::SetLayerMask(UnityEngine.LayerMask)
extern void ContactFilter2D_SetLayerMask_mC3FBC2D806C1A3ACB2D060CE48F8157505E42F9B (void);
// 0x00000034 System.Void UnityEngine.ContactFilter2D::SetDepth(System.Single,System.Single)
extern void ContactFilter2D_SetDepth_mE614DDDDAEA489D150E61D2DF8104F9292236F18 (void);
// 0x00000035 UnityEngine.ContactFilter2D UnityEngine.ContactFilter2D::CreateLegacyFilter(System.Int32,System.Single,System.Single)
extern void ContactFilter2D_CreateLegacyFilter_mE33DF262BE7E9E49D7CAF5B990450FE1A78C30A3 (void);
// 0x00000036 System.Void UnityEngine.ContactFilter2D::CheckConsistency_Injected(UnityEngine.ContactFilter2D&)
extern void ContactFilter2D_CheckConsistency_Injected_mD1E46463DF2E3C1FA3E651478E62023365937B0C (void);
// 0x00000037 UnityEngine.Collider2D UnityEngine.Collision2D::get_collider()
extern void Collision2D_get_collider_m90FA98F6619E9F1E2EFAE8132EDB6ECA1A2C4F37 (void);
// 0x00000038 UnityEngine.Rigidbody2D UnityEngine.Collision2D::get_rigidbody()
extern void Collision2D_get_rigidbody_mD763F2D56BF538A94AD62379D22E42335A67B60D (void);
// 0x00000039 UnityEngine.Transform UnityEngine.Collision2D::get_transform()
extern void Collision2D_get_transform_mC59737F246B2DAFF2AB4F6322664C87B28605CC7 (void);
// 0x0000003A UnityEngine.Vector2 UnityEngine.Collision2D::get_relativeVelocity()
extern void Collision2D_get_relativeVelocity_m1F0BB90BC73FB0A0EA27212D832BB3F26D4C004A (void);
// 0x0000003B UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern void RaycastHit2D_get_point_mB35E988E9E04328EFE926228A18334326721A36B (void);
// 0x0000003C UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern void RaycastHit2D_get_normal_m75F1EBDE347BACEB5A6A6AA72543C740806AB5F2 (void);
// 0x0000003D System.Single UnityEngine.RaycastHit2D::get_distance()
extern void RaycastHit2D_get_distance_mD0FE1482E2768CF587AFB65488459697EAB64613 (void);
// 0x0000003E UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern void RaycastHit2D_get_collider_mB56DFCD16B708852EEBDBB490BC8665DBF7487FD (void);
// 0x0000003F UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_position()
extern void Rigidbody2D_get_position_m07070C4416DFE2229070F95B349E411AE4869276 (void);
// 0x00000040 System.Void UnityEngine.Rigidbody2D::MovePosition(UnityEngine.Vector2)
extern void Rigidbody2D_MovePosition_m7F24879BB78DA0587168B257C56DCFD248A90895 (void);
// 0x00000041 UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern void Rigidbody2D_get_velocity_mBD8AC6F93F0E24CC41D2361BCEF74F81303720EF (void);
// 0x00000042 System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern void Rigidbody2D_set_velocity_m9335C5883B218F6FCDF7E229AC96232FCBAC4CE6 (void);
// 0x00000043 System.Single UnityEngine.Rigidbody2D::get_angularVelocity()
extern void Rigidbody2D_get_angularVelocity_mAD2505FB1F8C9E1A66D1EA8F8680D14380BFC58D (void);
// 0x00000044 System.Void UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)
extern void Rigidbody2D_set_angularVelocity_mFC06FB14E69DD4847F27E614900D22317AA5A390 (void);
// 0x00000045 System.Single UnityEngine.Rigidbody2D::get_drag()
extern void Rigidbody2D_get_drag_m0C65BDB12E059C56C2B5E1A9C0906F5A6AE0E92B (void);
// 0x00000046 System.Void UnityEngine.Rigidbody2D::set_drag(System.Single)
extern void Rigidbody2D_set_drag_m42A0E181D64FD374A047E8ABE351207847B324D0 (void);
// 0x00000047 System.Single UnityEngine.Rigidbody2D::get_angularDrag()
extern void Rigidbody2D_get_angularDrag_m017C7872E21D165A492C49BEE89CCBC9DF249B54 (void);
// 0x00000048 System.Void UnityEngine.Rigidbody2D::set_angularDrag(System.Single)
extern void Rigidbody2D_set_angularDrag_mF93603D37CFA437F6899EE53F711A3A7B5D0B599 (void);
// 0x00000049 System.Single UnityEngine.Rigidbody2D::get_gravityScale()
extern void Rigidbody2D_get_gravityScale_mCFA8E159F51B876E16EEF634A63415F7051AFF44 (void);
// 0x0000004A System.Void UnityEngine.Rigidbody2D::set_gravityScale(System.Single)
extern void Rigidbody2D_set_gravityScale_mAFD1A72661304467D20971BBCAA7E04B418F80FD (void);
// 0x0000004B UnityEngine.RigidbodyType2D UnityEngine.Rigidbody2D::get_bodyType()
extern void Rigidbody2D_get_bodyType_m20709275F3D8215592B2B89736AA8DDD2BF44ED1 (void);
// 0x0000004C System.Void UnityEngine.Rigidbody2D::set_bodyType(UnityEngine.RigidbodyType2D)
extern void Rigidbody2D_set_bodyType_mE2FAC2D78B06B445BD2AD58F87AC5B1865B23248 (void);
// 0x0000004D System.Boolean UnityEngine.Rigidbody2D::get_isKinematic()
extern void Rigidbody2D_get_isKinematic_m41BBC60A072047F850097C0391A002935DD277CB (void);
// 0x0000004E System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
extern void Rigidbody2D_set_isKinematic_m7C68AB4CFB6D301F0EDF0BFF66FB121ED3CC7853 (void);
// 0x0000004F System.Void UnityEngine.Rigidbody2D::set_simulated(System.Boolean)
extern void Rigidbody2D_set_simulated_m38E0BD6581E907DD87059034C4B2E8D47BBFE71D (void);
// 0x00000050 System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern void Rigidbody2D_AddForce_mDD5CAE0137A42660C2D585B090D7E24496976E1B (void);
// 0x00000051 System.Void UnityEngine.Rigidbody2D::.ctor()
extern void Rigidbody2D__ctor_mFF16B8ADAAE2FFD5FD4FBE3F412FC9E8FBBDBC88 (void);
// 0x00000052 System.Void UnityEngine.Rigidbody2D::get_position_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_get_position_Injected_m89CBCD6C0EDACABB4A20B2B22958CEDAE030DC44 (void);
// 0x00000053 System.Void UnityEngine.Rigidbody2D::MovePosition_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_MovePosition_Injected_m7B6D07CFCE2E864C008AB5CED9EF1C8231D95386 (void);
// 0x00000054 System.Void UnityEngine.Rigidbody2D::get_velocity_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_get_velocity_Injected_m980E2BA9FA750BA922DD2F79CEEA1CFF0B9B5D08 (void);
// 0x00000055 System.Void UnityEngine.Rigidbody2D::set_velocity_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_set_velocity_Injected_m060C7F62A3A0280CC02594A15E7DE4486D1BE05E (void);
// 0x00000056 System.Void UnityEngine.Rigidbody2D::AddForce_Injected(UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern void Rigidbody2D_AddForce_Injected_mC5372C179362B25CA579A94DBB11C5719F16452F (void);
// 0x00000057 System.Boolean UnityEngine.Collider2D::get_isTrigger()
extern void Collider2D_get_isTrigger_m982A3441480D505432B26A5B3DF6D0B34342EEE7 (void);
// 0x00000058 System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
extern void Collider2D_set_isTrigger_m19D5227BAB5D41F212D515C1E2CA433C2FEF7A48 (void);
// 0x00000059 System.Boolean UnityEngine.Collider2D::get_usedByComposite()
extern void Collider2D_get_usedByComposite_mFBCEE0F0F650485222B16C5F39A5604A20F89256 (void);
// 0x0000005A UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern void Collider2D_get_attachedRigidbody_m76D718444A94C258228DD98102DCF81C91CF9654 (void);
// 0x0000005B UnityEngine.Mesh UnityEngine.Collider2D::CreateMesh(System.Boolean,System.Boolean)
extern void Collider2D_CreateMesh_m3652DA9D992D70C69586B2CF76CC1B294434D487 (void);
// 0x0000005C System.UInt32 UnityEngine.Collider2D::GetShapeHash()
extern void Collider2D_GetShapeHash_m21660247FB3386589F9CB5EADA20A3566DCECB6D (void);
// 0x0000005D UnityEngine.Bounds UnityEngine.Collider2D::get_bounds()
extern void Collider2D_get_bounds_m74F65CE702BA9D9EED05B870325B4FE3B2401B5E (void);
// 0x0000005E System.Void UnityEngine.Collider2D::get_bounds_Injected(UnityEngine.Bounds&)
extern void Collider2D_get_bounds_Injected_mB3F24D39428D3C8E314CC6452878BD9A62C216E4 (void);
static Il2CppMethodPointer s_methodPointers[94] = 
{
	PhysicsScene2D_ToString_mACA22EF575F7544176360B16D431F2BB1DEBB307,
	PhysicsScene2D_op_Inequality_m7B70D9A8FA4E5ACC382E83862AE6BF7EE4CD22BB,
	PhysicsScene2D_GetHashCode_mD45B3437D088C66A35AE20066AD632D1D0858B1E,
	PhysicsScene2D_Equals_m4A19DE0675BD596A1B5AC0F7138A9A6F4D6029B3,
	PhysicsScene2D_Equals_mA7C243A71CFDBFA905F057CE3E9C5E61B34216FB,
	PhysicsScene2D_IsValid_m3C273A4AC45E6E9994DCBC66D11E003200FAF50C,
	PhysicsScene2D_IsValid_Internal_m18461F0649F3666D082886E1785B99AD1BC6380A,
	PhysicsScene2D_Simulate_m2210AE79B5D4713DA5BEFD4EB5857778651DCE62,
	PhysicsScene2D_Raycast_m5A2D66F6E7E8F34B6CF5B82099EFA4F69155F25D,
	PhysicsScene2D_Raycast_m74A71D9DBCC2CCD7454240AE784CEE5720E55EA0,
	PhysicsScene2D_Raycast_Internal_mDD401F1DAD381C0725F0F9CB65E6B23DC78D3C5A,
	PhysicsScene2D_Raycast_m004884696543F60917C1ED72374C1528207229C3,
	PhysicsScene2D_RaycastArray_Internal_m910B194258C3609328EC0DAD1C288F67B81FD030,
	PhysicsScene2D_Raycast_m541841D244633BA234ED72B01204161686D6B3B9,
	PhysicsScene2D_RaycastList_Internal_m990B80E682F006388ADFE1FEC61165FB21A58138,
	PhysicsScene2D_GetRayIntersection_mF3E0EC0D4F5A4B8C063E735979C851ED5B4B4C2E,
	PhysicsScene2D_GetRayIntersectionArray_Internal_mF48B0A8D9804F7938C2513B937602347DA4768AA,
	PhysicsScene2D_IsValid_Internal_Injected_m0E74DF97D3BAE7CA3FE64D5A8429C496072569F1,
	PhysicsScene2D_Raycast_Internal_Injected_mD6B513DC491DA368725681DDC4E4104C72650B77,
	PhysicsScene2D_RaycastArray_Internal_Injected_mE5A33C2FE2873FABD52129DC539856AD762FA3EE,
	PhysicsScene2D_RaycastList_Internal_Injected_mA190F786687B287B75E904E96C25DCE43422CF18,
	PhysicsScene2D_GetRayIntersectionArray_Internal_Injected_mCD438768C11300F4CA7444621467016833562054,
	PhysicsSceneExtensions2D_GetPhysicsScene2D_m578B450FE4359087F086A575817F7FDCC6EC0AD7,
	PhysicsSceneExtensions2D_GetPhysicsScene_Internal_mFC821A94DE88F39B0FE9854D858C7273265072D7,
	PhysicsSceneExtensions2D_GetPhysicsScene_Internal_Injected_mA3EE076447D7497740B95821A47B41BF8DF0417B,
	Physics2D_get_defaultPhysicsScene_m10876DD92CFE85E080A574F23FF30B044C03C9BD,
	Physics2D_get_queriesHitTriggers_m43C69B7B5475256AD329817C09D9D6C00EFB540E,
	Physics2D_Simulate_Internal_m3A1A3BFA605D5E39DB9A49EEEF572AC7A62221D7,
	Physics2D_Raycast_m8D056DA6FCBB18FD6F64BA247C16E050B0DBBF18,
	Physics2D_Raycast_m274780A393AF3EEAB93D4DAF2A5DD95706D5A2CB,
	Physics2D_Raycast_m6B89CCCAF549D1917B95F663E007382899E66BA7,
	Physics2D_Raycast_mED174EF64683E4E98190E82039AF753933585A5E,
	Physics2D_Raycast_m4F22AD6D93E283DCBFF1DF2F3B67988BFE092DAA,
	Physics2D_Raycast_mB3DEDF9E9B738EAB8ABB4CD6AF28E169EE329D46,
	Physics2D_Raycast_mCD80B702BD6EB5FEBC8B4D3B066DDD8ED0FE4652,
	Physics2D_Raycast_mCC46614BD92F9889D2F27AD6DFC24DB76131155D,
	Physics2D_RaycastAll_mC9E86E526CD2A3B7A7CB4C1F29F1F482694F77D6,
	Physics2D_RaycastAll_Internal_m55AC316F374C07D90B550B231D135BEA8F8C31ED,
	Physics2D_GetRayIntersectionAll_m0DF776894E1E3A8C03644D001938AD8B435C6C65,
	Physics2D_GetRayIntersectionAll_m0B3510E637D87DA64270E0B699C9E3AA28384368,
	Physics2D_GetRayIntersectionAll_m313BB4020C5DE25EDE1E23460C15CD5F44171C9A,
	Physics2D_GetRayIntersectionAll_Internal_mD38082338EF336C1B44572D8D9E608CFEE0C7A50,
	Physics2D_GetRayIntersectionNonAlloc_m931B8F65C30CFB7234D3CF51F296DE158422B12D,
	Physics2D_GetRayIntersectionNonAlloc_mE7B60A6CA6A5513B95D4714050DBA185D2F6FC52,
	Physics2D_GetRayIntersectionNonAlloc_mBE2332B21D30A74DCB161D48A629B32D747FB755,
	Physics2D__cctor_m538A4AED462ACB3482F9C397DE0B72BCDA2D3CFB,
	Physics2D_Simulate_Internal_Injected_m96924B9A71DFF35B53F205A54C89F7494C8FCC3F,
	Physics2D_RaycastAll_Internal_Injected_m1DBAE3FDFBFDB3E8CCD49E2D4F286265196FEC0B,
	Physics2D_GetRayIntersectionAll_Internal_Injected_m64C09D534E1B44E2B742C13EA712DE97FEB5EC57,
	ContactFilter2D_CheckConsistency_mD918F11F977EA35E87CF491F7AE8794F5D01DF72,
	ContactFilter2D_SetLayerMask_mC3FBC2D806C1A3ACB2D060CE48F8157505E42F9B,
	ContactFilter2D_SetDepth_mE614DDDDAEA489D150E61D2DF8104F9292236F18,
	ContactFilter2D_CreateLegacyFilter_mE33DF262BE7E9E49D7CAF5B990450FE1A78C30A3,
	ContactFilter2D_CheckConsistency_Injected_mD1E46463DF2E3C1FA3E651478E62023365937B0C,
	Collision2D_get_collider_m90FA98F6619E9F1E2EFAE8132EDB6ECA1A2C4F37,
	Collision2D_get_rigidbody_mD763F2D56BF538A94AD62379D22E42335A67B60D,
	Collision2D_get_transform_mC59737F246B2DAFF2AB4F6322664C87B28605CC7,
	Collision2D_get_relativeVelocity_m1F0BB90BC73FB0A0EA27212D832BB3F26D4C004A,
	RaycastHit2D_get_point_mB35E988E9E04328EFE926228A18334326721A36B,
	RaycastHit2D_get_normal_m75F1EBDE347BACEB5A6A6AA72543C740806AB5F2,
	RaycastHit2D_get_distance_mD0FE1482E2768CF587AFB65488459697EAB64613,
	RaycastHit2D_get_collider_mB56DFCD16B708852EEBDBB490BC8665DBF7487FD,
	Rigidbody2D_get_position_m07070C4416DFE2229070F95B349E411AE4869276,
	Rigidbody2D_MovePosition_m7F24879BB78DA0587168B257C56DCFD248A90895,
	Rigidbody2D_get_velocity_mBD8AC6F93F0E24CC41D2361BCEF74F81303720EF,
	Rigidbody2D_set_velocity_m9335C5883B218F6FCDF7E229AC96232FCBAC4CE6,
	Rigidbody2D_get_angularVelocity_mAD2505FB1F8C9E1A66D1EA8F8680D14380BFC58D,
	Rigidbody2D_set_angularVelocity_mFC06FB14E69DD4847F27E614900D22317AA5A390,
	Rigidbody2D_get_drag_m0C65BDB12E059C56C2B5E1A9C0906F5A6AE0E92B,
	Rigidbody2D_set_drag_m42A0E181D64FD374A047E8ABE351207847B324D0,
	Rigidbody2D_get_angularDrag_m017C7872E21D165A492C49BEE89CCBC9DF249B54,
	Rigidbody2D_set_angularDrag_mF93603D37CFA437F6899EE53F711A3A7B5D0B599,
	Rigidbody2D_get_gravityScale_mCFA8E159F51B876E16EEF634A63415F7051AFF44,
	Rigidbody2D_set_gravityScale_mAFD1A72661304467D20971BBCAA7E04B418F80FD,
	Rigidbody2D_get_bodyType_m20709275F3D8215592B2B89736AA8DDD2BF44ED1,
	Rigidbody2D_set_bodyType_mE2FAC2D78B06B445BD2AD58F87AC5B1865B23248,
	Rigidbody2D_get_isKinematic_m41BBC60A072047F850097C0391A002935DD277CB,
	Rigidbody2D_set_isKinematic_m7C68AB4CFB6D301F0EDF0BFF66FB121ED3CC7853,
	Rigidbody2D_set_simulated_m38E0BD6581E907DD87059034C4B2E8D47BBFE71D,
	Rigidbody2D_AddForce_mDD5CAE0137A42660C2D585B090D7E24496976E1B,
	Rigidbody2D__ctor_mFF16B8ADAAE2FFD5FD4FBE3F412FC9E8FBBDBC88,
	Rigidbody2D_get_position_Injected_m89CBCD6C0EDACABB4A20B2B22958CEDAE030DC44,
	Rigidbody2D_MovePosition_Injected_m7B6D07CFCE2E864C008AB5CED9EF1C8231D95386,
	Rigidbody2D_get_velocity_Injected_m980E2BA9FA750BA922DD2F79CEEA1CFF0B9B5D08,
	Rigidbody2D_set_velocity_Injected_m060C7F62A3A0280CC02594A15E7DE4486D1BE05E,
	Rigidbody2D_AddForce_Injected_mC5372C179362B25CA579A94DBB11C5719F16452F,
	Collider2D_get_isTrigger_m982A3441480D505432B26A5B3DF6D0B34342EEE7,
	Collider2D_set_isTrigger_m19D5227BAB5D41F212D515C1E2CA433C2FEF7A48,
	Collider2D_get_usedByComposite_mFBCEE0F0F650485222B16C5F39A5604A20F89256,
	Collider2D_get_attachedRigidbody_m76D718444A94C258228DD98102DCF81C91CF9654,
	Collider2D_CreateMesh_m3652DA9D992D70C69586B2CF76CC1B294434D487,
	Collider2D_GetShapeHash_m21660247FB3386589F9CB5EADA20A3566DCECB6D,
	Collider2D_get_bounds_m74F65CE702BA9D9EED05B870325B4FE3B2401B5E,
	Collider2D_get_bounds_Injected_mB3F24D39428D3C8E314CC6452878BD9A62C216E4,
};
extern void PhysicsScene2D_ToString_mACA22EF575F7544176360B16D431F2BB1DEBB307_AdjustorThunk (void);
extern void PhysicsScene2D_GetHashCode_mD45B3437D088C66A35AE20066AD632D1D0858B1E_AdjustorThunk (void);
extern void PhysicsScene2D_Equals_m4A19DE0675BD596A1B5AC0F7138A9A6F4D6029B3_AdjustorThunk (void);
extern void PhysicsScene2D_Equals_mA7C243A71CFDBFA905F057CE3E9C5E61B34216FB_AdjustorThunk (void);
extern void PhysicsScene2D_IsValid_m3C273A4AC45E6E9994DCBC66D11E003200FAF50C_AdjustorThunk (void);
extern void PhysicsScene2D_Simulate_m2210AE79B5D4713DA5BEFD4EB5857778651DCE62_AdjustorThunk (void);
extern void PhysicsScene2D_Raycast_m5A2D66F6E7E8F34B6CF5B82099EFA4F69155F25D_AdjustorThunk (void);
extern void PhysicsScene2D_Raycast_m74A71D9DBCC2CCD7454240AE784CEE5720E55EA0_AdjustorThunk (void);
extern void PhysicsScene2D_Raycast_m004884696543F60917C1ED72374C1528207229C3_AdjustorThunk (void);
extern void PhysicsScene2D_Raycast_m541841D244633BA234ED72B01204161686D6B3B9_AdjustorThunk (void);
extern void PhysicsScene2D_GetRayIntersection_mF3E0EC0D4F5A4B8C063E735979C851ED5B4B4C2E_AdjustorThunk (void);
extern void ContactFilter2D_CheckConsistency_mD918F11F977EA35E87CF491F7AE8794F5D01DF72_AdjustorThunk (void);
extern void ContactFilter2D_SetLayerMask_mC3FBC2D806C1A3ACB2D060CE48F8157505E42F9B_AdjustorThunk (void);
extern void ContactFilter2D_SetDepth_mE614DDDDAEA489D150E61D2DF8104F9292236F18_AdjustorThunk (void);
extern void RaycastHit2D_get_point_mB35E988E9E04328EFE926228A18334326721A36B_AdjustorThunk (void);
extern void RaycastHit2D_get_normal_m75F1EBDE347BACEB5A6A6AA72543C740806AB5F2_AdjustorThunk (void);
extern void RaycastHit2D_get_distance_mD0FE1482E2768CF587AFB65488459697EAB64613_AdjustorThunk (void);
extern void RaycastHit2D_get_collider_mB56DFCD16B708852EEBDBB490BC8665DBF7487FD_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[18] = 
{
	{ 0x06000001, PhysicsScene2D_ToString_mACA22EF575F7544176360B16D431F2BB1DEBB307_AdjustorThunk },
	{ 0x06000003, PhysicsScene2D_GetHashCode_mD45B3437D088C66A35AE20066AD632D1D0858B1E_AdjustorThunk },
	{ 0x06000004, PhysicsScene2D_Equals_m4A19DE0675BD596A1B5AC0F7138A9A6F4D6029B3_AdjustorThunk },
	{ 0x06000005, PhysicsScene2D_Equals_mA7C243A71CFDBFA905F057CE3E9C5E61B34216FB_AdjustorThunk },
	{ 0x06000006, PhysicsScene2D_IsValid_m3C273A4AC45E6E9994DCBC66D11E003200FAF50C_AdjustorThunk },
	{ 0x06000008, PhysicsScene2D_Simulate_m2210AE79B5D4713DA5BEFD4EB5857778651DCE62_AdjustorThunk },
	{ 0x06000009, PhysicsScene2D_Raycast_m5A2D66F6E7E8F34B6CF5B82099EFA4F69155F25D_AdjustorThunk },
	{ 0x0600000A, PhysicsScene2D_Raycast_m74A71D9DBCC2CCD7454240AE784CEE5720E55EA0_AdjustorThunk },
	{ 0x0600000C, PhysicsScene2D_Raycast_m004884696543F60917C1ED72374C1528207229C3_AdjustorThunk },
	{ 0x0600000E, PhysicsScene2D_Raycast_m541841D244633BA234ED72B01204161686D6B3B9_AdjustorThunk },
	{ 0x06000010, PhysicsScene2D_GetRayIntersection_mF3E0EC0D4F5A4B8C063E735979C851ED5B4B4C2E_AdjustorThunk },
	{ 0x06000032, ContactFilter2D_CheckConsistency_mD918F11F977EA35E87CF491F7AE8794F5D01DF72_AdjustorThunk },
	{ 0x06000033, ContactFilter2D_SetLayerMask_mC3FBC2D806C1A3ACB2D060CE48F8157505E42F9B_AdjustorThunk },
	{ 0x06000034, ContactFilter2D_SetDepth_mE614DDDDAEA489D150E61D2DF8104F9292236F18_AdjustorThunk },
	{ 0x0600003B, RaycastHit2D_get_point_mB35E988E9E04328EFE926228A18334326721A36B_AdjustorThunk },
	{ 0x0600003C, RaycastHit2D_get_normal_m75F1EBDE347BACEB5A6A6AA72543C740806AB5F2_AdjustorThunk },
	{ 0x0600003D, RaycastHit2D_get_distance_mD0FE1482E2768CF587AFB65488459697EAB64613_AdjustorThunk },
	{ 0x0600003E, RaycastHit2D_get_collider_mB56DFCD16B708852EEBDBB490BC8665DBF7487FD_AdjustorThunk },
};
static const int32_t s_InvokerIndices[94] = 
{
	6765,
	9429,
	6727,
	3982,
	3986,
	6666,
	10967,
	4040,
	947,
	946,
	7640,
	468,
	7311,
	468,
	7311,
	902,
	7312,
	10955,
	7343,
	7291,
	7291,
	7292,
	11247,
	11247,
	9953,
	12324,
	12297,
	9430,
	9741,
	8777,
	8200,
	7641,
	7330,
	8126,
	7599,
	7599,
	8772,
	7637,
	11220,
	9713,
	8763,
	7638,
	9596,
	8663,
	8125,
	12362,
	9356,
	7606,
	7607,
	6908,
	5495,
	3150,
	8572,
	11503,
	6765,
	6765,
	6765,
	6896,
	6896,
	6896,
	6836,
	6765,
	6896,
	5648,
	6896,
	5648,
	6836,
	5591,
	6836,
	5591,
	6836,
	5591,
	6836,
	5591,
	6727,
	5485,
	6666,
	5418,
	5418,
	3181,
	6908,
	5406,
	5406,
	5406,
	5406,
	2474,
	6666,
	5418,
	6666,
	6765,
	2364,
	6891,
	6664,
	5406,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_Physics2DModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_Physics2DModule_CodeGenModule = 
{
	"UnityEngine.Physics2DModule.dll",
	94,
	s_methodPointers,
	18,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

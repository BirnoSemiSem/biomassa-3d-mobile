﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Mirror.LogEntry::.ctor(System.String,UnityEngine.LogType)
extern void LogEntry__ctor_m91CDC82DC8D2111C6413CFED125B90BA0621AED7 (void);
// 0x00000002 System.Void Mirror.GUIConsole::Awake()
extern void GUIConsole_Awake_mCDE61F6A9F913E11470F8848A73EB7434EE4B166 (void);
// 0x00000003 System.Void Mirror.GUIConsole::OnLog(System.String,System.String,UnityEngine.LogType)
extern void GUIConsole_OnLog_m830DC47EA84B8F06CD4029864E8205D716230EE5 (void);
// 0x00000004 System.Void Mirror.GUIConsole::Update()
extern void GUIConsole_Update_m990CE474C3028D74160EA60388083B00A64D9CB8 (void);
// 0x00000005 System.Void Mirror.GUIConsole::OnGUI()
extern void GUIConsole_OnGUI_m5F30A615992B44A24C2E3B095D5E238839625466 (void);
// 0x00000006 System.Void Mirror.GUIConsole::.ctor()
extern void GUIConsole__ctor_mD1BEF3B54C70685D8F98302879D2FFD86BF5C64E (void);
// 0x00000007 System.Int32 Mirror.DistanceInterestManagement::GetVisRange(Mirror.NetworkIdentity)
extern void DistanceInterestManagement_GetVisRange_m12E05D584708ACDCEE55D0EFAC6F84324396FEFA (void);
// 0x00000008 System.Void Mirror.DistanceInterestManagement::Reset()
extern void DistanceInterestManagement_Reset_m92C7ADF0A54C5E47565DD00257C9E2EEBC5F847E (void);
// 0x00000009 System.Boolean Mirror.DistanceInterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void DistanceInterestManagement_OnCheckObserver_mE778FFC6C980A6B6ACD9EDD3B2C1B59C64596490 (void);
// 0x0000000A System.Void Mirror.DistanceInterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
extern void DistanceInterestManagement_OnRebuildObservers_mD078341BFA331385403EB4117990CA71CD96C821 (void);
// 0x0000000B System.Void Mirror.DistanceInterestManagement::Update()
extern void DistanceInterestManagement_Update_m873AC936A693A7CD2484283D78A0C36CA58E1DDB (void);
// 0x0000000C System.Void Mirror.DistanceInterestManagement::.ctor()
extern void DistanceInterestManagement__ctor_mB7E21EB4351E42F10092CD23AC0EE6FD6C274993 (void);
// 0x0000000D System.Void Mirror.DistanceInterestManagementCustomRange::.ctor()
extern void DistanceInterestManagementCustomRange__ctor_m7F8FFAC977C6269FB3BB3E04C2BFB7E88461FB43 (void);
// 0x0000000E System.Void Mirror.DistanceInterestManagementCustomRange::MirrorProcessed()
extern void DistanceInterestManagementCustomRange_MirrorProcessed_m99AA5AC739FEC1CC9703DC9D2D635D3B6965303F (void);
// 0x0000000F System.Void Mirror.MatchInterestManagement::OnSpawned(Mirror.NetworkIdentity)
extern void MatchInterestManagement_OnSpawned_m35C954BAF490E9F68B3837D00B0B9DC6EB7B85ED (void);
// 0x00000010 System.Void Mirror.MatchInterestManagement::OnDestroyed(Mirror.NetworkIdentity)
extern void MatchInterestManagement_OnDestroyed_m50546ECB371F098BF04A3D6D0EE5A3B63538B701 (void);
// 0x00000011 System.Void Mirror.MatchInterestManagement::Update()
extern void MatchInterestManagement_Update_m4525F713A4D84BE9A1FB86112F172D4222618C0B (void);
// 0x00000012 System.Void Mirror.MatchInterestManagement::UpdateDirtyMatches(System.Guid,System.Guid)
extern void MatchInterestManagement_UpdateDirtyMatches_mB59ABD2A1E6EAE4CCA07022A538A47A090908137 (void);
// 0x00000013 System.Void Mirror.MatchInterestManagement::UpdateMatchObjects(Mirror.NetworkIdentity,System.Guid,System.Guid)
extern void MatchInterestManagement_UpdateMatchObjects_m41FEB7113E66F2DB6DFF8B31B97E9B8864FFE111 (void);
// 0x00000014 System.Void Mirror.MatchInterestManagement::RebuildMatchObservers(System.Guid)
extern void MatchInterestManagement_RebuildMatchObservers_mD70F277258F937446E363C6B58BB1317BE379902 (void);
// 0x00000015 System.Boolean Mirror.MatchInterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void MatchInterestManagement_OnCheckObserver_m52D72C98383DE761B003F5010F3F9AE445B385BC (void);
// 0x00000016 System.Void Mirror.MatchInterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
extern void MatchInterestManagement_OnRebuildObservers_mFEB9F4610BEEB3DB89E033736AE8F2E473494D06 (void);
// 0x00000017 System.Void Mirror.MatchInterestManagement::.ctor()
extern void MatchInterestManagement__ctor_m53C8035973784DB9B82F1134FB055DF02877F990 (void);
// 0x00000018 System.Void Mirror.NetworkMatch::.ctor()
extern void NetworkMatch__ctor_m3BE0761DDCF97655C11D2DB8ADAEA21C6BB236E4 (void);
// 0x00000019 System.Void Mirror.NetworkMatch::MirrorProcessed()
extern void NetworkMatch_MirrorProcessed_m20066E6C5F87D82FD03EB126DA66325F18E37045 (void);
// 0x0000001A System.Void Mirror.SceneInterestManagement::OnSpawned(Mirror.NetworkIdentity)
extern void SceneInterestManagement_OnSpawned_mA65F971568AF6C53AA875D7CE9831D6EE3827739 (void);
// 0x0000001B System.Void Mirror.SceneInterestManagement::OnDestroyed(Mirror.NetworkIdentity)
extern void SceneInterestManagement_OnDestroyed_mB555665F50B9C653E7DAEEDCB1781253070E3C04 (void);
// 0x0000001C System.Void Mirror.SceneInterestManagement::Update()
extern void SceneInterestManagement_Update_mA48FE8022573A919B5C50C73A7CF2175D567B91F (void);
// 0x0000001D System.Void Mirror.SceneInterestManagement::RebuildSceneObservers(UnityEngine.SceneManagement.Scene)
extern void SceneInterestManagement_RebuildSceneObservers_mD17A30B2C27977D00BF4F0B0DE12F06CCA8F0A4C (void);
// 0x0000001E System.Boolean Mirror.SceneInterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void SceneInterestManagement_OnCheckObserver_m0982491A1BE8398EEFB6A6329775DE768FD195C5 (void);
// 0x0000001F System.Void Mirror.SceneInterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
extern void SceneInterestManagement_OnRebuildObservers_m8B7ECAAAF54AE76F5CE10E99E4158E2B2843269C (void);
// 0x00000020 System.Void Mirror.SceneInterestManagement::.ctor()
extern void SceneInterestManagement__ctor_mB6B5132982798C1D2FBAE9122BE56A057C101E1D (void);
// 0x00000021 System.Void Mirror.Grid2D`1::Add(UnityEngine.Vector2Int,T)
// 0x00000022 System.Void Mirror.Grid2D`1::GetAt(UnityEngine.Vector2Int,System.Collections.Generic.HashSet`1<T>)
// 0x00000023 System.Void Mirror.Grid2D`1::GetWithNeighbours(UnityEngine.Vector2Int,System.Collections.Generic.HashSet`1<T>)
// 0x00000024 System.Void Mirror.Grid2D`1::ClearNonAlloc()
// 0x00000025 System.Void Mirror.Grid2D`1::.ctor()
// 0x00000026 System.Int32 Mirror.SpatialHashingInterestManagement::get_resolution()
extern void SpatialHashingInterestManagement_get_resolution_m2B1130C366434CBFDE569B28B4A365E308BF27A3 (void);
// 0x00000027 UnityEngine.Vector2Int Mirror.SpatialHashingInterestManagement::ProjectToGrid(UnityEngine.Vector3)
extern void SpatialHashingInterestManagement_ProjectToGrid_m349C290374CFF7FB99D9ECB3A13BB90FE777BC50 (void);
// 0x00000028 System.Boolean Mirror.SpatialHashingInterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void SpatialHashingInterestManagement_OnCheckObserver_mA4C2B8D122C26BA6C0F5F22FF6E2D0999C09DDE3 (void);
// 0x00000029 System.Void Mirror.SpatialHashingInterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
extern void SpatialHashingInterestManagement_OnRebuildObservers_m3D1B09BDE0EF5484D3CE5CAA5CFCE5AB445E9A3D (void);
// 0x0000002A System.Void Mirror.SpatialHashingInterestManagement::Reset()
extern void SpatialHashingInterestManagement_Reset_m19589E358B323C2044C653841D6A5A83D58E413C (void);
// 0x0000002B System.Void Mirror.SpatialHashingInterestManagement::Update()
extern void SpatialHashingInterestManagement_Update_m8D8EEF04CE24356DC794EFD0634AACFDD5AB6604 (void);
// 0x0000002C System.Void Mirror.SpatialHashingInterestManagement::.ctor()
extern void SpatialHashingInterestManagement__ctor_mA5E9B2FE69CD2F872BB7B297A8FE49E6061E1505 (void);
// 0x0000002D System.Void Mirror.NetworkTeam::.ctor()
extern void NetworkTeam__ctor_mC5292FAAA69C6B4B38122848B79AABB01EF22A88 (void);
// 0x0000002E System.Void Mirror.NetworkTeam::MirrorProcessed()
extern void NetworkTeam_MirrorProcessed_mBF8D8DD041B115C600D637E262C473BDC58F2859 (void);
// 0x0000002F System.Void Mirror.TeamInterestManagement::OnSpawned(Mirror.NetworkIdentity)
extern void TeamInterestManagement_OnSpawned_mE421063E3E1357E624B0CF3CFF82012DE4044E3B (void);
// 0x00000030 System.Void Mirror.TeamInterestManagement::OnDestroyed(Mirror.NetworkIdentity)
extern void TeamInterestManagement_OnDestroyed_mF7CDF71416FA303814EDBDB0ACB9E30B50CA3836 (void);
// 0x00000031 System.Void Mirror.TeamInterestManagement::Update()
extern void TeamInterestManagement_Update_mB87A6F8A33201C0C13EDC1292337C94B6CDBADE9 (void);
// 0x00000032 System.Void Mirror.TeamInterestManagement::UpdateDirtyTeams(System.String,System.String)
extern void TeamInterestManagement_UpdateDirtyTeams_mFCA21AFFCC2DC9FC85672E0C4621B0CCC27D2246 (void);
// 0x00000033 System.Void Mirror.TeamInterestManagement::UpdateTeamObjects(Mirror.NetworkIdentity,System.String,System.String)
extern void TeamInterestManagement_UpdateTeamObjects_mF34863A90539AB04F4B692FF760A710FF1EDE6C7 (void);
// 0x00000034 System.Void Mirror.TeamInterestManagement::RebuildTeamObservers(System.String)
extern void TeamInterestManagement_RebuildTeamObservers_m67F50ECF8B74A2E4A4999C8785D8EA57100ED5D9 (void);
// 0x00000035 System.Boolean Mirror.TeamInterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void TeamInterestManagement_OnCheckObserver_mD597BD7F346C3F6CF64B6517CA5174AE62125390 (void);
// 0x00000036 System.Void Mirror.TeamInterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
extern void TeamInterestManagement_OnRebuildObservers_m7BEEDB65A58B202995D2D121B740736795C51877 (void);
// 0x00000037 System.Void Mirror.TeamInterestManagement::AddAllConnections(System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
extern void TeamInterestManagement_AddAllConnections_mB84A6B8F7805117012C031084AD0AA37D3CF1E39 (void);
// 0x00000038 System.Void Mirror.TeamInterestManagement::.ctor()
extern void TeamInterestManagement__ctor_mC9CCD5958EA9B1E3A8EB48888DFC98CC0F341914 (void);
// 0x00000039 System.Boolean Mirror.NetworkAnimator::get_SendMessagesAllowed()
extern void NetworkAnimator_get_SendMessagesAllowed_m4372D7CCF3AA75F13998A2BBC83987DE53055163 (void);
// 0x0000003A System.Void Mirror.NetworkAnimator::Awake()
extern void NetworkAnimator_Awake_mF3E9622BB09EA5CCCDBCC0B3670C224014048C4F (void);
// 0x0000003B System.Void Mirror.NetworkAnimator::FixedUpdate()
extern void NetworkAnimator_FixedUpdate_m5B89F06E14F9A209D4D9EE2B35159ADD1A6C985C (void);
// 0x0000003C System.Void Mirror.NetworkAnimator::CheckSpeed()
extern void NetworkAnimator_CheckSpeed_mDAF46D0BC7333AA37E1D73FA77AF42C45BA775D3 (void);
// 0x0000003D System.Void Mirror.NetworkAnimator::OnAnimatorSpeedChanged(System.Single,System.Single)
extern void NetworkAnimator_OnAnimatorSpeedChanged_m09D12F59674E4FE256CC4B68DE34EFBBC0D8584E (void);
// 0x0000003E System.Boolean Mirror.NetworkAnimator::CheckAnimStateChanged(System.Int32&,System.Single&,System.Int32)
extern void NetworkAnimator_CheckAnimStateChanged_m91D08C2DF3FC523A15A5911AF0C7F61496CB77BE (void);
// 0x0000003F System.Void Mirror.NetworkAnimator::CheckSendRate()
extern void NetworkAnimator_CheckSendRate_m328485F3DBCB9B30D2C0582B1FC34C67E2E3B951 (void);
// 0x00000040 System.Void Mirror.NetworkAnimator::SendAnimationMessage(System.Int32,System.Single,System.Int32,System.Single,System.Byte[])
extern void NetworkAnimator_SendAnimationMessage_m01B731EC39BB0FC0923DCF09EAE53B3E61BA4491 (void);
// 0x00000041 System.Void Mirror.NetworkAnimator::SendAnimationParametersMessage(System.Byte[])
extern void NetworkAnimator_SendAnimationParametersMessage_m27AAEDA4FD58B7E7988DB7914C6F8D382CC13803 (void);
// 0x00000042 System.Void Mirror.NetworkAnimator::HandleAnimMsg(System.Int32,System.Single,System.Int32,System.Single,Mirror.NetworkReader)
extern void NetworkAnimator_HandleAnimMsg_m608381B6C84879ED535969A1D2A82FA0B86FC242 (void);
// 0x00000043 System.Void Mirror.NetworkAnimator::HandleAnimParamsMsg(Mirror.NetworkReader)
extern void NetworkAnimator_HandleAnimParamsMsg_m685AAECD2BDDD7092858BA556F17EF53CBC9E605 (void);
// 0x00000044 System.Void Mirror.NetworkAnimator::HandleAnimTriggerMsg(System.Int32)
extern void NetworkAnimator_HandleAnimTriggerMsg_m836B7829C8547DE10C95E265A645C978FC5E9452 (void);
// 0x00000045 System.Void Mirror.NetworkAnimator::HandleAnimResetTriggerMsg(System.Int32)
extern void NetworkAnimator_HandleAnimResetTriggerMsg_m9EC721D30C5D7BFE86E4EE8B97D628AB617C0E44 (void);
// 0x00000046 System.UInt64 Mirror.NetworkAnimator::NextDirtyBits()
extern void NetworkAnimator_NextDirtyBits_mBD81C8CFF2DC273823CBB03DF7228476B1B2532A (void);
// 0x00000047 System.Boolean Mirror.NetworkAnimator::WriteParameters(Mirror.NetworkWriter,System.Boolean)
extern void NetworkAnimator_WriteParameters_mFA85AF1193AF8DB50CD8CD2749F558871C5FEC2A (void);
// 0x00000048 System.Void Mirror.NetworkAnimator::ReadParameters(Mirror.NetworkReader)
extern void NetworkAnimator_ReadParameters_mE11AAB472B73148B0988D33155647E184232D7D8 (void);
// 0x00000049 System.Boolean Mirror.NetworkAnimator::OnSerialize(Mirror.NetworkWriter,System.Boolean)
extern void NetworkAnimator_OnSerialize_m90E93BB04577BA90D0410B76B18DC11B581A027B (void);
// 0x0000004A System.Void Mirror.NetworkAnimator::OnDeserialize(Mirror.NetworkReader,System.Boolean)
extern void NetworkAnimator_OnDeserialize_mD5D93F2E6A45DB4DAA730C0EFC183F5B251135EC (void);
// 0x0000004B System.Void Mirror.NetworkAnimator::SetTrigger(System.String)
extern void NetworkAnimator_SetTrigger_mBFBFAB474DE4621AFA3921487963D5E5E3E1D181 (void);
// 0x0000004C System.Void Mirror.NetworkAnimator::SetTrigger(System.Int32)
extern void NetworkAnimator_SetTrigger_mB7955C55BFD52E0561D3F10C602E029A216206C1 (void);
// 0x0000004D System.Void Mirror.NetworkAnimator::ResetTrigger(System.String)
extern void NetworkAnimator_ResetTrigger_m2E261D70481794B0133BB4E7DCA19747877A8A56 (void);
// 0x0000004E System.Void Mirror.NetworkAnimator::ResetTrigger(System.Int32)
extern void NetworkAnimator_ResetTrigger_m12C3D776FB823150A181EFADA7C9FC5A5D072621 (void);
// 0x0000004F System.Void Mirror.NetworkAnimator::CmdOnAnimationServerMessage(System.Int32,System.Single,System.Int32,System.Single,System.Byte[])
extern void NetworkAnimator_CmdOnAnimationServerMessage_m5A73D5EE0159B7F277146ABAA92ECD1D754A4533 (void);
// 0x00000050 System.Void Mirror.NetworkAnimator::CmdOnAnimationParametersServerMessage(System.Byte[])
extern void NetworkAnimator_CmdOnAnimationParametersServerMessage_m517FF6B96255C1BB78A72061867489C3EAD70026 (void);
// 0x00000051 System.Void Mirror.NetworkAnimator::CmdOnAnimationTriggerServerMessage(System.Int32)
extern void NetworkAnimator_CmdOnAnimationTriggerServerMessage_m6FFC2677C24B4218C20354267CEBFD8BCB67D447 (void);
// 0x00000052 System.Void Mirror.NetworkAnimator::CmdOnAnimationResetTriggerServerMessage(System.Int32)
extern void NetworkAnimator_CmdOnAnimationResetTriggerServerMessage_m713A00270ED506782ED07E24F59A2B4A991376CC (void);
// 0x00000053 System.Void Mirror.NetworkAnimator::CmdSetAnimatorSpeed(System.Single)
extern void NetworkAnimator_CmdSetAnimatorSpeed_m486BD8252A288D51146F1040E900220455A945C4 (void);
// 0x00000054 System.Void Mirror.NetworkAnimator::RpcOnAnimationClientMessage(System.Int32,System.Single,System.Int32,System.Single,System.Byte[])
extern void NetworkAnimator_RpcOnAnimationClientMessage_m8F366B4DC2E3EB03F3507009DFB17BFCECCF5951 (void);
// 0x00000055 System.Void Mirror.NetworkAnimator::RpcOnAnimationParametersClientMessage(System.Byte[])
extern void NetworkAnimator_RpcOnAnimationParametersClientMessage_m415096E6363531A289FE824371C215C12F336C7D (void);
// 0x00000056 System.Void Mirror.NetworkAnimator::RpcOnAnimationTriggerClientMessage(System.Int32)
extern void NetworkAnimator_RpcOnAnimationTriggerClientMessage_mD314D48D8759DBCE7C0E766586DA883438495584 (void);
// 0x00000057 System.Void Mirror.NetworkAnimator::RpcOnAnimationResetTriggerClientMessage(System.Int32)
extern void NetworkAnimator_RpcOnAnimationResetTriggerClientMessage_m968AAAA769EA88FD373BEF99B6A7596AF093D238 (void);
// 0x00000058 System.Void Mirror.NetworkAnimator::.ctor()
extern void NetworkAnimator__ctor_m440265F51814E5C4E99207C6EDB133E76F208FB9 (void);
// 0x00000059 System.Boolean Mirror.NetworkAnimator::<Awake>b__14_0(UnityEngine.AnimatorControllerParameter)
extern void NetworkAnimator_U3CAwakeU3Eb__14_0_mF3F496D8681E4A54455BA1092DE2A4C88B197FCE (void);
// 0x0000005A System.Void Mirror.NetworkAnimator::MirrorProcessed()
extern void NetworkAnimator_MirrorProcessed_m6B265DF87A5010F76ECD2FD84DD8159149F859B7 (void);
// 0x0000005B System.Single Mirror.NetworkAnimator::get_NetworkanimatorSpeed()
extern void NetworkAnimator_get_NetworkanimatorSpeed_m9D39AE82B53EAFCD11A98AB3CB4669A68D8D42CE (void);
// 0x0000005C System.Void Mirror.NetworkAnimator::set_NetworkanimatorSpeed(System.Single)
extern void NetworkAnimator_set_NetworkanimatorSpeed_m62E06E8AA0515920D78DEE09603F26FAB03DFA4B (void);
// 0x0000005D System.Void Mirror.NetworkAnimator::UserCode_CmdOnAnimationServerMessage__Int32__Single__Int32__Single__Byte[](System.Int32,System.Single,System.Int32,System.Single,System.Byte[])
extern void NetworkAnimator_UserCode_CmdOnAnimationServerMessage__Int32__Single__Int32__Single__ByteU5BU5D_m9D02589DEE51FEBE0DA757CBC808546CF9846E27 (void);
// 0x0000005E System.Void Mirror.NetworkAnimator::InvokeUserCode_CmdOnAnimationServerMessage__Int32__Single__Int32__Single__Byte[](Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_CmdOnAnimationServerMessage__Int32__Single__Int32__Single__ByteU5BU5D_m7BBA7F78D7357FDBE5816BD686F8400B7B0584CA (void);
// 0x0000005F System.Void Mirror.NetworkAnimator::UserCode_CmdOnAnimationParametersServerMessage__Byte[](System.Byte[])
extern void NetworkAnimator_UserCode_CmdOnAnimationParametersServerMessage__ByteU5BU5D_mBED948C4DF85B3DC17C2464B1565B36223222996 (void);
// 0x00000060 System.Void Mirror.NetworkAnimator::InvokeUserCode_CmdOnAnimationParametersServerMessage__Byte[](Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_CmdOnAnimationParametersServerMessage__ByteU5BU5D_m4BA699465A623F50EB0F06F66CF7F3940B1444E7 (void);
// 0x00000061 System.Void Mirror.NetworkAnimator::UserCode_CmdOnAnimationTriggerServerMessage__Int32(System.Int32)
extern void NetworkAnimator_UserCode_CmdOnAnimationTriggerServerMessage__Int32_m14B502BD60282FFAA5D5BF3C882E4DE93E63581E (void);
// 0x00000062 System.Void Mirror.NetworkAnimator::InvokeUserCode_CmdOnAnimationTriggerServerMessage__Int32(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_CmdOnAnimationTriggerServerMessage__Int32_mA9EBBF3ED3746A3779AD9766D195907AF04B5FCD (void);
// 0x00000063 System.Void Mirror.NetworkAnimator::UserCode_CmdOnAnimationResetTriggerServerMessage__Int32(System.Int32)
extern void NetworkAnimator_UserCode_CmdOnAnimationResetTriggerServerMessage__Int32_m09021B5E5F96F820379CF0E51462E14E87C0018E (void);
// 0x00000064 System.Void Mirror.NetworkAnimator::InvokeUserCode_CmdOnAnimationResetTriggerServerMessage__Int32(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_CmdOnAnimationResetTriggerServerMessage__Int32_m565C0682E01F6C5A7B188124A6F8139C7CB0DBC6 (void);
// 0x00000065 System.Void Mirror.NetworkAnimator::UserCode_CmdSetAnimatorSpeed__Single(System.Single)
extern void NetworkAnimator_UserCode_CmdSetAnimatorSpeed__Single_m7F71C44E4C125AF8A1CD08F372AB6FE39A13CE52 (void);
// 0x00000066 System.Void Mirror.NetworkAnimator::InvokeUserCode_CmdSetAnimatorSpeed__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_CmdSetAnimatorSpeed__Single_mC6B549CC13CA65B43699CE813374D470BDE50014 (void);
// 0x00000067 System.Void Mirror.NetworkAnimator::UserCode_RpcOnAnimationClientMessage__Int32__Single__Int32__Single__Byte[](System.Int32,System.Single,System.Int32,System.Single,System.Byte[])
extern void NetworkAnimator_UserCode_RpcOnAnimationClientMessage__Int32__Single__Int32__Single__ByteU5BU5D_m84FD050FE9C01CE0CEA79C6207A08EABC4904F23 (void);
// 0x00000068 System.Void Mirror.NetworkAnimator::InvokeUserCode_RpcOnAnimationClientMessage__Int32__Single__Int32__Single__Byte[](Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_RpcOnAnimationClientMessage__Int32__Single__Int32__Single__ByteU5BU5D_mE55F86395222424AE1AF34A31C18EB528E707996 (void);
// 0x00000069 System.Void Mirror.NetworkAnimator::UserCode_RpcOnAnimationParametersClientMessage__Byte[](System.Byte[])
extern void NetworkAnimator_UserCode_RpcOnAnimationParametersClientMessage__ByteU5BU5D_m7559B1EA4EAC8774016A207B59451AE266403EBF (void);
// 0x0000006A System.Void Mirror.NetworkAnimator::InvokeUserCode_RpcOnAnimationParametersClientMessage__Byte[](Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_RpcOnAnimationParametersClientMessage__ByteU5BU5D_m83DF9518D29AF25D45E70EA0805C4768C929709B (void);
// 0x0000006B System.Void Mirror.NetworkAnimator::UserCode_RpcOnAnimationTriggerClientMessage__Int32(System.Int32)
extern void NetworkAnimator_UserCode_RpcOnAnimationTriggerClientMessage__Int32_m3144BA9CDE4FD344ACCF9017156B28BF72668F96 (void);
// 0x0000006C System.Void Mirror.NetworkAnimator::InvokeUserCode_RpcOnAnimationTriggerClientMessage__Int32(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_RpcOnAnimationTriggerClientMessage__Int32_mBD479A0DE51BB9EEF62FE2914B4AEB620C653EC2 (void);
// 0x0000006D System.Void Mirror.NetworkAnimator::UserCode_RpcOnAnimationResetTriggerClientMessage__Int32(System.Int32)
extern void NetworkAnimator_UserCode_RpcOnAnimationResetTriggerClientMessage__Int32_m834BA2E7C7F845EC163E756984A9D37D11D2C510 (void);
// 0x0000006E System.Void Mirror.NetworkAnimator::InvokeUserCode_RpcOnAnimationResetTriggerClientMessage__Int32(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_RpcOnAnimationResetTriggerClientMessage__Int32_m97363954B4A10EB76C8805A913DDDD355DF3FAD3 (void);
// 0x0000006F System.Void Mirror.NetworkAnimator::.cctor()
extern void NetworkAnimator__cctor_mD55D588FF4458B4B31D17DD4C156FB2811919C01 (void);
// 0x00000070 System.Boolean Mirror.NetworkAnimator::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkAnimator_SerializeSyncVars_m29547B23155411F3127C7AAF934BA5BECE9A4965 (void);
// 0x00000071 System.Void Mirror.NetworkAnimator::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkAnimator_DeserializeSyncVars_m843987C2BA63B9B3D42EFFDC0E987FF6EF200DCE (void);
// 0x00000072 System.Void Mirror.NetworkLobbyManager::.ctor()
extern void NetworkLobbyManager__ctor_mDD438F9E8843DD6EC931E379E35A7DD9174F230A (void);
// 0x00000073 System.Void Mirror.NetworkLobbyPlayer::.ctor()
extern void NetworkLobbyPlayer__ctor_mDE6D3E256AA6830D69248C10B78EE6DF26D8D36F (void);
// 0x00000074 System.Void Mirror.NetworkLobbyPlayer::MirrorProcessed()
extern void NetworkLobbyPlayer_MirrorProcessed_m4F8A7A47CEAD82D21054880150975E599C32C2AA (void);
// 0x00000075 System.Void Mirror.NetworkPingDisplay::OnGUI()
extern void NetworkPingDisplay_OnGUI_m8837A9849EC29D1CA88C40497AB89882328E8C05 (void);
// 0x00000076 System.Void Mirror.NetworkPingDisplay::.ctor()
extern void NetworkPingDisplay__ctor_mBC8A423E0D15B0C993B47934910C668E3125269B (void);
// 0x00000077 System.Boolean Mirror.NetworkRoomManager::get_allPlayersReady()
extern void NetworkRoomManager_get_allPlayersReady_m3D2082D0C7505187293ACF4EE52DD040C04139A2 (void);
// 0x00000078 System.Void Mirror.NetworkRoomManager::set_allPlayersReady(System.Boolean)
extern void NetworkRoomManager_set_allPlayersReady_m3298A66DAA3046834FEBA482445BBCA7BBA48B23 (void);
// 0x00000079 System.Void Mirror.NetworkRoomManager::OnValidate()
extern void NetworkRoomManager_OnValidate_mBBFC0B5CEFFCCC7FEC0CF5C4FC431483850A9A56 (void);
// 0x0000007A System.Void Mirror.NetworkRoomManager::ReadyStatusChanged()
extern void NetworkRoomManager_ReadyStatusChanged_mB6EA41087E53FD14E0CB6EB2C1442A4B29B83C1C (void);
// 0x0000007B System.Void Mirror.NetworkRoomManager::OnServerReady(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnServerReady_m722104B1C4891F10BE0DD1314C23C93C55C562A3 (void);
// 0x0000007C System.Void Mirror.NetworkRoomManager::SceneLoadedForPlayer(Mirror.NetworkConnectionToClient,UnityEngine.GameObject)
extern void NetworkRoomManager_SceneLoadedForPlayer_m0ACA8FF2E8AEC1E7743F300A2C816B0C0EB956D7 (void);
// 0x0000007D System.Void Mirror.NetworkRoomManager::CheckReadyToBegin()
extern void NetworkRoomManager_CheckReadyToBegin_m4255BD331B9D656CD71781142BC9E23E7833B9AD (void);
// 0x0000007E System.Void Mirror.NetworkRoomManager::CallOnClientEnterRoom()
extern void NetworkRoomManager_CallOnClientEnterRoom_m9D5D793797E831E2C1865C797BBCE9BD11002F39 (void);
// 0x0000007F System.Void Mirror.NetworkRoomManager::CallOnClientExitRoom()
extern void NetworkRoomManager_CallOnClientExitRoom_mDF423C0F220431627CE40219CC21418C4EB27242 (void);
// 0x00000080 System.Void Mirror.NetworkRoomManager::OnServerConnect(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnServerConnect_mB4357D953A85EE1475F8B907233D7834C9C639E8 (void);
// 0x00000081 System.Void Mirror.NetworkRoomManager::OnServerDisconnect(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnServerDisconnect_mED4D7DBD4D9C917B2A785F3D088AB2C46BD6BC18 (void);
// 0x00000082 System.Void Mirror.NetworkRoomManager::OnServerAddPlayer(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnServerAddPlayer_m1866C29C4AF0935EEC8A05992B04456FE97ADCB9 (void);
// 0x00000083 System.Void Mirror.NetworkRoomManager::RecalculateRoomPlayerIndices()
extern void NetworkRoomManager_RecalculateRoomPlayerIndices_mDD15686D1EF99B21CB4B687A80AEEC7A8D513F22 (void);
// 0x00000084 System.Void Mirror.NetworkRoomManager::ServerChangeScene(System.String)
extern void NetworkRoomManager_ServerChangeScene_m37ECD16EAB2903DB90B89A945982491703C10632 (void);
// 0x00000085 System.Void Mirror.NetworkRoomManager::OnServerSceneChanged(System.String)
extern void NetworkRoomManager_OnServerSceneChanged_m8C9275BC2C3BAD39BF505B6346BE8D00CAF85443 (void);
// 0x00000086 System.Void Mirror.NetworkRoomManager::OnStartServer()
extern void NetworkRoomManager_OnStartServer_mB672B2EAF2186976331F24E65CA93487E10FE132 (void);
// 0x00000087 System.Void Mirror.NetworkRoomManager::OnStartHost()
extern void NetworkRoomManager_OnStartHost_m05576B071D7FBC1BFAD9099C0FAD5AAAC0FB5914 (void);
// 0x00000088 System.Void Mirror.NetworkRoomManager::OnStopServer()
extern void NetworkRoomManager_OnStopServer_mD9604B3569B3CB4C224E5982C27B02A10D4F9F72 (void);
// 0x00000089 System.Void Mirror.NetworkRoomManager::OnStopHost()
extern void NetworkRoomManager_OnStopHost_mEBDB6131C1C88915C480BD7351F1095A4FF2617A (void);
// 0x0000008A System.Void Mirror.NetworkRoomManager::OnStartClient()
extern void NetworkRoomManager_OnStartClient_m3CB42985BBC56EC3CBC6C156956E11814D4CAB5A (void);
// 0x0000008B System.Void Mirror.NetworkRoomManager::OnClientConnect()
extern void NetworkRoomManager_OnClientConnect_mDAF25D4A6D6DD2B89965E49ECFC2FA460D0B9946 (void);
// 0x0000008C System.Void Mirror.NetworkRoomManager::OnClientDisconnect()
extern void NetworkRoomManager_OnClientDisconnect_m884295F9A0714B46DEDD300F1B7528BE7C123FFA (void);
// 0x0000008D System.Void Mirror.NetworkRoomManager::OnStopClient()
extern void NetworkRoomManager_OnStopClient_m347169D4CACF5D2DF76B5A0B979AC3177900378A (void);
// 0x0000008E System.Void Mirror.NetworkRoomManager::OnClientSceneChanged()
extern void NetworkRoomManager_OnClientSceneChanged_m9AF840B1BD5364A799A6A21AB64EC2465AEF3AC8 (void);
// 0x0000008F System.Void Mirror.NetworkRoomManager::OnRoomStartHost()
extern void NetworkRoomManager_OnRoomStartHost_m48B3CEB2BADCE0213AE1573618F3329A21D6A776 (void);
// 0x00000090 System.Void Mirror.NetworkRoomManager::OnRoomStopHost()
extern void NetworkRoomManager_OnRoomStopHost_m59A119E866FD9DBA48730998B5C26759ECF92844 (void);
// 0x00000091 System.Void Mirror.NetworkRoomManager::OnRoomStartServer()
extern void NetworkRoomManager_OnRoomStartServer_m7E063FDDB0AD99262E252B407704A8F333A41100 (void);
// 0x00000092 System.Void Mirror.NetworkRoomManager::OnRoomStopServer()
extern void NetworkRoomManager_OnRoomStopServer_m22B121E391F19BBD7FE621A71469E74BB2DC0F2F (void);
// 0x00000093 System.Void Mirror.NetworkRoomManager::OnRoomServerConnect(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnRoomServerConnect_mEA0C2852C0DD7A4E2D5DED27C844B1912DFDB9F2 (void);
// 0x00000094 System.Void Mirror.NetworkRoomManager::OnRoomServerDisconnect(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnRoomServerDisconnect_mCCDEDC4DF723343782A07426B6BEA7417CC06515 (void);
// 0x00000095 System.Void Mirror.NetworkRoomManager::OnRoomServerSceneChanged(System.String)
extern void NetworkRoomManager_OnRoomServerSceneChanged_mB4102EBFB1E0A4A98E62ABA3944A7F0F2AF220CA (void);
// 0x00000096 UnityEngine.GameObject Mirror.NetworkRoomManager::OnRoomServerCreateRoomPlayer(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnRoomServerCreateRoomPlayer_mB90B114AE4682757FAF4A401234C54B9A92393E3 (void);
// 0x00000097 UnityEngine.GameObject Mirror.NetworkRoomManager::OnRoomServerCreateGamePlayer(Mirror.NetworkConnectionToClient,UnityEngine.GameObject)
extern void NetworkRoomManager_OnRoomServerCreateGamePlayer_m0A931317C73F05FAB84D40AAD5A795151D2965FA (void);
// 0x00000098 System.Void Mirror.NetworkRoomManager::OnRoomServerAddPlayer(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnRoomServerAddPlayer_m619CFC854D6EEFE62955973D796E8F09ADF87B0E (void);
// 0x00000099 System.Boolean Mirror.NetworkRoomManager::OnRoomServerSceneLoadedForPlayer(Mirror.NetworkConnectionToClient,UnityEngine.GameObject,UnityEngine.GameObject)
extern void NetworkRoomManager_OnRoomServerSceneLoadedForPlayer_m44C72F5EEEB65EF04C7381616F0012DB8A494EC0 (void);
// 0x0000009A System.Void Mirror.NetworkRoomManager::OnRoomServerPlayersReady()
extern void NetworkRoomManager_OnRoomServerPlayersReady_mE9FBB299CB678712AC6111D64694E256A820215D (void);
// 0x0000009B System.Void Mirror.NetworkRoomManager::OnRoomServerPlayersNotReady()
extern void NetworkRoomManager_OnRoomServerPlayersNotReady_m810E9E88EBB6471872A29CA17ED26F74CAC55995 (void);
// 0x0000009C System.Void Mirror.NetworkRoomManager::OnRoomClientEnter()
extern void NetworkRoomManager_OnRoomClientEnter_mC1F34FF29F855787E018A38D9733945AFB6D5ED0 (void);
// 0x0000009D System.Void Mirror.NetworkRoomManager::OnRoomClientExit()
extern void NetworkRoomManager_OnRoomClientExit_m5581110225F6CEC643BDD963777CBBB2160A7668 (void);
// 0x0000009E System.Void Mirror.NetworkRoomManager::OnRoomClientConnect()
extern void NetworkRoomManager_OnRoomClientConnect_mFC1CD2694515096D35E272E024F067A60F32CB57 (void);
// 0x0000009F System.Void Mirror.NetworkRoomManager::OnRoomClientConnect(Mirror.NetworkConnection)
extern void NetworkRoomManager_OnRoomClientConnect_m8364F1B62B69C9308CCF9EA28A845F50A5585F0A (void);
// 0x000000A0 System.Void Mirror.NetworkRoomManager::OnRoomClientDisconnect()
extern void NetworkRoomManager_OnRoomClientDisconnect_mD079444CA1F06D8A5E03E8FAD6984E273A040E63 (void);
// 0x000000A1 System.Void Mirror.NetworkRoomManager::OnRoomClientDisconnect(Mirror.NetworkConnection)
extern void NetworkRoomManager_OnRoomClientDisconnect_m7AE0AC7B5FB7432607FFB707CC17E249B1DA9896 (void);
// 0x000000A2 System.Void Mirror.NetworkRoomManager::OnRoomStartClient()
extern void NetworkRoomManager_OnRoomStartClient_m034772143DC8E6D6D7BBE8D435B4F9792E306849 (void);
// 0x000000A3 System.Void Mirror.NetworkRoomManager::OnRoomStopClient()
extern void NetworkRoomManager_OnRoomStopClient_mF85E94F1C15BBB786E60847354CB7D23362597B0 (void);
// 0x000000A4 System.Void Mirror.NetworkRoomManager::OnRoomClientSceneChanged()
extern void NetworkRoomManager_OnRoomClientSceneChanged_mD001F5BDC5A5BC18356653212308E4370E390B99 (void);
// 0x000000A5 System.Void Mirror.NetworkRoomManager::OnRoomClientSceneChanged(Mirror.NetworkConnection)
extern void NetworkRoomManager_OnRoomClientSceneChanged_mD34F382DBA6D1EF978E3ABC3C52D2CBB8CD00DE4 (void);
// 0x000000A6 System.Void Mirror.NetworkRoomManager::OnRoomClientAddPlayerFailed()
extern void NetworkRoomManager_OnRoomClientAddPlayerFailed_m341E588654470F7CFC3844A36329DE10360EDFE8 (void);
// 0x000000A7 System.Void Mirror.NetworkRoomManager::OnGUI()
extern void NetworkRoomManager_OnGUI_mE31AF52E018C083842D6A656E912882B33A9BD85 (void);
// 0x000000A8 System.Void Mirror.NetworkRoomManager::.ctor()
extern void NetworkRoomManager__ctor_mADF6C6FA77F112272B1B11D167CF9B6AEDA64FAC (void);
// 0x000000A9 System.Void Mirror.NetworkRoomManager/<>c::.cctor()
extern void U3CU3Ec__cctor_mC9018B53B62A757D391F9AA4C7C901C8FAAF6153 (void);
// 0x000000AA System.Void Mirror.NetworkRoomManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mC6C29BAB4A3EE410677501FB94F2EF94854C1C20 (void);
// 0x000000AB System.Boolean Mirror.NetworkRoomManager/<>c::<CheckReadyToBegin>b__16_0(System.Collections.Generic.KeyValuePair`2<System.Int32,Mirror.NetworkConnectionToClient>)
extern void U3CU3Ec_U3CCheckReadyToBeginU3Eb__16_0_mD539D4CF62A3D159F1A837E7C81A2476F20EE142 (void);
// 0x000000AC System.Void Mirror.NetworkRoomPlayer::Start()
extern void NetworkRoomPlayer_Start_m6EF9EE75D76707F7B26F9AEBE4EE9E9B5F3F7280 (void);
// 0x000000AD System.Void Mirror.NetworkRoomPlayer::OnDisable()
extern void NetworkRoomPlayer_OnDisable_m9F4C01360486541184371C6FCADF9DDEC25688EE (void);
// 0x000000AE System.Void Mirror.NetworkRoomPlayer::CmdChangeReadyState(System.Boolean)
extern void NetworkRoomPlayer_CmdChangeReadyState_m000A9554EE062B6FF78483B8EF55BC2C37F747CC (void);
// 0x000000AF System.Void Mirror.NetworkRoomPlayer::IndexChanged(System.Int32,System.Int32)
extern void NetworkRoomPlayer_IndexChanged_m8F0D830B73CCED0429FAF3C523AAF493E3ED4AB8 (void);
// 0x000000B0 System.Void Mirror.NetworkRoomPlayer::ReadyStateChanged(System.Boolean,System.Boolean)
extern void NetworkRoomPlayer_ReadyStateChanged_mD559C745FFBFE7550B7724D6879F168B6E7C40C8 (void);
// 0x000000B1 System.Void Mirror.NetworkRoomPlayer::OnClientEnterRoom()
extern void NetworkRoomPlayer_OnClientEnterRoom_m85D020E10399B927E54824FE8C47555B03175E72 (void);
// 0x000000B2 System.Void Mirror.NetworkRoomPlayer::OnClientExitRoom()
extern void NetworkRoomPlayer_OnClientExitRoom_mB76D7D0975B7F773A5EC5C56775BF363A58961D0 (void);
// 0x000000B3 System.Void Mirror.NetworkRoomPlayer::OnGUI()
extern void NetworkRoomPlayer_OnGUI_mFE9117894DF69C10E929E26C6675A6754778076E (void);
// 0x000000B4 System.Void Mirror.NetworkRoomPlayer::DrawPlayerReadyState()
extern void NetworkRoomPlayer_DrawPlayerReadyState_mE071CB838FDCF734437D9A1CDDD4238D3F3F0C76 (void);
// 0x000000B5 System.Void Mirror.NetworkRoomPlayer::DrawPlayerReadyButton()
extern void NetworkRoomPlayer_DrawPlayerReadyButton_mC5F76C68BA3F210A8892B244F2C551D9150FCBF1 (void);
// 0x000000B6 System.Void Mirror.NetworkRoomPlayer::.ctor()
extern void NetworkRoomPlayer__ctor_m74CD1F66EB67F56C14FE577173623D97C67AC2F4 (void);
// 0x000000B7 System.Void Mirror.NetworkRoomPlayer::MirrorProcessed()
extern void NetworkRoomPlayer_MirrorProcessed_m8F12D3397E5F231AD05BAECE3C9B127E1900F8D2 (void);
// 0x000000B8 System.Boolean Mirror.NetworkRoomPlayer::get_NetworkreadyToBegin()
extern void NetworkRoomPlayer_get_NetworkreadyToBegin_mCE15308447206625859308A223210717E40EEA20 (void);
// 0x000000B9 System.Void Mirror.NetworkRoomPlayer::set_NetworkreadyToBegin(System.Boolean)
extern void NetworkRoomPlayer_set_NetworkreadyToBegin_m69E8573E8325C9C28BC750BEC5B53990C63076BC (void);
// 0x000000BA System.Int32 Mirror.NetworkRoomPlayer::get_Networkindex()
extern void NetworkRoomPlayer_get_Networkindex_mCB254CC3650E7295574621F316249926C47BEB2B (void);
// 0x000000BB System.Void Mirror.NetworkRoomPlayer::set_Networkindex(System.Int32)
extern void NetworkRoomPlayer_set_Networkindex_mAC4BA4F2F4D428C601711E32E7CE3ED2D5B298D8 (void);
// 0x000000BC System.Void Mirror.NetworkRoomPlayer::UserCode_CmdChangeReadyState__Boolean(System.Boolean)
extern void NetworkRoomPlayer_UserCode_CmdChangeReadyState__Boolean_mC036C4A64EA54820574BAE749F549678604CEF91 (void);
// 0x000000BD System.Void Mirror.NetworkRoomPlayer::InvokeUserCode_CmdChangeReadyState__Boolean(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRoomPlayer_InvokeUserCode_CmdChangeReadyState__Boolean_mD6D0A78007DCA9399CA9CF7C8A0DF430587F6770 (void);
// 0x000000BE System.Void Mirror.NetworkRoomPlayer::.cctor()
extern void NetworkRoomPlayer__cctor_m38C2FA8512D80C691861CA50E688CA7CC41CF3EE (void);
// 0x000000BF System.Boolean Mirror.NetworkRoomPlayer::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkRoomPlayer_SerializeSyncVars_m6DE1E92238A3D8384E9D6FEF915201B86DCC48F4 (void);
// 0x000000C0 System.Void Mirror.NetworkRoomPlayer::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkRoomPlayer_DeserializeSyncVars_m098744A499CAE6CCF5F0B4CE4EFD8EB1763FD950 (void);
// 0x000000C1 System.Void Mirror.NetworkStatistics::Start()
extern void NetworkStatistics_Start_mD27C99149BC22F0371DFC0459575EA71404AE1B9 (void);
// 0x000000C2 System.Void Mirror.NetworkStatistics::OnDestroy()
extern void NetworkStatistics_OnDestroy_m490F38913FF37D0EF27BB9472A29E01BEA014918 (void);
// 0x000000C3 System.Void Mirror.NetworkStatistics::OnClientReceive(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkStatistics_OnClientReceive_m15522D2E0B7191369DE9355ABA7F90BF71CDD99E (void);
// 0x000000C4 System.Void Mirror.NetworkStatistics::OnClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkStatistics_OnClientSend_m33338E874CFA1A2FAA3505F4C50B4A7D226A7551 (void);
// 0x000000C5 System.Void Mirror.NetworkStatistics::OnServerReceive(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkStatistics_OnServerReceive_mACB3C2E96142F36C880A180BA361AFD54ED08C83 (void);
// 0x000000C6 System.Void Mirror.NetworkStatistics::OnServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkStatistics_OnServerSend_mBA206EE1021B894E4B3192C9E4C48E48BFB4E4B7 (void);
// 0x000000C7 System.Void Mirror.NetworkStatistics::Update()
extern void NetworkStatistics_Update_m7219A51EEB78B1BDC84A231E893F4B7200D9D49E (void);
// 0x000000C8 System.Void Mirror.NetworkStatistics::UpdateClient()
extern void NetworkStatistics_UpdateClient_m30614B8549A2D75E3FC88B3659FE9F5ABEA7DFC9 (void);
// 0x000000C9 System.Void Mirror.NetworkStatistics::UpdateServer()
extern void NetworkStatistics_UpdateServer_mB0E7DDEA0CBFDC0C8D32490CA77757E200CE74C3 (void);
// 0x000000CA System.Void Mirror.NetworkStatistics::OnGUI()
extern void NetworkStatistics_OnGUI_mA76E96F7EBAB1A822C45DB2EE31E62C68BE768BE (void);
// 0x000000CB System.Void Mirror.NetworkStatistics::OnClientGUI()
extern void NetworkStatistics_OnClientGUI_m7BD9B76BF5DCDDA9B2ACDDB4FF9ED4B946A7479F (void);
// 0x000000CC System.Void Mirror.NetworkStatistics::OnServerGUI()
extern void NetworkStatistics_OnServerGUI_m2A65A2A6CB5A59256691D86770DFE4955BFB7E7C (void);
// 0x000000CD System.Void Mirror.NetworkStatistics::.ctor()
extern void NetworkStatistics__ctor_m5AF22E614AF6170202942D6E9D80582DF3917CF1 (void);
// 0x000000CE UnityEngine.Transform Mirror.NetworkTransform::get_targetComponent()
extern void NetworkTransform_get_targetComponent_mFC4994F9CDF95FBAD976A28AC8E940EF17E04974 (void);
// 0x000000CF System.Void Mirror.NetworkTransform::.ctor()
extern void NetworkTransform__ctor_m4C0F82053B3B751AB1CD14631F756E29F593A4F6 (void);
// 0x000000D0 System.Void Mirror.NetworkTransform::MirrorProcessed()
extern void NetworkTransform_MirrorProcessed_mBD058E5A0361F5B3C6EC376DCA9D0A74393E1690 (void);
// 0x000000D1 System.Boolean Mirror.NetworkTransformBase::get_IsClientWithAuthority()
extern void NetworkTransformBase_get_IsClientWithAuthority_m4842DA4F62C34FD10EE8161E2BDBF18EEDDF218A (void);
// 0x000000D2 UnityEngine.Transform Mirror.NetworkTransformBase::get_targetComponent()
// 0x000000D3 System.Single Mirror.NetworkTransformBase::get_bufferTime()
extern void NetworkTransformBase_get_bufferTime_m1B5C82B0BA8A2DE2BEE359D97B13348AC72A3080 (void);
// 0x000000D4 Mirror.NTSnapshot Mirror.NetworkTransformBase::ConstructSnapshot()
extern void NetworkTransformBase_ConstructSnapshot_mC1FA78F69BA5F58304E138E23072BD9EE4A68133 (void);
// 0x000000D5 System.Void Mirror.NetworkTransformBase::ApplySnapshot(Mirror.NTSnapshot,Mirror.NTSnapshot,Mirror.NTSnapshot)
extern void NetworkTransformBase_ApplySnapshot_m69F8D52B462CCA5723E99FDC224733F69267E59E (void);
// 0x000000D6 System.Boolean Mirror.NetworkTransformBase::CompareSnapshots(Mirror.NTSnapshot)
extern void NetworkTransformBase_CompareSnapshots_m5A648CA2DD6DF96B62668101C2D389CCCDD6093B (void);
// 0x000000D7 System.Void Mirror.NetworkTransformBase::CmdClientToServerSync(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkTransformBase_CmdClientToServerSync_m912A267278EE16E9636BFA7B70B2DB2099E6F891 (void);
// 0x000000D8 System.Void Mirror.NetworkTransformBase::OnClientToServerSync(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkTransformBase_OnClientToServerSync_mD0C05A2ADDD36FDE327FEE8A3D5C64E227D8AF36 (void);
// 0x000000D9 System.Void Mirror.NetworkTransformBase::RpcServerToClientSync(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkTransformBase_RpcServerToClientSync_mAB05B7FD9EDF0C1F38C8E38BD3AF5310723F21BE (void);
// 0x000000DA System.Void Mirror.NetworkTransformBase::OnServerToClientSync(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkTransformBase_OnServerToClientSync_mA4929D2693891DBB4367B883644FAE22FDCAD9BF (void);
// 0x000000DB System.Void Mirror.NetworkTransformBase::UpdateServer()
extern void NetworkTransformBase_UpdateServer_mD27A06E138396A3CDFE1C694059285D88797C51F (void);
// 0x000000DC System.Void Mirror.NetworkTransformBase::UpdateClient()
extern void NetworkTransformBase_UpdateClient_m81EC6BA4DFD32748F11CCC4E282B1ED54DEB94D4 (void);
// 0x000000DD System.Void Mirror.NetworkTransformBase::Update()
extern void NetworkTransformBase_Update_mB7004C46943A35BE3FD76EAE96EB0B1D0FA52B31 (void);
// 0x000000DE System.Void Mirror.NetworkTransformBase::OnTeleport(UnityEngine.Vector3)
extern void NetworkTransformBase_OnTeleport_m8DE18114C98F024D1449F9E7838211C0AEE06ECB (void);
// 0x000000DF System.Void Mirror.NetworkTransformBase::OnTeleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_OnTeleport_mEF0E028428D75F39607798E34E7330CA74E3435B (void);
// 0x000000E0 System.Void Mirror.NetworkTransformBase::RpcTeleport(UnityEngine.Vector3)
extern void NetworkTransformBase_RpcTeleport_m8F984E88B2BB51B01EB14DCB70BD6B9F4582CEDD (void);
// 0x000000E1 System.Void Mirror.NetworkTransformBase::RpcTeleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_RpcTeleport_m073EA6A55FF2452BEE377422CB362A909A60B8E2 (void);
// 0x000000E2 System.Void Mirror.NetworkTransformBase::RpcTeleportAndRotate(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_RpcTeleportAndRotate_m438027D989CDEA06B42C9FA2E7486E046EF18538 (void);
// 0x000000E3 System.Void Mirror.NetworkTransformBase::CmdTeleport(UnityEngine.Vector3)
extern void NetworkTransformBase_CmdTeleport_m6C5557F3E6E7A0578D1509D2794721F8EACDF505 (void);
// 0x000000E4 System.Void Mirror.NetworkTransformBase::CmdTeleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_CmdTeleport_m17DA700489EE7C25D0E258D2511D4A159BB67C40 (void);
// 0x000000E5 System.Void Mirror.NetworkTransformBase::CmdTeleportAndRotate(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_CmdTeleportAndRotate_mF7A98E2F343F702B155606C1352B9D86741876D2 (void);
// 0x000000E6 System.Void Mirror.NetworkTransformBase::Reset()
extern void NetworkTransformBase_Reset_m413C870079713BE5B927D42BBF1538FDE3A6752B (void);
// 0x000000E7 System.Void Mirror.NetworkTransformBase::OnDisable()
extern void NetworkTransformBase_OnDisable_m789E9CF179F78C53F78316F5470593FAB69E6F56 (void);
// 0x000000E8 System.Void Mirror.NetworkTransformBase::OnEnable()
extern void NetworkTransformBase_OnEnable_m4D0775F66252AAFE8700EBE4570AD51741A113F1 (void);
// 0x000000E9 System.Void Mirror.NetworkTransformBase::OnValidate()
extern void NetworkTransformBase_OnValidate_mC1C2EEF652769DD0AD4C7F47EB85F3EFA276E5E0 (void);
// 0x000000EA System.Boolean Mirror.NetworkTransformBase::OnSerialize(Mirror.NetworkWriter,System.Boolean)
extern void NetworkTransformBase_OnSerialize_m20F95948C257ED8A185D85474387D6D706D15F6C (void);
// 0x000000EB System.Void Mirror.NetworkTransformBase::OnDeserialize(Mirror.NetworkReader,System.Boolean)
extern void NetworkTransformBase_OnDeserialize_m4476BC41308C9029BE559F9AE8D1060D7300FF8E (void);
// 0x000000EC System.Void Mirror.NetworkTransformBase::.ctor()
extern void NetworkTransformBase__ctor_m5D39D8174AD78B18A54EA9CC4AD441C41B9D5279 (void);
// 0x000000ED System.Void Mirror.NetworkTransformBase::MirrorProcessed()
extern void NetworkTransformBase_MirrorProcessed_mCAC10AC384F4D5DB2184D989D0B69CB919A592AA (void);
// 0x000000EE System.Void Mirror.NetworkTransformBase::UserCode_CmdClientToServerSync__Nullable`1__Nullable`1__Nullable`1(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkTransformBase_UserCode_CmdClientToServerSync__Nullable_1__Nullable_1__Nullable_1_m4F781351B6313C63D4F9D98CD2053463D0F417DA (void);
// 0x000000EF System.Void Mirror.NetworkTransformBase::InvokeUserCode_CmdClientToServerSync__Nullable`1__Nullable`1__Nullable`1(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_CmdClientToServerSync__Nullable_1__Nullable_1__Nullable_1_m98E7B81C718F89D232305CE9427D91FEED5A51F5 (void);
// 0x000000F0 System.Void Mirror.NetworkTransformBase::UserCode_RpcServerToClientSync__Nullable`1__Nullable`1__Nullable`1(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkTransformBase_UserCode_RpcServerToClientSync__Nullable_1__Nullable_1__Nullable_1_m87DA955CDAA2A3F88D70F6A55AA6AF520D3850A7 (void);
// 0x000000F1 System.Void Mirror.NetworkTransformBase::InvokeUserCode_RpcServerToClientSync__Nullable`1__Nullable`1__Nullable`1(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_RpcServerToClientSync__Nullable_1__Nullable_1__Nullable_1_m00F366184378BC5621A53AE9164D36B43EAD6140 (void);
// 0x000000F2 System.Void Mirror.NetworkTransformBase::UserCode_RpcTeleport__Vector3(UnityEngine.Vector3)
extern void NetworkTransformBase_UserCode_RpcTeleport__Vector3_mF15097C4D3D9BF5DDCC9DD5C12A82A936897AC11 (void);
// 0x000000F3 System.Void Mirror.NetworkTransformBase::InvokeUserCode_RpcTeleport__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_RpcTeleport__Vector3_m10EA5EA7EEB6F7D22AF07A5259C2A390C34F873B (void);
// 0x000000F4 System.Void Mirror.NetworkTransformBase::UserCode_RpcTeleport__Vector3__Quaternion(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_UserCode_RpcTeleport__Vector3__Quaternion_m5A641AAAD87ECEAB5C4C039A4EAD987E44DA2E20 (void);
// 0x000000F5 System.Void Mirror.NetworkTransformBase::InvokeUserCode_RpcTeleport__Vector3__Quaternion(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_RpcTeleport__Vector3__Quaternion_mF8411DF293C91CB3207A22F0A319E70AD875D6E2 (void);
// 0x000000F6 System.Void Mirror.NetworkTransformBase::UserCode_RpcTeleportAndRotate__Vector3__Quaternion(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_UserCode_RpcTeleportAndRotate__Vector3__Quaternion_m44B7D8F60DDE38EB16162B8206AABE57C9DB10E7 (void);
// 0x000000F7 System.Void Mirror.NetworkTransformBase::InvokeUserCode_RpcTeleportAndRotate__Vector3__Quaternion(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_RpcTeleportAndRotate__Vector3__Quaternion_m14633BCD6A50F4CE01EED94730DB263F4318A388 (void);
// 0x000000F8 System.Void Mirror.NetworkTransformBase::UserCode_CmdTeleport__Vector3(UnityEngine.Vector3)
extern void NetworkTransformBase_UserCode_CmdTeleport__Vector3_m480D819918CD7F7BAF42AAD2F70C124DEB75E038 (void);
// 0x000000F9 System.Void Mirror.NetworkTransformBase::InvokeUserCode_CmdTeleport__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_CmdTeleport__Vector3_m93525715C8208A3B8BDE7DDF858312E643883190 (void);
// 0x000000FA System.Void Mirror.NetworkTransformBase::UserCode_CmdTeleport__Vector3__Quaternion(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_UserCode_CmdTeleport__Vector3__Quaternion_m5BD0928D8F4441D66FF71BF0E956262C97490013 (void);
// 0x000000FB System.Void Mirror.NetworkTransformBase::InvokeUserCode_CmdTeleport__Vector3__Quaternion(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_CmdTeleport__Vector3__Quaternion_mE6EE9AA0BDA716133F80DAB7339672E5F8B9AD7E (void);
// 0x000000FC System.Void Mirror.NetworkTransformBase::UserCode_CmdTeleportAndRotate__Vector3__Quaternion(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_UserCode_CmdTeleportAndRotate__Vector3__Quaternion_m988FE9B5046BD9F99262C6C8480F68FBBBC5BAD9 (void);
// 0x000000FD System.Void Mirror.NetworkTransformBase::InvokeUserCode_CmdTeleportAndRotate__Vector3__Quaternion(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_CmdTeleportAndRotate__Vector3__Quaternion_m704E2B0211403EDC73DD43ADCF3A5AF8781EEF00 (void);
// 0x000000FE System.Void Mirror.NetworkTransformBase::.cctor()
extern void NetworkTransformBase__cctor_m626DF45B29C12A0F19AD72B4A8E7FDD43DDBFA8B (void);
// 0x000000FF UnityEngine.Transform Mirror.NetworkTransformChild::get_targetComponent()
extern void NetworkTransformChild_get_targetComponent_m3DF9FD6F234A2EF6C017DA3224A0523D931E37FA (void);
// 0x00000100 System.Void Mirror.NetworkTransformChild::.ctor()
extern void NetworkTransformChild__ctor_mD838D95F75CAC92A52E737D3248FA00EA5302D2C (void);
// 0x00000101 System.Void Mirror.NetworkTransformChild::MirrorProcessed()
extern void NetworkTransformChild_MirrorProcessed_m674B9987C43009E2C46A2F69CEE0FBA1AAFF29A5 (void);
// 0x00000102 System.Double Mirror.NTSnapshot::get_remoteTimestamp()
extern void NTSnapshot_get_remoteTimestamp_m1AA923C8C9FF1F473CEECEAC7AB1BFB887999BBE (void);
// 0x00000103 System.Void Mirror.NTSnapshot::set_remoteTimestamp(System.Double)
extern void NTSnapshot_set_remoteTimestamp_mB571444062A9349310C12DEBCF202DF5E6D90038 (void);
// 0x00000104 System.Double Mirror.NTSnapshot::get_localTimestamp()
extern void NTSnapshot_get_localTimestamp_mB4EB78DF581B393267C3B8B9E922D83B9388E4FE (void);
// 0x00000105 System.Void Mirror.NTSnapshot::set_localTimestamp(System.Double)
extern void NTSnapshot_set_localTimestamp_mDE387C34337B2611340C05E048A3BAD0832E7111 (void);
// 0x00000106 System.Void Mirror.NTSnapshot::.ctor(System.Double,System.Double,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void NTSnapshot__ctor_mD163416E3EC45F2AC30B3BF21C603EFCDF6A2D1B (void);
// 0x00000107 Mirror.NTSnapshot Mirror.NTSnapshot::Interpolate(Mirror.NTSnapshot,Mirror.NTSnapshot,System.Double)
extern void NTSnapshot_Interpolate_mE0508B4ABF4DE8B33DFD3509F41EC70C7C7E4F5F (void);
// 0x00000108 System.Boolean Mirror.Experimental.NetworkLerpRigidbody::get_IgnoreSync()
extern void NetworkLerpRigidbody_get_IgnoreSync_m0ED2D74F9EA799C910BD8CE2046BA07EA4BCDAD5 (void);
// 0x00000109 System.Boolean Mirror.Experimental.NetworkLerpRigidbody::get_ClientWithAuthority()
extern void NetworkLerpRigidbody_get_ClientWithAuthority_mCA3FA6948CD62ECE1FE9619173F6F4C24BAFA1AE (void);
// 0x0000010A System.Void Mirror.Experimental.NetworkLerpRigidbody::OnValidate()
extern void NetworkLerpRigidbody_OnValidate_m51A20D53A25FCE7510C97C08826C8FBAFC2912FF (void);
// 0x0000010B System.Void Mirror.Experimental.NetworkLerpRigidbody::Update()
extern void NetworkLerpRigidbody_Update_m61EF256EB33FC7A341FE59C049320280CD36BAE9 (void);
// 0x0000010C System.Void Mirror.Experimental.NetworkLerpRigidbody::SyncToClients()
extern void NetworkLerpRigidbody_SyncToClients_m1078A84609C18A2D554BA306B30BA906E1E6D576 (void);
// 0x0000010D System.Void Mirror.Experimental.NetworkLerpRigidbody::SendToServer()
extern void NetworkLerpRigidbody_SendToServer_mC5DE95622F16A8F9FF7F13E7F8059681A3A80142 (void);
// 0x0000010E System.Void Mirror.Experimental.NetworkLerpRigidbody::CmdSendState(UnityEngine.Vector3,UnityEngine.Vector3)
extern void NetworkLerpRigidbody_CmdSendState_m93401347EC12BF8A425DB7609CC4C50F9480836E (void);
// 0x0000010F System.Void Mirror.Experimental.NetworkLerpRigidbody::FixedUpdate()
extern void NetworkLerpRigidbody_FixedUpdate_mAC7598042C3158A83B179B478D80785CA0D90712 (void);
// 0x00000110 System.Void Mirror.Experimental.NetworkLerpRigidbody::.ctor()
extern void NetworkLerpRigidbody__ctor_m3683F5C16A7E2EE236F48A9B6A1C2D537F63E4A9 (void);
// 0x00000111 System.Void Mirror.Experimental.NetworkLerpRigidbody::MirrorProcessed()
extern void NetworkLerpRigidbody_MirrorProcessed_mC79601FA6A1EA88D4EC3D9E19801B7B4DDDF8237 (void);
// 0x00000112 UnityEngine.Vector3 Mirror.Experimental.NetworkLerpRigidbody::get_NetworktargetVelocity()
extern void NetworkLerpRigidbody_get_NetworktargetVelocity_mFC35BFA0C9539B67AA04145E6D55AAAEC562216B (void);
// 0x00000113 System.Void Mirror.Experimental.NetworkLerpRigidbody::set_NetworktargetVelocity(UnityEngine.Vector3)
extern void NetworkLerpRigidbody_set_NetworktargetVelocity_m57CA07453234CB22A3087BB2C11F3DAE1BA2AB0A (void);
// 0x00000114 UnityEngine.Vector3 Mirror.Experimental.NetworkLerpRigidbody::get_NetworktargetPosition()
extern void NetworkLerpRigidbody_get_NetworktargetPosition_mE50A8DE64B66990CE242B23A0A64A4E47ED3D174 (void);
// 0x00000115 System.Void Mirror.Experimental.NetworkLerpRigidbody::set_NetworktargetPosition(UnityEngine.Vector3)
extern void NetworkLerpRigidbody_set_NetworktargetPosition_mFDFB0915494D4E4A7041BBC2A32401424EC1A737 (void);
// 0x00000116 System.Void Mirror.Experimental.NetworkLerpRigidbody::UserCode_CmdSendState__Vector3__Vector3(UnityEngine.Vector3,UnityEngine.Vector3)
extern void NetworkLerpRigidbody_UserCode_CmdSendState__Vector3__Vector3_mE901895B6550CCA26C600C73A67556B60C54FC2F (void);
// 0x00000117 System.Void Mirror.Experimental.NetworkLerpRigidbody::InvokeUserCode_CmdSendState__Vector3__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkLerpRigidbody_InvokeUserCode_CmdSendState__Vector3__Vector3_mA3F2F1CF77B8E5E177FD68CF1C0BC82CF7AF0AD9 (void);
// 0x00000118 System.Void Mirror.Experimental.NetworkLerpRigidbody::.cctor()
extern void NetworkLerpRigidbody__cctor_m7C404671430C36C4331BD5F09F808E44B327AFC5 (void);
// 0x00000119 System.Boolean Mirror.Experimental.NetworkLerpRigidbody::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkLerpRigidbody_SerializeSyncVars_mBC592FBA9BBBE0A0401CD634731EECD76E9E73A8 (void);
// 0x0000011A System.Void Mirror.Experimental.NetworkLerpRigidbody::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkLerpRigidbody_DeserializeSyncVars_m46F8767D5BEBDE14AADE3C4F3038EBDA6200EA04 (void);
// 0x0000011B System.Void Mirror.Experimental.NetworkRigidbody::OnValidate()
extern void NetworkRigidbody_OnValidate_mEAC00439EC11949D787A204DA7AC4816485EB624 (void);
// 0x0000011C System.Boolean Mirror.Experimental.NetworkRigidbody::get_IgnoreSync()
extern void NetworkRigidbody_get_IgnoreSync_m8A4A9BCF9C61B22AE1D9E0C2F6FED2179F9D26F5 (void);
// 0x0000011D System.Boolean Mirror.Experimental.NetworkRigidbody::get_ClientWithAuthority()
extern void NetworkRigidbody_get_ClientWithAuthority_m35787455D13760A0CFE8D0E025FDC62C3F3BFD49 (void);
// 0x0000011E System.Void Mirror.Experimental.NetworkRigidbody::OnVelocityChanged(UnityEngine.Vector3,UnityEngine.Vector3)
extern void NetworkRigidbody_OnVelocityChanged_mA7C108A53E19FDDCED6DA74DD0109423DC48FAD2 (void);
// 0x0000011F System.Void Mirror.Experimental.NetworkRigidbody::OnAngularVelocityChanged(UnityEngine.Vector3,UnityEngine.Vector3)
extern void NetworkRigidbody_OnAngularVelocityChanged_mF3CF1B86FFA297336F21B71F452E33E0661496C5 (void);
// 0x00000120 System.Void Mirror.Experimental.NetworkRigidbody::OnIsKinematicChanged(System.Boolean,System.Boolean)
extern void NetworkRigidbody_OnIsKinematicChanged_mB1B2A2680510626A35B8E169001AE74D8D412A4E (void);
// 0x00000121 System.Void Mirror.Experimental.NetworkRigidbody::OnUseGravityChanged(System.Boolean,System.Boolean)
extern void NetworkRigidbody_OnUseGravityChanged_m360676D33BCB5AF32E55394AC21501AF0457DB08 (void);
// 0x00000122 System.Void Mirror.Experimental.NetworkRigidbody::OnuDragChanged(System.Single,System.Single)
extern void NetworkRigidbody_OnuDragChanged_mAF40FE2070567FEA56F9A999A26B0BB62D52EE6F (void);
// 0x00000123 System.Void Mirror.Experimental.NetworkRigidbody::OnAngularDragChanged(System.Single,System.Single)
extern void NetworkRigidbody_OnAngularDragChanged_m4C51F88AFB0597BD044579F30E586BA0DB061994 (void);
// 0x00000124 System.Void Mirror.Experimental.NetworkRigidbody::Update()
extern void NetworkRigidbody_Update_mDC5DB47294AEA738C5F80BE2D1B133524AF99E80 (void);
// 0x00000125 System.Void Mirror.Experimental.NetworkRigidbody::FixedUpdate()
extern void NetworkRigidbody_FixedUpdate_m0F56F9647E1ADFC98FE2AF3E9AE798097E45C63C (void);
// 0x00000126 System.Void Mirror.Experimental.NetworkRigidbody::SyncToClients()
extern void NetworkRigidbody_SyncToClients_m90F9398F3676F811B25FA011E772F9D81C60A78C (void);
// 0x00000127 System.Void Mirror.Experimental.NetworkRigidbody::SendToServer()
extern void NetworkRigidbody_SendToServer_mF133C6FB4703D6FA725FD49EDA82EC5497A9C3E4 (void);
// 0x00000128 System.Void Mirror.Experimental.NetworkRigidbody::SendVelocity()
extern void NetworkRigidbody_SendVelocity_m3769B4FB59251D4BDA2AE142274F21BDF703131D (void);
// 0x00000129 System.Void Mirror.Experimental.NetworkRigidbody::SendRigidBodySettings()
extern void NetworkRigidbody_SendRigidBodySettings_mE5E53E6D8EDE59550F4DBCD908C3B1868D6D5A89 (void);
// 0x0000012A System.Void Mirror.Experimental.NetworkRigidbody::CmdSendVelocity(UnityEngine.Vector3)
extern void NetworkRigidbody_CmdSendVelocity_mA2C9C1BBF59E5D7AFB0FDF1F9F01A091190BFF21 (void);
// 0x0000012B System.Void Mirror.Experimental.NetworkRigidbody::CmdSendVelocityAndAngular(UnityEngine.Vector3,UnityEngine.Vector3)
extern void NetworkRigidbody_CmdSendVelocityAndAngular_mBE8AB7AB2EE9BC6C1B3E03E44EB244DE384E52EE (void);
// 0x0000012C System.Void Mirror.Experimental.NetworkRigidbody::CmdSendIsKinematic(System.Boolean)
extern void NetworkRigidbody_CmdSendIsKinematic_mADAE264BA3DF5A19173377DF9B3B3B9F10C0E486 (void);
// 0x0000012D System.Void Mirror.Experimental.NetworkRigidbody::CmdSendUseGravity(System.Boolean)
extern void NetworkRigidbody_CmdSendUseGravity_m3298BAD8192D4336B7372CA486C0AE2188DF85D5 (void);
// 0x0000012E System.Void Mirror.Experimental.NetworkRigidbody::CmdSendDrag(System.Single)
extern void NetworkRigidbody_CmdSendDrag_mB36830B010A748F6FD83CD718B6A4B778314405F (void);
// 0x0000012F System.Void Mirror.Experimental.NetworkRigidbody::CmdSendAngularDrag(System.Single)
extern void NetworkRigidbody_CmdSendAngularDrag_m8D02B2D7912120C22E6F96383108B6A5DA6102FE (void);
// 0x00000130 System.Void Mirror.Experimental.NetworkRigidbody::.ctor()
extern void NetworkRigidbody__ctor_m84AE218BBA8229A552B53FEC93456AB77BAF0B17 (void);
// 0x00000131 System.Void Mirror.Experimental.NetworkRigidbody::MirrorProcessed()
extern void NetworkRigidbody_MirrorProcessed_mCF978BCD865DCA8411D3562265D71C43C2BA4740 (void);
// 0x00000132 UnityEngine.Vector3 Mirror.Experimental.NetworkRigidbody::get_Networkvelocity()
extern void NetworkRigidbody_get_Networkvelocity_mEF0D199753411679BA0684957FE371703D34476B (void);
// 0x00000133 System.Void Mirror.Experimental.NetworkRigidbody::set_Networkvelocity(UnityEngine.Vector3)
extern void NetworkRigidbody_set_Networkvelocity_m133B3EDAEF908DFD45B9547D9A28E0E8F58B7BD8 (void);
// 0x00000134 UnityEngine.Vector3 Mirror.Experimental.NetworkRigidbody::get_NetworkangularVelocity()
extern void NetworkRigidbody_get_NetworkangularVelocity_m066174F245E22E23840E2408BFC3A34A09F77586 (void);
// 0x00000135 System.Void Mirror.Experimental.NetworkRigidbody::set_NetworkangularVelocity(UnityEngine.Vector3)
extern void NetworkRigidbody_set_NetworkangularVelocity_m1CDFD3728122933194B6A06AA9707310DF039D06 (void);
// 0x00000136 System.Boolean Mirror.Experimental.NetworkRigidbody::get_NetworkisKinematic()
extern void NetworkRigidbody_get_NetworkisKinematic_mBB6F8AA6FA957F8BBD9F3A8138E0EC97E54B8133 (void);
// 0x00000137 System.Void Mirror.Experimental.NetworkRigidbody::set_NetworkisKinematic(System.Boolean)
extern void NetworkRigidbody_set_NetworkisKinematic_mCD921B67C804E7B3FFDA0355CA13E7CD81640225 (void);
// 0x00000138 System.Boolean Mirror.Experimental.NetworkRigidbody::get_NetworkuseGravity()
extern void NetworkRigidbody_get_NetworkuseGravity_mB0170724514DC1235BC20BCAEC6CD1DC647AB1B5 (void);
// 0x00000139 System.Void Mirror.Experimental.NetworkRigidbody::set_NetworkuseGravity(System.Boolean)
extern void NetworkRigidbody_set_NetworkuseGravity_m4FCCD8F5A6165295E81ABA0A3AEADA56C1359469 (void);
// 0x0000013A System.Single Mirror.Experimental.NetworkRigidbody::get_Networkdrag()
extern void NetworkRigidbody_get_Networkdrag_m52BE28035A1CCEB6975B65A1E62EF09C27B9FB5E (void);
// 0x0000013B System.Void Mirror.Experimental.NetworkRigidbody::set_Networkdrag(System.Single)
extern void NetworkRigidbody_set_Networkdrag_m831F50A14CD3AD6FFCDD3AD6C446BFA6E084D320 (void);
// 0x0000013C System.Single Mirror.Experimental.NetworkRigidbody::get_NetworkangularDrag()
extern void NetworkRigidbody_get_NetworkangularDrag_mD3A2530687880B0C0762F1E313A4A038C50E119F (void);
// 0x0000013D System.Void Mirror.Experimental.NetworkRigidbody::set_NetworkangularDrag(System.Single)
extern void NetworkRigidbody_set_NetworkangularDrag_mAEB65DC77BBBB1702176774A208A526DDCCCAD7B (void);
// 0x0000013E System.Void Mirror.Experimental.NetworkRigidbody::UserCode_CmdSendVelocity__Vector3(UnityEngine.Vector3)
extern void NetworkRigidbody_UserCode_CmdSendVelocity__Vector3_m63EEECCE66CA0D5D6FA36697D3020671CAB8DF8D (void);
// 0x0000013F System.Void Mirror.Experimental.NetworkRigidbody::InvokeUserCode_CmdSendVelocity__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody_InvokeUserCode_CmdSendVelocity__Vector3_mEB43C89B0E69623242A190EE837C6823D4667DE9 (void);
// 0x00000140 System.Void Mirror.Experimental.NetworkRigidbody::UserCode_CmdSendVelocityAndAngular__Vector3__Vector3(UnityEngine.Vector3,UnityEngine.Vector3)
extern void NetworkRigidbody_UserCode_CmdSendVelocityAndAngular__Vector3__Vector3_mAA3E685714459A666BEE87D162688C295BAF5EA0 (void);
// 0x00000141 System.Void Mirror.Experimental.NetworkRigidbody::InvokeUserCode_CmdSendVelocityAndAngular__Vector3__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody_InvokeUserCode_CmdSendVelocityAndAngular__Vector3__Vector3_mD40CB5650874EBA89B5B3C16C4C45528DE8AFA17 (void);
// 0x00000142 System.Void Mirror.Experimental.NetworkRigidbody::UserCode_CmdSendIsKinematic__Boolean(System.Boolean)
extern void NetworkRigidbody_UserCode_CmdSendIsKinematic__Boolean_mDFA5AA5020A2FB98F883334AE28AA5FFF07B152C (void);
// 0x00000143 System.Void Mirror.Experimental.NetworkRigidbody::InvokeUserCode_CmdSendIsKinematic__Boolean(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody_InvokeUserCode_CmdSendIsKinematic__Boolean_m7EF9E9DA879635852C6FBA43630B593B3F31F478 (void);
// 0x00000144 System.Void Mirror.Experimental.NetworkRigidbody::UserCode_CmdSendUseGravity__Boolean(System.Boolean)
extern void NetworkRigidbody_UserCode_CmdSendUseGravity__Boolean_mCB8FED78264795AC668DC081AC7ABF6FD738359D (void);
// 0x00000145 System.Void Mirror.Experimental.NetworkRigidbody::InvokeUserCode_CmdSendUseGravity__Boolean(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody_InvokeUserCode_CmdSendUseGravity__Boolean_m72B42BBEC1A25EA52D6B5662E14FC5AA94938D02 (void);
// 0x00000146 System.Void Mirror.Experimental.NetworkRigidbody::UserCode_CmdSendDrag__Single(System.Single)
extern void NetworkRigidbody_UserCode_CmdSendDrag__Single_m92957119B7E9349480C11309B7EB35934402193B (void);
// 0x00000147 System.Void Mirror.Experimental.NetworkRigidbody::InvokeUserCode_CmdSendDrag__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody_InvokeUserCode_CmdSendDrag__Single_mEC583CEF7956AD90F0AF3905C3B3801FBBEF4819 (void);
// 0x00000148 System.Void Mirror.Experimental.NetworkRigidbody::UserCode_CmdSendAngularDrag__Single(System.Single)
extern void NetworkRigidbody_UserCode_CmdSendAngularDrag__Single_mFA69E95FE27BA7CF823EB8BE6BDC899E745C7E79 (void);
// 0x00000149 System.Void Mirror.Experimental.NetworkRigidbody::InvokeUserCode_CmdSendAngularDrag__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody_InvokeUserCode_CmdSendAngularDrag__Single_m0B33877523BBD08B3F1B70551DE8DA66308B71E3 (void);
// 0x0000014A System.Void Mirror.Experimental.NetworkRigidbody::.cctor()
extern void NetworkRigidbody__cctor_m3F631417A8D52A890EF443F65164C1FB3409C5A9 (void);
// 0x0000014B System.Boolean Mirror.Experimental.NetworkRigidbody::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkRigidbody_SerializeSyncVars_m074559AAAB496A1A89B665A26325B75CBCA3524A (void);
// 0x0000014C System.Void Mirror.Experimental.NetworkRigidbody::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkRigidbody_DeserializeSyncVars_m8971AA80CBEE9BC6758AF7381C07DBEFB4E1810F (void);
// 0x0000014D System.Void Mirror.Experimental.NetworkRigidbody/ClientSyncState::.ctor()
extern void ClientSyncState__ctor_mC543590AB9EEAEAF13854D46B768CF26EDC5B08A (void);
// 0x0000014E System.Void Mirror.Experimental.NetworkRigidbody2D::OnValidate()
extern void NetworkRigidbody2D_OnValidate_m12F3620BA2EE1745D8368ED7286B2A580D587132 (void);
// 0x0000014F System.Boolean Mirror.Experimental.NetworkRigidbody2D::get_IgnoreSync()
extern void NetworkRigidbody2D_get_IgnoreSync_m008BA545EAE32A3F7748CF36AEF69358A892A0AC (void);
// 0x00000150 System.Boolean Mirror.Experimental.NetworkRigidbody2D::get_ClientWithAuthority()
extern void NetworkRigidbody2D_get_ClientWithAuthority_m419682C385D0B113199042FF253D1FAC8EE680DD (void);
// 0x00000151 System.Void Mirror.Experimental.NetworkRigidbody2D::OnVelocityChanged(UnityEngine.Vector2,UnityEngine.Vector2)
extern void NetworkRigidbody2D_OnVelocityChanged_mB52A772E8868846B22CDF591EF379ACFED0B4649 (void);
// 0x00000152 System.Void Mirror.Experimental.NetworkRigidbody2D::OnAngularVelocityChanged(System.Single,System.Single)
extern void NetworkRigidbody2D_OnAngularVelocityChanged_mDEDAF5034D6F42667AC43AA2EB22672FB9B9CC2A (void);
// 0x00000153 System.Void Mirror.Experimental.NetworkRigidbody2D::OnIsKinematicChanged(System.Boolean,System.Boolean)
extern void NetworkRigidbody2D_OnIsKinematicChanged_mE6695E5F0CE731AA2955F54B1E7DED4A7E63BC51 (void);
// 0x00000154 System.Void Mirror.Experimental.NetworkRigidbody2D::OnGravityScaleChanged(System.Single,System.Single)
extern void NetworkRigidbody2D_OnGravityScaleChanged_m9887C2BB1F8DDDFD8C5A6BAD12DF484CD8D4BDB9 (void);
// 0x00000155 System.Void Mirror.Experimental.NetworkRigidbody2D::OnuDragChanged(System.Single,System.Single)
extern void NetworkRigidbody2D_OnuDragChanged_m7C0CDFCB341A5CD040B227C4F4E3E805759F2C45 (void);
// 0x00000156 System.Void Mirror.Experimental.NetworkRigidbody2D::OnAngularDragChanged(System.Single,System.Single)
extern void NetworkRigidbody2D_OnAngularDragChanged_m41AF89DA353773E70C00468316DE34B9A42C51F1 (void);
// 0x00000157 System.Void Mirror.Experimental.NetworkRigidbody2D::Update()
extern void NetworkRigidbody2D_Update_m832C5AF365D3E3A6F8BEF851AD6730B57F8D0BA7 (void);
// 0x00000158 System.Void Mirror.Experimental.NetworkRigidbody2D::FixedUpdate()
extern void NetworkRigidbody2D_FixedUpdate_m8AC6065AF7928EC114DC8B3B68BD1C8F920F4C56 (void);
// 0x00000159 System.Void Mirror.Experimental.NetworkRigidbody2D::SyncToClients()
extern void NetworkRigidbody2D_SyncToClients_m9CF6E870AE483A560522711D1EEFCA2812BF722B (void);
// 0x0000015A System.Void Mirror.Experimental.NetworkRigidbody2D::SendToServer()
extern void NetworkRigidbody2D_SendToServer_m2F258C46315494DA0A216062D835A1DFA1BF2F82 (void);
// 0x0000015B System.Void Mirror.Experimental.NetworkRigidbody2D::SendVelocity()
extern void NetworkRigidbody2D_SendVelocity_m488AC1D609CBDDB99E7692D9782EE48CEEE1E3D0 (void);
// 0x0000015C System.Void Mirror.Experimental.NetworkRigidbody2D::SendRigidBodySettings()
extern void NetworkRigidbody2D_SendRigidBodySettings_m0FD6B32441438E45B9EE602BF9722CB5129A2773 (void);
// 0x0000015D System.Void Mirror.Experimental.NetworkRigidbody2D::CmdSendVelocity(UnityEngine.Vector2)
extern void NetworkRigidbody2D_CmdSendVelocity_m346CC5DD8CF8B413F17C77ED141104B9515A003E (void);
// 0x0000015E System.Void Mirror.Experimental.NetworkRigidbody2D::CmdSendVelocityAndAngular(UnityEngine.Vector2,System.Single)
extern void NetworkRigidbody2D_CmdSendVelocityAndAngular_mC11356B93759425A91C385B4D98905ABC29C6992 (void);
// 0x0000015F System.Void Mirror.Experimental.NetworkRigidbody2D::CmdSendIsKinematic(System.Boolean)
extern void NetworkRigidbody2D_CmdSendIsKinematic_mC395694DE63C8727AD33FDAF5E9B696135C2BF43 (void);
// 0x00000160 System.Void Mirror.Experimental.NetworkRigidbody2D::CmdChangeGravityScale(System.Single)
extern void NetworkRigidbody2D_CmdChangeGravityScale_mFCA659FC30BB080688C0BBA2B1AF4553D113391C (void);
// 0x00000161 System.Void Mirror.Experimental.NetworkRigidbody2D::CmdSendDrag(System.Single)
extern void NetworkRigidbody2D_CmdSendDrag_m314C188BE4CE3B8C18AA095C4697E5C7495F9199 (void);
// 0x00000162 System.Void Mirror.Experimental.NetworkRigidbody2D::CmdSendAngularDrag(System.Single)
extern void NetworkRigidbody2D_CmdSendAngularDrag_mB12CD2A1C195502EC36CD38AEFD04BF654E39C48 (void);
// 0x00000163 System.Void Mirror.Experimental.NetworkRigidbody2D::.ctor()
extern void NetworkRigidbody2D__ctor_mD7241A558F5A2FD0E2BF9BAE59B5E2E13A903627 (void);
// 0x00000164 System.Void Mirror.Experimental.NetworkRigidbody2D::MirrorProcessed()
extern void NetworkRigidbody2D_MirrorProcessed_m61E0EF6FECAE614F892CED615F973A2F8DEA0A7B (void);
// 0x00000165 UnityEngine.Vector2 Mirror.Experimental.NetworkRigidbody2D::get_Networkvelocity()
extern void NetworkRigidbody2D_get_Networkvelocity_mC74ABB0F2849867B9E6362E472815388DC92B955 (void);
// 0x00000166 System.Void Mirror.Experimental.NetworkRigidbody2D::set_Networkvelocity(UnityEngine.Vector2)
extern void NetworkRigidbody2D_set_Networkvelocity_mE4A457220B4AB1C6195BEF2DEDEAB49B68886140 (void);
// 0x00000167 System.Single Mirror.Experimental.NetworkRigidbody2D::get_NetworkangularVelocity()
extern void NetworkRigidbody2D_get_NetworkangularVelocity_mC64533A49CF2C988C0B51E3275DB75DE6991A24B (void);
// 0x00000168 System.Void Mirror.Experimental.NetworkRigidbody2D::set_NetworkangularVelocity(System.Single)
extern void NetworkRigidbody2D_set_NetworkangularVelocity_m12F485686C78CC3A81AEFA7F62C9B36468649924 (void);
// 0x00000169 System.Boolean Mirror.Experimental.NetworkRigidbody2D::get_NetworkisKinematic()
extern void NetworkRigidbody2D_get_NetworkisKinematic_m5024E96C5B0B7800F79A4B5C4361DFD0955328D8 (void);
// 0x0000016A System.Void Mirror.Experimental.NetworkRigidbody2D::set_NetworkisKinematic(System.Boolean)
extern void NetworkRigidbody2D_set_NetworkisKinematic_m72A8887DA1EE3320A20DFAD8A118909A13249A34 (void);
// 0x0000016B System.Single Mirror.Experimental.NetworkRigidbody2D::get_NetworkgravityScale()
extern void NetworkRigidbody2D_get_NetworkgravityScale_m9952509F42650C3ED6E4C0D2AA61CCECBD0FE1E0 (void);
// 0x0000016C System.Void Mirror.Experimental.NetworkRigidbody2D::set_NetworkgravityScale(System.Single)
extern void NetworkRigidbody2D_set_NetworkgravityScale_mA850FEBCF63736C555EAFC1903840665CE3098C0 (void);
// 0x0000016D System.Single Mirror.Experimental.NetworkRigidbody2D::get_Networkdrag()
extern void NetworkRigidbody2D_get_Networkdrag_mA5865030789C4B614CCCC42EFA90E5C1C78C8F27 (void);
// 0x0000016E System.Void Mirror.Experimental.NetworkRigidbody2D::set_Networkdrag(System.Single)
extern void NetworkRigidbody2D_set_Networkdrag_m1DEC71BF34C256D3FEB4518C28CA4DB11613E9CE (void);
// 0x0000016F System.Single Mirror.Experimental.NetworkRigidbody2D::get_NetworkangularDrag()
extern void NetworkRigidbody2D_get_NetworkangularDrag_m8FF720CF826BA10DC5C46FCBCB3A227AF3DA44F9 (void);
// 0x00000170 System.Void Mirror.Experimental.NetworkRigidbody2D::set_NetworkangularDrag(System.Single)
extern void NetworkRigidbody2D_set_NetworkangularDrag_m4921CAD2C48BD26A2AD32B763892A96E0422577C (void);
// 0x00000171 System.Void Mirror.Experimental.NetworkRigidbody2D::UserCode_CmdSendVelocity__Vector2(UnityEngine.Vector2)
extern void NetworkRigidbody2D_UserCode_CmdSendVelocity__Vector2_mEC55D2A8EE795820694422AF2FA191D30B3E3582 (void);
// 0x00000172 System.Void Mirror.Experimental.NetworkRigidbody2D::InvokeUserCode_CmdSendVelocity__Vector2(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody2D_InvokeUserCode_CmdSendVelocity__Vector2_m3E446857383A1D1B0A4B5B6802D98CE7B6778725 (void);
// 0x00000173 System.Void Mirror.Experimental.NetworkRigidbody2D::UserCode_CmdSendVelocityAndAngular__Vector2__Single(UnityEngine.Vector2,System.Single)
extern void NetworkRigidbody2D_UserCode_CmdSendVelocityAndAngular__Vector2__Single_m57DBF13383F15024B79E7B7F438983295C595345 (void);
// 0x00000174 System.Void Mirror.Experimental.NetworkRigidbody2D::InvokeUserCode_CmdSendVelocityAndAngular__Vector2__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody2D_InvokeUserCode_CmdSendVelocityAndAngular__Vector2__Single_m0B83ED5CD00F6CAB25AFEFFBAB17C0B65AC0A3C0 (void);
// 0x00000175 System.Void Mirror.Experimental.NetworkRigidbody2D::UserCode_CmdSendIsKinematic__Boolean(System.Boolean)
extern void NetworkRigidbody2D_UserCode_CmdSendIsKinematic__Boolean_m886EAC686E7616C1685B7A26C46E49F9B29B4280 (void);
// 0x00000176 System.Void Mirror.Experimental.NetworkRigidbody2D::InvokeUserCode_CmdSendIsKinematic__Boolean(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody2D_InvokeUserCode_CmdSendIsKinematic__Boolean_m03E56F5761C4103E4A5E1986AA90FB3C4A0A904D (void);
// 0x00000177 System.Void Mirror.Experimental.NetworkRigidbody2D::UserCode_CmdChangeGravityScale__Single(System.Single)
extern void NetworkRigidbody2D_UserCode_CmdChangeGravityScale__Single_mFEB489727311A915C0361D4E076FA7C611480336 (void);
// 0x00000178 System.Void Mirror.Experimental.NetworkRigidbody2D::InvokeUserCode_CmdChangeGravityScale__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody2D_InvokeUserCode_CmdChangeGravityScale__Single_mFD4FBDFCBC3A6E3D2E1B8989991976595EDE48AF (void);
// 0x00000179 System.Void Mirror.Experimental.NetworkRigidbody2D::UserCode_CmdSendDrag__Single(System.Single)
extern void NetworkRigidbody2D_UserCode_CmdSendDrag__Single_m9CECE61B613A57AB785DFD78BE849F34372A2823 (void);
// 0x0000017A System.Void Mirror.Experimental.NetworkRigidbody2D::InvokeUserCode_CmdSendDrag__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody2D_InvokeUserCode_CmdSendDrag__Single_mFC664CDA520B78818E6E7FADA9F7FD1B39C851A1 (void);
// 0x0000017B System.Void Mirror.Experimental.NetworkRigidbody2D::UserCode_CmdSendAngularDrag__Single(System.Single)
extern void NetworkRigidbody2D_UserCode_CmdSendAngularDrag__Single_m6E4BDCFEA554CE74A1D76C200468102A478630E3 (void);
// 0x0000017C System.Void Mirror.Experimental.NetworkRigidbody2D::InvokeUserCode_CmdSendAngularDrag__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody2D_InvokeUserCode_CmdSendAngularDrag__Single_m68C640B904C9392DA183519A9CDA28820575EEAE (void);
// 0x0000017D System.Void Mirror.Experimental.NetworkRigidbody2D::.cctor()
extern void NetworkRigidbody2D__cctor_mD69E41A075815AD406B563256F98ED6B185E489C (void);
// 0x0000017E System.Boolean Mirror.Experimental.NetworkRigidbody2D::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkRigidbody2D_SerializeSyncVars_mF0A1AA955871A431E156CCC8FB1DB762D93B74B1 (void);
// 0x0000017F System.Void Mirror.Experimental.NetworkRigidbody2D::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkRigidbody2D_DeserializeSyncVars_m2349FF5C5C14BA6959F9A9170A14BBA774E0A8E4 (void);
// 0x00000180 System.Void Mirror.Experimental.NetworkRigidbody2D/ClientSyncState::.ctor()
extern void ClientSyncState__ctor_mDE6071EDBDBC39E9B9244EF3294054219B67549C (void);
// 0x00000181 UnityEngine.Transform Mirror.Experimental.NetworkTransform::get_targetTransform()
extern void NetworkTransform_get_targetTransform_mCF1300F8F25C5542751DBCAF4DD4DC99170CF033 (void);
// 0x00000182 System.Void Mirror.Experimental.NetworkTransform::.ctor()
extern void NetworkTransform__ctor_mB2DBD23A7FD227B96DFC737102DB7FCA727FE096 (void);
// 0x00000183 System.Void Mirror.Experimental.NetworkTransform::MirrorProcessed()
extern void NetworkTransform_MirrorProcessed_mCAD95FC4269E0581E25AC8D3329E1B446DB9BBC6 (void);
// 0x00000184 UnityEngine.Transform Mirror.Experimental.NetworkTransformBase::get_targetTransform()
// 0x00000185 System.Boolean Mirror.Experimental.NetworkTransformBase::get_IsOwnerWithClientAuthority()
extern void NetworkTransformBase_get_IsOwnerWithClientAuthority_m9721ED1E3BC17C1F050C61321E2D40ACC1F57F76 (void);
// 0x00000186 System.Void Mirror.Experimental.NetworkTransformBase::FixedUpdate()
extern void NetworkTransformBase_FixedUpdate_m820E7E32C0E10DD2FF089967FEC4AC4A30B4B122 (void);
// 0x00000187 System.Void Mirror.Experimental.NetworkTransformBase::ServerUpdate()
extern void NetworkTransformBase_ServerUpdate_m069451E249D5FD53A5ECDBACC43FF1291F9B975E (void);
// 0x00000188 System.Void Mirror.Experimental.NetworkTransformBase::ClientAuthorityUpdate()
extern void NetworkTransformBase_ClientAuthorityUpdate_m64CE70B6F8EB8D2194236087761E3B63471475F7 (void);
// 0x00000189 System.Void Mirror.Experimental.NetworkTransformBase::ClientRemoteUpdate()
extern void NetworkTransformBase_ClientRemoteUpdate_m69D9AE30FB0E0B22FB2D30851F2F4B83B4F1C355 (void);
// 0x0000018A System.Boolean Mirror.Experimental.NetworkTransformBase::HasEitherMovedRotatedScaled()
extern void NetworkTransformBase_HasEitherMovedRotatedScaled_m8C45DA33762E51CAEA75368A98452BEC643C2D7D (void);
// 0x0000018B System.Boolean Mirror.Experimental.NetworkTransformBase::get_HasMoved()
extern void NetworkTransformBase_get_HasMoved_m213F6051B04D176E1BC9F5F474C72587E589595A (void);
// 0x0000018C System.Boolean Mirror.Experimental.NetworkTransformBase::get_HasRotated()
extern void NetworkTransformBase_get_HasRotated_mC41C1E6CA23EE731DD00DC568C1CFA7F032D659E (void);
// 0x0000018D System.Boolean Mirror.Experimental.NetworkTransformBase::get_HasScaled()
extern void NetworkTransformBase_get_HasScaled_m90AE6D5D682172233C5B0E713B05E14F6DD983AE (void);
// 0x0000018E System.Boolean Mirror.Experimental.NetworkTransformBase::NeedsTeleport()
extern void NetworkTransformBase_NeedsTeleport_mA94281756734F56CD48DAA2185322CC8478A12CD (void);
// 0x0000018F System.Void Mirror.Experimental.NetworkTransformBase::CmdClientToServerSync(UnityEngine.Vector3,System.UInt32,UnityEngine.Vector3)
extern void NetworkTransformBase_CmdClientToServerSync_m0BBA2C3C4758CA0869FC380FBFCF3C17CD93DEDC (void);
// 0x00000190 System.Void Mirror.Experimental.NetworkTransformBase::RpcMove(UnityEngine.Vector3,System.UInt32,UnityEngine.Vector3)
extern void NetworkTransformBase_RpcMove_mEC79579548C60F30381B296BFD6D1F0C583ADB60 (void);
// 0x00000191 System.Void Mirror.Experimental.NetworkTransformBase::SetGoal(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void NetworkTransformBase_SetGoal_m8D4D99265CAAE907A213DC0FAA334A46E2B5749A (void);
// 0x00000192 System.Single Mirror.Experimental.NetworkTransformBase::EstimateMovementSpeed(Mirror.Experimental.NetworkTransformBase/DataPoint,Mirror.Experimental.NetworkTransformBase/DataPoint,UnityEngine.Transform,System.Single)
extern void NetworkTransformBase_EstimateMovementSpeed_m956B628AA7775F341E941B2BAF38CA6DDA1F0A0A (void);
// 0x00000193 System.Void Mirror.Experimental.NetworkTransformBase::ApplyPositionRotationScale(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void NetworkTransformBase_ApplyPositionRotationScale_m9E3F3DA287D1BF95E10A6005F4A555E5AF759FEB (void);
// 0x00000194 UnityEngine.Vector3 Mirror.Experimental.NetworkTransformBase::InterpolatePosition(Mirror.Experimental.NetworkTransformBase/DataPoint,Mirror.Experimental.NetworkTransformBase/DataPoint,UnityEngine.Vector3)
extern void NetworkTransformBase_InterpolatePosition_m90B70F68A5D27B3429D20B9E903627658A7E524B (void);
// 0x00000195 UnityEngine.Quaternion Mirror.Experimental.NetworkTransformBase::InterpolateRotation(Mirror.Experimental.NetworkTransformBase/DataPoint,Mirror.Experimental.NetworkTransformBase/DataPoint,UnityEngine.Quaternion)
extern void NetworkTransformBase_InterpolateRotation_mED10AEC6B1C9E2006FBAAF1202994A1AD85E4853 (void);
// 0x00000196 UnityEngine.Vector3 Mirror.Experimental.NetworkTransformBase::InterpolateScale(Mirror.Experimental.NetworkTransformBase/DataPoint,Mirror.Experimental.NetworkTransformBase/DataPoint,UnityEngine.Vector3)
extern void NetworkTransformBase_InterpolateScale_mA2358259E572923F29774671EDAC4D24CD673E8E (void);
// 0x00000197 System.Single Mirror.Experimental.NetworkTransformBase::CurrentInterpolationFactor(Mirror.Experimental.NetworkTransformBase/DataPoint,Mirror.Experimental.NetworkTransformBase/DataPoint)
extern void NetworkTransformBase_CurrentInterpolationFactor_mCFEAB6E6A682DBB4DC357BAE66AAF58E67E8B333 (void);
// 0x00000198 System.Void Mirror.Experimental.NetworkTransformBase::ServerTeleport(UnityEngine.Vector3)
extern void NetworkTransformBase_ServerTeleport_m76794E84A5C4AE260E8F5F8F9BB0184E3F82FF7C (void);
// 0x00000199 System.Void Mirror.Experimental.NetworkTransformBase::ServerTeleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_ServerTeleport_m759BDF1BFE5D9208D2827A36725384B5949A0EA6 (void);
// 0x0000019A System.Void Mirror.Experimental.NetworkTransformBase::DoTeleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_DoTeleport_mF56F6AE07B5EE385E32D271AC22AA411A7EE8BAA (void);
// 0x0000019B System.Void Mirror.Experimental.NetworkTransformBase::RpcTeleport(UnityEngine.Vector3,System.UInt32,System.Boolean)
extern void NetworkTransformBase_RpcTeleport_m221E2786D89AA7375316A3680F0F77F775289AE5 (void);
// 0x0000019C System.Void Mirror.Experimental.NetworkTransformBase::CmdTeleportFinished()
extern void NetworkTransformBase_CmdTeleportFinished_mBBAC7ED1FF0E1DDFBCBCB968749B5F6A8A1866CE (void);
// 0x0000019D System.Void Mirror.Experimental.NetworkTransformBase::OnDrawGizmos()
extern void NetworkTransformBase_OnDrawGizmos_m8EDF0DCF900C88C886FC50A9ECE7C7F485F184A2 (void);
// 0x0000019E System.Void Mirror.Experimental.NetworkTransformBase::DrawDataPointGizmo(Mirror.Experimental.NetworkTransformBase/DataPoint,UnityEngine.Color)
extern void NetworkTransformBase_DrawDataPointGizmo_mB286B27082D0D29C123308E310694E1C6744A317 (void);
// 0x0000019F System.Void Mirror.Experimental.NetworkTransformBase::DrawLineBetweenDataPoints(Mirror.Experimental.NetworkTransformBase/DataPoint,Mirror.Experimental.NetworkTransformBase/DataPoint,UnityEngine.Color)
extern void NetworkTransformBase_DrawLineBetweenDataPoints_mC7663D2FA02D48D365E30EFAAC86152237607E53 (void);
// 0x000001A0 System.Void Mirror.Experimental.NetworkTransformBase::.ctor()
extern void NetworkTransformBase__ctor_m65620375EB0F2525064390026555E29217B15B34 (void);
// 0x000001A1 System.Void Mirror.Experimental.NetworkTransformBase::MirrorProcessed()
extern void NetworkTransformBase_MirrorProcessed_mD9BD1A601136D645A37A567AA6BF35C245D4FB3F (void);
// 0x000001A2 System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworkclientAuthority()
extern void NetworkTransformBase_get_NetworkclientAuthority_mCC851CD9636198F344E5053199E1B5B9C3D2E335 (void);
// 0x000001A3 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworkclientAuthority(System.Boolean)
extern void NetworkTransformBase_set_NetworkclientAuthority_m05304900053A008C8D858B076CD5B1FC60AFAD40 (void);
// 0x000001A4 System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworkexcludeOwnerUpdate()
extern void NetworkTransformBase_get_NetworkexcludeOwnerUpdate_mA9A2B57E82D9ACC3922347CA46AD5D94643A87BB (void);
// 0x000001A5 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworkexcludeOwnerUpdate(System.Boolean)
extern void NetworkTransformBase_set_NetworkexcludeOwnerUpdate_m3875A9E7C11970F952A03E5F671983BC24233678 (void);
// 0x000001A6 System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworksyncPosition()
extern void NetworkTransformBase_get_NetworksyncPosition_mBE4C25CFB4412E5FEBDFAD2AD1B49194DB7DCDB3 (void);
// 0x000001A7 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworksyncPosition(System.Boolean)
extern void NetworkTransformBase_set_NetworksyncPosition_mFA708031F5FF4D89FBE9580EF85A90FBBD33058D (void);
// 0x000001A8 System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworksyncRotation()
extern void NetworkTransformBase_get_NetworksyncRotation_m7BDAD8F697638B9B4A03360C7EC9869E75F4ADC0 (void);
// 0x000001A9 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworksyncRotation(System.Boolean)
extern void NetworkTransformBase_set_NetworksyncRotation_mA9E6ACC3519C4AC2BA5711EAB870468E02EA096C (void);
// 0x000001AA System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworksyncScale()
extern void NetworkTransformBase_get_NetworksyncScale_m6220D5DB18E1CC9B54FEB9F337F5F2842CB329B7 (void);
// 0x000001AB System.Void Mirror.Experimental.NetworkTransformBase::set_NetworksyncScale(System.Boolean)
extern void NetworkTransformBase_set_NetworksyncScale_mFAAB7AF70F0EFC1F1885E3BDEDBC7CDE700AB52C (void);
// 0x000001AC System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworkinterpolatePosition()
extern void NetworkTransformBase_get_NetworkinterpolatePosition_mBE161844A81B74706391C290FD9A8C1722D2B08A (void);
// 0x000001AD System.Void Mirror.Experimental.NetworkTransformBase::set_NetworkinterpolatePosition(System.Boolean)
extern void NetworkTransformBase_set_NetworkinterpolatePosition_m9D512FA43FDFCBB6868A6D8D889D4BB748C76CB1 (void);
// 0x000001AE System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworkinterpolateRotation()
extern void NetworkTransformBase_get_NetworkinterpolateRotation_mD8F4DA7C1180480CE6664587292C04736E38B368 (void);
// 0x000001AF System.Void Mirror.Experimental.NetworkTransformBase::set_NetworkinterpolateRotation(System.Boolean)
extern void NetworkTransformBase_set_NetworkinterpolateRotation_mE63F265FDD48BD39D9AC4DB49A7496AD1F277807 (void);
// 0x000001B0 System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworkinterpolateScale()
extern void NetworkTransformBase_get_NetworkinterpolateScale_mF0C5D7BFC4CD0DF1C7C40C8E26B56EF94883E94F (void);
// 0x000001B1 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworkinterpolateScale(System.Boolean)
extern void NetworkTransformBase_set_NetworkinterpolateScale_mF649A173B8D2A2D07C5D077BFD7CA0C106BFEA58 (void);
// 0x000001B2 System.Single Mirror.Experimental.NetworkTransformBase::get_NetworklocalPositionSensitivity()
extern void NetworkTransformBase_get_NetworklocalPositionSensitivity_m4170C73A674F2B08C99CB9DFA2BD2AF45FFA38D8 (void);
// 0x000001B3 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworklocalPositionSensitivity(System.Single)
extern void NetworkTransformBase_set_NetworklocalPositionSensitivity_m2E52EB05AFFA33B013B2A1D9B876B4676294DCA7 (void);
// 0x000001B4 System.Single Mirror.Experimental.NetworkTransformBase::get_NetworklocalRotationSensitivity()
extern void NetworkTransformBase_get_NetworklocalRotationSensitivity_mA356ADCE7DB71CDE2DAD445E2479A76C1213DA66 (void);
// 0x000001B5 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworklocalRotationSensitivity(System.Single)
extern void NetworkTransformBase_set_NetworklocalRotationSensitivity_mEF44DFC9FC3BFEACC9038AB0E1F04C6B9473F60F (void);
// 0x000001B6 System.Single Mirror.Experimental.NetworkTransformBase::get_NetworklocalScaleSensitivity()
extern void NetworkTransformBase_get_NetworklocalScaleSensitivity_m259A702CB6E3DAE97697E10D6D5D7A17CD967AB0 (void);
// 0x000001B7 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworklocalScaleSensitivity(System.Single)
extern void NetworkTransformBase_set_NetworklocalScaleSensitivity_m5AD8E522B6FEA85C542D9B20E6B886A5E3B87EA2 (void);
// 0x000001B8 System.Void Mirror.Experimental.NetworkTransformBase::UserCode_CmdClientToServerSync__Vector3__UInt32__Vector3(UnityEngine.Vector3,System.UInt32,UnityEngine.Vector3)
extern void NetworkTransformBase_UserCode_CmdClientToServerSync__Vector3__UInt32__Vector3_m1F0B88E57AB9F761124EA80E6FB52902E1A607C2 (void);
// 0x000001B9 System.Void Mirror.Experimental.NetworkTransformBase::InvokeUserCode_CmdClientToServerSync__Vector3__UInt32__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_CmdClientToServerSync__Vector3__UInt32__Vector3_mB953618B6A1826E4587A4B7DED6FEA5718A7BDB3 (void);
// 0x000001BA System.Void Mirror.Experimental.NetworkTransformBase::UserCode_RpcMove__Vector3__UInt32__Vector3(UnityEngine.Vector3,System.UInt32,UnityEngine.Vector3)
extern void NetworkTransformBase_UserCode_RpcMove__Vector3__UInt32__Vector3_m3C5C91D6C4A5B719784372ADC5C8D84CDC5D1BDE (void);
// 0x000001BB System.Void Mirror.Experimental.NetworkTransformBase::InvokeUserCode_RpcMove__Vector3__UInt32__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_RpcMove__Vector3__UInt32__Vector3_mC60C3534DB40A7A54DDF5186D304E2A7B27920A7 (void);
// 0x000001BC System.Void Mirror.Experimental.NetworkTransformBase::UserCode_RpcTeleport__Vector3__UInt32__Boolean(UnityEngine.Vector3,System.UInt32,System.Boolean)
extern void NetworkTransformBase_UserCode_RpcTeleport__Vector3__UInt32__Boolean_m0E4572551184F488D9F1B0B7F79751D4F621E9FE (void);
// 0x000001BD System.Void Mirror.Experimental.NetworkTransformBase::InvokeUserCode_RpcTeleport__Vector3__UInt32__Boolean(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_RpcTeleport__Vector3__UInt32__Boolean_mEE82A2521701CB5F74247CC6833CA59063ADA2D4 (void);
// 0x000001BE System.Void Mirror.Experimental.NetworkTransformBase::UserCode_CmdTeleportFinished()
extern void NetworkTransformBase_UserCode_CmdTeleportFinished_m4FB55893D9E380F337DC436DAB6009B63C0530BF (void);
// 0x000001BF System.Void Mirror.Experimental.NetworkTransformBase::InvokeUserCode_CmdTeleportFinished(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_CmdTeleportFinished_m23D03D021DC54EE303EFCFCB590EA48E0162A8DD (void);
// 0x000001C0 System.Void Mirror.Experimental.NetworkTransformBase::.cctor()
extern void NetworkTransformBase__cctor_mFCF26DBAF21508A966C1D422DA33B611D9C8FAC4 (void);
// 0x000001C1 System.Boolean Mirror.Experimental.NetworkTransformBase::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkTransformBase_SerializeSyncVars_m80B56E89AEEEC2291412E4D154B001122304126A (void);
// 0x000001C2 System.Void Mirror.Experimental.NetworkTransformBase::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkTransformBase_DeserializeSyncVars_m617E2DEB847D9A263254FC4FC41DB79C77E49C33 (void);
// 0x000001C3 System.Boolean Mirror.Experimental.NetworkTransformBase/DataPoint::get_isValid()
extern void DataPoint_get_isValid_m89E5E486FBD3EF2F8094C64B18D4E52F0AABC25C (void);
// 0x000001C4 UnityEngine.Transform Mirror.Experimental.NetworkTransformChild::get_targetTransform()
extern void NetworkTransformChild_get_targetTransform_mC9E43B03EB9F9F5BE79A75DE29D9743BC707228D (void);
// 0x000001C5 System.Void Mirror.Experimental.NetworkTransformChild::.ctor()
extern void NetworkTransformChild__ctor_mB51C8BF137FEA223AECCFCC03F5D32506178B4F0 (void);
// 0x000001C6 System.Void Mirror.Experimental.NetworkTransformChild::MirrorProcessed()
extern void NetworkTransformChild_MirrorProcessed_m58BC2D4978E5DF300B7F4F29FA41B6DAC5D1FE5F (void);
// 0x000001C7 System.Void Mirror.Discovery.ServerFoundUnityEvent::.ctor()
extern void ServerFoundUnityEvent__ctor_m94727746B8368497DCBF89704D475C29027B6FDA (void);
// 0x000001C8 System.Int64 Mirror.Discovery.NetworkDiscovery::get_ServerId()
extern void NetworkDiscovery_get_ServerId_m7CACB9535680796E7E84F7269FA086071D4A31BA (void);
// 0x000001C9 System.Void Mirror.Discovery.NetworkDiscovery::set_ServerId(System.Int64)
extern void NetworkDiscovery_set_ServerId_mBD7B0DEAE754513530C99244F76F3CD58CAF4E9F (void);
// 0x000001CA System.Void Mirror.Discovery.NetworkDiscovery::Start()
extern void NetworkDiscovery_Start_m00F61409BF0EECE9E4866742B47CF12DB83CC8DB (void);
// 0x000001CB Mirror.Discovery.ServerResponse Mirror.Discovery.NetworkDiscovery::ProcessRequest(Mirror.Discovery.ServerRequest,System.Net.IPEndPoint)
extern void NetworkDiscovery_ProcessRequest_m0A23AA045FFE7B73B3DCF611E56463FA038F7572 (void);
// 0x000001CC Mirror.Discovery.ServerRequest Mirror.Discovery.NetworkDiscovery::GetRequest()
extern void NetworkDiscovery_GetRequest_m766AC1A5F4FC24955B640EBF11F64C0B03321999 (void);
// 0x000001CD System.Void Mirror.Discovery.NetworkDiscovery::ProcessResponse(Mirror.Discovery.ServerResponse,System.Net.IPEndPoint)
extern void NetworkDiscovery_ProcessResponse_m8E66484218C817247C2815D6E9AA0152586D79C1 (void);
// 0x000001CE System.Void Mirror.Discovery.NetworkDiscovery::.ctor()
extern void NetworkDiscovery__ctor_m38F5C2816F38111FDCD8ECE3705F392299CC1292 (void);
// 0x000001CF System.Boolean Mirror.Discovery.NetworkDiscoveryBase`2::get_SupportedOnThisPlatform()
// 0x000001D0 System.Int64 Mirror.Discovery.NetworkDiscoveryBase`2::RandomLong()
// 0x000001D1 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::Start()
// 0x000001D2 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::OnApplicationQuit()
// 0x000001D3 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::OnDisable()
// 0x000001D4 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::OnDestroy()
// 0x000001D5 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::Shutdown()
// 0x000001D6 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::AdvertiseServer()
// 0x000001D7 System.Threading.Tasks.Task Mirror.Discovery.NetworkDiscoveryBase`2::ServerListenAsync()
// 0x000001D8 System.Threading.Tasks.Task Mirror.Discovery.NetworkDiscoveryBase`2::ReceiveRequestAsync(System.Net.Sockets.UdpClient)
// 0x000001D9 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::ProcessClientRequest(Request,System.Net.IPEndPoint)
// 0x000001DA Response Mirror.Discovery.NetworkDiscoveryBase`2::ProcessRequest(Request,System.Net.IPEndPoint)
// 0x000001DB System.Void Mirror.Discovery.NetworkDiscoveryBase`2::BeginMulticastLock()
// 0x000001DC System.Void Mirror.Discovery.NetworkDiscoveryBase`2::EndpMulticastLock()
// 0x000001DD System.Void Mirror.Discovery.NetworkDiscoveryBase`2::StartDiscovery()
// 0x000001DE System.Void Mirror.Discovery.NetworkDiscoveryBase`2::StopDiscovery()
// 0x000001DF System.Threading.Tasks.Task Mirror.Discovery.NetworkDiscoveryBase`2::ClientListenAsync()
// 0x000001E0 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::BroadcastDiscoveryRequest()
// 0x000001E1 Request Mirror.Discovery.NetworkDiscoveryBase`2::GetRequest()
// 0x000001E2 System.Threading.Tasks.Task Mirror.Discovery.NetworkDiscoveryBase`2::ReceiveGameBroadcastAsync(System.Net.Sockets.UdpClient)
// 0x000001E3 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::ProcessResponse(Response,System.Net.IPEndPoint)
// 0x000001E4 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::.ctor()
// 0x000001E5 System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ServerListenAsync>d__15::MoveNext()
// 0x000001E6 System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ServerListenAsync>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000001E7 System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ReceiveRequestAsync>d__16::MoveNext()
// 0x000001E8 System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ReceiveRequestAsync>d__16::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000001E9 System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ClientListenAsync>d__23::MoveNext()
// 0x000001EA System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ClientListenAsync>d__23::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000001EB System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ReceiveGameBroadcastAsync>d__26::MoveNext()
// 0x000001EC System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ReceiveGameBroadcastAsync>d__26::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000001ED System.Void Mirror.Discovery.NetworkDiscoveryHUD::OnGUI()
extern void NetworkDiscoveryHUD_OnGUI_m64F2C1FDE4446BCA975D7466504E5AFBC2338FFD (void);
// 0x000001EE System.Void Mirror.Discovery.NetworkDiscoveryHUD::DrawGUI()
extern void NetworkDiscoveryHUD_DrawGUI_m8F607644FFD39F9504295E06B0573515CDC61835 (void);
// 0x000001EF System.Void Mirror.Discovery.NetworkDiscoveryHUD::StopButtons()
extern void NetworkDiscoveryHUD_StopButtons_m974CC76A233D560105C90A2C11970CA01CB3AF42 (void);
// 0x000001F0 System.Void Mirror.Discovery.NetworkDiscoveryHUD::Connect(Mirror.Discovery.ServerResponse)
extern void NetworkDiscoveryHUD_Connect_m83903FD713C4576A22E0290FE4E50AAE265C8357 (void);
// 0x000001F1 System.Void Mirror.Discovery.NetworkDiscoveryHUD::OnDiscoveredServer(Mirror.Discovery.ServerResponse)
extern void NetworkDiscoveryHUD_OnDiscoveredServer_m2AF940684E8C0798798D904B8403C80748EC6BFA (void);
// 0x000001F2 System.Void Mirror.Discovery.NetworkDiscoveryHUD::.ctor()
extern void NetworkDiscoveryHUD__ctor_mB8B12A6BADDEC862DA48B514EE141C1D070C437C (void);
// 0x000001F3 System.Net.IPEndPoint Mirror.Discovery.ServerResponse::get_EndPoint()
extern void ServerResponse_get_EndPoint_mBFDE2455B6E2DC58E357C27D23D58D76694CB35D (void);
// 0x000001F4 System.Void Mirror.Discovery.ServerResponse::set_EndPoint(System.Net.IPEndPoint)
extern void ServerResponse_set_EndPoint_m12FC283447C2EB408A52D7EA1510173763CB8463 (void);
// 0x000001F5 Mirror.ReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ReadyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ReadyMessage_m06480E84CC3B19B04E6573A08D1CEDC68E2B2C50 (void);
// 0x000001F6 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ReadyMessage(Mirror.NetworkWriter,Mirror.ReadyMessage)
extern void GeneratedNetworkCode__Write_Mirror_ReadyMessage_m1ABF2F5BB5DF55AF44620112E5FB6AC4410D7DCC (void);
// 0x000001F7 Mirror.NotReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NotReadyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m561BF5BCE9FC86C6F37D8699FC75EB83E769C239 (void);
// 0x000001F8 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NotReadyMessage(Mirror.NetworkWriter,Mirror.NotReadyMessage)
extern void GeneratedNetworkCode__Write_Mirror_NotReadyMessage_m2C3071681A4D7171B21B58B252EA553E85C57CDB (void);
// 0x000001F9 Mirror.AddPlayerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.AddPlayerMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_mF53BB66F92BDD045E279B3395ADB22E544164742 (void);
// 0x000001FA System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.AddPlayerMessage(Mirror.NetworkWriter,Mirror.AddPlayerMessage)
extern void GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_m252F2DFAAC1A40BB906513ED762CC227601BF6F5 (void);
// 0x000001FB Mirror.SceneMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SceneMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SceneMessage_m8842C2807940A70FAC96357C2DA921D0A881E6F8 (void);
// 0x000001FC Mirror.SceneOperation Mirror.GeneratedNetworkCode::_Read_Mirror.SceneOperation(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SceneOperation_m6EF9BACF8BC61D214F0B7687DB375556D58BD0B5 (void);
// 0x000001FD System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneMessage(Mirror.NetworkWriter,Mirror.SceneMessage)
extern void GeneratedNetworkCode__Write_Mirror_SceneMessage_m661773F482F43CD62A9A787FCFD4768AB3ECD59B (void);
// 0x000001FE System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneOperation(Mirror.NetworkWriter,Mirror.SceneOperation)
extern void GeneratedNetworkCode__Write_Mirror_SceneOperation_mBF55AECA974A3FD1ACE96CBEA7513205A820A78B (void);
// 0x000001FF Mirror.CommandMessage Mirror.GeneratedNetworkCode::_Read_Mirror.CommandMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_CommandMessage_m2506B9CDD17789E721D0CCC2F6DB1A2428A1C3B9 (void);
// 0x00000200 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.CommandMessage(Mirror.NetworkWriter,Mirror.CommandMessage)
extern void GeneratedNetworkCode__Write_Mirror_CommandMessage_m9751EAE9DBDC058EE20ACD3CDFFCE8AA86A52EEB (void);
// 0x00000201 Mirror.RpcMessage Mirror.GeneratedNetworkCode::_Read_Mirror.RpcMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_RpcMessage_mF12E2594E03EBD1A674D892F6ED4974A93D42F2E (void);
// 0x00000202 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.RpcMessage(Mirror.NetworkWriter,Mirror.RpcMessage)
extern void GeneratedNetworkCode__Write_Mirror_RpcMessage_mB7AD2C90A88C6FB1625352488BF3A74AEE60D901 (void);
// 0x00000203 Mirror.SpawnMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SpawnMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SpawnMessage_m4D488884D64FCFEE1EC076E9CF8BBA442FDF8014 (void);
// 0x00000204 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SpawnMessage(Mirror.NetworkWriter,Mirror.SpawnMessage)
extern void GeneratedNetworkCode__Write_Mirror_SpawnMessage_m994E303D1012261A96D8459337314BAC322E27C7 (void);
// 0x00000205 Mirror.ChangeOwnerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ChangeOwnerMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_mB76DF471DFB06E15D34400220CC8D42A8AF5FF25 (void);
// 0x00000206 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ChangeOwnerMessage(Mirror.NetworkWriter,Mirror.ChangeOwnerMessage)
extern void GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m2D15284E263AADFB70FA3A5A5BD409A7FB759DCF (void);
// 0x00000207 Mirror.ObjectSpawnStartedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_m112EE6BB7CA29C9AA118942B15784B907D2368FE (void);
// 0x00000208 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnStartedMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m49A85D44721088FCF11FADA6B439917931BEA0F9 (void);
// 0x00000209 Mirror.ObjectSpawnFinishedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_mCEC6B1996601932F124D9CBB8ADD4682EB5FF99A (void);
// 0x0000020A System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnFinishedMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_mC120FD8DF8C4E9D1E054D60F5BA203C1222F2255 (void);
// 0x0000020B Mirror.ObjectDestroyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectDestroyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_mCB0FBE61E22F8AC1880E1CDFD12B0BEF29537D5F (void);
// 0x0000020C System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectDestroyMessage(Mirror.NetworkWriter,Mirror.ObjectDestroyMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_mFEED939D871503CBCFFE9A27839E8F0E6F23FC1F (void);
// 0x0000020D Mirror.ObjectHideMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectHideMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_mE7900C1AEBECD02903094AD790504D9AD7AAEB93 (void);
// 0x0000020E System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectHideMessage(Mirror.NetworkWriter,Mirror.ObjectHideMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_m4420F0EFA51E36569D0CCE98CBD6D4DF0CE0A60D (void);
// 0x0000020F Mirror.EntityStateMessage Mirror.GeneratedNetworkCode::_Read_Mirror.EntityStateMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_EntityStateMessage_m01CECE9E01DDE00D8F54A9F911E79D50526F5219 (void);
// 0x00000210 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.EntityStateMessage(Mirror.NetworkWriter,Mirror.EntityStateMessage)
extern void GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m328E999051FAE859E8612DAFC86A845071ABB5C6 (void);
// 0x00000211 Mirror.NetworkPingMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPingMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m1D1484F1E38A57BA020BD6F677E260EC8E0F4DB7 (void);
// 0x00000212 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPingMessage(Mirror.NetworkWriter,Mirror.NetworkPingMessage)
extern void GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_m06ADA370CB0B3921F240FFA14F67ED8559B550F8 (void);
// 0x00000213 Mirror.NetworkPongMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPongMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_m49244A6CDEE5A502776FB107388190F5AD11A4A4 (void);
// 0x00000214 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPongMessage(Mirror.NetworkWriter,Mirror.NetworkPongMessage)
extern void GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_m7B710FB7E82A39893692BFF8CB962A6BFC7A93D2 (void);
// 0x00000215 Mirror.Discovery.ServerRequest Mirror.GeneratedNetworkCode::_Read_Mirror.Discovery.ServerRequest(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_Discovery_ServerRequest_mB9AE535D408EC75C5111E6965EF6435A5F6FAF67 (void);
// 0x00000216 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Discovery.ServerRequest(Mirror.NetworkWriter,Mirror.Discovery.ServerRequest)
extern void GeneratedNetworkCode__Write_Mirror_Discovery_ServerRequest_mAEE89796A010BB37C20E8EE17EB56B75225500FD (void);
// 0x00000217 Mirror.Discovery.ServerResponse Mirror.GeneratedNetworkCode::_Read_Mirror.Discovery.ServerResponse(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_Discovery_ServerResponse_mA45BA115B8C1DE768E9EA1AA575F40E1011EFF0F (void);
// 0x00000218 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Discovery.ServerResponse(Mirror.NetworkWriter,Mirror.Discovery.ServerResponse)
extern void GeneratedNetworkCode__Write_Mirror_Discovery_ServerResponse_m81E05EF9031925687BA7F6118029B50B26B9E793 (void);
// 0x00000219 System.Void Mirror.GeneratedNetworkCode::InitReadWriters()
extern void GeneratedNetworkCode_InitReadWriters_m069419D2444914F86D93F89136A93BBB6CD55191 (void);
static Il2CppMethodPointer s_methodPointers[537] = 
{
	LogEntry__ctor_m91CDC82DC8D2111C6413CFED125B90BA0621AED7,
	GUIConsole_Awake_mCDE61F6A9F913E11470F8848A73EB7434EE4B166,
	GUIConsole_OnLog_m830DC47EA84B8F06CD4029864E8205D716230EE5,
	GUIConsole_Update_m990CE474C3028D74160EA60388083B00A64D9CB8,
	GUIConsole_OnGUI_m5F30A615992B44A24C2E3B095D5E238839625466,
	GUIConsole__ctor_mD1BEF3B54C70685D8F98302879D2FFD86BF5C64E,
	DistanceInterestManagement_GetVisRange_m12E05D584708ACDCEE55D0EFAC6F84324396FEFA,
	DistanceInterestManagement_Reset_m92C7ADF0A54C5E47565DD00257C9E2EEBC5F847E,
	DistanceInterestManagement_OnCheckObserver_mE778FFC6C980A6B6ACD9EDD3B2C1B59C64596490,
	DistanceInterestManagement_OnRebuildObservers_mD078341BFA331385403EB4117990CA71CD96C821,
	DistanceInterestManagement_Update_m873AC936A693A7CD2484283D78A0C36CA58E1DDB,
	DistanceInterestManagement__ctor_mB7E21EB4351E42F10092CD23AC0EE6FD6C274993,
	DistanceInterestManagementCustomRange__ctor_m7F8FFAC977C6269FB3BB3E04C2BFB7E88461FB43,
	DistanceInterestManagementCustomRange_MirrorProcessed_m99AA5AC739FEC1CC9703DC9D2D635D3B6965303F,
	MatchInterestManagement_OnSpawned_m35C954BAF490E9F68B3837D00B0B9DC6EB7B85ED,
	MatchInterestManagement_OnDestroyed_m50546ECB371F098BF04A3D6D0EE5A3B63538B701,
	MatchInterestManagement_Update_m4525F713A4D84BE9A1FB86112F172D4222618C0B,
	MatchInterestManagement_UpdateDirtyMatches_mB59ABD2A1E6EAE4CCA07022A538A47A090908137,
	MatchInterestManagement_UpdateMatchObjects_m41FEB7113E66F2DB6DFF8B31B97E9B8864FFE111,
	MatchInterestManagement_RebuildMatchObservers_mD70F277258F937446E363C6B58BB1317BE379902,
	MatchInterestManagement_OnCheckObserver_m52D72C98383DE761B003F5010F3F9AE445B385BC,
	MatchInterestManagement_OnRebuildObservers_mFEB9F4610BEEB3DB89E033736AE8F2E473494D06,
	MatchInterestManagement__ctor_m53C8035973784DB9B82F1134FB055DF02877F990,
	NetworkMatch__ctor_m3BE0761DDCF97655C11D2DB8ADAEA21C6BB236E4,
	NetworkMatch_MirrorProcessed_m20066E6C5F87D82FD03EB126DA66325F18E37045,
	SceneInterestManagement_OnSpawned_mA65F971568AF6C53AA875D7CE9831D6EE3827739,
	SceneInterestManagement_OnDestroyed_mB555665F50B9C653E7DAEEDCB1781253070E3C04,
	SceneInterestManagement_Update_mA48FE8022573A919B5C50C73A7CF2175D567B91F,
	SceneInterestManagement_RebuildSceneObservers_mD17A30B2C27977D00BF4F0B0DE12F06CCA8F0A4C,
	SceneInterestManagement_OnCheckObserver_m0982491A1BE8398EEFB6A6329775DE768FD195C5,
	SceneInterestManagement_OnRebuildObservers_m8B7ECAAAF54AE76F5CE10E99E4158E2B2843269C,
	SceneInterestManagement__ctor_mB6B5132982798C1D2FBAE9122BE56A057C101E1D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SpatialHashingInterestManagement_get_resolution_m2B1130C366434CBFDE569B28B4A365E308BF27A3,
	SpatialHashingInterestManagement_ProjectToGrid_m349C290374CFF7FB99D9ECB3A13BB90FE777BC50,
	SpatialHashingInterestManagement_OnCheckObserver_mA4C2B8D122C26BA6C0F5F22FF6E2D0999C09DDE3,
	SpatialHashingInterestManagement_OnRebuildObservers_m3D1B09BDE0EF5484D3CE5CAA5CFCE5AB445E9A3D,
	SpatialHashingInterestManagement_Reset_m19589E358B323C2044C653841D6A5A83D58E413C,
	SpatialHashingInterestManagement_Update_m8D8EEF04CE24356DC794EFD0634AACFDD5AB6604,
	SpatialHashingInterestManagement__ctor_mA5E9B2FE69CD2F872BB7B297A8FE49E6061E1505,
	NetworkTeam__ctor_mC5292FAAA69C6B4B38122848B79AABB01EF22A88,
	NetworkTeam_MirrorProcessed_mBF8D8DD041B115C600D637E262C473BDC58F2859,
	TeamInterestManagement_OnSpawned_mE421063E3E1357E624B0CF3CFF82012DE4044E3B,
	TeamInterestManagement_OnDestroyed_mF7CDF71416FA303814EDBDB0ACB9E30B50CA3836,
	TeamInterestManagement_Update_mB87A6F8A33201C0C13EDC1292337C94B6CDBADE9,
	TeamInterestManagement_UpdateDirtyTeams_mFCA21AFFCC2DC9FC85672E0C4621B0CCC27D2246,
	TeamInterestManagement_UpdateTeamObjects_mF34863A90539AB04F4B692FF760A710FF1EDE6C7,
	TeamInterestManagement_RebuildTeamObservers_m67F50ECF8B74A2E4A4999C8785D8EA57100ED5D9,
	TeamInterestManagement_OnCheckObserver_mD597BD7F346C3F6CF64B6517CA5174AE62125390,
	TeamInterestManagement_OnRebuildObservers_m7BEEDB65A58B202995D2D121B740736795C51877,
	TeamInterestManagement_AddAllConnections_mB84A6B8F7805117012C031084AD0AA37D3CF1E39,
	TeamInterestManagement__ctor_mC9CCD5958EA9B1E3A8EB48888DFC98CC0F341914,
	NetworkAnimator_get_SendMessagesAllowed_m4372D7CCF3AA75F13998A2BBC83987DE53055163,
	NetworkAnimator_Awake_mF3E9622BB09EA5CCCDBCC0B3670C224014048C4F,
	NetworkAnimator_FixedUpdate_m5B89F06E14F9A209D4D9EE2B35159ADD1A6C985C,
	NetworkAnimator_CheckSpeed_mDAF46D0BC7333AA37E1D73FA77AF42C45BA775D3,
	NetworkAnimator_OnAnimatorSpeedChanged_m09D12F59674E4FE256CC4B68DE34EFBBC0D8584E,
	NetworkAnimator_CheckAnimStateChanged_m91D08C2DF3FC523A15A5911AF0C7F61496CB77BE,
	NetworkAnimator_CheckSendRate_m328485F3DBCB9B30D2C0582B1FC34C67E2E3B951,
	NetworkAnimator_SendAnimationMessage_m01B731EC39BB0FC0923DCF09EAE53B3E61BA4491,
	NetworkAnimator_SendAnimationParametersMessage_m27AAEDA4FD58B7E7988DB7914C6F8D382CC13803,
	NetworkAnimator_HandleAnimMsg_m608381B6C84879ED535969A1D2A82FA0B86FC242,
	NetworkAnimator_HandleAnimParamsMsg_m685AAECD2BDDD7092858BA556F17EF53CBC9E605,
	NetworkAnimator_HandleAnimTriggerMsg_m836B7829C8547DE10C95E265A645C978FC5E9452,
	NetworkAnimator_HandleAnimResetTriggerMsg_m9EC721D30C5D7BFE86E4EE8B97D628AB617C0E44,
	NetworkAnimator_NextDirtyBits_mBD81C8CFF2DC273823CBB03DF7228476B1B2532A,
	NetworkAnimator_WriteParameters_mFA85AF1193AF8DB50CD8CD2749F558871C5FEC2A,
	NetworkAnimator_ReadParameters_mE11AAB472B73148B0988D33155647E184232D7D8,
	NetworkAnimator_OnSerialize_m90E93BB04577BA90D0410B76B18DC11B581A027B,
	NetworkAnimator_OnDeserialize_mD5D93F2E6A45DB4DAA730C0EFC183F5B251135EC,
	NetworkAnimator_SetTrigger_mBFBFAB474DE4621AFA3921487963D5E5E3E1D181,
	NetworkAnimator_SetTrigger_mB7955C55BFD52E0561D3F10C602E029A216206C1,
	NetworkAnimator_ResetTrigger_m2E261D70481794B0133BB4E7DCA19747877A8A56,
	NetworkAnimator_ResetTrigger_m12C3D776FB823150A181EFADA7C9FC5A5D072621,
	NetworkAnimator_CmdOnAnimationServerMessage_m5A73D5EE0159B7F277146ABAA92ECD1D754A4533,
	NetworkAnimator_CmdOnAnimationParametersServerMessage_m517FF6B96255C1BB78A72061867489C3EAD70026,
	NetworkAnimator_CmdOnAnimationTriggerServerMessage_m6FFC2677C24B4218C20354267CEBFD8BCB67D447,
	NetworkAnimator_CmdOnAnimationResetTriggerServerMessage_m713A00270ED506782ED07E24F59A2B4A991376CC,
	NetworkAnimator_CmdSetAnimatorSpeed_m486BD8252A288D51146F1040E900220455A945C4,
	NetworkAnimator_RpcOnAnimationClientMessage_m8F366B4DC2E3EB03F3507009DFB17BFCECCF5951,
	NetworkAnimator_RpcOnAnimationParametersClientMessage_m415096E6363531A289FE824371C215C12F336C7D,
	NetworkAnimator_RpcOnAnimationTriggerClientMessage_mD314D48D8759DBCE7C0E766586DA883438495584,
	NetworkAnimator_RpcOnAnimationResetTriggerClientMessage_m968AAAA769EA88FD373BEF99B6A7596AF093D238,
	NetworkAnimator__ctor_m440265F51814E5C4E99207C6EDB133E76F208FB9,
	NetworkAnimator_U3CAwakeU3Eb__14_0_mF3F496D8681E4A54455BA1092DE2A4C88B197FCE,
	NetworkAnimator_MirrorProcessed_m6B265DF87A5010F76ECD2FD84DD8159149F859B7,
	NetworkAnimator_get_NetworkanimatorSpeed_m9D39AE82B53EAFCD11A98AB3CB4669A68D8D42CE,
	NetworkAnimator_set_NetworkanimatorSpeed_m62E06E8AA0515920D78DEE09603F26FAB03DFA4B,
	NetworkAnimator_UserCode_CmdOnAnimationServerMessage__Int32__Single__Int32__Single__ByteU5BU5D_m9D02589DEE51FEBE0DA757CBC808546CF9846E27,
	NetworkAnimator_InvokeUserCode_CmdOnAnimationServerMessage__Int32__Single__Int32__Single__ByteU5BU5D_m7BBA7F78D7357FDBE5816BD686F8400B7B0584CA,
	NetworkAnimator_UserCode_CmdOnAnimationParametersServerMessage__ByteU5BU5D_mBED948C4DF85B3DC17C2464B1565B36223222996,
	NetworkAnimator_InvokeUserCode_CmdOnAnimationParametersServerMessage__ByteU5BU5D_m4BA699465A623F50EB0F06F66CF7F3940B1444E7,
	NetworkAnimator_UserCode_CmdOnAnimationTriggerServerMessage__Int32_m14B502BD60282FFAA5D5BF3C882E4DE93E63581E,
	NetworkAnimator_InvokeUserCode_CmdOnAnimationTriggerServerMessage__Int32_mA9EBBF3ED3746A3779AD9766D195907AF04B5FCD,
	NetworkAnimator_UserCode_CmdOnAnimationResetTriggerServerMessage__Int32_m09021B5E5F96F820379CF0E51462E14E87C0018E,
	NetworkAnimator_InvokeUserCode_CmdOnAnimationResetTriggerServerMessage__Int32_m565C0682E01F6C5A7B188124A6F8139C7CB0DBC6,
	NetworkAnimator_UserCode_CmdSetAnimatorSpeed__Single_m7F71C44E4C125AF8A1CD08F372AB6FE39A13CE52,
	NetworkAnimator_InvokeUserCode_CmdSetAnimatorSpeed__Single_mC6B549CC13CA65B43699CE813374D470BDE50014,
	NetworkAnimator_UserCode_RpcOnAnimationClientMessage__Int32__Single__Int32__Single__ByteU5BU5D_m84FD050FE9C01CE0CEA79C6207A08EABC4904F23,
	NetworkAnimator_InvokeUserCode_RpcOnAnimationClientMessage__Int32__Single__Int32__Single__ByteU5BU5D_mE55F86395222424AE1AF34A31C18EB528E707996,
	NetworkAnimator_UserCode_RpcOnAnimationParametersClientMessage__ByteU5BU5D_m7559B1EA4EAC8774016A207B59451AE266403EBF,
	NetworkAnimator_InvokeUserCode_RpcOnAnimationParametersClientMessage__ByteU5BU5D_m83DF9518D29AF25D45E70EA0805C4768C929709B,
	NetworkAnimator_UserCode_RpcOnAnimationTriggerClientMessage__Int32_m3144BA9CDE4FD344ACCF9017156B28BF72668F96,
	NetworkAnimator_InvokeUserCode_RpcOnAnimationTriggerClientMessage__Int32_mBD479A0DE51BB9EEF62FE2914B4AEB620C653EC2,
	NetworkAnimator_UserCode_RpcOnAnimationResetTriggerClientMessage__Int32_m834BA2E7C7F845EC163E756984A9D37D11D2C510,
	NetworkAnimator_InvokeUserCode_RpcOnAnimationResetTriggerClientMessage__Int32_m97363954B4A10EB76C8805A913DDDD355DF3FAD3,
	NetworkAnimator__cctor_mD55D588FF4458B4B31D17DD4C156FB2811919C01,
	NetworkAnimator_SerializeSyncVars_m29547B23155411F3127C7AAF934BA5BECE9A4965,
	NetworkAnimator_DeserializeSyncVars_m843987C2BA63B9B3D42EFFDC0E987FF6EF200DCE,
	NetworkLobbyManager__ctor_mDD438F9E8843DD6EC931E379E35A7DD9174F230A,
	NetworkLobbyPlayer__ctor_mDE6D3E256AA6830D69248C10B78EE6DF26D8D36F,
	NetworkLobbyPlayer_MirrorProcessed_m4F8A7A47CEAD82D21054880150975E599C32C2AA,
	NetworkPingDisplay_OnGUI_m8837A9849EC29D1CA88C40497AB89882328E8C05,
	NetworkPingDisplay__ctor_mBC8A423E0D15B0C993B47934910C668E3125269B,
	NetworkRoomManager_get_allPlayersReady_m3D2082D0C7505187293ACF4EE52DD040C04139A2,
	NetworkRoomManager_set_allPlayersReady_m3298A66DAA3046834FEBA482445BBCA7BBA48B23,
	NetworkRoomManager_OnValidate_mBBFC0B5CEFFCCC7FEC0CF5C4FC431483850A9A56,
	NetworkRoomManager_ReadyStatusChanged_mB6EA41087E53FD14E0CB6EB2C1442A4B29B83C1C,
	NetworkRoomManager_OnServerReady_m722104B1C4891F10BE0DD1314C23C93C55C562A3,
	NetworkRoomManager_SceneLoadedForPlayer_m0ACA8FF2E8AEC1E7743F300A2C816B0C0EB956D7,
	NetworkRoomManager_CheckReadyToBegin_m4255BD331B9D656CD71781142BC9E23E7833B9AD,
	NetworkRoomManager_CallOnClientEnterRoom_m9D5D793797E831E2C1865C797BBCE9BD11002F39,
	NetworkRoomManager_CallOnClientExitRoom_mDF423C0F220431627CE40219CC21418C4EB27242,
	NetworkRoomManager_OnServerConnect_mB4357D953A85EE1475F8B907233D7834C9C639E8,
	NetworkRoomManager_OnServerDisconnect_mED4D7DBD4D9C917B2A785F3D088AB2C46BD6BC18,
	NetworkRoomManager_OnServerAddPlayer_m1866C29C4AF0935EEC8A05992B04456FE97ADCB9,
	NetworkRoomManager_RecalculateRoomPlayerIndices_mDD15686D1EF99B21CB4B687A80AEEC7A8D513F22,
	NetworkRoomManager_ServerChangeScene_m37ECD16EAB2903DB90B89A945982491703C10632,
	NetworkRoomManager_OnServerSceneChanged_m8C9275BC2C3BAD39BF505B6346BE8D00CAF85443,
	NetworkRoomManager_OnStartServer_mB672B2EAF2186976331F24E65CA93487E10FE132,
	NetworkRoomManager_OnStartHost_m05576B071D7FBC1BFAD9099C0FAD5AAAC0FB5914,
	NetworkRoomManager_OnStopServer_mD9604B3569B3CB4C224E5982C27B02A10D4F9F72,
	NetworkRoomManager_OnStopHost_mEBDB6131C1C88915C480BD7351F1095A4FF2617A,
	NetworkRoomManager_OnStartClient_m3CB42985BBC56EC3CBC6C156956E11814D4CAB5A,
	NetworkRoomManager_OnClientConnect_mDAF25D4A6D6DD2B89965E49ECFC2FA460D0B9946,
	NetworkRoomManager_OnClientDisconnect_m884295F9A0714B46DEDD300F1B7528BE7C123FFA,
	NetworkRoomManager_OnStopClient_m347169D4CACF5D2DF76B5A0B979AC3177900378A,
	NetworkRoomManager_OnClientSceneChanged_m9AF840B1BD5364A799A6A21AB64EC2465AEF3AC8,
	NetworkRoomManager_OnRoomStartHost_m48B3CEB2BADCE0213AE1573618F3329A21D6A776,
	NetworkRoomManager_OnRoomStopHost_m59A119E866FD9DBA48730998B5C26759ECF92844,
	NetworkRoomManager_OnRoomStartServer_m7E063FDDB0AD99262E252B407704A8F333A41100,
	NetworkRoomManager_OnRoomStopServer_m22B121E391F19BBD7FE621A71469E74BB2DC0F2F,
	NetworkRoomManager_OnRoomServerConnect_mEA0C2852C0DD7A4E2D5DED27C844B1912DFDB9F2,
	NetworkRoomManager_OnRoomServerDisconnect_mCCDEDC4DF723343782A07426B6BEA7417CC06515,
	NetworkRoomManager_OnRoomServerSceneChanged_mB4102EBFB1E0A4A98E62ABA3944A7F0F2AF220CA,
	NetworkRoomManager_OnRoomServerCreateRoomPlayer_mB90B114AE4682757FAF4A401234C54B9A92393E3,
	NetworkRoomManager_OnRoomServerCreateGamePlayer_m0A931317C73F05FAB84D40AAD5A795151D2965FA,
	NetworkRoomManager_OnRoomServerAddPlayer_m619CFC854D6EEFE62955973D796E8F09ADF87B0E,
	NetworkRoomManager_OnRoomServerSceneLoadedForPlayer_m44C72F5EEEB65EF04C7381616F0012DB8A494EC0,
	NetworkRoomManager_OnRoomServerPlayersReady_mE9FBB299CB678712AC6111D64694E256A820215D,
	NetworkRoomManager_OnRoomServerPlayersNotReady_m810E9E88EBB6471872A29CA17ED26F74CAC55995,
	NetworkRoomManager_OnRoomClientEnter_mC1F34FF29F855787E018A38D9733945AFB6D5ED0,
	NetworkRoomManager_OnRoomClientExit_m5581110225F6CEC643BDD963777CBBB2160A7668,
	NetworkRoomManager_OnRoomClientConnect_mFC1CD2694515096D35E272E024F067A60F32CB57,
	NetworkRoomManager_OnRoomClientConnect_m8364F1B62B69C9308CCF9EA28A845F50A5585F0A,
	NetworkRoomManager_OnRoomClientDisconnect_mD079444CA1F06D8A5E03E8FAD6984E273A040E63,
	NetworkRoomManager_OnRoomClientDisconnect_m7AE0AC7B5FB7432607FFB707CC17E249B1DA9896,
	NetworkRoomManager_OnRoomStartClient_m034772143DC8E6D6D7BBE8D435B4F9792E306849,
	NetworkRoomManager_OnRoomStopClient_mF85E94F1C15BBB786E60847354CB7D23362597B0,
	NetworkRoomManager_OnRoomClientSceneChanged_mD001F5BDC5A5BC18356653212308E4370E390B99,
	NetworkRoomManager_OnRoomClientSceneChanged_mD34F382DBA6D1EF978E3ABC3C52D2CBB8CD00DE4,
	NetworkRoomManager_OnRoomClientAddPlayerFailed_m341E588654470F7CFC3844A36329DE10360EDFE8,
	NetworkRoomManager_OnGUI_mE31AF52E018C083842D6A656E912882B33A9BD85,
	NetworkRoomManager__ctor_mADF6C6FA77F112272B1B11D167CF9B6AEDA64FAC,
	U3CU3Ec__cctor_mC9018B53B62A757D391F9AA4C7C901C8FAAF6153,
	U3CU3Ec__ctor_mC6C29BAB4A3EE410677501FB94F2EF94854C1C20,
	U3CU3Ec_U3CCheckReadyToBeginU3Eb__16_0_mD539D4CF62A3D159F1A837E7C81A2476F20EE142,
	NetworkRoomPlayer_Start_m6EF9EE75D76707F7B26F9AEBE4EE9E9B5F3F7280,
	NetworkRoomPlayer_OnDisable_m9F4C01360486541184371C6FCADF9DDEC25688EE,
	NetworkRoomPlayer_CmdChangeReadyState_m000A9554EE062B6FF78483B8EF55BC2C37F747CC,
	NetworkRoomPlayer_IndexChanged_m8F0D830B73CCED0429FAF3C523AAF493E3ED4AB8,
	NetworkRoomPlayer_ReadyStateChanged_mD559C745FFBFE7550B7724D6879F168B6E7C40C8,
	NetworkRoomPlayer_OnClientEnterRoom_m85D020E10399B927E54824FE8C47555B03175E72,
	NetworkRoomPlayer_OnClientExitRoom_mB76D7D0975B7F773A5EC5C56775BF363A58961D0,
	NetworkRoomPlayer_OnGUI_mFE9117894DF69C10E929E26C6675A6754778076E,
	NetworkRoomPlayer_DrawPlayerReadyState_mE071CB838FDCF734437D9A1CDDD4238D3F3F0C76,
	NetworkRoomPlayer_DrawPlayerReadyButton_mC5F76C68BA3F210A8892B244F2C551D9150FCBF1,
	NetworkRoomPlayer__ctor_m74CD1F66EB67F56C14FE577173623D97C67AC2F4,
	NetworkRoomPlayer_MirrorProcessed_m8F12D3397E5F231AD05BAECE3C9B127E1900F8D2,
	NetworkRoomPlayer_get_NetworkreadyToBegin_mCE15308447206625859308A223210717E40EEA20,
	NetworkRoomPlayer_set_NetworkreadyToBegin_m69E8573E8325C9C28BC750BEC5B53990C63076BC,
	NetworkRoomPlayer_get_Networkindex_mCB254CC3650E7295574621F316249926C47BEB2B,
	NetworkRoomPlayer_set_Networkindex_mAC4BA4F2F4D428C601711E32E7CE3ED2D5B298D8,
	NetworkRoomPlayer_UserCode_CmdChangeReadyState__Boolean_mC036C4A64EA54820574BAE749F549678604CEF91,
	NetworkRoomPlayer_InvokeUserCode_CmdChangeReadyState__Boolean_mD6D0A78007DCA9399CA9CF7C8A0DF430587F6770,
	NetworkRoomPlayer__cctor_m38C2FA8512D80C691861CA50E688CA7CC41CF3EE,
	NetworkRoomPlayer_SerializeSyncVars_m6DE1E92238A3D8384E9D6FEF915201B86DCC48F4,
	NetworkRoomPlayer_DeserializeSyncVars_m098744A499CAE6CCF5F0B4CE4EFD8EB1763FD950,
	NetworkStatistics_Start_mD27C99149BC22F0371DFC0459575EA71404AE1B9,
	NetworkStatistics_OnDestroy_m490F38913FF37D0EF27BB9472A29E01BEA014918,
	NetworkStatistics_OnClientReceive_m15522D2E0B7191369DE9355ABA7F90BF71CDD99E,
	NetworkStatistics_OnClientSend_m33338E874CFA1A2FAA3505F4C50B4A7D226A7551,
	NetworkStatistics_OnServerReceive_mACB3C2E96142F36C880A180BA361AFD54ED08C83,
	NetworkStatistics_OnServerSend_mBA206EE1021B894E4B3192C9E4C48E48BFB4E4B7,
	NetworkStatistics_Update_m7219A51EEB78B1BDC84A231E893F4B7200D9D49E,
	NetworkStatistics_UpdateClient_m30614B8549A2D75E3FC88B3659FE9F5ABEA7DFC9,
	NetworkStatistics_UpdateServer_mB0E7DDEA0CBFDC0C8D32490CA77757E200CE74C3,
	NetworkStatistics_OnGUI_mA76E96F7EBAB1A822C45DB2EE31E62C68BE768BE,
	NetworkStatistics_OnClientGUI_m7BD9B76BF5DCDDA9B2ACDDB4FF9ED4B946A7479F,
	NetworkStatistics_OnServerGUI_m2A65A2A6CB5A59256691D86770DFE4955BFB7E7C,
	NetworkStatistics__ctor_m5AF22E614AF6170202942D6E9D80582DF3917CF1,
	NetworkTransform_get_targetComponent_mFC4994F9CDF95FBAD976A28AC8E940EF17E04974,
	NetworkTransform__ctor_m4C0F82053B3B751AB1CD14631F756E29F593A4F6,
	NetworkTransform_MirrorProcessed_mBD058E5A0361F5B3C6EC376DCA9D0A74393E1690,
	NetworkTransformBase_get_IsClientWithAuthority_m4842DA4F62C34FD10EE8161E2BDBF18EEDDF218A,
	NULL,
	NetworkTransformBase_get_bufferTime_m1B5C82B0BA8A2DE2BEE359D97B13348AC72A3080,
	NetworkTransformBase_ConstructSnapshot_mC1FA78F69BA5F58304E138E23072BD9EE4A68133,
	NetworkTransformBase_ApplySnapshot_m69F8D52B462CCA5723E99FDC224733F69267E59E,
	NetworkTransformBase_CompareSnapshots_m5A648CA2DD6DF96B62668101C2D389CCCDD6093B,
	NetworkTransformBase_CmdClientToServerSync_m912A267278EE16E9636BFA7B70B2DB2099E6F891,
	NetworkTransformBase_OnClientToServerSync_mD0C05A2ADDD36FDE327FEE8A3D5C64E227D8AF36,
	NetworkTransformBase_RpcServerToClientSync_mAB05B7FD9EDF0C1F38C8E38BD3AF5310723F21BE,
	NetworkTransformBase_OnServerToClientSync_mA4929D2693891DBB4367B883644FAE22FDCAD9BF,
	NetworkTransformBase_UpdateServer_mD27A06E138396A3CDFE1C694059285D88797C51F,
	NetworkTransformBase_UpdateClient_m81EC6BA4DFD32748F11CCC4E282B1ED54DEB94D4,
	NetworkTransformBase_Update_mB7004C46943A35BE3FD76EAE96EB0B1D0FA52B31,
	NetworkTransformBase_OnTeleport_m8DE18114C98F024D1449F9E7838211C0AEE06ECB,
	NetworkTransformBase_OnTeleport_mEF0E028428D75F39607798E34E7330CA74E3435B,
	NetworkTransformBase_RpcTeleport_m8F984E88B2BB51B01EB14DCB70BD6B9F4582CEDD,
	NetworkTransformBase_RpcTeleport_m073EA6A55FF2452BEE377422CB362A909A60B8E2,
	NetworkTransformBase_RpcTeleportAndRotate_m438027D989CDEA06B42C9FA2E7486E046EF18538,
	NetworkTransformBase_CmdTeleport_m6C5557F3E6E7A0578D1509D2794721F8EACDF505,
	NetworkTransformBase_CmdTeleport_m17DA700489EE7C25D0E258D2511D4A159BB67C40,
	NetworkTransformBase_CmdTeleportAndRotate_mF7A98E2F343F702B155606C1352B9D86741876D2,
	NetworkTransformBase_Reset_m413C870079713BE5B927D42BBF1538FDE3A6752B,
	NetworkTransformBase_OnDisable_m789E9CF179F78C53F78316F5470593FAB69E6F56,
	NetworkTransformBase_OnEnable_m4D0775F66252AAFE8700EBE4570AD51741A113F1,
	NetworkTransformBase_OnValidate_mC1C2EEF652769DD0AD4C7F47EB85F3EFA276E5E0,
	NetworkTransformBase_OnSerialize_m20F95948C257ED8A185D85474387D6D706D15F6C,
	NetworkTransformBase_OnDeserialize_m4476BC41308C9029BE559F9AE8D1060D7300FF8E,
	NetworkTransformBase__ctor_m5D39D8174AD78B18A54EA9CC4AD441C41B9D5279,
	NetworkTransformBase_MirrorProcessed_mCAC10AC384F4D5DB2184D989D0B69CB919A592AA,
	NetworkTransformBase_UserCode_CmdClientToServerSync__Nullable_1__Nullable_1__Nullable_1_m4F781351B6313C63D4F9D98CD2053463D0F417DA,
	NetworkTransformBase_InvokeUserCode_CmdClientToServerSync__Nullable_1__Nullable_1__Nullable_1_m98E7B81C718F89D232305CE9427D91FEED5A51F5,
	NetworkTransformBase_UserCode_RpcServerToClientSync__Nullable_1__Nullable_1__Nullable_1_m87DA955CDAA2A3F88D70F6A55AA6AF520D3850A7,
	NetworkTransformBase_InvokeUserCode_RpcServerToClientSync__Nullable_1__Nullable_1__Nullable_1_m00F366184378BC5621A53AE9164D36B43EAD6140,
	NetworkTransformBase_UserCode_RpcTeleport__Vector3_mF15097C4D3D9BF5DDCC9DD5C12A82A936897AC11,
	NetworkTransformBase_InvokeUserCode_RpcTeleport__Vector3_m10EA5EA7EEB6F7D22AF07A5259C2A390C34F873B,
	NetworkTransformBase_UserCode_RpcTeleport__Vector3__Quaternion_m5A641AAAD87ECEAB5C4C039A4EAD987E44DA2E20,
	NetworkTransformBase_InvokeUserCode_RpcTeleport__Vector3__Quaternion_mF8411DF293C91CB3207A22F0A319E70AD875D6E2,
	NetworkTransformBase_UserCode_RpcTeleportAndRotate__Vector3__Quaternion_m44B7D8F60DDE38EB16162B8206AABE57C9DB10E7,
	NetworkTransformBase_InvokeUserCode_RpcTeleportAndRotate__Vector3__Quaternion_m14633BCD6A50F4CE01EED94730DB263F4318A388,
	NetworkTransformBase_UserCode_CmdTeleport__Vector3_m480D819918CD7F7BAF42AAD2F70C124DEB75E038,
	NetworkTransformBase_InvokeUserCode_CmdTeleport__Vector3_m93525715C8208A3B8BDE7DDF858312E643883190,
	NetworkTransformBase_UserCode_CmdTeleport__Vector3__Quaternion_m5BD0928D8F4441D66FF71BF0E956262C97490013,
	NetworkTransformBase_InvokeUserCode_CmdTeleport__Vector3__Quaternion_mE6EE9AA0BDA716133F80DAB7339672E5F8B9AD7E,
	NetworkTransformBase_UserCode_CmdTeleportAndRotate__Vector3__Quaternion_m988FE9B5046BD9F99262C6C8480F68FBBBC5BAD9,
	NetworkTransformBase_InvokeUserCode_CmdTeleportAndRotate__Vector3__Quaternion_m704E2B0211403EDC73DD43ADCF3A5AF8781EEF00,
	NetworkTransformBase__cctor_m626DF45B29C12A0F19AD72B4A8E7FDD43DDBFA8B,
	NetworkTransformChild_get_targetComponent_m3DF9FD6F234A2EF6C017DA3224A0523D931E37FA,
	NetworkTransformChild__ctor_mD838D95F75CAC92A52E737D3248FA00EA5302D2C,
	NetworkTransformChild_MirrorProcessed_m674B9987C43009E2C46A2F69CEE0FBA1AAFF29A5,
	NTSnapshot_get_remoteTimestamp_m1AA923C8C9FF1F473CEECEAC7AB1BFB887999BBE,
	NTSnapshot_set_remoteTimestamp_mB571444062A9349310C12DEBCF202DF5E6D90038,
	NTSnapshot_get_localTimestamp_mB4EB78DF581B393267C3B8B9E922D83B9388E4FE,
	NTSnapshot_set_localTimestamp_mDE387C34337B2611340C05E048A3BAD0832E7111,
	NTSnapshot__ctor_mD163416E3EC45F2AC30B3BF21C603EFCDF6A2D1B,
	NTSnapshot_Interpolate_mE0508B4ABF4DE8B33DFD3509F41EC70C7C7E4F5F,
	NetworkLerpRigidbody_get_IgnoreSync_m0ED2D74F9EA799C910BD8CE2046BA07EA4BCDAD5,
	NetworkLerpRigidbody_get_ClientWithAuthority_mCA3FA6948CD62ECE1FE9619173F6F4C24BAFA1AE,
	NetworkLerpRigidbody_OnValidate_m51A20D53A25FCE7510C97C08826C8FBAFC2912FF,
	NetworkLerpRigidbody_Update_m61EF256EB33FC7A341FE59C049320280CD36BAE9,
	NetworkLerpRigidbody_SyncToClients_m1078A84609C18A2D554BA306B30BA906E1E6D576,
	NetworkLerpRigidbody_SendToServer_mC5DE95622F16A8F9FF7F13E7F8059681A3A80142,
	NetworkLerpRigidbody_CmdSendState_m93401347EC12BF8A425DB7609CC4C50F9480836E,
	NetworkLerpRigidbody_FixedUpdate_mAC7598042C3158A83B179B478D80785CA0D90712,
	NetworkLerpRigidbody__ctor_m3683F5C16A7E2EE236F48A9B6A1C2D537F63E4A9,
	NetworkLerpRigidbody_MirrorProcessed_mC79601FA6A1EA88D4EC3D9E19801B7B4DDDF8237,
	NetworkLerpRigidbody_get_NetworktargetVelocity_mFC35BFA0C9539B67AA04145E6D55AAAEC562216B,
	NetworkLerpRigidbody_set_NetworktargetVelocity_m57CA07453234CB22A3087BB2C11F3DAE1BA2AB0A,
	NetworkLerpRigidbody_get_NetworktargetPosition_mE50A8DE64B66990CE242B23A0A64A4E47ED3D174,
	NetworkLerpRigidbody_set_NetworktargetPosition_mFDFB0915494D4E4A7041BBC2A32401424EC1A737,
	NetworkLerpRigidbody_UserCode_CmdSendState__Vector3__Vector3_mE901895B6550CCA26C600C73A67556B60C54FC2F,
	NetworkLerpRigidbody_InvokeUserCode_CmdSendState__Vector3__Vector3_mA3F2F1CF77B8E5E177FD68CF1C0BC82CF7AF0AD9,
	NetworkLerpRigidbody__cctor_m7C404671430C36C4331BD5F09F808E44B327AFC5,
	NetworkLerpRigidbody_SerializeSyncVars_mBC592FBA9BBBE0A0401CD634731EECD76E9E73A8,
	NetworkLerpRigidbody_DeserializeSyncVars_m46F8767D5BEBDE14AADE3C4F3038EBDA6200EA04,
	NetworkRigidbody_OnValidate_mEAC00439EC11949D787A204DA7AC4816485EB624,
	NetworkRigidbody_get_IgnoreSync_m8A4A9BCF9C61B22AE1D9E0C2F6FED2179F9D26F5,
	NetworkRigidbody_get_ClientWithAuthority_m35787455D13760A0CFE8D0E025FDC62C3F3BFD49,
	NetworkRigidbody_OnVelocityChanged_mA7C108A53E19FDDCED6DA74DD0109423DC48FAD2,
	NetworkRigidbody_OnAngularVelocityChanged_mF3CF1B86FFA297336F21B71F452E33E0661496C5,
	NetworkRigidbody_OnIsKinematicChanged_mB1B2A2680510626A35B8E169001AE74D8D412A4E,
	NetworkRigidbody_OnUseGravityChanged_m360676D33BCB5AF32E55394AC21501AF0457DB08,
	NetworkRigidbody_OnuDragChanged_mAF40FE2070567FEA56F9A999A26B0BB62D52EE6F,
	NetworkRigidbody_OnAngularDragChanged_m4C51F88AFB0597BD044579F30E586BA0DB061994,
	NetworkRigidbody_Update_mDC5DB47294AEA738C5F80BE2D1B133524AF99E80,
	NetworkRigidbody_FixedUpdate_m0F56F9647E1ADFC98FE2AF3E9AE798097E45C63C,
	NetworkRigidbody_SyncToClients_m90F9398F3676F811B25FA011E772F9D81C60A78C,
	NetworkRigidbody_SendToServer_mF133C6FB4703D6FA725FD49EDA82EC5497A9C3E4,
	NetworkRigidbody_SendVelocity_m3769B4FB59251D4BDA2AE142274F21BDF703131D,
	NetworkRigidbody_SendRigidBodySettings_mE5E53E6D8EDE59550F4DBCD908C3B1868D6D5A89,
	NetworkRigidbody_CmdSendVelocity_mA2C9C1BBF59E5D7AFB0FDF1F9F01A091190BFF21,
	NetworkRigidbody_CmdSendVelocityAndAngular_mBE8AB7AB2EE9BC6C1B3E03E44EB244DE384E52EE,
	NetworkRigidbody_CmdSendIsKinematic_mADAE264BA3DF5A19173377DF9B3B3B9F10C0E486,
	NetworkRigidbody_CmdSendUseGravity_m3298BAD8192D4336B7372CA486C0AE2188DF85D5,
	NetworkRigidbody_CmdSendDrag_mB36830B010A748F6FD83CD718B6A4B778314405F,
	NetworkRigidbody_CmdSendAngularDrag_m8D02B2D7912120C22E6F96383108B6A5DA6102FE,
	NetworkRigidbody__ctor_m84AE218BBA8229A552B53FEC93456AB77BAF0B17,
	NetworkRigidbody_MirrorProcessed_mCF978BCD865DCA8411D3562265D71C43C2BA4740,
	NetworkRigidbody_get_Networkvelocity_mEF0D199753411679BA0684957FE371703D34476B,
	NetworkRigidbody_set_Networkvelocity_m133B3EDAEF908DFD45B9547D9A28E0E8F58B7BD8,
	NetworkRigidbody_get_NetworkangularVelocity_m066174F245E22E23840E2408BFC3A34A09F77586,
	NetworkRigidbody_set_NetworkangularVelocity_m1CDFD3728122933194B6A06AA9707310DF039D06,
	NetworkRigidbody_get_NetworkisKinematic_mBB6F8AA6FA957F8BBD9F3A8138E0EC97E54B8133,
	NetworkRigidbody_set_NetworkisKinematic_mCD921B67C804E7B3FFDA0355CA13E7CD81640225,
	NetworkRigidbody_get_NetworkuseGravity_mB0170724514DC1235BC20BCAEC6CD1DC647AB1B5,
	NetworkRigidbody_set_NetworkuseGravity_m4FCCD8F5A6165295E81ABA0A3AEADA56C1359469,
	NetworkRigidbody_get_Networkdrag_m52BE28035A1CCEB6975B65A1E62EF09C27B9FB5E,
	NetworkRigidbody_set_Networkdrag_m831F50A14CD3AD6FFCDD3AD6C446BFA6E084D320,
	NetworkRigidbody_get_NetworkangularDrag_mD3A2530687880B0C0762F1E313A4A038C50E119F,
	NetworkRigidbody_set_NetworkangularDrag_mAEB65DC77BBBB1702176774A208A526DDCCCAD7B,
	NetworkRigidbody_UserCode_CmdSendVelocity__Vector3_m63EEECCE66CA0D5D6FA36697D3020671CAB8DF8D,
	NetworkRigidbody_InvokeUserCode_CmdSendVelocity__Vector3_mEB43C89B0E69623242A190EE837C6823D4667DE9,
	NetworkRigidbody_UserCode_CmdSendVelocityAndAngular__Vector3__Vector3_mAA3E685714459A666BEE87D162688C295BAF5EA0,
	NetworkRigidbody_InvokeUserCode_CmdSendVelocityAndAngular__Vector3__Vector3_mD40CB5650874EBA89B5B3C16C4C45528DE8AFA17,
	NetworkRigidbody_UserCode_CmdSendIsKinematic__Boolean_mDFA5AA5020A2FB98F883334AE28AA5FFF07B152C,
	NetworkRigidbody_InvokeUserCode_CmdSendIsKinematic__Boolean_m7EF9E9DA879635852C6FBA43630B593B3F31F478,
	NetworkRigidbody_UserCode_CmdSendUseGravity__Boolean_mCB8FED78264795AC668DC081AC7ABF6FD738359D,
	NetworkRigidbody_InvokeUserCode_CmdSendUseGravity__Boolean_m72B42BBEC1A25EA52D6B5662E14FC5AA94938D02,
	NetworkRigidbody_UserCode_CmdSendDrag__Single_m92957119B7E9349480C11309B7EB35934402193B,
	NetworkRigidbody_InvokeUserCode_CmdSendDrag__Single_mEC583CEF7956AD90F0AF3905C3B3801FBBEF4819,
	NetworkRigidbody_UserCode_CmdSendAngularDrag__Single_mFA69E95FE27BA7CF823EB8BE6BDC899E745C7E79,
	NetworkRigidbody_InvokeUserCode_CmdSendAngularDrag__Single_m0B33877523BBD08B3F1B70551DE8DA66308B71E3,
	NetworkRigidbody__cctor_m3F631417A8D52A890EF443F65164C1FB3409C5A9,
	NetworkRigidbody_SerializeSyncVars_m074559AAAB496A1A89B665A26325B75CBCA3524A,
	NetworkRigidbody_DeserializeSyncVars_m8971AA80CBEE9BC6758AF7381C07DBEFB4E1810F,
	ClientSyncState__ctor_mC543590AB9EEAEAF13854D46B768CF26EDC5B08A,
	NetworkRigidbody2D_OnValidate_m12F3620BA2EE1745D8368ED7286B2A580D587132,
	NetworkRigidbody2D_get_IgnoreSync_m008BA545EAE32A3F7748CF36AEF69358A892A0AC,
	NetworkRigidbody2D_get_ClientWithAuthority_m419682C385D0B113199042FF253D1FAC8EE680DD,
	NetworkRigidbody2D_OnVelocityChanged_mB52A772E8868846B22CDF591EF379ACFED0B4649,
	NetworkRigidbody2D_OnAngularVelocityChanged_mDEDAF5034D6F42667AC43AA2EB22672FB9B9CC2A,
	NetworkRigidbody2D_OnIsKinematicChanged_mE6695E5F0CE731AA2955F54B1E7DED4A7E63BC51,
	NetworkRigidbody2D_OnGravityScaleChanged_m9887C2BB1F8DDDFD8C5A6BAD12DF484CD8D4BDB9,
	NetworkRigidbody2D_OnuDragChanged_m7C0CDFCB341A5CD040B227C4F4E3E805759F2C45,
	NetworkRigidbody2D_OnAngularDragChanged_m41AF89DA353773E70C00468316DE34B9A42C51F1,
	NetworkRigidbody2D_Update_m832C5AF365D3E3A6F8BEF851AD6730B57F8D0BA7,
	NetworkRigidbody2D_FixedUpdate_m8AC6065AF7928EC114DC8B3B68BD1C8F920F4C56,
	NetworkRigidbody2D_SyncToClients_m9CF6E870AE483A560522711D1EEFCA2812BF722B,
	NetworkRigidbody2D_SendToServer_m2F258C46315494DA0A216062D835A1DFA1BF2F82,
	NetworkRigidbody2D_SendVelocity_m488AC1D609CBDDB99E7692D9782EE48CEEE1E3D0,
	NetworkRigidbody2D_SendRigidBodySettings_m0FD6B32441438E45B9EE602BF9722CB5129A2773,
	NetworkRigidbody2D_CmdSendVelocity_m346CC5DD8CF8B413F17C77ED141104B9515A003E,
	NetworkRigidbody2D_CmdSendVelocityAndAngular_mC11356B93759425A91C385B4D98905ABC29C6992,
	NetworkRigidbody2D_CmdSendIsKinematic_mC395694DE63C8727AD33FDAF5E9B696135C2BF43,
	NetworkRigidbody2D_CmdChangeGravityScale_mFCA659FC30BB080688C0BBA2B1AF4553D113391C,
	NetworkRigidbody2D_CmdSendDrag_m314C188BE4CE3B8C18AA095C4697E5C7495F9199,
	NetworkRigidbody2D_CmdSendAngularDrag_mB12CD2A1C195502EC36CD38AEFD04BF654E39C48,
	NetworkRigidbody2D__ctor_mD7241A558F5A2FD0E2BF9BAE59B5E2E13A903627,
	NetworkRigidbody2D_MirrorProcessed_m61E0EF6FECAE614F892CED615F973A2F8DEA0A7B,
	NetworkRigidbody2D_get_Networkvelocity_mC74ABB0F2849867B9E6362E472815388DC92B955,
	NetworkRigidbody2D_set_Networkvelocity_mE4A457220B4AB1C6195BEF2DEDEAB49B68886140,
	NetworkRigidbody2D_get_NetworkangularVelocity_mC64533A49CF2C988C0B51E3275DB75DE6991A24B,
	NetworkRigidbody2D_set_NetworkangularVelocity_m12F485686C78CC3A81AEFA7F62C9B36468649924,
	NetworkRigidbody2D_get_NetworkisKinematic_m5024E96C5B0B7800F79A4B5C4361DFD0955328D8,
	NetworkRigidbody2D_set_NetworkisKinematic_m72A8887DA1EE3320A20DFAD8A118909A13249A34,
	NetworkRigidbody2D_get_NetworkgravityScale_m9952509F42650C3ED6E4C0D2AA61CCECBD0FE1E0,
	NetworkRigidbody2D_set_NetworkgravityScale_mA850FEBCF63736C555EAFC1903840665CE3098C0,
	NetworkRigidbody2D_get_Networkdrag_mA5865030789C4B614CCCC42EFA90E5C1C78C8F27,
	NetworkRigidbody2D_set_Networkdrag_m1DEC71BF34C256D3FEB4518C28CA4DB11613E9CE,
	NetworkRigidbody2D_get_NetworkangularDrag_m8FF720CF826BA10DC5C46FCBCB3A227AF3DA44F9,
	NetworkRigidbody2D_set_NetworkangularDrag_m4921CAD2C48BD26A2AD32B763892A96E0422577C,
	NetworkRigidbody2D_UserCode_CmdSendVelocity__Vector2_mEC55D2A8EE795820694422AF2FA191D30B3E3582,
	NetworkRigidbody2D_InvokeUserCode_CmdSendVelocity__Vector2_m3E446857383A1D1B0A4B5B6802D98CE7B6778725,
	NetworkRigidbody2D_UserCode_CmdSendVelocityAndAngular__Vector2__Single_m57DBF13383F15024B79E7B7F438983295C595345,
	NetworkRigidbody2D_InvokeUserCode_CmdSendVelocityAndAngular__Vector2__Single_m0B83ED5CD00F6CAB25AFEFFBAB17C0B65AC0A3C0,
	NetworkRigidbody2D_UserCode_CmdSendIsKinematic__Boolean_m886EAC686E7616C1685B7A26C46E49F9B29B4280,
	NetworkRigidbody2D_InvokeUserCode_CmdSendIsKinematic__Boolean_m03E56F5761C4103E4A5E1986AA90FB3C4A0A904D,
	NetworkRigidbody2D_UserCode_CmdChangeGravityScale__Single_mFEB489727311A915C0361D4E076FA7C611480336,
	NetworkRigidbody2D_InvokeUserCode_CmdChangeGravityScale__Single_mFD4FBDFCBC3A6E3D2E1B8989991976595EDE48AF,
	NetworkRigidbody2D_UserCode_CmdSendDrag__Single_m9CECE61B613A57AB785DFD78BE849F34372A2823,
	NetworkRigidbody2D_InvokeUserCode_CmdSendDrag__Single_mFC664CDA520B78818E6E7FADA9F7FD1B39C851A1,
	NetworkRigidbody2D_UserCode_CmdSendAngularDrag__Single_m6E4BDCFEA554CE74A1D76C200468102A478630E3,
	NetworkRigidbody2D_InvokeUserCode_CmdSendAngularDrag__Single_m68C640B904C9392DA183519A9CDA28820575EEAE,
	NetworkRigidbody2D__cctor_mD69E41A075815AD406B563256F98ED6B185E489C,
	NetworkRigidbody2D_SerializeSyncVars_mF0A1AA955871A431E156CCC8FB1DB762D93B74B1,
	NetworkRigidbody2D_DeserializeSyncVars_m2349FF5C5C14BA6959F9A9170A14BBA774E0A8E4,
	ClientSyncState__ctor_mDE6071EDBDBC39E9B9244EF3294054219B67549C,
	NetworkTransform_get_targetTransform_mCF1300F8F25C5542751DBCAF4DD4DC99170CF033,
	NetworkTransform__ctor_mB2DBD23A7FD227B96DFC737102DB7FCA727FE096,
	NetworkTransform_MirrorProcessed_mCAD95FC4269E0581E25AC8D3329E1B446DB9BBC6,
	NULL,
	NetworkTransformBase_get_IsOwnerWithClientAuthority_m9721ED1E3BC17C1F050C61321E2D40ACC1F57F76,
	NetworkTransformBase_FixedUpdate_m820E7E32C0E10DD2FF089967FEC4AC4A30B4B122,
	NetworkTransformBase_ServerUpdate_m069451E249D5FD53A5ECDBACC43FF1291F9B975E,
	NetworkTransformBase_ClientAuthorityUpdate_m64CE70B6F8EB8D2194236087761E3B63471475F7,
	NetworkTransformBase_ClientRemoteUpdate_m69D9AE30FB0E0B22FB2D30851F2F4B83B4F1C355,
	NetworkTransformBase_HasEitherMovedRotatedScaled_m8C45DA33762E51CAEA75368A98452BEC643C2D7D,
	NetworkTransformBase_get_HasMoved_m213F6051B04D176E1BC9F5F474C72587E589595A,
	NetworkTransformBase_get_HasRotated_mC41C1E6CA23EE731DD00DC568C1CFA7F032D659E,
	NetworkTransformBase_get_HasScaled_m90AE6D5D682172233C5B0E713B05E14F6DD983AE,
	NetworkTransformBase_NeedsTeleport_mA94281756734F56CD48DAA2185322CC8478A12CD,
	NetworkTransformBase_CmdClientToServerSync_m0BBA2C3C4758CA0869FC380FBFCF3C17CD93DEDC,
	NetworkTransformBase_RpcMove_mEC79579548C60F30381B296BFD6D1F0C583ADB60,
	NetworkTransformBase_SetGoal_m8D4D99265CAAE907A213DC0FAA334A46E2B5749A,
	NetworkTransformBase_EstimateMovementSpeed_m956B628AA7775F341E941B2BAF38CA6DDA1F0A0A,
	NetworkTransformBase_ApplyPositionRotationScale_m9E3F3DA287D1BF95E10A6005F4A555E5AF759FEB,
	NetworkTransformBase_InterpolatePosition_m90B70F68A5D27B3429D20B9E903627658A7E524B,
	NetworkTransformBase_InterpolateRotation_mED10AEC6B1C9E2006FBAAF1202994A1AD85E4853,
	NetworkTransformBase_InterpolateScale_mA2358259E572923F29774671EDAC4D24CD673E8E,
	NetworkTransformBase_CurrentInterpolationFactor_mCFEAB6E6A682DBB4DC357BAE66AAF58E67E8B333,
	NetworkTransformBase_ServerTeleport_m76794E84A5C4AE260E8F5F8F9BB0184E3F82FF7C,
	NetworkTransformBase_ServerTeleport_m759BDF1BFE5D9208D2827A36725384B5949A0EA6,
	NetworkTransformBase_DoTeleport_mF56F6AE07B5EE385E32D271AC22AA411A7EE8BAA,
	NetworkTransformBase_RpcTeleport_m221E2786D89AA7375316A3680F0F77F775289AE5,
	NetworkTransformBase_CmdTeleportFinished_mBBAC7ED1FF0E1DDFBCBCB968749B5F6A8A1866CE,
	NetworkTransformBase_OnDrawGizmos_m8EDF0DCF900C88C886FC50A9ECE7C7F485F184A2,
	NetworkTransformBase_DrawDataPointGizmo_mB286B27082D0D29C123308E310694E1C6744A317,
	NetworkTransformBase_DrawLineBetweenDataPoints_mC7663D2FA02D48D365E30EFAAC86152237607E53,
	NetworkTransformBase__ctor_m65620375EB0F2525064390026555E29217B15B34,
	NetworkTransformBase_MirrorProcessed_mD9BD1A601136D645A37A567AA6BF35C245D4FB3F,
	NetworkTransformBase_get_NetworkclientAuthority_mCC851CD9636198F344E5053199E1B5B9C3D2E335,
	NetworkTransformBase_set_NetworkclientAuthority_m05304900053A008C8D858B076CD5B1FC60AFAD40,
	NetworkTransformBase_get_NetworkexcludeOwnerUpdate_mA9A2B57E82D9ACC3922347CA46AD5D94643A87BB,
	NetworkTransformBase_set_NetworkexcludeOwnerUpdate_m3875A9E7C11970F952A03E5F671983BC24233678,
	NetworkTransformBase_get_NetworksyncPosition_mBE4C25CFB4412E5FEBDFAD2AD1B49194DB7DCDB3,
	NetworkTransformBase_set_NetworksyncPosition_mFA708031F5FF4D89FBE9580EF85A90FBBD33058D,
	NetworkTransformBase_get_NetworksyncRotation_m7BDAD8F697638B9B4A03360C7EC9869E75F4ADC0,
	NetworkTransformBase_set_NetworksyncRotation_mA9E6ACC3519C4AC2BA5711EAB870468E02EA096C,
	NetworkTransformBase_get_NetworksyncScale_m6220D5DB18E1CC9B54FEB9F337F5F2842CB329B7,
	NetworkTransformBase_set_NetworksyncScale_mFAAB7AF70F0EFC1F1885E3BDEDBC7CDE700AB52C,
	NetworkTransformBase_get_NetworkinterpolatePosition_mBE161844A81B74706391C290FD9A8C1722D2B08A,
	NetworkTransformBase_set_NetworkinterpolatePosition_m9D512FA43FDFCBB6868A6D8D889D4BB748C76CB1,
	NetworkTransformBase_get_NetworkinterpolateRotation_mD8F4DA7C1180480CE6664587292C04736E38B368,
	NetworkTransformBase_set_NetworkinterpolateRotation_mE63F265FDD48BD39D9AC4DB49A7496AD1F277807,
	NetworkTransformBase_get_NetworkinterpolateScale_mF0C5D7BFC4CD0DF1C7C40C8E26B56EF94883E94F,
	NetworkTransformBase_set_NetworkinterpolateScale_mF649A173B8D2A2D07C5D077BFD7CA0C106BFEA58,
	NetworkTransformBase_get_NetworklocalPositionSensitivity_m4170C73A674F2B08C99CB9DFA2BD2AF45FFA38D8,
	NetworkTransformBase_set_NetworklocalPositionSensitivity_m2E52EB05AFFA33B013B2A1D9B876B4676294DCA7,
	NetworkTransformBase_get_NetworklocalRotationSensitivity_mA356ADCE7DB71CDE2DAD445E2479A76C1213DA66,
	NetworkTransformBase_set_NetworklocalRotationSensitivity_mEF44DFC9FC3BFEACC9038AB0E1F04C6B9473F60F,
	NetworkTransformBase_get_NetworklocalScaleSensitivity_m259A702CB6E3DAE97697E10D6D5D7A17CD967AB0,
	NetworkTransformBase_set_NetworklocalScaleSensitivity_m5AD8E522B6FEA85C542D9B20E6B886A5E3B87EA2,
	NetworkTransformBase_UserCode_CmdClientToServerSync__Vector3__UInt32__Vector3_m1F0B88E57AB9F761124EA80E6FB52902E1A607C2,
	NetworkTransformBase_InvokeUserCode_CmdClientToServerSync__Vector3__UInt32__Vector3_mB953618B6A1826E4587A4B7DED6FEA5718A7BDB3,
	NetworkTransformBase_UserCode_RpcMove__Vector3__UInt32__Vector3_m3C5C91D6C4A5B719784372ADC5C8D84CDC5D1BDE,
	NetworkTransformBase_InvokeUserCode_RpcMove__Vector3__UInt32__Vector3_mC60C3534DB40A7A54DDF5186D304E2A7B27920A7,
	NetworkTransformBase_UserCode_RpcTeleport__Vector3__UInt32__Boolean_m0E4572551184F488D9F1B0B7F79751D4F621E9FE,
	NetworkTransformBase_InvokeUserCode_RpcTeleport__Vector3__UInt32__Boolean_mEE82A2521701CB5F74247CC6833CA59063ADA2D4,
	NetworkTransformBase_UserCode_CmdTeleportFinished_m4FB55893D9E380F337DC436DAB6009B63C0530BF,
	NetworkTransformBase_InvokeUserCode_CmdTeleportFinished_m23D03D021DC54EE303EFCFCB590EA48E0162A8DD,
	NetworkTransformBase__cctor_mFCF26DBAF21508A966C1D422DA33B611D9C8FAC4,
	NetworkTransformBase_SerializeSyncVars_m80B56E89AEEEC2291412E4D154B001122304126A,
	NetworkTransformBase_DeserializeSyncVars_m617E2DEB847D9A263254FC4FC41DB79C77E49C33,
	DataPoint_get_isValid_m89E5E486FBD3EF2F8094C64B18D4E52F0AABC25C,
	NetworkTransformChild_get_targetTransform_mC9E43B03EB9F9F5BE79A75DE29D9743BC707228D,
	NetworkTransformChild__ctor_mB51C8BF137FEA223AECCFCC03F5D32506178B4F0,
	NetworkTransformChild_MirrorProcessed_m58BC2D4978E5DF300B7F4F29FA41B6DAC5D1FE5F,
	ServerFoundUnityEvent__ctor_m94727746B8368497DCBF89704D475C29027B6FDA,
	NetworkDiscovery_get_ServerId_m7CACB9535680796E7E84F7269FA086071D4A31BA,
	NetworkDiscovery_set_ServerId_mBD7B0DEAE754513530C99244F76F3CD58CAF4E9F,
	NetworkDiscovery_Start_m00F61409BF0EECE9E4866742B47CF12DB83CC8DB,
	NetworkDiscovery_ProcessRequest_m0A23AA045FFE7B73B3DCF611E56463FA038F7572,
	NetworkDiscovery_GetRequest_m766AC1A5F4FC24955B640EBF11F64C0B03321999,
	NetworkDiscovery_ProcessResponse_m8E66484218C817247C2815D6E9AA0152586D79C1,
	NetworkDiscovery__ctor_m38F5C2816F38111FDCD8ECE3705F392299CC1292,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkDiscoveryHUD_OnGUI_m64F2C1FDE4446BCA975D7466504E5AFBC2338FFD,
	NetworkDiscoveryHUD_DrawGUI_m8F607644FFD39F9504295E06B0573515CDC61835,
	NetworkDiscoveryHUD_StopButtons_m974CC76A233D560105C90A2C11970CA01CB3AF42,
	NetworkDiscoveryHUD_Connect_m83903FD713C4576A22E0290FE4E50AAE265C8357,
	NetworkDiscoveryHUD_OnDiscoveredServer_m2AF940684E8C0798798D904B8403C80748EC6BFA,
	NetworkDiscoveryHUD__ctor_mB8B12A6BADDEC862DA48B514EE141C1D070C437C,
	ServerResponse_get_EndPoint_mBFDE2455B6E2DC58E357C27D23D58D76694CB35D,
	ServerResponse_set_EndPoint_m12FC283447C2EB408A52D7EA1510173763CB8463,
	GeneratedNetworkCode__Read_Mirror_ReadyMessage_m06480E84CC3B19B04E6573A08D1CEDC68E2B2C50,
	GeneratedNetworkCode__Write_Mirror_ReadyMessage_m1ABF2F5BB5DF55AF44620112E5FB6AC4410D7DCC,
	GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m561BF5BCE9FC86C6F37D8699FC75EB83E769C239,
	GeneratedNetworkCode__Write_Mirror_NotReadyMessage_m2C3071681A4D7171B21B58B252EA553E85C57CDB,
	GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_mF53BB66F92BDD045E279B3395ADB22E544164742,
	GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_m252F2DFAAC1A40BB906513ED762CC227601BF6F5,
	GeneratedNetworkCode__Read_Mirror_SceneMessage_m8842C2807940A70FAC96357C2DA921D0A881E6F8,
	GeneratedNetworkCode__Read_Mirror_SceneOperation_m6EF9BACF8BC61D214F0B7687DB375556D58BD0B5,
	GeneratedNetworkCode__Write_Mirror_SceneMessage_m661773F482F43CD62A9A787FCFD4768AB3ECD59B,
	GeneratedNetworkCode__Write_Mirror_SceneOperation_mBF55AECA974A3FD1ACE96CBEA7513205A820A78B,
	GeneratedNetworkCode__Read_Mirror_CommandMessage_m2506B9CDD17789E721D0CCC2F6DB1A2428A1C3B9,
	GeneratedNetworkCode__Write_Mirror_CommandMessage_m9751EAE9DBDC058EE20ACD3CDFFCE8AA86A52EEB,
	GeneratedNetworkCode__Read_Mirror_RpcMessage_mF12E2594E03EBD1A674D892F6ED4974A93D42F2E,
	GeneratedNetworkCode__Write_Mirror_RpcMessage_mB7AD2C90A88C6FB1625352488BF3A74AEE60D901,
	GeneratedNetworkCode__Read_Mirror_SpawnMessage_m4D488884D64FCFEE1EC076E9CF8BBA442FDF8014,
	GeneratedNetworkCode__Write_Mirror_SpawnMessage_m994E303D1012261A96D8459337314BAC322E27C7,
	GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_mB76DF471DFB06E15D34400220CC8D42A8AF5FF25,
	GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m2D15284E263AADFB70FA3A5A5BD409A7FB759DCF,
	GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_m112EE6BB7CA29C9AA118942B15784B907D2368FE,
	GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m49A85D44721088FCF11FADA6B439917931BEA0F9,
	GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_mCEC6B1996601932F124D9CBB8ADD4682EB5FF99A,
	GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_mC120FD8DF8C4E9D1E054D60F5BA203C1222F2255,
	GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_mCB0FBE61E22F8AC1880E1CDFD12B0BEF29537D5F,
	GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_mFEED939D871503CBCFFE9A27839E8F0E6F23FC1F,
	GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_mE7900C1AEBECD02903094AD790504D9AD7AAEB93,
	GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_m4420F0EFA51E36569D0CCE98CBD6D4DF0CE0A60D,
	GeneratedNetworkCode__Read_Mirror_EntityStateMessage_m01CECE9E01DDE00D8F54A9F911E79D50526F5219,
	GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m328E999051FAE859E8612DAFC86A845071ABB5C6,
	GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m1D1484F1E38A57BA020BD6F677E260EC8E0F4DB7,
	GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_m06ADA370CB0B3921F240FFA14F67ED8559B550F8,
	GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_m49244A6CDEE5A502776FB107388190F5AD11A4A4,
	GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_m7B710FB7E82A39893692BFF8CB962A6BFC7A93D2,
	GeneratedNetworkCode__Read_Mirror_Discovery_ServerRequest_mB9AE535D408EC75C5111E6965EF6435A5F6FAF67,
	GeneratedNetworkCode__Write_Mirror_Discovery_ServerRequest_mAEE89796A010BB37C20E8EE17EB56B75225500FD,
	GeneratedNetworkCode__Read_Mirror_Discovery_ServerResponse_mA45BA115B8C1DE768E9EA1AA575F40E1011EFF0F,
	GeneratedNetworkCode__Write_Mirror_Discovery_ServerResponse_m81E05EF9031925687BA7F6118029B50B26B9E793,
	GeneratedNetworkCode_InitReadWriters_m069419D2444914F86D93F89136A93BBB6CD55191,
};
extern void LogEntry__ctor_m91CDC82DC8D2111C6413CFED125B90BA0621AED7_AdjustorThunk (void);
extern void NTSnapshot_get_remoteTimestamp_m1AA923C8C9FF1F473CEECEAC7AB1BFB887999BBE_AdjustorThunk (void);
extern void NTSnapshot_set_remoteTimestamp_mB571444062A9349310C12DEBCF202DF5E6D90038_AdjustorThunk (void);
extern void NTSnapshot_get_localTimestamp_mB4EB78DF581B393267C3B8B9E922D83B9388E4FE_AdjustorThunk (void);
extern void NTSnapshot_set_localTimestamp_mDE387C34337B2611340C05E048A3BAD0832E7111_AdjustorThunk (void);
extern void NTSnapshot__ctor_mD163416E3EC45F2AC30B3BF21C603EFCDF6A2D1B_AdjustorThunk (void);
extern void DataPoint_get_isValid_m89E5E486FBD3EF2F8094C64B18D4E52F0AABC25C_AdjustorThunk (void);
extern void ServerResponse_get_EndPoint_mBFDE2455B6E2DC58E357C27D23D58D76694CB35D_AdjustorThunk (void);
extern void ServerResponse_set_EndPoint_m12FC283447C2EB408A52D7EA1510173763CB8463_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[9] = 
{
	{ 0x06000001, LogEntry__ctor_m91CDC82DC8D2111C6413CFED125B90BA0621AED7_AdjustorThunk },
	{ 0x06000102, NTSnapshot_get_remoteTimestamp_m1AA923C8C9FF1F473CEECEAC7AB1BFB887999BBE_AdjustorThunk },
	{ 0x06000103, NTSnapshot_set_remoteTimestamp_mB571444062A9349310C12DEBCF202DF5E6D90038_AdjustorThunk },
	{ 0x06000104, NTSnapshot_get_localTimestamp_mB4EB78DF581B393267C3B8B9E922D83B9388E4FE_AdjustorThunk },
	{ 0x06000105, NTSnapshot_set_localTimestamp_mDE387C34337B2611340C05E048A3BAD0832E7111_AdjustorThunk },
	{ 0x06000106, NTSnapshot__ctor_mD163416E3EC45F2AC30B3BF21C603EFCDF6A2D1B_AdjustorThunk },
	{ 0x060001C3, DataPoint_get_isValid_m89E5E486FBD3EF2F8094C64B18D4E52F0AABC25C_AdjustorThunk },
	{ 0x060001F3, ServerResponse_get_EndPoint_mBFDE2455B6E2DC58E357C27D23D58D76694CB35D_AdjustorThunk },
	{ 0x060001F4, ServerResponse_set_EndPoint_m12FC283447C2EB408A52D7EA1510173763CB8463_AdjustorThunk },
};
static const int32_t s_InvokerIndices[537] = 
{
	3056,
	6908,
	1610,
	6908,
	6908,
	6908,
	4648,
	6908,
	1922,
	3068,
	6908,
	6908,
	6908,
	6908,
	5521,
	5521,
	6908,
	2516,
	1580,
	5479,
	1922,
	3068,
	6908,
	6908,
	6908,
	5521,
	5521,
	6908,
	5578,
	1922,
	3068,
	6908,
	0,
	0,
	0,
	0,
	0,
	6727,
	5117,
	1922,
	3068,
	6908,
	6908,
	6908,
	6908,
	6908,
	5521,
	5521,
	6908,
	3068,
	1613,
	5521,
	1922,
	3068,
	5521,
	6908,
	6666,
	6908,
	6908,
	6908,
	3150,
	1192,
	6908,
	534,
	5521,
	534,
	5521,
	5485,
	5485,
	6892,
	1915,
	5521,
	1915,
	3041,
	5521,
	5485,
	5521,
	5485,
	534,
	5521,
	5485,
	5485,
	5591,
	534,
	5521,
	5485,
	5485,
	6908,
	3982,
	6908,
	6836,
	5591,
	534,
	9086,
	5521,
	9086,
	5485,
	9086,
	5485,
	9086,
	5591,
	9086,
	534,
	9086,
	5521,
	9086,
	5485,
	9086,
	5485,
	9086,
	12362,
	1915,
	3041,
	6908,
	6908,
	6908,
	6908,
	6908,
	6666,
	5418,
	6908,
	6908,
	5521,
	3068,
	6908,
	6908,
	6908,
	5521,
	5521,
	5521,
	6908,
	5521,
	5521,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	5521,
	5521,
	5521,
	4903,
	2383,
	5521,
	1250,
	6908,
	6908,
	6908,
	6908,
	6908,
	5521,
	6908,
	5521,
	6908,
	6908,
	6908,
	5521,
	6908,
	6908,
	6908,
	12362,
	6908,
	3670,
	6908,
	6908,
	5418,
	2769,
	2487,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6666,
	5418,
	6727,
	5485,
	5418,
	9086,
	12362,
	1915,
	3041,
	6908,
	6908,
	2451,
	2451,
	1486,
	1486,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6765,
	6908,
	6908,
	6666,
	0,
	6836,
	6757,
	1560,
	3978,
	1430,
	1430,
	1430,
	1430,
	6908,
	6908,
	6908,
	5650,
	3188,
	5650,
	3188,
	3188,
	5650,
	3188,
	3188,
	6908,
	6908,
	6908,
	6908,
	1915,
	3041,
	6908,
	6908,
	1430,
	9086,
	1430,
	9086,
	5650,
	9086,
	3188,
	9086,
	3188,
	9086,
	5650,
	9086,
	3188,
	9086,
	3188,
	9086,
	12362,
	6765,
	6908,
	6908,
	6692,
	5449,
	6692,
	5449,
	515,
	8709,
	6666,
	6666,
	6908,
	6908,
	6908,
	6908,
	3190,
	6908,
	6908,
	6908,
	6898,
	5650,
	6898,
	5650,
	3190,
	9086,
	12362,
	1915,
	3041,
	6908,
	6666,
	6666,
	3190,
	3190,
	2487,
	2487,
	3150,
	3150,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	5650,
	3190,
	5418,
	5418,
	5591,
	5591,
	6908,
	6908,
	6898,
	5650,
	6898,
	5650,
	6666,
	5418,
	6666,
	5418,
	6836,
	5591,
	6836,
	5591,
	5650,
	9086,
	3190,
	9086,
	5418,
	9086,
	5418,
	9086,
	5591,
	9086,
	5591,
	9086,
	12362,
	1915,
	3041,
	6908,
	6908,
	6666,
	6666,
	3184,
	3150,
	2487,
	3150,
	3150,
	3150,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	5648,
	3183,
	5418,
	5591,
	5591,
	5591,
	6908,
	6908,
	6896,
	5648,
	6836,
	5591,
	6666,
	5418,
	6836,
	5591,
	6836,
	5591,
	6836,
	5591,
	5648,
	9086,
	3183,
	9086,
	5418,
	9086,
	5591,
	9086,
	5591,
	9086,
	5591,
	9086,
	12362,
	1915,
	3041,
	6908,
	6765,
	6908,
	6908,
	0,
	6666,
	6908,
	6908,
	6908,
	6908,
	6666,
	6666,
	6666,
	6666,
	6666,
	1699,
	1699,
	1697,
	8211,
	1697,
	1408,
	1383,
	1408,
	9793,
	5650,
	3188,
	3188,
	1698,
	6908,
	6908,
	10180,
	9134,
	6908,
	6908,
	6666,
	5418,
	6666,
	5418,
	6666,
	5418,
	6666,
	5418,
	6666,
	5418,
	6666,
	5418,
	6666,
	5418,
	6666,
	5418,
	6836,
	5591,
	6836,
	5591,
	6836,
	5591,
	1699,
	9086,
	1699,
	9086,
	1698,
	9086,
	6908,
	9086,
	12362,
	1915,
	3041,
	6666,
	6765,
	6908,
	6908,
	6908,
	6728,
	5486,
	6908,
	2407,
	6830,
	3145,
	6908,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	6908,
	6908,
	6908,
	5585,
	5585,
	6908,
	6765,
	5521,
	11262,
	10115,
	11165,
	10105,
	10939,
	10082,
	11302,
	10965,
	10122,
	10083,
	11010,
	10088,
	11281,
	10118,
	11334,
	10127,
	10999,
	10084,
	11245,
	10110,
	11244,
	10109,
	11242,
	10107,
	11243,
	10108,
	11052,
	10091,
	11163,
	10103,
	11164,
	10104,
	11304,
	10124,
	11305,
	10125,
	12362,
};
static const Il2CppTokenRangePair s_rgctxIndices[6] = 
{
	{ 0x02000009, { 0, 21 } },
	{ 0x02000026, { 21, 17 } },
	{ 0x02000027, { 38, 3 } },
	{ 0x02000028, { 41, 4 } },
	{ 0x02000029, { 45, 2 } },
	{ 0x0200002A, { 47, 4 } },
};
extern const uint32_t g_rgctx_Dictionary_2_t202067BC8A67DFAFC55842E8BA751E40E987B978;
extern const uint32_t g_rgctx_Dictionary_2_TryGetValue_m834E95F03205017B9803513E330D285430C9808A;
extern const uint32_t g_rgctx_HashSet_1_tD6C8D263E3339386BB7728E9759572FA863E61EC;
extern const uint32_t g_rgctx_HashSet_1__ctor_m82B3870B0893DC5F536A6D840DEC597440724E05;
extern const uint32_t g_rgctx_Dictionary_2_set_Item_mAE1AF16574A6DCE174E405DF99B3E8F720AB4C70;
extern const uint32_t g_rgctx_HashSet_1_Add_mDE721A489FB48AEA720EC6538129E22FE1F1B72B;
extern const uint32_t g_rgctx_HashSet_1_GetEnumerator_m0D9F02CAE1DF3B4CB202F5FD8C1566732BBFEEA2;
extern const uint32_t g_rgctx_Enumerator_get_Current_m58839FAC0E557ED1935977247C70F5B853CADEC7;
extern const uint32_t g_rgctx_Enumerator_MoveNext_mAF067D73EE6BFDF1841D69970ACD024B5368A96C;
extern const uint32_t g_rgctx_Enumerator_t80564832614C8DB78949D420DA4CD4D97A9120C1;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_t80564832614C8DB78949D420DA4CD4D97A9120C1_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_HashSet_1_Clear_m19D5FC74F537C03C10323E886138C5C75FABC388;
extern const uint32_t g_rgctx_Grid2D_1_GetAt_mE542681EFED237560D7C99D2297574882427BFA0;
extern const uint32_t g_rgctx_Dictionary_2_get_Values_mCF6E4D5EA04FD3A003828A97C825C9E2E51EC430;
extern const uint32_t g_rgctx_ValueCollection_t5EB4BBDBA4F24A9BF769A0E85ACF9FBF7B302775;
extern const uint32_t g_rgctx_ValueCollection_GetEnumerator_mFBA9D7F4DD59C28D201D48BB2DBF3270A551E846;
extern const uint32_t g_rgctx_Enumerator_get_Current_mADCC52B3A1F061061AFE386B9890A354AE7C491D;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m8641CBDADFD8CCCE61542A9190EB8D742BCAE3F8;
extern const uint32_t g_rgctx_Enumerator_t4A76D2C8D90B4F994348C8E60EFB24C525303953;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_t4A76D2C8D90B4F994348C8E60EFB24C525303953_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_Dictionary_2__ctor_m88CE238EE1AC8EA42944D74683CF28BB7252F644;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_Shutdown_m78BE8756AD75206B217D0BFECCCDBFB3A0C54373;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_EndpMulticastLock_m636A1A2E45A92932EE5C1EB55A9B7498A50156A6;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_get_SupportedOnThisPlatform_m5BD5EA3796B77C99D75A04E86225A09E10214509;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_tDD9DBAEDBA57EC0C4068170759457B38ACED5E6F;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_StopDiscovery_m6F05D6C3D31560C53509E268D4517C5321D13C58;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_ServerListenAsync_m0553F95D9679CB1762B4FF5F6652E349F2A0DDD4;
extern const uint32_t g_rgctx_AsyncTaskMethodBuilder_Start_TisU3CServerListenAsyncU3Ed__15_tA5B69EE86C52F2AC903135D25BAB65C83E4C897D_mC2A4FF075F40241C2ACE549D3EA34FCD8EE647B4;
extern const uint32_t g_rgctx_AsyncTaskMethodBuilder_Start_TisU3CReceiveRequestAsyncU3Ed__16_tFE71B8CE6B1CE644BEDE2A00558BDE5BF76642E3_mBCBFA1DAFEF853641F17B13A25C050549ACAE141;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_tDD9DBAEDBA57EC0C4068170759457B38ACED5E6F;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_ProcessRequest_mE9A953DC846C1031DC20149CB1DEC8E8F8F1C520;
extern const uint32_t g_rgctx_Response_tD1F981BA1C1ACD2D9C6D46E38FE6669B1A42F547;
extern const uint32_t g_rgctx_NetworkWriter_Write_TisResponse_tD1F981BA1C1ACD2D9C6D46E38FE6669B1A42F547_m354863DB919474D496B449860984D5195695493B;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_ClientListenAsync_m5C670A206D1E9B62B09A801CCC866E0ADEA11D34;
extern const uint32_t g_rgctx_AsyncTaskMethodBuilder_Start_TisU3CClientListenAsyncU3Ed__23_tC6830A4F9388C20AEF4C43489796879D3E9E62D7_m8421DB748E2E00AE759EE974785AACCA7AA244DD;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_GetRequest_m688FE177E81C60EC834FFA58AA7DA257A234BFBC;
extern const uint32_t g_rgctx_NetworkWriter_Write_TisRequest_t3B478170C934EBA5BF13DD52FB50E03246AEAF4C_m1C638C17A6074E3F06DD242D379087920EB1D861;
extern const uint32_t g_rgctx_AsyncTaskMethodBuilder_Start_TisU3CReceiveGameBroadcastAsyncU3Ed__26_tED26DB3264A39F95F609A35C366A675528D3196C_mAFFF32CAE3EE9B74C10EAC04C72FA03240B9F06A;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_BeginMulticastLock_m1B6B3C5949046A8CA4CA313EFD075DD8D40CAB11;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_ReceiveRequestAsync_mE3398D16C39FE94B7E5527893502CB20A1FE020A;
extern const uint32_t g_rgctx_AsyncTaskMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t9B661AC8C2EFA6BAB94C77BB24A5DDA82D61F833_TisU3CServerListenAsyncU3Ed__15_tB98CD89A2EAD036CA798B1036A7C6F9A1ED07E9D_m9C0F0B1B3DA8CAC83AAF65F462141F1481E09AA4;
extern const uint32_t g_rgctx_AsyncTaskMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_1_tA5F6339168CD98D4324E4938379288C303ABF23A_TisU3CReceiveRequestAsyncU3Ed__16_tF6B969C4FE690A499CB116CAD07DEADC70E312EC_m8FA658F040C4FFF9493567242C68CC969A180568;
extern const uint32_t g_rgctx_NetworkReader_Read_TisRequest_tE5184BB38AA572F86EB462DA3371F7D15167AD65_mB68F5F1E34F6A8D417FA37A5E7138FBEA4B5DBFC;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_t5D08D31384256F6D5F8310CA9971982C075E83CF;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_ProcessClientRequest_mE35AD800F65976653325BDB63EF39AABE03143A0;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_ReceiveGameBroadcastAsync_m5EE392A2C6A22BB1F5EA93EEF72D3988F01F4679;
extern const uint32_t g_rgctx_AsyncTaskMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t9B661AC8C2EFA6BAB94C77BB24A5DDA82D61F833_TisU3CClientListenAsyncU3Ed__23_t6940E39D9EF6F0A0333254550ADC7DE0DB1E5F0A_m78D7B8BDC4604EDD010EB8D5CEFED3033C495CB6;
extern const uint32_t g_rgctx_AsyncTaskMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_1_tA5F6339168CD98D4324E4938379288C303ABF23A_TisU3CReceiveGameBroadcastAsyncU3Ed__26_tA0CD3E430EE42934438EF75D80FD07378F016AC7_m39C6F7D59F380C696C51CF86178E672C53790509;
extern const uint32_t g_rgctx_NetworkReader_Read_TisResponse_tC4A5C54F9BFB89DC0A7239ECE192D1C553F8E9C9_m8977778774CD07035EDBD572743B13EC0D24221B;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_t814C411B352638BB18457B11FA7BA20545283927;
extern const uint32_t g_rgctx_NetworkDiscoveryBase_2_ProcessResponse_mB3B7AC26FAC835D53143602ACB923F09A574C26F;
static const Il2CppRGCTXDefinition s_rgctxValues[51] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_t202067BC8A67DFAFC55842E8BA751E40E987B978 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_TryGetValue_m834E95F03205017B9803513E330D285430C9808A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_HashSet_1_tD6C8D263E3339386BB7728E9759572FA863E61EC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HashSet_1__ctor_m82B3870B0893DC5F536A6D840DEC597440724E05 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_set_Item_mAE1AF16574A6DCE174E405DF99B3E8F720AB4C70 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HashSet_1_Add_mDE721A489FB48AEA720EC6538129E22FE1F1B72B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HashSet_1_GetEnumerator_m0D9F02CAE1DF3B4CB202F5FD8C1566732BBFEEA2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_m58839FAC0E557ED1935977247C70F5B853CADEC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_mAF067D73EE6BFDF1841D69970ACD024B5368A96C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t80564832614C8DB78949D420DA4CD4D97A9120C1 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_t80564832614C8DB78949D420DA4CD4D97A9120C1_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HashSet_1_Clear_m19D5FC74F537C03C10323E886138C5C75FABC388 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Grid2D_1_GetAt_mE542681EFED237560D7C99D2297574882427BFA0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_get_Values_mCF6E4D5EA04FD3A003828A97C825C9E2E51EC430 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ValueCollection_t5EB4BBDBA4F24A9BF769A0E85ACF9FBF7B302775 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ValueCollection_GetEnumerator_mFBA9D7F4DD59C28D201D48BB2DBF3270A551E846 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_mADCC52B3A1F061061AFE386B9890A354AE7C491D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m8641CBDADFD8CCCE61542A9190EB8D742BCAE3F8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t4A76D2C8D90B4F994348C8E60EFB24C525303953 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_t4A76D2C8D90B4F994348C8E60EFB24C525303953_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2__ctor_m88CE238EE1AC8EA42944D74683CF28BB7252F644 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_Shutdown_m78BE8756AD75206B217D0BFECCCDBFB3A0C54373 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_EndpMulticastLock_m636A1A2E45A92932EE5C1EB55A9B7498A50156A6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_get_SupportedOnThisPlatform_m5BD5EA3796B77C99D75A04E86225A09E10214509 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NetworkDiscoveryBase_2_tDD9DBAEDBA57EC0C4068170759457B38ACED5E6F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_StopDiscovery_m6F05D6C3D31560C53509E268D4517C5321D13C58 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_ServerListenAsync_m0553F95D9679CB1762B4FF5F6652E349F2A0DDD4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncTaskMethodBuilder_Start_TisU3CServerListenAsyncU3Ed__15_tA5B69EE86C52F2AC903135D25BAB65C83E4C897D_mC2A4FF075F40241C2ACE549D3EA34FCD8EE647B4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncTaskMethodBuilder_Start_TisU3CReceiveRequestAsyncU3Ed__16_tFE71B8CE6B1CE644BEDE2A00558BDE5BF76642E3_mBCBFA1DAFEF853641F17B13A25C050549ACAE141 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NetworkDiscoveryBase_2_tDD9DBAEDBA57EC0C4068170759457B38ACED5E6F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_ProcessRequest_mE9A953DC846C1031DC20149CB1DEC8E8F8F1C520 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Response_tD1F981BA1C1ACD2D9C6D46E38FE6669B1A42F547 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkWriter_Write_TisResponse_tD1F981BA1C1ACD2D9C6D46E38FE6669B1A42F547_m354863DB919474D496B449860984D5195695493B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_ClientListenAsync_m5C670A206D1E9B62B09A801CCC866E0ADEA11D34 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncTaskMethodBuilder_Start_TisU3CClientListenAsyncU3Ed__23_tC6830A4F9388C20AEF4C43489796879D3E9E62D7_m8421DB748E2E00AE759EE974785AACCA7AA244DD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_GetRequest_m688FE177E81C60EC834FFA58AA7DA257A234BFBC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkWriter_Write_TisRequest_t3B478170C934EBA5BF13DD52FB50E03246AEAF4C_m1C638C17A6074E3F06DD242D379087920EB1D861 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncTaskMethodBuilder_Start_TisU3CReceiveGameBroadcastAsyncU3Ed__26_tED26DB3264A39F95F609A35C366A675528D3196C_mAFFF32CAE3EE9B74C10EAC04C72FA03240B9F06A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_BeginMulticastLock_m1B6B3C5949046A8CA4CA313EFD075DD8D40CAB11 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_ReceiveRequestAsync_mE3398D16C39FE94B7E5527893502CB20A1FE020A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncTaskMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t9B661AC8C2EFA6BAB94C77BB24A5DDA82D61F833_TisU3CServerListenAsyncU3Ed__15_tB98CD89A2EAD036CA798B1036A7C6F9A1ED07E9D_m9C0F0B1B3DA8CAC83AAF65F462141F1481E09AA4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncTaskMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_1_tA5F6339168CD98D4324E4938379288C303ABF23A_TisU3CReceiveRequestAsyncU3Ed__16_tF6B969C4FE690A499CB116CAD07DEADC70E312EC_m8FA658F040C4FFF9493567242C68CC969A180568 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkReader_Read_TisRequest_tE5184BB38AA572F86EB462DA3371F7D15167AD65_mB68F5F1E34F6A8D417FA37A5E7138FBEA4B5DBFC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NetworkDiscoveryBase_2_t5D08D31384256F6D5F8310CA9971982C075E83CF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_ProcessClientRequest_mE35AD800F65976653325BDB63EF39AABE03143A0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_ReceiveGameBroadcastAsync_m5EE392A2C6A22BB1F5EA93EEF72D3988F01F4679 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncTaskMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t9B661AC8C2EFA6BAB94C77BB24A5DDA82D61F833_TisU3CClientListenAsyncU3Ed__23_t6940E39D9EF6F0A0333254550ADC7DE0DB1E5F0A_m78D7B8BDC4604EDD010EB8D5CEFED3033C495CB6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncTaskMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_1_tA5F6339168CD98D4324E4938379288C303ABF23A_TisU3CReceiveGameBroadcastAsyncU3Ed__26_tA0CD3E430EE42934438EF75D80FD07378F016AC7_m39C6F7D59F380C696C51CF86178E672C53790509 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkReader_Read_TisResponse_tC4A5C54F9BFB89DC0A7239ECE192D1C553F8E9C9_m8977778774CD07035EDBD572743B13EC0D24221B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NetworkDiscoveryBase_2_t814C411B352638BB18457B11FA7BA20545283927 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiscoveryBase_2_ProcessResponse_mB3B7AC26FAC835D53143602ACB923F09A574C26F },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Mirror_Components_CodeGenModule;
const Il2CppCodeGenModule g_Mirror_Components_CodeGenModule = 
{
	"Mirror.Components.dll",
	537,
	s_methodPointers,
	9,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	6,
	s_rgctxIndices,
	51,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

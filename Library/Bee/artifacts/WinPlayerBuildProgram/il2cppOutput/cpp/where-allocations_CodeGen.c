﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Int32 WhereAllocation.Extensions::ReceiveFrom_NonAlloc(System.Net.Sockets.Socket,System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,WhereAllocation.IPEndPointNonAlloc)
extern void Extensions_ReceiveFrom_NonAlloc_m24C1633B683DA049F306E29EB04F76FBE77DB166 (void);
// 0x00000002 System.Int32 WhereAllocation.Extensions::ReceiveFrom_NonAlloc(System.Net.Sockets.Socket,System.Byte[],WhereAllocation.IPEndPointNonAlloc)
extern void Extensions_ReceiveFrom_NonAlloc_m4ECD8A50A627E1032FE7D94802FE8C6671A5659F (void);
// 0x00000003 System.Int32 WhereAllocation.Extensions::SendTo_NonAlloc(System.Net.Sockets.Socket,System.Byte[],System.Int32,System.Int32,System.Net.Sockets.SocketFlags,WhereAllocation.IPEndPointNonAlloc)
extern void Extensions_SendTo_NonAlloc_m181E39E48138857E2B613410E1E83B467F8ED1A8 (void);
// 0x00000004 System.Void WhereAllocation.IPEndPointNonAlloc::.ctor(System.Int64,System.Int32)
extern void IPEndPointNonAlloc__ctor_mC29EBF9DC4AAF7DCAB009C442ADAAA7DA7394350 (void);
// 0x00000005 System.Void WhereAllocation.IPEndPointNonAlloc::.ctor(System.Net.IPAddress,System.Int32)
extern void IPEndPointNonAlloc__ctor_mA351C8D192AFF8FF435AFD30C6641A849179FF9B (void);
// 0x00000006 System.Net.SocketAddress WhereAllocation.IPEndPointNonAlloc::Serialize()
extern void IPEndPointNonAlloc_Serialize_m6F9853D1258D5B8AF339A5F52B3F2C413C43DA94 (void);
// 0x00000007 System.Net.EndPoint WhereAllocation.IPEndPointNonAlloc::Create(System.Net.SocketAddress)
extern void IPEndPointNonAlloc_Create_m7AC6EA965FF3F4CB98CB0C5D2345355CAFB2418C (void);
// 0x00000008 System.Int32 WhereAllocation.IPEndPointNonAlloc::GetHashCode()
extern void IPEndPointNonAlloc_GetHashCode_m50DBB4CE6596E6F3D387A1144BE8241209E55F78 (void);
// 0x00000009 System.Net.IPEndPoint WhereAllocation.IPEndPointNonAlloc::DeepCopyIPEndPoint()
extern void IPEndPointNonAlloc_DeepCopyIPEndPoint_m61ECB5E032DB173F3E0E9EEB9912768F18DDE395 (void);
static Il2CppMethodPointer s_methodPointers[9] = 
{
	Extensions_ReceiveFrom_NonAlloc_m24C1633B683DA049F306E29EB04F76FBE77DB166,
	Extensions_ReceiveFrom_NonAlloc_m4ECD8A50A627E1032FE7D94802FE8C6671A5659F,
	Extensions_SendTo_NonAlloc_m181E39E48138857E2B613410E1E83B467F8ED1A8,
	IPEndPointNonAlloc__ctor_mC29EBF9DC4AAF7DCAB009C442ADAAA7DA7394350,
	IPEndPointNonAlloc__ctor_mA351C8D192AFF8FF435AFD30C6641A849179FF9B,
	IPEndPointNonAlloc_Serialize_m6F9853D1258D5B8AF339A5F52B3F2C413C43DA94,
	IPEndPointNonAlloc_Create_m7AC6EA965FF3F4CB98CB0C5D2345355CAFB2418C,
	IPEndPointNonAlloc_GetHashCode_m50DBB4CE6596E6F3D387A1144BE8241209E55F78,
	IPEndPointNonAlloc_DeepCopyIPEndPoint_m61ECB5E032DB173F3E0E9EEB9912768F18DDE395,
};
static const int32_t s_InvokerIndices[9] = 
{
	7306,
	8652,
	7306,
	2989,
	3056,
	6765,
	4903,
	6727,
	6765,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_whereU2Dallocations_CodeGenModule;
const Il2CppCodeGenModule g_whereU2Dallocations_CodeGenModule = 
{
	"where-allocations.dll",
	9,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void LangListObjects::.ctor()
extern void LangListObjects__ctor_mF9FA24FE1D0E85C955BC351AD9DDF37B2FADD43F (void);
// 0x00000002 System.Void WordKey::.ctor()
extern void WordKey__ctor_m755A833309D2357BC9DE87F55C05A4DB868276C6 (void);
// 0x00000003 System.Void Word::.ctor()
extern void Word__ctor_mB7EBB67548188CEDCD7A2676962409B5AB3A9FB5 (void);
// 0x00000004 System.Void Word::.ctor(WordKey)
extern void Word__ctor_m47F087DD6ED2B3E44CC4E236BB441CBB5384B6E9 (void);
// 0x00000005 System.Void LangPhrase::.ctor()
extern void LangPhrase__ctor_m8F6F42B1DA6959D603220B6181730F40B0CC69EE (void);
// 0x00000006 System.Void LangsList::SetLanguage(System.Int32)
extern void LangsList_SetLanguage_m9BFC6E45A17C13AC006B6DA5CE70B08CC6934E46 (void);
// 0x00000007 System.Void LangsList::SetLanguage(System.Int32,System.Boolean)
extern void LangsList_SetLanguage_mBCAE8A1D809389D521EA494D38AF1EBF1F74973A (void);
// 0x00000008 System.Void LangsList::Awake()
extern void LangsList_Awake_m2DFC24655432212366B8394830874E2828120DC3 (void);
// 0x00000009 System.String LangsList::GetWord(System.String)
extern void LangsList_GetWord_mE61CAE1AE18637A4C92157234916A2A514BFA82F (void);
// 0x0000000A System.Void LangsList::.ctor()
extern void LangsList__ctor_mD81ED2A1F2A34D86CC35D89EFC6942FE9D03B4CE (void);
// 0x0000000B System.Void LangsList::.cctor()
extern void LangsList__cctor_m2B3315C3B4624739E96D39700F728C46230857C4 (void);
// 0x0000000C System.Boolean LangsList::<Awake>b__7_0(LangsList)
extern void LangsList_U3CAwakeU3Eb__7_0_mC3973477BEB8F4CC3A5139FC537380401C8A54EB (void);
// 0x0000000D System.Void TextTranslator::Start()
extern void TextTranslator_Start_m994F96984B00F79FE154E0434E628039C5D92117 (void);
// 0x0000000E System.Void TextTranslator::OnDestroy()
extern void TextTranslator_OnDestroy_mA4305EC096A06937517E0676CC95763D0E57085A (void);
// 0x0000000F System.Void TextTranslator::ReTranslate()
extern void TextTranslator_ReTranslate_mC01045BAB4375F23F22D492D1520E46AFF1C92F2 (void);
// 0x00000010 System.Void TextTranslator::.ctor()
extern void TextTranslator__ctor_m89912AE4EAA4591319FC6172EC56D9B09754D9EA (void);
// 0x00000011 System.Void AnimStartAndDestroyToTime::Start()
extern void AnimStartAndDestroyToTime_Start_m47A7C8CAC5D20A924250B26FCFB6A74451CC73A6 (void);
// 0x00000012 System.Void AnimStartAndDestroyToTime::Go()
extern void AnimStartAndDestroyToTime_Go_mE9F8A47A5EB1B0F4B53BC587677CA756F8A5ECE4 (void);
// 0x00000013 System.Void AnimStartAndDestroyToTime::Update()
extern void AnimStartAndDestroyToTime_Update_mD4C4F491911922728A7C2B5E7A75F46D0157B12E (void);
// 0x00000014 System.Void AnimStartAndDestroyToTime::Destroy()
extern void AnimStartAndDestroyToTime_Destroy_m14DFF992EC89CD3CC51741060767141038D0C41C (void);
// 0x00000015 System.Void AnimStartAndDestroyToTime::OnBecameVisible()
extern void AnimStartAndDestroyToTime_OnBecameVisible_m6AC05336D2F629FA8B9D1A2ECD2399FB7F85049E (void);
// 0x00000016 System.Void AnimStartAndDestroyToTime::OnBecameInvisible()
extern void AnimStartAndDestroyToTime_OnBecameInvisible_m7FE9D9366A63EDFF658011D633CFFC3DCD7E8484 (void);
// 0x00000017 System.Void AnimStartAndDestroyToTime::CancelAnim()
extern void AnimStartAndDestroyToTime_CancelAnim_m322545E427ECDE50EA2CCD9417264851C902D6BE (void);
// 0x00000018 System.Void AnimStartAndDestroyToTime::.ctor()
extern void AnimStartAndDestroyToTime__ctor_m8BB83363487CCC583D6D26DDF109AA64308F85D1 (void);
// 0x00000019 System.Void AudioPlayer::Start()
extern void AudioPlayer_Start_m1CD4C540B92CF134746EF50084C5729768B93CD8 (void);
// 0x0000001A System.Void AudioPlayer::OnEnable()
extern void AudioPlayer_OnEnable_m2EA09467BDDFA715E7387850815875081F5F6043 (void);
// 0x0000001B System.Void AudioPlayer::OnDisable()
extern void AudioPlayer_OnDisable_mC74844A3C99181607EF9ED53E94790EB11CC93CE (void);
// 0x0000001C System.Void AudioPlayer::Update()
extern void AudioPlayer_Update_m71C498949724E9689C4FDD4D201F06855334D235 (void);
// 0x0000001D System.Void AudioPlayer::SettingsAudioVolume()
extern void AudioPlayer_SettingsAudioVolume_m63CC13B6656A897C0332B996735B7A88AFC28871 (void);
// 0x0000001E System.Void AudioPlayer::Feet()
extern void AudioPlayer_Feet_mCCE9B95ECD13EAD4546A6C0BE7E46E617D7DB084 (void);
// 0x0000001F System.Void AudioPlayer::SoundPlayerSpawn(UnityEngine.GameObject,System.Int32)
extern void AudioPlayer_SoundPlayerSpawn_m5979A9D44DE01824B9EE54D6667E909668901297 (void);
// 0x00000020 System.Void AudioPlayer::SoundPlayerPain(UnityEngine.GameObject,UnityEngine.GameObject,System.Int32)
extern void AudioPlayer_SoundPlayerPain_m8A8E37096EE65A1E5679C694318F23FCC0EA82FB (void);
// 0x00000021 System.Void AudioPlayer::SoundPlayerDeath(UnityEngine.GameObject,System.Int32)
extern void AudioPlayer_SoundPlayerDeath_mDD269B763D280AA408E48804E985DBA5E5011498 (void);
// 0x00000022 System.Void AudioPlayer::SoundPlayerInfection(UnityEngine.GameObject,System.Int32)
extern void AudioPlayer_SoundPlayerInfection_m3CE1982168920BABCA0D6EFDC0BF892799E56399 (void);
// 0x00000023 System.Void AudioPlayer::SoundPlayerAttackFireWeapon(UnityEngine.GameObject,System.Int32)
extern void AudioPlayer_SoundPlayerAttackFireWeapon_m289015323330D6442499CCE9E7A6045315D89CC3 (void);
// 0x00000024 System.Void AudioPlayer::SoundPlayerAttackFireKnife(UnityEngine.GameObject,System.Int32,HitKnife)
extern void AudioPlayer_SoundPlayerAttackFireKnife_m43DF50D8608887354EE798CF83B0D908B51B4348 (void);
// 0x00000025 System.Void AudioPlayer::SoundPlayerStartReloading(UnityEngine.GameObject,System.Int32)
extern void AudioPlayer_SoundPlayerStartReloading_mB5541EAA551DF74D6233D10B4AD3FEEB5D4B07F0 (void);
// 0x00000026 System.Void AudioPlayer::SoundPlayerEndReloading(UnityEngine.GameObject,System.Int32)
extern void AudioPlayer_SoundPlayerEndReloading_m87F5D0952DB39A072C1E719FCEAE0A52A91F4CF8 (void);
// 0x00000027 System.Void AudioPlayer::SoundPlayerStartDraw(UnityEngine.GameObject,System.Int32)
extern void AudioPlayer_SoundPlayerStartDraw_m91F8E746B02B0EBDADBAA591B47373E0CD1FE39D (void);
// 0x00000028 System.Collections.IEnumerator AudioPlayer::SoundPlay(AudioPlayer/AudioPlayerControlSound)
extern void AudioPlayer_SoundPlay_mA77227916A855889C7050672482B7D4969B3CDEA (void);
// 0x00000029 System.Collections.IEnumerator AudioPlayer::SoundPlayKnife(HitKnife)
extern void AudioPlayer_SoundPlayKnife_mDD7E87609E80841A98CF121DD87414ECC0E0D977 (void);
// 0x0000002A System.Void AudioPlayer::.ctor()
extern void AudioPlayer__ctor_m3F81E1F07C97F2ECE2A837B3FB918ECBBCA14B17 (void);
// 0x0000002B System.Void AudioPlayer/<SoundPlay>d__36::.ctor(System.Int32)
extern void U3CSoundPlayU3Ed__36__ctor_m2DE5B8265C28712F6FF5F8D285AA8AF076CFB5B3 (void);
// 0x0000002C System.Void AudioPlayer/<SoundPlay>d__36::System.IDisposable.Dispose()
extern void U3CSoundPlayU3Ed__36_System_IDisposable_Dispose_m011B4ED87B8E9BDCA188A6CD5664B312F2BA4BC4 (void);
// 0x0000002D System.Boolean AudioPlayer/<SoundPlay>d__36::MoveNext()
extern void U3CSoundPlayU3Ed__36_MoveNext_m9F4B721644E47F43D31F13AB5D41CAF1EFC680F5 (void);
// 0x0000002E System.Object AudioPlayer/<SoundPlay>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSoundPlayU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B8A6327E96C1BBA5C4C400B58FC6CC39DCE381C (void);
// 0x0000002F System.Void AudioPlayer/<SoundPlay>d__36::System.Collections.IEnumerator.Reset()
extern void U3CSoundPlayU3Ed__36_System_Collections_IEnumerator_Reset_m9E8010590CC4F4292DA803905DD843F54E3E660A (void);
// 0x00000030 System.Object AudioPlayer/<SoundPlay>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CSoundPlayU3Ed__36_System_Collections_IEnumerator_get_Current_mD284DE0568D4BF807DCE34F54F4E0B0859F73897 (void);
// 0x00000031 System.Void AudioPlayer/<SoundPlayKnife>d__37::.ctor(System.Int32)
extern void U3CSoundPlayKnifeU3Ed__37__ctor_mA22859D48DA881DD6F65C91DC33633014CD71D64 (void);
// 0x00000032 System.Void AudioPlayer/<SoundPlayKnife>d__37::System.IDisposable.Dispose()
extern void U3CSoundPlayKnifeU3Ed__37_System_IDisposable_Dispose_m433752B96784B7C0301599D88ECB41BA89E7C4E0 (void);
// 0x00000033 System.Boolean AudioPlayer/<SoundPlayKnife>d__37::MoveNext()
extern void U3CSoundPlayKnifeU3Ed__37_MoveNext_m7B64730D671619C7D84247B2999EB9376B6DD3CE (void);
// 0x00000034 System.Object AudioPlayer/<SoundPlayKnife>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSoundPlayKnifeU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC999731DE4D94CA0046A5B1AF7B1F919B8B089B (void);
// 0x00000035 System.Void AudioPlayer/<SoundPlayKnife>d__37::System.Collections.IEnumerator.Reset()
extern void U3CSoundPlayKnifeU3Ed__37_System_Collections_IEnumerator_Reset_m171E01904BA8475E3E90A3F5095FB71C6081690B (void);
// 0x00000036 System.Object AudioPlayer/<SoundPlayKnife>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CSoundPlayKnifeU3Ed__37_System_Collections_IEnumerator_get_Current_mA50080FDBDF4AD8ABAA2365E704AC7AF33369DB7 (void);
// 0x00000037 System.Void AudioSignal::Start()
extern void AudioSignal_Start_m15BD61E02483A4F8A7C1605D56668ED7880B9B6C (void);
// 0x00000038 System.Void AudioSignal::OnStartClient()
extern void AudioSignal_OnStartClient_m2FD67489CEF2FFCA66AFBA93B4AEE9D2A297C374 (void);
// 0x00000039 System.Void AudioSignal::OnStartServer()
extern void AudioSignal_OnStartServer_m03050F48682696819B4574110A26AC93CAA31F7A (void);
// 0x0000003A System.Void AudioSignal::Initialization()
extern void AudioSignal_Initialization_m878B52DA70C7430B3EDC78BF16C7231AFDE1FEEA (void);
// 0x0000003B System.Void AudioSignal::OnEnable()
extern void AudioSignal_OnEnable_m192A6CD9820410AC2AE1778D96C602BF4DCF2882 (void);
// 0x0000003C System.Void AudioSignal::OnDisable()
extern void AudioSignal_OnDisable_m857D9CF801892BED74092C94A21B9F4972FF9363 (void);
// 0x0000003D System.Void AudioSignal::Update()
extern void AudioSignal_Update_m2931CC495A8FA7474AD9555823E6D3CC70E99B8E (void);
// 0x0000003E System.Void AudioSignal::RoundStart()
extern void AudioSignal_RoundStart_mC835B1FD3432DEDF5DCA274EB0D805B01D0FE8FF (void);
// 0x0000003F System.Void AudioSignal::RoundEnd()
extern void AudioSignal_RoundEnd_m45D5DEBC11A6977EA7444A7D02B71FAA9FD56AA3 (void);
// 0x00000040 System.Void AudioSignal::MusicStart()
extern void AudioSignal_MusicStart_m8FBD1721ED93FB964924AC216C1D778101441CCB (void);
// 0x00000041 System.Void AudioSignal::HumanWin()
extern void AudioSignal_HumanWin_m29A68083A7881EEF6EA00E6DDF7FF7639ED5071C (void);
// 0x00000042 System.Void AudioSignal::ZombieWin()
extern void AudioSignal_ZombieWin_m882299473F23F34B8B20E3501C88D8F5328E7D6B (void);
// 0x00000043 System.Collections.IEnumerator AudioSignal::Fade(UnityEngine.AudioSource,System.Single,System.Single)
extern void AudioSignal_Fade_m9C7DF64837E7774132C79AAC02B3D5556500BD2D (void);
// 0x00000044 System.Void AudioSignal::.ctor()
extern void AudioSignal__ctor_mE0872A7307695C3581BC07C03E8191C7632DF7E9 (void);
// 0x00000045 System.Void AudioSignal::MirrorProcessed()
extern void AudioSignal_MirrorProcessed_mC440D17D276A6C8B62CB774ECA93A04612371693 (void);
// 0x00000046 System.Void AudioSignal/<Fade>d__18::.ctor(System.Int32)
extern void U3CFadeU3Ed__18__ctor_m04F74D837E2B61AB8B456A45AD8D345360C252FE (void);
// 0x00000047 System.Void AudioSignal/<Fade>d__18::System.IDisposable.Dispose()
extern void U3CFadeU3Ed__18_System_IDisposable_Dispose_m40B9B4570E3E4E738CE3BD9255454EA5D131CB86 (void);
// 0x00000048 System.Boolean AudioSignal/<Fade>d__18::MoveNext()
extern void U3CFadeU3Ed__18_MoveNext_m001CE9431A85376751362EA8F53BA63AD4D7BD0E (void);
// 0x00000049 System.Object AudioSignal/<Fade>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C29197FD70723DBB336DBC54E629A8C44C06B56 (void);
// 0x0000004A System.Void AudioSignal/<Fade>d__18::System.Collections.IEnumerator.Reset()
extern void U3CFadeU3Ed__18_System_Collections_IEnumerator_Reset_m51E92B5A6364520E75C02EE72DC898A3035E4FEF (void);
// 0x0000004B System.Object AudioSignal/<Fade>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CFadeU3Ed__18_System_Collections_IEnumerator_get_Current_m72C595B77613BBD323F05B75827AB437D488E38F (void);
// 0x0000004C System.Int32 BotControl::get_GetSuccessRateOtherStatus()
extern void BotControl_get_GetSuccessRateOtherStatus_mC35DC770F6E4BECE896BD1E3A74261C24FD3C0CC (void);
// 0x0000004D System.Int32 BotControl::get_GetSuccessRateLogicGroupActivated()
extern void BotControl_get_GetSuccessRateLogicGroupActivated_m37A98C2113D6F062E18A5A0DF9096F113E4B768C (void);
// 0x0000004E System.Single BotControl::get_GetMinTimerChangeBotStatusBeforeStartEvent()
extern void BotControl_get_GetMinTimerChangeBotStatusBeforeStartEvent_m6F7294610D47559827208D205F6157ED5966AA00 (void);
// 0x0000004F System.Single BotControl::get_GetMaxTimerChangeBotStatusBeforeStartEvent()
extern void BotControl_get_GetMaxTimerChangeBotStatusBeforeStartEvent_m0F1A5BC9BC2E6DCFB9C87C5C876D0B00382D0787 (void);
// 0x00000050 System.Single BotControl::get_GetMinTimerChangeBotStatusAfterStartEvent()
extern void BotControl_get_GetMinTimerChangeBotStatusAfterStartEvent_m87C956449E4DB1A2B637516EF685FED2C1318FDB (void);
// 0x00000051 System.Single BotControl::get_GetMaxTimerChangeBotStatusAfterStartEvent()
extern void BotControl_get_GetMaxTimerChangeBotStatusAfterStartEvent_m2CEC1402EAD58B250CE447AB587BE0C25703E6D1 (void);
// 0x00000052 System.Single BotControl::get_GetMinTimerDelayDetect()
extern void BotControl_get_GetMinTimerDelayDetect_m2692F57C55820EE7829F68C9FDFD478A5967E4B6 (void);
// 0x00000053 System.Single BotControl::get_GetMaxTimerDelayDetect()
extern void BotControl_get_GetMaxTimerDelayDetect_mDE4BA0568CFB0CA324F6F5F65FEEE5B56C5D043C (void);
// 0x00000054 System.Single BotControl::get_GetRadiusPlayerDetect()
extern void BotControl_get_GetRadiusPlayerDetect_m497841562CA0068B6285200622E6D1C3F6E7FAC8 (void);
// 0x00000055 System.Single BotControl::get_GetRadiusKnifeAttack()
extern void BotControl_get_GetRadiusKnifeAttack_m0AEDCB1351D41BDE3EBA94B6525B4E5750B86F6A (void);
// 0x00000056 System.Int32 BotControl::get_GetSuccessRateChangeWeapon()
extern void BotControl_get_GetSuccessRateChangeWeapon_m458C0576BF1FD79874994AB562C640382A450E30 (void);
// 0x00000057 System.Single BotControl::get_GetMinTimerChangeWeapon()
extern void BotControl_get_GetMinTimerChangeWeapon_mF03C30205FD13AB71E6C7DF688BD14465A73B2E1 (void);
// 0x00000058 System.Single BotControl::get_GetMaxTimerChangeWeapon()
extern void BotControl_get_GetMaxTimerChangeWeapon_mECC736C58A042DE90AF96031D184AA0AE5DD88CE (void);
// 0x00000059 System.Single BotControl::get_GetMinTimerWeaponForReload()
extern void BotControl_get_GetMinTimerWeaponForReload_mA1B2BE8CE78FD95210648C90CE8EF12CCF931B82 (void);
// 0x0000005A System.Single BotControl::get_GetMaxTimerWeaponForReload()
extern void BotControl_get_GetMaxTimerWeaponForReload_mCD9C7221DAADF1B536BD62FDF007DAC42D5A3922 (void);
// 0x0000005B System.Single BotControl::get_GetMinTimerLookStatus()
extern void BotControl_get_GetMinTimerLookStatus_mC1CA426B4EE18E9818A683342E13EC855C376AA7 (void);
// 0x0000005C System.Single BotControl::get_GetMaxTimerLookStatus()
extern void BotControl_get_GetMaxTimerLookStatus_mE18820DD24A005E744F8BB5CCA1F71C93FA1F4E4 (void);
// 0x0000005D System.Single BotControl::get_GetLookSpeedNormal()
extern void BotControl_get_GetLookSpeedNormal_mCECEADBFB431D42FB5CD0524B9746E880ABADD26 (void);
// 0x0000005E System.Single BotControl::get_GetLookSpeedDangerHuman()
extern void BotControl_get_GetLookSpeedDangerHuman_m0BBBA13CAF20A252AC9A1BBB090A98395FE978A7 (void);
// 0x0000005F System.Single BotControl::get_GetLookSpeedDangerZombie()
extern void BotControl_get_GetLookSpeedDangerZombie_mD99BFFC210745CEDA537598EB43CFE82CDA59EAF (void);
// 0x00000060 System.Void BotControl::Start()
extern void BotControl_Start_mFB7FE8067F660EC9EF9C9CDB5C71485644013F40 (void);
// 0x00000061 System.Void BotControl::OnEnable()
extern void BotControl_OnEnable_m3049361A40B16725EC1FEC47AC223D5569E04A3B (void);
// 0x00000062 System.Void BotControl::OnDisable()
extern void BotControl_OnDisable_mCA924B888BC03EB89575D35F0DBCD02BE727CBCB (void);
// 0x00000063 System.Void BotControl::NewRound()
extern void BotControl_NewRound_m80D8D2AF6C8129DEA055A7F9545C7386BC1B21D6 (void);
// 0x00000064 System.Void BotControl::Spawn(UnityEngine.GameObject,System.Int32)
extern void BotControl_Spawn_mCC01B6ADD2948BA59296574A388EB12BB6A7C9A6 (void);
// 0x00000065 System.Void BotControl::StartRound()
extern void BotControl_StartRound_m7089BBB2024CC04F2B576D013B1BC6CF25DCBB63 (void);
// 0x00000066 System.Void BotControl::EndRound()
extern void BotControl_EndRound_m2E80805263C3BCABC74759601EA2D8171A3253D4 (void);
// 0x00000067 System.Void BotControl::GameStart()
extern void BotControl_GameStart_m5B916F92FF8FCD1DC9E6C57C19694684418419FD (void);
// 0x00000068 System.Void BotControl::Update()
extern void BotControl_Update_m2A08E1CAB7B159E2AC5008F16DD09AE30D833347 (void);
// 0x00000069 System.Void BotControl::AnimWalk()
extern void BotControl_AnimWalk_m1FD4FC7B5CA06A2138DEBB67E3B8C884927EFE60 (void);
// 0x0000006A System.Void BotControl::LookTarget(UnityEngine.Vector3,System.Single)
extern void BotControl_LookTarget_m16AE873AEDD5D487AB32B1DE94765083473AF9FB (void);
// 0x0000006B System.Void BotControl::ChangeBotStatus()
extern void BotControl_ChangeBotStatus_m76BE5AB7EE405F5948230E6EB08FAEF84EA3FBC7 (void);
// 0x0000006C System.Void BotControl::BotWalk(UnityEngine.Vector3)
extern void BotControl_BotWalk_mDA544C56761B76BF652A1D4D61575D21A1955836 (void);
// 0x0000006D System.Void BotControl::BotWalkStop()
extern void BotControl_BotWalkStop_m18E5AD3DF3E941BB3EADB22FC5B454662045CB53 (void);
// 0x0000006E System.Void BotControl::BotStop()
extern void BotControl_BotStop_mD31736E0DACE604BB42852BC0A16A9C9DF983F8F (void);
// 0x0000006F System.Collections.IEnumerator BotControl::BotDanger()
extern void BotControl_BotDanger_m798679F8A270B3D4A0EE4CF0D3D066BDDD12EF9E (void);
// 0x00000070 System.Void BotControl::OnBecameVisible()
extern void BotControl_OnBecameVisible_m7EB18249CD0771EF7EF6917091DD161598E921A4 (void);
// 0x00000071 System.Void BotControl::OnBecameInvisible()
extern void BotControl_OnBecameInvisible_m0674AADA80B4411A05D1AEC511F4EC0A6220F58E (void);
// 0x00000072 System.Void BotControl::.ctor()
extern void BotControl__ctor_mFEBC274FC14D8FDA217DA8511B6F9B35D4D460D6 (void);
// 0x00000073 System.Void BotControl::MirrorProcessed()
extern void BotControl_MirrorProcessed_m589257C72A5B213E66952718A9984395A8DB14B1 (void);
// 0x00000074 UnityEngine.Vector2 BotControl::get_NetworkVelocityMove()
extern void BotControl_get_NetworkVelocityMove_m2D43AC3704B84E3F4F76AA0C16F63F97016589C7 (void);
// 0x00000075 System.Void BotControl::set_NetworkVelocityMove(UnityEngine.Vector2)
extern void BotControl_set_NetworkVelocityMove_m4AB4D035F17BC8F58510006CF3C39999464EE408 (void);
// 0x00000076 System.Boolean BotControl::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void BotControl_SerializeSyncVars_m6B9724CA95E6629DC50F2907478ED89C88A21195 (void);
// 0x00000077 System.Void BotControl::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void BotControl_DeserializeSyncVars_m15F05CDD79088E38A4DA0C6FA7F76EEDE2804956 (void);
// 0x00000078 System.Void BotControl/<BotDanger>d__105::.ctor(System.Int32)
extern void U3CBotDangerU3Ed__105__ctor_m221CEC151ECC709F3DDC0243666ACF0ED2CC14E8 (void);
// 0x00000079 System.Void BotControl/<BotDanger>d__105::System.IDisposable.Dispose()
extern void U3CBotDangerU3Ed__105_System_IDisposable_Dispose_m5EF6912AD9CE169A942D0B7425EFBC5AE5451964 (void);
// 0x0000007A System.Boolean BotControl/<BotDanger>d__105::MoveNext()
extern void U3CBotDangerU3Ed__105_MoveNext_m47E174450C0E0445DA66348CC48605F0C7078F14 (void);
// 0x0000007B System.Object BotControl/<BotDanger>d__105::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBotDangerU3Ed__105_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E7A886F407742212E5EE49C65283C20AE84148C (void);
// 0x0000007C System.Void BotControl/<BotDanger>d__105::System.Collections.IEnumerator.Reset()
extern void U3CBotDangerU3Ed__105_System_Collections_IEnumerator_Reset_m94FB6A4F3CC8D880AFF9605472A0D75D5D816F0A (void);
// 0x0000007D System.Object BotControl/<BotDanger>d__105::System.Collections.IEnumerator.get_Current()
extern void U3CBotDangerU3Ed__105_System_Collections_IEnumerator_get_Current_m105D3060BE2580D69F713CBB1BAF45F031E725AC (void);
// 0x0000007E System.Void BotsNavigation::Start()
extern void BotsNavigation_Start_mADFD94836498FB35BA0C6BEC17EE457B6EAB10CA (void);
// 0x0000007F System.Void BotsNavigation::OnEnable()
extern void BotsNavigation_OnEnable_mCF9AAB2208AFFB5A68E1FBA3BC93A20D503C7A91 (void);
// 0x00000080 System.Void BotsNavigation::OnDisable()
extern void BotsNavigation_OnDisable_m9EA20D968712C769F63EB76BA06B387856FA82A7 (void);
// 0x00000081 System.Void BotsNavigation::RoundStart()
extern void BotsNavigation_RoundStart_m73716610B4E794058F61DA230A9FF4C1C241A7A2 (void);
// 0x00000082 System.Void BotsNavigation::GameStart()
extern void BotsNavigation_GameStart_m7C3C437035465AA7A396C068B8A062C17E87467A (void);
// 0x00000083 System.Void BotsNavigation::Update()
extern void BotsNavigation_Update_m9E30FADB27AFBAD11CB5950ACCB8A7DC82D89115 (void);
// 0x00000084 System.Void BotsNavigation::SettingsNavigation()
extern void BotsNavigation_SettingsNavigation_m7BBA47583D668F6D55036E5285E96838796C5BD5 (void);
// 0x00000085 System.Void BotsNavigation::BotWalkZombie()
extern void BotsNavigation_BotWalkZombie_m6B06957F2B7C905F637B109732ECB67A29B74A01 (void);
// 0x00000086 System.Void BotsNavigation::BotLookZombie()
extern void BotsNavigation_BotLookZombie_m90DCDDE940C624D1970C4B4370695F8D21A67DF4 (void);
// 0x00000087 System.Void BotsNavigation::BotFollowPlayerZombie()
extern void BotsNavigation_BotFollowPlayerZombie_mC3E08C20C9CE26674853E58EA3729E9E6A02945D (void);
// 0x00000088 System.Void BotsNavigation::BotWalkHuman()
extern void BotsNavigation_BotWalkHuman_m9429F59B6D3129E00007A96E3E89D0FE877D7C43 (void);
// 0x00000089 System.Void BotsNavigation::BotRunToShelter()
extern void BotsNavigation_BotRunToShelter_m66A9A51AFDC7B9930BD13F5D4A5985493596D808 (void);
// 0x0000008A System.Void BotsNavigation::BotPatroling()
extern void BotsNavigation_BotPatroling_m0D44C535491B3261E781A621370A0B3E4DC31A62 (void);
// 0x0000008B System.Void BotsNavigation::BotFollowPlayerHuman()
extern void BotsNavigation_BotFollowPlayerHuman_mB5CB0F2C6107E1CC293287E476D7875307FAF232 (void);
// 0x0000008C System.Void BotsNavigation::BotDangerEscape()
extern void BotsNavigation_BotDangerEscape_mB842F6AC154341EF365D961416674A8C0BB34DC1 (void);
// 0x0000008D System.Void BotsNavigation::Infection(UnityEngine.GameObject,System.Int32)
extern void BotsNavigation_Infection_m44ECBA0C625B383837F6DE45222EF50D41D8EF9E (void);
// 0x0000008E System.Collections.IEnumerator BotsNavigation::boolBack(System.String)
extern void BotsNavigation_boolBack_m5387812E3B3255893AC162FFB7E7BCABBFC71DA5 (void);
// 0x0000008F System.Void BotsNavigation::ChangePosition()
extern void BotsNavigation_ChangePosition_mF7E80CA3795BF632C85447C71E20E33D39DD2D2F (void);
// 0x00000090 UnityEngine.Vector3 BotsNavigation::RandomBoxPoint(UnityEngine.Vector3,System.Single,System.Single)
extern void BotsNavigation_RandomBoxPoint_mCCBD7BA284B297B0949FBD08A9BC018A08982D1F (void);
// 0x00000091 System.Void BotsNavigation::.ctor()
extern void BotsNavigation__ctor_mAE072991EBD4D2357E04507B08A3084FBD49D35C (void);
// 0x00000092 System.Void BotsNavigation::MirrorProcessed()
extern void BotsNavigation_MirrorProcessed_m5653952270560CA5A20A67260F2822E3AB1A4146 (void);
// 0x00000093 System.Void BotsNavigation/<boolBack>d__40::.ctor(System.Int32)
extern void U3CboolBackU3Ed__40__ctor_mD689334AC96570604E2AC7FC7FC33A7542995AE5 (void);
// 0x00000094 System.Void BotsNavigation/<boolBack>d__40::System.IDisposable.Dispose()
extern void U3CboolBackU3Ed__40_System_IDisposable_Dispose_m4353B9B6195C979E67B4A12E0EA6DC031B712F21 (void);
// 0x00000095 System.Boolean BotsNavigation/<boolBack>d__40::MoveNext()
extern void U3CboolBackU3Ed__40_MoveNext_m95C325D33CFFD0DFEF0FC6D4DE67FFE16D6CE7CA (void);
// 0x00000096 System.Object BotsNavigation/<boolBack>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CboolBackU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0D96E3ABC5240D76ABD79A956367532A0A99ADB (void);
// 0x00000097 System.Void BotsNavigation/<boolBack>d__40::System.Collections.IEnumerator.Reset()
extern void U3CboolBackU3Ed__40_System_Collections_IEnumerator_Reset_m42373B28CE3BA620A0D50413ED04834C43AC1A27 (void);
// 0x00000098 System.Object BotsNavigation/<boolBack>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CboolBackU3Ed__40_System_Collections_IEnumerator_get_Current_mA0706E0471CE3E08071AE74E058E12620D7AA711 (void);
// 0x00000099 System.Void SheltersAndLooksOfBunker::OnDrawGizmos()
extern void SheltersAndLooksOfBunker_OnDrawGizmos_m6D1CB7A60296788432585ED3D0DBDF792B563A97 (void);
// 0x0000009A System.Void SheltersAndLooksOfBunker::.ctor()
extern void SheltersAndLooksOfBunker__ctor_m259DBAF17ED8275F33D6A276714686EAF3A71A16 (void);
// 0x0000009B System.Void ShelterZone::OnDrawGizmos()
extern void ShelterZone_OnDrawGizmos_m0E5D24946B1EEE8A36DC066AFB2C20FC1AAB0863 (void);
// 0x0000009C System.Void ShelterZone::.ctor()
extern void ShelterZone__ctor_m8D5CDB73B0510B144B3AB1AD081B2BFB9616DBF2 (void);
// 0x0000009D System.Void Client::.ctor()
extern void Client__ctor_m6F46A6650E8D17FC2CDF2D840FF97EA786B45115 (void);
// 0x0000009E System.Void InfoPlayer::.ctor()
extern void InfoPlayer__ctor_m6C37B7684249B50F50131E336F5217C337A7FF53 (void);
// 0x0000009F System.Void ControlController::Start()
extern void ControlController_Start_mC5F6991434A8C683060B813617E3EBD7FC153AB9 (void);
// 0x000000A0 System.Void ControlController::Update()
extern void ControlController_Update_m23D6752B723E211C98908F2843F8D05FC99FE7D3 (void);
// 0x000000A1 System.Void ControlController::SpectrMode()
extern void ControlController_SpectrMode_m62B96CE7FF24B50427AE5BB63FA802B43CCDA233 (void);
// 0x000000A2 System.Void ControlController::LeftButtonSpect()
extern void ControlController_LeftButtonSpect_mA35A594F9F492E0094B252FA7A2E04ADFB871BEB (void);
// 0x000000A3 System.Void ControlController::RightButtonSpect()
extern void ControlController_RightButtonSpect_m49EECF8AAA6C439AD8AADAEF44ABAD199A69551B (void);
// 0x000000A4 System.Void ControlController::SpectrLogic(System.Boolean,System.Int32)
extern void ControlController_SpectrLogic_mA777432B5887D7886E926E593D1C38F92BD836D1 (void);
// 0x000000A5 System.Void ControlController::.ctor()
extern void ControlController__ctor_mD1F85E28FCE120DE333E03C0F6C2CB50B80E364D (void);
// 0x000000A6 System.Void Damage::TakeDamege(UnityEngine.GameObject,UnityEngine.GameObject,System.Int32)
extern void Damage_TakeDamege_m3ADFEBD6BA534720EBC2C2514B79D9C0A0D21B43 (void);
// 0x000000A7 System.Collections.IEnumerator Damage::ParticalBlood(UnityEngine.GameObject)
extern void Damage_ParticalBlood_m805151B37021615F7F5898E312B9DA77AD4AC6B2 (void);
// 0x000000A8 System.Collections.IEnumerator Damage::DeathPlayer(UnityEngine.GameObject,System.Int32)
extern void Damage_DeathPlayer_m3E0A420DFB1315C1AD1D71CBE681C5D06F183E48 (void);
// 0x000000A9 System.Void Damage::RpcTakeDamage(Player,System.Boolean,System.Boolean)
extern void Damage_RpcTakeDamage_m69B5F3A52F8E9132492BF8E1C4787B6F30256F7F (void);
// 0x000000AA System.Void Damage::.ctor()
extern void Damage__ctor_mD125E75891B1101B5BB47BD4E746206A6AD8BCA6 (void);
// 0x000000AB System.Void Damage::MirrorProcessed()
extern void Damage_MirrorProcessed_mC4B07E8306110D3C1983DD9701210647DA190CFC (void);
// 0x000000AC System.Void Damage::UserCode_RpcTakeDamage__Player__Boolean__Boolean(Player,System.Boolean,System.Boolean)
extern void Damage_UserCode_RpcTakeDamage__Player__Boolean__Boolean_m9EDE1CAF8DC44A67B84076E7ECB40AD00C777684 (void);
// 0x000000AD System.Void Damage::InvokeUserCode_RpcTakeDamage__Player__Boolean__Boolean(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void Damage_InvokeUserCode_RpcTakeDamage__Player__Boolean__Boolean_m624B82138A2C3F6AA872F73A001A978E2F014938 (void);
// 0x000000AE System.Void Damage::.cctor()
extern void Damage__cctor_mD2D5B5B2313BB27D5855B7C876226C0B780EA62C (void);
// 0x000000AF System.Void Damage/<ParticalBlood>d__9::.ctor(System.Int32)
extern void U3CParticalBloodU3Ed__9__ctor_m10C83BCF6B741100C24C39A3F553EE590DB263AF (void);
// 0x000000B0 System.Void Damage/<ParticalBlood>d__9::System.IDisposable.Dispose()
extern void U3CParticalBloodU3Ed__9_System_IDisposable_Dispose_m3A344C32AA387E53104658FADA8E2C895AD7C5DE (void);
// 0x000000B1 System.Boolean Damage/<ParticalBlood>d__9::MoveNext()
extern void U3CParticalBloodU3Ed__9_MoveNext_m4BE986E6366159849A0139F1129004AAE50E7730 (void);
// 0x000000B2 System.Object Damage/<ParticalBlood>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CParticalBloodU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA7A1C26D7FFFB65096C5DFF22740E12B4FF956C3 (void);
// 0x000000B3 System.Void Damage/<ParticalBlood>d__9::System.Collections.IEnumerator.Reset()
extern void U3CParticalBloodU3Ed__9_System_Collections_IEnumerator_Reset_mC6644B1B39AA1AB650824B7D8CABA8B4C65C21A1 (void);
// 0x000000B4 System.Object Damage/<ParticalBlood>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CParticalBloodU3Ed__9_System_Collections_IEnumerator_get_Current_m84B3A4C3F4B2016FAD12223A2EFBD53A603FD346 (void);
// 0x000000B5 System.Void Damage/<DeathPlayer>d__10::.ctor(System.Int32)
extern void U3CDeathPlayerU3Ed__10__ctor_m5019E1F3628215826FA70A0B1B885743EA1F3047 (void);
// 0x000000B6 System.Void Damage/<DeathPlayer>d__10::System.IDisposable.Dispose()
extern void U3CDeathPlayerU3Ed__10_System_IDisposable_Dispose_mFB3C5B6375B309C85936EDE988E294C23E0EF0EF (void);
// 0x000000B7 System.Boolean Damage/<DeathPlayer>d__10::MoveNext()
extern void U3CDeathPlayerU3Ed__10_MoveNext_m9CA210173058ED53BD07220D96ACBC1CDA7D8B5F (void);
// 0x000000B8 System.Object Damage/<DeathPlayer>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDeathPlayerU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2ECDAB91D117960073FDF562D993A1457FFE3AD2 (void);
// 0x000000B9 System.Void Damage/<DeathPlayer>d__10::System.Collections.IEnumerator.Reset()
extern void U3CDeathPlayerU3Ed__10_System_Collections_IEnumerator_Reset_m0249F481D131ACBB03579B415CF22F466741F6DF (void);
// 0x000000BA System.Object Damage/<DeathPlayer>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CDeathPlayerU3Ed__10_System_Collections_IEnumerator_get_Current_mD0073E9C4E18112CC69E16FB891E442B56EB1D59 (void);
// 0x000000BB System.Void DataBaseScript::.ctor()
extern void DataBaseScript__ctor_m2259E1B1E1F61018B5E826F0AFF949568DDE3CD4 (void);
// 0x000000BC System.Void DataSaver::saveData(T,System.String)
// 0x000000BD T DataSaver::loadData(System.String)
// 0x000000BE System.Boolean DataSaver::deleteData(System.String)
extern void DataSaver_deleteData_m84608CAC00D096B1C5982E49A2AFAAB6250926D9 (void);
// 0x000000BF System.Void DataSaver::.ctor()
extern void DataSaver__ctor_mE99C7D193CC90DDDDA6566AA8E62F5248DEAD760 (void);
// 0x000000C0 System.Void Biohazard::Start()
extern void Biohazard_Start_m41657261F53B877110AC0B4C4AADE895A2D1BDED (void);
// 0x000000C1 System.Void Biohazard::OnStartClient()
extern void Biohazard_OnStartClient_m5803DF3D7E1CD654EFEF7BEC2BC11AA09CFAD244 (void);
// 0x000000C2 System.Void Biohazard::OnStartServer()
extern void Biohazard_OnStartServer_m0A172AE3E025B84D89404E534B6A161EC5F2EDA1 (void);
// 0x000000C3 System.Void Biohazard::Initialization()
extern void Biohazard_Initialization_m1D90743712EE5C58E75338A7D2DCD24BD550E306 (void);
// 0x000000C4 System.Void Biohazard::OnEnable()
extern void Biohazard_OnEnable_m69D9FCD5C8E90F6D3932BB583C43E834A191FD5F (void);
// 0x000000C5 System.Void Biohazard::OnDisable()
extern void Biohazard_OnDisable_mEFFEC019B06298D6B195203652B94A263D4B0A66 (void);
// 0x000000C6 System.Void Biohazard::Update()
extern void Biohazard_Update_mC7CE387CEAB7711F599AE8B7DD6053A0297BD69D (void);
// 0x000000C7 System.Void Biohazard::CountsPlayers()
extern void Biohazard_CountsPlayers_m22DA2382E60EEDD2F699067F8466D0FD274A6D28 (void);
// 0x000000C8 System.Void Biohazard::RoundHumanWin()
extern void Biohazard_RoundHumanWin_mDED733E7218A9622C1193FD7972E0D0BCBC8FE00 (void);
// 0x000000C9 System.Void Biohazard::RoundZombieWin()
extern void Biohazard_RoundZombieWin_m687F70BB9DA28AFAEBBEDD38532B4C1334285DC6 (void);
// 0x000000CA System.Void Biohazard::StartingTimerPreInfecton()
extern void Biohazard_StartingTimerPreInfecton_m36F1FA934F4C8DC979BFA969F432488250D80DB4 (void);
// 0x000000CB System.Void Biohazard::SettingsMod()
extern void Biohazard_SettingsMod_m8EAB2F066A910988E2B36612D7FFD41436283C87 (void);
// 0x000000CC System.Void Biohazard::PlayerSpawn(UnityEngine.GameObject,System.Int32)
extern void Biohazard_PlayerSpawn_mCAB284BE4F66A78D5707CD6D26D226EB55820D52 (void);
// 0x000000CD System.Void Biohazard::PlayerDeath(UnityEngine.GameObject,System.Int32)
extern void Biohazard_PlayerDeath_mAE521F91538F0A64EFF80FBC794F1A7D990C9FEA (void);
// 0x000000CE System.Void Biohazard::EndRound()
extern void Biohazard_EndRound_m70E310922FAFA7DFA6FC5EA7603D337E3CF53D93 (void);
// 0x000000CF System.Collections.IEnumerator Biohazard::PlayerDelay(Player)
extern void Biohazard_PlayerDelay_m6AD935034BFEEB057EAA45FB7F29C99EFF5D0552 (void);
// 0x000000D0 System.Collections.IEnumerator Biohazard::StartInfection()
extern void Biohazard_StartInfection_m3BBDBAC59E45DFB78D6E3672B7389E243804577B (void);
// 0x000000D1 System.Collections.IEnumerator Biohazard::InfectionPlayer(UnityEngine.GameObject)
extern void Biohazard_InfectionPlayer_mE6BD4456B49FDA746DFF5EB4A0B06B4BA60B0DE5 (void);
// 0x000000D2 System.Void Biohazard::HunterStarting()
extern void Biohazard_HunterStarting_mAE9D4D7AED08A88BFD404209F633C525E61540C0 (void);
// 0x000000D3 System.Void Biohazard::Antidot(UnityEngine.GameObject)
extern void Biohazard_Antidot_m57FE9E65C86BF9A26E58732198708D12997C5041 (void);
// 0x000000D4 System.Void Biohazard::WeaponReset(Player,WeaponSystem)
extern void Biohazard_WeaponReset_mD5A02EAA90F13CB0ABA4ED73DCE7F4178C53C8E5 (void);
// 0x000000D5 System.Void Biohazard::SwitchSettingsMode()
extern void Biohazard_SwitchSettingsMode_m9017CDED15DF6A47F51074ED3A227DEFE1D35BB3 (void);
// 0x000000D6 System.Void Biohazard::UpdatePlayerProperties(UnityEngine.GameObject)
extern void Biohazard_UpdatePlayerProperties_mD411E687569D650B017FE899690563AA847BF0F6 (void);
// 0x000000D7 System.Void Biohazard::PlayerClassParametrs(Player)
extern void Biohazard_PlayerClassParametrs_m9A0D537E646F3A5A57F15C1B1DDC7AD3473EB940 (void);
// 0x000000D8 System.Void Biohazard::PlayerSoundUpdate(Player)
extern void Biohazard_PlayerSoundUpdate_m7B44A39AB2ABD2BD8E14F177860A0DAE65BBA65C (void);
// 0x000000D9 System.Void Biohazard::RpcModStatus(System.Boolean,Biohazard/BiohazardMode)
extern void Biohazard_RpcModStatus_m0C43BF5184CD600239B451310ED1DC673CE62572 (void);
// 0x000000DA System.Void Biohazard::RpcRoundHumanWin()
extern void Biohazard_RpcRoundHumanWin_m10946EF7C7A8431A1716B8494245B9A52C59D4CD (void);
// 0x000000DB System.Void Biohazard::RpcRoundZombieWin()
extern void Biohazard_RpcRoundZombieWin_m23C7ECA71CC370B9C5961178D6318528588AA720 (void);
// 0x000000DC System.Void Biohazard::RpcInfection(UnityEngine.GameObject)
extern void Biohazard_RpcInfection_m4E31F764CB6FC4319BA054806776E0951A5EF17C (void);
// 0x000000DD System.Void Biohazard::.ctor()
extern void Biohazard__ctor_mDE1F6137491C1F8092EB0DBBA4AF5058A23FC5E4 (void);
// 0x000000DE System.Void Biohazard::MirrorProcessed()
extern void Biohazard_MirrorProcessed_m235E3863FB78B7EF342AA0EDEF54BE38BDC69145 (void);
// 0x000000DF System.Int32 Biohazard::get_NetworkSyncStatusMod()
extern void Biohazard_get_NetworkSyncStatusMod_mC57AC58FCFCC0AAA7A5B9EBC21C43793E24E2B24 (void);
// 0x000000E0 System.Void Biohazard::set_NetworkSyncStatusMod(System.Int32)
extern void Biohazard_set_NetworkSyncStatusMod_m1F537B9DBE6CC32971D67A6B2FC7F75D883A098A (void);
// 0x000000E1 System.Int32 Biohazard::get_NetworkPlayerCount()
extern void Biohazard_get_NetworkPlayerCount_mADB23128B9645514AFD91D9079DCBC716E19B3C2 (void);
// 0x000000E2 System.Void Biohazard::set_NetworkPlayerCount(System.Int32)
extern void Biohazard_set_NetworkPlayerCount_m0D511CAE7AFA19C6C95DE005276225345B3CA8CC (void);
// 0x000000E3 System.Int32 Biohazard::get_NetworkZombieCount()
extern void Biohazard_get_NetworkZombieCount_m3E8507EF7EE6780F9B30EF11D45D9D8BB89EE558 (void);
// 0x000000E4 System.Void Biohazard::set_NetworkZombieCount(System.Int32)
extern void Biohazard_set_NetworkZombieCount_m6BEC877604366B9D2ADB98A073FD0BA2E537BD36 (void);
// 0x000000E5 System.Int32 Biohazard::get_NetworkHumanCount()
extern void Biohazard_get_NetworkHumanCount_m5C83976F9CD651274286DCC071604BE285D72A67 (void);
// 0x000000E6 System.Void Biohazard::set_NetworkHumanCount(System.Int32)
extern void Biohazard_set_NetworkHumanCount_mFE401B68D31F8081372FF0C3B925713E371356E8 (void);
// 0x000000E7 System.Void Biohazard::UserCode_RpcModStatus__Boolean__BiohazardMode(System.Boolean,Biohazard/BiohazardMode)
extern void Biohazard_UserCode_RpcModStatus__Boolean__BiohazardMode_m62797101A38B3A7BAF8FAE4F1C8E39A89AAC8BB8 (void);
// 0x000000E8 System.Void Biohazard::InvokeUserCode_RpcModStatus__Boolean__BiohazardMode(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void Biohazard_InvokeUserCode_RpcModStatus__Boolean__BiohazardMode_m896B017243D5055A76E670A150FB31240F09ED30 (void);
// 0x000000E9 System.Void Biohazard::UserCode_RpcRoundHumanWin()
extern void Biohazard_UserCode_RpcRoundHumanWin_m3D24852738A64CB25FF0C69940C99688841354CC (void);
// 0x000000EA System.Void Biohazard::InvokeUserCode_RpcRoundHumanWin(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void Biohazard_InvokeUserCode_RpcRoundHumanWin_m36D182C015F7206665B6AC29DDB8D939451D5373 (void);
// 0x000000EB System.Void Biohazard::UserCode_RpcRoundZombieWin()
extern void Biohazard_UserCode_RpcRoundZombieWin_m6E8B191CEE0044BB611D360D17FBE2A5FBB19F96 (void);
// 0x000000EC System.Void Biohazard::InvokeUserCode_RpcRoundZombieWin(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void Biohazard_InvokeUserCode_RpcRoundZombieWin_mFD072912C9350DD7E4D7E52475B62003C570BC99 (void);
// 0x000000ED System.Void Biohazard::UserCode_RpcInfection__GameObject(UnityEngine.GameObject)
extern void Biohazard_UserCode_RpcInfection__GameObject_m99F6CC124252B439CF9708E1FA28297A7C687619 (void);
// 0x000000EE System.Void Biohazard::InvokeUserCode_RpcInfection__GameObject(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void Biohazard_InvokeUserCode_RpcInfection__GameObject_m32A6F2A36D23C076CE7BF662FC4362E0118D13FD (void);
// 0x000000EF System.Void Biohazard::.cctor()
extern void Biohazard__cctor_mCDE2632565703B2AECEDFB917490F330C24B88FE (void);
// 0x000000F0 System.Boolean Biohazard::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void Biohazard_SerializeSyncVars_mF3AED1660DF26A794FB9B8CC0A8B4D44BCF234C8 (void);
// 0x000000F1 System.Void Biohazard::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void Biohazard_DeserializeSyncVars_mD7F3B4B7EFFF1B26878FCE8C840B9E033056305C (void);
// 0x000000F2 System.Void Biohazard/<PlayerDelay>d__55::.ctor(System.Int32)
extern void U3CPlayerDelayU3Ed__55__ctor_mC6D33A58A323B49AE3474C565C39408D255B3AE4 (void);
// 0x000000F3 System.Void Biohazard/<PlayerDelay>d__55::System.IDisposable.Dispose()
extern void U3CPlayerDelayU3Ed__55_System_IDisposable_Dispose_m1F899D065C87BC56DBB5483F374EC53D926ED82B (void);
// 0x000000F4 System.Boolean Biohazard/<PlayerDelay>d__55::MoveNext()
extern void U3CPlayerDelayU3Ed__55_MoveNext_m104628BC65BDC2B0FDC86C8A0FDF3AB1883B9B54 (void);
// 0x000000F5 System.Object Biohazard/<PlayerDelay>d__55::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayerDelayU3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3009327C699C382A542E0965C4AF90E3A6513299 (void);
// 0x000000F6 System.Void Biohazard/<PlayerDelay>d__55::System.Collections.IEnumerator.Reset()
extern void U3CPlayerDelayU3Ed__55_System_Collections_IEnumerator_Reset_mF78C31D41000880B0FF35348F05CAF130F37B7F0 (void);
// 0x000000F7 System.Object Biohazard/<PlayerDelay>d__55::System.Collections.IEnumerator.get_Current()
extern void U3CPlayerDelayU3Ed__55_System_Collections_IEnumerator_get_Current_m7C5D35A2E1B20177C063D045FE7184CBCAB4D8CE (void);
// 0x000000F8 System.Void Biohazard/<StartInfection>d__56::.ctor(System.Int32)
extern void U3CStartInfectionU3Ed__56__ctor_m929AEF60D5D0AE22D600BFB748C81879CC47F49E (void);
// 0x000000F9 System.Void Biohazard/<StartInfection>d__56::System.IDisposable.Dispose()
extern void U3CStartInfectionU3Ed__56_System_IDisposable_Dispose_m02C0645913D842745F8CB3FE08C15F0E2F0F6BC1 (void);
// 0x000000FA System.Boolean Biohazard/<StartInfection>d__56::MoveNext()
extern void U3CStartInfectionU3Ed__56_MoveNext_m0B3C1B0D4F725C742C9434CABDD065625AA5BA85 (void);
// 0x000000FB System.Object Biohazard/<StartInfection>d__56::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartInfectionU3Ed__56_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EEDCBB6639AE6D84E5E8E86F09ED8A8DCA42F39 (void);
// 0x000000FC System.Void Biohazard/<StartInfection>d__56::System.Collections.IEnumerator.Reset()
extern void U3CStartInfectionU3Ed__56_System_Collections_IEnumerator_Reset_m0A7C0F8DD717C692A4769C94D49E73AEE123C3B4 (void);
// 0x000000FD System.Object Biohazard/<StartInfection>d__56::System.Collections.IEnumerator.get_Current()
extern void U3CStartInfectionU3Ed__56_System_Collections_IEnumerator_get_Current_m6DC65643A63C3FF516BAED1C038676EAE1FECD8A (void);
// 0x000000FE System.Void Biohazard/<InfectionPlayer>d__57::.ctor(System.Int32)
extern void U3CInfectionPlayerU3Ed__57__ctor_m12BDEF3CA67A0B22D9F0094EF99933C67337C843 (void);
// 0x000000FF System.Void Biohazard/<InfectionPlayer>d__57::System.IDisposable.Dispose()
extern void U3CInfectionPlayerU3Ed__57_System_IDisposable_Dispose_mD0E751ADCB82EEF0BECF741C8AA46A1C0E3A5B85 (void);
// 0x00000100 System.Boolean Biohazard/<InfectionPlayer>d__57::MoveNext()
extern void U3CInfectionPlayerU3Ed__57_MoveNext_mCC9FD279189EA471F4A279CB775C753C46A18E89 (void);
// 0x00000101 System.Object Biohazard/<InfectionPlayer>d__57::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInfectionPlayerU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE384AA7F58470DBBB9C4F5C16A09D684301A0CB (void);
// 0x00000102 System.Void Biohazard/<InfectionPlayer>d__57::System.Collections.IEnumerator.Reset()
extern void U3CInfectionPlayerU3Ed__57_System_Collections_IEnumerator_Reset_mB1C8A009F01139118189043EBE5B879D4EE502B3 (void);
// 0x00000103 System.Object Biohazard/<InfectionPlayer>d__57::System.Collections.IEnumerator.get_Current()
extern void U3CInfectionPlayerU3Ed__57_System_Collections_IEnumerator_get_Current_m9B97BEE72C710597863CDB5ABFBC3DDDB928A924 (void);
// 0x00000104 System.Void GameControl::Start()
extern void GameControl_Start_m1B81A3A747085E94CC8962FD4FCF12995361FDDC (void);
// 0x00000105 System.Void GameControl::OnStartClient()
extern void GameControl_OnStartClient_m2C4C5E4C1C466970C2802D9D77D1145F2AF260CE (void);
// 0x00000106 System.Void GameControl::OnStartServer()
extern void GameControl_OnStartServer_m3B5A3DA59694DD5CCD9485D30087BAAF311EE2B0 (void);
// 0x00000107 System.Void GameControl::Initialization()
extern void GameControl_Initialization_m60903A8E9944547D4BAEF19A84EB2A7593EF386C (void);
// 0x00000108 System.Void GameControl::OnEnable()
extern void GameControl_OnEnable_m1F1D947872A5120F18F61EC19F58BBD8E3289822 (void);
// 0x00000109 System.Void GameControl::OnDisable()
extern void GameControl_OnDisable_m47502C9A7158CDC85658D582E216D2252B1DE100 (void);
// 0x0000010A System.Void GameControl::Update()
extern void GameControl_Update_m5C6C1398A3CBEE8718E10B4AD10E30C5162727D3 (void);
// 0x0000010B System.Void GameControl::NewRound()
extern void GameControl_NewRound_mD0F704352FCA50DCDCE3AC7AF0041EAC9F3B6FC9 (void);
// 0x0000010C System.Void GameControl::GameStart()
extern void GameControl_GameStart_m38E85DEF9D27A4041E9E8294FE253A2AA416DC56 (void);
// 0x0000010D System.Void GameControl::GameEnd()
extern void GameControl_GameEnd_m561450FEA45451FC32E711334275CD8528DE331E (void);
// 0x0000010E System.Void GameControl::PlayerDeath(UnityEngine.GameObject,System.Int32)
extern void GameControl_PlayerDeath_m2CFC2D0A3260DC939328DCE82E3867BD8D265E47 (void);
// 0x0000010F System.Collections.IEnumerator GameControl::RespawnPlayer(UnityEngine.GameObject,System.Single,Player/Team)
extern void GameControl_RespawnPlayer_m557E24EA2FF0197F39191DF2A69FFFCE58EEDAD3 (void);
// 0x00000110 System.Void GameControl::SetWeapon(Player,System.Int32,WeaponSystem)
extern void GameControl_SetWeapon_mBDB5817434023EB4C4C589EC93173D597AE0A155 (void);
// 0x00000111 System.Void GameControl::WeaponUpdate(UnityEngine.GameObject)
extern void GameControl_WeaponUpdate_m90EA4F3C513C6C02D5EB285F8EE90DAB5B7D83A6 (void);
// 0x00000112 System.Void GameControl::UpdateWeapon()
extern void GameControl_UpdateWeapon_mAC992CFEE371B2643A8785303031AA288E8858FC (void);
// 0x00000113 System.Void GameControl::ConnectedRespawn(UnityEngine.GameObject)
extern void GameControl_ConnectedRespawn_m8702D1A134BD280CFE3EB58533DA02E48FFE583B (void);
// 0x00000114 System.Void GameControl::RpcNewRound()
extern void GameControl_RpcNewRound_m2C512899543F960D02D71B2A11B97F350AE1E88D (void);
// 0x00000115 System.Void GameControl::RpcCreateBot(UnityEngine.GameObject,System.Int32)
extern void GameControl_RpcCreateBot_mFFD715E6028CD40F3DD926B9873A048DF4246CD5 (void);
// 0x00000116 System.Void GameControl::RpcDestroyBot(UnityEngine.GameObject)
extern void GameControl_RpcDestroyBot_m083B16ED901ED5D50E64D2242B06D5E8FE3BFE1D (void);
// 0x00000117 System.Void GameControl::RpcGameStart()
extern void GameControl_RpcGameStart_mB202A573544EE7B69D6ED5F0D09E505F9F22E204 (void);
// 0x00000118 System.Void GameControl::RpcGameEnd()
extern void GameControl_RpcGameEnd_m16EBDD5AC3C7B7BE71985FEE88203B0984CF29CD (void);
// 0x00000119 System.Void GameControl::TargetConnectedRespawn(Mirror.NetworkConnection,UnityEngine.GameObject,System.Boolean)
extern void GameControl_TargetConnectedRespawn_m40EDC60BE82430706B157F6A1AE0D5819EC3C454 (void);
// 0x0000011A System.Void GameControl::TargetRespawn(Mirror.NetworkConnection,UnityEngine.GameObject)
extern void GameControl_TargetRespawn_m27A1A0E0C12E09DC9BB402A210F64F67412E3A6D (void);
// 0x0000011B System.Void GameControl::.ctor()
extern void GameControl__ctor_m4650C9F61DFC74BEDDADA2654D755EEB825AD161 (void);
// 0x0000011C System.Void GameControl::.cctor()
extern void GameControl__cctor_m86BF3DA0366363970A422CEC002A1A8FB030EB7A (void);
// 0x0000011D System.Void GameControl::MirrorProcessed()
extern void GameControl_MirrorProcessed_mE7CDA3AF7F46F83A548D0379CD00677B355899EB (void);
// 0x0000011E System.Single GameControl::get_NetworkRoundTime()
extern void GameControl_get_NetworkRoundTime_m7774B24548508E9752EFE370FCF7A2B79A872B37 (void);
// 0x0000011F System.Void GameControl::set_NetworkRoundTime(System.Single)
extern void GameControl_set_NetworkRoundTime_mA68105F3E669B6B916E4F06C481F5A5C42F9F6F2 (void);
// 0x00000120 System.Void GameControl::UserCode_RpcNewRound()
extern void GameControl_UserCode_RpcNewRound_mC2708F0B9DD1FCB32B551968FA57C5617BE2739E (void);
// 0x00000121 System.Void GameControl::InvokeUserCode_RpcNewRound(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void GameControl_InvokeUserCode_RpcNewRound_m71230F20CE58ACB6089D96E63B705557FD477063 (void);
// 0x00000122 System.Void GameControl::UserCode_RpcCreateBot__GameObject__Int32(UnityEngine.GameObject,System.Int32)
extern void GameControl_UserCode_RpcCreateBot__GameObject__Int32_mF6E41C23F2B88CD80A29263551D1A67D6449BDD9 (void);
// 0x00000123 System.Void GameControl::InvokeUserCode_RpcCreateBot__GameObject__Int32(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void GameControl_InvokeUserCode_RpcCreateBot__GameObject__Int32_m19731046C5A83A71BDF03958EA589A450E0E2269 (void);
// 0x00000124 System.Void GameControl::UserCode_RpcDestroyBot__GameObject(UnityEngine.GameObject)
extern void GameControl_UserCode_RpcDestroyBot__GameObject_mD9312ACA7FAB013A14479FCFA369F38F3891850D (void);
// 0x00000125 System.Void GameControl::InvokeUserCode_RpcDestroyBot__GameObject(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void GameControl_InvokeUserCode_RpcDestroyBot__GameObject_m583C2302425E58718C2DC9E0D23E7FAE6941B27F (void);
// 0x00000126 System.Void GameControl::UserCode_RpcGameStart()
extern void GameControl_UserCode_RpcGameStart_m19235B1B56F9D8CACE8ADE9428EBD8F270D28816 (void);
// 0x00000127 System.Void GameControl::InvokeUserCode_RpcGameStart(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void GameControl_InvokeUserCode_RpcGameStart_mD1954F26C48D8D5D6F6EF4CCDF01AAE7386BC0A4 (void);
// 0x00000128 System.Void GameControl::UserCode_RpcGameEnd()
extern void GameControl_UserCode_RpcGameEnd_m4FF429F9F33066B3A7E13F749542439AB929EA3A (void);
// 0x00000129 System.Void GameControl::InvokeUserCode_RpcGameEnd(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void GameControl_InvokeUserCode_RpcGameEnd_m45A38DEA4022C57E581F6A6E47DA79040EE5800F (void);
// 0x0000012A System.Void GameControl::UserCode_TargetConnectedRespawn__NetworkConnection__GameObject__Boolean(Mirror.NetworkConnection,UnityEngine.GameObject,System.Boolean)
extern void GameControl_UserCode_TargetConnectedRespawn__NetworkConnection__GameObject__Boolean_mC2DB44ABA9F443BF7A710EA30B46438407722D6A (void);
// 0x0000012B System.Void GameControl::InvokeUserCode_TargetConnectedRespawn__NetworkConnection__GameObject__Boolean(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void GameControl_InvokeUserCode_TargetConnectedRespawn__NetworkConnection__GameObject__Boolean_m519331FDF9C775D9FBAD3D52822F5D1D870C3DA1 (void);
// 0x0000012C System.Void GameControl::UserCode_TargetRespawn__NetworkConnection__GameObject(Mirror.NetworkConnection,UnityEngine.GameObject)
extern void GameControl_UserCode_TargetRespawn__NetworkConnection__GameObject_m90E76EC466F3B17F53A3B72FB0569F1F354A3D22 (void);
// 0x0000012D System.Void GameControl::InvokeUserCode_TargetRespawn__NetworkConnection__GameObject(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void GameControl_InvokeUserCode_TargetRespawn__NetworkConnection__GameObject_mD1716FC979F2E898D1E8BD39D4B6476E75BD0ABD (void);
// 0x0000012E System.Boolean GameControl::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void GameControl_SerializeSyncVars_m7EC609B61679B38172A6809848C75C9BD94CDDDC (void);
// 0x0000012F System.Void GameControl::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void GameControl_DeserializeSyncVars_mB015388089C73455CA0E50152FEA8E44F0CDCB43 (void);
// 0x00000130 System.Void GameControl/<RespawnPlayer>d__54::.ctor(System.Int32)
extern void U3CRespawnPlayerU3Ed__54__ctor_mA4E718B817323750E68E1D720B6A8F20AA360FE2 (void);
// 0x00000131 System.Void GameControl/<RespawnPlayer>d__54::System.IDisposable.Dispose()
extern void U3CRespawnPlayerU3Ed__54_System_IDisposable_Dispose_m3C395F12E2266E8A85B958A24EC318AFA6980A61 (void);
// 0x00000132 System.Boolean GameControl/<RespawnPlayer>d__54::MoveNext()
extern void U3CRespawnPlayerU3Ed__54_MoveNext_mC3917DE2315B53D495DDA2AB988578976EC747E1 (void);
// 0x00000133 System.Object GameControl/<RespawnPlayer>d__54::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnPlayerU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m850B6D70EA2DB2D68A16565D8E5238C539095EAE (void);
// 0x00000134 System.Void GameControl/<RespawnPlayer>d__54::System.Collections.IEnumerator.Reset()
extern void U3CRespawnPlayerU3Ed__54_System_Collections_IEnumerator_Reset_m8A6E5B3FB7B97021DB65AC5159E5E66760536866 (void);
// 0x00000135 System.Object GameControl/<RespawnPlayer>d__54::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnPlayerU3Ed__54_System_Collections_IEnumerator_get_Current_m4ADCAF8F49FDF6E05FA16DA7A90E13A7602F42A5 (void);
// 0x00000136 System.Void HumanClassSystem::.ctor()
extern void HumanClassSystem__ctor_mB5CF88AD73FF0AAEE60736F53A46C7BB44474760 (void);
// 0x00000137 System.Void HumanClassSystem::MirrorProcessed()
extern void HumanClassSystem_MirrorProcessed_mE4423A1BAEB30B2992C0A5DCE00819CCE4EFF8A2 (void);
// 0x00000138 System.Void ZombieClassSystem::.ctor()
extern void ZombieClassSystem__ctor_mEFB9AF170E0BFFDD0283FD63925572A272C26776 (void);
// 0x00000139 System.Void ZombieClassSystem::MirrorProcessed()
extern void ZombieClassSystem_MirrorProcessed_m80AD122A37FF833BE428F0153F6F8B447BE68C10 (void);
// 0x0000013A System.Void KnockBack::TakeKnockBack(System.Single,System.Single,UnityEngine.GameObject,UnityEngine.GameObject)
extern void KnockBack_TakeKnockBack_mFF5E0F44D64A30B5420484B9E7FC3178878215C1 (void);
// 0x0000013B System.Collections.IEnumerator KnockBack::KnockBackForce(System.Single,System.Single,UnityEngine.GameObject,UnityEngine.GameObject)
extern void KnockBack_KnockBackForce_m1AFD3E987032231B560F2A2A22D038866E4BF815 (void);
// 0x0000013C System.Collections.IEnumerator KnockBack::KnockBackVelocity(System.Single,System.Single,UnityEngine.GameObject,UnityEngine.GameObject)
extern void KnockBack_KnockBackVelocity_mA2C1095D7AECB59B13EC33E78073E37E067A5F86 (void);
// 0x0000013D System.Void KnockBack::TargetTakeKnockBack(Mirror.NetworkConnection,System.Single,System.Single,UnityEngine.GameObject,UnityEngine.GameObject)
extern void KnockBack_TargetTakeKnockBack_m8EE4FBEE1C7E162EA4584A1673FD8DF355828916 (void);
// 0x0000013E System.Void KnockBack::.ctor()
extern void KnockBack__ctor_mF25C0A16848BD198414EFCB6B5459B2F569F9854 (void);
// 0x0000013F System.Void KnockBack::MirrorProcessed()
extern void KnockBack_MirrorProcessed_m432570BCA25E2D9C3EF442EBA2888F0866C06355 (void);
// 0x00000140 System.Void KnockBack::UserCode_TargetTakeKnockBack__NetworkConnection__Single__Single__GameObject__GameObject(Mirror.NetworkConnection,System.Single,System.Single,UnityEngine.GameObject,UnityEngine.GameObject)
extern void KnockBack_UserCode_TargetTakeKnockBack__NetworkConnection__Single__Single__GameObject__GameObject_m342E8DD4053E1417879FDCCC6E2984DA807FC1C2 (void);
// 0x00000141 System.Void KnockBack::InvokeUserCode_TargetTakeKnockBack__NetworkConnection__Single__Single__GameObject__GameObject(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void KnockBack_InvokeUserCode_TargetTakeKnockBack__NetworkConnection__Single__Single__GameObject__GameObject_m0113EF3D148E93B9AB743FE95EB47EFDD3DBF69F (void);
// 0x00000142 System.Void KnockBack::.cctor()
extern void KnockBack__cctor_mD1692C84DE1DDF01A4AB01E039F3CCE1F8FD4CB3 (void);
// 0x00000143 System.Void KnockBack/<KnockBackForce>d__4::.ctor(System.Int32)
extern void U3CKnockBackForceU3Ed__4__ctor_m72C0012DC03C05BA3A406CF147F9515061B6D4E2 (void);
// 0x00000144 System.Void KnockBack/<KnockBackForce>d__4::System.IDisposable.Dispose()
extern void U3CKnockBackForceU3Ed__4_System_IDisposable_Dispose_mB43FABF925D165DC15D915549E5BA530A113EAAF (void);
// 0x00000145 System.Boolean KnockBack/<KnockBackForce>d__4::MoveNext()
extern void U3CKnockBackForceU3Ed__4_MoveNext_mD4D1290F5E6A741DB9C57575B6EC10AC2D48BA67 (void);
// 0x00000146 System.Object KnockBack/<KnockBackForce>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKnockBackForceU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A1172CC5A8540E564E787F2D9FB863C007289E1 (void);
// 0x00000147 System.Void KnockBack/<KnockBackForce>d__4::System.Collections.IEnumerator.Reset()
extern void U3CKnockBackForceU3Ed__4_System_Collections_IEnumerator_Reset_m47372F0870D2409D2DE6167A18AD71A87DAEB865 (void);
// 0x00000148 System.Object KnockBack/<KnockBackForce>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CKnockBackForceU3Ed__4_System_Collections_IEnumerator_get_Current_mEDD1D4006917A23EB05CBB5233BFA4B5A862F5A5 (void);
// 0x00000149 System.Void KnockBack/<KnockBackVelocity>d__5::.ctor(System.Int32)
extern void U3CKnockBackVelocityU3Ed__5__ctor_m273A9340EEED85D5D0F23D3DD6CB12E3BBF8251D (void);
// 0x0000014A System.Void KnockBack/<KnockBackVelocity>d__5::System.IDisposable.Dispose()
extern void U3CKnockBackVelocityU3Ed__5_System_IDisposable_Dispose_m81A90A7CCF9937B6E5FA2ADD74754FF69A02AAE0 (void);
// 0x0000014B System.Boolean KnockBack/<KnockBackVelocity>d__5::MoveNext()
extern void U3CKnockBackVelocityU3Ed__5_MoveNext_m4C846522EAB3E87D91171F41F54362028FF38B31 (void);
// 0x0000014C System.Object KnockBack/<KnockBackVelocity>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKnockBackVelocityU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03FBF3DDF333C9A1FFDB50C61A7932858730F7CA (void);
// 0x0000014D System.Void KnockBack/<KnockBackVelocity>d__5::System.Collections.IEnumerator.Reset()
extern void U3CKnockBackVelocityU3Ed__5_System_Collections_IEnumerator_Reset_mCF0F2E5B6EE7FD0EFE7B0E0851361469FBFCC6B6 (void);
// 0x0000014E System.Object KnockBack/<KnockBackVelocity>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CKnockBackVelocityU3Ed__5_System_Collections_IEnumerator_get_Current_m8017DB713BC252A541546B489EF880C6E7129FD5 (void);
// 0x0000014F System.Void MapController::Start()
extern void MapController_Start_m06F8596FE8E28CF3A47CA2B2AEE3031A7946FB32 (void);
// 0x00000150 System.Void MapController::Update()
extern void MapController_Update_m456861AC51F11BC72FCAAF5A3CF3E81CEE8CFFC8 (void);
// 0x00000151 System.Void MapController::.ctor()
extern void MapController__ctor_m00E8FD02C6C4C34F39E2AE09EDB82BD51066766E (void);
// 0x00000152 System.Void Message::.ctor()
extern void Message__ctor_m4B34B407C9C10627022C55648F679C2844877969 (void);
// 0x00000153 System.Void Chat::Start()
extern void Chat_Start_mB932E30F3890B834E2A2D297926737B08FA18C07 (void);
// 0x00000154 System.Void Chat::Go()
extern void Chat_Go_m60B1459A5D172C027DAEE1427CE70065E10E3E66 (void);
// 0x00000155 System.Void Chat::Update()
extern void Chat_Update_m4849782CFCD60741E334AA0ECF5BD66C02705AE2 (void);
// 0x00000156 System.Void Chat::SelectButtonOnChat()
extern void Chat_SelectButtonOnChat_mE1BC4051EF21E8A3D6A4795D73B52215DFD32A44 (void);
// 0x00000157 System.Void Chat::PlayerConnected()
extern void Chat_PlayerConnected_m64B171664EFE7510B7C643709007B517AE64C399 (void);
// 0x00000158 System.Void Chat::RpcSend(System.String)
extern void Chat_RpcSend_mC5C406683294CD2705AFD2681FDC44900A569C67 (void);
// 0x00000159 System.Void Chat::.ctor()
extern void Chat__ctor_mC00AAE8BD6B361CE8BC543EA5539923C4011A0B0 (void);
// 0x0000015A System.Void Chat::MirrorProcessed()
extern void Chat_MirrorProcessed_m0A609414E768D40F1F982748B13DD3F1C9BB5948 (void);
// 0x0000015B System.Void Chat::UserCode_RpcSend__String(System.String)
extern void Chat_UserCode_RpcSend__String_m3ED066440B8207FEAB8F34498B0EF438969C6A08 (void);
// 0x0000015C System.Void Chat::InvokeUserCode_RpcSend__String(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void Chat_InvokeUserCode_RpcSend__String_mC0EF415C0C85ECAB74E1419400142304D0054DE0 (void);
// 0x0000015D System.Void Chat::.cctor()
extern void Chat__cctor_m0FEA77DB4A8967D83EC47E83E80D34FDEC15255A (void);
// 0x0000015E System.Void ChatIdentity::Start()
extern void ChatIdentity_Start_mB0B95CC29175B778D978A2F02E055B24261B784C (void);
// 0x0000015F System.Void ChatIdentity::OnStartAuthority()
extern void ChatIdentity_OnStartAuthority_m0D6A71308EB42217DCE839D8D440BA3DAE77A123 (void);
// 0x00000160 System.Void ChatIdentity::Say(System.String)
extern void ChatIdentity_Say_m99AF7586D55A99046D6CEE1E43282C0BD076FA22 (void);
// 0x00000161 System.Void ChatIdentity::CmdSend(System.String)
extern void ChatIdentity_CmdSend_mD4792F0DD3C725AFD6D4150FC86C60B137CE7C94 (void);
// 0x00000162 System.Void ChatIdentity::NewGlobalMessage(System.String,Chat/Sender)
extern void ChatIdentity_NewGlobalMessage_m4584BCA44F7E52475478E46F7FE4645F7E6E222C (void);
// 0x00000163 System.Void ChatIdentity::Send(System.String)
extern void ChatIdentity_Send_mBBC7BC727AFC3BDF8950839F54854FE75999CCB0 (void);
// 0x00000164 System.Void ChatIdentity::.ctor()
extern void ChatIdentity__ctor_m4AEF7AE9A28BDF8E1A5B75289588C21CCBD48F62 (void);
// 0x00000165 System.Void ChatIdentity::MirrorProcessed()
extern void ChatIdentity_MirrorProcessed_m78BEAA05B2967C8766543049E852FDDD1D9AF64C (void);
// 0x00000166 System.Void ChatIdentity::UserCode_CmdSend__String(System.String)
extern void ChatIdentity_UserCode_CmdSend__String_m1C2B2912C8F0EB374F7743FF6D6A45449CFFB086 (void);
// 0x00000167 System.Void ChatIdentity::InvokeUserCode_CmdSend__String(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void ChatIdentity_InvokeUserCode_CmdSend__String_m60A55A7B76373FBE8BEF8A74985D8C8F0034A48A (void);
// 0x00000168 System.Void ChatIdentity::.cctor()
extern void ChatIdentity__cctor_m80F11B67D62C0E1A9B582E79E7E58DD0965D1FCE (void);
// 0x00000169 System.Void Connect::Start()
extern void Connect_Start_m6937A4E017D6BECF97AB0D39144BE55C6E321EC4 (void);
// 0x0000016A System.Void Connect::OnLoginUserButtonClick()
extern void Connect_OnLoginUserButtonClick_mD3C8C7BE74945AA030D084E4756119D217A70365 (void);
// 0x0000016B System.Void Connect::StartRemoteServer()
extern void Connect_StartRemoteServer_mC647C300BA80FA7F4AE9D512DC17D01E21AF27BE (void);
// 0x0000016C System.Void Connect::LoginRemoteUser()
extern void Connect_LoginRemoteUser_mD3A9457A05351C33DC1C52A389928F450FEC276B (void);
// 0x0000016D System.String Connect::Encrypt(System.String)
extern void Connect_Encrypt_mA114B0336C310A608595C6B244537AB116B3EB43 (void);
// 0x0000016E System.Void Connect::.ctor()
extern void Connect__ctor_m749BA923DE54DD9B222AA2E24EC33A1223F33FE6 (void);
// 0x0000016F System.Void Net::Start()
extern void Net_Start_m4E68E40073EE4BF450F961DD7FFBEEB78FE9FBA3 (void);
// 0x00000170 System.Void Net::GameScene()
extern void Net_GameScene_mD7C3DACA59DA1B2E6050F882F90FF952842DB5B5 (void);
// 0x00000171 System.Void Net::OnServerAddPlayer(Mirror.NetworkConnectionToClient)
extern void Net_OnServerAddPlayer_m2293E1E6F97ABECD29D407A89413010592C32210 (void);
// 0x00000172 System.Void Net::OnClientConnect()
extern void Net_OnClientConnect_mE4BD88CA16FB498394D424541ACEE1A2B8D4E582 (void);
// 0x00000173 System.Void Net::OnClientDisconnect()
extern void Net_OnClientDisconnect_m80D1D48D6A23C8C1406A080D523E24678D8D260B (void);
// 0x00000174 System.Void Net::OnClientError(System.Exception)
extern void Net_OnClientError_mAE919A7AAB51B81A093D1EFF4D201A113F2AA64D (void);
// 0x00000175 System.Void Net::ErrorUI(System.String)
extern void Net_ErrorUI_m04CB926739519C1573F04B8B3604F64B9CB18066 (void);
// 0x00000176 System.Void Net::OnStartServer()
extern void Net_OnStartServer_m422D8D3B0136363712DED520DA99E474970378C2 (void);
// 0x00000177 System.Void Net::OnServerDisconnect(Mirror.NetworkConnectionToClient)
extern void Net_OnServerDisconnect_m67991EA47F91E8B414AD904BC2D8BA017B2CC076 (void);
// 0x00000178 System.Void Net::OnStopServer()
extern void Net_OnStopServer_m938830849C0F2E617227646986EEB7E9791BBBC2 (void);
// 0x00000179 System.Void Net::ResetPlayerObjectForServer()
extern void Net_ResetPlayerObjectForServer_mAFB9CA3B80FCDDF8FE5AEE5310CA8D0462DE53D6 (void);
// 0x0000017A System.Void Net::ClickReConnectToServer()
extern void Net_ClickReConnectToServer_m6266B931AAA9994BA223E015873B8221777FC724 (void);
// 0x0000017B System.Collections.IEnumerator Net::CheckConnectToServer()
extern void Net_CheckConnectToServer_m7698224F525AF309219980FC327BEF3E55F54157 (void);
// 0x0000017C System.Collections.IEnumerator Net::Synchronization()
extern void Net_Synchronization_m4E59006527622E3D1524DA3DCD31522A3BB83529 (void);
// 0x0000017D System.Collections.IEnumerator Net::RespawnPlayer(UnityEngine.GameObject)
extern void Net_RespawnPlayer_m1C15E5CCA94452B9D26E318B065D21658B5A4C12 (void);
// 0x0000017E System.Void Net::.ctor()
extern void Net__ctor_m4DB9F24446ACD7090CC7AF749439A222AECE06EA (void);
// 0x0000017F System.Void Net/<CheckConnectToServer>d__15::.ctor(System.Int32)
extern void U3CCheckConnectToServerU3Ed__15__ctor_m6A7C06EB201047790347323BB99F9EF73D40FC6C (void);
// 0x00000180 System.Void Net/<CheckConnectToServer>d__15::System.IDisposable.Dispose()
extern void U3CCheckConnectToServerU3Ed__15_System_IDisposable_Dispose_m04F7E5D41509FD51D9B1C0BE4C1DA93E9CD283FD (void);
// 0x00000181 System.Boolean Net/<CheckConnectToServer>d__15::MoveNext()
extern void U3CCheckConnectToServerU3Ed__15_MoveNext_mF0EC8AAF376F1E79BBE8AA44124CBAE68A1D8328 (void);
// 0x00000182 System.Object Net/<CheckConnectToServer>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckConnectToServerU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7F6189864FF038A3E198FD48C6BB6CF42FB8AF9 (void);
// 0x00000183 System.Void Net/<CheckConnectToServer>d__15::System.Collections.IEnumerator.Reset()
extern void U3CCheckConnectToServerU3Ed__15_System_Collections_IEnumerator_Reset_mBF41BA7E451BB36DEC8523754C375A3586F9AB5C (void);
// 0x00000184 System.Object Net/<CheckConnectToServer>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CCheckConnectToServerU3Ed__15_System_Collections_IEnumerator_get_Current_m52036E81CE2C536F6FB74106663605C1FDE814C1 (void);
// 0x00000185 System.Void Net/<Synchronization>d__16::.ctor(System.Int32)
extern void U3CSynchronizationU3Ed__16__ctor_m8977ECABF395C8CF7FDE942FB44A1B81251ECA85 (void);
// 0x00000186 System.Void Net/<Synchronization>d__16::System.IDisposable.Dispose()
extern void U3CSynchronizationU3Ed__16_System_IDisposable_Dispose_mAA1DA48472072ECB3DD2BB56D4399DFDB0B52DAF (void);
// 0x00000187 System.Boolean Net/<Synchronization>d__16::MoveNext()
extern void U3CSynchronizationU3Ed__16_MoveNext_mE57C56AC5BB95D58323F56F90D8335F8A3390372 (void);
// 0x00000188 System.Object Net/<Synchronization>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSynchronizationU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1F322DA317469B334D65CA1E27F06A3AD047819D (void);
// 0x00000189 System.Void Net/<Synchronization>d__16::System.Collections.IEnumerator.Reset()
extern void U3CSynchronizationU3Ed__16_System_Collections_IEnumerator_Reset_m0421FF7BB3650E5A1F2C6C5E85EA98A7189E6C8B (void);
// 0x0000018A System.Object Net/<Synchronization>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CSynchronizationU3Ed__16_System_Collections_IEnumerator_get_Current_m2F8880D5B0C843236F801B1B7E72D86A56758044 (void);
// 0x0000018B System.Void Net/<RespawnPlayer>d__17::.ctor(System.Int32)
extern void U3CRespawnPlayerU3Ed__17__ctor_m54F97976A5AD867EA446F91AB38CC31CE51C648C (void);
// 0x0000018C System.Void Net/<RespawnPlayer>d__17::System.IDisposable.Dispose()
extern void U3CRespawnPlayerU3Ed__17_System_IDisposable_Dispose_m78BBBF329B4BD5D6CD5BD35580A2D156142E174C (void);
// 0x0000018D System.Boolean Net/<RespawnPlayer>d__17::MoveNext()
extern void U3CRespawnPlayerU3Ed__17_MoveNext_mCE493C23022869325C60EC9A1DFF7D6BE76DD53C (void);
// 0x0000018E System.Object Net/<RespawnPlayer>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnPlayerU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5070AAAD19B9DEDBC232A36E7C1800F0C3200B3E (void);
// 0x0000018F System.Void Net/<RespawnPlayer>d__17::System.Collections.IEnumerator.Reset()
extern void U3CRespawnPlayerU3Ed__17_System_Collections_IEnumerator_Reset_m1613E82D8FFCADDD21E22A250B4E9AE59F4849D1 (void);
// 0x00000190 System.Object Net/<RespawnPlayer>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnPlayerU3Ed__17_System_Collections_IEnumerator_get_Current_m55141794AE2E6675F2AB6FB6A5961FF4733D01F5 (void);
// 0x00000191 System.Void OptimizationLogic::Start()
extern void OptimizationLogic_Start_mB67AF52F43586DF9D8E54EA9654D76BB2CF3B2CD (void);
// 0x00000192 UnityEngine.GameObject OptimizationLogic::GetPoolObject(OptimizationLogic/Effects)
extern void OptimizationLogic_GetPoolObject_m985010BEFE471A7BD5694FBB5319C9E16F901151 (void);
// 0x00000193 UnityEngine.GameObject OptimizationLogic::CreateObj(UnityEngine.GameObject)
extern void OptimizationLogic_CreateObj_m8FDDE5BBD51FDE364CD25B9E0781F51CAE6764F1 (void);
// 0x00000194 UnityEngine.GameObject OptimizationLogic::GetPoolObjectForList(System.Collections.Generic.List`1<UnityEngine.GameObject>,OptimizationLogic/Effects)
extern void OptimizationLogic_GetPoolObjectForList_mAC88C0493481FF10D3B49AA755C4BDB755B07978 (void);
// 0x00000195 System.Void OptimizationLogic::.ctor()
extern void OptimizationLogic__ctor_m264720E911452F671F36929D69C3E0B5F1AA8173 (void);
// 0x00000196 System.Void GameOptions::Awake()
extern void GameOptions_Awake_m1885F4E9DFFAA8E8D46E8A8E9A6B4A37C26AFFCF (void);
// 0x00000197 System.Void GameOptions::.ctor()
extern void GameOptions__ctor_m02A2D6501699F1B60BC8E05B4E3536DFD52340A2 (void);
// 0x00000198 System.Void ServerOptions::ServerOptionsStart()
extern void ServerOptions_ServerOptionsStart_m0DF8C2A7E8CE57DB082A2EFACCF9173663813D5C (void);
// 0x00000199 System.Void ServerOptions::LoadingMap()
extern void ServerOptions_LoadingMap_mA6A64B17542EDB20BE57C3C37BED6AE7B3C06C9B (void);
// 0x0000019A System.Void ServerOptions::Update()
extern void ServerOptions_Update_mDD264A743AADEAFBE2AAB615CA7DBC1E8BD33E8C (void);
// 0x0000019B System.Void ServerOptions::ChangeMap()
extern void ServerOptions_ChangeMap_m6CD5F866F2102B385D12C0953AC39CAC4879E9FF (void);
// 0x0000019C System.Void ServerOptions::.ctor()
extern void ServerOptions__ctor_m1F1C8CF2B5ED5652A9B62B2895D382F4203E5D7D (void);
// 0x0000019D System.Void PlayerInfo::.ctor()
extern void PlayerInfo__ctor_m8ED3F2D1B7789917E0A65CEB89D94F10D834E180 (void);
// 0x0000019E System.Void SpecificalOptions::Awake()
extern void SpecificalOptions_Awake_mC5A9CDA708F708780852C0F41566BEF950AD24FC (void);
// 0x0000019F System.Void SpecificalOptions::OnApplicationQuit()
extern void SpecificalOptions_OnApplicationQuit_mDC534A4345E293EC49786DC8AB02BA708F7CFDB1 (void);
// 0x000001A0 System.Void SpecificalOptions::OnEnable()
extern void SpecificalOptions_OnEnable_mCC248E9216B0A2920A57757DF3C99D2E88B7D88A (void);
// 0x000001A1 System.Void SpecificalOptions::OnDisable()
extern void SpecificalOptions_OnDisable_m4AF21E0A1ED77731CC6E09DBC197B9206E574B08 (void);
// 0x000001A2 System.Void SpecificalOptions::SetPlayerPrefsAudio()
extern void SpecificalOptions_SetPlayerPrefsAudio_m709BA57A368373EB1D4D813151936078CD306F59 (void);
// 0x000001A3 System.Void SpecificalOptions::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void SpecificalOptions_OnSceneLoaded_m340EFCAEEDDC2884CC55A4E01DDE3F7E3FBA1E3C (void);
// 0x000001A4 System.Void SpecificalOptions::.ctor()
extern void SpecificalOptions__ctor_m68614D68BB3134035873D23833B43D211A4F649B (void);
// 0x000001A5 System.Void CameraPlayer::Start()
extern void CameraPlayer_Start_mE391D1AC69C915ABE6470144A780DA71AB61E95E (void);
// 0x000001A6 System.Void CameraPlayer::FixedUpdate()
extern void CameraPlayer_FixedUpdate_m732E904A8B1E30A3276C5D7EFEB836BC590F7857 (void);
// 0x000001A7 System.Void CameraPlayer::ChangePlayerTarget(UnityEngine.GameObject)
extern void CameraPlayer_ChangePlayerTarget_mCB4639A21A74BC6AF460FEF69D43B30E18E82C5D (void);
// 0x000001A8 System.Void CameraPlayer::OnDrawGizmos()
extern void CameraPlayer_OnDrawGizmos_m7780147BB2BB6A8677006F545A3CA4C1CDD640AE (void);
// 0x000001A9 System.Void CameraPlayer::.ctor()
extern void CameraPlayer__ctor_m35E2A08BDC9B80BD06677095BF3B4795693130EF (void);
// 0x000001AA System.Single ControllerPlayer::get_GetMinTimerDelayDetect()
extern void ControllerPlayer_get_GetMinTimerDelayDetect_mD8747EFDED7E2ED70386829400BA0CF460F56858 (void);
// 0x000001AB System.Single ControllerPlayer::get_GetMaxTimerDelayDetect()
extern void ControllerPlayer_get_GetMaxTimerDelayDetect_m741D6C2194D4EB51ADDB8B00BB6283E2E7EEE704 (void);
// 0x000001AC System.Single ControllerPlayer::get_GetRadiusPlayerDetect()
extern void ControllerPlayer_get_GetRadiusPlayerDetect_m3E9BFDB83C38F2914AC1BEF805444D05650A2A5E (void);
// 0x000001AD System.Single ControllerPlayer::get_GetLookSpeedNormal()
extern void ControllerPlayer_get_GetLookSpeedNormal_mC580A4F3188D2826D0DADCEECCC5079ADAF2A44B (void);
// 0x000001AE System.Single ControllerPlayer::get_GetLookSpeedDanger()
extern void ControllerPlayer_get_GetLookSpeedDanger_m1C264B1170B3AF1E5C76B4A7D21AEEFA276A5FB6 (void);
// 0x000001AF System.Void ControllerPlayer::Start()
extern void ControllerPlayer_Start_m56DEC6E3650E6C1971323481B53F7909E2AF2E6A (void);
// 0x000001B0 System.Void ControllerPlayer::OnEnable()
extern void ControllerPlayer_OnEnable_m2DBA56DBE38D05CB3AC8877FE5E5E2137D895E4F (void);
// 0x000001B1 System.Void ControllerPlayer::OnDisable()
extern void ControllerPlayer_OnDisable_m7A2D9297B58D3DDFCC3CD4BA7DA01D6502102084 (void);
// 0x000001B2 System.Void ControllerPlayer::FixedUpdate()
extern void ControllerPlayer_FixedUpdate_m577EC0E3A34D81D26D4E48302096763E44ADB369 (void);
// 0x000001B3 System.Void ControllerPlayer::PlayerSpeed()
extern void ControllerPlayer_PlayerSpeed_mE44D0CA80A191872F223CFC8B3DEF6249649236D (void);
// 0x000001B4 System.Void ControllerPlayer::Look(UnityEngine.Vector2,System.Single)
extern void ControllerPlayer_Look_mB12CAA9593012A41343C7F0A6589509DE4A93579 (void);
// 0x000001B5 System.Collections.IEnumerator ControllerPlayer::PlayerDanger()
extern void ControllerPlayer_PlayerDanger_m8FA57CA9B1512D0ED0152531CABA0EC135A3DFDF (void);
// 0x000001B6 System.Void ControllerPlayer::ChangeTargetMove(UnityEngine.Vector2)
extern void ControllerPlayer_ChangeTargetMove_mD675BC293E4BEA98571977335CBA6417C26AF071 (void);
// 0x000001B7 System.Void ControllerPlayer::Spawn(UnityEngine.GameObject,System.Int32)
extern void ControllerPlayer_Spawn_mB1BE92677B5FB0122EFA4D17ADD6F9854F4FAA83 (void);
// 0x000001B8 System.Void ControllerPlayer::OnBecameVisible()
extern void ControllerPlayer_OnBecameVisible_m4A3C7C3C1F544E843741E8B83BD4FA3B9F399A8C (void);
// 0x000001B9 System.Void ControllerPlayer::OnBecameInvisible()
extern void ControllerPlayer_OnBecameInvisible_m9B1A17403932161CABF34CCC158F9609E4E5D733 (void);
// 0x000001BA System.Void ControllerPlayer::CmdChangeTargetMove(UnityEngine.Vector2)
extern void ControllerPlayer_CmdChangeTargetMove_mC52908508B647F73191DE56BD6A98CEA75298205 (void);
// 0x000001BB System.Void ControllerPlayer::.ctor()
extern void ControllerPlayer__ctor_m3720DF1D4DADE6167033F4A6E4D114EB74700159 (void);
// 0x000001BC System.Void ControllerPlayer::MirrorProcessed()
extern void ControllerPlayer_MirrorProcessed_m8B3D87F31A08AF5AB95AAD8C3FC49B2E9DCDE709 (void);
// 0x000001BD System.Single ControllerPlayer::get_Networkspeed()
extern void ControllerPlayer_get_Networkspeed_m2AFBC3E1BADE3E9F81E436106A311396333687D8 (void);
// 0x000001BE System.Void ControllerPlayer::set_Networkspeed(System.Single)
extern void ControllerPlayer_set_Networkspeed_m8052B51DD8F51BBBBE90BD6EFF571E2D7A007052 (void);
// 0x000001BF UnityEngine.Vector2 ControllerPlayer::get_NetworkTargetMove()
extern void ControllerPlayer_get_NetworkTargetMove_m28E04A08B9F00E90B94D344A690BB35A61D5E9DA (void);
// 0x000001C0 System.Void ControllerPlayer::set_NetworkTargetMove(UnityEngine.Vector2)
extern void ControllerPlayer_set_NetworkTargetMove_m9BE0FC88BBB89EC34B54BB51E69A7F2B2AD8E070 (void);
// 0x000001C1 System.Void ControllerPlayer::UserCode_CmdChangeTargetMove__Vector2(UnityEngine.Vector2)
extern void ControllerPlayer_UserCode_CmdChangeTargetMove__Vector2_mAA7EFDBB558863B40B2868421834ABAE76E52DD3 (void);
// 0x000001C2 System.Void ControllerPlayer::InvokeUserCode_CmdChangeTargetMove__Vector2(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void ControllerPlayer_InvokeUserCode_CmdChangeTargetMove__Vector2_m149EBCF339F5F0849F2CF5E34992735CBAF736B1 (void);
// 0x000001C3 System.Void ControllerPlayer::.cctor()
extern void ControllerPlayer__cctor_m11E26AAB6304212AB47C47EE8D0A491E336E9243 (void);
// 0x000001C4 System.Boolean ControllerPlayer::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void ControllerPlayer_SerializeSyncVars_mB95CFF5D55BAEF6117DE2467F095959E9787435E (void);
// 0x000001C5 System.Void ControllerPlayer::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void ControllerPlayer_DeserializeSyncVars_m0D76207E61E25822417E171B0EDA7D21E1509D50 (void);
// 0x000001C6 System.Void ControllerPlayer/<PlayerDanger>d__33::.ctor(System.Int32)
extern void U3CPlayerDangerU3Ed__33__ctor_m35DFC7F5B0AC0217441F028C58112F1D87135A7E (void);
// 0x000001C7 System.Void ControllerPlayer/<PlayerDanger>d__33::System.IDisposable.Dispose()
extern void U3CPlayerDangerU3Ed__33_System_IDisposable_Dispose_mBC37BF23A78A621A7B88E4F72D364F6B890074AC (void);
// 0x000001C8 System.Boolean ControllerPlayer/<PlayerDanger>d__33::MoveNext()
extern void U3CPlayerDangerU3Ed__33_MoveNext_mBFCE85DC08948B7B3BC19DF39F79235F0FA878F9 (void);
// 0x000001C9 System.Object ControllerPlayer/<PlayerDanger>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayerDangerU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3807726B0D4FC2B664D39A0D4485A5788F5B966 (void);
// 0x000001CA System.Void ControllerPlayer/<PlayerDanger>d__33::System.Collections.IEnumerator.Reset()
extern void U3CPlayerDangerU3Ed__33_System_Collections_IEnumerator_Reset_m4F8795CD90BA79F15D90A1F1AEDE98788B6FDF23 (void);
// 0x000001CB System.Object ControllerPlayer/<PlayerDanger>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CPlayerDangerU3Ed__33_System_Collections_IEnumerator_get_Current_m853CDDA78A9CD40A3F80E0C391D8103C9CD90B28 (void);
// 0x000001CC System.Void IndicatorPlayer::Update()
extern void IndicatorPlayer_Update_m349A12509FE2DDB6F15B3D0091D8EC2AA5D3FD89 (void);
// 0x000001CD System.Void IndicatorPlayer::ChangeIndicator(IndicatorPlayer/Indicator)
extern void IndicatorPlayer_ChangeIndicator_m53D6E24AAA48F72DD68F8669A7F6FA5DF0796091 (void);
// 0x000001CE System.Void IndicatorPlayer::OnBecameVisible()
extern void IndicatorPlayer_OnBecameVisible_m9E4D078030471B9072E6AA3A1B2143EE72DE77EB (void);
// 0x000001CF System.Void IndicatorPlayer::OnBecameInvisible()
extern void IndicatorPlayer_OnBecameInvisible_mCB2A7C9F8530F50178C47BB99CA55F9C0FBC3813 (void);
// 0x000001D0 System.Void IndicatorPlayer::.ctor()
extern void IndicatorPlayer__ctor_mA9003AA6A5476A1947A0B70352376A4315BCEFAA (void);
// 0x000001D1 System.Int32 Player::get_GetPlayerID()
extern void Player_get_GetPlayerID_m30AC015EBE34384BBB2555F9668F51D80AED8FE6 (void);
// 0x000001D2 System.Void Player::Start()
extern void Player_Start_mC31CF4F40DDEA35C5E39E8C43EC37284AE7C453D (void);
// 0x000001D3 System.Void Player::OnEnable()
extern void Player_OnEnable_mE0997C2EDE52E3BC53CCBF961D4FE375347F0906 (void);
// 0x000001D4 System.Void Player::OnDisable()
extern void Player_OnDisable_m37872CE0A672A116C94A05662826159004ECB59C (void);
// 0x000001D5 System.Void Player::Update()
extern void Player_Update_m95E134A5EF1B5164EA281F61D7FA436F59BE3C9F (void);
// 0x000001D6 System.Void Player::NewRound()
extern void Player_NewRound_m6BC047292E6EEC6124341733988CDA7F8C43B58E (void);
// 0x000001D7 System.Void Player::Spawn(UnityEngine.GameObject,System.Int32)
extern void Player_Spawn_m6D393268D6ADFF434E3ADBC808410E3B3965C6E3 (void);
// 0x000001D8 System.Void Player::TakeDamege(UnityEngine.GameObject,UnityEngine.GameObject,System.Int32)
extern void Player_TakeDamege_m3A146034BFF4B1940D3CA7F05A2864502C7746EC (void);
// 0x000001D9 System.Void Player::Death(UnityEngine.GameObject,System.Int32)
extern void Player_Death_m473F6D4E542F8EB6AC1E7A56B87B299960E040D8 (void);
// 0x000001DA System.Collections.IEnumerator Player::PainShockEnd()
extern void Player_PainShockEnd_m66B8A40C0D1AB54A2F232CD8D5B6D5D41C6141C1 (void);
// 0x000001DB System.Collections.IEnumerator Player::RegenerationHP()
extern void Player_RegenerationHP_m960CAA6BC95D9D70C9B1746F1CD68475E6F99A02 (void);
// 0x000001DC System.Void Player::CmdChangeNickName(System.String)
extern void Player_CmdChangeNickName_mFB1560B417DCD16484F4897AE5655D48F3101A59 (void);
// 0x000001DD System.Void Player::.ctor()
extern void Player__ctor_m0A83E0706592FC871B0CF188B37AFC6649F3D85D (void);
// 0x000001DE System.Void Player::MirrorProcessed()
extern void Player_MirrorProcessed_mCFB97F456345EC354F716A07C5F1AA47FF9A38A7 (void);
// 0x000001DF System.Int32 Player::get_NetworkRandomSpawn()
extern void Player_get_NetworkRandomSpawn_mE687ACD85FB375FE3834B14615AAD3FAFAD149BF (void);
// 0x000001E0 System.Void Player::set_NetworkRandomSpawn(System.Int32)
extern void Player_set_NetworkRandomSpawn_m7CCF87166E40D30EC7C98CB0EA0B3572ABD63B68 (void);
// 0x000001E1 System.Int32 Player::get_NetworkRandomWeaponPrimary()
extern void Player_get_NetworkRandomWeaponPrimary_m0F8EB67E923DC2AE73D6458AAC0E45825979A486 (void);
// 0x000001E2 System.Void Player::set_NetworkRandomWeaponPrimary(System.Int32)
extern void Player_set_NetworkRandomWeaponPrimary_m9995196F3FD54CF6B93E313C645C66ADD1EB5B1E (void);
// 0x000001E3 System.Int32 Player::get_NetworkRandomWeaponSecond()
extern void Player_get_NetworkRandomWeaponSecond_m579D97655F5211E2459DE27B20425C63DC31B7C7 (void);
// 0x000001E4 System.Void Player::set_NetworkRandomWeaponSecond(System.Int32)
extern void Player_set_NetworkRandomWeaponSecond_mEEB7EBE1C89FC26733CF104083AA3E408BDBA050 (void);
// 0x000001E5 System.Int32 Player::get_NetworkRandomWeaponGrenade()
extern void Player_get_NetworkRandomWeaponGrenade_mA35170C6138469BFEA9BA41551C0792AE4862A1F (void);
// 0x000001E6 System.Void Player::set_NetworkRandomWeaponGrenade(System.Int32)
extern void Player_set_NetworkRandomWeaponGrenade_mC8D36429735A27FCF453809676E546C6D8F4FC4C (void);
// 0x000001E7 Player/Team Player::get_NetworkPlayerTeam()
extern void Player_get_NetworkPlayerTeam_mA8386FACF79E149E8BB94BEEF5253FDD2FF4D199 (void);
// 0x000001E8 System.Void Player::set_NetworkPlayerTeam(Player/Team)
extern void Player_set_NetworkPlayerTeam_m73300B6D92D01B1619B7693D2BF119C0397FA4ED (void);
// 0x000001E9 System.Int32 Player::get_NetworkHealth()
extern void Player_get_NetworkHealth_m92A0B62B8D005E6E002E68ADA8DAB57CD32D79A9 (void);
// 0x000001EA System.Void Player::set_NetworkHealth(System.Int32)
extern void Player_set_NetworkHealth_m4489A1B611D6DF762F420AE6397449D6D2DAA45B (void);
// 0x000001EB System.Int32 Player::get_NetworkArmor()
extern void Player_get_NetworkArmor_m1A66E3E4A48701F97FAA2279441C10009AACF1FA (void);
// 0x000001EC System.Void Player::set_NetworkArmor(System.Int32)
extern void Player_set_NetworkArmor_m20B729D5E0F89E61282955A50D4DD1C47DE0640D (void);
// 0x000001ED System.Boolean Player::get_NetworkIsAlive()
extern void Player_get_NetworkIsAlive_m1C1C74C68DC7BF3AF163A2124619DA290A8B846D (void);
// 0x000001EE System.Void Player::set_NetworkIsAlive(System.Boolean)
extern void Player_set_NetworkIsAlive_mAF47289BAF8A12F522067409D5E91BB02A38EE51 (void);
// 0x000001EF System.Int32 Player::get_NetworkZombieSelect()
extern void Player_get_NetworkZombieSelect_m63703B70AAC85B5DCF4F903A788ACD6055D69460 (void);
// 0x000001F0 System.Void Player::set_NetworkZombieSelect(System.Int32)
extern void Player_set_NetworkZombieSelect_mAD4B80471EB9FFE66A9D126C86E147072B25AE9C (void);
// 0x000001F1 System.Int32 Player::get_NetworkHumanSelect()
extern void Player_get_NetworkHumanSelect_m0E1B7E4039EDFA4DE97C8008F3EA36D5ACAF2745 (void);
// 0x000001F2 System.Void Player::set_NetworkHumanSelect(System.Int32)
extern void Player_set_NetworkHumanSelect_m8F4BFE8BA24D2C6C0C0B0450C5894547EF251258 (void);
// 0x000001F3 System.Int32 Player::get_NetworkActiveWeapon()
extern void Player_get_NetworkActiveWeapon_mAA566E04C7E0B6B301C9F62DBC0B6BBEC25A707E (void);
// 0x000001F4 System.Void Player::set_NetworkActiveWeapon(System.Int32)
extern void Player_set_NetworkActiveWeapon_m11E6B71B0737DEF8261957D396575D9B8337637E (void);
// 0x000001F5 System.Void Player::UserCode_CmdChangeNickName__String(System.String)
extern void Player_UserCode_CmdChangeNickName__String_m8B28931926E4AAA899E48023F4A7F91757B320DC (void);
// 0x000001F6 System.Void Player::InvokeUserCode_CmdChangeNickName__String(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void Player_InvokeUserCode_CmdChangeNickName__String_mA3C74685F88B9F301ACCF64A42CB19335EC9859B (void);
// 0x000001F7 System.Void Player::.cctor()
extern void Player__cctor_m0083B3C3F6B4BD82C21F3694C66578096CE88056 (void);
// 0x000001F8 System.Boolean Player::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void Player_SerializeSyncVars_m5C4CBC51C7FACE68E643DADD507593565E98B837 (void);
// 0x000001F9 System.Void Player::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void Player_DeserializeSyncVars_mCEF21E6BE12F24DB432A0F7C76CCC34300FB5826 (void);
// 0x000001FA System.Void Player/<PainShockEnd>d__59::.ctor(System.Int32)
extern void U3CPainShockEndU3Ed__59__ctor_mC57116471909E82E33A1CD2FF550E600DAC51A6E (void);
// 0x000001FB System.Void Player/<PainShockEnd>d__59::System.IDisposable.Dispose()
extern void U3CPainShockEndU3Ed__59_System_IDisposable_Dispose_mFAC0436D92442D84B64553F65902CE8D2D869265 (void);
// 0x000001FC System.Boolean Player/<PainShockEnd>d__59::MoveNext()
extern void U3CPainShockEndU3Ed__59_MoveNext_m673BBBC8CA7E377185045D33CB34A150489E53D9 (void);
// 0x000001FD System.Object Player/<PainShockEnd>d__59::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPainShockEndU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED116ED94FBD1AE37B3D7C83D970E39883A81B8F (void);
// 0x000001FE System.Void Player/<PainShockEnd>d__59::System.Collections.IEnumerator.Reset()
extern void U3CPainShockEndU3Ed__59_System_Collections_IEnumerator_Reset_m08FD0B421C5ABAE8F87B105B513339CC636F543D (void);
// 0x000001FF System.Object Player/<PainShockEnd>d__59::System.Collections.IEnumerator.get_Current()
extern void U3CPainShockEndU3Ed__59_System_Collections_IEnumerator_get_Current_mCD9B09C0F74A05453A375495B74704EC24A15806 (void);
// 0x00000200 System.Void Player/<RegenerationHP>d__60::.ctor(System.Int32)
extern void U3CRegenerationHPU3Ed__60__ctor_m62BC6AD141602A0FC0A3CCDDCB07FDCF15021380 (void);
// 0x00000201 System.Void Player/<RegenerationHP>d__60::System.IDisposable.Dispose()
extern void U3CRegenerationHPU3Ed__60_System_IDisposable_Dispose_mA264057531FAE4ED23F337EC8686775BBA79A864 (void);
// 0x00000202 System.Boolean Player/<RegenerationHP>d__60::MoveNext()
extern void U3CRegenerationHPU3Ed__60_MoveNext_m7E766866283495320E4891C03240C4840AC6DAFC (void);
// 0x00000203 System.Object Player/<RegenerationHP>d__60::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRegenerationHPU3Ed__60_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3F2C7B58801AC5BD80866F8836087DAB37DA8F8 (void);
// 0x00000204 System.Void Player/<RegenerationHP>d__60::System.Collections.IEnumerator.Reset()
extern void U3CRegenerationHPU3Ed__60_System_Collections_IEnumerator_Reset_m78602A0FE4A196E68A3E1C1769174F5E060CFFDE (void);
// 0x00000205 System.Object Player/<RegenerationHP>d__60::System.Collections.IEnumerator.get_Current()
extern void U3CRegenerationHPU3Ed__60_System_Collections_IEnumerator_get_Current_mAB5DCA38B5DCD430D04D36362F16C47CE6EC8A47 (void);
// 0x00000206 System.Void ButtonUI::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ButtonUI_OnPointerDown_mF438F4563D2BAD01189C3691FDB967AD17B3367E (void);
// 0x00000207 System.Void ButtonUI::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void ButtonUI_OnPointerUp_mEB5E7813722A474E1B61F4CC15B6CE4E79D401CF (void);
// 0x00000208 System.Void ButtonUI::.ctor()
extern void ButtonUI__ctor_m76E2C33B8629D3C1BBE5F0FB391CEF0BEAF1612C (void);
// 0x00000209 System.Void FPS::Start()
extern void FPS_Start_mA6545EFC3FF91D5CEBB3267F4C5EBE7C3E9E7008 (void);
// 0x0000020A System.Void FPS::Update()
extern void FPS_Update_m72ED9FF60A51EC6A4E6F5A8C57AA942FF886D745 (void);
// 0x0000020B System.Void FPS::.ctor()
extern void FPS__ctor_m78558469A6F3BEC1D5C8412D1C93DF93E28930E3 (void);
// 0x0000020C System.Void Joystic::Start()
extern void Joystic_Start_m500E5442955741D999D999B7CFF5AAC768977FAC (void);
// 0x0000020D System.Void Joystic::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Joystic_OnPointerDown_m6380E23C2D3986A5560C8D83CE950F56BA3E5781 (void);
// 0x0000020E System.Void Joystic::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystic_OnDrag_m46518C26668217BD804452DC55D0A0B918A647CD (void);
// 0x0000020F System.Void Joystic::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Joystic_OnPointerUp_mA415F304ABCA609433E913EECC5F56B86EDB8406 (void);
// 0x00000210 System.Void Joystic::Update()
extern void Joystic_Update_mDC90924B55A781B384D518FA1784E989B18D53D9 (void);
// 0x00000211 System.Void Joystic::Reset()
extern void Joystic_Reset_m2C80B43512407FF519473221FE5B32E3A77B2574 (void);
// 0x00000212 System.Void Joystic::.ctor()
extern void Joystic__ctor_m403D214D36872DCA408EDC87B74FF073DA2E6C33 (void);
// 0x00000213 System.Void ProgressBarPlayer::.ctor()
extern void ProgressBarPlayer__ctor_mCF4587216BB8DF021895D8A7B14198EB094F5677 (void);
// 0x00000214 System.Void UI::Start()
extern void UI_Start_m36F3E11677AB5677BD3A76B5865E90CD3609183A (void);
// 0x00000215 System.Void UI::OnEnable()
extern void UI_OnEnable_m44B9420109C4449103DE83E15A90586994BF473A (void);
// 0x00000216 System.Void UI::OnDisable()
extern void UI_OnDisable_mF1CCC77FA9FD9663F40B471055D7C3005D1EC7CD (void);
// 0x00000217 System.Void UI::Update()
extern void UI_Update_m7F9212980C571870E0700BAB400E09111370F6ED (void);
// 0x00000218 System.Void UI::NewRound()
extern void UI_NewRound_m9294525460481E99636D11167A7BA8DB71402F55 (void);
// 0x00000219 System.Void UI::Hud(Player)
extern void UI_Hud_m30D04E6DDAB479948800BC82511415874426170D (void);
// 0x0000021A System.Void UI::SpectrMode()
extern void UI_SpectrMode_mB66E941648ED8D2F2FA37335F54B1BCCEB16EF0D (void);
// 0x0000021B System.Void UI::PlayerSpawn(UnityEngine.GameObject,System.Int32)
extern void UI_PlayerSpawn_mB08FFFF223BE5249179F3C08F2F925B99A418C8B (void);
// 0x0000021C System.Void UI::PlayerInfection(UnityEngine.GameObject,System.Int32)
extern void UI_PlayerInfection_m24FB3BEF736EE685C0F48B8F688E7CD783B8EC52 (void);
// 0x0000021D System.Void UI::.ctor()
extern void UI__ctor_m177FCA8E7C6A148BFF6FED9F758B3396F25FDFBB (void);
// 0x0000021E System.Void UI::MirrorProcessed()
extern void UI_MirrorProcessed_mF86C288D8CDA8B535BDDEBA0C7B4E55FD136E9D1 (void);
// 0x0000021F System.Void AuthManager::Start()
extern void AuthManager_Start_m97E5064CC9D561CF5F02ED40A5DA54C780D590DD (void);
// 0x00000220 System.Void AuthManager::Update()
extern void AuthManager_Update_m482A7E3DBC24C89F9DCD039E01BD79F3055917F0 (void);
// 0x00000221 System.Void AuthManager::Awake()
extern void AuthManager_Awake_mAD76EDE68AFA86FCF349B3F047D17CF221DB624D (void);
// 0x00000222 System.Void AuthManager::InitializeFirebase()
extern void AuthManager_InitializeFirebase_m4A4D9283F5DA4ED0D8B2B789070ED0657D94B831 (void);
// 0x00000223 System.Void AuthManager::AuthStateChanged(System.Object,System.EventArgs)
extern void AuthManager_AuthStateChanged_mEC7BFA976CB597A17ED7486BB92E421A5ACEFF5E (void);
// 0x00000224 System.Void AuthManager::SingIn()
extern void AuthManager_SingIn_mC4262C873C15468E1B2E22066184F2B01E3E90DC (void);
// 0x00000225 System.Void AuthManager::LogIn()
extern void AuthManager_LogIn_mEAE77B8BD10E42314A1CB85382D04121B4FC34DB (void);
// 0x00000226 System.Void AuthManager::ResetIn()
extern void AuthManager_ResetIn_m2241F281C5FD94CE762E4393DED873D298BD67A2 (void);
// 0x00000227 System.Void AuthManager::SignUp()
extern void AuthManager_SignUp_mB1A08A8EB30FCDD4CE72FC8725E8EA26711C306F (void);
// 0x00000228 System.Void AuthManager::LogUp()
extern void AuthManager_LogUp_m5746BF25FDA5FF6FCCC74F14F7F6C0ECD6F507DE (void);
// 0x00000229 System.Void AuthManager::ResetUp()
extern void AuthManager_ResetUp_m817B985488F50C8EDB49A6A1892BAC15FCBF26EF (void);
// 0x0000022A System.Void AuthManager::ChangeProfilPlayer()
extern void AuthManager_ChangeProfilPlayer_mDED19B138B9CD052EAAC8B9528B50FCF3C164B21 (void);
// 0x0000022B System.Void AuthManager::ChangePassword()
extern void AuthManager_ChangePassword_mB42517058966986640C3D1FC49AB4817017AF24E (void);
// 0x0000022C System.Void AuthManager::ChangeEmail()
extern void AuthManager_ChangeEmail_mC08BBFB6A892E1E1E6E24039E740B3E02149E25F (void);
// 0x0000022D System.Void AuthManager::LogUpAutomatically()
extern void AuthManager_LogUpAutomatically_m341A6D033697EBE43360C694F4BCE4C7BE17CA4F (void);
// 0x0000022E System.Void AuthManager::SignOut()
extern void AuthManager_SignOut_mF62FD633E2FC42E07C6D1FC221C5B62ED57E9807 (void);
// 0x0000022F System.Collections.IEnumerator AuthManager::CheckAutologin()
extern void AuthManager_CheckAutologin_mB2CB954E6ED7BDD4199E3C2A7043FCCAB5810275 (void);
// 0x00000230 System.Void AuthManager::AutoLogin()
extern void AuthManager_AutoLogin_m4200EC18286CA1905B1812300A89FA1F87FF3053 (void);
// 0x00000231 System.Void AuthManager::MainMenuStart()
extern void AuthManager_MainMenuStart_m1529DBC84E352EA0FAADE4F595377B234067815B (void);
// 0x00000232 System.Collections.IEnumerator AuthManager::Login(System.String,System.String)
extern void AuthManager_Login_mF1FF5FD0DCEF9FDFAE7E881FE6F5DA0AD4B107B8 (void);
// 0x00000233 System.Collections.IEnumerator AuthManager::Register(System.String,System.String,System.String)
extern void AuthManager_Register_m0CD751F7BE9882772B934A2C3894509EA486A4B0 (void);
// 0x00000234 System.Collections.IEnumerator AuthManager::SignOutUser()
extern void AuthManager_SignOutUser_m4102DBABBB637A0825191621BEF4A940C05B01D7 (void);
// 0x00000235 System.Collections.IEnumerator AuthManager::SendVerificationEmail()
extern void AuthManager_SendVerificationEmail_m2EA21C7918E2A6F573A3263E2DA06798ACEDB027 (void);
// 0x00000236 System.Collections.IEnumerator AuthManager::SendEmailResetPassword(System.String)
extern void AuthManager_SendEmailResetPassword_m94357B7222F00CDB75D00BE4709AC54BF33E44A2 (void);
// 0x00000237 System.Collections.IEnumerator AuthManager::UpdateProfile(System.String,System.String)
extern void AuthManager_UpdateProfile_m3B8998F884CCB9FB3F1E0DA84DC6FBDC59816D9C (void);
// 0x00000238 System.Collections.IEnumerator AuthManager::UpdateEmail(System.String)
extern void AuthManager_UpdateEmail_m1460CFBCFE4BF77A786E7E376E39615DB3E58333 (void);
// 0x00000239 System.Collections.IEnumerator AuthManager::UpdatePassword(System.String)
extern void AuthManager_UpdatePassword_mF05DB411F217C8FF9E917A958652698D1980737F (void);
// 0x0000023A System.Collections.IEnumerator AuthManager::AuthErrorActive(System.String)
extern void AuthManager_AuthErrorActive_mA3DD4D0267BB03C7E1F4291595A7277E48DBB14E (void);
// 0x0000023B System.Collections.IEnumerator AuthManager::ChangeAuthErrorActive(System.String)
extern void AuthManager_ChangeAuthErrorActive_mBE4974C9C8218A6A1D6A8ACB9E5A9944BA55B141 (void);
// 0x0000023C System.String AuthManager::GetErrorMessage(System.Exception)
extern void AuthManager_GetErrorMessage_m5962051FFC6E74EF15FBF5F73A0E87E9C79A1E48 (void);
// 0x0000023D System.String AuthManager::GetErrorMessage(Firebase.Auth.AuthError)
extern void AuthManager_GetErrorMessage_m88A26CAB3670464277B5ECD479EA1375BA9C86E6 (void);
// 0x0000023E System.Void AuthManager::.ctor()
extern void AuthManager__ctor_m86FDEE5047527DE532CFA45A0E04C31249157A6A (void);
// 0x0000023F System.Void AuthManager::<Awake>b__24_0(System.Threading.Tasks.Task`1<Firebase.DependencyStatus>)
extern void AuthManager_U3CAwakeU3Eb__24_0_m3C280BE7B1D692D7CBCB73DCE1BE8F948AB71086 (void);
// 0x00000240 System.Void AuthManager/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m5C6549002CFF26EA07E18A707839725ACE200D83 (void);
// 0x00000241 System.Boolean AuthManager/<>c__DisplayClass38_0::<CheckAutologin>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CCheckAutologinU3Eb__0_mD99C2A48375E755A2A28694759754CF6BA01C579 (void);
// 0x00000242 System.Void AuthManager/<CheckAutologin>d__38::.ctor(System.Int32)
extern void U3CCheckAutologinU3Ed__38__ctor_mC81665B6F9B1436A214574AD3D21DEC353CFC6E0 (void);
// 0x00000243 System.Void AuthManager/<CheckAutologin>d__38::System.IDisposable.Dispose()
extern void U3CCheckAutologinU3Ed__38_System_IDisposable_Dispose_mE639042F8AC2139C0E64FE55FF05CEDC6A2C6CDA (void);
// 0x00000244 System.Boolean AuthManager/<CheckAutologin>d__38::MoveNext()
extern void U3CCheckAutologinU3Ed__38_MoveNext_m498118F3F0E2235AEF4C91E9B7BC45C2E4B53EE5 (void);
// 0x00000245 System.Object AuthManager/<CheckAutologin>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckAutologinU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA405A2D7A98480B524959E27FAE9448455D12C9F (void);
// 0x00000246 System.Void AuthManager/<CheckAutologin>d__38::System.Collections.IEnumerator.Reset()
extern void U3CCheckAutologinU3Ed__38_System_Collections_IEnumerator_Reset_m0978E02EE23A5D8BF237CC3A525203CD6DE2D2C1 (void);
// 0x00000247 System.Object AuthManager/<CheckAutologin>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CCheckAutologinU3Ed__38_System_Collections_IEnumerator_get_Current_mAF917D184CCFC0E2ABC0B9D22B45ACD33536E1DE (void);
// 0x00000248 System.Void AuthManager/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_mD2BF48B7141F645B0C9A6CE700C1B192DA432F3E (void);
// 0x00000249 System.Boolean AuthManager/<>c__DisplayClass41_0::<Login>b__0()
extern void U3CU3Ec__DisplayClass41_0_U3CLoginU3Eb__0_m70253CAC9C05A58B525FA5BAE0AB5321D08E719F (void);
// 0x0000024A System.Void AuthManager/<Login>d__41::.ctor(System.Int32)
extern void U3CLoginU3Ed__41__ctor_mD7C8E88B30B117F2969CF81B6BEA81E226A93468 (void);
// 0x0000024B System.Void AuthManager/<Login>d__41::System.IDisposable.Dispose()
extern void U3CLoginU3Ed__41_System_IDisposable_Dispose_m23AA0C16C3F8F34E45664B610C57423D8A4E35C1 (void);
// 0x0000024C System.Boolean AuthManager/<Login>d__41::MoveNext()
extern void U3CLoginU3Ed__41_MoveNext_mA7AE25B99D8AB45AA1B4FB1A2BBFD622399EC192 (void);
// 0x0000024D System.Object AuthManager/<Login>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoginU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5796593F57EBC70501B9CC212C2FD7F67C42EACB (void);
// 0x0000024E System.Void AuthManager/<Login>d__41::System.Collections.IEnumerator.Reset()
extern void U3CLoginU3Ed__41_System_Collections_IEnumerator_Reset_mE0D601D4266AC0328B62A774414C95E90E712D4B (void);
// 0x0000024F System.Object AuthManager/<Login>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CLoginU3Ed__41_System_Collections_IEnumerator_get_Current_m43D8D1611C5C677D6FB0B96CA2892B00F13B0B65 (void);
// 0x00000250 System.Void AuthManager/<>c__DisplayClass42_0::.ctor()
extern void U3CU3Ec__DisplayClass42_0__ctor_m77863F0C9B59CBEEE678590D4DA36AB89C16FDFD (void);
// 0x00000251 System.Boolean AuthManager/<>c__DisplayClass42_0::<Register>b__0()
extern void U3CU3Ec__DisplayClass42_0_U3CRegisterU3Eb__0_mCDD7364D53BCDF4D9E595C1C9FC3E9ABDB61A01C (void);
// 0x00000252 System.Void AuthManager/<>c__DisplayClass42_1::.ctor()
extern void U3CU3Ec__DisplayClass42_1__ctor_mBB5C576C3B015A82BAFCEFE2A411AE79378FC26B (void);
// 0x00000253 System.Boolean AuthManager/<>c__DisplayClass42_1::<Register>b__1()
extern void U3CU3Ec__DisplayClass42_1_U3CRegisterU3Eb__1_mF09D3AC7F5675FC3245FD44B9A2D616D34B64D96 (void);
// 0x00000254 System.Void AuthManager/<Register>d__42::.ctor(System.Int32)
extern void U3CRegisterU3Ed__42__ctor_mD2BB1D429927D21C59BD9D898F69131362F997E0 (void);
// 0x00000255 System.Void AuthManager/<Register>d__42::System.IDisposable.Dispose()
extern void U3CRegisterU3Ed__42_System_IDisposable_Dispose_m60A18E42318DFE076DCA1472E6A2D1BEFAF11729 (void);
// 0x00000256 System.Boolean AuthManager/<Register>d__42::MoveNext()
extern void U3CRegisterU3Ed__42_MoveNext_mCA8695326C91D3618225715313FB4AF28B8CA2BE (void);
// 0x00000257 System.Object AuthManager/<Register>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRegisterU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A96022E0CE1F302520C834120290D006868F898 (void);
// 0x00000258 System.Void AuthManager/<Register>d__42::System.Collections.IEnumerator.Reset()
extern void U3CRegisterU3Ed__42_System_Collections_IEnumerator_Reset_mF2CF7331A567F6154F1646A220FE0761C9F5C565 (void);
// 0x00000259 System.Object AuthManager/<Register>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CRegisterU3Ed__42_System_Collections_IEnumerator_get_Current_mEFCF61E44FB08CD3980E911DFD283CB1B975C1E8 (void);
// 0x0000025A System.Void AuthManager/<SignOutUser>d__43::.ctor(System.Int32)
extern void U3CSignOutUserU3Ed__43__ctor_m7AF2FE95FB2D59B9E754DB1F4726CF2A52FDD3FB (void);
// 0x0000025B System.Void AuthManager/<SignOutUser>d__43::System.IDisposable.Dispose()
extern void U3CSignOutUserU3Ed__43_System_IDisposable_Dispose_m006875FA63A705277D670FA68BFB2A8CBD98CFA9 (void);
// 0x0000025C System.Boolean AuthManager/<SignOutUser>d__43::MoveNext()
extern void U3CSignOutUserU3Ed__43_MoveNext_mE6703A6668E3B2979FC7534AC7211B8CD7BF6253 (void);
// 0x0000025D System.Object AuthManager/<SignOutUser>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSignOutUserU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m06ACB6E1E6BCAADB0C5F3BA67F3AB4B46378A9E2 (void);
// 0x0000025E System.Void AuthManager/<SignOutUser>d__43::System.Collections.IEnumerator.Reset()
extern void U3CSignOutUserU3Ed__43_System_Collections_IEnumerator_Reset_m88908E66867BA6B4690525FE0F701D5EA923C20F (void);
// 0x0000025F System.Object AuthManager/<SignOutUser>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CSignOutUserU3Ed__43_System_Collections_IEnumerator_get_Current_m442644530018780CEB682F52CFEB0F61199F0B12 (void);
// 0x00000260 System.Void AuthManager/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m627B73A4D48F6AB116E47F8B5F957AC0402D2EC9 (void);
// 0x00000261 System.Boolean AuthManager/<>c__DisplayClass44_0::<SendVerificationEmail>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CSendVerificationEmailU3Eb__0_mBDB6187C93744EE020BC1272E53570621D9320F6 (void);
// 0x00000262 System.Void AuthManager/<SendVerificationEmail>d__44::.ctor(System.Int32)
extern void U3CSendVerificationEmailU3Ed__44__ctor_m5C54FE487B46478D8C516CAF9E59BC43DF4EB692 (void);
// 0x00000263 System.Void AuthManager/<SendVerificationEmail>d__44::System.IDisposable.Dispose()
extern void U3CSendVerificationEmailU3Ed__44_System_IDisposable_Dispose_m412F1C5062FE68461A015CC9BF0254421A595412 (void);
// 0x00000264 System.Boolean AuthManager/<SendVerificationEmail>d__44::MoveNext()
extern void U3CSendVerificationEmailU3Ed__44_MoveNext_m2301BD9DACE144DFA68DD53F093DE511CFC8A8FC (void);
// 0x00000265 System.Object AuthManager/<SendVerificationEmail>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendVerificationEmailU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCA75C6FA9841A50F5708C698C80C306C8BD5C337 (void);
// 0x00000266 System.Void AuthManager/<SendVerificationEmail>d__44::System.Collections.IEnumerator.Reset()
extern void U3CSendVerificationEmailU3Ed__44_System_Collections_IEnumerator_Reset_mA72C0B89C7DFA5D646495D58F74E54D75CD7D380 (void);
// 0x00000267 System.Object AuthManager/<SendVerificationEmail>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CSendVerificationEmailU3Ed__44_System_Collections_IEnumerator_get_Current_m001C0D9CFAD17F8F5295CD1145FF50615A2C0C74 (void);
// 0x00000268 System.Void AuthManager/<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_mE0217B46E93186604428805E5B99B80855975999 (void);
// 0x00000269 System.Boolean AuthManager/<>c__DisplayClass45_0::<SendEmailResetPassword>b__0()
extern void U3CU3Ec__DisplayClass45_0_U3CSendEmailResetPasswordU3Eb__0_m9EDB9F24886FD93D20E4DB7B6BB71E1E43023FD7 (void);
// 0x0000026A System.Void AuthManager/<SendEmailResetPassword>d__45::.ctor(System.Int32)
extern void U3CSendEmailResetPasswordU3Ed__45__ctor_m757642BC766586B6287ADF6EB0876B83D7FA9DD3 (void);
// 0x0000026B System.Void AuthManager/<SendEmailResetPassword>d__45::System.IDisposable.Dispose()
extern void U3CSendEmailResetPasswordU3Ed__45_System_IDisposable_Dispose_m37647C9E309BD7F74F41C8E5EF8AB4C1E00DCA95 (void);
// 0x0000026C System.Boolean AuthManager/<SendEmailResetPassword>d__45::MoveNext()
extern void U3CSendEmailResetPasswordU3Ed__45_MoveNext_m303FF866785CB59F0E1E398AF67CAC0CCCEB2B14 (void);
// 0x0000026D System.Object AuthManager/<SendEmailResetPassword>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendEmailResetPasswordU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44DBBBD44B4354DAE3B4C35857C75486B10774AE (void);
// 0x0000026E System.Void AuthManager/<SendEmailResetPassword>d__45::System.Collections.IEnumerator.Reset()
extern void U3CSendEmailResetPasswordU3Ed__45_System_Collections_IEnumerator_Reset_mFF9480AE1A71224B2C03372A35DCCA92C07D2186 (void);
// 0x0000026F System.Object AuthManager/<SendEmailResetPassword>d__45::System.Collections.IEnumerator.get_Current()
extern void U3CSendEmailResetPasswordU3Ed__45_System_Collections_IEnumerator_get_Current_m79F279C1999CEF3612353B65B16DF549E6FA4EF6 (void);
// 0x00000270 System.Void AuthManager/<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_m950B3D710D3F545E0D3987DA0A069E1F6C74194E (void);
// 0x00000271 System.Boolean AuthManager/<>c__DisplayClass46_0::<UpdateProfile>b__0()
extern void U3CU3Ec__DisplayClass46_0_U3CUpdateProfileU3Eb__0_mC68B9F81721D44F21AAC0F3A631CA2EFDDDCEE5C (void);
// 0x00000272 System.Void AuthManager/<UpdateProfile>d__46::.ctor(System.Int32)
extern void U3CUpdateProfileU3Ed__46__ctor_m0DA4679A5EC520B73C1306FDBF04AACD8F2F4133 (void);
// 0x00000273 System.Void AuthManager/<UpdateProfile>d__46::System.IDisposable.Dispose()
extern void U3CUpdateProfileU3Ed__46_System_IDisposable_Dispose_mE6FEC49BBBED4DE5217B8F567D78B1CEA234DBBC (void);
// 0x00000274 System.Boolean AuthManager/<UpdateProfile>d__46::MoveNext()
extern void U3CUpdateProfileU3Ed__46_MoveNext_m9BA808CA8F31EB4C6F99CD927CBABC9D3B40A3E0 (void);
// 0x00000275 System.Object AuthManager/<UpdateProfile>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateProfileU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD6B6B2B74E31A686EAC9A35867981BDB145DEDF8 (void);
// 0x00000276 System.Void AuthManager/<UpdateProfile>d__46::System.Collections.IEnumerator.Reset()
extern void U3CUpdateProfileU3Ed__46_System_Collections_IEnumerator_Reset_mDD319D639D94A339CEB4D81D8F50D99788AE0028 (void);
// 0x00000277 System.Object AuthManager/<UpdateProfile>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateProfileU3Ed__46_System_Collections_IEnumerator_get_Current_mE39741D0E6655303119F2B6C573A7FE16EB3E3AB (void);
// 0x00000278 System.Void AuthManager/<>c__DisplayClass47_0::.ctor()
extern void U3CU3Ec__DisplayClass47_0__ctor_m074A21CF02D9B78611BEFFB63E2B451C138ADF70 (void);
// 0x00000279 System.Boolean AuthManager/<>c__DisplayClass47_0::<UpdateEmail>b__0()
extern void U3CU3Ec__DisplayClass47_0_U3CUpdateEmailU3Eb__0_m64D387E6E2F4F0EEAB2228FD57EE3524BA3781A2 (void);
// 0x0000027A System.Void AuthManager/<UpdateEmail>d__47::.ctor(System.Int32)
extern void U3CUpdateEmailU3Ed__47__ctor_m5CE201273557713D8C0CEEB9BAB82CBD96F6E610 (void);
// 0x0000027B System.Void AuthManager/<UpdateEmail>d__47::System.IDisposable.Dispose()
extern void U3CUpdateEmailU3Ed__47_System_IDisposable_Dispose_m6B221ECE5290EA17C11DFA3778FFCF65ADCC2CF3 (void);
// 0x0000027C System.Boolean AuthManager/<UpdateEmail>d__47::MoveNext()
extern void U3CUpdateEmailU3Ed__47_MoveNext_m4BFB6B2F23F3ED16C0E1A720E8FFE2D2F161BCB5 (void);
// 0x0000027D System.Object AuthManager/<UpdateEmail>d__47::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateEmailU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50EF9FDD445F0C71D5A7523F66F6AB43E34BC375 (void);
// 0x0000027E System.Void AuthManager/<UpdateEmail>d__47::System.Collections.IEnumerator.Reset()
extern void U3CUpdateEmailU3Ed__47_System_Collections_IEnumerator_Reset_m58C4308C2013210D6D2157E4F9DFC0D06824B5BD (void);
// 0x0000027F System.Object AuthManager/<UpdateEmail>d__47::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateEmailU3Ed__47_System_Collections_IEnumerator_get_Current_m59344BC1E05FF12290FEA0B9E02C8D5A438D1CFE (void);
// 0x00000280 System.Void AuthManager/<>c__DisplayClass48_0::.ctor()
extern void U3CU3Ec__DisplayClass48_0__ctor_mE8825A59F9E005BC70C239833F75C45565BDF6A8 (void);
// 0x00000281 System.Boolean AuthManager/<>c__DisplayClass48_0::<UpdatePassword>b__0()
extern void U3CU3Ec__DisplayClass48_0_U3CUpdatePasswordU3Eb__0_m29BAF83052654B25F7C02BF69407D858D3E4F49E (void);
// 0x00000282 System.Void AuthManager/<UpdatePassword>d__48::.ctor(System.Int32)
extern void U3CUpdatePasswordU3Ed__48__ctor_mAA88E72D4668D9B07073BE1DAD60735C39FC5B0E (void);
// 0x00000283 System.Void AuthManager/<UpdatePassword>d__48::System.IDisposable.Dispose()
extern void U3CUpdatePasswordU3Ed__48_System_IDisposable_Dispose_mDC8909F8221BE4610B673BC4995B60C7E16DA98E (void);
// 0x00000284 System.Boolean AuthManager/<UpdatePassword>d__48::MoveNext()
extern void U3CUpdatePasswordU3Ed__48_MoveNext_mF861008803A1C37A0B434F53FC0C463F7C80432F (void);
// 0x00000285 System.Object AuthManager/<UpdatePassword>d__48::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdatePasswordU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FBABE4C7FF4B09D68F84ABBB5A5BBFD2DB43703 (void);
// 0x00000286 System.Void AuthManager/<UpdatePassword>d__48::System.Collections.IEnumerator.Reset()
extern void U3CUpdatePasswordU3Ed__48_System_Collections_IEnumerator_Reset_mE1198FF4C8A04E34A168BAC8E263304CDC811EB3 (void);
// 0x00000287 System.Object AuthManager/<UpdatePassword>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CUpdatePasswordU3Ed__48_System_Collections_IEnumerator_get_Current_m01608B9CA30675CBE7757A308E31F11C55D499EE (void);
// 0x00000288 System.Void AuthManager/<AuthErrorActive>d__49::.ctor(System.Int32)
extern void U3CAuthErrorActiveU3Ed__49__ctor_mFFBC47D6C8C8EE7B5EA163B9902006C95C5E948E (void);
// 0x00000289 System.Void AuthManager/<AuthErrorActive>d__49::System.IDisposable.Dispose()
extern void U3CAuthErrorActiveU3Ed__49_System_IDisposable_Dispose_m2EEF93DF611A6839B57995362B5C9FFD4641D547 (void);
// 0x0000028A System.Boolean AuthManager/<AuthErrorActive>d__49::MoveNext()
extern void U3CAuthErrorActiveU3Ed__49_MoveNext_m8CB391EE29ABEFAD615E4B6FF912C61B1BCB5A71 (void);
// 0x0000028B System.Object AuthManager/<AuthErrorActive>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAuthErrorActiveU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF7FE852369288B0D8B12988B6457E618B0BAD583 (void);
// 0x0000028C System.Void AuthManager/<AuthErrorActive>d__49::System.Collections.IEnumerator.Reset()
extern void U3CAuthErrorActiveU3Ed__49_System_Collections_IEnumerator_Reset_mD85CF991E5A1B7AA728FFA15A26A753A53127D2D (void);
// 0x0000028D System.Object AuthManager/<AuthErrorActive>d__49::System.Collections.IEnumerator.get_Current()
extern void U3CAuthErrorActiveU3Ed__49_System_Collections_IEnumerator_get_Current_mADFF12D3685EB688EF69AB6882A42D8223EB34EE (void);
// 0x0000028E System.Void AuthManager/<ChangeAuthErrorActive>d__50::.ctor(System.Int32)
extern void U3CChangeAuthErrorActiveU3Ed__50__ctor_m34A47F826214243C0907412D72EA4582C9263CCC (void);
// 0x0000028F System.Void AuthManager/<ChangeAuthErrorActive>d__50::System.IDisposable.Dispose()
extern void U3CChangeAuthErrorActiveU3Ed__50_System_IDisposable_Dispose_mDE1896D88DAF41D3E2A3EEA90049CFC26B74A14B (void);
// 0x00000290 System.Boolean AuthManager/<ChangeAuthErrorActive>d__50::MoveNext()
extern void U3CChangeAuthErrorActiveU3Ed__50_MoveNext_mC04ABF94EFEFDAE672B9A5605224D97CEB62B402 (void);
// 0x00000291 System.Object AuthManager/<ChangeAuthErrorActive>d__50::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangeAuthErrorActiveU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m171D3454F5EC31FBCE4C2A06242B46ECA904E011 (void);
// 0x00000292 System.Void AuthManager/<ChangeAuthErrorActive>d__50::System.Collections.IEnumerator.Reset()
extern void U3CChangeAuthErrorActiveU3Ed__50_System_Collections_IEnumerator_Reset_m3353920BED247E05E8A8842E19CCC4AA1D05502C (void);
// 0x00000293 System.Object AuthManager/<ChangeAuthErrorActive>d__50::System.Collections.IEnumerator.get_Current()
extern void U3CChangeAuthErrorActiveU3Ed__50_System_Collections_IEnumerator_get_Current_m5F0AFC3D87064D259F4F64647BD68E165CD8BAA5 (void);
// 0x00000294 System.Void ButtonManager::Start()
extern void ButtonManager_Start_m1031E938A2FB8D85F993AA7DF29D06DAF0CDF930 (void);
// 0x00000295 System.Void ButtonManager::ClickBack(UnityEngine.UI.Button)
extern void ButtonManager_ClickBack_m0002732635A63052989846BED837A7B0911A25AD (void);
// 0x00000296 System.Void ButtonManager::ClickButtonGroupSelect(UnityEngine.UI.Button)
extern void ButtonManager_ClickButtonGroupSelect_mDA32E75E33587B0B2C1D43F09B4358A579614DE2 (void);
// 0x00000297 System.Void ButtonManager::SliderTrigger(System.Boolean)
extern void ButtonManager_SliderTrigger_m89DC503FE0C253F2D40F77016E2E520C07E9F0BA (void);
// 0x00000298 System.Void ButtonManager::ClickChangePallet(UnityEngine.GameObject)
extern void ButtonManager_ClickChangePallet_m9A2509CE56135FA16D2DFF4055416B9E007E6618 (void);
// 0x00000299 System.Void ButtonManager::OutEndAnimation()
extern void ButtonManager_OutEndAnimation_m66FF610E7A3E821FFF5B9447E25F7D8257FE5274 (void);
// 0x0000029A System.Collections.IEnumerator ButtonManager::StartChangePallet(UnityEngine.GameObject)
extern void ButtonManager_StartChangePallet_m2742BB38D0B570101CAEBA3E0CCA85B1B61EF891 (void);
// 0x0000029B System.Void ButtonManager::.ctor()
extern void ButtonManager__ctor_m7DBD91D2AF27494F6AC7DC74DE679919AB1DD71F (void);
// 0x0000029C System.Void ButtonManager/<StartChangePallet>d__13::.ctor(System.Int32)
extern void U3CStartChangePalletU3Ed__13__ctor_mF756C84C7C30361C69DEF892C73B1DFDA0561F01 (void);
// 0x0000029D System.Void ButtonManager/<StartChangePallet>d__13::System.IDisposable.Dispose()
extern void U3CStartChangePalletU3Ed__13_System_IDisposable_Dispose_m3AB6D9B337287CD393B345141A4D758BB5302615 (void);
// 0x0000029E System.Boolean ButtonManager/<StartChangePallet>d__13::MoveNext()
extern void U3CStartChangePalletU3Ed__13_MoveNext_m20747389C99D9FF7D1C5DC8E5885EE2771D72251 (void);
// 0x0000029F System.Object ButtonManager/<StartChangePallet>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartChangePalletU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31E23F424F24BC59FC51444B25D2D12B440DD9A1 (void);
// 0x000002A0 System.Void ButtonManager/<StartChangePallet>d__13::System.Collections.IEnumerator.Reset()
extern void U3CStartChangePalletU3Ed__13_System_Collections_IEnumerator_Reset_m81E4A239130CDF3791CA184E686D9C812BB24B73 (void);
// 0x000002A1 System.Object ButtonManager/<StartChangePallet>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CStartChangePalletU3Ed__13_System_Collections_IEnumerator_get_Current_m38E85BB85CE1CA5B76A84B62FD4AE9DB3FD28493 (void);
// 0x000002A2 System.Void MainMenuControl::Start()
extern void MainMenuControl_Start_m5A990AA29D2379423480E21FD217E06CFA1DB7E4 (void);
// 0x000002A3 System.Void MainMenuControl::OnEnable()
extern void MainMenuControl_OnEnable_m9F8939ECC76B39D9E500C334B9AB8A1796523D79 (void);
// 0x000002A4 System.Void MainMenuControl::OnDisable()
extern void MainMenuControl_OnDisable_m770BC4441ED271511900D310E94F3501E7664A90 (void);
// 0x000002A5 System.Void MainMenuControl::GameStartConnect()
extern void MainMenuControl_GameStartConnect_mA95A58AAC0CE87FEFBD3B137F897788A7D319AE0 (void);
// 0x000002A6 System.Void MainMenuControl::GameStartHost()
extern void MainMenuControl_GameStartHost_m13FCBFDCE985465367DEF82E6A4024FA06DE457D (void);
// 0x000002A7 System.Void MainMenuControl::Update()
extern void MainMenuControl_Update_m4F18C263EA6A5D1515F465ACBB42F6010DEF91EF (void);
// 0x000002A8 System.Void MainMenuControl::UpdateCountPlayers()
extern void MainMenuControl_UpdateCountPlayers_m0A40191194D38DE7679EBFE8CF4B4F0C393668A4 (void);
// 0x000002A9 System.Void MainMenuControl::ClickBack()
extern void MainMenuControl_ClickBack_m61132060455D361C68ED0A02ED106A9B74537D60 (void);
// 0x000002AA System.Void MainMenuControl::ClickCLoseAllWindow()
extern void MainMenuControl_ClickCLoseAllWindow_mEFE7428D9B7DDBF7EE2CBD3573CA15DE2FAD1B2F (void);
// 0x000002AB System.Void MainMenuControl::ClickAddWindow(UnityEngine.GameObject)
extern void MainMenuControl_ClickAddWindow_m14C01F1B483F13A22D3B4446D4894513D5CAD1B2 (void);
// 0x000002AC System.Void MainMenuControl::ClickOpenTelegram()
extern void MainMenuControl_ClickOpenTelegram_mB532914DC4706441881D9136FC371C0EBC985F9E (void);
// 0x000002AD System.Void MainMenuControl::ClickOpenVK()
extern void MainMenuControl_ClickOpenVK_m8D974E9492763A4A4FE4F933F32BE1D6D95B6D02 (void);
// 0x000002AE System.Void MainMenuControl::ClickOpenDiscordServer()
extern void MainMenuControl_ClickOpenDiscordServer_m063B6651D3ACD6BCC37724957404B857A63AF873 (void);
// 0x000002AF System.Void MainMenuControl::AddWindows(UnityEngine.GameObject)
extern void MainMenuControl_AddWindows_m667081B4BF5D0969D8359CCDE0B4A3D8704BEB97 (void);
// 0x000002B0 System.Void MainMenuControl::DelWindows(System.Int32)
extern void MainMenuControl_DelWindows_mEA75916FC791F50DBD80B450BECB5474890C230B (void);
// 0x000002B1 System.Void MainMenuControl::OutEndAnimation()
extern void MainMenuControl_OutEndAnimation_m9D6F6DB72FCD5BF4F8D20A418AD27437C427748D (void);
// 0x000002B2 System.Void MainMenuControl::ButtonPlayAnimation(UnityEngine.UI.Button)
extern void MainMenuControl_ButtonPlayAnimation_mC5F9B7674407F439F148DA7B5448940D19EB0A46 (void);
// 0x000002B3 System.Collections.IEnumerator MainMenuControl::ConnectingToServer()
extern void MainMenuControl_ConnectingToServer_m9815C9BB7C63A4E6CF256D7B7D1862EEFD49A7A2 (void);
// 0x000002B4 System.Collections.IEnumerator MainMenuControl::HostToServer()
extern void MainMenuControl_HostToServer_mEBC8D45D87D56935EA9002FC3BC13561ED9904C6 (void);
// 0x000002B5 System.Collections.IEnumerator MainMenuControl::ConnectingToLRM()
extern void MainMenuControl_ConnectingToLRM_m031A8E0AF2774195BB4DC23979C7873E0963FF52 (void);
// 0x000002B6 System.Collections.IEnumerator MainMenuControl::CountOnlinePlayers()
extern void MainMenuControl_CountOnlinePlayers_m75EC274D561D4A318B136654536455FB4C4B2E6E (void);
// 0x000002B7 System.Void MainMenuControl::ClickReConnectRelay()
extern void MainMenuControl_ClickReConnectRelay_m09AFB3388898EEF625A5FEFE453D5856D591624C (void);
// 0x000002B8 System.Void MainMenuControl::ClickReConnectServer()
extern void MainMenuControl_ClickReConnectServer_mFEBE38B2C485D110533CE06E71E2D4635A48D770 (void);
// 0x000002B9 System.Void MainMenuControl::ClickExitGame()
extern void MainMenuControl_ClickExitGame_mA1BFCE023E61AFEEFE872E1543FE1486CA7725B3 (void);
// 0x000002BA System.Void MainMenuControl::ClickResetWindows()
extern void MainMenuControl_ClickResetWindows_m1C0A7ACAD38662501DB670582454FD86A01E8EB2 (void);
// 0x000002BB System.Void MainMenuControl::.ctor()
extern void MainMenuControl__ctor_mE98B75947C78BB3D9BE987B56E5B6DAADC24063F (void);
// 0x000002BC System.Void MainMenuControl/<ConnectingToServer>d__35::.ctor(System.Int32)
extern void U3CConnectingToServerU3Ed__35__ctor_m433961DBFFC8B29B9FC91DEAAB6FA84DCAEB2542 (void);
// 0x000002BD System.Void MainMenuControl/<ConnectingToServer>d__35::System.IDisposable.Dispose()
extern void U3CConnectingToServerU3Ed__35_System_IDisposable_Dispose_mC0128468E20E7A9048CA3F747C65D55F9714E6D6 (void);
// 0x000002BE System.Boolean MainMenuControl/<ConnectingToServer>d__35::MoveNext()
extern void U3CConnectingToServerU3Ed__35_MoveNext_m362E82EE2D14AB570D960DF55403FA13006E9091 (void);
// 0x000002BF System.Object MainMenuControl/<ConnectingToServer>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CConnectingToServerU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m916BDF74A0BF3FF7D6F6EAB675CE3365447DB6BE (void);
// 0x000002C0 System.Void MainMenuControl/<ConnectingToServer>d__35::System.Collections.IEnumerator.Reset()
extern void U3CConnectingToServerU3Ed__35_System_Collections_IEnumerator_Reset_m6C300616782C1B47071BA1E44101D49E29864B06 (void);
// 0x000002C1 System.Object MainMenuControl/<ConnectingToServer>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CConnectingToServerU3Ed__35_System_Collections_IEnumerator_get_Current_m399ADB384FA7DF8E5E8D0B75E463E301E3C58867 (void);
// 0x000002C2 System.Void MainMenuControl/<HostToServer>d__36::.ctor(System.Int32)
extern void U3CHostToServerU3Ed__36__ctor_m4CFC93BE8001B38E6B4224AAABE23B128A575309 (void);
// 0x000002C3 System.Void MainMenuControl/<HostToServer>d__36::System.IDisposable.Dispose()
extern void U3CHostToServerU3Ed__36_System_IDisposable_Dispose_m9A19C5632AADB17F029407A23C4E538DD957B6BC (void);
// 0x000002C4 System.Boolean MainMenuControl/<HostToServer>d__36::MoveNext()
extern void U3CHostToServerU3Ed__36_MoveNext_m25E74AD52EEEF02DD5598402AA692C2CFD235C17 (void);
// 0x000002C5 System.Object MainMenuControl/<HostToServer>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHostToServerU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22A38B6EBE276F63F8B72721BAC4D3F768305851 (void);
// 0x000002C6 System.Void MainMenuControl/<HostToServer>d__36::System.Collections.IEnumerator.Reset()
extern void U3CHostToServerU3Ed__36_System_Collections_IEnumerator_Reset_m01F45D77FEC0C7844D9DD4E2298360FEE574177C (void);
// 0x000002C7 System.Object MainMenuControl/<HostToServer>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CHostToServerU3Ed__36_System_Collections_IEnumerator_get_Current_m7450718E3367504B463D65A815BA476ECF4E2823 (void);
// 0x000002C8 System.Void MainMenuControl/<ConnectingToLRM>d__37::.ctor(System.Int32)
extern void U3CConnectingToLRMU3Ed__37__ctor_mFB518AE4EB735116FFF09EA3E40EF10A3CD242B0 (void);
// 0x000002C9 System.Void MainMenuControl/<ConnectingToLRM>d__37::System.IDisposable.Dispose()
extern void U3CConnectingToLRMU3Ed__37_System_IDisposable_Dispose_m8434195EFDD6629268A5826B0F8C9AC3B2FC684A (void);
// 0x000002CA System.Boolean MainMenuControl/<ConnectingToLRM>d__37::MoveNext()
extern void U3CConnectingToLRMU3Ed__37_MoveNext_m6C422959F75983C76A13AC9FF8E863321D397D9B (void);
// 0x000002CB System.Object MainMenuControl/<ConnectingToLRM>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CConnectingToLRMU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCA86DB31BFB9CF05B0BD58546A5896CB39FFB614 (void);
// 0x000002CC System.Void MainMenuControl/<ConnectingToLRM>d__37::System.Collections.IEnumerator.Reset()
extern void U3CConnectingToLRMU3Ed__37_System_Collections_IEnumerator_Reset_mE9D55DAA28D3A4B8DF7D0A51B2E231B868696618 (void);
// 0x000002CD System.Object MainMenuControl/<ConnectingToLRM>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CConnectingToLRMU3Ed__37_System_Collections_IEnumerator_get_Current_m8E2AA08B940F0F334DA6EB90CBE19C7167707568 (void);
// 0x000002CE System.Void MainMenuControl/<CountOnlinePlayers>d__38::.ctor(System.Int32)
extern void U3CCountOnlinePlayersU3Ed__38__ctor_mA12C83277514599CB1F8BA620FB116395AC1E84E (void);
// 0x000002CF System.Void MainMenuControl/<CountOnlinePlayers>d__38::System.IDisposable.Dispose()
extern void U3CCountOnlinePlayersU3Ed__38_System_IDisposable_Dispose_mC94ADECBE3C7B0DDDBA3720468D8A549B089AD8A (void);
// 0x000002D0 System.Boolean MainMenuControl/<CountOnlinePlayers>d__38::MoveNext()
extern void U3CCountOnlinePlayersU3Ed__38_MoveNext_mF099087BE6178F294172AC700E0AF903FCF7ABD6 (void);
// 0x000002D1 System.Object MainMenuControl/<CountOnlinePlayers>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCountOnlinePlayersU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCFB03C179CB78D57B46CF35A8060CC89772AC970 (void);
// 0x000002D2 System.Void MainMenuControl/<CountOnlinePlayers>d__38::System.Collections.IEnumerator.Reset()
extern void U3CCountOnlinePlayersU3Ed__38_System_Collections_IEnumerator_Reset_m5F7BB953FB2413303FAF8989F82FE1D072439091 (void);
// 0x000002D3 System.Object MainMenuControl/<CountOnlinePlayers>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CCountOnlinePlayersU3Ed__38_System_Collections_IEnumerator_get_Current_mBEA02B72DB4677596562B14C14F9D5A44AB1EF02 (void);
// 0x000002D4 System.Void PalleteWindow::ActivePallete(UnityEngine.GameObject)
extern void PalleteWindow_ActivePallete_m271CA227AFA80571686F2B2C0097C8D587964A4D (void);
// 0x000002D5 System.Void PalleteWindow::.ctor()
extern void PalleteWindow__ctor_mE94FEADFBEF02B5CC6DF427BF3714896CB517CF6 (void);
// 0x000002D6 System.Void PauseMenu::Start()
extern void PauseMenu_Start_m4BBF1E67B42A2E936C792E83778ADCD87DE3C80E (void);
// 0x000002D7 System.Void PauseMenu::Update()
extern void PauseMenu_Update_m5097E74BFD4385B73BA9EF7198886DCEC7DF9A83 (void);
// 0x000002D8 System.Void PauseMenu::OpenPauseMenu()
extern void PauseMenu_OpenPauseMenu_m0E0CC7533A8CBC96C8834E9C76C3E706466E496D (void);
// 0x000002D9 System.Void PauseMenu::ClosePauseMenu()
extern void PauseMenu_ClosePauseMenu_m3219C11348E15C30E63C2F6CEC824ECED86A3529 (void);
// 0x000002DA System.Void PauseMenu::Back()
extern void PauseMenu_Back_mE8BF9FE1496D39D84FD49595FC0E13456C035E68 (void);
// 0x000002DB System.Void PauseMenu::ClickWindowSettingsMenu()
extern void PauseMenu_ClickWindowSettingsMenu_mFB868B201C44AEA0BC39473FD47E4BEA3BA9D0E8 (void);
// 0x000002DC System.Void PauseMenu::ClickWindowSettingsAdvanceMenu()
extern void PauseMenu_ClickWindowSettingsAdvanceMenu_m6D2EA061912D92E875C452A8BBBA6949EC594B37 (void);
// 0x000002DD System.Void PauseMenu::ClickWindowSettingsKeyControlMenu()
extern void PauseMenu_ClickWindowSettingsKeyControlMenu_m1C3F422553096BBFE527F0E1BF27A0D7259FEDAB (void);
// 0x000002DE System.Void PauseMenu::AddWindows(UnityEngine.GameObject,System.Boolean)
extern void PauseMenu_AddWindows_mF5F7FD662E0128F1645BD4E6F61D4BE3E4744D73 (void);
// 0x000002DF System.Void PauseMenu::DelWindows(System.Int32)
extern void PauseMenu_DelWindows_mAE992E139D2CB4E18072BBDA89C0A0573102EBB1 (void);
// 0x000002E0 System.Void PauseMenu::OpenMainMenu()
extern void PauseMenu_OpenMainMenu_m87B5B52024C75DE90EE17FE84822C7BF7AF822ED (void);
// 0x000002E1 System.Void PauseMenu::.ctor()
extern void PauseMenu__ctor_m81B0E020DC5008DA4D414200BAAF7122B430D826 (void);
// 0x000002E2 System.Void CreateServer::OnEnable()
extern void CreateServer_OnEnable_mFF3131AD614D24B0B16C2BCBBB193213C034E4B1 (void);
// 0x000002E3 System.Void CreateServer::Update()
extern void CreateServer_Update_m91FB4B6FE5CA66CD6A767673D75FF800F3DE695B (void);
// 0x000002E4 System.Void CreateServer::ScrollView(UnityEngine.Transform,UnityEngine.UI.Scrollbar,System.Single&,System.Int32&)
extern void CreateServer_ScrollView_mFF5D45DAF5E777B96E7D102DC8DA9A295BF8FF84 (void);
// 0x000002E5 System.Void CreateServer::ClickLeftMapsScroll()
extern void CreateServer_ClickLeftMapsScroll_mDE52B63B7C97F67FB9F245534A7450BCA7E33A41 (void);
// 0x000002E6 System.Void CreateServer::ClickRightMapsScroll()
extern void CreateServer_ClickRightMapsScroll_m59D294E5E2D29296902F695828A82FED07F01E2A (void);
// 0x000002E7 System.Void CreateServer::ClickLeftModeScroll()
extern void CreateServer_ClickLeftModeScroll_mAE33E06DBD56B79E0422026E82011FEA0EA7F0D4 (void);
// 0x000002E8 System.Void CreateServer::ClickRightModeScroll()
extern void CreateServer_ClickRightModeScroll_mCB389617A86D403925EFA251957D2BB598ED8BF5 (void);
// 0x000002E9 System.Void CreateServer::StartHostGame()
extern void CreateServer_StartHostGame_m7A2CDCEE4B4310CE1D990E24379BB9029F35A572 (void);
// 0x000002EA System.Void CreateServer::ChangeValueCountsBot(System.Single)
extern void CreateServer_ChangeValueCountsBot_m52A6A966183A41804923786F8512BCA70FBFF573 (void);
// 0x000002EB System.Void CreateServer::ChangeValueCountsPlayers(System.Single)
extern void CreateServer_ChangeValueCountsPlayers_m82D6D766477333F1205BF35BF9DC5D61CC99B345 (void);
// 0x000002EC System.Void CreateServer::EditInputCountBots()
extern void CreateServer_EditInputCountBots_m0CEB3A23FC4C618AE0F52803B984EAE09EE2649E (void);
// 0x000002ED System.Void CreateServer::EditInputCountPlayers()
extern void CreateServer_EditInputCountPlayers_mC2D5E21020FD1406FA68C6788AB50B03BED3EED4 (void);
// 0x000002EE System.Void CreateServer::CheckCount(TMPro.TMP_InputField,UnityEngine.UI.Slider)
extern void CreateServer_CheckCount_mF33BAB8B7992DE12F780B62092711B6D76EB3581 (void);
// 0x000002EF System.Void CreateServer::.ctor()
extern void CreateServer__ctor_m5F7B30DFB1AB3E6E145D1B66DE75EDC81DDC430D (void);
// 0x000002F0 System.Void ServerBrowser::OnEnable()
extern void ServerBrowser_OnEnable_m6C01059CC8F8BDA8F7E2B43F27A1FC674BC304FC (void);
// 0x000002F1 System.Void ServerBrowser::OnDisable()
extern void ServerBrowser_OnDisable_mB8AF8DC7EDC61FDA825CF5E84423A4D09013BB2E (void);
// 0x000002F2 System.Void ServerBrowser::RefreshServerList()
extern void ServerBrowser_RefreshServerList_m3F6AFFE1B9E995B55B2EAC15CE6105E7EADB48D4 (void);
// 0x000002F3 System.Void ServerBrowser::ServerListUpdate()
extern void ServerBrowser_ServerListUpdate_m44068C4908946B58DC0D59DEE8ACBA96497CFC1B (void);
// 0x000002F4 System.Void ServerBrowser::SwicthSelectServer(UnityEngine.UI.Button,System.String,System.String)
extern void ServerBrowser_SwicthSelectServer_m17DDE9D78CC2BBEFFEDA6C30A350CAEF6C98939D (void);
// 0x000002F5 System.Void ServerBrowser::ConnectToServer()
extern void ServerBrowser_ConnectToServer_m515AC6A4016D09DB024CE09D03E5A0583646E66E (void);
// 0x000002F6 System.Collections.IEnumerator ServerBrowser::StartPing(ServerInformation,System.String)
extern void ServerBrowser_StartPing_mC4E889626730F032113850A9730C1DBF782EEFEB (void);
// 0x000002F7 System.Void ServerBrowser::.ctor()
extern void ServerBrowser__ctor_m408A0ED06C701707905968057C18252CE1267F23 (void);
// 0x000002F8 System.Void ServerBrowser/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mE36EEEC20B88163FC227156C5CF0062C049C435E (void);
// 0x000002F9 System.Void ServerBrowser/<>c__DisplayClass8_0::<ServerListUpdate>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CServerListUpdateU3Eb__0_m9A338534DE0704CEAAB6EC98D3B8E38F02AFB7D5 (void);
// 0x000002FA System.Void ServerBrowser/<StartPing>d__11::.ctor(System.Int32)
extern void U3CStartPingU3Ed__11__ctor_mBD47212DE0932EB92DE8D77ED07501B9B31C1818 (void);
// 0x000002FB System.Void ServerBrowser/<StartPing>d__11::System.IDisposable.Dispose()
extern void U3CStartPingU3Ed__11_System_IDisposable_Dispose_m1FF785A3ABA494062420B116E7087E22756F58A8 (void);
// 0x000002FC System.Boolean ServerBrowser/<StartPing>d__11::MoveNext()
extern void U3CStartPingU3Ed__11_MoveNext_m13B9943B3055BA7DBE3DBA3AD639277015EF4164 (void);
// 0x000002FD System.Object ServerBrowser/<StartPing>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartPingU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE45FDCF25A97219E109F9958B3397C36DED41EC (void);
// 0x000002FE System.Void ServerBrowser/<StartPing>d__11::System.Collections.IEnumerator.Reset()
extern void U3CStartPingU3Ed__11_System_Collections_IEnumerator_Reset_mC506869216121BD2FF45403CFC46C83446E0A4E0 (void);
// 0x000002FF System.Object ServerBrowser/<StartPing>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CStartPingU3Ed__11_System_Collections_IEnumerator_get_Current_m1F02CDBA07F7975FD7809AF88CBE376189C21256 (void);
// 0x00000300 UnityEngine.Color ServerInformation::get_GetColorNormal()
extern void ServerInformation_get_GetColorNormal_m5B1BACB3FF1C6C19161F02EA535034FA578C9896 (void);
// 0x00000301 UnityEngine.Color ServerInformation::get_GetColorSelected()
extern void ServerInformation_get_GetColorSelected_m6BA48D3A071A56E47BB602B940F241B40CB9EAEE (void);
// 0x00000302 System.Void ServerInformation::.ctor()
extern void ServerInformation__ctor_m0FA109DE412552EBF59982C1AEADB8732956C0FF (void);
// 0x00000303 System.Void SettingsGame::Start()
extern void SettingsGame_Start_m4DF9EFDA27FEAB7625BF2343378702C9EB2E10EC (void);
// 0x00000304 System.Void SettingsGame::ChangeLanguageForEng()
extern void SettingsGame_ChangeLanguageForEng_m5D8146DCE1D473E19DE5E1FA99BBB4A54BEE0067 (void);
// 0x00000305 System.Void SettingsGame::ChangeLanguageForRus()
extern void SettingsGame_ChangeLanguageForRus_mE1B951867E371DF72FF2D7294ED750F477CA1E95 (void);
// 0x00000306 System.Void SettingsGame::ChangeMusicVolume(System.Single)
extern void SettingsGame_ChangeMusicVolume_mBDE070825C5236785F8ABCCE364DCE1545CD6D24 (void);
// 0x00000307 System.Void SettingsGame::ClickSwithMusicVol()
extern void SettingsGame_ClickSwithMusicVol_m054154B5D3CD504A34135B96B2FF7D4483E42F12 (void);
// 0x00000308 System.Void SettingsGame::ChangeEffectVolume(System.Single)
extern void SettingsGame_ChangeEffectVolume_m09E33AE4FD40BC267CF9711C6737C4FD3BCB310E (void);
// 0x00000309 System.Void SettingsGame::ClickSwithEffectVol()
extern void SettingsGame_ClickSwithEffectVol_mBE1684FFF7004ACB421831212885F21EC246482A (void);
// 0x0000030A System.Void SettingsGame::TextUpdate()
extern void SettingsGame_TextUpdate_m6DBEA27F0A0CA11DDFA6CDE1FD66D9C23768DCA4 (void);
// 0x0000030B System.Void SettingsGame::Save()
extern void SettingsGame_Save_m7283B2803FAC4A8CABB9846E3027A891778F5528 (void);
// 0x0000030C System.Void SettingsGame::.ctor()
extern void SettingsGame__ctor_m5612C103BE6229F483F4FAB73410FD00C5E359E0 (void);
// 0x0000030D System.Void DialogMenu::EscToGameExit()
extern void DialogMenu_EscToGameExit_m7C9C15975826857BBD558F4857812F2FFB888962 (void);
// 0x0000030E System.Void DialogMenu::EscToMainMenu()
extern void DialogMenu_EscToMainMenu_m4996D0BD8D082402D65F294E4299D801AD5D4845 (void);
// 0x0000030F System.Void DialogMenu::ExitDialog(System.String)
extern void DialogMenu_ExitDialog_m34335913385F04A3BA60956919AF2094E357889A (void);
// 0x00000310 System.Void DialogMenu::ExitToMainMenu(System.String)
extern void DialogMenu_ExitToMainMenu_m490C9E9C2E7B090426B115B1739119B4FDFC3A0B (void);
// 0x00000311 System.Void DialogMenu::ShortDialog(System.String)
extern void DialogMenu_ShortDialog_m61294B266DF82F9CB273B9A807BBDAE9792464FC (void);
// 0x00000312 System.Void DialogMenu::ExitGame()
extern void DialogMenu_ExitGame_m99AE1F4242A555FF08C3579F5F0F9B4B59B94239 (void);
// 0x00000313 System.Void DialogMenu::ExitMainMenu()
extern void DialogMenu_ExitMainMenu_m99AF88FE0A8ED7C462384D6F8EF79483C6DDC940 (void);
// 0x00000314 System.Void DialogMenu::DialogExitClose()
extern void DialogMenu_DialogExitClose_m565824B4E9AC555EC3D50C79A5675B7F2BB97C3A (void);
// 0x00000315 System.Void DialogMenu::.ctor()
extern void DialogMenu__ctor_mB0CE5C1D992746EBC2C02254E77E8E36E8B42B62 (void);
// 0x00000316 System.Void ErrorMenu::ErrorWindowUp(System.String)
extern void ErrorMenu_ErrorWindowUp_m0BBD92A4BD6F535E8963ADC7F94947B4D8202D24 (void);
// 0x00000317 System.Void ErrorMenu::ErrorWindowClear()
extern void ErrorMenu_ErrorWindowClear_mBA64B569F267C14006FC70C6CECABBCCDD2381AB (void);
// 0x00000318 System.Void ErrorMenu::.ctor()
extern void ErrorMenu__ctor_m83D6DEE3E842D045242E239E4DCE7D578EEDB8D1 (void);
// 0x00000319 System.Void LoadingMenu::LoadingWindowUp(System.String)
extern void LoadingMenu_LoadingWindowUp_m04C003133686D43F3586281EBC6C37D70724A5F0 (void);
// 0x0000031A System.Void LoadingMenu::LoadingWindowClear()
extern void LoadingMenu_LoadingWindowClear_m82AC3ECD452F1C9A9B1FEBF3F4301A79CC26FD96 (void);
// 0x0000031B System.Collections.IEnumerator LoadingMenu::LoadingStatus()
extern void LoadingMenu_LoadingStatus_mE9F7AC74460344CE76E4449CAA6C601B71CDF0C5 (void);
// 0x0000031C System.Void LoadingMenu::.ctor()
extern void LoadingMenu__ctor_m12E993E7328315265B3C2FB1D84F7A68C7B1A8EF (void);
// 0x0000031D System.Void LoadingMenu/<LoadingStatus>d__5::.ctor(System.Int32)
extern void U3CLoadingStatusU3Ed__5__ctor_m11D5EB9F7BB96A2825B1B5D1240ED2A029D47AAE (void);
// 0x0000031E System.Void LoadingMenu/<LoadingStatus>d__5::System.IDisposable.Dispose()
extern void U3CLoadingStatusU3Ed__5_System_IDisposable_Dispose_m3325C7495797B66A333178E1BDB082CF881452C7 (void);
// 0x0000031F System.Boolean LoadingMenu/<LoadingStatus>d__5::MoveNext()
extern void U3CLoadingStatusU3Ed__5_MoveNext_mEC4878D65D6066A9313021450C7BF35E9A25478F (void);
// 0x00000320 System.Object LoadingMenu/<LoadingStatus>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadingStatusU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7E28D7C17E2A100B640816FF452A379A812A7D5C (void);
// 0x00000321 System.Void LoadingMenu/<LoadingStatus>d__5::System.Collections.IEnumerator.Reset()
extern void U3CLoadingStatusU3Ed__5_System_Collections_IEnumerator_Reset_mB868070640E6326C2CEC525E5D539D4CE28C5F80 (void);
// 0x00000322 System.Object LoadingMenu/<LoadingStatus>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CLoadingStatusU3Ed__5_System_Collections_IEnumerator_get_Current_mF91916F9105E83BC5F9C68158E7F05C07F9D76CB (void);
// 0x00000323 System.Void Bullet::Start()
extern void Bullet_Start_m6BD187DD353835D248DA404B169DCE29CEB2B813 (void);
// 0x00000324 System.Void Bullet::OnEnable()
extern void Bullet_OnEnable_mE783E45F0A09A5565B6A9817C8D3156A408A53BF (void);
// 0x00000325 System.Void Bullet::OnDisable()
extern void Bullet_OnDisable_m705119D56961445D3C8DD69A6CAA460C7359A5E5 (void);
// 0x00000326 System.Void Bullet::OnBecameVisible()
extern void Bullet_OnBecameVisible_m601E3E5C4F1AC56FC4D596AB558FFAB6BC28FD02 (void);
// 0x00000327 System.Void Bullet::OnBecameInvisible()
extern void Bullet_OnBecameInvisible_mA5A705C76BF9F9C6A33E633BE4547E3958871A43 (void);
// 0x00000328 System.Void Bullet::Go(UnityEngine.GameObject,System.Boolean)
extern void Bullet_Go_m77E9D58C5D0847200F5CE9AB4072661BCE94BE36 (void);
// 0x00000329 System.Void Bullet::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Bullet_OnTriggerEnter2D_mC3642C0546BCF5AFC8036C6B77D66560C04685E8 (void);
// 0x0000032A System.Void Bullet::InvisibleBullet()
extern void Bullet_InvisibleBullet_mBBC7AD27848FCBBFF76DE715AB89F1340B08E683 (void);
// 0x0000032B System.Void Bullet::Update()
extern void Bullet_Update_m5AA63D0B1F389C2CFEE77466E1C39ADC813B4DBC (void);
// 0x0000032C System.Void Bullet::.ctor()
extern void Bullet__ctor_m873C02F2114EA93A35E4392013AC831246756CBA (void);
// 0x0000032D System.Void Bullet::MirrorProcessed()
extern void Bullet_MirrorProcessed_mB5F9D324A3A08B06AABEE992B06E0CCC12585F1D (void);
// 0x0000032E System.Void HitKnife::Start()
extern void HitKnife_Start_mB2CB7618396AC44864AEF10C1EFA108268655775 (void);
// 0x0000032F System.Void HitKnife::Update()
extern void HitKnife_Update_m8AEA492C52D896C5DEC4317772E44E6856F60D2B (void);
// 0x00000330 System.Void HitKnife::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void HitKnife_OnTriggerEnter2D_mE50DCB6F60F61FC8C3702B234776B2F2FD35220B (void);
// 0x00000331 System.Void HitKnife::.ctor()
extern void HitKnife__ctor_m48AE5DEDA4DA42AC06D1D6C34281A7F5C452A4F6 (void);
// 0x00000332 System.Void WeaponControl::Start()
extern void WeaponControl_Start_mBDA1FE7AF9673E190ED499FCAE108B6F9A147BE6 (void);
// 0x00000333 System.Void WeaponControl::OnEnable()
extern void WeaponControl_OnEnable_mBAD7B426B159119F0C27F064D7432BAC8784C57F (void);
// 0x00000334 System.Void WeaponControl::OnDisable()
extern void WeaponControl_OnDisable_m5D5CFD1D658180C90542C5B7AAB89F8241877F45 (void);
// 0x00000335 System.Void WeaponControl::Update()
extern void WeaponControl_Update_mDB82A41BDF78DFC1404D281879CC134FCFE43D4F (void);
// 0x00000336 System.Void WeaponControl::ClickButtonFireEnter()
extern void WeaponControl_ClickButtonFireEnter_mBAE8622104C0FC53C1A4DC423B0910B179C651CD (void);
// 0x00000337 System.Void WeaponControl::ClickButtonFireExit()
extern void WeaponControl_ClickButtonFireExit_m5EF84ADE5922B05246AA042E8A9D601768A903D1 (void);
// 0x00000338 System.Void WeaponControl::ClickButtonReloadEnter()
extern void WeaponControl_ClickButtonReloadEnter_m359F4ED13342F639AE4C0C1DBC6160D9B212B5C0 (void);
// 0x00000339 System.Void WeaponControl::ClickChangeWeapon(System.Int32)
extern void WeaponControl_ClickChangeWeapon_mC0B1B607B53E92D6DAA6B1439C3CA01BA198165B (void);
// 0x0000033A System.Void WeaponControl::KnifeAttack()
extern void WeaponControl_KnifeAttack_m19078CFBDF066F974B11DF2DE83E5E4E176F3541 (void);
// 0x0000033B System.Void WeaponControl::Shoot(System.Single)
extern void WeaponControl_Shoot_m03B21B1FD4906B69B5B0BD9876D364B681533485 (void);
// 0x0000033C System.Void WeaponControl::Reset(UnityEngine.GameObject,System.Int32)
extern void WeaponControl_Reset_m86118D956763988627EF36DDEFA1D3248BAF8DCB (void);
// 0x0000033D System.Void WeaponControl::SwicthWeapon()
extern void WeaponControl_SwicthWeapon_m5FACD294BF791B7AD51A3E09F526C0EA0E9DD284 (void);
// 0x0000033E System.Collections.IEnumerator WeaponControl::Reload()
extern void WeaponControl_Reload_m7F5CB27F85E70B5CF01510E7345FE136AB9EF431 (void);
// 0x0000033F System.Collections.IEnumerator WeaponControl::PrograssBarFade(UnityEngine.UI.Slider,System.Single)
extern void WeaponControl_PrograssBarFade_m470EB457E7A2A2CD2D75B802AF70EFAAF22DF197 (void);
// 0x00000340 System.Collections.IEnumerator WeaponControl::ChangeWeapon()
extern void WeaponControl_ChangeWeapon_m7D5F51638F94266D7C3C58A8E472DEB1115A3F2B (void);
// 0x00000341 System.Void WeaponControl::RpcStartReload()
extern void WeaponControl_RpcStartReload_m7F17D208E1581E5C0F8A389A1E3D7F7153892FD5 (void);
// 0x00000342 System.Void WeaponControl::RpcStartChangeWeapon(System.Int32)
extern void WeaponControl_RpcStartChangeWeapon_mF7795D82EC3A86E93DC323F996E7F0F25FA4CD66 (void);
// 0x00000343 System.Void WeaponControl::RpcShotFire(System.Single,System.Int32,System.Single)
extern void WeaponControl_RpcShotFire_mA0C1DA5A85B4295076A7DE7BD0C91C1CB2666FC2 (void);
// 0x00000344 System.Void WeaponControl::CmdReset(UnityEngine.GameObject,System.Int32)
extern void WeaponControl_CmdReset_m3D82E9A4F056E2AE37BDC6EFA0EA156D26E75BAA (void);
// 0x00000345 System.Void WeaponControl::CmdFireEnter()
extern void WeaponControl_CmdFireEnter_mB7C14DBFF6005C964A9A10723652210278CF354A (void);
// 0x00000346 System.Void WeaponControl::CmdFireExit()
extern void WeaponControl_CmdFireExit_m1D94572AAD3580DCE1F0F49085F136C5D533B58F (void);
// 0x00000347 System.Void WeaponControl::CmdStartReload()
extern void WeaponControl_CmdStartReload_m15CAD01E5F0817872265F1101FDF4F906792B364 (void);
// 0x00000348 System.Void WeaponControl::CmdStartChangeWeapon(System.Int32)
extern void WeaponControl_CmdStartChangeWeapon_m5F6597DF1827F878AD087C6A4547D30775403F95 (void);
// 0x00000349 System.Void WeaponControl::CmdShotFire(System.Single)
extern void WeaponControl_CmdShotFire_mFCE0622BFD3DE203E063EAD8922B5C8D1A738109 (void);
// 0x0000034A System.Void WeaponControl::CmdStartKnifeAttack()
extern void WeaponControl_CmdStartKnifeAttack_mD9A30A45F85ECF8B58980B04F00CE9DC0666C6EE (void);
// 0x0000034B System.Void WeaponControl::CmdEndKnifeAttack()
extern void WeaponControl_CmdEndKnifeAttack_m4A342F0F082C20E7F19117C8335C7FEC0CDD31D1 (void);
// 0x0000034C System.Void WeaponControl::CmdKnifeAttack()
extern void WeaponControl_CmdKnifeAttack_m4BEDB8D08F811144E58754F1BCD25119575D4F7F (void);
// 0x0000034D System.Void WeaponControl::.ctor()
extern void WeaponControl__ctor_mE4FE7F4BCFA5FA6381D285AC5F9775F5C0D25A2C (void);
// 0x0000034E System.Void WeaponControl::MirrorProcessed()
extern void WeaponControl_MirrorProcessed_mC1CADD7165682003EB95FD631E48DF4071A76BAE (void);
// 0x0000034F System.Boolean WeaponControl::get_NetworkIsFire()
extern void WeaponControl_get_NetworkIsFire_m56110E930FFB16DD9038274979ADEB9B0E2F5649 (void);
// 0x00000350 System.Void WeaponControl::set_NetworkIsFire(System.Boolean)
extern void WeaponControl_set_NetworkIsFire_m9E6AFF8BF211C11F6EA53A0D660DF51F5A3E115F (void);
// 0x00000351 System.Boolean WeaponControl::get_NetworkIsReload()
extern void WeaponControl_get_NetworkIsReload_m8C96F0E687DEB81495ADF25873E0D4EC21621235 (void);
// 0x00000352 System.Void WeaponControl::set_NetworkIsReload(System.Boolean)
extern void WeaponControl_set_NetworkIsReload_m42DCB131625C2EBC0E0078A31588C77471CDF9A7 (void);
// 0x00000353 System.Boolean WeaponControl::get_NetworkIsDraw()
extern void WeaponControl_get_NetworkIsDraw_mCD1A2251C03AD2EB28C9BA20A8858953DD7612B0 (void);
// 0x00000354 System.Void WeaponControl::set_NetworkIsDraw(System.Boolean)
extern void WeaponControl_set_NetworkIsDraw_m5BA850BF2768D781C0F93F01980FB748C4EEA5CD (void);
// 0x00000355 System.Void WeaponControl::UserCode_RpcStartReload()
extern void WeaponControl_UserCode_RpcStartReload_m1C9479FFF870C40788C54B3BE926FF8175E62964 (void);
// 0x00000356 System.Void WeaponControl::InvokeUserCode_RpcStartReload(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void WeaponControl_InvokeUserCode_RpcStartReload_mCF31C958234C8E7E4816893D564B3F0417BA4C6E (void);
// 0x00000357 System.Void WeaponControl::UserCode_RpcStartChangeWeapon__Int32(System.Int32)
extern void WeaponControl_UserCode_RpcStartChangeWeapon__Int32_m61573DDA39B5DFCBFE348F9E1BD56D4E13D93325 (void);
// 0x00000358 System.Void WeaponControl::InvokeUserCode_RpcStartChangeWeapon__Int32(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void WeaponControl_InvokeUserCode_RpcStartChangeWeapon__Int32_m3BFB7BE6FFA8E8DFA908A484CC119710EDDBBCEA (void);
// 0x00000359 System.Void WeaponControl::UserCode_RpcShotFire__Single__Int32__Single(System.Single,System.Int32,System.Single)
extern void WeaponControl_UserCode_RpcShotFire__Single__Int32__Single_m11E21573C5F56D10582780F695C1D52695FC399D (void);
// 0x0000035A System.Void WeaponControl::InvokeUserCode_RpcShotFire__Single__Int32__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void WeaponControl_InvokeUserCode_RpcShotFire__Single__Int32__Single_mD277C6D7DB5E2F6AFECDDC629644BA38C803C06A (void);
// 0x0000035B System.Void WeaponControl::UserCode_CmdReset__GameObject__Int32(UnityEngine.GameObject,System.Int32)
extern void WeaponControl_UserCode_CmdReset__GameObject__Int32_m234CD5D411126FB490B95C730F71B8B780DCE43D (void);
// 0x0000035C System.Void WeaponControl::InvokeUserCode_CmdReset__GameObject__Int32(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void WeaponControl_InvokeUserCode_CmdReset__GameObject__Int32_m25AD7726FF38547E422D76EECD4E6105FA1876BF (void);
// 0x0000035D System.Void WeaponControl::UserCode_CmdFireEnter()
extern void WeaponControl_UserCode_CmdFireEnter_mB8481B162A29F012AF0950B8B3AD16C9FED4EC23 (void);
// 0x0000035E System.Void WeaponControl::InvokeUserCode_CmdFireEnter(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void WeaponControl_InvokeUserCode_CmdFireEnter_m2D3A0E9B53F5DB6760BB6CAE13A45132E3991C44 (void);
// 0x0000035F System.Void WeaponControl::UserCode_CmdFireExit()
extern void WeaponControl_UserCode_CmdFireExit_m4C637652D181F389EF42ABD950BBAC666437521C (void);
// 0x00000360 System.Void WeaponControl::InvokeUserCode_CmdFireExit(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void WeaponControl_InvokeUserCode_CmdFireExit_mE8DD27A33EF20371B3F33EF2AB2B3BABF31A4996 (void);
// 0x00000361 System.Void WeaponControl::UserCode_CmdStartReload()
extern void WeaponControl_UserCode_CmdStartReload_mCDD3FF59349DA26584FA20334E8C2B5C0EE20729 (void);
// 0x00000362 System.Void WeaponControl::InvokeUserCode_CmdStartReload(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void WeaponControl_InvokeUserCode_CmdStartReload_m41DA12F90B81972E22C08BEA09725F13CDBB2204 (void);
// 0x00000363 System.Void WeaponControl::UserCode_CmdStartChangeWeapon__Int32(System.Int32)
extern void WeaponControl_UserCode_CmdStartChangeWeapon__Int32_m290CE8A164C77DDE8511F8787007B72EF60C1B4A (void);
// 0x00000364 System.Void WeaponControl::InvokeUserCode_CmdStartChangeWeapon__Int32(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void WeaponControl_InvokeUserCode_CmdStartChangeWeapon__Int32_m452E3D89A5EC19E718564CF186B07DA55E4403BB (void);
// 0x00000365 System.Void WeaponControl::UserCode_CmdShotFire__Single(System.Single)
extern void WeaponControl_UserCode_CmdShotFire__Single_mC907DE481CDE0B6593A0444BA8FB042F00F23A6E (void);
// 0x00000366 System.Void WeaponControl::InvokeUserCode_CmdShotFire__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void WeaponControl_InvokeUserCode_CmdShotFire__Single_mD21A0D8E7913F807E5FC94014ACAB9ED130F8F18 (void);
// 0x00000367 System.Void WeaponControl::UserCode_CmdStartKnifeAttack()
extern void WeaponControl_UserCode_CmdStartKnifeAttack_m7E093F0F9B2C3734FB3307BF9759D8B0A5E3EDAE (void);
// 0x00000368 System.Void WeaponControl::InvokeUserCode_CmdStartKnifeAttack(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void WeaponControl_InvokeUserCode_CmdStartKnifeAttack_m6FF5AB5E46BBC166077BD3BF17EAEA53944B894E (void);
// 0x00000369 System.Void WeaponControl::UserCode_CmdEndKnifeAttack()
extern void WeaponControl_UserCode_CmdEndKnifeAttack_mD4D87A0B210A2CD305C4A709365979DE3F0CA30C (void);
// 0x0000036A System.Void WeaponControl::InvokeUserCode_CmdEndKnifeAttack(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void WeaponControl_InvokeUserCode_CmdEndKnifeAttack_m8AD0D4422CB843058445A296C0F7411996C59F1C (void);
// 0x0000036B System.Void WeaponControl::UserCode_CmdKnifeAttack()
extern void WeaponControl_UserCode_CmdKnifeAttack_mAD4DF5E7CC0C09269E2FA24DAB8CA28362B4C5AB (void);
// 0x0000036C System.Void WeaponControl::InvokeUserCode_CmdKnifeAttack(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void WeaponControl_InvokeUserCode_CmdKnifeAttack_m8BF0733BE0CB88DE657A481C736BC2600E92EED7 (void);
// 0x0000036D System.Void WeaponControl::.cctor()
extern void WeaponControl__cctor_m61709E3C2D5929160CFE69910DBA7DDBB7CE4A8A (void);
// 0x0000036E System.Boolean WeaponControl::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void WeaponControl_SerializeSyncVars_mB288A9A0DDA6734B788F9C844B9EC4FB13DCBF54 (void);
// 0x0000036F System.Void WeaponControl::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void WeaponControl_DeserializeSyncVars_m0788F81DCE731EFB206CCFA69B2075BE66BDE7E1 (void);
// 0x00000370 System.Void WeaponControl/<Reload>d__38::.ctor(System.Int32)
extern void U3CReloadU3Ed__38__ctor_m2391BCB1B758CA8A33100D5C2C071C29FF4E82F3 (void);
// 0x00000371 System.Void WeaponControl/<Reload>d__38::System.IDisposable.Dispose()
extern void U3CReloadU3Ed__38_System_IDisposable_Dispose_m980774824432B9864DB0241F711A9B10D764A934 (void);
// 0x00000372 System.Boolean WeaponControl/<Reload>d__38::MoveNext()
extern void U3CReloadU3Ed__38_MoveNext_m63B21D865DA25545A8E8623675366842273A0669 (void);
// 0x00000373 System.Object WeaponControl/<Reload>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReloadU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE2DF8003F5D1C1ED2D2EB0866800ED7042BBAD9 (void);
// 0x00000374 System.Void WeaponControl/<Reload>d__38::System.Collections.IEnumerator.Reset()
extern void U3CReloadU3Ed__38_System_Collections_IEnumerator_Reset_mFD4F17B89F01E8F3315EDE66781F01A314DF4FB0 (void);
// 0x00000375 System.Object WeaponControl/<Reload>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CReloadU3Ed__38_System_Collections_IEnumerator_get_Current_m6C97394B944EA7154D3B125406B56B8BCDB54DCC (void);
// 0x00000376 System.Void WeaponControl/<PrograssBarFade>d__39::.ctor(System.Int32)
extern void U3CPrograssBarFadeU3Ed__39__ctor_m8852E5C1047AA92A294016BD6A24DA8CCB81A75F (void);
// 0x00000377 System.Void WeaponControl/<PrograssBarFade>d__39::System.IDisposable.Dispose()
extern void U3CPrograssBarFadeU3Ed__39_System_IDisposable_Dispose_m1971B09CC635D50A6B35CF0E87D42EBBB7EC39F6 (void);
// 0x00000378 System.Boolean WeaponControl/<PrograssBarFade>d__39::MoveNext()
extern void U3CPrograssBarFadeU3Ed__39_MoveNext_mA4396385D8535298CD6D0F77190F0B8CF8843424 (void);
// 0x00000379 System.Object WeaponControl/<PrograssBarFade>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPrograssBarFadeU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE06A4DE2F1DA0F492F8A658E3EC1D399E2955D3 (void);
// 0x0000037A System.Void WeaponControl/<PrograssBarFade>d__39::System.Collections.IEnumerator.Reset()
extern void U3CPrograssBarFadeU3Ed__39_System_Collections_IEnumerator_Reset_m702B9CFC7DFD101545967C070FDE4C057B73C51C (void);
// 0x0000037B System.Object WeaponControl/<PrograssBarFade>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CPrograssBarFadeU3Ed__39_System_Collections_IEnumerator_get_Current_m7B489ED2631687FCA36618C477396F9268AFCFE5 (void);
// 0x0000037C System.Void WeaponControl/<ChangeWeapon>d__40::.ctor(System.Int32)
extern void U3CChangeWeaponU3Ed__40__ctor_mBC4A4CE38A6976F737DAC02C93267CD5F3C12EC5 (void);
// 0x0000037D System.Void WeaponControl/<ChangeWeapon>d__40::System.IDisposable.Dispose()
extern void U3CChangeWeaponU3Ed__40_System_IDisposable_Dispose_mCC74DE0AF826B82D8A960BEECB51E13B7361F545 (void);
// 0x0000037E System.Boolean WeaponControl/<ChangeWeapon>d__40::MoveNext()
extern void U3CChangeWeaponU3Ed__40_MoveNext_m29955CBB0A3999447C0E2DC141C915AC5B72CD12 (void);
// 0x0000037F System.Object WeaponControl/<ChangeWeapon>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangeWeaponU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDC9AABD1BFAF500E9621E7D076BE33DD0486A4CE (void);
// 0x00000380 System.Void WeaponControl/<ChangeWeapon>d__40::System.Collections.IEnumerator.Reset()
extern void U3CChangeWeaponU3Ed__40_System_Collections_IEnumerator_Reset_m4B2CD62780E6167980B4F2F5A9439771EB00E049 (void);
// 0x00000381 System.Object WeaponControl/<ChangeWeapon>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CChangeWeaponU3Ed__40_System_Collections_IEnumerator_get_Current_mBEAC66F5EC124256C155BA32DB27ABC74680DBBC (void);
// 0x00000382 System.Void WeaponSystem::.ctor()
extern void WeaponSystem__ctor_mBEEDEE562EDAE8EF6104C6C88E792692B1A03E9C (void);
// 0x00000383 System.Void WeaponSystem::MirrorProcessed()
extern void WeaponSystem_MirrorProcessed_mD666B9BB91ED4FD51FFD06E1D58191A2DE6777EF (void);
// 0x00000384 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7 (void);
// 0x00000385 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46 (void);
// 0x00000386 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722 (void);
// 0x00000387 System.Void ChatController::.ctor()
extern void ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719 (void);
// 0x00000388 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435 (void);
// 0x00000389 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9 (void);
// 0x0000038A System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138 (void);
// 0x0000038B System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2 (void);
// 0x0000038C System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C (void);
// 0x0000038D System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0 (void);
// 0x0000038E System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565 (void);
// 0x0000038F System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m3EF23BF40634D4262D8A2AE3DB14140FEFB4BF52 (void);
// 0x00000390 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA (void);
// 0x00000391 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9 (void);
// 0x00000392 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0 (void);
// 0x00000393 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3 (void);
// 0x00000394 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D (void);
// 0x00000395 TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3 (void);
// 0x00000396 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C (void);
// 0x00000397 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82 (void);
// 0x00000398 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5 (void);
// 0x00000399 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C (void);
// 0x0000039A System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3 (void);
// 0x0000039B TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908 (void);
// 0x0000039C System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427 (void);
// 0x0000039D TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D (void);
// 0x0000039E System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF (void);
// 0x0000039F System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052 (void);
// 0x000003A0 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1 (void);
// 0x000003A1 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A (void);
// 0x000003A2 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6 (void);
// 0x000003A3 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189 (void);
// 0x000003A4 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475 (void);
// 0x000003A5 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33 (void);
// 0x000003A6 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6 (void);
// 0x000003A7 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02 (void);
// 0x000003A8 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB (void);
// 0x000003A9 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E (void);
// 0x000003AA System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963 (void);
// 0x000003AB System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2 (void);
// 0x000003AC System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2 (void);
// 0x000003AD System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC (void);
// 0x000003AE System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9 (void);
// 0x000003AF System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4 (void);
// 0x000003B0 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69 (void);
// 0x000003B1 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D (void);
// 0x000003B2 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9 (void);
// 0x000003B3 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6 (void);
// 0x000003B4 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m553F892690ED74A33F57B1359743D31F8BB93C2A (void);
// 0x000003B5 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD (void);
// 0x000003B6 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA (void);
// 0x000003B7 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB (void);
// 0x000003B8 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A (void);
// 0x000003B9 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC (void);
// 0x000003BA System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9 (void);
// 0x000003BB System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46 (void);
// 0x000003BC System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC9F90586F057E3728D9F93BB0E12197C9B994EEA (void);
// 0x000003BD System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A (void);
// 0x000003BE System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29 (void);
// 0x000003BF System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1 (void);
// 0x000003C0 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mDEE8E96AE811C5A84CB2C04440CD4662E2F918D3 (void);
// 0x000003C1 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D (void);
// 0x000003C2 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6 (void);
// 0x000003C3 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90 (void);
// 0x000003C4 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27 (void);
// 0x000003C5 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788 (void);
// 0x000003C6 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5 (void);
// 0x000003C7 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F (void);
// 0x000003C8 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F (void);
// 0x000003C9 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878 (void);
// 0x000003CA System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72 (void);
// 0x000003CB System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695 (void);
// 0x000003CC System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A (void);
// 0x000003CD System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371 (void);
// 0x000003CE System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88 (void);
// 0x000003CF System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56 (void);
// 0x000003D0 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B (void);
// 0x000003D1 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D (void);
// 0x000003D2 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71 (void);
// 0x000003D3 System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC (void);
// 0x000003D4 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95 (void);
// 0x000003D5 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1C76BF8EAC2CDC2BAC58755622763B9318DA51CA (void);
// 0x000003D6 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA (void);
// 0x000003D7 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8 (void);
// 0x000003D8 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE (void);
// 0x000003D9 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807 (void);
// 0x000003DA System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93 (void);
// 0x000003DB System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690 (void);
// 0x000003DC UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB (void);
// 0x000003DD System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362 (void);
// 0x000003DE System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22 (void);
// 0x000003DF System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2 (void);
// 0x000003E0 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51 (void);
// 0x000003E1 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E (void);
// 0x000003E2 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB (void);
// 0x000003E3 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mB6C5974E8F57160AE544E1D2FD44621EEF3ACAB5 (void);
// 0x000003E4 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5 (void);
// 0x000003E5 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D (void);
// 0x000003E6 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6 (void);
// 0x000003E7 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0 (void);
// 0x000003E8 System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA (void);
// 0x000003E9 System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2 (void);
// 0x000003EA System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45 (void);
// 0x000003EB System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA (void);
// 0x000003EC System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9B7AEE80C1E70D2D2FF5811A54AFD6189CD7F5A9 (void);
// 0x000003ED System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8 (void);
// 0x000003EE System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C (void);
// 0x000003EF System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39 (void);
// 0x000003F0 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95 (void);
// 0x000003F1 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689 (void);
// 0x000003F2 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A (void);
// 0x000003F3 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52 (void);
// 0x000003F4 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1 (void);
// 0x000003F5 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025 (void);
// 0x000003F6 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45 (void);
// 0x000003F7 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B (void);
// 0x000003F8 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE (void);
// 0x000003F9 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854 (void);
// 0x000003FA System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mD12057609EFCBCA8E7B61B0421D4A7C5A206C8C3 (void);
// 0x000003FB System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186 (void);
// 0x000003FC System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC (void);
// 0x000003FD System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5 (void);
// 0x000003FE System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C (void);
// 0x000003FF System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683 (void);
// 0x00000400 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mE5E0678716735BDF0D632FE43E392981E75A1C4D (void);
// 0x00000401 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E (void);
// 0x00000402 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2 (void);
// 0x00000403 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49 (void);
// 0x00000404 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148 (void);
// 0x00000405 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE (void);
// 0x00000406 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E (void);
// 0x00000407 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_m5783C7EE3A7D230F5D8FE65111F7F0B3AACF4A5D (void);
// 0x00000408 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m641EA4B8AFFBA5E132B7EE46F0962E8CA6F860ED (void);
// 0x00000409 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_mB9551C24D0AAF928ADC9DE924F2325884F0A2DB4 (void);
// 0x0000040A System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85AD23CB0E7EC78A4A4B07652C7DCB90B27B5A52 (void);
// 0x0000040B System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mDE1C8E5AAFA2BE515D1868F76D4921E627F51112 (void);
// 0x0000040C System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m8FA714E4D7408B556C1A280744593DCD07D7B9D0 (void);
// 0x0000040D System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m093946C77D9946811A7799D06F8D7170FB42A8EB (void);
// 0x0000040E System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m11B84CE30C7048FC3BA409ABDF39AFFEA7AE188D (void);
// 0x0000040F System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_m947F374D2AC3CE949148BC815B2ABBBD8E94D594 (void);
// 0x00000410 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA09D1AB4BEF083569018FF550A5D0E651CF9F2CD (void);
// 0x00000411 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_mC3C5ED94D6E74FB8551D2654A9279CF3E9E1DEBE (void);
// 0x00000412 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_mB487D32B2C46BA2099D42B89F2275CC454E4483D (void);
// 0x00000413 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_m9A84A570D2582918A6B1287139527E9AB2CF088D (void);
// 0x00000414 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66 (void);
// 0x00000415 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233 (void);
// 0x00000416 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084 (void);
// 0x00000417 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C (void);
// 0x00000418 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839 (void);
// 0x00000419 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063 (void);
// 0x0000041A System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1 (void);
// 0x0000041B System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A (void);
// 0x0000041C System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C (void);
// 0x0000041D System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A (void);
// 0x0000041E System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079 (void);
// 0x0000041F System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723 (void);
// 0x00000420 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43 (void);
// 0x00000421 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E (void);
// 0x00000422 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6 (void);
// 0x00000423 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE (void);
// 0x00000424 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215 (void);
// 0x00000425 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79 (void);
// 0x00000426 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559 (void);
// 0x00000427 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779 (void);
// 0x00000428 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF (void);
// 0x00000429 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E (void);
// 0x0000042A System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797 (void);
// 0x0000042B System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B (void);
// 0x0000042C System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A (void);
// 0x0000042D System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503 (void);
// 0x0000042E System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE (void);
// 0x0000042F System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87 (void);
// 0x00000430 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B (void);
// 0x00000431 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D (void);
// 0x00000432 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE (void);
// 0x00000433 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF (void);
// 0x00000434 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98 (void);
// 0x00000435 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65 (void);
// 0x00000436 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97 (void);
// 0x00000437 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772 (void);
// 0x00000438 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398 (void);
// 0x00000439 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301 (void);
// 0x0000043A System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491 (void);
// 0x0000043B System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F (void);
// 0x0000043C System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3 (void);
// 0x0000043D System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36 (void);
// 0x0000043E System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848 (void);
// 0x0000043F System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1 (void);
// 0x00000440 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B (void);
// 0x00000441 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC (void);
// 0x00000442 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1 (void);
// 0x00000443 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262 (void);
// 0x00000444 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF (void);
// 0x00000445 System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60 (void);
// 0x00000446 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1 (void);
// 0x00000447 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m319AC50F2DE1572FB7D7AF4F5F65958D01477899 (void);
// 0x00000448 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C (void);
// 0x00000449 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C (void);
// 0x0000044A System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354 (void);
// 0x0000044B System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2 (void);
// 0x0000044C System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76 (void);
// 0x0000044D System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8 (void);
// 0x0000044E System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88 (void);
// 0x0000044F System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D (void);
// 0x00000450 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A (void);
// 0x00000451 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8 (void);
// 0x00000452 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273 (void);
// 0x00000453 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405 (void);
// 0x00000454 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m15291DCCCEC264095634B26DD6F24D52360BDAF0 (void);
// 0x00000455 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9 (void);
// 0x00000456 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867 (void);
// 0x00000457 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304 (void);
// 0x00000458 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23 (void);
// 0x00000459 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E (void);
// 0x0000045A System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77 (void);
// 0x0000045B System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38 (void);
// 0x0000045C System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB (void);
// 0x0000045D System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A (void);
// 0x0000045E System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7 (void);
// 0x0000045F System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934 (void);
// 0x00000460 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE (void);
// 0x00000461 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m2F84864A089CBA0B878B7AC1EA39A49B82682A90 (void);
// 0x00000462 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B (void);
// 0x00000463 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E (void);
// 0x00000464 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21 (void);
// 0x00000465 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5 (void);
// 0x00000466 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1 (void);
// 0x00000467 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D (void);
// 0x00000468 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F (void);
// 0x00000469 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198 (void);
// 0x0000046A System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D (void);
// 0x0000046B System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8 (void);
// 0x0000046C System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348 (void);
// 0x0000046D System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845 (void);
// 0x0000046E System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5A5869FEFA67D5E9659F1145B83581D954550C1A (void);
// 0x0000046F System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0 (void);
// 0x00000470 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0 (void);
// 0x00000471 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47 (void);
// 0x00000472 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47 (void);
// 0x00000473 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB (void);
// 0x00000474 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7 (void);
// 0x00000475 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0 (void);
// 0x00000476 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422 (void);
// 0x00000477 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A (void);
// 0x00000478 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A (void);
// 0x00000479 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6 (void);
// 0x0000047A System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E (void);
// 0x0000047B System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E (void);
// 0x0000047C System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B (void);
// 0x0000047D System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDE5E71C88F5096FD70EB061287ADF0B847732821 (void);
// 0x0000047E System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C (void);
// 0x0000047F System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F (void);
// 0x00000480 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809 (void);
// 0x00000481 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF (void);
// 0x00000482 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98 (void);
// 0x00000483 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795 (void);
// 0x00000484 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB (void);
// 0x00000485 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53 (void);
// 0x00000486 System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02 (void);
// 0x00000487 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3 (void);
// 0x00000488 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m07746C332D2D8CE5DEA59873C26F2FAD4B369B42 (void);
// 0x00000489 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19 (void);
// 0x0000048A Mirror.ReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ReadyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ReadyMessage_mDECE83CB1F7CAE9FCAD92216F267E4E3908A96FE (void);
// 0x0000048B System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ReadyMessage(Mirror.NetworkWriter,Mirror.ReadyMessage)
extern void GeneratedNetworkCode__Write_Mirror_ReadyMessage_m22A9F16A2ED7E7477E8A31FF1042EB955CFEC6C9 (void);
// 0x0000048C Mirror.NotReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NotReadyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m37D2FAD98BE3268003281DD78F579902BCE0AB12 (void);
// 0x0000048D System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NotReadyMessage(Mirror.NetworkWriter,Mirror.NotReadyMessage)
extern void GeneratedNetworkCode__Write_Mirror_NotReadyMessage_m709FB1A91BB6E0372774E3BAB26503A3464D27AE (void);
// 0x0000048E Mirror.AddPlayerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.AddPlayerMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_m6ACC79432A87A0C5880AB67857D7FAB6827D7330 (void);
// 0x0000048F System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.AddPlayerMessage(Mirror.NetworkWriter,Mirror.AddPlayerMessage)
extern void GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_m231493B9DF2D00D49C6C2E3A83C39AD189AD3017 (void);
// 0x00000490 Mirror.SceneMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SceneMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SceneMessage_m5B5705BDB0E91D54BAB322CB078B3FD920FF63F8 (void);
// 0x00000491 Mirror.SceneOperation Mirror.GeneratedNetworkCode::_Read_Mirror.SceneOperation(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SceneOperation_mD4ED81C2591AC8322FA7364DEFDE26D2EE4A2062 (void);
// 0x00000492 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneMessage(Mirror.NetworkWriter,Mirror.SceneMessage)
extern void GeneratedNetworkCode__Write_Mirror_SceneMessage_mBD1A8505934B960E4F9812A5BA688DF968E35797 (void);
// 0x00000493 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneOperation(Mirror.NetworkWriter,Mirror.SceneOperation)
extern void GeneratedNetworkCode__Write_Mirror_SceneOperation_mE4502A1970E814F38F917FE749E6B4AA55ED7D3D (void);
// 0x00000494 Mirror.CommandMessage Mirror.GeneratedNetworkCode::_Read_Mirror.CommandMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_CommandMessage_m0B599CA7B82D3B35311F0C5F6730041F39C22850 (void);
// 0x00000495 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.CommandMessage(Mirror.NetworkWriter,Mirror.CommandMessage)
extern void GeneratedNetworkCode__Write_Mirror_CommandMessage_m39DB7CB6D020DFA7DD3EBFFE4051433454FAAF8D (void);
// 0x00000496 Mirror.RpcMessage Mirror.GeneratedNetworkCode::_Read_Mirror.RpcMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_RpcMessage_m284BD1F0303CCF36765BC5FACF8DD3998785B231 (void);
// 0x00000497 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.RpcMessage(Mirror.NetworkWriter,Mirror.RpcMessage)
extern void GeneratedNetworkCode__Write_Mirror_RpcMessage_mF3D3F9507C55BC65DA9ACF43CF6798501C7981AF (void);
// 0x00000498 Mirror.SpawnMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SpawnMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SpawnMessage_mBEDBC1E005B2C268463C08A8FBECC4A352889194 (void);
// 0x00000499 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SpawnMessage(Mirror.NetworkWriter,Mirror.SpawnMessage)
extern void GeneratedNetworkCode__Write_Mirror_SpawnMessage_mA5536FE3E9CDEB8DCE7EBEB08FADF0804180C82E (void);
// 0x0000049A Mirror.ChangeOwnerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ChangeOwnerMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m69DC049A017111C5469BBF6E56EC2FFC9668B560 (void);
// 0x0000049B System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ChangeOwnerMessage(Mirror.NetworkWriter,Mirror.ChangeOwnerMessage)
extern void GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m0F0F15EA4F7C2EE4EDBCDC1E6A84807969166645 (void);
// 0x0000049C Mirror.ObjectSpawnStartedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_m37449385EB28A378ABBF028640DAD41AB268167A (void);
// 0x0000049D System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnStartedMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m6176D0EE3FCF5DE4E25DFA2B25BB76CF4D432530 (void);
// 0x0000049E Mirror.ObjectSpawnFinishedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_m4C7F65AA88BCCBCA1D61598503EC00CDFCB972AA (void);
// 0x0000049F System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnFinishedMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_m822D8A7F9BE22578E8328CEB230C4DCD07979611 (void);
// 0x000004A0 Mirror.ObjectDestroyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectDestroyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_m2BAB658B17391EDDEDC4F22761289289C76E1B83 (void);
// 0x000004A1 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectDestroyMessage(Mirror.NetworkWriter,Mirror.ObjectDestroyMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_mD34C28740181B2CFF6A4A5FE3A1F5CDE969F41F1 (void);
// 0x000004A2 Mirror.ObjectHideMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectHideMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_mF2DD6D81FAE045AC90FD58F6D8B82E66009B59A1 (void);
// 0x000004A3 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectHideMessage(Mirror.NetworkWriter,Mirror.ObjectHideMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_mDC761E38F362527307B8FA54B649D2B852A35F62 (void);
// 0x000004A4 Mirror.EntityStateMessage Mirror.GeneratedNetworkCode::_Read_Mirror.EntityStateMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_EntityStateMessage_m98FA70058010B17049B34B87AEBC6275D0965CF9 (void);
// 0x000004A5 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.EntityStateMessage(Mirror.NetworkWriter,Mirror.EntityStateMessage)
extern void GeneratedNetworkCode__Write_Mirror_EntityStateMessage_mA6D54713D611B9216AB4778A6A92C072A8E6BA3A (void);
// 0x000004A6 Mirror.NetworkPingMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPingMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m1E332657361099EA23D1BD7BEB42BEC6A8048182 (void);
// 0x000004A7 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPingMessage(Mirror.NetworkWriter,Mirror.NetworkPingMessage)
extern void GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_mDE8F039A87837E7C8EAE14539CFD4F7C82742C14 (void);
// 0x000004A8 Mirror.NetworkPongMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPongMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_mAD99024E75E186DF6AA03E6513368B4C30986599 (void);
// 0x000004A9 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPongMessage(Mirror.NetworkWriter,Mirror.NetworkPongMessage)
extern void GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_m39D438FAA612F77B0018012E1D0FD20BD5974A35 (void);
// 0x000004AA System.Void Mirror.GeneratedNetworkCode::_Write_Biohazard/BiohazardMode(Mirror.NetworkWriter,Biohazard/BiohazardMode)
extern void GeneratedNetworkCode__Write_Biohazard_BiohazardMode_m11DA497A740C38581B368E4E87FBFF0985A2AA17 (void);
// 0x000004AB Biohazard/BiohazardMode Mirror.GeneratedNetworkCode::_Read_Biohazard/BiohazardMode(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Biohazard_BiohazardMode_m8B0D17121F10F0E29870F2A7CB5C47A47FE5C3C4 (void);
// 0x000004AC System.Void Mirror.GeneratedNetworkCode::_Write_Player/Team(Mirror.NetworkWriter,Player/Team)
extern void GeneratedNetworkCode__Write_Player_Team_mE32D6B48C76F11E106C9C97A22D3D42DA17C3E58 (void);
// 0x000004AD Player/Team Mirror.GeneratedNetworkCode::_Read_Player/Team(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Player_Team_m5CA2CA4F79E65248AF5959E4B85D1FA6D9EA99AC (void);
// 0x000004AE System.Void Mirror.GeneratedNetworkCode::InitReadWriters()
extern void GeneratedNetworkCode_InitReadWriters_mF8776E1B3A143B9E4860920DCEB00C30A97369E6 (void);
static Il2CppMethodPointer s_methodPointers[1198] = 
{
	LangListObjects__ctor_mF9FA24FE1D0E85C955BC351AD9DDF37B2FADD43F,
	WordKey__ctor_m755A833309D2357BC9DE87F55C05A4DB868276C6,
	Word__ctor_mB7EBB67548188CEDCD7A2676962409B5AB3A9FB5,
	Word__ctor_m47F087DD6ED2B3E44CC4E236BB441CBB5384B6E9,
	LangPhrase__ctor_m8F6F42B1DA6959D603220B6181730F40B0CC69EE,
	LangsList_SetLanguage_m9BFC6E45A17C13AC006B6DA5CE70B08CC6934E46,
	LangsList_SetLanguage_mBCAE8A1D809389D521EA494D38AF1EBF1F74973A,
	LangsList_Awake_m2DFC24655432212366B8394830874E2828120DC3,
	LangsList_GetWord_mE61CAE1AE18637A4C92157234916A2A514BFA82F,
	LangsList__ctor_mD81ED2A1F2A34D86CC35D89EFC6942FE9D03B4CE,
	LangsList__cctor_m2B3315C3B4624739E96D39700F728C46230857C4,
	LangsList_U3CAwakeU3Eb__7_0_mC3973477BEB8F4CC3A5139FC537380401C8A54EB,
	TextTranslator_Start_m994F96984B00F79FE154E0434E628039C5D92117,
	TextTranslator_OnDestroy_mA4305EC096A06937517E0676CC95763D0E57085A,
	TextTranslator_ReTranslate_mC01045BAB4375F23F22D492D1520E46AFF1C92F2,
	TextTranslator__ctor_m89912AE4EAA4591319FC6172EC56D9B09754D9EA,
	AnimStartAndDestroyToTime_Start_m47A7C8CAC5D20A924250B26FCFB6A74451CC73A6,
	AnimStartAndDestroyToTime_Go_mE9F8A47A5EB1B0F4B53BC587677CA756F8A5ECE4,
	AnimStartAndDestroyToTime_Update_mD4C4F491911922728A7C2B5E7A75F46D0157B12E,
	AnimStartAndDestroyToTime_Destroy_m14DFF992EC89CD3CC51741060767141038D0C41C,
	AnimStartAndDestroyToTime_OnBecameVisible_m6AC05336D2F629FA8B9D1A2ECD2399FB7F85049E,
	AnimStartAndDestroyToTime_OnBecameInvisible_m7FE9D9366A63EDFF658011D633CFFC3DCD7E8484,
	AnimStartAndDestroyToTime_CancelAnim_m322545E427ECDE50EA2CCD9417264851C902D6BE,
	AnimStartAndDestroyToTime__ctor_m8BB83363487CCC583D6D26DDF109AA64308F85D1,
	AudioPlayer_Start_m1CD4C540B92CF134746EF50084C5729768B93CD8,
	AudioPlayer_OnEnable_m2EA09467BDDFA715E7387850815875081F5F6043,
	AudioPlayer_OnDisable_mC74844A3C99181607EF9ED53E94790EB11CC93CE,
	AudioPlayer_Update_m71C498949724E9689C4FDD4D201F06855334D235,
	AudioPlayer_SettingsAudioVolume_m63CC13B6656A897C0332B996735B7A88AFC28871,
	AudioPlayer_Feet_mCCE9B95ECD13EAD4546A6C0BE7E46E617D7DB084,
	AudioPlayer_SoundPlayerSpawn_m5979A9D44DE01824B9EE54D6667E909668901297,
	AudioPlayer_SoundPlayerPain_m8A8E37096EE65A1E5679C694318F23FCC0EA82FB,
	AudioPlayer_SoundPlayerDeath_mDD269B763D280AA408E48804E985DBA5E5011498,
	AudioPlayer_SoundPlayerInfection_m3CE1982168920BABCA0D6EFDC0BF892799E56399,
	AudioPlayer_SoundPlayerAttackFireWeapon_m289015323330D6442499CCE9E7A6045315D89CC3,
	AudioPlayer_SoundPlayerAttackFireKnife_m43DF50D8608887354EE798CF83B0D908B51B4348,
	AudioPlayer_SoundPlayerStartReloading_mB5541EAA551DF74D6233D10B4AD3FEEB5D4B07F0,
	AudioPlayer_SoundPlayerEndReloading_m87F5D0952DB39A072C1E719FCEAE0A52A91F4CF8,
	AudioPlayer_SoundPlayerStartDraw_m91F8E746B02B0EBDADBAA591B47373E0CD1FE39D,
	AudioPlayer_SoundPlay_mA77227916A855889C7050672482B7D4969B3CDEA,
	AudioPlayer_SoundPlayKnife_mDD7E87609E80841A98CF121DD87414ECC0E0D977,
	AudioPlayer__ctor_m3F81E1F07C97F2ECE2A837B3FB918ECBBCA14B17,
	U3CSoundPlayU3Ed__36__ctor_m2DE5B8265C28712F6FF5F8D285AA8AF076CFB5B3,
	U3CSoundPlayU3Ed__36_System_IDisposable_Dispose_m011B4ED87B8E9BDCA188A6CD5664B312F2BA4BC4,
	U3CSoundPlayU3Ed__36_MoveNext_m9F4B721644E47F43D31F13AB5D41CAF1EFC680F5,
	U3CSoundPlayU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B8A6327E96C1BBA5C4C400B58FC6CC39DCE381C,
	U3CSoundPlayU3Ed__36_System_Collections_IEnumerator_Reset_m9E8010590CC4F4292DA803905DD843F54E3E660A,
	U3CSoundPlayU3Ed__36_System_Collections_IEnumerator_get_Current_mD284DE0568D4BF807DCE34F54F4E0B0859F73897,
	U3CSoundPlayKnifeU3Ed__37__ctor_mA22859D48DA881DD6F65C91DC33633014CD71D64,
	U3CSoundPlayKnifeU3Ed__37_System_IDisposable_Dispose_m433752B96784B7C0301599D88ECB41BA89E7C4E0,
	U3CSoundPlayKnifeU3Ed__37_MoveNext_m7B64730D671619C7D84247B2999EB9376B6DD3CE,
	U3CSoundPlayKnifeU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC999731DE4D94CA0046A5B1AF7B1F919B8B089B,
	U3CSoundPlayKnifeU3Ed__37_System_Collections_IEnumerator_Reset_m171E01904BA8475E3E90A3F5095FB71C6081690B,
	U3CSoundPlayKnifeU3Ed__37_System_Collections_IEnumerator_get_Current_mA50080FDBDF4AD8ABAA2365E704AC7AF33369DB7,
	AudioSignal_Start_m15BD61E02483A4F8A7C1605D56668ED7880B9B6C,
	AudioSignal_OnStartClient_m2FD67489CEF2FFCA66AFBA93B4AEE9D2A297C374,
	AudioSignal_OnStartServer_m03050F48682696819B4574110A26AC93CAA31F7A,
	AudioSignal_Initialization_m878B52DA70C7430B3EDC78BF16C7231AFDE1FEEA,
	AudioSignal_OnEnable_m192A6CD9820410AC2AE1778D96C602BF4DCF2882,
	AudioSignal_OnDisable_m857D9CF801892BED74092C94A21B9F4972FF9363,
	AudioSignal_Update_m2931CC495A8FA7474AD9555823E6D3CC70E99B8E,
	AudioSignal_RoundStart_mC835B1FD3432DEDF5DCA274EB0D805B01D0FE8FF,
	AudioSignal_RoundEnd_m45D5DEBC11A6977EA7444A7D02B71FAA9FD56AA3,
	AudioSignal_MusicStart_m8FBD1721ED93FB964924AC216C1D778101441CCB,
	AudioSignal_HumanWin_m29A68083A7881EEF6EA00E6DDF7FF7639ED5071C,
	AudioSignal_ZombieWin_m882299473F23F34B8B20E3501C88D8F5328E7D6B,
	AudioSignal_Fade_m9C7DF64837E7774132C79AAC02B3D5556500BD2D,
	AudioSignal__ctor_mE0872A7307695C3581BC07C03E8191C7632DF7E9,
	AudioSignal_MirrorProcessed_mC440D17D276A6C8B62CB774ECA93A04612371693,
	U3CFadeU3Ed__18__ctor_m04F74D837E2B61AB8B456A45AD8D345360C252FE,
	U3CFadeU3Ed__18_System_IDisposable_Dispose_m40B9B4570E3E4E738CE3BD9255454EA5D131CB86,
	U3CFadeU3Ed__18_MoveNext_m001CE9431A85376751362EA8F53BA63AD4D7BD0E,
	U3CFadeU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C29197FD70723DBB336DBC54E629A8C44C06B56,
	U3CFadeU3Ed__18_System_Collections_IEnumerator_Reset_m51E92B5A6364520E75C02EE72DC898A3035E4FEF,
	U3CFadeU3Ed__18_System_Collections_IEnumerator_get_Current_m72C595B77613BBD323F05B75827AB437D488E38F,
	BotControl_get_GetSuccessRateOtherStatus_mC35DC770F6E4BECE896BD1E3A74261C24FD3C0CC,
	BotControl_get_GetSuccessRateLogicGroupActivated_m37A98C2113D6F062E18A5A0DF9096F113E4B768C,
	BotControl_get_GetMinTimerChangeBotStatusBeforeStartEvent_m6F7294610D47559827208D205F6157ED5966AA00,
	BotControl_get_GetMaxTimerChangeBotStatusBeforeStartEvent_m0F1A5BC9BC2E6DCFB9C87C5C876D0B00382D0787,
	BotControl_get_GetMinTimerChangeBotStatusAfterStartEvent_m87C956449E4DB1A2B637516EF685FED2C1318FDB,
	BotControl_get_GetMaxTimerChangeBotStatusAfterStartEvent_m2CEC1402EAD58B250CE447AB587BE0C25703E6D1,
	BotControl_get_GetMinTimerDelayDetect_m2692F57C55820EE7829F68C9FDFD478A5967E4B6,
	BotControl_get_GetMaxTimerDelayDetect_mDE4BA0568CFB0CA324F6F5F65FEEE5B56C5D043C,
	BotControl_get_GetRadiusPlayerDetect_m497841562CA0068B6285200622E6D1C3F6E7FAC8,
	BotControl_get_GetRadiusKnifeAttack_m0AEDCB1351D41BDE3EBA94B6525B4E5750B86F6A,
	BotControl_get_GetSuccessRateChangeWeapon_m458C0576BF1FD79874994AB562C640382A450E30,
	BotControl_get_GetMinTimerChangeWeapon_mF03C30205FD13AB71E6C7DF688BD14465A73B2E1,
	BotControl_get_GetMaxTimerChangeWeapon_mECC736C58A042DE90AF96031D184AA0AE5DD88CE,
	BotControl_get_GetMinTimerWeaponForReload_mA1B2BE8CE78FD95210648C90CE8EF12CCF931B82,
	BotControl_get_GetMaxTimerWeaponForReload_mCD9C7221DAADF1B536BD62FDF007DAC42D5A3922,
	BotControl_get_GetMinTimerLookStatus_mC1CA426B4EE18E9818A683342E13EC855C376AA7,
	BotControl_get_GetMaxTimerLookStatus_mE18820DD24A005E744F8BB5CCA1F71C93FA1F4E4,
	BotControl_get_GetLookSpeedNormal_mCECEADBFB431D42FB5CD0524B9746E880ABADD26,
	BotControl_get_GetLookSpeedDangerHuman_m0BBBA13CAF20A252AC9A1BBB090A98395FE978A7,
	BotControl_get_GetLookSpeedDangerZombie_mD99BFFC210745CEDA537598EB43CFE82CDA59EAF,
	BotControl_Start_mFB7FE8067F660EC9EF9C9CDB5C71485644013F40,
	BotControl_OnEnable_m3049361A40B16725EC1FEC47AC223D5569E04A3B,
	BotControl_OnDisable_mCA924B888BC03EB89575D35F0DBCD02BE727CBCB,
	BotControl_NewRound_m80D8D2AF6C8129DEA055A7F9545C7386BC1B21D6,
	BotControl_Spawn_mCC01B6ADD2948BA59296574A388EB12BB6A7C9A6,
	BotControl_StartRound_m7089BBB2024CC04F2B576D013B1BC6CF25DCBB63,
	BotControl_EndRound_m2E80805263C3BCABC74759601EA2D8171A3253D4,
	BotControl_GameStart_m5B916F92FF8FCD1DC9E6C57C19694684418419FD,
	BotControl_Update_m2A08E1CAB7B159E2AC5008F16DD09AE30D833347,
	BotControl_AnimWalk_m1FD4FC7B5CA06A2138DEBB67E3B8C884927EFE60,
	BotControl_LookTarget_m16AE873AEDD5D487AB32B1DE94765083473AF9FB,
	BotControl_ChangeBotStatus_m76BE5AB7EE405F5948230E6EB08FAEF84EA3FBC7,
	BotControl_BotWalk_mDA544C56761B76BF652A1D4D61575D21A1955836,
	BotControl_BotWalkStop_m18E5AD3DF3E941BB3EADB22FC5B454662045CB53,
	BotControl_BotStop_mD31736E0DACE604BB42852BC0A16A9C9DF983F8F,
	BotControl_BotDanger_m798679F8A270B3D4A0EE4CF0D3D066BDDD12EF9E,
	BotControl_OnBecameVisible_m7EB18249CD0771EF7EF6917091DD161598E921A4,
	BotControl_OnBecameInvisible_m0674AADA80B4411A05D1AEC511F4EC0A6220F58E,
	BotControl__ctor_mFEBC274FC14D8FDA217DA8511B6F9B35D4D460D6,
	BotControl_MirrorProcessed_m589257C72A5B213E66952718A9984395A8DB14B1,
	BotControl_get_NetworkVelocityMove_m2D43AC3704B84E3F4F76AA0C16F63F97016589C7,
	BotControl_set_NetworkVelocityMove_m4AB4D035F17BC8F58510006CF3C39999464EE408,
	BotControl_SerializeSyncVars_m6B9724CA95E6629DC50F2907478ED89C88A21195,
	BotControl_DeserializeSyncVars_m15F05CDD79088E38A4DA0C6FA7F76EEDE2804956,
	U3CBotDangerU3Ed__105__ctor_m221CEC151ECC709F3DDC0243666ACF0ED2CC14E8,
	U3CBotDangerU3Ed__105_System_IDisposable_Dispose_m5EF6912AD9CE169A942D0B7425EFBC5AE5451964,
	U3CBotDangerU3Ed__105_MoveNext_m47E174450C0E0445DA66348CC48605F0C7078F14,
	U3CBotDangerU3Ed__105_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E7A886F407742212E5EE49C65283C20AE84148C,
	U3CBotDangerU3Ed__105_System_Collections_IEnumerator_Reset_m94FB6A4F3CC8D880AFF9605472A0D75D5D816F0A,
	U3CBotDangerU3Ed__105_System_Collections_IEnumerator_get_Current_m105D3060BE2580D69F713CBB1BAF45F031E725AC,
	BotsNavigation_Start_mADFD94836498FB35BA0C6BEC17EE457B6EAB10CA,
	BotsNavigation_OnEnable_mCF9AAB2208AFFB5A68E1FBA3BC93A20D503C7A91,
	BotsNavigation_OnDisable_m9EA20D968712C769F63EB76BA06B387856FA82A7,
	BotsNavigation_RoundStart_m73716610B4E794058F61DA230A9FF4C1C241A7A2,
	BotsNavigation_GameStart_m7C3C437035465AA7A396C068B8A062C17E87467A,
	BotsNavigation_Update_m9E30FADB27AFBAD11CB5950ACCB8A7DC82D89115,
	BotsNavigation_SettingsNavigation_m7BBA47583D668F6D55036E5285E96838796C5BD5,
	BotsNavigation_BotWalkZombie_m6B06957F2B7C905F637B109732ECB67A29B74A01,
	BotsNavigation_BotLookZombie_m90DCDDE940C624D1970C4B4370695F8D21A67DF4,
	BotsNavigation_BotFollowPlayerZombie_mC3E08C20C9CE26674853E58EA3729E9E6A02945D,
	BotsNavigation_BotWalkHuman_m9429F59B6D3129E00007A96E3E89D0FE877D7C43,
	BotsNavigation_BotRunToShelter_m66A9A51AFDC7B9930BD13F5D4A5985493596D808,
	BotsNavigation_BotPatroling_m0D44C535491B3261E781A621370A0B3E4DC31A62,
	BotsNavigation_BotFollowPlayerHuman_mB5CB0F2C6107E1CC293287E476D7875307FAF232,
	BotsNavigation_BotDangerEscape_mB842F6AC154341EF365D961416674A8C0BB34DC1,
	BotsNavigation_Infection_m44ECBA0C625B383837F6DE45222EF50D41D8EF9E,
	BotsNavigation_boolBack_m5387812E3B3255893AC162FFB7E7BCABBFC71DA5,
	BotsNavigation_ChangePosition_mF7E80CA3795BF632C85447C71E20E33D39DD2D2F,
	BotsNavigation_RandomBoxPoint_mCCBD7BA284B297B0949FBD08A9BC018A08982D1F,
	BotsNavigation__ctor_mAE072991EBD4D2357E04507B08A3084FBD49D35C,
	BotsNavigation_MirrorProcessed_m5653952270560CA5A20A67260F2822E3AB1A4146,
	U3CboolBackU3Ed__40__ctor_mD689334AC96570604E2AC7FC7FC33A7542995AE5,
	U3CboolBackU3Ed__40_System_IDisposable_Dispose_m4353B9B6195C979E67B4A12E0EA6DC031B712F21,
	U3CboolBackU3Ed__40_MoveNext_m95C325D33CFFD0DFEF0FC6D4DE67FFE16D6CE7CA,
	U3CboolBackU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0D96E3ABC5240D76ABD79A956367532A0A99ADB,
	U3CboolBackU3Ed__40_System_Collections_IEnumerator_Reset_m42373B28CE3BA620A0D50413ED04834C43AC1A27,
	U3CboolBackU3Ed__40_System_Collections_IEnumerator_get_Current_mA0706E0471CE3E08071AE74E058E12620D7AA711,
	SheltersAndLooksOfBunker_OnDrawGizmos_m6D1CB7A60296788432585ED3D0DBDF792B563A97,
	SheltersAndLooksOfBunker__ctor_m259DBAF17ED8275F33D6A276714686EAF3A71A16,
	ShelterZone_OnDrawGizmos_m0E5D24946B1EEE8A36DC066AFB2C20FC1AAB0863,
	ShelterZone__ctor_m8D5CDB73B0510B144B3AB1AD081B2BFB9616DBF2,
	Client__ctor_m6F46A6650E8D17FC2CDF2D840FF97EA786B45115,
	InfoPlayer__ctor_m6C37B7684249B50F50131E336F5217C337A7FF53,
	ControlController_Start_mC5F6991434A8C683060B813617E3EBD7FC153AB9,
	ControlController_Update_m23D6752B723E211C98908F2843F8D05FC99FE7D3,
	ControlController_SpectrMode_m62B96CE7FF24B50427AE5BB63FA802B43CCDA233,
	ControlController_LeftButtonSpect_mA35A594F9F492E0094B252FA7A2E04ADFB871BEB,
	ControlController_RightButtonSpect_m49EECF8AAA6C439AD8AADAEF44ABAD199A69551B,
	ControlController_SpectrLogic_mA777432B5887D7886E926E593D1C38F92BD836D1,
	ControlController__ctor_mD1F85E28FCE120DE333E03C0F6C2CB50B80E364D,
	Damage_TakeDamege_m3ADFEBD6BA534720EBC2C2514B79D9C0A0D21B43,
	Damage_ParticalBlood_m805151B37021615F7F5898E312B9DA77AD4AC6B2,
	Damage_DeathPlayer_m3E0A420DFB1315C1AD1D71CBE681C5D06F183E48,
	Damage_RpcTakeDamage_m69B5F3A52F8E9132492BF8E1C4787B6F30256F7F,
	Damage__ctor_mD125E75891B1101B5BB47BD4E746206A6AD8BCA6,
	Damage_MirrorProcessed_mC4B07E8306110D3C1983DD9701210647DA190CFC,
	Damage_UserCode_RpcTakeDamage__Player__Boolean__Boolean_m9EDE1CAF8DC44A67B84076E7ECB40AD00C777684,
	Damage_InvokeUserCode_RpcTakeDamage__Player__Boolean__Boolean_m624B82138A2C3F6AA872F73A001A978E2F014938,
	Damage__cctor_mD2D5B5B2313BB27D5855B7C876226C0B780EA62C,
	U3CParticalBloodU3Ed__9__ctor_m10C83BCF6B741100C24C39A3F553EE590DB263AF,
	U3CParticalBloodU3Ed__9_System_IDisposable_Dispose_m3A344C32AA387E53104658FADA8E2C895AD7C5DE,
	U3CParticalBloodU3Ed__9_MoveNext_m4BE986E6366159849A0139F1129004AAE50E7730,
	U3CParticalBloodU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA7A1C26D7FFFB65096C5DFF22740E12B4FF956C3,
	U3CParticalBloodU3Ed__9_System_Collections_IEnumerator_Reset_mC6644B1B39AA1AB650824B7D8CABA8B4C65C21A1,
	U3CParticalBloodU3Ed__9_System_Collections_IEnumerator_get_Current_m84B3A4C3F4B2016FAD12223A2EFBD53A603FD346,
	U3CDeathPlayerU3Ed__10__ctor_m5019E1F3628215826FA70A0B1B885743EA1F3047,
	U3CDeathPlayerU3Ed__10_System_IDisposable_Dispose_mFB3C5B6375B309C85936EDE988E294C23E0EF0EF,
	U3CDeathPlayerU3Ed__10_MoveNext_m9CA210173058ED53BD07220D96ACBC1CDA7D8B5F,
	U3CDeathPlayerU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2ECDAB91D117960073FDF562D993A1457FFE3AD2,
	U3CDeathPlayerU3Ed__10_System_Collections_IEnumerator_Reset_m0249F481D131ACBB03579B415CF22F466741F6DF,
	U3CDeathPlayerU3Ed__10_System_Collections_IEnumerator_get_Current_mD0073E9C4E18112CC69E16FB891E442B56EB1D59,
	DataBaseScript__ctor_m2259E1B1E1F61018B5E826F0AFF949568DDE3CD4,
	NULL,
	NULL,
	DataSaver_deleteData_m84608CAC00D096B1C5982E49A2AFAAB6250926D9,
	DataSaver__ctor_mE99C7D193CC90DDDDA6566AA8E62F5248DEAD760,
	Biohazard_Start_m41657261F53B877110AC0B4C4AADE895A2D1BDED,
	Biohazard_OnStartClient_m5803DF3D7E1CD654EFEF7BEC2BC11AA09CFAD244,
	Biohazard_OnStartServer_m0A172AE3E025B84D89404E534B6A161EC5F2EDA1,
	Biohazard_Initialization_m1D90743712EE5C58E75338A7D2DCD24BD550E306,
	Biohazard_OnEnable_m69D9FCD5C8E90F6D3932BB583C43E834A191FD5F,
	Biohazard_OnDisable_mEFFEC019B06298D6B195203652B94A263D4B0A66,
	Biohazard_Update_mC7CE387CEAB7711F599AE8B7DD6053A0297BD69D,
	Biohazard_CountsPlayers_m22DA2382E60EEDD2F699067F8466D0FD274A6D28,
	Biohazard_RoundHumanWin_mDED733E7218A9622C1193FD7972E0D0BCBC8FE00,
	Biohazard_RoundZombieWin_m687F70BB9DA28AFAEBBEDD38532B4C1334285DC6,
	Biohazard_StartingTimerPreInfecton_m36F1FA934F4C8DC979BFA969F432488250D80DB4,
	Biohazard_SettingsMod_m8EAB2F066A910988E2B36612D7FFD41436283C87,
	Biohazard_PlayerSpawn_mCAB284BE4F66A78D5707CD6D26D226EB55820D52,
	Biohazard_PlayerDeath_mAE521F91538F0A64EFF80FBC794F1A7D990C9FEA,
	Biohazard_EndRound_m70E310922FAFA7DFA6FC5EA7603D337E3CF53D93,
	Biohazard_PlayerDelay_m6AD935034BFEEB057EAA45FB7F29C99EFF5D0552,
	Biohazard_StartInfection_m3BBDBAC59E45DFB78D6E3672B7389E243804577B,
	Biohazard_InfectionPlayer_mE6BD4456B49FDA746DFF5EB4A0B06B4BA60B0DE5,
	Biohazard_HunterStarting_mAE9D4D7AED08A88BFD404209F633C525E61540C0,
	Biohazard_Antidot_m57FE9E65C86BF9A26E58732198708D12997C5041,
	Biohazard_WeaponReset_mD5A02EAA90F13CB0ABA4ED73DCE7F4178C53C8E5,
	Biohazard_SwitchSettingsMode_m9017CDED15DF6A47F51074ED3A227DEFE1D35BB3,
	Biohazard_UpdatePlayerProperties_mD411E687569D650B017FE899690563AA847BF0F6,
	Biohazard_PlayerClassParametrs_m9A0D537E646F3A5A57F15C1B1DDC7AD3473EB940,
	Biohazard_PlayerSoundUpdate_m7B44A39AB2ABD2BD8E14F177860A0DAE65BBA65C,
	Biohazard_RpcModStatus_m0C43BF5184CD600239B451310ED1DC673CE62572,
	Biohazard_RpcRoundHumanWin_m10946EF7C7A8431A1716B8494245B9A52C59D4CD,
	Biohazard_RpcRoundZombieWin_m23C7ECA71CC370B9C5961178D6318528588AA720,
	Biohazard_RpcInfection_m4E31F764CB6FC4319BA054806776E0951A5EF17C,
	Biohazard__ctor_mDE1F6137491C1F8092EB0DBBA4AF5058A23FC5E4,
	Biohazard_MirrorProcessed_m235E3863FB78B7EF342AA0EDEF54BE38BDC69145,
	Biohazard_get_NetworkSyncStatusMod_mC57AC58FCFCC0AAA7A5B9EBC21C43793E24E2B24,
	Biohazard_set_NetworkSyncStatusMod_m1F537B9DBE6CC32971D67A6B2FC7F75D883A098A,
	Biohazard_get_NetworkPlayerCount_mADB23128B9645514AFD91D9079DCBC716E19B3C2,
	Biohazard_set_NetworkPlayerCount_m0D511CAE7AFA19C6C95DE005276225345B3CA8CC,
	Biohazard_get_NetworkZombieCount_m3E8507EF7EE6780F9B30EF11D45D9D8BB89EE558,
	Biohazard_set_NetworkZombieCount_m6BEC877604366B9D2ADB98A073FD0BA2E537BD36,
	Biohazard_get_NetworkHumanCount_m5C83976F9CD651274286DCC071604BE285D72A67,
	Biohazard_set_NetworkHumanCount_mFE401B68D31F8081372FF0C3B925713E371356E8,
	Biohazard_UserCode_RpcModStatus__Boolean__BiohazardMode_m62797101A38B3A7BAF8FAE4F1C8E39A89AAC8BB8,
	Biohazard_InvokeUserCode_RpcModStatus__Boolean__BiohazardMode_m896B017243D5055A76E670A150FB31240F09ED30,
	Biohazard_UserCode_RpcRoundHumanWin_m3D24852738A64CB25FF0C69940C99688841354CC,
	Biohazard_InvokeUserCode_RpcRoundHumanWin_m36D182C015F7206665B6AC29DDB8D939451D5373,
	Biohazard_UserCode_RpcRoundZombieWin_m6E8B191CEE0044BB611D360D17FBE2A5FBB19F96,
	Biohazard_InvokeUserCode_RpcRoundZombieWin_mFD072912C9350DD7E4D7E52475B62003C570BC99,
	Biohazard_UserCode_RpcInfection__GameObject_m99F6CC124252B439CF9708E1FA28297A7C687619,
	Biohazard_InvokeUserCode_RpcInfection__GameObject_m32A6F2A36D23C076CE7BF662FC4362E0118D13FD,
	Biohazard__cctor_mCDE2632565703B2AECEDFB917490F330C24B88FE,
	Biohazard_SerializeSyncVars_mF3AED1660DF26A794FB9B8CC0A8B4D44BCF234C8,
	Biohazard_DeserializeSyncVars_mD7F3B4B7EFFF1B26878FCE8C840B9E033056305C,
	U3CPlayerDelayU3Ed__55__ctor_mC6D33A58A323B49AE3474C565C39408D255B3AE4,
	U3CPlayerDelayU3Ed__55_System_IDisposable_Dispose_m1F899D065C87BC56DBB5483F374EC53D926ED82B,
	U3CPlayerDelayU3Ed__55_MoveNext_m104628BC65BDC2B0FDC86C8A0FDF3AB1883B9B54,
	U3CPlayerDelayU3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3009327C699C382A542E0965C4AF90E3A6513299,
	U3CPlayerDelayU3Ed__55_System_Collections_IEnumerator_Reset_mF78C31D41000880B0FF35348F05CAF130F37B7F0,
	U3CPlayerDelayU3Ed__55_System_Collections_IEnumerator_get_Current_m7C5D35A2E1B20177C063D045FE7184CBCAB4D8CE,
	U3CStartInfectionU3Ed__56__ctor_m929AEF60D5D0AE22D600BFB748C81879CC47F49E,
	U3CStartInfectionU3Ed__56_System_IDisposable_Dispose_m02C0645913D842745F8CB3FE08C15F0E2F0F6BC1,
	U3CStartInfectionU3Ed__56_MoveNext_m0B3C1B0D4F725C742C9434CABDD065625AA5BA85,
	U3CStartInfectionU3Ed__56_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EEDCBB6639AE6D84E5E8E86F09ED8A8DCA42F39,
	U3CStartInfectionU3Ed__56_System_Collections_IEnumerator_Reset_m0A7C0F8DD717C692A4769C94D49E73AEE123C3B4,
	U3CStartInfectionU3Ed__56_System_Collections_IEnumerator_get_Current_m6DC65643A63C3FF516BAED1C038676EAE1FECD8A,
	U3CInfectionPlayerU3Ed__57__ctor_m12BDEF3CA67A0B22D9F0094EF99933C67337C843,
	U3CInfectionPlayerU3Ed__57_System_IDisposable_Dispose_mD0E751ADCB82EEF0BECF741C8AA46A1C0E3A5B85,
	U3CInfectionPlayerU3Ed__57_MoveNext_mCC9FD279189EA471F4A279CB775C753C46A18E89,
	U3CInfectionPlayerU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE384AA7F58470DBBB9C4F5C16A09D684301A0CB,
	U3CInfectionPlayerU3Ed__57_System_Collections_IEnumerator_Reset_mB1C8A009F01139118189043EBE5B879D4EE502B3,
	U3CInfectionPlayerU3Ed__57_System_Collections_IEnumerator_get_Current_m9B97BEE72C710597863CDB5ABFBC3DDDB928A924,
	GameControl_Start_m1B81A3A747085E94CC8962FD4FCF12995361FDDC,
	GameControl_OnStartClient_m2C4C5E4C1C466970C2802D9D77D1145F2AF260CE,
	GameControl_OnStartServer_m3B5A3DA59694DD5CCD9485D30087BAAF311EE2B0,
	GameControl_Initialization_m60903A8E9944547D4BAEF19A84EB2A7593EF386C,
	GameControl_OnEnable_m1F1D947872A5120F18F61EC19F58BBD8E3289822,
	GameControl_OnDisable_m47502C9A7158CDC85658D582E216D2252B1DE100,
	GameControl_Update_m5C6C1398A3CBEE8718E10B4AD10E30C5162727D3,
	GameControl_NewRound_mD0F704352FCA50DCDCE3AC7AF0041EAC9F3B6FC9,
	GameControl_GameStart_m38E85DEF9D27A4041E9E8294FE253A2AA416DC56,
	GameControl_GameEnd_m561450FEA45451FC32E711334275CD8528DE331E,
	GameControl_PlayerDeath_m2CFC2D0A3260DC939328DCE82E3867BD8D265E47,
	GameControl_RespawnPlayer_m557E24EA2FF0197F39191DF2A69FFFCE58EEDAD3,
	GameControl_SetWeapon_mBDB5817434023EB4C4C589EC93173D597AE0A155,
	GameControl_WeaponUpdate_m90EA4F3C513C6C02D5EB285F8EE90DAB5B7D83A6,
	GameControl_UpdateWeapon_mAC992CFEE371B2643A8785303031AA288E8858FC,
	GameControl_ConnectedRespawn_m8702D1A134BD280CFE3EB58533DA02E48FFE583B,
	GameControl_RpcNewRound_m2C512899543F960D02D71B2A11B97F350AE1E88D,
	GameControl_RpcCreateBot_mFFD715E6028CD40F3DD926B9873A048DF4246CD5,
	GameControl_RpcDestroyBot_m083B16ED901ED5D50E64D2242B06D5E8FE3BFE1D,
	GameControl_RpcGameStart_mB202A573544EE7B69D6ED5F0D09E505F9F22E204,
	GameControl_RpcGameEnd_m16EBDD5AC3C7B7BE71985FEE88203B0984CF29CD,
	GameControl_TargetConnectedRespawn_m40EDC60BE82430706B157F6A1AE0D5819EC3C454,
	GameControl_TargetRespawn_m27A1A0E0C12E09DC9BB402A210F64F67412E3A6D,
	GameControl__ctor_m4650C9F61DFC74BEDDADA2654D755EEB825AD161,
	GameControl__cctor_m86BF3DA0366363970A422CEC002A1A8FB030EB7A,
	GameControl_MirrorProcessed_mE7CDA3AF7F46F83A548D0379CD00677B355899EB,
	GameControl_get_NetworkRoundTime_m7774B24548508E9752EFE370FCF7A2B79A872B37,
	GameControl_set_NetworkRoundTime_mA68105F3E669B6B916E4F06C481F5A5C42F9F6F2,
	GameControl_UserCode_RpcNewRound_mC2708F0B9DD1FCB32B551968FA57C5617BE2739E,
	GameControl_InvokeUserCode_RpcNewRound_m71230F20CE58ACB6089D96E63B705557FD477063,
	GameControl_UserCode_RpcCreateBot__GameObject__Int32_mF6E41C23F2B88CD80A29263551D1A67D6449BDD9,
	GameControl_InvokeUserCode_RpcCreateBot__GameObject__Int32_m19731046C5A83A71BDF03958EA589A450E0E2269,
	GameControl_UserCode_RpcDestroyBot__GameObject_mD9312ACA7FAB013A14479FCFA369F38F3891850D,
	GameControl_InvokeUserCode_RpcDestroyBot__GameObject_m583C2302425E58718C2DC9E0D23E7FAE6941B27F,
	GameControl_UserCode_RpcGameStart_m19235B1B56F9D8CACE8ADE9428EBD8F270D28816,
	GameControl_InvokeUserCode_RpcGameStart_mD1954F26C48D8D5D6F6EF4CCDF01AAE7386BC0A4,
	GameControl_UserCode_RpcGameEnd_m4FF429F9F33066B3A7E13F749542439AB929EA3A,
	GameControl_InvokeUserCode_RpcGameEnd_m45A38DEA4022C57E581F6A6E47DA79040EE5800F,
	GameControl_UserCode_TargetConnectedRespawn__NetworkConnection__GameObject__Boolean_mC2DB44ABA9F443BF7A710EA30B46438407722D6A,
	GameControl_InvokeUserCode_TargetConnectedRespawn__NetworkConnection__GameObject__Boolean_m519331FDF9C775D9FBAD3D52822F5D1D870C3DA1,
	GameControl_UserCode_TargetRespawn__NetworkConnection__GameObject_m90E76EC466F3B17F53A3B72FB0569F1F354A3D22,
	GameControl_InvokeUserCode_TargetRespawn__NetworkConnection__GameObject_mD1716FC979F2E898D1E8BD39D4B6476E75BD0ABD,
	GameControl_SerializeSyncVars_m7EC609B61679B38172A6809848C75C9BD94CDDDC,
	GameControl_DeserializeSyncVars_mB015388089C73455CA0E50152FEA8E44F0CDCB43,
	U3CRespawnPlayerU3Ed__54__ctor_mA4E718B817323750E68E1D720B6A8F20AA360FE2,
	U3CRespawnPlayerU3Ed__54_System_IDisposable_Dispose_m3C395F12E2266E8A85B958A24EC318AFA6980A61,
	U3CRespawnPlayerU3Ed__54_MoveNext_mC3917DE2315B53D495DDA2AB988578976EC747E1,
	U3CRespawnPlayerU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m850B6D70EA2DB2D68A16565D8E5238C539095EAE,
	U3CRespawnPlayerU3Ed__54_System_Collections_IEnumerator_Reset_m8A6E5B3FB7B97021DB65AC5159E5E66760536866,
	U3CRespawnPlayerU3Ed__54_System_Collections_IEnumerator_get_Current_m4ADCAF8F49FDF6E05FA16DA7A90E13A7602F42A5,
	HumanClassSystem__ctor_mB5CF88AD73FF0AAEE60736F53A46C7BB44474760,
	HumanClassSystem_MirrorProcessed_mE4423A1BAEB30B2992C0A5DCE00819CCE4EFF8A2,
	ZombieClassSystem__ctor_mEFB9AF170E0BFFDD0283FD63925572A272C26776,
	ZombieClassSystem_MirrorProcessed_m80AD122A37FF833BE428F0153F6F8B447BE68C10,
	KnockBack_TakeKnockBack_mFF5E0F44D64A30B5420484B9E7FC3178878215C1,
	KnockBack_KnockBackForce_m1AFD3E987032231B560F2A2A22D038866E4BF815,
	KnockBack_KnockBackVelocity_mA2C1095D7AECB59B13EC33E78073E37E067A5F86,
	KnockBack_TargetTakeKnockBack_m8EE4FBEE1C7E162EA4584A1673FD8DF355828916,
	KnockBack__ctor_mF25C0A16848BD198414EFCB6B5459B2F569F9854,
	KnockBack_MirrorProcessed_m432570BCA25E2D9C3EF442EBA2888F0866C06355,
	KnockBack_UserCode_TargetTakeKnockBack__NetworkConnection__Single__Single__GameObject__GameObject_m342E8DD4053E1417879FDCCC6E2984DA807FC1C2,
	KnockBack_InvokeUserCode_TargetTakeKnockBack__NetworkConnection__Single__Single__GameObject__GameObject_m0113EF3D148E93B9AB743FE95EB47EFDD3DBF69F,
	KnockBack__cctor_mD1692C84DE1DDF01A4AB01E039F3CCE1F8FD4CB3,
	U3CKnockBackForceU3Ed__4__ctor_m72C0012DC03C05BA3A406CF147F9515061B6D4E2,
	U3CKnockBackForceU3Ed__4_System_IDisposable_Dispose_mB43FABF925D165DC15D915549E5BA530A113EAAF,
	U3CKnockBackForceU3Ed__4_MoveNext_mD4D1290F5E6A741DB9C57575B6EC10AC2D48BA67,
	U3CKnockBackForceU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A1172CC5A8540E564E787F2D9FB863C007289E1,
	U3CKnockBackForceU3Ed__4_System_Collections_IEnumerator_Reset_m47372F0870D2409D2DE6167A18AD71A87DAEB865,
	U3CKnockBackForceU3Ed__4_System_Collections_IEnumerator_get_Current_mEDD1D4006917A23EB05CBB5233BFA4B5A862F5A5,
	U3CKnockBackVelocityU3Ed__5__ctor_m273A9340EEED85D5D0F23D3DD6CB12E3BBF8251D,
	U3CKnockBackVelocityU3Ed__5_System_IDisposable_Dispose_m81A90A7CCF9937B6E5FA2ADD74754FF69A02AAE0,
	U3CKnockBackVelocityU3Ed__5_MoveNext_m4C846522EAB3E87D91171F41F54362028FF38B31,
	U3CKnockBackVelocityU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03FBF3DDF333C9A1FFDB50C61A7932858730F7CA,
	U3CKnockBackVelocityU3Ed__5_System_Collections_IEnumerator_Reset_mCF0F2E5B6EE7FD0EFE7B0E0851361469FBFCC6B6,
	U3CKnockBackVelocityU3Ed__5_System_Collections_IEnumerator_get_Current_m8017DB713BC252A541546B489EF880C6E7129FD5,
	MapController_Start_m06F8596FE8E28CF3A47CA2B2AEE3031A7946FB32,
	MapController_Update_m456861AC51F11BC72FCAAF5A3CF3E81CEE8CFFC8,
	MapController__ctor_m00E8FD02C6C4C34F39E2AE09EDB82BD51066766E,
	Message__ctor_m4B34B407C9C10627022C55648F679C2844877969,
	Chat_Start_mB932E30F3890B834E2A2D297926737B08FA18C07,
	Chat_Go_m60B1459A5D172C027DAEE1427CE70065E10E3E66,
	Chat_Update_m4849782CFCD60741E334AA0ECF5BD66C02705AE2,
	Chat_SelectButtonOnChat_mE1BC4051EF21E8A3D6A4795D73B52215DFD32A44,
	Chat_PlayerConnected_m64B171664EFE7510B7C643709007B517AE64C399,
	Chat_RpcSend_mC5C406683294CD2705AFD2681FDC44900A569C67,
	Chat__ctor_mC00AAE8BD6B361CE8BC543EA5539923C4011A0B0,
	Chat_MirrorProcessed_m0A609414E768D40F1F982748B13DD3F1C9BB5948,
	Chat_UserCode_RpcSend__String_m3ED066440B8207FEAB8F34498B0EF438969C6A08,
	Chat_InvokeUserCode_RpcSend__String_mC0EF415C0C85ECAB74E1419400142304D0054DE0,
	Chat__cctor_m0FEA77DB4A8967D83EC47E83E80D34FDEC15255A,
	ChatIdentity_Start_mB0B95CC29175B778D978A2F02E055B24261B784C,
	ChatIdentity_OnStartAuthority_m0D6A71308EB42217DCE839D8D440BA3DAE77A123,
	ChatIdentity_Say_m99AF7586D55A99046D6CEE1E43282C0BD076FA22,
	ChatIdentity_CmdSend_mD4792F0DD3C725AFD6D4150FC86C60B137CE7C94,
	ChatIdentity_NewGlobalMessage_m4584BCA44F7E52475478E46F7FE4645F7E6E222C,
	ChatIdentity_Send_mBBC7BC727AFC3BDF8950839F54854FE75999CCB0,
	ChatIdentity__ctor_m4AEF7AE9A28BDF8E1A5B75289588C21CCBD48F62,
	ChatIdentity_MirrorProcessed_m78BEAA05B2967C8766543049E852FDDD1D9AF64C,
	ChatIdentity_UserCode_CmdSend__String_m1C2B2912C8F0EB374F7743FF6D6A45449CFFB086,
	ChatIdentity_InvokeUserCode_CmdSend__String_m60A55A7B76373FBE8BEF8A74985D8C8F0034A48A,
	ChatIdentity__cctor_m80F11B67D62C0E1A9B582E79E7E58DD0965D1FCE,
	Connect_Start_m6937A4E017D6BECF97AB0D39144BE55C6E321EC4,
	Connect_OnLoginUserButtonClick_mD3C8C7BE74945AA030D084E4756119D217A70365,
	Connect_StartRemoteServer_mC647C300BA80FA7F4AE9D512DC17D01E21AF27BE,
	Connect_LoginRemoteUser_mD3A9457A05351C33DC1C52A389928F450FEC276B,
	Connect_Encrypt_mA114B0336C310A608595C6B244537AB116B3EB43,
	Connect__ctor_m749BA923DE54DD9B222AA2E24EC33A1223F33FE6,
	Net_Start_m4E68E40073EE4BF450F961DD7FFBEEB78FE9FBA3,
	Net_GameScene_mD7C3DACA59DA1B2E6050F882F90FF952842DB5B5,
	Net_OnServerAddPlayer_m2293E1E6F97ABECD29D407A89413010592C32210,
	Net_OnClientConnect_mE4BD88CA16FB498394D424541ACEE1A2B8D4E582,
	Net_OnClientDisconnect_m80D1D48D6A23C8C1406A080D523E24678D8D260B,
	Net_OnClientError_mAE919A7AAB51B81A093D1EFF4D201A113F2AA64D,
	Net_ErrorUI_m04CB926739519C1573F04B8B3604F64B9CB18066,
	Net_OnStartServer_m422D8D3B0136363712DED520DA99E474970378C2,
	Net_OnServerDisconnect_m67991EA47F91E8B414AD904BC2D8BA017B2CC076,
	Net_OnStopServer_m938830849C0F2E617227646986EEB7E9791BBBC2,
	Net_ResetPlayerObjectForServer_mAFB9CA3B80FCDDF8FE5AEE5310CA8D0462DE53D6,
	Net_ClickReConnectToServer_m6266B931AAA9994BA223E015873B8221777FC724,
	Net_CheckConnectToServer_m7698224F525AF309219980FC327BEF3E55F54157,
	Net_Synchronization_m4E59006527622E3D1524DA3DCD31522A3BB83529,
	Net_RespawnPlayer_m1C15E5CCA94452B9D26E318B065D21658B5A4C12,
	Net__ctor_m4DB9F24446ACD7090CC7AF749439A222AECE06EA,
	U3CCheckConnectToServerU3Ed__15__ctor_m6A7C06EB201047790347323BB99F9EF73D40FC6C,
	U3CCheckConnectToServerU3Ed__15_System_IDisposable_Dispose_m04F7E5D41509FD51D9B1C0BE4C1DA93E9CD283FD,
	U3CCheckConnectToServerU3Ed__15_MoveNext_mF0EC8AAF376F1E79BBE8AA44124CBAE68A1D8328,
	U3CCheckConnectToServerU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7F6189864FF038A3E198FD48C6BB6CF42FB8AF9,
	U3CCheckConnectToServerU3Ed__15_System_Collections_IEnumerator_Reset_mBF41BA7E451BB36DEC8523754C375A3586F9AB5C,
	U3CCheckConnectToServerU3Ed__15_System_Collections_IEnumerator_get_Current_m52036E81CE2C536F6FB74106663605C1FDE814C1,
	U3CSynchronizationU3Ed__16__ctor_m8977ECABF395C8CF7FDE942FB44A1B81251ECA85,
	U3CSynchronizationU3Ed__16_System_IDisposable_Dispose_mAA1DA48472072ECB3DD2BB56D4399DFDB0B52DAF,
	U3CSynchronizationU3Ed__16_MoveNext_mE57C56AC5BB95D58323F56F90D8335F8A3390372,
	U3CSynchronizationU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1F322DA317469B334D65CA1E27F06A3AD047819D,
	U3CSynchronizationU3Ed__16_System_Collections_IEnumerator_Reset_m0421FF7BB3650E5A1F2C6C5E85EA98A7189E6C8B,
	U3CSynchronizationU3Ed__16_System_Collections_IEnumerator_get_Current_m2F8880D5B0C843236F801B1B7E72D86A56758044,
	U3CRespawnPlayerU3Ed__17__ctor_m54F97976A5AD867EA446F91AB38CC31CE51C648C,
	U3CRespawnPlayerU3Ed__17_System_IDisposable_Dispose_m78BBBF329B4BD5D6CD5BD35580A2D156142E174C,
	U3CRespawnPlayerU3Ed__17_MoveNext_mCE493C23022869325C60EC9A1DFF7D6BE76DD53C,
	U3CRespawnPlayerU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5070AAAD19B9DEDBC232A36E7C1800F0C3200B3E,
	U3CRespawnPlayerU3Ed__17_System_Collections_IEnumerator_Reset_m1613E82D8FFCADDD21E22A250B4E9AE59F4849D1,
	U3CRespawnPlayerU3Ed__17_System_Collections_IEnumerator_get_Current_m55141794AE2E6675F2AB6FB6A5961FF4733D01F5,
	OptimizationLogic_Start_mB67AF52F43586DF9D8E54EA9654D76BB2CF3B2CD,
	OptimizationLogic_GetPoolObject_m985010BEFE471A7BD5694FBB5319C9E16F901151,
	OptimizationLogic_CreateObj_m8FDDE5BBD51FDE364CD25B9E0781F51CAE6764F1,
	OptimizationLogic_GetPoolObjectForList_mAC88C0493481FF10D3B49AA755C4BDB755B07978,
	OptimizationLogic__ctor_m264720E911452F671F36929D69C3E0B5F1AA8173,
	GameOptions_Awake_m1885F4E9DFFAA8E8D46E8A8E9A6B4A37C26AFFCF,
	GameOptions__ctor_m02A2D6501699F1B60BC8E05B4E3536DFD52340A2,
	ServerOptions_ServerOptionsStart_m0DF8C2A7E8CE57DB082A2EFACCF9173663813D5C,
	ServerOptions_LoadingMap_mA6A64B17542EDB20BE57C3C37BED6AE7B3C06C9B,
	ServerOptions_Update_mDD264A743AADEAFBE2AAB615CA7DBC1E8BD33E8C,
	ServerOptions_ChangeMap_m6CD5F866F2102B385D12C0953AC39CAC4879E9FF,
	ServerOptions__ctor_m1F1C8CF2B5ED5652A9B62B2895D382F4203E5D7D,
	PlayerInfo__ctor_m8ED3F2D1B7789917E0A65CEB89D94F10D834E180,
	SpecificalOptions_Awake_mC5A9CDA708F708780852C0F41566BEF950AD24FC,
	SpecificalOptions_OnApplicationQuit_mDC534A4345E293EC49786DC8AB02BA708F7CFDB1,
	SpecificalOptions_OnEnable_mCC248E9216B0A2920A57757DF3C99D2E88B7D88A,
	SpecificalOptions_OnDisable_m4AF21E0A1ED77731CC6E09DBC197B9206E574B08,
	SpecificalOptions_SetPlayerPrefsAudio_m709BA57A368373EB1D4D813151936078CD306F59,
	SpecificalOptions_OnSceneLoaded_m340EFCAEEDDC2884CC55A4E01DDE3F7E3FBA1E3C,
	SpecificalOptions__ctor_m68614D68BB3134035873D23833B43D211A4F649B,
	CameraPlayer_Start_mE391D1AC69C915ABE6470144A780DA71AB61E95E,
	CameraPlayer_FixedUpdate_m732E904A8B1E30A3276C5D7EFEB836BC590F7857,
	CameraPlayer_ChangePlayerTarget_mCB4639A21A74BC6AF460FEF69D43B30E18E82C5D,
	CameraPlayer_OnDrawGizmos_m7780147BB2BB6A8677006F545A3CA4C1CDD640AE,
	CameraPlayer__ctor_m35E2A08BDC9B80BD06677095BF3B4795693130EF,
	ControllerPlayer_get_GetMinTimerDelayDetect_mD8747EFDED7E2ED70386829400BA0CF460F56858,
	ControllerPlayer_get_GetMaxTimerDelayDetect_m741D6C2194D4EB51ADDB8B00BB6283E2E7EEE704,
	ControllerPlayer_get_GetRadiusPlayerDetect_m3E9BFDB83C38F2914AC1BEF805444D05650A2A5E,
	ControllerPlayer_get_GetLookSpeedNormal_mC580A4F3188D2826D0DADCEECCC5079ADAF2A44B,
	ControllerPlayer_get_GetLookSpeedDanger_m1C264B1170B3AF1E5C76B4A7D21AEEFA276A5FB6,
	ControllerPlayer_Start_m56DEC6E3650E6C1971323481B53F7909E2AF2E6A,
	ControllerPlayer_OnEnable_m2DBA56DBE38D05CB3AC8877FE5E5E2137D895E4F,
	ControllerPlayer_OnDisable_m7A2D9297B58D3DDFCC3CD4BA7DA01D6502102084,
	ControllerPlayer_FixedUpdate_m577EC0E3A34D81D26D4E48302096763E44ADB369,
	ControllerPlayer_PlayerSpeed_mE44D0CA80A191872F223CFC8B3DEF6249649236D,
	ControllerPlayer_Look_mB12CAA9593012A41343C7F0A6589509DE4A93579,
	ControllerPlayer_PlayerDanger_m8FA57CA9B1512D0ED0152531CABA0EC135A3DFDF,
	ControllerPlayer_ChangeTargetMove_mD675BC293E4BEA98571977335CBA6417C26AF071,
	ControllerPlayer_Spawn_mB1BE92677B5FB0122EFA4D17ADD6F9854F4FAA83,
	ControllerPlayer_OnBecameVisible_m4A3C7C3C1F544E843741E8B83BD4FA3B9F399A8C,
	ControllerPlayer_OnBecameInvisible_m9B1A17403932161CABF34CCC158F9609E4E5D733,
	ControllerPlayer_CmdChangeTargetMove_mC52908508B647F73191DE56BD6A98CEA75298205,
	ControllerPlayer__ctor_m3720DF1D4DADE6167033F4A6E4D114EB74700159,
	ControllerPlayer_MirrorProcessed_m8B3D87F31A08AF5AB95AAD8C3FC49B2E9DCDE709,
	ControllerPlayer_get_Networkspeed_m2AFBC3E1BADE3E9F81E436106A311396333687D8,
	ControllerPlayer_set_Networkspeed_m8052B51DD8F51BBBBE90BD6EFF571E2D7A007052,
	ControllerPlayer_get_NetworkTargetMove_m28E04A08B9F00E90B94D344A690BB35A61D5E9DA,
	ControllerPlayer_set_NetworkTargetMove_m9BE0FC88BBB89EC34B54BB51E69A7F2B2AD8E070,
	ControllerPlayer_UserCode_CmdChangeTargetMove__Vector2_mAA7EFDBB558863B40B2868421834ABAE76E52DD3,
	ControllerPlayer_InvokeUserCode_CmdChangeTargetMove__Vector2_m149EBCF339F5F0849F2CF5E34992735CBAF736B1,
	ControllerPlayer__cctor_m11E26AAB6304212AB47C47EE8D0A491E336E9243,
	ControllerPlayer_SerializeSyncVars_mB95CFF5D55BAEF6117DE2467F095959E9787435E,
	ControllerPlayer_DeserializeSyncVars_m0D76207E61E25822417E171B0EDA7D21E1509D50,
	U3CPlayerDangerU3Ed__33__ctor_m35DFC7F5B0AC0217441F028C58112F1D87135A7E,
	U3CPlayerDangerU3Ed__33_System_IDisposable_Dispose_mBC37BF23A78A621A7B88E4F72D364F6B890074AC,
	U3CPlayerDangerU3Ed__33_MoveNext_mBFCE85DC08948B7B3BC19DF39F79235F0FA878F9,
	U3CPlayerDangerU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3807726B0D4FC2B664D39A0D4485A5788F5B966,
	U3CPlayerDangerU3Ed__33_System_Collections_IEnumerator_Reset_m4F8795CD90BA79F15D90A1F1AEDE98788B6FDF23,
	U3CPlayerDangerU3Ed__33_System_Collections_IEnumerator_get_Current_m853CDDA78A9CD40A3F80E0C391D8103C9CD90B28,
	IndicatorPlayer_Update_m349A12509FE2DDB6F15B3D0091D8EC2AA5D3FD89,
	IndicatorPlayer_ChangeIndicator_m53D6E24AAA48F72DD68F8669A7F6FA5DF0796091,
	IndicatorPlayer_OnBecameVisible_m9E4D078030471B9072E6AA3A1B2143EE72DE77EB,
	IndicatorPlayer_OnBecameInvisible_mCB2A7C9F8530F50178C47BB99CA55F9C0FBC3813,
	IndicatorPlayer__ctor_mA9003AA6A5476A1947A0B70352376A4315BCEFAA,
	Player_get_GetPlayerID_m30AC015EBE34384BBB2555F9668F51D80AED8FE6,
	Player_Start_mC31CF4F40DDEA35C5E39E8C43EC37284AE7C453D,
	Player_OnEnable_mE0997C2EDE52E3BC53CCBF961D4FE375347F0906,
	Player_OnDisable_m37872CE0A672A116C94A05662826159004ECB59C,
	Player_Update_m95E134A5EF1B5164EA281F61D7FA436F59BE3C9F,
	Player_NewRound_m6BC047292E6EEC6124341733988CDA7F8C43B58E,
	Player_Spawn_m6D393268D6ADFF434E3ADBC808410E3B3965C6E3,
	Player_TakeDamege_m3A146034BFF4B1940D3CA7F05A2864502C7746EC,
	Player_Death_m473F6D4E542F8EB6AC1E7A56B87B299960E040D8,
	Player_PainShockEnd_m66B8A40C0D1AB54A2F232CD8D5B6D5D41C6141C1,
	Player_RegenerationHP_m960CAA6BC95D9D70C9B1746F1CD68475E6F99A02,
	Player_CmdChangeNickName_mFB1560B417DCD16484F4897AE5655D48F3101A59,
	Player__ctor_m0A83E0706592FC871B0CF188B37AFC6649F3D85D,
	Player_MirrorProcessed_mCFB97F456345EC354F716A07C5F1AA47FF9A38A7,
	Player_get_NetworkRandomSpawn_mE687ACD85FB375FE3834B14615AAD3FAFAD149BF,
	Player_set_NetworkRandomSpawn_m7CCF87166E40D30EC7C98CB0EA0B3572ABD63B68,
	Player_get_NetworkRandomWeaponPrimary_m0F8EB67E923DC2AE73D6458AAC0E45825979A486,
	Player_set_NetworkRandomWeaponPrimary_m9995196F3FD54CF6B93E313C645C66ADD1EB5B1E,
	Player_get_NetworkRandomWeaponSecond_m579D97655F5211E2459DE27B20425C63DC31B7C7,
	Player_set_NetworkRandomWeaponSecond_mEEB7EBE1C89FC26733CF104083AA3E408BDBA050,
	Player_get_NetworkRandomWeaponGrenade_mA35170C6138469BFEA9BA41551C0792AE4862A1F,
	Player_set_NetworkRandomWeaponGrenade_mC8D36429735A27FCF453809676E546C6D8F4FC4C,
	Player_get_NetworkPlayerTeam_mA8386FACF79E149E8BB94BEEF5253FDD2FF4D199,
	Player_set_NetworkPlayerTeam_m73300B6D92D01B1619B7693D2BF119C0397FA4ED,
	Player_get_NetworkHealth_m92A0B62B8D005E6E002E68ADA8DAB57CD32D79A9,
	Player_set_NetworkHealth_m4489A1B611D6DF762F420AE6397449D6D2DAA45B,
	Player_get_NetworkArmor_m1A66E3E4A48701F97FAA2279441C10009AACF1FA,
	Player_set_NetworkArmor_m20B729D5E0F89E61282955A50D4DD1C47DE0640D,
	Player_get_NetworkIsAlive_m1C1C74C68DC7BF3AF163A2124619DA290A8B846D,
	Player_set_NetworkIsAlive_mAF47289BAF8A12F522067409D5E91BB02A38EE51,
	Player_get_NetworkZombieSelect_m63703B70AAC85B5DCF4F903A788ACD6055D69460,
	Player_set_NetworkZombieSelect_mAD4B80471EB9FFE66A9D126C86E147072B25AE9C,
	Player_get_NetworkHumanSelect_m0E1B7E4039EDFA4DE97C8008F3EA36D5ACAF2745,
	Player_set_NetworkHumanSelect_m8F4BFE8BA24D2C6C0C0B0450C5894547EF251258,
	Player_get_NetworkActiveWeapon_mAA566E04C7E0B6B301C9F62DBC0B6BBEC25A707E,
	Player_set_NetworkActiveWeapon_m11E6B71B0737DEF8261957D396575D9B8337637E,
	Player_UserCode_CmdChangeNickName__String_m8B28931926E4AAA899E48023F4A7F91757B320DC,
	Player_InvokeUserCode_CmdChangeNickName__String_mA3C74685F88B9F301ACCF64A42CB19335EC9859B,
	Player__cctor_m0083B3C3F6B4BD82C21F3694C66578096CE88056,
	Player_SerializeSyncVars_m5C4CBC51C7FACE68E643DADD507593565E98B837,
	Player_DeserializeSyncVars_mCEF21E6BE12F24DB432A0F7C76CCC34300FB5826,
	U3CPainShockEndU3Ed__59__ctor_mC57116471909E82E33A1CD2FF550E600DAC51A6E,
	U3CPainShockEndU3Ed__59_System_IDisposable_Dispose_mFAC0436D92442D84B64553F65902CE8D2D869265,
	U3CPainShockEndU3Ed__59_MoveNext_m673BBBC8CA7E377185045D33CB34A150489E53D9,
	U3CPainShockEndU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED116ED94FBD1AE37B3D7C83D970E39883A81B8F,
	U3CPainShockEndU3Ed__59_System_Collections_IEnumerator_Reset_m08FD0B421C5ABAE8F87B105B513339CC636F543D,
	U3CPainShockEndU3Ed__59_System_Collections_IEnumerator_get_Current_mCD9B09C0F74A05453A375495B74704EC24A15806,
	U3CRegenerationHPU3Ed__60__ctor_m62BC6AD141602A0FC0A3CCDDCB07FDCF15021380,
	U3CRegenerationHPU3Ed__60_System_IDisposable_Dispose_mA264057531FAE4ED23F337EC8686775BBA79A864,
	U3CRegenerationHPU3Ed__60_MoveNext_m7E766866283495320E4891C03240C4840AC6DAFC,
	U3CRegenerationHPU3Ed__60_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3F2C7B58801AC5BD80866F8836087DAB37DA8F8,
	U3CRegenerationHPU3Ed__60_System_Collections_IEnumerator_Reset_m78602A0FE4A196E68A3E1C1769174F5E060CFFDE,
	U3CRegenerationHPU3Ed__60_System_Collections_IEnumerator_get_Current_mAB5DCA38B5DCD430D04D36362F16C47CE6EC8A47,
	ButtonUI_OnPointerDown_mF438F4563D2BAD01189C3691FDB967AD17B3367E,
	ButtonUI_OnPointerUp_mEB5E7813722A474E1B61F4CC15B6CE4E79D401CF,
	ButtonUI__ctor_m76E2C33B8629D3C1BBE5F0FB391CEF0BEAF1612C,
	FPS_Start_mA6545EFC3FF91D5CEBB3267F4C5EBE7C3E9E7008,
	FPS_Update_m72ED9FF60A51EC6A4E6F5A8C57AA942FF886D745,
	FPS__ctor_m78558469A6F3BEC1D5C8412D1C93DF93E28930E3,
	Joystic_Start_m500E5442955741D999D999B7CFF5AAC768977FAC,
	Joystic_OnPointerDown_m6380E23C2D3986A5560C8D83CE950F56BA3E5781,
	Joystic_OnDrag_m46518C26668217BD804452DC55D0A0B918A647CD,
	Joystic_OnPointerUp_mA415F304ABCA609433E913EECC5F56B86EDB8406,
	Joystic_Update_mDC90924B55A781B384D518FA1784E989B18D53D9,
	Joystic_Reset_m2C80B43512407FF519473221FE5B32E3A77B2574,
	Joystic__ctor_m403D214D36872DCA408EDC87B74FF073DA2E6C33,
	ProgressBarPlayer__ctor_mCF4587216BB8DF021895D8A7B14198EB094F5677,
	UI_Start_m36F3E11677AB5677BD3A76B5865E90CD3609183A,
	UI_OnEnable_m44B9420109C4449103DE83E15A90586994BF473A,
	UI_OnDisable_mF1CCC77FA9FD9663F40B471055D7C3005D1EC7CD,
	UI_Update_m7F9212980C571870E0700BAB400E09111370F6ED,
	UI_NewRound_m9294525460481E99636D11167A7BA8DB71402F55,
	UI_Hud_m30D04E6DDAB479948800BC82511415874426170D,
	UI_SpectrMode_mB66E941648ED8D2F2FA37335F54B1BCCEB16EF0D,
	UI_PlayerSpawn_mB08FFFF223BE5249179F3C08F2F925B99A418C8B,
	UI_PlayerInfection_m24FB3BEF736EE685C0F48B8F688E7CD783B8EC52,
	UI__ctor_m177FCA8E7C6A148BFF6FED9F758B3396F25FDFBB,
	UI_MirrorProcessed_mF86C288D8CDA8B535BDDEBA0C7B4E55FD136E9D1,
	AuthManager_Start_m97E5064CC9D561CF5F02ED40A5DA54C780D590DD,
	AuthManager_Update_m482A7E3DBC24C89F9DCD039E01BD79F3055917F0,
	AuthManager_Awake_mAD76EDE68AFA86FCF349B3F047D17CF221DB624D,
	AuthManager_InitializeFirebase_m4A4D9283F5DA4ED0D8B2B789070ED0657D94B831,
	AuthManager_AuthStateChanged_mEC7BFA976CB597A17ED7486BB92E421A5ACEFF5E,
	AuthManager_SingIn_mC4262C873C15468E1B2E22066184F2B01E3E90DC,
	AuthManager_LogIn_mEAE77B8BD10E42314A1CB85382D04121B4FC34DB,
	AuthManager_ResetIn_m2241F281C5FD94CE762E4393DED873D298BD67A2,
	AuthManager_SignUp_mB1A08A8EB30FCDD4CE72FC8725E8EA26711C306F,
	AuthManager_LogUp_m5746BF25FDA5FF6FCCC74F14F7F6C0ECD6F507DE,
	AuthManager_ResetUp_m817B985488F50C8EDB49A6A1892BAC15FCBF26EF,
	AuthManager_ChangeProfilPlayer_mDED19B138B9CD052EAAC8B9528B50FCF3C164B21,
	AuthManager_ChangePassword_mB42517058966986640C3D1FC49AB4817017AF24E,
	AuthManager_ChangeEmail_mC08BBFB6A892E1E1E6E24039E740B3E02149E25F,
	AuthManager_LogUpAutomatically_m341A6D033697EBE43360C694F4BCE4C7BE17CA4F,
	AuthManager_SignOut_mF62FD633E2FC42E07C6D1FC221C5B62ED57E9807,
	AuthManager_CheckAutologin_mB2CB954E6ED7BDD4199E3C2A7043FCCAB5810275,
	AuthManager_AutoLogin_m4200EC18286CA1905B1812300A89FA1F87FF3053,
	AuthManager_MainMenuStart_m1529DBC84E352EA0FAADE4F595377B234067815B,
	AuthManager_Login_mF1FF5FD0DCEF9FDFAE7E881FE6F5DA0AD4B107B8,
	AuthManager_Register_m0CD751F7BE9882772B934A2C3894509EA486A4B0,
	AuthManager_SignOutUser_m4102DBABBB637A0825191621BEF4A940C05B01D7,
	AuthManager_SendVerificationEmail_m2EA21C7918E2A6F573A3263E2DA06798ACEDB027,
	AuthManager_SendEmailResetPassword_m94357B7222F00CDB75D00BE4709AC54BF33E44A2,
	AuthManager_UpdateProfile_m3B8998F884CCB9FB3F1E0DA84DC6FBDC59816D9C,
	AuthManager_UpdateEmail_m1460CFBCFE4BF77A786E7E376E39615DB3E58333,
	AuthManager_UpdatePassword_mF05DB411F217C8FF9E917A958652698D1980737F,
	AuthManager_AuthErrorActive_mA3DD4D0267BB03C7E1F4291595A7277E48DBB14E,
	AuthManager_ChangeAuthErrorActive_mBE4974C9C8218A6A1D6A8ACB9E5A9944BA55B141,
	AuthManager_GetErrorMessage_m5962051FFC6E74EF15FBF5F73A0E87E9C79A1E48,
	AuthManager_GetErrorMessage_m88A26CAB3670464277B5ECD479EA1375BA9C86E6,
	AuthManager__ctor_m86FDEE5047527DE532CFA45A0E04C31249157A6A,
	AuthManager_U3CAwakeU3Eb__24_0_m3C280BE7B1D692D7CBCB73DCE1BE8F948AB71086,
	U3CU3Ec__DisplayClass38_0__ctor_m5C6549002CFF26EA07E18A707839725ACE200D83,
	U3CU3Ec__DisplayClass38_0_U3CCheckAutologinU3Eb__0_mD99C2A48375E755A2A28694759754CF6BA01C579,
	U3CCheckAutologinU3Ed__38__ctor_mC81665B6F9B1436A214574AD3D21DEC353CFC6E0,
	U3CCheckAutologinU3Ed__38_System_IDisposable_Dispose_mE639042F8AC2139C0E64FE55FF05CEDC6A2C6CDA,
	U3CCheckAutologinU3Ed__38_MoveNext_m498118F3F0E2235AEF4C91E9B7BC45C2E4B53EE5,
	U3CCheckAutologinU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA405A2D7A98480B524959E27FAE9448455D12C9F,
	U3CCheckAutologinU3Ed__38_System_Collections_IEnumerator_Reset_m0978E02EE23A5D8BF237CC3A525203CD6DE2D2C1,
	U3CCheckAutologinU3Ed__38_System_Collections_IEnumerator_get_Current_mAF917D184CCFC0E2ABC0B9D22B45ACD33536E1DE,
	U3CU3Ec__DisplayClass41_0__ctor_mD2BF48B7141F645B0C9A6CE700C1B192DA432F3E,
	U3CU3Ec__DisplayClass41_0_U3CLoginU3Eb__0_m70253CAC9C05A58B525FA5BAE0AB5321D08E719F,
	U3CLoginU3Ed__41__ctor_mD7C8E88B30B117F2969CF81B6BEA81E226A93468,
	U3CLoginU3Ed__41_System_IDisposable_Dispose_m23AA0C16C3F8F34E45664B610C57423D8A4E35C1,
	U3CLoginU3Ed__41_MoveNext_mA7AE25B99D8AB45AA1B4FB1A2BBFD622399EC192,
	U3CLoginU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5796593F57EBC70501B9CC212C2FD7F67C42EACB,
	U3CLoginU3Ed__41_System_Collections_IEnumerator_Reset_mE0D601D4266AC0328B62A774414C95E90E712D4B,
	U3CLoginU3Ed__41_System_Collections_IEnumerator_get_Current_m43D8D1611C5C677D6FB0B96CA2892B00F13B0B65,
	U3CU3Ec__DisplayClass42_0__ctor_m77863F0C9B59CBEEE678590D4DA36AB89C16FDFD,
	U3CU3Ec__DisplayClass42_0_U3CRegisterU3Eb__0_mCDD7364D53BCDF4D9E595C1C9FC3E9ABDB61A01C,
	U3CU3Ec__DisplayClass42_1__ctor_mBB5C576C3B015A82BAFCEFE2A411AE79378FC26B,
	U3CU3Ec__DisplayClass42_1_U3CRegisterU3Eb__1_mF09D3AC7F5675FC3245FD44B9A2D616D34B64D96,
	U3CRegisterU3Ed__42__ctor_mD2BB1D429927D21C59BD9D898F69131362F997E0,
	U3CRegisterU3Ed__42_System_IDisposable_Dispose_m60A18E42318DFE076DCA1472E6A2D1BEFAF11729,
	U3CRegisterU3Ed__42_MoveNext_mCA8695326C91D3618225715313FB4AF28B8CA2BE,
	U3CRegisterU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A96022E0CE1F302520C834120290D006868F898,
	U3CRegisterU3Ed__42_System_Collections_IEnumerator_Reset_mF2CF7331A567F6154F1646A220FE0761C9F5C565,
	U3CRegisterU3Ed__42_System_Collections_IEnumerator_get_Current_mEFCF61E44FB08CD3980E911DFD283CB1B975C1E8,
	U3CSignOutUserU3Ed__43__ctor_m7AF2FE95FB2D59B9E754DB1F4726CF2A52FDD3FB,
	U3CSignOutUserU3Ed__43_System_IDisposable_Dispose_m006875FA63A705277D670FA68BFB2A8CBD98CFA9,
	U3CSignOutUserU3Ed__43_MoveNext_mE6703A6668E3B2979FC7534AC7211B8CD7BF6253,
	U3CSignOutUserU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m06ACB6E1E6BCAADB0C5F3BA67F3AB4B46378A9E2,
	U3CSignOutUserU3Ed__43_System_Collections_IEnumerator_Reset_m88908E66867BA6B4690525FE0F701D5EA923C20F,
	U3CSignOutUserU3Ed__43_System_Collections_IEnumerator_get_Current_m442644530018780CEB682F52CFEB0F61199F0B12,
	U3CU3Ec__DisplayClass44_0__ctor_m627B73A4D48F6AB116E47F8B5F957AC0402D2EC9,
	U3CU3Ec__DisplayClass44_0_U3CSendVerificationEmailU3Eb__0_mBDB6187C93744EE020BC1272E53570621D9320F6,
	U3CSendVerificationEmailU3Ed__44__ctor_m5C54FE487B46478D8C516CAF9E59BC43DF4EB692,
	U3CSendVerificationEmailU3Ed__44_System_IDisposable_Dispose_m412F1C5062FE68461A015CC9BF0254421A595412,
	U3CSendVerificationEmailU3Ed__44_MoveNext_m2301BD9DACE144DFA68DD53F093DE511CFC8A8FC,
	U3CSendVerificationEmailU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCA75C6FA9841A50F5708C698C80C306C8BD5C337,
	U3CSendVerificationEmailU3Ed__44_System_Collections_IEnumerator_Reset_mA72C0B89C7DFA5D646495D58F74E54D75CD7D380,
	U3CSendVerificationEmailU3Ed__44_System_Collections_IEnumerator_get_Current_m001C0D9CFAD17F8F5295CD1145FF50615A2C0C74,
	U3CU3Ec__DisplayClass45_0__ctor_mE0217B46E93186604428805E5B99B80855975999,
	U3CU3Ec__DisplayClass45_0_U3CSendEmailResetPasswordU3Eb__0_m9EDB9F24886FD93D20E4DB7B6BB71E1E43023FD7,
	U3CSendEmailResetPasswordU3Ed__45__ctor_m757642BC766586B6287ADF6EB0876B83D7FA9DD3,
	U3CSendEmailResetPasswordU3Ed__45_System_IDisposable_Dispose_m37647C9E309BD7F74F41C8E5EF8AB4C1E00DCA95,
	U3CSendEmailResetPasswordU3Ed__45_MoveNext_m303FF866785CB59F0E1E398AF67CAC0CCCEB2B14,
	U3CSendEmailResetPasswordU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44DBBBD44B4354DAE3B4C35857C75486B10774AE,
	U3CSendEmailResetPasswordU3Ed__45_System_Collections_IEnumerator_Reset_mFF9480AE1A71224B2C03372A35DCCA92C07D2186,
	U3CSendEmailResetPasswordU3Ed__45_System_Collections_IEnumerator_get_Current_m79F279C1999CEF3612353B65B16DF549E6FA4EF6,
	U3CU3Ec__DisplayClass46_0__ctor_m950B3D710D3F545E0D3987DA0A069E1F6C74194E,
	U3CU3Ec__DisplayClass46_0_U3CUpdateProfileU3Eb__0_mC68B9F81721D44F21AAC0F3A631CA2EFDDDCEE5C,
	U3CUpdateProfileU3Ed__46__ctor_m0DA4679A5EC520B73C1306FDBF04AACD8F2F4133,
	U3CUpdateProfileU3Ed__46_System_IDisposable_Dispose_mE6FEC49BBBED4DE5217B8F567D78B1CEA234DBBC,
	U3CUpdateProfileU3Ed__46_MoveNext_m9BA808CA8F31EB4C6F99CD927CBABC9D3B40A3E0,
	U3CUpdateProfileU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD6B6B2B74E31A686EAC9A35867981BDB145DEDF8,
	U3CUpdateProfileU3Ed__46_System_Collections_IEnumerator_Reset_mDD319D639D94A339CEB4D81D8F50D99788AE0028,
	U3CUpdateProfileU3Ed__46_System_Collections_IEnumerator_get_Current_mE39741D0E6655303119F2B6C573A7FE16EB3E3AB,
	U3CU3Ec__DisplayClass47_0__ctor_m074A21CF02D9B78611BEFFB63E2B451C138ADF70,
	U3CU3Ec__DisplayClass47_0_U3CUpdateEmailU3Eb__0_m64D387E6E2F4F0EEAB2228FD57EE3524BA3781A2,
	U3CUpdateEmailU3Ed__47__ctor_m5CE201273557713D8C0CEEB9BAB82CBD96F6E610,
	U3CUpdateEmailU3Ed__47_System_IDisposable_Dispose_m6B221ECE5290EA17C11DFA3778FFCF65ADCC2CF3,
	U3CUpdateEmailU3Ed__47_MoveNext_m4BFB6B2F23F3ED16C0E1A720E8FFE2D2F161BCB5,
	U3CUpdateEmailU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50EF9FDD445F0C71D5A7523F66F6AB43E34BC375,
	U3CUpdateEmailU3Ed__47_System_Collections_IEnumerator_Reset_m58C4308C2013210D6D2157E4F9DFC0D06824B5BD,
	U3CUpdateEmailU3Ed__47_System_Collections_IEnumerator_get_Current_m59344BC1E05FF12290FEA0B9E02C8D5A438D1CFE,
	U3CU3Ec__DisplayClass48_0__ctor_mE8825A59F9E005BC70C239833F75C45565BDF6A8,
	U3CU3Ec__DisplayClass48_0_U3CUpdatePasswordU3Eb__0_m29BAF83052654B25F7C02BF69407D858D3E4F49E,
	U3CUpdatePasswordU3Ed__48__ctor_mAA88E72D4668D9B07073BE1DAD60735C39FC5B0E,
	U3CUpdatePasswordU3Ed__48_System_IDisposable_Dispose_mDC8909F8221BE4610B673BC4995B60C7E16DA98E,
	U3CUpdatePasswordU3Ed__48_MoveNext_mF861008803A1C37A0B434F53FC0C463F7C80432F,
	U3CUpdatePasswordU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FBABE4C7FF4B09D68F84ABBB5A5BBFD2DB43703,
	U3CUpdatePasswordU3Ed__48_System_Collections_IEnumerator_Reset_mE1198FF4C8A04E34A168BAC8E263304CDC811EB3,
	U3CUpdatePasswordU3Ed__48_System_Collections_IEnumerator_get_Current_m01608B9CA30675CBE7757A308E31F11C55D499EE,
	U3CAuthErrorActiveU3Ed__49__ctor_mFFBC47D6C8C8EE7B5EA163B9902006C95C5E948E,
	U3CAuthErrorActiveU3Ed__49_System_IDisposable_Dispose_m2EEF93DF611A6839B57995362B5C9FFD4641D547,
	U3CAuthErrorActiveU3Ed__49_MoveNext_m8CB391EE29ABEFAD615E4B6FF912C61B1BCB5A71,
	U3CAuthErrorActiveU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF7FE852369288B0D8B12988B6457E618B0BAD583,
	U3CAuthErrorActiveU3Ed__49_System_Collections_IEnumerator_Reset_mD85CF991E5A1B7AA728FFA15A26A753A53127D2D,
	U3CAuthErrorActiveU3Ed__49_System_Collections_IEnumerator_get_Current_mADFF12D3685EB688EF69AB6882A42D8223EB34EE,
	U3CChangeAuthErrorActiveU3Ed__50__ctor_m34A47F826214243C0907412D72EA4582C9263CCC,
	U3CChangeAuthErrorActiveU3Ed__50_System_IDisposable_Dispose_mDE1896D88DAF41D3E2A3EEA90049CFC26B74A14B,
	U3CChangeAuthErrorActiveU3Ed__50_MoveNext_mC04ABF94EFEFDAE672B9A5605224D97CEB62B402,
	U3CChangeAuthErrorActiveU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m171D3454F5EC31FBCE4C2A06242B46ECA904E011,
	U3CChangeAuthErrorActiveU3Ed__50_System_Collections_IEnumerator_Reset_m3353920BED247E05E8A8842E19CCC4AA1D05502C,
	U3CChangeAuthErrorActiveU3Ed__50_System_Collections_IEnumerator_get_Current_m5F0AFC3D87064D259F4F64647BD68E165CD8BAA5,
	ButtonManager_Start_m1031E938A2FB8D85F993AA7DF29D06DAF0CDF930,
	ButtonManager_ClickBack_m0002732635A63052989846BED837A7B0911A25AD,
	ButtonManager_ClickButtonGroupSelect_mDA32E75E33587B0B2C1D43F09B4358A579614DE2,
	ButtonManager_SliderTrigger_m89DC503FE0C253F2D40F77016E2E520C07E9F0BA,
	ButtonManager_ClickChangePallet_m9A2509CE56135FA16D2DFF4055416B9E007E6618,
	ButtonManager_OutEndAnimation_m66FF610E7A3E821FFF5B9447E25F7D8257FE5274,
	ButtonManager_StartChangePallet_m2742BB38D0B570101CAEBA3E0CCA85B1B61EF891,
	ButtonManager__ctor_m7DBD91D2AF27494F6AC7DC74DE679919AB1DD71F,
	U3CStartChangePalletU3Ed__13__ctor_mF756C84C7C30361C69DEF892C73B1DFDA0561F01,
	U3CStartChangePalletU3Ed__13_System_IDisposable_Dispose_m3AB6D9B337287CD393B345141A4D758BB5302615,
	U3CStartChangePalletU3Ed__13_MoveNext_m20747389C99D9FF7D1C5DC8E5885EE2771D72251,
	U3CStartChangePalletU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31E23F424F24BC59FC51444B25D2D12B440DD9A1,
	U3CStartChangePalletU3Ed__13_System_Collections_IEnumerator_Reset_m81E4A239130CDF3791CA184E686D9C812BB24B73,
	U3CStartChangePalletU3Ed__13_System_Collections_IEnumerator_get_Current_m38E85BB85CE1CA5B76A84B62FD4AE9DB3FD28493,
	MainMenuControl_Start_m5A990AA29D2379423480E21FD217E06CFA1DB7E4,
	MainMenuControl_OnEnable_m9F8939ECC76B39D9E500C334B9AB8A1796523D79,
	MainMenuControl_OnDisable_m770BC4441ED271511900D310E94F3501E7664A90,
	MainMenuControl_GameStartConnect_mA95A58AAC0CE87FEFBD3B137F897788A7D319AE0,
	MainMenuControl_GameStartHost_m13FCBFDCE985465367DEF82E6A4024FA06DE457D,
	MainMenuControl_Update_m4F18C263EA6A5D1515F465ACBB42F6010DEF91EF,
	MainMenuControl_UpdateCountPlayers_m0A40191194D38DE7679EBFE8CF4B4F0C393668A4,
	MainMenuControl_ClickBack_m61132060455D361C68ED0A02ED106A9B74537D60,
	MainMenuControl_ClickCLoseAllWindow_mEFE7428D9B7DDBF7EE2CBD3573CA15DE2FAD1B2F,
	MainMenuControl_ClickAddWindow_m14C01F1B483F13A22D3B4446D4894513D5CAD1B2,
	MainMenuControl_ClickOpenTelegram_mB532914DC4706441881D9136FC371C0EBC985F9E,
	MainMenuControl_ClickOpenVK_m8D974E9492763A4A4FE4F933F32BE1D6D95B6D02,
	MainMenuControl_ClickOpenDiscordServer_m063B6651D3ACD6BCC37724957404B857A63AF873,
	MainMenuControl_AddWindows_m667081B4BF5D0969D8359CCDE0B4A3D8704BEB97,
	MainMenuControl_DelWindows_mEA75916FC791F50DBD80B450BECB5474890C230B,
	MainMenuControl_OutEndAnimation_m9D6F6DB72FCD5BF4F8D20A418AD27437C427748D,
	MainMenuControl_ButtonPlayAnimation_mC5F9B7674407F439F148DA7B5448940D19EB0A46,
	MainMenuControl_ConnectingToServer_m9815C9BB7C63A4E6CF256D7B7D1862EEFD49A7A2,
	MainMenuControl_HostToServer_mEBC8D45D87D56935EA9002FC3BC13561ED9904C6,
	MainMenuControl_ConnectingToLRM_m031A8E0AF2774195BB4DC23979C7873E0963FF52,
	MainMenuControl_CountOnlinePlayers_m75EC274D561D4A318B136654536455FB4C4B2E6E,
	MainMenuControl_ClickReConnectRelay_m09AFB3388898EEF625A5FEFE453D5856D591624C,
	MainMenuControl_ClickReConnectServer_mFEBE38B2C485D110533CE06E71E2D4635A48D770,
	MainMenuControl_ClickExitGame_mA1BFCE023E61AFEEFE872E1543FE1486CA7725B3,
	MainMenuControl_ClickResetWindows_m1C0A7ACAD38662501DB670582454FD86A01E8EB2,
	MainMenuControl__ctor_mE98B75947C78BB3D9BE987B56E5B6DAADC24063F,
	U3CConnectingToServerU3Ed__35__ctor_m433961DBFFC8B29B9FC91DEAAB6FA84DCAEB2542,
	U3CConnectingToServerU3Ed__35_System_IDisposable_Dispose_mC0128468E20E7A9048CA3F747C65D55F9714E6D6,
	U3CConnectingToServerU3Ed__35_MoveNext_m362E82EE2D14AB570D960DF55403FA13006E9091,
	U3CConnectingToServerU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m916BDF74A0BF3FF7D6F6EAB675CE3365447DB6BE,
	U3CConnectingToServerU3Ed__35_System_Collections_IEnumerator_Reset_m6C300616782C1B47071BA1E44101D49E29864B06,
	U3CConnectingToServerU3Ed__35_System_Collections_IEnumerator_get_Current_m399ADB384FA7DF8E5E8D0B75E463E301E3C58867,
	U3CHostToServerU3Ed__36__ctor_m4CFC93BE8001B38E6B4224AAABE23B128A575309,
	U3CHostToServerU3Ed__36_System_IDisposable_Dispose_m9A19C5632AADB17F029407A23C4E538DD957B6BC,
	U3CHostToServerU3Ed__36_MoveNext_m25E74AD52EEEF02DD5598402AA692C2CFD235C17,
	U3CHostToServerU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22A38B6EBE276F63F8B72721BAC4D3F768305851,
	U3CHostToServerU3Ed__36_System_Collections_IEnumerator_Reset_m01F45D77FEC0C7844D9DD4E2298360FEE574177C,
	U3CHostToServerU3Ed__36_System_Collections_IEnumerator_get_Current_m7450718E3367504B463D65A815BA476ECF4E2823,
	U3CConnectingToLRMU3Ed__37__ctor_mFB518AE4EB735116FFF09EA3E40EF10A3CD242B0,
	U3CConnectingToLRMU3Ed__37_System_IDisposable_Dispose_m8434195EFDD6629268A5826B0F8C9AC3B2FC684A,
	U3CConnectingToLRMU3Ed__37_MoveNext_m6C422959F75983C76A13AC9FF8E863321D397D9B,
	U3CConnectingToLRMU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCA86DB31BFB9CF05B0BD58546A5896CB39FFB614,
	U3CConnectingToLRMU3Ed__37_System_Collections_IEnumerator_Reset_mE9D55DAA28D3A4B8DF7D0A51B2E231B868696618,
	U3CConnectingToLRMU3Ed__37_System_Collections_IEnumerator_get_Current_m8E2AA08B940F0F334DA6EB90CBE19C7167707568,
	U3CCountOnlinePlayersU3Ed__38__ctor_mA12C83277514599CB1F8BA620FB116395AC1E84E,
	U3CCountOnlinePlayersU3Ed__38_System_IDisposable_Dispose_mC94ADECBE3C7B0DDDBA3720468D8A549B089AD8A,
	U3CCountOnlinePlayersU3Ed__38_MoveNext_mF099087BE6178F294172AC700E0AF903FCF7ABD6,
	U3CCountOnlinePlayersU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCFB03C179CB78D57B46CF35A8060CC89772AC970,
	U3CCountOnlinePlayersU3Ed__38_System_Collections_IEnumerator_Reset_m5F7BB953FB2413303FAF8989F82FE1D072439091,
	U3CCountOnlinePlayersU3Ed__38_System_Collections_IEnumerator_get_Current_mBEA02B72DB4677596562B14C14F9D5A44AB1EF02,
	PalleteWindow_ActivePallete_m271CA227AFA80571686F2B2C0097C8D587964A4D,
	PalleteWindow__ctor_mE94FEADFBEF02B5CC6DF427BF3714896CB517CF6,
	PauseMenu_Start_m4BBF1E67B42A2E936C792E83778ADCD87DE3C80E,
	PauseMenu_Update_m5097E74BFD4385B73BA9EF7198886DCEC7DF9A83,
	PauseMenu_OpenPauseMenu_m0E0CC7533A8CBC96C8834E9C76C3E706466E496D,
	PauseMenu_ClosePauseMenu_m3219C11348E15C30E63C2F6CEC824ECED86A3529,
	PauseMenu_Back_mE8BF9FE1496D39D84FD49595FC0E13456C035E68,
	PauseMenu_ClickWindowSettingsMenu_mFB868B201C44AEA0BC39473FD47E4BEA3BA9D0E8,
	PauseMenu_ClickWindowSettingsAdvanceMenu_m6D2EA061912D92E875C452A8BBBA6949EC594B37,
	PauseMenu_ClickWindowSettingsKeyControlMenu_m1C3F422553096BBFE527F0E1BF27A0D7259FEDAB,
	PauseMenu_AddWindows_mF5F7FD662E0128F1645BD4E6F61D4BE3E4744D73,
	PauseMenu_DelWindows_mAE992E139D2CB4E18072BBDA89C0A0573102EBB1,
	PauseMenu_OpenMainMenu_m87B5B52024C75DE90EE17FE84822C7BF7AF822ED,
	PauseMenu__ctor_m81B0E020DC5008DA4D414200BAAF7122B430D826,
	CreateServer_OnEnable_mFF3131AD614D24B0B16C2BCBBB193213C034E4B1,
	CreateServer_Update_m91FB4B6FE5CA66CD6A767673D75FF800F3DE695B,
	CreateServer_ScrollView_mFF5D45DAF5E777B96E7D102DC8DA9A295BF8FF84,
	CreateServer_ClickLeftMapsScroll_mDE52B63B7C97F67FB9F245534A7450BCA7E33A41,
	CreateServer_ClickRightMapsScroll_m59D294E5E2D29296902F695828A82FED07F01E2A,
	CreateServer_ClickLeftModeScroll_mAE33E06DBD56B79E0422026E82011FEA0EA7F0D4,
	CreateServer_ClickRightModeScroll_mCB389617A86D403925EFA251957D2BB598ED8BF5,
	CreateServer_StartHostGame_m7A2CDCEE4B4310CE1D990E24379BB9029F35A572,
	CreateServer_ChangeValueCountsBot_m52A6A966183A41804923786F8512BCA70FBFF573,
	CreateServer_ChangeValueCountsPlayers_m82D6D766477333F1205BF35BF9DC5D61CC99B345,
	CreateServer_EditInputCountBots_m0CEB3A23FC4C618AE0F52803B984EAE09EE2649E,
	CreateServer_EditInputCountPlayers_mC2D5E21020FD1406FA68C6788AB50B03BED3EED4,
	CreateServer_CheckCount_mF33BAB8B7992DE12F780B62092711B6D76EB3581,
	CreateServer__ctor_m5F7B30DFB1AB3E6E145D1B66DE75EDC81DDC430D,
	ServerBrowser_OnEnable_m6C01059CC8F8BDA8F7E2B43F27A1FC674BC304FC,
	ServerBrowser_OnDisable_mB8AF8DC7EDC61FDA825CF5E84423A4D09013BB2E,
	ServerBrowser_RefreshServerList_m3F6AFFE1B9E995B55B2EAC15CE6105E7EADB48D4,
	ServerBrowser_ServerListUpdate_m44068C4908946B58DC0D59DEE8ACBA96497CFC1B,
	ServerBrowser_SwicthSelectServer_m17DDE9D78CC2BBEFFEDA6C30A350CAEF6C98939D,
	ServerBrowser_ConnectToServer_m515AC6A4016D09DB024CE09D03E5A0583646E66E,
	ServerBrowser_StartPing_mC4E889626730F032113850A9730C1DBF782EEFEB,
	ServerBrowser__ctor_m408A0ED06C701707905968057C18252CE1267F23,
	U3CU3Ec__DisplayClass8_0__ctor_mE36EEEC20B88163FC227156C5CF0062C049C435E,
	U3CU3Ec__DisplayClass8_0_U3CServerListUpdateU3Eb__0_m9A338534DE0704CEAAB6EC98D3B8E38F02AFB7D5,
	U3CStartPingU3Ed__11__ctor_mBD47212DE0932EB92DE8D77ED07501B9B31C1818,
	U3CStartPingU3Ed__11_System_IDisposable_Dispose_m1FF785A3ABA494062420B116E7087E22756F58A8,
	U3CStartPingU3Ed__11_MoveNext_m13B9943B3055BA7DBE3DBA3AD639277015EF4164,
	U3CStartPingU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE45FDCF25A97219E109F9958B3397C36DED41EC,
	U3CStartPingU3Ed__11_System_Collections_IEnumerator_Reset_mC506869216121BD2FF45403CFC46C83446E0A4E0,
	U3CStartPingU3Ed__11_System_Collections_IEnumerator_get_Current_m1F02CDBA07F7975FD7809AF88CBE376189C21256,
	ServerInformation_get_GetColorNormal_m5B1BACB3FF1C6C19161F02EA535034FA578C9896,
	ServerInformation_get_GetColorSelected_m6BA48D3A071A56E47BB602B940F241B40CB9EAEE,
	ServerInformation__ctor_m0FA109DE412552EBF59982C1AEADB8732956C0FF,
	SettingsGame_Start_m4DF9EFDA27FEAB7625BF2343378702C9EB2E10EC,
	SettingsGame_ChangeLanguageForEng_m5D8146DCE1D473E19DE5E1FA99BBB4A54BEE0067,
	SettingsGame_ChangeLanguageForRus_mE1B951867E371DF72FF2D7294ED750F477CA1E95,
	SettingsGame_ChangeMusicVolume_mBDE070825C5236785F8ABCCE364DCE1545CD6D24,
	SettingsGame_ClickSwithMusicVol_m054154B5D3CD504A34135B96B2FF7D4483E42F12,
	SettingsGame_ChangeEffectVolume_m09E33AE4FD40BC267CF9711C6737C4FD3BCB310E,
	SettingsGame_ClickSwithEffectVol_mBE1684FFF7004ACB421831212885F21EC246482A,
	SettingsGame_TextUpdate_m6DBEA27F0A0CA11DDFA6CDE1FD66D9C23768DCA4,
	SettingsGame_Save_m7283B2803FAC4A8CABB9846E3027A891778F5528,
	SettingsGame__ctor_m5612C103BE6229F483F4FAB73410FD00C5E359E0,
	DialogMenu_EscToGameExit_m7C9C15975826857BBD558F4857812F2FFB888962,
	DialogMenu_EscToMainMenu_m4996D0BD8D082402D65F294E4299D801AD5D4845,
	DialogMenu_ExitDialog_m34335913385F04A3BA60956919AF2094E357889A,
	DialogMenu_ExitToMainMenu_m490C9E9C2E7B090426B115B1739119B4FDFC3A0B,
	DialogMenu_ShortDialog_m61294B266DF82F9CB273B9A807BBDAE9792464FC,
	DialogMenu_ExitGame_m99AE1F4242A555FF08C3579F5F0F9B4B59B94239,
	DialogMenu_ExitMainMenu_m99AF88FE0A8ED7C462384D6F8EF79483C6DDC940,
	DialogMenu_DialogExitClose_m565824B4E9AC555EC3D50C79A5675B7F2BB97C3A,
	DialogMenu__ctor_mB0CE5C1D992746EBC2C02254E77E8E36E8B42B62,
	ErrorMenu_ErrorWindowUp_m0BBD92A4BD6F535E8963ADC7F94947B4D8202D24,
	ErrorMenu_ErrorWindowClear_mBA64B569F267C14006FC70C6CECABBCCDD2381AB,
	ErrorMenu__ctor_m83D6DEE3E842D045242E239E4DCE7D578EEDB8D1,
	LoadingMenu_LoadingWindowUp_m04C003133686D43F3586281EBC6C37D70724A5F0,
	LoadingMenu_LoadingWindowClear_m82AC3ECD452F1C9A9B1FEBF3F4301A79CC26FD96,
	LoadingMenu_LoadingStatus_mE9F7AC74460344CE76E4449CAA6C601B71CDF0C5,
	LoadingMenu__ctor_m12E993E7328315265B3C2FB1D84F7A68C7B1A8EF,
	U3CLoadingStatusU3Ed__5__ctor_m11D5EB9F7BB96A2825B1B5D1240ED2A029D47AAE,
	U3CLoadingStatusU3Ed__5_System_IDisposable_Dispose_m3325C7495797B66A333178E1BDB082CF881452C7,
	U3CLoadingStatusU3Ed__5_MoveNext_mEC4878D65D6066A9313021450C7BF35E9A25478F,
	U3CLoadingStatusU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7E28D7C17E2A100B640816FF452A379A812A7D5C,
	U3CLoadingStatusU3Ed__5_System_Collections_IEnumerator_Reset_mB868070640E6326C2CEC525E5D539D4CE28C5F80,
	U3CLoadingStatusU3Ed__5_System_Collections_IEnumerator_get_Current_mF91916F9105E83BC5F9C68158E7F05C07F9D76CB,
	Bullet_Start_m6BD187DD353835D248DA404B169DCE29CEB2B813,
	Bullet_OnEnable_mE783E45F0A09A5565B6A9817C8D3156A408A53BF,
	Bullet_OnDisable_m705119D56961445D3C8DD69A6CAA460C7359A5E5,
	Bullet_OnBecameVisible_m601E3E5C4F1AC56FC4D596AB558FFAB6BC28FD02,
	Bullet_OnBecameInvisible_mA5A705C76BF9F9C6A33E633BE4547E3958871A43,
	Bullet_Go_m77E9D58C5D0847200F5CE9AB4072661BCE94BE36,
	Bullet_OnTriggerEnter2D_mC3642C0546BCF5AFC8036C6B77D66560C04685E8,
	Bullet_InvisibleBullet_mBBC7AD27848FCBBFF76DE715AB89F1340B08E683,
	Bullet_Update_m5AA63D0B1F389C2CFEE77466E1C39ADC813B4DBC,
	Bullet__ctor_m873C02F2114EA93A35E4392013AC831246756CBA,
	Bullet_MirrorProcessed_mB5F9D324A3A08B06AABEE992B06E0CCC12585F1D,
	HitKnife_Start_mB2CB7618396AC44864AEF10C1EFA108268655775,
	HitKnife_Update_m8AEA492C52D896C5DEC4317772E44E6856F60D2B,
	HitKnife_OnTriggerEnter2D_mE50DCB6F60F61FC8C3702B234776B2F2FD35220B,
	HitKnife__ctor_m48AE5DEDA4DA42AC06D1D6C34281A7F5C452A4F6,
	WeaponControl_Start_mBDA1FE7AF9673E190ED499FCAE108B6F9A147BE6,
	WeaponControl_OnEnable_mBAD7B426B159119F0C27F064D7432BAC8784C57F,
	WeaponControl_OnDisable_m5D5CFD1D658180C90542C5B7AAB89F8241877F45,
	WeaponControl_Update_mDB82A41BDF78DFC1404D281879CC134FCFE43D4F,
	WeaponControl_ClickButtonFireEnter_mBAE8622104C0FC53C1A4DC423B0910B179C651CD,
	WeaponControl_ClickButtonFireExit_m5EF84ADE5922B05246AA042E8A9D601768A903D1,
	WeaponControl_ClickButtonReloadEnter_m359F4ED13342F639AE4C0C1DBC6160D9B212B5C0,
	WeaponControl_ClickChangeWeapon_mC0B1B607B53E92D6DAA6B1439C3CA01BA198165B,
	WeaponControl_KnifeAttack_m19078CFBDF066F974B11DF2DE83E5E4E176F3541,
	WeaponControl_Shoot_m03B21B1FD4906B69B5B0BD9876D364B681533485,
	WeaponControl_Reset_m86118D956763988627EF36DDEFA1D3248BAF8DCB,
	WeaponControl_SwicthWeapon_m5FACD294BF791B7AD51A3E09F526C0EA0E9DD284,
	WeaponControl_Reload_m7F5CB27F85E70B5CF01510E7345FE136AB9EF431,
	WeaponControl_PrograssBarFade_m470EB457E7A2A2CD2D75B802AF70EFAAF22DF197,
	WeaponControl_ChangeWeapon_m7D5F51638F94266D7C3C58A8E472DEB1115A3F2B,
	WeaponControl_RpcStartReload_m7F17D208E1581E5C0F8A389A1E3D7F7153892FD5,
	WeaponControl_RpcStartChangeWeapon_mF7795D82EC3A86E93DC323F996E7F0F25FA4CD66,
	WeaponControl_RpcShotFire_mA0C1DA5A85B4295076A7DE7BD0C91C1CB2666FC2,
	WeaponControl_CmdReset_m3D82E9A4F056E2AE37BDC6EFA0EA156D26E75BAA,
	WeaponControl_CmdFireEnter_mB7C14DBFF6005C964A9A10723652210278CF354A,
	WeaponControl_CmdFireExit_m1D94572AAD3580DCE1F0F49085F136C5D533B58F,
	WeaponControl_CmdStartReload_m15CAD01E5F0817872265F1101FDF4F906792B364,
	WeaponControl_CmdStartChangeWeapon_m5F6597DF1827F878AD087C6A4547D30775403F95,
	WeaponControl_CmdShotFire_mFCE0622BFD3DE203E063EAD8922B5C8D1A738109,
	WeaponControl_CmdStartKnifeAttack_mD9A30A45F85ECF8B58980B04F00CE9DC0666C6EE,
	WeaponControl_CmdEndKnifeAttack_m4A342F0F082C20E7F19117C8335C7FEC0CDD31D1,
	WeaponControl_CmdKnifeAttack_m4BEDB8D08F811144E58754F1BCD25119575D4F7F,
	WeaponControl__ctor_mE4FE7F4BCFA5FA6381D285AC5F9775F5C0D25A2C,
	WeaponControl_MirrorProcessed_mC1CADD7165682003EB95FD631E48DF4071A76BAE,
	WeaponControl_get_NetworkIsFire_m56110E930FFB16DD9038274979ADEB9B0E2F5649,
	WeaponControl_set_NetworkIsFire_m9E6AFF8BF211C11F6EA53A0D660DF51F5A3E115F,
	WeaponControl_get_NetworkIsReload_m8C96F0E687DEB81495ADF25873E0D4EC21621235,
	WeaponControl_set_NetworkIsReload_m42DCB131625C2EBC0E0078A31588C77471CDF9A7,
	WeaponControl_get_NetworkIsDraw_mCD1A2251C03AD2EB28C9BA20A8858953DD7612B0,
	WeaponControl_set_NetworkIsDraw_m5BA850BF2768D781C0F93F01980FB748C4EEA5CD,
	WeaponControl_UserCode_RpcStartReload_m1C9479FFF870C40788C54B3BE926FF8175E62964,
	WeaponControl_InvokeUserCode_RpcStartReload_mCF31C958234C8E7E4816893D564B3F0417BA4C6E,
	WeaponControl_UserCode_RpcStartChangeWeapon__Int32_m61573DDA39B5DFCBFE348F9E1BD56D4E13D93325,
	WeaponControl_InvokeUserCode_RpcStartChangeWeapon__Int32_m3BFB7BE6FFA8E8DFA908A484CC119710EDDBBCEA,
	WeaponControl_UserCode_RpcShotFire__Single__Int32__Single_m11E21573C5F56D10582780F695C1D52695FC399D,
	WeaponControl_InvokeUserCode_RpcShotFire__Single__Int32__Single_mD277C6D7DB5E2F6AFECDDC629644BA38C803C06A,
	WeaponControl_UserCode_CmdReset__GameObject__Int32_m234CD5D411126FB490B95C730F71B8B780DCE43D,
	WeaponControl_InvokeUserCode_CmdReset__GameObject__Int32_m25AD7726FF38547E422D76EECD4E6105FA1876BF,
	WeaponControl_UserCode_CmdFireEnter_mB8481B162A29F012AF0950B8B3AD16C9FED4EC23,
	WeaponControl_InvokeUserCode_CmdFireEnter_m2D3A0E9B53F5DB6760BB6CAE13A45132E3991C44,
	WeaponControl_UserCode_CmdFireExit_m4C637652D181F389EF42ABD950BBAC666437521C,
	WeaponControl_InvokeUserCode_CmdFireExit_mE8DD27A33EF20371B3F33EF2AB2B3BABF31A4996,
	WeaponControl_UserCode_CmdStartReload_mCDD3FF59349DA26584FA20334E8C2B5C0EE20729,
	WeaponControl_InvokeUserCode_CmdStartReload_m41DA12F90B81972E22C08BEA09725F13CDBB2204,
	WeaponControl_UserCode_CmdStartChangeWeapon__Int32_m290CE8A164C77DDE8511F8787007B72EF60C1B4A,
	WeaponControl_InvokeUserCode_CmdStartChangeWeapon__Int32_m452E3D89A5EC19E718564CF186B07DA55E4403BB,
	WeaponControl_UserCode_CmdShotFire__Single_mC907DE481CDE0B6593A0444BA8FB042F00F23A6E,
	WeaponControl_InvokeUserCode_CmdShotFire__Single_mD21A0D8E7913F807E5FC94014ACAB9ED130F8F18,
	WeaponControl_UserCode_CmdStartKnifeAttack_m7E093F0F9B2C3734FB3307BF9759D8B0A5E3EDAE,
	WeaponControl_InvokeUserCode_CmdStartKnifeAttack_m6FF5AB5E46BBC166077BD3BF17EAEA53944B894E,
	WeaponControl_UserCode_CmdEndKnifeAttack_mD4D87A0B210A2CD305C4A709365979DE3F0CA30C,
	WeaponControl_InvokeUserCode_CmdEndKnifeAttack_m8AD0D4422CB843058445A296C0F7411996C59F1C,
	WeaponControl_UserCode_CmdKnifeAttack_mAD4DF5E7CC0C09269E2FA24DAB8CA28362B4C5AB,
	WeaponControl_InvokeUserCode_CmdKnifeAttack_m8BF0733BE0CB88DE657A481C736BC2600E92EED7,
	WeaponControl__cctor_m61709E3C2D5929160CFE69910DBA7DDBB7CE4A8A,
	WeaponControl_SerializeSyncVars_mB288A9A0DDA6734B788F9C844B9EC4FB13DCBF54,
	WeaponControl_DeserializeSyncVars_m0788F81DCE731EFB206CCFA69B2075BE66BDE7E1,
	U3CReloadU3Ed__38__ctor_m2391BCB1B758CA8A33100D5C2C071C29FF4E82F3,
	U3CReloadU3Ed__38_System_IDisposable_Dispose_m980774824432B9864DB0241F711A9B10D764A934,
	U3CReloadU3Ed__38_MoveNext_m63B21D865DA25545A8E8623675366842273A0669,
	U3CReloadU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE2DF8003F5D1C1ED2D2EB0866800ED7042BBAD9,
	U3CReloadU3Ed__38_System_Collections_IEnumerator_Reset_mFD4F17B89F01E8F3315EDE66781F01A314DF4FB0,
	U3CReloadU3Ed__38_System_Collections_IEnumerator_get_Current_m6C97394B944EA7154D3B125406B56B8BCDB54DCC,
	U3CPrograssBarFadeU3Ed__39__ctor_m8852E5C1047AA92A294016BD6A24DA8CCB81A75F,
	U3CPrograssBarFadeU3Ed__39_System_IDisposable_Dispose_m1971B09CC635D50A6B35CF0E87D42EBBB7EC39F6,
	U3CPrograssBarFadeU3Ed__39_MoveNext_mA4396385D8535298CD6D0F77190F0B8CF8843424,
	U3CPrograssBarFadeU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE06A4DE2F1DA0F492F8A658E3EC1D399E2955D3,
	U3CPrograssBarFadeU3Ed__39_System_Collections_IEnumerator_Reset_m702B9CFC7DFD101545967C070FDE4C057B73C51C,
	U3CPrograssBarFadeU3Ed__39_System_Collections_IEnumerator_get_Current_m7B489ED2631687FCA36618C477396F9268AFCFE5,
	U3CChangeWeaponU3Ed__40__ctor_mBC4A4CE38A6976F737DAC02C93267CD5F3C12EC5,
	U3CChangeWeaponU3Ed__40_System_IDisposable_Dispose_mCC74DE0AF826B82D8A960BEECB51E13B7361F545,
	U3CChangeWeaponU3Ed__40_MoveNext_m29955CBB0A3999447C0E2DC141C915AC5B72CD12,
	U3CChangeWeaponU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDC9AABD1BFAF500E9621E7D076BE33DD0486A4CE,
	U3CChangeWeaponU3Ed__40_System_Collections_IEnumerator_Reset_m4B2CD62780E6167980B4F2F5A9439771EB00E049,
	U3CChangeWeaponU3Ed__40_System_Collections_IEnumerator_get_Current_mBEAC66F5EC124256C155BA32DB27ABC74680DBBC,
	WeaponSystem__ctor_mBEEDEE562EDAE8EF6104C6C88E792692B1A03E9C,
	WeaponSystem_MirrorProcessed_mD666B9BB91ED4FD51FFD06E1D58191A2DE6777EF,
	ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7,
	ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46,
	ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722,
	ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719,
	EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435,
	EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9,
	EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138,
	U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C,
	U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m3EF23BF40634D4262D8A2AE3DB14140FEFB4BF52,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA,
	TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9,
	TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0,
	TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3,
	TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D,
	TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3,
	TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C,
	TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82,
	TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5,
	TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C,
	TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3,
	TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908,
	TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427,
	TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D,
	TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF,
	TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052,
	TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1,
	TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A,
	TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6,
	TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189,
	TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475,
	TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33,
	TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6,
	TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02,
	TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB,
	CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E,
	SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963,
	WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2,
	LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2,
	LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC,
	Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9,
	Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4,
	U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D,
	U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m553F892690ED74A33F57B1359743D31F8BB93C2A,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD,
	Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA,
	Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB,
	U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC,
	U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC9F90586F057E3728D9F93BB0E12197C9B994EEA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A,
	Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29,
	Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1,
	Benchmark03_Awake_mDEE8E96AE811C5A84CB2C04440CD4662E2F918D3,
	Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D,
	Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6,
	Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90,
	Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27,
	CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788,
	CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5,
	CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F,
	CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F,
	CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878,
	ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72,
	ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695,
	ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A,
	ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371,
	ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88,
	ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56,
	ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B,
	U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71,
	U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1C76BF8EAC2CDC2BAC58755622763B9318DA51CA,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA,
	SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8,
	SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE,
	SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807,
	SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93,
	SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690,
	SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB,
	SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362,
	SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22,
	U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51,
	U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mB6C5974E8F57160AE544E1D2FD44621EEF3ACAB5,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5,
	TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D,
	TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6,
	TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0,
	U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2,
	U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9B7AEE80C1E70D2D2FF5811A54AFD6189CD7F5A9,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8,
	TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C,
	TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39,
	TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95,
	TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689,
	TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A,
	TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52,
	TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1,
	TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025,
	U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B,
	U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mD12057609EFCBCA8E7B61B0421D4A7C5A206C8C3,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186,
	U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5,
	U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mE5E0678716735BDF0D632FE43E392981E75A1C4D,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E,
	TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2,
	TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE,
	TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E,
	U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_m5783C7EE3A7D230F5D8FE65111F7F0B3AACF4A5D,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m641EA4B8AFFBA5E132B7EE46F0962E8CA6F860ED,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_mB9551C24D0AAF928ADC9DE924F2325884F0A2DB4,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85AD23CB0E7EC78A4A4B07652C7DCB90B27B5A52,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mDE1C8E5AAFA2BE515D1868F76D4921E627F51112,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m8FA714E4D7408B556C1A280744593DCD07D7B9D0,
	U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m093946C77D9946811A7799D06F8D7170FB42A8EB,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m11B84CE30C7048FC3BA409ABDF39AFFEA7AE188D,
	U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_m947F374D2AC3CE949148BC815B2ABBBD8E94D594,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA09D1AB4BEF083569018FF550A5D0E651CF9F2CD,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_mC3C5ED94D6E74FB8551D2654A9279CF3E9E1DEBE,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_mB487D32B2C46BA2099D42B89F2275CC454E4483D,
	TextMeshSpawner_Awake_m9A84A570D2582918A6B1287139527E9AB2CF088D,
	TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66,
	TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233,
	TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C,
	TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839,
	TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063,
	TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1,
	TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A,
	TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C,
	TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A,
	TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723,
	TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43,
	TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E,
	TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6,
	TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE,
	TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215,
	TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79,
	TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559,
	TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779,
	TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF,
	TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E,
	TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797,
	TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B,
	TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A,
	TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503,
	TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE,
	TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87,
	TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B,
	TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE,
	TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF,
	TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98,
	TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65,
	TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97,
	TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398,
	TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301,
	TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491,
	TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F,
	TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36,
	TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848,
	VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1,
	VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B,
	VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC,
	VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m319AC50F2DE1572FB7D7AF4F5F65958D01477899,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C,
	VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C,
	VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354,
	VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2,
	VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76,
	VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8,
	VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88,
	VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D,
	U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m15291DCCCEC264095634B26DD6F24D52360BDAF0,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9,
	VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867,
	VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304,
	VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23,
	VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E,
	VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77,
	VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38,
	VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB,
	U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m2F84864A089CBA0B878B7AC1EA39A49B82682A90,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B,
	VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E,
	VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21,
	VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5,
	VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1,
	VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D,
	VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F,
	VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198,
	U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5A5869FEFA67D5E9659F1145B83581D954550C1A,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0,
	VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0,
	VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47,
	VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47,
	VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB,
	VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7,
	VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0,
	VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422,
	U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A,
	U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDE5E71C88F5096FD70EB061287ADF0B847732821,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C,
	WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F,
	WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809,
	WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF,
	WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98,
	WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795,
	U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53,
	U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m07746C332D2D8CE5DEA59873C26F2FAD4B369B42,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19,
	GeneratedNetworkCode__Read_Mirror_ReadyMessage_mDECE83CB1F7CAE9FCAD92216F267E4E3908A96FE,
	GeneratedNetworkCode__Write_Mirror_ReadyMessage_m22A9F16A2ED7E7477E8A31FF1042EB955CFEC6C9,
	GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m37D2FAD98BE3268003281DD78F579902BCE0AB12,
	GeneratedNetworkCode__Write_Mirror_NotReadyMessage_m709FB1A91BB6E0372774E3BAB26503A3464D27AE,
	GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_m6ACC79432A87A0C5880AB67857D7FAB6827D7330,
	GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_m231493B9DF2D00D49C6C2E3A83C39AD189AD3017,
	GeneratedNetworkCode__Read_Mirror_SceneMessage_m5B5705BDB0E91D54BAB322CB078B3FD920FF63F8,
	GeneratedNetworkCode__Read_Mirror_SceneOperation_mD4ED81C2591AC8322FA7364DEFDE26D2EE4A2062,
	GeneratedNetworkCode__Write_Mirror_SceneMessage_mBD1A8505934B960E4F9812A5BA688DF968E35797,
	GeneratedNetworkCode__Write_Mirror_SceneOperation_mE4502A1970E814F38F917FE749E6B4AA55ED7D3D,
	GeneratedNetworkCode__Read_Mirror_CommandMessage_m0B599CA7B82D3B35311F0C5F6730041F39C22850,
	GeneratedNetworkCode__Write_Mirror_CommandMessage_m39DB7CB6D020DFA7DD3EBFFE4051433454FAAF8D,
	GeneratedNetworkCode__Read_Mirror_RpcMessage_m284BD1F0303CCF36765BC5FACF8DD3998785B231,
	GeneratedNetworkCode__Write_Mirror_RpcMessage_mF3D3F9507C55BC65DA9ACF43CF6798501C7981AF,
	GeneratedNetworkCode__Read_Mirror_SpawnMessage_mBEDBC1E005B2C268463C08A8FBECC4A352889194,
	GeneratedNetworkCode__Write_Mirror_SpawnMessage_mA5536FE3E9CDEB8DCE7EBEB08FADF0804180C82E,
	GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m69DC049A017111C5469BBF6E56EC2FFC9668B560,
	GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m0F0F15EA4F7C2EE4EDBCDC1E6A84807969166645,
	GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_m37449385EB28A378ABBF028640DAD41AB268167A,
	GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m6176D0EE3FCF5DE4E25DFA2B25BB76CF4D432530,
	GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_m4C7F65AA88BCCBCA1D61598503EC00CDFCB972AA,
	GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_m822D8A7F9BE22578E8328CEB230C4DCD07979611,
	GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_m2BAB658B17391EDDEDC4F22761289289C76E1B83,
	GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_mD34C28740181B2CFF6A4A5FE3A1F5CDE969F41F1,
	GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_mF2DD6D81FAE045AC90FD58F6D8B82E66009B59A1,
	GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_mDC761E38F362527307B8FA54B649D2B852A35F62,
	GeneratedNetworkCode__Read_Mirror_EntityStateMessage_m98FA70058010B17049B34B87AEBC6275D0965CF9,
	GeneratedNetworkCode__Write_Mirror_EntityStateMessage_mA6D54713D611B9216AB4778A6A92C072A8E6BA3A,
	GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m1E332657361099EA23D1BD7BEB42BEC6A8048182,
	GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_mDE8F039A87837E7C8EAE14539CFD4F7C82742C14,
	GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_mAD99024E75E186DF6AA03E6513368B4C30986599,
	GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_m39D438FAA612F77B0018012E1D0FD20BD5974A35,
	GeneratedNetworkCode__Write_Biohazard_BiohazardMode_m11DA497A740C38581B368E4E87FBFF0985A2AA17,
	GeneratedNetworkCode__Read_Biohazard_BiohazardMode_m8B0D17121F10F0E29870F2A7CB5C47A47FE5C3C4,
	GeneratedNetworkCode__Write_Player_Team_mE32D6B48C76F11E106C9C97A22D3D42DA17C3E58,
	GeneratedNetworkCode__Read_Player_Team_m5CA2CA4F79E65248AF5959E4B85D1FA6D9EA99AC,
	GeneratedNetworkCode_InitReadWriters_mF8776E1B3A143B9E4860920DCEB00C30A97369E6,
};
static const int32_t s_InvokerIndices[1198] = 
{
	6908,
	6908,
	6908,
	5521,
	6908,
	11511,
	9982,
	6908,
	11219,
	6908,
	12362,
	3982,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	3056,
	1610,
	3056,
	3056,
	3056,
	1590,
	3056,
	3056,
	3056,
	4899,
	4903,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	8757,
	6908,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6727,
	6727,
	6836,
	6836,
	6836,
	6836,
	6836,
	6836,
	6836,
	6836,
	6727,
	6836,
	6836,
	6836,
	6836,
	6836,
	6836,
	6836,
	6836,
	6836,
	6908,
	6908,
	6908,
	6908,
	3056,
	6908,
	6908,
	6908,
	6908,
	6908,
	3189,
	6908,
	5650,
	6908,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	6896,
	5648,
	1915,
	3041,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	3056,
	4903,
	6908,
	1407,
	6908,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	2488,
	6908,
	1610,
	4903,
	2381,
	1570,
	6908,
	6908,
	1570,
	9086,
	12362,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	0,
	0,
	10965,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	3056,
	3056,
	6908,
	4903,
	6765,
	4903,
	6908,
	5521,
	3068,
	6908,
	5521,
	5521,
	5521,
	2488,
	6908,
	6908,
	5521,
	6908,
	6908,
	6727,
	5485,
	6727,
	5485,
	6727,
	5485,
	6727,
	5485,
	2488,
	9086,
	6908,
	9086,
	6908,
	9086,
	5521,
	9086,
	12362,
	1915,
	3041,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	3056,
	1371,
	1590,
	5521,
	6908,
	5521,
	6908,
	3056,
	5521,
	6908,
	6908,
	1607,
	3068,
	6908,
	12362,
	6908,
	6836,
	5591,
	6908,
	9086,
	3056,
	9086,
	5521,
	9086,
	6908,
	9086,
	6908,
	9086,
	1607,
	9086,
	3068,
	9086,
	1915,
	3041,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	1136,
	944,
	944,
	582,
	6908,
	6908,
	582,
	9086,
	12362,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	5521,
	6908,
	6908,
	5521,
	9086,
	12362,
	6908,
	6908,
	5521,
	5521,
	3056,
	5521,
	6908,
	6908,
	5521,
	9086,
	12362,
	6908,
	6908,
	6908,
	6908,
	4903,
	6908,
	6908,
	6908,
	5521,
	6908,
	6908,
	5521,
	5521,
	6908,
	5521,
	6908,
	6908,
	6908,
	6765,
	6765,
	4903,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	4899,
	4903,
	2381,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	3136,
	6908,
	6908,
	6908,
	5521,
	6908,
	6908,
	6836,
	6836,
	6836,
	6836,
	6836,
	6908,
	6908,
	6908,
	6908,
	6908,
	3183,
	6765,
	5648,
	3056,
	6908,
	6908,
	5648,
	6908,
	6908,
	6836,
	5591,
	6896,
	5648,
	5648,
	9086,
	12362,
	1915,
	3041,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	5485,
	6908,
	6908,
	6908,
	6727,
	6908,
	6908,
	6908,
	6908,
	6908,
	3056,
	1610,
	3056,
	6765,
	6765,
	5521,
	6908,
	6908,
	6727,
	5485,
	6727,
	5485,
	6727,
	5485,
	6727,
	5485,
	6727,
	5485,
	6727,
	5485,
	6727,
	5485,
	6666,
	5418,
	6727,
	5485,
	6727,
	5485,
	6727,
	5485,
	5521,
	9086,
	12362,
	1915,
	3041,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5521,
	5521,
	6908,
	6908,
	6908,
	6908,
	6908,
	5521,
	5521,
	5521,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	5521,
	6908,
	3056,
	3056,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	3068,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6765,
	6908,
	6908,
	2383,
	1368,
	6765,
	6765,
	4903,
	2383,
	4903,
	4903,
	4903,
	4903,
	4903,
	4899,
	6908,
	5521,
	6908,
	6666,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6666,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6666,
	6908,
	6666,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6666,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6666,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6666,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6666,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6666,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	5521,
	5521,
	5418,
	5521,
	6908,
	4903,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	5521,
	6908,
	6908,
	6908,
	5521,
	5485,
	6908,
	5521,
	6765,
	6765,
	6765,
	6765,
	6908,
	6908,
	6908,
	6908,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5521,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	3041,
	5485,
	6908,
	6908,
	6908,
	6908,
	1088,
	6908,
	6908,
	6908,
	6908,
	6908,
	5591,
	5591,
	6908,
	6908,
	3068,
	6908,
	6908,
	6908,
	6908,
	6908,
	1613,
	6908,
	2383,
	6908,
	6908,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6670,
	6670,
	6908,
	6908,
	6908,
	6908,
	5591,
	6908,
	5591,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	5521,
	5521,
	5521,
	6908,
	6908,
	6908,
	6908,
	5521,
	6908,
	6908,
	5521,
	6908,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	6908,
	3041,
	5521,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	5521,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	5485,
	6908,
	5591,
	3056,
	6908,
	6765,
	9706,
	6765,
	6908,
	5485,
	1670,
	3056,
	6908,
	6908,
	6908,
	5485,
	5591,
	6908,
	6908,
	6908,
	6908,
	6908,
	6666,
	5418,
	6666,
	5418,
	6666,
	5418,
	6908,
	9086,
	5485,
	9086,
	1670,
	9086,
	3056,
	9086,
	6908,
	9086,
	6908,
	9086,
	6908,
	9086,
	5485,
	9086,
	5591,
	9086,
	6908,
	9086,
	6908,
	9086,
	6908,
	9086,
	12362,
	1915,
	3041,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	5521,
	6908,
	6908,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	1398,
	6908,
	1398,
	6908,
	6765,
	5521,
	6765,
	5521,
	6765,
	5521,
	6765,
	5521,
	6765,
	5521,
	6908,
	6908,
	5521,
	5521,
	3165,
	3165,
	1587,
	1587,
	1610,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	6908,
	4903,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	5521,
	4903,
	4903,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6765,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	5485,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	5485,
	6908,
	6908,
	6908,
	3165,
	3165,
	1587,
	1587,
	1610,
	6908,
	6908,
	6908,
	6908,
	5521,
	5521,
	6908,
	6908,
	6908,
	6908,
	5521,
	6908,
	5521,
	5521,
	5521,
	5521,
	5485,
	6908,
	6908,
	6908,
	6908,
	5485,
	6908,
	6908,
	6908,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	5521,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	5521,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	5521,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	6908,
	6908,
	5521,
	6765,
	6908,
	6908,
	2156,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	6908,
	6908,
	4903,
	6765,
	6908,
	5485,
	6908,
	6666,
	6765,
	6908,
	6765,
	11262,
	10115,
	11165,
	10105,
	10939,
	10082,
	11302,
	10965,
	10122,
	10083,
	11010,
	10088,
	11281,
	10118,
	11334,
	10127,
	10999,
	10084,
	11245,
	10110,
	11244,
	10109,
	11242,
	10107,
	11243,
	10108,
	11052,
	10091,
	11163,
	10103,
	11164,
	10104,
	10095,
	11092,
	10095,
	11092,
	12362,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x060000BC, { 0, 1 } },
	{ 0x060000BD, { 1, 3 } },
};
extern const uint32_t g_rgctx_T_t5A25CCDFE11FF7EA8E026ADA761346D6EA6FD2AA;
extern const uint32_t g_rgctx_JsonUtility_FromJson_TisT_tEB3D2D1F3A0D4DA1E9676DE966353DE3C34DA1F5_mBBF1E7355743EB6865929922C665DAA68DC0C7F2;
extern const uint32_t g_rgctx_T_tEB3D2D1F3A0D4DA1E9676DE966353DE3C34DA1F5;
extern const uint32_t g_rgctx_T_tEB3D2D1F3A0D4DA1E9676DE966353DE3C34DA1F5;
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t5A25CCDFE11FF7EA8E026ADA761346D6EA6FD2AA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonUtility_FromJson_TisT_tEB3D2D1F3A0D4DA1E9676DE966353DE3C34DA1F5_mBBF1E7355743EB6865929922C665DAA68DC0C7F2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tEB3D2D1F3A0D4DA1E9676DE966353DE3C34DA1F5 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tEB3D2D1F3A0D4DA1E9676DE966353DE3C34DA1F5 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1198,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

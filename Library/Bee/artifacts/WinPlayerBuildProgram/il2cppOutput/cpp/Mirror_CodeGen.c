﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m94AFFD8C6D8833094CF3E792FD0961D61B08B3BE (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsUnmanagedAttribute::.ctor()
extern void IsUnmanagedAttribute__ctor_m675B7DAF4C1DADC62DFCCD061BFA40123307934C (void);
// 0x00000003 System.Int32 kcp2k.KcpTransport::FromKcpChannel(kcp2k.KcpChannel)
extern void KcpTransport_FromKcpChannel_mE1CCEFD2A9E964A0C89560F6DB0172C75A6EC716 (void);
// 0x00000004 kcp2k.KcpChannel kcp2k.KcpTransport::ToKcpChannel(System.Int32)
extern void KcpTransport_ToKcpChannel_mE432B8EE0BD4D3B41362274122B3147227DE2922 (void);
// 0x00000005 System.Void kcp2k.KcpTransport::Awake()
extern void KcpTransport_Awake_mC4B5B95AB1E9B9D1EB9E5557B183742D567F34EA (void);
// 0x00000006 System.Void kcp2k.KcpTransport::OnValidate()
extern void KcpTransport_OnValidate_m5993AC5DC6ABB89891C7B4F4A4514C00F2BA865E (void);
// 0x00000007 System.Boolean kcp2k.KcpTransport::Available()
extern void KcpTransport_Available_mDA3BAC62C809A798A547268E68121B3ABA72DED6 (void);
// 0x00000008 System.Boolean kcp2k.KcpTransport::ClientConnected()
extern void KcpTransport_ClientConnected_mBAA27FD04F9936DC343A4042E6C5DEE69AA4D3D3 (void);
// 0x00000009 System.Void kcp2k.KcpTransport::ClientConnect(System.String)
extern void KcpTransport_ClientConnect_m4A3672DC09D87F271CE7B7A979E764CB02A06C0E (void);
// 0x0000000A System.Void kcp2k.KcpTransport::ClientConnect(System.Uri)
extern void KcpTransport_ClientConnect_m11819F34D781BA55F83B239305B3446CF0825D56 (void);
// 0x0000000B System.Void kcp2k.KcpTransport::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
extern void KcpTransport_ClientSend_m50CAEF627B64AAB4D113CF4DC2C0AADDA53CE6D1 (void);
// 0x0000000C System.Void kcp2k.KcpTransport::ClientDisconnect()
extern void KcpTransport_ClientDisconnect_m3D121AB3095CC6D39760FD49E9EBBC9002E9B59E (void);
// 0x0000000D System.Void kcp2k.KcpTransport::ClientEarlyUpdate()
extern void KcpTransport_ClientEarlyUpdate_mA9295A15B66875839B43C7A3BA7385D44F0A6199 (void);
// 0x0000000E System.Void kcp2k.KcpTransport::ClientLateUpdate()
extern void KcpTransport_ClientLateUpdate_m1409F5F97A36C7C18AE060A64F9C881AEE644DF1 (void);
// 0x0000000F System.Uri kcp2k.KcpTransport::ServerUri()
extern void KcpTransport_ServerUri_mFFCC8D110A35287BBA75768008686CCAE89ADBE8 (void);
// 0x00000010 System.Boolean kcp2k.KcpTransport::ServerActive()
extern void KcpTransport_ServerActive_m43207C3745E18B193E2571E41E42CDB5FE1A4AB4 (void);
// 0x00000011 System.Void kcp2k.KcpTransport::ServerStart()
extern void KcpTransport_ServerStart_m88DDC0773E40D1C73B26320BF5F8441A4FC875A1 (void);
// 0x00000012 System.Void kcp2k.KcpTransport::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void KcpTransport_ServerSend_m9172730670C1FF5FF4AD7648C0B94402899ABF8F (void);
// 0x00000013 System.Void kcp2k.KcpTransport::ServerDisconnect(System.Int32)
extern void KcpTransport_ServerDisconnect_mC5FDB541625FD58044BB1C90738A0CF2B13E5FC4 (void);
// 0x00000014 System.String kcp2k.KcpTransport::ServerGetClientAddress(System.Int32)
extern void KcpTransport_ServerGetClientAddress_m6BF2BCAEDA25D0883D3FF388BF9FFD0BE1E2DA12 (void);
// 0x00000015 System.Void kcp2k.KcpTransport::ServerStop()
extern void KcpTransport_ServerStop_mEB5DBFC594B11EE22DD8D46ED1218B8554F56514 (void);
// 0x00000016 System.Void kcp2k.KcpTransport::ServerEarlyUpdate()
extern void KcpTransport_ServerEarlyUpdate_mEB6EDEEFC58471EAD1EF599183EE2306B127F03E (void);
// 0x00000017 System.Void kcp2k.KcpTransport::ServerLateUpdate()
extern void KcpTransport_ServerLateUpdate_m144C8EE30B4BBB0CE6A7CB14B7307293BE35AD10 (void);
// 0x00000018 System.Void kcp2k.KcpTransport::Shutdown()
extern void KcpTransport_Shutdown_m26F8F3058772178601B6AD00F2DB83023730A327 (void);
// 0x00000019 System.Int32 kcp2k.KcpTransport::GetMaxPacketSize(System.Int32)
extern void KcpTransport_GetMaxPacketSize_mA70F8AD2CE6700EC34B8C363D3F2426C23B31769 (void);
// 0x0000001A System.Int32 kcp2k.KcpTransport::GetBatchThreshold(System.Int32)
extern void KcpTransport_GetBatchThreshold_m3AB86C5A22DC4CE2A82026E6FFEEC1A47FA4F69C (void);
// 0x0000001B System.Int64 kcp2k.KcpTransport::GetAverageMaxSendRate()
extern void KcpTransport_GetAverageMaxSendRate_mA295A0CCFFEF490F4E77E2CB2DC87D38F08168BD (void);
// 0x0000001C System.Int64 kcp2k.KcpTransport::GetAverageMaxReceiveRate()
extern void KcpTransport_GetAverageMaxReceiveRate_m8BFF62372AE77AE085E2F4D266C48230BBE886DE (void);
// 0x0000001D System.Int64 kcp2k.KcpTransport::GetTotalSendQueue()
extern void KcpTransport_GetTotalSendQueue_mE75D614D3C322855765441DFF18922F94C6245DA (void);
// 0x0000001E System.Int64 kcp2k.KcpTransport::GetTotalReceiveQueue()
extern void KcpTransport_GetTotalReceiveQueue_m4D8318B0BA1D346A16805124225070F0662C2322 (void);
// 0x0000001F System.Int64 kcp2k.KcpTransport::GetTotalSendBuffer()
extern void KcpTransport_GetTotalSendBuffer_m2DDE8C525CD7FC321E21929BF7F76520DB17002E (void);
// 0x00000020 System.Int64 kcp2k.KcpTransport::GetTotalReceiveBuffer()
extern void KcpTransport_GetTotalReceiveBuffer_m929E3D0CC26E6B54611C361AFBFAB97DCB405672 (void);
// 0x00000021 System.String kcp2k.KcpTransport::PrettyBytes(System.Int64)
extern void KcpTransport_PrettyBytes_m3222A9BD856CB6FD635F9CE4E558E38DCB20E8F0 (void);
// 0x00000022 System.Void kcp2k.KcpTransport::OnLogStatistics()
extern void KcpTransport_OnLogStatistics_mA13F25110EAAE2C422C00750D7BE5C3A1D5B0E78 (void);
// 0x00000023 System.String kcp2k.KcpTransport::ToString()
extern void KcpTransport_ToString_mF10DB3787CA152B727862297A2B4F506C3AA2ACD (void);
// 0x00000024 System.Void kcp2k.KcpTransport::.ctor()
extern void KcpTransport__ctor_m260525AD3CA58577B7BBA0A6B86A37316D38B31F (void);
// 0x00000025 System.Void kcp2k.KcpTransport::<Awake>b__22_1()
extern void KcpTransport_U3CAwakeU3Eb__22_1_mD585625EEEEFDA459E21C79E171EAD641219A73E (void);
// 0x00000026 System.Void kcp2k.KcpTransport::<Awake>b__22_2(System.ArraySegment`1<System.Byte>,kcp2k.KcpChannel)
extern void KcpTransport_U3CAwakeU3Eb__22_2_m8F37F800819FF5F4B6FECA1B9A245473C5C32F2B (void);
// 0x00000027 System.Void kcp2k.KcpTransport::<Awake>b__22_3()
extern void KcpTransport_U3CAwakeU3Eb__22_3_m9A3F481236C3E9DD6A06BAC1B1443E21A4044B8D (void);
// 0x00000028 System.Void kcp2k.KcpTransport::<Awake>b__22_4()
extern void KcpTransport_U3CAwakeU3Eb__22_4_mD594B1C327335DD24937F880886741D585E53D89 (void);
// 0x00000029 System.Void kcp2k.KcpTransport::<Awake>b__22_5(System.ArraySegment`1<System.Byte>,kcp2k.KcpChannel)
extern void KcpTransport_U3CAwakeU3Eb__22_5_m1E28C66BA0C7467B4CAB6F8D5E877DF6800995D7 (void);
// 0x0000002A System.Void kcp2k.KcpTransport::<Awake>b__22_6()
extern void KcpTransport_U3CAwakeU3Eb__22_6_m54A1094FEEFD3B9B1B19ABA976AAFD7577E8F16C (void);
// 0x0000002B System.Void kcp2k.KcpTransport::<Awake>b__22_7(System.Int32)
extern void KcpTransport_U3CAwakeU3Eb__22_7_mAE5A7AFB6F41D61019B01A537D72FE5CDF06DA59 (void);
// 0x0000002C System.Void kcp2k.KcpTransport::<Awake>b__22_8(System.Int32,System.ArraySegment`1<System.Byte>,kcp2k.KcpChannel)
extern void KcpTransport_U3CAwakeU3Eb__22_8_m54B4233D6D2ADC62534CAA719263563E1D8CD27B (void);
// 0x0000002D System.Void kcp2k.KcpTransport::<Awake>b__22_9(System.Int32)
extern void KcpTransport_U3CAwakeU3Eb__22_9_m6FD62175783CFB309030E6F9725D821C0983454E (void);
// 0x0000002E System.Void kcp2k.KcpTransport::<Awake>b__22_10(System.Int32)
extern void KcpTransport_U3CAwakeU3Eb__22_10_m8069602520BA0FEE777D94EC9BDEA0F710ADBE22 (void);
// 0x0000002F System.Void kcp2k.KcpTransport::<Awake>b__22_11(System.Int32,System.ArraySegment`1<System.Byte>,kcp2k.KcpChannel)
extern void KcpTransport_U3CAwakeU3Eb__22_11_m55D6B3F6AEC961923BAA4DDC249A46A017706E47 (void);
// 0x00000030 System.Void kcp2k.KcpTransport::<Awake>b__22_12(System.Int32)
extern void KcpTransport_U3CAwakeU3Eb__22_12_mFAEF2E49C32F3BB992B2051112F437416C61E3FD (void);
// 0x00000031 System.Void kcp2k.KcpTransport/<>c::.cctor()
extern void U3CU3Ec__cctor_m8D151DACD678CCE4BC5B001A5BCC16FE7939CC73 (void);
// 0x00000032 System.Void kcp2k.KcpTransport/<>c::.ctor()
extern void U3CU3Ec__ctor_m06E38D24631445FEA971806DD448CB15039C8295 (void);
// 0x00000033 System.Void kcp2k.KcpTransport/<>c::<Awake>b__22_0(System.String)
extern void U3CU3Ec_U3CAwakeU3Eb__22_0_m72C38CD0649A005181240E31C60F306C5427522F (void);
// 0x00000034 System.Int64 kcp2k.KcpTransport/<>c::<GetAverageMaxSendRate>b__44_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetAverageMaxSendRateU3Eb__44_0_m80F7D97E7C065E18BBABA10C388C11A810777E6B (void);
// 0x00000035 System.Int64 kcp2k.KcpTransport/<>c::<GetAverageMaxReceiveRate>b__45_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetAverageMaxReceiveRateU3Eb__45_0_mB02934C9AE268E5D93E48C23EDDA7E74D771168B (void);
// 0x00000036 System.Int32 kcp2k.KcpTransport/<>c::<GetTotalSendQueue>b__46_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetTotalSendQueueU3Eb__46_0_m16161ECB4AC750F816D29258431FD9DAD2E28942 (void);
// 0x00000037 System.Int32 kcp2k.KcpTransport/<>c::<GetTotalReceiveQueue>b__47_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetTotalReceiveQueueU3Eb__47_0_mB5BE6853C1A138D9B621A526353E6DAE22D7AA0B (void);
// 0x00000038 System.Int32 kcp2k.KcpTransport/<>c::<GetTotalSendBuffer>b__48_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetTotalSendBufferU3Eb__48_0_m632B118EA360946EA0D67972F5C4D6B6D3F95F8D (void);
// 0x00000039 System.Int32 kcp2k.KcpTransport/<>c::<GetTotalReceiveBuffer>b__49_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetTotalReceiveBufferU3Eb__49_0_m793D399CF36BC5221DC7DC73B859F3E8927ED419 (void);
// 0x0000003A System.Void Mirror.SyncVarAttribute::.ctor()
extern void SyncVarAttribute__ctor_m5460E3D2EBFF0BE62504E2CE8B80DA021DAAC367 (void);
// 0x0000003B System.Void Mirror.CommandAttribute::.ctor()
extern void CommandAttribute__ctor_mE831DDD68AD14E7103B6CC0566A6E623EEAB281D (void);
// 0x0000003C System.Void Mirror.ClientRpcAttribute::.ctor()
extern void ClientRpcAttribute__ctor_mE29EAD24EC877A67BDF0DD6FF83AE936005137AB (void);
// 0x0000003D System.Void Mirror.TargetRpcAttribute::.ctor()
extern void TargetRpcAttribute__ctor_m523EA49BB6BC4A1C2C83DAE1CE196F995F03DC29 (void);
// 0x0000003E System.Void Mirror.ServerAttribute::.ctor()
extern void ServerAttribute__ctor_mAE57EB8632202A70EF0F82B640F31076E266EC61 (void);
// 0x0000003F System.Void Mirror.ServerCallbackAttribute::.ctor()
extern void ServerCallbackAttribute__ctor_m9E363D7193F18FD1E1F4D2EA92D23C88FD80D80D (void);
// 0x00000040 System.Void Mirror.ClientAttribute::.ctor()
extern void ClientAttribute__ctor_m0715C84E926C9EEF43D5777AC41925C0F95CF684 (void);
// 0x00000041 System.Void Mirror.ClientCallbackAttribute::.ctor()
extern void ClientCallbackAttribute__ctor_m78743AE405A644C18FA74E531C91042224D26CBD (void);
// 0x00000042 System.Void Mirror.SceneAttribute::.ctor()
extern void SceneAttribute__ctor_mBDDD93EDE68594FD69DF69052B2CC2719704B89A (void);
// 0x00000043 System.Void Mirror.ShowInInspectorAttribute::.ctor()
extern void ShowInInspectorAttribute__ctor_mF58724049FE25823AC9D97426459CBD05532E7F7 (void);
// 0x00000044 System.Void Mirror.Batcher::.ctor(System.Int32)
extern void Batcher__ctor_mD8E8A83534FC660326FB00ED14FFF66629E4C15F (void);
// 0x00000045 System.Void Mirror.Batcher::AddMessage(System.ArraySegment`1<System.Byte>,System.Double)
extern void Batcher_AddMessage_m7A8F80DC9DBF64B02DAEDE2394B47385C7DF8BEE (void);
// 0x00000046 System.Void Mirror.Batcher::CopyAndReturn(Mirror.NetworkWriterPooled,Mirror.NetworkWriter)
extern void Batcher_CopyAndReturn_mB3EDA280C4935D70A7BA91C1E95D0CFEE2C0B3CD (void);
// 0x00000047 System.Boolean Mirror.Batcher::GetBatch(Mirror.NetworkWriter)
extern void Batcher_GetBatch_m94CF4CEF213738205C87FFC3EFB00F599E0DE7A3 (void);
// 0x00000048 System.Int32 Mirror.Unbatcher::get_BatchesCount()
extern void Unbatcher_get_BatchesCount_m986752974D3465F3C92C5A49B854B9314322C9B7 (void);
// 0x00000049 System.Void Mirror.Unbatcher::StartReadingBatch(Mirror.NetworkWriterPooled)
extern void Unbatcher_StartReadingBatch_m0DE0FE72F1B8CC6A5BD96E26425AA3DB37700F8D (void);
// 0x0000004A System.Boolean Mirror.Unbatcher::AddBatch(System.ArraySegment`1<System.Byte>)
extern void Unbatcher_AddBatch_mB625670EC3127D387396C42073052F23EDCB37CB (void);
// 0x0000004B System.Boolean Mirror.Unbatcher::GetNextMessage(Mirror.NetworkReader&,System.Double&)
extern void Unbatcher_GetNextMessage_mF551255B8E429C16F4586881905E28C80157B7F4 (void);
// 0x0000004C System.Void Mirror.Unbatcher::.ctor()
extern void Unbatcher__ctor_m332FE2E909BC6429FA7260BBD51D60CB90364E20 (void);
// 0x0000004D System.Int32 Mirror.Compression::LargestAbsoluteComponentIndex(UnityEngine.Vector4,System.Single&,UnityEngine.Vector3&)
extern void Compression_LargestAbsoluteComponentIndex_mF93CA9F88C5C144E50D5BC9AFE5BB3870C7D81F0 (void);
// 0x0000004E System.UInt16 Mirror.Compression::ScaleFloatToUShort(System.Single,System.Single,System.Single,System.UInt16,System.UInt16)
extern void Compression_ScaleFloatToUShort_m3D013F6AFA08E1E5967E1FFFC5835F9F5C0020FA (void);
// 0x0000004F System.Single Mirror.Compression::ScaleUShortToFloat(System.UInt16,System.UInt16,System.UInt16,System.Single,System.Single)
extern void Compression_ScaleUShortToFloat_m0D1581C586B11177DB5BA934B5E86F505308E61A (void);
// 0x00000050 System.Single Mirror.Compression::QuaternionElement(UnityEngine.Quaternion,System.Int32)
extern void Compression_QuaternionElement_m34FB4EBB300E5464CD5BF88B4EEDB51E0D0351A2 (void);
// 0x00000051 System.UInt32 Mirror.Compression::CompressQuaternion(UnityEngine.Quaternion)
extern void Compression_CompressQuaternion_m17FF0335074A75F1FBB4A013CB320A6DF4A4EB4B (void);
// 0x00000052 UnityEngine.Quaternion Mirror.Compression::QuaternionNormalizeSafe(UnityEngine.Quaternion)
extern void Compression_QuaternionNormalizeSafe_m55ED0BD8698C6A081E433A9E00075CB5FFC225E6 (void);
// 0x00000053 UnityEngine.Quaternion Mirror.Compression::DecompressQuaternion(System.UInt32)
extern void Compression_DecompressQuaternion_m97E0C9DC786CB565358A8F4476BAE340667A2385 (void);
// 0x00000054 System.Void Mirror.Compression::CompressVarUInt(Mirror.NetworkWriter,System.UInt64)
extern void Compression_CompressVarUInt_mBE4774C4312609FE845F178152E3D189F0836A9F (void);
// 0x00000055 System.Void Mirror.Compression::CompressVarInt(Mirror.NetworkWriter,System.Int64)
extern void Compression_CompressVarInt_m8C2BC3E814B3C8E983CAE1FBC46010661E9D552F (void);
// 0x00000056 System.UInt64 Mirror.Compression::DecompressVarUInt(Mirror.NetworkReader)
extern void Compression_DecompressVarUInt_m6FFA961BEE08C48253EE0519959843A1D9587C0A (void);
// 0x00000057 System.Int64 Mirror.Compression::DecompressVarInt(Mirror.NetworkReader)
extern void Compression_DecompressVarInt_m2C488DA0B3176C21314EA3D70504D608D2FA6DFD (void);
// 0x00000058 System.Void Mirror.ExponentialMovingAverage::.ctor(System.Int32)
extern void ExponentialMovingAverage__ctor_m1CD28C151EEBED3094D29E53321BD3A98E6CE053 (void);
// 0x00000059 System.Void Mirror.ExponentialMovingAverage::Add(System.Double)
extern void ExponentialMovingAverage_Add_m0817D656C4AC628729650F2DB1D1B0B5AF90E828 (void);
// 0x0000005A System.Int32 Mirror.Extensions::GetStableHashCode(System.String)
extern void Extensions_GetStableHashCode_mDE19358D2CA9C78D4686D92B6602E2B5B936556C (void);
// 0x0000005B System.String Mirror.Extensions::GetMethodName(System.Delegate)
extern void Extensions_GetMethodName_mD053FC30E67EDED20AE40682988FA349619AD670 (void);
// 0x0000005C System.Void Mirror.Extensions::CopyTo(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.List`1<T>)
// 0x0000005D System.Boolean Mirror.Extensions::TryDequeue(System.Collections.Generic.Queue`1<T>,T&)
// 0x0000005E System.Void Mirror.InterestManagement::Awake()
extern void InterestManagement_Awake_m91C60669B89E7D5E72820D3EBC7DD96E0EA86A81 (void);
// 0x0000005F System.Void Mirror.InterestManagement::Reset()
extern void InterestManagement_Reset_mB675D8CABBE15E1896C60920B1DCC7714A07FD2B (void);
// 0x00000060 System.Boolean Mirror.InterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
// 0x00000061 System.Void Mirror.InterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
// 0x00000062 System.Void Mirror.InterestManagement::RebuildAll()
extern void InterestManagement_RebuildAll_mBEB1072D6C2780155E9CD552768EF75EA5E6619C (void);
// 0x00000063 System.Void Mirror.InterestManagement::SetHostVisibility(Mirror.NetworkIdentity,System.Boolean)
extern void InterestManagement_SetHostVisibility_m6F968317C97072717125B0B25764B70AC21DFD40 (void);
// 0x00000064 System.Void Mirror.InterestManagement::OnSpawned(Mirror.NetworkIdentity)
extern void InterestManagement_OnSpawned_m0DF5418E719C14D340B48F4FC9B59F72A22FD622 (void);
// 0x00000065 System.Void Mirror.InterestManagement::OnDestroyed(Mirror.NetworkIdentity)
extern void InterestManagement_OnDestroyed_m1AEB8C56EC644EEC2267A193DA3F36E68FEDCBB9 (void);
// 0x00000066 System.Void Mirror.InterestManagement::.ctor()
extern void InterestManagement__ctor_mA085F758D3CB060F5066F098F0CFFDE02B91A830 (void);
// 0x00000067 System.Void Mirror.LocalConnectionToClient::.ctor()
extern void LocalConnectionToClient__ctor_mD682323192A4EAA8B206EC7867E64225404374A6 (void);
// 0x00000068 System.String Mirror.LocalConnectionToClient::get_address()
extern void LocalConnectionToClient_get_address_m928E5645D6837B8D8AE39376E806F1D0622C033A (void);
// 0x00000069 System.Void Mirror.LocalConnectionToClient::Send(System.ArraySegment`1<System.Byte>,System.Int32)
extern void LocalConnectionToClient_Send_mD23691121DF107DA2A3565F2A32F963D1349F612 (void);
// 0x0000006A System.Boolean Mirror.LocalConnectionToClient::IsAlive(System.Single)
extern void LocalConnectionToClient_IsAlive_mE163AADEDCCDE354600472EF978A16E8858BAF70 (void);
// 0x0000006B System.Void Mirror.LocalConnectionToClient::DisconnectInternal()
extern void LocalConnectionToClient_DisconnectInternal_mF193A3FAB258E180AB1AC139C632745C22147006 (void);
// 0x0000006C System.Void Mirror.LocalConnectionToClient::Disconnect()
extern void LocalConnectionToClient_Disconnect_mE9F4063A5E2B473E1AF5D677B4ED910BA8330495 (void);
// 0x0000006D System.String Mirror.LocalConnectionToServer::get_address()
extern void LocalConnectionToServer_get_address_m7EE79DD4773F7D66AB2874733A70B353D392742B (void);
// 0x0000006E System.Void Mirror.LocalConnectionToServer::QueueConnectedEvent()
extern void LocalConnectionToServer_QueueConnectedEvent_m80C26FFC38800B089CD32BFAF1BA9122D572CEFD (void);
// 0x0000006F System.Void Mirror.LocalConnectionToServer::QueueDisconnectedEvent()
extern void LocalConnectionToServer_QueueDisconnectedEvent_m67EA5F1FA05D1A7D5101D44280D75CB27CC5945B (void);
// 0x00000070 System.Void Mirror.LocalConnectionToServer::Send(System.ArraySegment`1<System.Byte>,System.Int32)
extern void LocalConnectionToServer_Send_mC02C10C84FE7F2826F067E426F73FD45800E57C3 (void);
// 0x00000071 System.Void Mirror.LocalConnectionToServer::Update()
extern void LocalConnectionToServer_Update_m953199C45763C2D110C0CB1720F08C9190AC5F3E (void);
// 0x00000072 System.Void Mirror.LocalConnectionToServer::DisconnectInternal()
extern void LocalConnectionToServer_DisconnectInternal_m2444BD1D95B854D1F09D0D6FF3E527DC6010415D (void);
// 0x00000073 System.Void Mirror.LocalConnectionToServer::Disconnect()
extern void LocalConnectionToServer_Disconnect_mDBD38307FF977677532B6C27AEBD1770556CEC19 (void);
// 0x00000074 System.Boolean Mirror.LocalConnectionToServer::IsAlive(System.Single)
extern void LocalConnectionToServer_IsAlive_mA20298DE00228363AB3FE09B24A83A803848D618 (void);
// 0x00000075 System.Void Mirror.LocalConnectionToServer::.ctor()
extern void LocalConnectionToServer__ctor_m51AC250892A8ADC06A6DF45B961DAD1D8930E00E (void);
// 0x00000076 System.Double Mirror.Mathd::LerpUnclamped(System.Double,System.Double,System.Double)
extern void Mathd_LerpUnclamped_m2B1C40B47CF48A868E627DACCEACDE0C9111DBD7 (void);
// 0x00000077 System.Double Mirror.Mathd::Clamp01(System.Double)
extern void Mathd_Clamp01_m7FA273B332D04AAB267D225B0C4D080E3E7E31F6 (void);
// 0x00000078 System.Double Mirror.Mathd::InverseLerp(System.Double,System.Double,System.Double)
extern void Mathd_InverseLerp_m010B2A1EBFAF5D0B560066C0E65E7243932D79E3 (void);
// 0x00000079 System.Int32 Mirror.MessagePacking::get_MaxContentSize()
extern void MessagePacking_get_MaxContentSize_m08A3CAEC36C4FC63995F0C631A43F58E4D3761DC (void);
// 0x0000007A System.UInt16 Mirror.MessagePacking::GetId()
// 0x0000007B System.Void Mirror.MessagePacking::Pack(T,Mirror.NetworkWriter)
// 0x0000007C System.Boolean Mirror.MessagePacking::Unpack(Mirror.NetworkReader,System.UInt16&)
extern void MessagePacking_Unpack_mFFDC4731C89B68D3A80FC425A290457C210899C7 (void);
// 0x0000007D Mirror.NetworkMessageDelegate Mirror.MessagePacking::WrapHandler(System.Action`3<C,T,System.Int32>,System.Boolean)
// 0x0000007E Mirror.NetworkMessageDelegate Mirror.MessagePacking::WrapHandler(System.Action`2<C,T>,System.Boolean)
// 0x0000007F System.Void Mirror.MessagePacking/<>c__DisplayClass6_0`2::.ctor()
// 0x00000080 System.Void Mirror.MessagePacking/<>c__DisplayClass6_0`2::<WrapHandler>b__0(Mirror.NetworkConnection,Mirror.NetworkReader,System.Int32)
// 0x00000081 System.Void Mirror.MessagePacking/<>c__DisplayClass7_0`2::.ctor()
// 0x00000082 System.Void Mirror.MessagePacking/<>c__DisplayClass7_0`2::<WrapHandler>g__Wrapped|0(C,T,System.Int32)
// 0x00000083 System.Void Mirror.NetworkPingMessage::.ctor(System.Double)
extern void NetworkPingMessage__ctor_m54E9D94B3A2C74849855189A2580FCBDD1C86D94 (void);
// 0x00000084 System.Void Mirror.UnityEventNetworkConnection::.ctor()
extern void UnityEventNetworkConnection__ctor_mE5A0CD8B31569300790A92DD74132151435BB336 (void);
// 0x00000085 System.Void Mirror.NetworkAuthenticator::OnStartServer()
extern void NetworkAuthenticator_OnStartServer_mE4093F0CABA168B50ABA5CAC8C5458B02C8BA67C (void);
// 0x00000086 System.Void Mirror.NetworkAuthenticator::OnStopServer()
extern void NetworkAuthenticator_OnStopServer_m30FE34BAAACA29F6EF2D6D85B3D9D78D4B5924A7 (void);
// 0x00000087 System.Void Mirror.NetworkAuthenticator::OnServerAuthenticate(Mirror.NetworkConnectionToClient)
extern void NetworkAuthenticator_OnServerAuthenticate_m5BD7A2C0666CF51DC6A1552FC246AC5C934087BE (void);
// 0x00000088 System.Void Mirror.NetworkAuthenticator::ServerAccept(Mirror.NetworkConnectionToClient)
extern void NetworkAuthenticator_ServerAccept_mACF91D447AFB44C815ECBC7C79295C23AB6D22A5 (void);
// 0x00000089 System.Void Mirror.NetworkAuthenticator::ServerReject(Mirror.NetworkConnectionToClient)
extern void NetworkAuthenticator_ServerReject_m7A761D9E757E5426A8B9EAE9699AB6EA473AB1BE (void);
// 0x0000008A System.Void Mirror.NetworkAuthenticator::OnStartClient()
extern void NetworkAuthenticator_OnStartClient_m0CD8BF9259DED666B6C93AD5F1C925B4ABA7390A (void);
// 0x0000008B System.Void Mirror.NetworkAuthenticator::OnStopClient()
extern void NetworkAuthenticator_OnStopClient_mE32A1242E920E1F70FC26D8AFA6C5EE1EEB4763C (void);
// 0x0000008C System.Void Mirror.NetworkAuthenticator::OnClientAuthenticate()
extern void NetworkAuthenticator_OnClientAuthenticate_m698B490B070DB24853E82ABAA3014DD7FA333FED (void);
// 0x0000008D System.Void Mirror.NetworkAuthenticator::ClientAccept()
extern void NetworkAuthenticator_ClientAccept_m35557AF9301C3105E4E1DDA474A8972141179DF4 (void);
// 0x0000008E System.Void Mirror.NetworkAuthenticator::ClientReject()
extern void NetworkAuthenticator_ClientReject_m97E6B85AC941084DE35435FD35DE280804D64C66 (void);
// 0x0000008F System.Void Mirror.NetworkAuthenticator::Reset()
extern void NetworkAuthenticator_Reset_m937A72247EEFE2F232123658A2C1D384C065F8B7 (void);
// 0x00000090 System.Void Mirror.NetworkAuthenticator::.ctor()
extern void NetworkAuthenticator__ctor_mB7C72F9607CD3EE26A45F1702FA61FC1E226B855 (void);
// 0x00000091 System.Boolean Mirror.NetworkBehaviour::get_isServer()
extern void NetworkBehaviour_get_isServer_m1E9842F452B42CDFCCF64673D52142E9B1270B9C (void);
// 0x00000092 System.Boolean Mirror.NetworkBehaviour::get_isClient()
extern void NetworkBehaviour_get_isClient_mA90574E13E8163389D82C8DF66C0AC070E091A68 (void);
// 0x00000093 System.Boolean Mirror.NetworkBehaviour::get_isLocalPlayer()
extern void NetworkBehaviour_get_isLocalPlayer_m957C0F05035CEAA69E6B06E8E94C7143AB51305F (void);
// 0x00000094 System.Boolean Mirror.NetworkBehaviour::get_isServerOnly()
extern void NetworkBehaviour_get_isServerOnly_m6F68A5CCB47E8527E22CC4385A17D6B3D90F742D (void);
// 0x00000095 System.Boolean Mirror.NetworkBehaviour::get_isClientOnly()
extern void NetworkBehaviour_get_isClientOnly_m4E9CB13F7BD31E5312168A155CC169BBD9AE6F4F (void);
// 0x00000096 System.Boolean Mirror.NetworkBehaviour::get_hasAuthority()
extern void NetworkBehaviour_get_hasAuthority_m9974B1143E13AE7F01DDEBE4D151AD383CCD17F0 (void);
// 0x00000097 System.UInt32 Mirror.NetworkBehaviour::get_netId()
extern void NetworkBehaviour_get_netId_mB3920F9E26854072F59CA87B5E428C54AE49694E (void);
// 0x00000098 Mirror.NetworkConnection Mirror.NetworkBehaviour::get_connectionToServer()
extern void NetworkBehaviour_get_connectionToServer_mF8A6B1E6340A8902BFC3A352B0422CBFD9D772C0 (void);
// 0x00000099 Mirror.NetworkConnectionToClient Mirror.NetworkBehaviour::get_connectionToClient()
extern void NetworkBehaviour_get_connectionToClient_mB8F2C6152FACA59FD1E16517EC227E631159D5A7 (void);
// 0x0000009A System.Boolean Mirror.NetworkBehaviour::HasSyncObjects()
extern void NetworkBehaviour_HasSyncObjects_m0AAB904D0F7BCEF9CC643D7475F63B24CCA0E3BC (void);
// 0x0000009B Mirror.NetworkIdentity Mirror.NetworkBehaviour::get_netIdentity()
extern void NetworkBehaviour_get_netIdentity_mDAF7D6C8D3DE522EEB2BBA08486446A011BE875D (void);
// 0x0000009C System.Void Mirror.NetworkBehaviour::set_netIdentity(Mirror.NetworkIdentity)
extern void NetworkBehaviour_set_netIdentity_m5DA4B47B0A8A8B3FFE8F5A768E06151CA61EE170 (void);
// 0x0000009D System.Int32 Mirror.NetworkBehaviour::get_ComponentIndex()
extern void NetworkBehaviour_get_ComponentIndex_mB490AE2622BC8169CFC0B49818D6F06C73EDA76B (void);
// 0x0000009E System.Void Mirror.NetworkBehaviour::set_ComponentIndex(System.Int32)
extern void NetworkBehaviour_set_ComponentIndex_mCD0B3BDD40102F8DFBF2B44A866C570258E3D72C (void);
// 0x0000009F System.UInt64 Mirror.NetworkBehaviour::get_syncVarDirtyBits()
extern void NetworkBehaviour_get_syncVarDirtyBits_m96E01E08568BF6521B3B69F8F8F36884CAABEEB4 (void);
// 0x000000A0 System.Void Mirror.NetworkBehaviour::set_syncVarDirtyBits(System.UInt64)
extern void NetworkBehaviour_set_syncVarDirtyBits_m521AED68C76A47BA3C2758D50A28869C340DA6DF (void);
// 0x000000A1 System.Boolean Mirror.NetworkBehaviour::GetSyncVarHookGuard(System.UInt64)
extern void NetworkBehaviour_GetSyncVarHookGuard_mF9DBFE6D19AC4402C44FDD0BEF11C1C99B694ED8 (void);
// 0x000000A2 System.Boolean Mirror.NetworkBehaviour::getSyncVarHookGuard(System.UInt64)
extern void NetworkBehaviour_getSyncVarHookGuard_m9324239619976E41D7BA80ED159EA975DA157B95 (void);
// 0x000000A3 System.Void Mirror.NetworkBehaviour::SetSyncVarHookGuard(System.UInt64,System.Boolean)
extern void NetworkBehaviour_SetSyncVarHookGuard_m8098794BA748ACFFE6B68C69B9D5609E43F71334 (void);
// 0x000000A4 System.Void Mirror.NetworkBehaviour::setSyncVarHookGuard(System.UInt64,System.Boolean)
extern void NetworkBehaviour_setSyncVarHookGuard_m703119E70E8190716761F9318611A0A3CA999454 (void);
// 0x000000A5 System.Void Mirror.NetworkBehaviour::SetSyncVarDirtyBit(System.UInt64)
extern void NetworkBehaviour_SetSyncVarDirtyBit_mDCA45CAE44ADE6477E858536153376996FE61E40 (void);
// 0x000000A6 System.Void Mirror.NetworkBehaviour::SetDirtyBit(System.UInt64)
extern void NetworkBehaviour_SetDirtyBit_m18202DFE2962FA3082E2BE3A02D1FDBEA00295CC (void);
// 0x000000A7 System.Boolean Mirror.NetworkBehaviour::IsDirty()
extern void NetworkBehaviour_IsDirty_mF99D908ECDA3514F74B9C807729EE470A4573E5B (void);
// 0x000000A8 System.Void Mirror.NetworkBehaviour::ClearAllDirtyBits()
extern void NetworkBehaviour_ClearAllDirtyBits_m5E3672905E1E1BD43993C840A2866BB31067F66C (void);
// 0x000000A9 System.Void Mirror.NetworkBehaviour::InitSyncObject(Mirror.SyncObject)
extern void NetworkBehaviour_InitSyncObject_mD7B4B335A69AB45D931DB842B8E47C722EE3621C (void);
// 0x000000AA System.Void Mirror.NetworkBehaviour::SendCommandInternal(System.String,Mirror.NetworkWriter,System.Int32,System.Boolean)
extern void NetworkBehaviour_SendCommandInternal_m7F39ACA5AF256C2595C6D81CF19AF0333C8BDDC7 (void);
// 0x000000AB System.Void Mirror.NetworkBehaviour::SendRPCInternal(System.String,Mirror.NetworkWriter,System.Int32,System.Boolean)
extern void NetworkBehaviour_SendRPCInternal_m570E5B73F726DF4EE80A97483215E244D6C1C9DD (void);
// 0x000000AC System.Void Mirror.NetworkBehaviour::SendTargetRPCInternal(Mirror.NetworkConnection,System.String,Mirror.NetworkWriter,System.Int32)
extern void NetworkBehaviour_SendTargetRPCInternal_m8E4B839063E4B5CCB1F2EAC9DA83EED74F3BD91E (void);
// 0x000000AD System.Void Mirror.NetworkBehaviour::GeneratedSyncVarSetter(T,T&,System.UInt64,System.Action`2<T,T>)
// 0x000000AE System.Void Mirror.NetworkBehaviour::GeneratedSyncVarSetter_GameObject(UnityEngine.GameObject,UnityEngine.GameObject&,System.UInt64,System.Action`2<UnityEngine.GameObject,UnityEngine.GameObject>,System.UInt32&)
extern void NetworkBehaviour_GeneratedSyncVarSetter_GameObject_mCF8A10F08EAF57737B52D161DD83312B6BF47105 (void);
// 0x000000AF System.Void Mirror.NetworkBehaviour::GeneratedSyncVarSetter_NetworkIdentity(Mirror.NetworkIdentity,Mirror.NetworkIdentity&,System.UInt64,System.Action`2<Mirror.NetworkIdentity,Mirror.NetworkIdentity>,System.UInt32&)
extern void NetworkBehaviour_GeneratedSyncVarSetter_NetworkIdentity_mACE2BBEECB92E6DFDB873ECC775A6C64BEB79929 (void);
// 0x000000B0 System.Void Mirror.NetworkBehaviour::GeneratedSyncVarSetter_NetworkBehaviour(T,T&,System.UInt64,System.Action`2<T,T>,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar&)
// 0x000000B1 System.Boolean Mirror.NetworkBehaviour::SyncVarGameObjectEqual(UnityEngine.GameObject,System.UInt32)
extern void NetworkBehaviour_SyncVarGameObjectEqual_m6989106B1F820A05D898018C355C2B37EA50AAE2 (void);
// 0x000000B2 System.Void Mirror.NetworkBehaviour::SetSyncVarGameObject(UnityEngine.GameObject,UnityEngine.GameObject&,System.UInt64,System.UInt32&)
extern void NetworkBehaviour_SetSyncVarGameObject_mF35ADD89B1A28955C7956DBEF8B1A33193963E9E (void);
// 0x000000B3 UnityEngine.GameObject Mirror.NetworkBehaviour::GetSyncVarGameObject(System.UInt32,UnityEngine.GameObject&)
extern void NetworkBehaviour_GetSyncVarGameObject_m605D12A70F5E6EF9919CDB4D550FFFBE4C466FE3 (void);
// 0x000000B4 System.Boolean Mirror.NetworkBehaviour::SyncVarNetworkIdentityEqual(Mirror.NetworkIdentity,System.UInt32)
extern void NetworkBehaviour_SyncVarNetworkIdentityEqual_mE6D21F6C6812F37FC6B45E8A5909300C646A2782 (void);
// 0x000000B5 System.Void Mirror.NetworkBehaviour::GeneratedSyncVarDeserialize(T&,System.Action`2<T,T>,T)
// 0x000000B6 System.Void Mirror.NetworkBehaviour::GeneratedSyncVarDeserialize_GameObject(UnityEngine.GameObject&,System.Action`2<UnityEngine.GameObject,UnityEngine.GameObject>,Mirror.NetworkReader,System.UInt32&)
extern void NetworkBehaviour_GeneratedSyncVarDeserialize_GameObject_m0814F9B2296DB7492B5F2C9317B406A2E69AED64 (void);
// 0x000000B7 System.Void Mirror.NetworkBehaviour::GeneratedSyncVarDeserialize_NetworkIdentity(Mirror.NetworkIdentity&,System.Action`2<Mirror.NetworkIdentity,Mirror.NetworkIdentity>,Mirror.NetworkReader,System.UInt32&)
extern void NetworkBehaviour_GeneratedSyncVarDeserialize_NetworkIdentity_mD5FB29C1EDBB9E8095A30BFFE1C49BF6BF5D7501 (void);
// 0x000000B8 System.Void Mirror.NetworkBehaviour::GeneratedSyncVarDeserialize_NetworkBehaviour(T&,System.Action`2<T,T>,Mirror.NetworkReader,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar&)
// 0x000000B9 System.Void Mirror.NetworkBehaviour::SetSyncVarNetworkIdentity(Mirror.NetworkIdentity,Mirror.NetworkIdentity&,System.UInt64,System.UInt32&)
extern void NetworkBehaviour_SetSyncVarNetworkIdentity_mE032B81D8FB0D7C3A779F7A9BF5BB610F1851E75 (void);
// 0x000000BA Mirror.NetworkIdentity Mirror.NetworkBehaviour::GetSyncVarNetworkIdentity(System.UInt32,Mirror.NetworkIdentity&)
extern void NetworkBehaviour_GetSyncVarNetworkIdentity_mDACFF6689ACAF87156C56F1367C7449B2CE3467E (void);
// 0x000000BB System.Boolean Mirror.NetworkBehaviour::SyncVarNetworkBehaviourEqual(T,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar)
// 0x000000BC System.Void Mirror.NetworkBehaviour::SetSyncVarNetworkBehaviour(T,T&,System.UInt64,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar&)
// 0x000000BD T Mirror.NetworkBehaviour::GetSyncVarNetworkBehaviour(Mirror.NetworkBehaviour/NetworkBehaviourSyncVar,T&)
// 0x000000BE System.Boolean Mirror.NetworkBehaviour::SyncVarEqual(T,T&)
// 0x000000BF System.Void Mirror.NetworkBehaviour::SetSyncVar(T,T&,System.UInt64)
// 0x000000C0 System.Boolean Mirror.NetworkBehaviour::OnSerialize(Mirror.NetworkWriter,System.Boolean)
extern void NetworkBehaviour_OnSerialize_m360FE2CC92D9C705A9E0598FDF4C8527E7778FCD (void);
// 0x000000C1 System.Void Mirror.NetworkBehaviour::OnDeserialize(Mirror.NetworkReader,System.Boolean)
extern void NetworkBehaviour_OnDeserialize_mEB77EDB6C3C748AAE3AB4225FB48D47146D11AEF (void);
// 0x000000C2 System.Boolean Mirror.NetworkBehaviour::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkBehaviour_SerializeSyncVars_mF60B2EAAE4B8BBE7044C528F6B3D34CBEB09EA65 (void);
// 0x000000C3 System.Void Mirror.NetworkBehaviour::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkBehaviour_DeserializeSyncVars_m4EECC3AAB3CAAD098D0B6056C590BBD98AEAF68C (void);
// 0x000000C4 System.Boolean Mirror.NetworkBehaviour::SerializeObjectsAll(Mirror.NetworkWriter)
extern void NetworkBehaviour_SerializeObjectsAll_m81592519E358E82E1CD8358A873C0AF075D16B5D (void);
// 0x000000C5 System.Boolean Mirror.NetworkBehaviour::SerializeObjectsDelta(Mirror.NetworkWriter)
extern void NetworkBehaviour_SerializeObjectsDelta_mC9A5A1082FC83204986F5B87A0806B45CA3BBBB7 (void);
// 0x000000C6 System.Void Mirror.NetworkBehaviour::DeSerializeObjectsAll(Mirror.NetworkReader)
extern void NetworkBehaviour_DeSerializeObjectsAll_m0958114631F640903075897341A5C3C55A01D62A (void);
// 0x000000C7 System.Void Mirror.NetworkBehaviour::DeSerializeObjectsDelta(Mirror.NetworkReader)
extern void NetworkBehaviour_DeSerializeObjectsDelta_m1A05FFFBC8FA68C2302BE9FDA87450446A87CB2D (void);
// 0x000000C8 System.Void Mirror.NetworkBehaviour::ResetSyncObjects()
extern void NetworkBehaviour_ResetSyncObjects_m8F15F614664E0F74352049BC18BC91EE5304AA24 (void);
// 0x000000C9 System.Void Mirror.NetworkBehaviour::OnStartServer()
extern void NetworkBehaviour_OnStartServer_m3419FB42E919414317B3F207B92C54C416787074 (void);
// 0x000000CA System.Void Mirror.NetworkBehaviour::OnStopServer()
extern void NetworkBehaviour_OnStopServer_m7E9BDD1C4E29A1FCB0C7D1C902F04E18394D1FD4 (void);
// 0x000000CB System.Void Mirror.NetworkBehaviour::OnStartClient()
extern void NetworkBehaviour_OnStartClient_m0621500FCDF7735D0F305FAA4AF2D7A6EE108925 (void);
// 0x000000CC System.Void Mirror.NetworkBehaviour::OnStopClient()
extern void NetworkBehaviour_OnStopClient_m1C27F8D943A29DA3DD88A21264F72CCDE2352AB5 (void);
// 0x000000CD System.Void Mirror.NetworkBehaviour::OnStartLocalPlayer()
extern void NetworkBehaviour_OnStartLocalPlayer_mA165E1998DCDA1A953210549EE45E077C9A9728B (void);
// 0x000000CE System.Void Mirror.NetworkBehaviour::OnStopLocalPlayer()
extern void NetworkBehaviour_OnStopLocalPlayer_m876B5D778E9377FAF0DB9D3D24F148A612F15041 (void);
// 0x000000CF System.Void Mirror.NetworkBehaviour::OnStartAuthority()
extern void NetworkBehaviour_OnStartAuthority_mB5BADB2A156DEFCC66B2EA093C80CCABB8DB2753 (void);
// 0x000000D0 System.Void Mirror.NetworkBehaviour::OnStopAuthority()
extern void NetworkBehaviour_OnStopAuthority_m77D01714689FE908D5E840E4474C6F0D93B700F8 (void);
// 0x000000D1 System.Void Mirror.NetworkBehaviour::.ctor()
extern void NetworkBehaviour__ctor_m63A1588EA5424B45068E2E6F74CDF520BF889765 (void);
// 0x000000D2 System.Void Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::.ctor(System.UInt32,System.Int32)
extern void NetworkBehaviourSyncVar__ctor_mFD3CF614B6356621B5CC741500D4FAD1E77A447B (void);
// 0x000000D3 System.Boolean Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::Equals(Mirror.NetworkBehaviour/NetworkBehaviourSyncVar)
extern void NetworkBehaviourSyncVar_Equals_m94E677024C09599396FF1AEB14DA042D2FB8B485 (void);
// 0x000000D4 System.Boolean Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::Equals(System.UInt32,System.Int32)
extern void NetworkBehaviourSyncVar_Equals_mB5F55C22E07441A6816A3D82647F0E3097CEBC06 (void);
// 0x000000D5 System.String Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::ToString()
extern void NetworkBehaviourSyncVar_ToString_mBCD8FA34F95F8B2E52F282476D543394C3006FDD (void);
// 0x000000D6 System.Void Mirror.NetworkBehaviour/<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_m76FB1D32E6319FAC696081BC46A87BC4475CE116 (void);
// 0x000000D7 System.Void Mirror.NetworkBehaviour/<>c__DisplayClass45_0::<InitSyncObject>b__0()
extern void U3CU3Ec__DisplayClass45_0_U3CInitSyncObjectU3Eb__0_mF868C5D54DA19C10366FF54C752618E6F4F7DC36 (void);
// 0x000000D8 System.Boolean Mirror.NetworkBehaviour/<>c__DisplayClass45_0::<InitSyncObject>b__1()
extern void U3CU3Ec__DisplayClass45_0_U3CInitSyncObjectU3Eb__1_mA65D00456E668E2C3500B3FBCA5E9698E53C2E41 (void);
// 0x000000D9 Mirror.NetworkConnection Mirror.NetworkClient::get_connection()
extern void NetworkClient_get_connection_m2B3FE54C58837E91419E19A6CC8BE1E94CDECA8E (void);
// 0x000000DA System.Void Mirror.NetworkClient::set_connection(Mirror.NetworkConnection)
extern void NetworkClient_set_connection_m2FDFE8A03D75526959C9C6925F2D9D61C36D96F1 (void);
// 0x000000DB Mirror.NetworkIdentity Mirror.NetworkClient::get_localPlayer()
extern void NetworkClient_get_localPlayer_m2EF038BD98B1533642CDDD88FE3A2E857ECC7E42 (void);
// 0x000000DC System.Void Mirror.NetworkClient::set_localPlayer(Mirror.NetworkIdentity)
extern void NetworkClient_set_localPlayer_mCF07ABA4A5E90FBA8EEE4B941D96F1E000962557 (void);
// 0x000000DD System.String Mirror.NetworkClient::get_serverIp()
extern void NetworkClient_get_serverIp_m7ECA1AC413B2F98A8E32B3DA46405933029159A1 (void);
// 0x000000DE System.Boolean Mirror.NetworkClient::get_active()
extern void NetworkClient_get_active_m505597A136FC73CF3075CFA8E6B8A75DC686E1DC (void);
// 0x000000DF System.Boolean Mirror.NetworkClient::get_isConnecting()
extern void NetworkClient_get_isConnecting_m31B917869BCEA13334BC92D06B610362A8F60B80 (void);
// 0x000000E0 System.Boolean Mirror.NetworkClient::get_isConnected()
extern void NetworkClient_get_isConnected_m44CC2B9852A93C107983648935F9FD570630E6FF (void);
// 0x000000E1 System.Boolean Mirror.NetworkClient::get_isHostClient()
extern void NetworkClient_get_isHostClient_mC27E5DF67807AEC44EF107117794F5A68530C0E4 (void);
// 0x000000E2 System.Void Mirror.NetworkClient::AddTransportHandlers()
extern void NetworkClient_AddTransportHandlers_m73563AC8BB32F2D6635099855330637FB854E712 (void);
// 0x000000E3 System.Void Mirror.NetworkClient::RemoveTransportHandlers()
extern void NetworkClient_RemoveTransportHandlers_mD98C3A9A35E41319BC78616293409C3CD6CEC87C (void);
// 0x000000E4 System.Void Mirror.NetworkClient::RegisterSystemHandlers(System.Boolean)
extern void NetworkClient_RegisterSystemHandlers_mA88AD3AE61C023D0415C7FE149196CE902143059 (void);
// 0x000000E5 System.Void Mirror.NetworkClient::Connect(System.String)
extern void NetworkClient_Connect_mFE768C43186239AC9730A35D498906E6A9E3EFBF (void);
// 0x000000E6 System.Void Mirror.NetworkClient::Connect(System.Uri)
extern void NetworkClient_Connect_mC7A782B0FB7327ED6D5D90C2D159E3BDE7A7B159 (void);
// 0x000000E7 System.Void Mirror.NetworkClient::ConnectHost()
extern void NetworkClient_ConnectHost_m72D26C20B6BE60DB32760056A4B41A78BE2A50A3 (void);
// 0x000000E8 System.Void Mirror.NetworkClient::ConnectLocalServer()
extern void NetworkClient_ConnectLocalServer_m454D1654FF382948EA1E53DBCDB03002EFA505EE (void);
// 0x000000E9 System.Void Mirror.NetworkClient::Disconnect()
extern void NetworkClient_Disconnect_m21A0E9A0BFB3E5C2A6579668E8D2DBB9B6B2DBE9 (void);
// 0x000000EA System.Void Mirror.NetworkClient::OnTransportConnected()
extern void NetworkClient_OnTransportConnected_m5616C2719EEEB72494EE2CB0A58D52C5171BFE0A (void);
// 0x000000EB System.Boolean Mirror.NetworkClient::UnpackAndInvoke(Mirror.NetworkReader,System.Int32)
extern void NetworkClient_UnpackAndInvoke_mEE7ECFE5D434F8152EF9EFD9BEDB8B21F188B580 (void);
// 0x000000EC System.Void Mirror.NetworkClient::OnTransportData(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkClient_OnTransportData_m0DAC58D9B39B189EF721143A8C5BB3154C79F7EB (void);
// 0x000000ED System.Void Mirror.NetworkClient::OnTransportDisconnected()
extern void NetworkClient_OnTransportDisconnected_mCB848CF3AEB421CC0C225A49E8D0571FC6315EC9 (void);
// 0x000000EE System.Void Mirror.NetworkClient::OnError(System.Exception)
extern void NetworkClient_OnError_mCEBB3E46CA7D1075109F85D04F1E963BDDEDB384 (void);
// 0x000000EF System.Void Mirror.NetworkClient::Send(T,System.Int32)
// 0x000000F0 System.Void Mirror.NetworkClient::RegisterHandler(System.Action`1<T>,System.Boolean)
// 0x000000F1 System.Void Mirror.NetworkClient::ReplaceHandler(System.Action`2<Mirror.NetworkConnection,T>,System.Boolean)
// 0x000000F2 System.Void Mirror.NetworkClient::ReplaceHandler(System.Action`1<T>,System.Boolean)
// 0x000000F3 System.Boolean Mirror.NetworkClient::UnregisterHandler()
// 0x000000F4 System.Boolean Mirror.NetworkClient::GetPrefab(System.Guid,UnityEngine.GameObject&)
extern void NetworkClient_GetPrefab_m678989575DB12B4AF7E5EB6D7E093F97ED13A6C5 (void);
// 0x000000F5 System.Void Mirror.NetworkClient::RegisterPrefabIdentity(Mirror.NetworkIdentity)
extern void NetworkClient_RegisterPrefabIdentity_mE60ADF6ED57BC9DE082E2477E11A0560E1AAE529 (void);
// 0x000000F6 System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,System.Guid)
extern void NetworkClient_RegisterPrefab_m7B4E7F40011057CB98595AFC4B8FB45036BC27EB (void);
// 0x000000F7 System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject)
extern void NetworkClient_RegisterPrefab_m3A96053F28D0C61055AA9E94C3281B552E7DACC8 (void);
// 0x000000F8 System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,System.Guid,Mirror.SpawnDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterPrefab_m369DB14A7C16BD210B56709ACFA8E3D63CDFCFFC (void);
// 0x000000F9 System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,Mirror.SpawnDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterPrefab_mD121E8EAFF1469CECF448B19061C848599F6CB9A (void);
// 0x000000FA System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,System.Guid,Mirror.SpawnHandlerDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterPrefab_mF0893802FA981D769F7FB441BB70F474396066A4 (void);
// 0x000000FB System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,Mirror.SpawnHandlerDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterPrefab_m19D9EB2C5EF81464F30419447C4E11B3DFD1497F (void);
// 0x000000FC System.Void Mirror.NetworkClient::UnregisterPrefab(UnityEngine.GameObject)
extern void NetworkClient_UnregisterPrefab_m23F13011D72D521245371ADD22951C9FD5A6BF95 (void);
// 0x000000FD System.Void Mirror.NetworkClient::RegisterSpawnHandler(System.Guid,Mirror.SpawnDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterSpawnHandler_m4EA8FE3694A36DC3EC8500F5EA85E77F17902CE1 (void);
// 0x000000FE System.Void Mirror.NetworkClient::RegisterSpawnHandler(System.Guid,Mirror.SpawnHandlerDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterSpawnHandler_m0E8EA0177F3FDECE2826F66A561364F51C3061AD (void);
// 0x000000FF System.Void Mirror.NetworkClient::UnregisterSpawnHandler(System.Guid)
extern void NetworkClient_UnregisterSpawnHandler_m02F713EB933B35F96D397A0B3F689266FC07748F (void);
// 0x00000100 System.Void Mirror.NetworkClient::ClearSpawners()
extern void NetworkClient_ClearSpawners_m18C672B489B9C1B823DA85AE1ABFC69ADE33DF27 (void);
// 0x00000101 System.Boolean Mirror.NetworkClient::InvokeUnSpawnHandler(System.Guid,UnityEngine.GameObject)
extern void NetworkClient_InvokeUnSpawnHandler_m08A37C52CC165663C4A2AC204F42751E3BCE5E35 (void);
// 0x00000102 System.Boolean Mirror.NetworkClient::Ready()
extern void NetworkClient_Ready_mBBB945097A673E4EFE7DDE934C210D2A9EE4B6A7 (void);
// 0x00000103 System.Void Mirror.NetworkClient::InternalAddPlayer(Mirror.NetworkIdentity)
extern void NetworkClient_InternalAddPlayer_m00C082E62196CCE3F7A85F85B1A553D716CB525C (void);
// 0x00000104 System.Boolean Mirror.NetworkClient::AddPlayer()
extern void NetworkClient_AddPlayer_m78B4C74A788202FFA45C0EB1AD74BE2AC746C3D7 (void);
// 0x00000105 System.Void Mirror.NetworkClient::ApplySpawnPayload(Mirror.NetworkIdentity,Mirror.SpawnMessage)
extern void NetworkClient_ApplySpawnPayload_mD8F72DBDD33209F4B37A3781FC2ADF8F0F2EA3D5 (void);
// 0x00000106 System.Boolean Mirror.NetworkClient::FindOrSpawnObject(Mirror.SpawnMessage,Mirror.NetworkIdentity&)
extern void NetworkClient_FindOrSpawnObject_mBFAEAB7A2B6119C4EC9E7678A987E1DD4425819A (void);
// 0x00000107 Mirror.NetworkIdentity Mirror.NetworkClient::GetExistingObject(System.UInt32)
extern void NetworkClient_GetExistingObject_m13DC5B6A58A798E70761E38214A6478633080986 (void);
// 0x00000108 Mirror.NetworkIdentity Mirror.NetworkClient::SpawnPrefab(Mirror.SpawnMessage)
extern void NetworkClient_SpawnPrefab_m3B3377464433A00377FB27423863AEF7008D8A62 (void);
// 0x00000109 Mirror.NetworkIdentity Mirror.NetworkClient::SpawnSceneObject(System.UInt64)
extern void NetworkClient_SpawnSceneObject_m2FBF0C72F74A5494F100B6B9ABEC66C41D7AEEAA (void);
// 0x0000010A Mirror.NetworkIdentity Mirror.NetworkClient::GetAndRemoveSceneObject(System.UInt64)
extern void NetworkClient_GetAndRemoveSceneObject_m9BA68CB7047443A10B501CBE36462443DE67BBF0 (void);
// 0x0000010B System.Boolean Mirror.NetworkClient::ConsiderForSpawning(Mirror.NetworkIdentity)
extern void NetworkClient_ConsiderForSpawning_m225F825B55ECA8B916CBA0A6AE60C0ACF404153C (void);
// 0x0000010C System.Void Mirror.NetworkClient::PrepareToSpawnSceneObjects()
extern void NetworkClient_PrepareToSpawnSceneObjects_mD342375563CBA39EF02992FF712207E957041702 (void);
// 0x0000010D System.Void Mirror.NetworkClient::OnObjectSpawnStarted(Mirror.ObjectSpawnStartedMessage)
extern void NetworkClient_OnObjectSpawnStarted_m5526C66CE4E5B24261003BB6D09D34F06488F833 (void);
// 0x0000010E System.Void Mirror.NetworkClient::OnObjectSpawnFinished(Mirror.ObjectSpawnFinishedMessage)
extern void NetworkClient_OnObjectSpawnFinished_m6C0835A836B4E69E9D7785F3CFE04EBBF8814036 (void);
// 0x0000010F System.Void Mirror.NetworkClient::ClearNullFromSpawned()
extern void NetworkClient_ClearNullFromSpawned_m8B7DF7CD5B305ACA67B440171A3F84F5634D7C81 (void);
// 0x00000110 System.Void Mirror.NetworkClient::OnHostClientObjectDestroy(Mirror.ObjectDestroyMessage)
extern void NetworkClient_OnHostClientObjectDestroy_m464DAEC93D5A115810546D68E91D26EE5B1E69CB (void);
// 0x00000111 System.Void Mirror.NetworkClient::OnHostClientObjectHide(Mirror.ObjectHideMessage)
extern void NetworkClient_OnHostClientObjectHide_mAF1056993D91A711735DED6B50F7BA1A7D0EE5B2 (void);
// 0x00000112 System.Void Mirror.NetworkClient::OnHostClientSpawn(Mirror.SpawnMessage)
extern void NetworkClient_OnHostClientSpawn_mCE0BB50FA56A108FF5495B2927ACF908BD14E175 (void);
// 0x00000113 System.Void Mirror.NetworkClient::OnEntityStateMessage(Mirror.EntityStateMessage)
extern void NetworkClient_OnEntityStateMessage_m56AB050D55735359FAC9CA4632B6BC27EB224577 (void);
// 0x00000114 System.Void Mirror.NetworkClient::OnRPCMessage(Mirror.RpcMessage)
extern void NetworkClient_OnRPCMessage_m7D4C5E49AED3F41FDBCDA1A820F19043D0E229E6 (void);
// 0x00000115 System.Void Mirror.NetworkClient::OnObjectHide(Mirror.ObjectHideMessage)
extern void NetworkClient_OnObjectHide_m6F77C650A6B5543BC3E4890C79230DBC81186A2E (void);
// 0x00000116 System.Void Mirror.NetworkClient::OnObjectDestroy(Mirror.ObjectDestroyMessage)
extern void NetworkClient_OnObjectDestroy_mF55A4E10FF6A53CCEE452EEC40AB24F35AB34B9C (void);
// 0x00000117 System.Void Mirror.NetworkClient::OnSpawn(Mirror.SpawnMessage)
extern void NetworkClient_OnSpawn_m9137AB0B4880B88ECBE13ADAE18D4F1A798978C9 (void);
// 0x00000118 System.Void Mirror.NetworkClient::OnChangeOwner(Mirror.ChangeOwnerMessage)
extern void NetworkClient_OnChangeOwner_m0E7ABCA9E0E783B18C0EE4BF5B999D3CECDF8447 (void);
// 0x00000119 System.Void Mirror.NetworkClient::ChangeOwner(Mirror.NetworkIdentity,Mirror.ChangeOwnerMessage)
extern void NetworkClient_ChangeOwner_m598BD6CDF65A61C6BE1308819183028ADF29B9DC (void);
// 0x0000011A System.Void Mirror.NetworkClient::CheckForLocalPlayer(Mirror.NetworkIdentity)
extern void NetworkClient_CheckForLocalPlayer_m3C7EE54D70BAEACF185150DA524A9F6B363B0745 (void);
// 0x0000011B System.Void Mirror.NetworkClient::DestroyObject(System.UInt32)
extern void NetworkClient_DestroyObject_mA2CF724C2D5C63742526D4764D6F0A784EA176CF (void);
// 0x0000011C System.Void Mirror.NetworkClient::NetworkEarlyUpdate()
extern void NetworkClient_NetworkEarlyUpdate_m0AA4A20D1F75DD16082ABC5071C15EAEF7B9FA1A (void);
// 0x0000011D System.Void Mirror.NetworkClient::NetworkLateUpdate()
extern void NetworkClient_NetworkLateUpdate_mC4885568D5F12AC2B253E7B28A9ED634F1A5E0D4 (void);
// 0x0000011E System.Void Mirror.NetworkClient::DestroyAllClientObjects()
extern void NetworkClient_DestroyAllClientObjects_mBE6F479C200E10833F9FC134478B3377AE1E4F15 (void);
// 0x0000011F System.Void Mirror.NetworkClient::Shutdown()
extern void NetworkClient_Shutdown_mB8DE5EAF29857E25ECAE1808AEC2E174121B280C (void);
// 0x00000120 System.Void Mirror.NetworkClient::.cctor()
extern void NetworkClient__cctor_mB9642212474ADA1517DDF308D7B7017FB2FC68C7 (void);
// 0x00000121 System.Void Mirror.NetworkClient/<>c::.cctor()
extern void U3CU3Ec__cctor_mF5B8CFBF1BC1295E5E9CC72B812966F97F5E3939 (void);
// 0x00000122 System.Void Mirror.NetworkClient/<>c::.ctor()
extern void U3CU3Ec__ctor_m775B14A08D98B75CB79315A8FC616EAFF41E20BA (void);
// 0x00000123 System.Void Mirror.NetworkClient/<>c::<RegisterSystemHandlers>b__35_0(Mirror.NetworkPongMessage)
extern void U3CU3Ec_U3CRegisterSystemHandlersU3Eb__35_0_m639849CBAACBF44DD414FBB7B6ECA35809B1F6D2 (void);
// 0x00000124 System.Void Mirror.NetworkClient/<>c::<RegisterSystemHandlers>b__35_1(Mirror.ObjectSpawnStartedMessage)
extern void U3CU3Ec_U3CRegisterSystemHandlersU3Eb__35_1_mBC891D7B3EF593243DFFEA3E5EB5515C1E149848 (void);
// 0x00000125 System.Void Mirror.NetworkClient/<>c::<RegisterSystemHandlers>b__35_2(Mirror.ObjectSpawnFinishedMessage)
extern void U3CU3Ec_U3CRegisterSystemHandlersU3Eb__35_2_m8B2FFF3053405F86FD6F9F7B52F217FAED9192BE (void);
// 0x00000126 System.Void Mirror.NetworkClient/<>c::<RegisterSystemHandlers>b__35_3(Mirror.EntityStateMessage)
extern void U3CU3Ec_U3CRegisterSystemHandlersU3Eb__35_3_m7F600632F8FFE6760AF1395A04F488CA0F639E3E (void);
// 0x00000127 System.UInt32 Mirror.NetworkClient/<>c::<OnObjectSpawnFinished>b__77_0(Mirror.NetworkIdentity)
extern void U3CU3Ec_U3COnObjectSpawnFinishedU3Eb__77_0_mC5F002B53A92AA2C5419A6417B270B70A0875017 (void);
// 0x00000128 System.Void Mirror.NetworkClient/<>c__DisplayClass47_0`1::.ctor()
// 0x00000129 System.Void Mirror.NetworkClient/<>c__DisplayClass47_0`1::<RegisterHandler>g__HandlerWrapped|0(Mirror.NetworkConnection,T)
// 0x0000012A System.Void Mirror.NetworkClient/<>c__DisplayClass49_0`1::.ctor()
// 0x0000012B System.Void Mirror.NetworkClient/<>c__DisplayClass49_0`1::<ReplaceHandler>b__0(Mirror.NetworkConnection,T)
// 0x0000012C System.Void Mirror.NetworkClient/<>c__DisplayClass55_0::.ctor()
extern void U3CU3Ec__DisplayClass55_0__ctor_m7BD53CFF3662760A31EA20BC3F8E7999D7146F1E (void);
// 0x0000012D UnityEngine.GameObject Mirror.NetworkClient/<>c__DisplayClass55_0::<RegisterPrefab>b__0(Mirror.SpawnMessage)
extern void U3CU3Ec__DisplayClass55_0_U3CRegisterPrefabU3Eb__0_m7232C5199884C33F5570D35F7BA6822726FDBC14 (void);
// 0x0000012E System.Void Mirror.NetworkClient/<>c__DisplayClass56_0::.ctor()
extern void U3CU3Ec__DisplayClass56_0__ctor_mE9EDCF65F817A4400C5691DB28712B2A6BFC3E30 (void);
// 0x0000012F UnityEngine.GameObject Mirror.NetworkClient/<>c__DisplayClass56_0::<RegisterPrefab>b__0(Mirror.SpawnMessage)
extern void U3CU3Ec__DisplayClass56_0_U3CRegisterPrefabU3Eb__0_m4F4A14C3BB286ACD33F093AD93AC16A1460E284D (void);
// 0x00000130 System.Void Mirror.NetworkClient/<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_m2BC6035ADA1F23F80ED2FDF81540A2E7BE138ECF (void);
// 0x00000131 UnityEngine.GameObject Mirror.NetworkClient/<>c__DisplayClass60_0::<RegisterSpawnHandler>b__0(Mirror.SpawnMessage)
extern void U3CU3Ec__DisplayClass60_0_U3CRegisterSpawnHandlerU3Eb__0_m9AA0ED1CF7F40F1F508B63BF276976A9B1F4C6CD (void);
// 0x00000132 System.Collections.Generic.HashSet`1<Mirror.NetworkIdentity> Mirror.NetworkConnection::get_observing()
extern void NetworkConnection_get_observing_m76F1265AC9C153CD56B967C995F48DBE790A279B (void);
// 0x00000133 System.String Mirror.NetworkConnection::get_address()
// 0x00000134 Mirror.NetworkIdentity Mirror.NetworkConnection::get_identity()
extern void NetworkConnection_get_identity_m47B5BA14E5FA41A648F2E98231B8FB4E6BC3693D (void);
// 0x00000135 System.Void Mirror.NetworkConnection::set_identity(Mirror.NetworkIdentity)
extern void NetworkConnection_set_identity_m7058218EAFA085B7D92C16E38E2ECAE6B5C2360D (void);
// 0x00000136 System.Collections.Generic.HashSet`1<Mirror.NetworkIdentity> Mirror.NetworkConnection::get_clientOwnedObjects()
extern void NetworkConnection_get_clientOwnedObjects_mE2F8EEF302DA963516E0856BE8606ACB842AE74A (void);
// 0x00000137 System.Double Mirror.NetworkConnection::get_remoteTimeStamp()
extern void NetworkConnection_get_remoteTimeStamp_m78C1EC9507C1E7BFA574B37371B199C1F4C9DE5D (void);
// 0x00000138 System.Void Mirror.NetworkConnection::set_remoteTimeStamp(System.Double)
extern void NetworkConnection_set_remoteTimeStamp_m4B0FB9D21E7497854E55A6A5C25E1FEED72005A4 (void);
// 0x00000139 System.Void Mirror.NetworkConnection::.ctor()
extern void NetworkConnection__ctor_mF9507734AD718690090796907B2C6E5B9B182636 (void);
// 0x0000013A System.Void Mirror.NetworkConnection::.ctor(System.Int32)
extern void NetworkConnection__ctor_m1A1F427C7F07409461CBBAC865E6F3B97BE23C8E (void);
// 0x0000013B Mirror.Batcher Mirror.NetworkConnection::GetBatchForChannelId(System.Int32)
extern void NetworkConnection_GetBatchForChannelId_mFB551DBCCF6DB50B41F1A01E7C8012D25C781F1B (void);
// 0x0000013C System.Boolean Mirror.NetworkConnection::ValidatePacketSize(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkConnection_ValidatePacketSize_mF48FE6D020DDE39177F34A7D29A3A1A9294686FD (void);
// 0x0000013D System.Void Mirror.NetworkConnection::Send(T,System.Int32)
// 0x0000013E System.Void Mirror.NetworkConnection::Send(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkConnection_Send_m17F96D002C2EB4DEA0187D7E03AD01D8171ED346 (void);
// 0x0000013F System.Void Mirror.NetworkConnection::SendToTransport(System.ArraySegment`1<System.Byte>,System.Int32)
// 0x00000140 System.Void Mirror.NetworkConnection::Update()
extern void NetworkConnection_Update_m689B01F67CA54136724EC3FFAADF8443F50DF267 (void);
// 0x00000141 System.Boolean Mirror.NetworkConnection::IsAlive(System.Single)
extern void NetworkConnection_IsAlive_mE03A9D79115AC77D8B030E010E9E3D1C96346FF3 (void);
// 0x00000142 System.Void Mirror.NetworkConnection::Disconnect()
// 0x00000143 System.String Mirror.NetworkConnection::ToString()
extern void NetworkConnection_ToString_m7E1111EE75534CAA3E357A53F6F26CCA5853ECAB (void);
// 0x00000144 System.String Mirror.NetworkConnectionToClient::get_address()
extern void NetworkConnectionToClient_get_address_m8570CE0FFF752E1CE4F8FB6162C4A7967E47D6C5 (void);
// 0x00000145 System.Void Mirror.NetworkConnectionToClient::.ctor(System.Int32)
extern void NetworkConnectionToClient__ctor_m5705661C93CCE87C4BFDDDA27B81B805E3A36FB2 (void);
// 0x00000146 System.Void Mirror.NetworkConnectionToClient::SendToTransport(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkConnectionToClient_SendToTransport_mF9447C215A986A1C69C2F39DD722A6D5ADD09621 (void);
// 0x00000147 System.Void Mirror.NetworkConnectionToClient::Disconnect()
extern void NetworkConnectionToClient_Disconnect_m7CBA0B3910013ECA4202640E695D075087C92105 (void);
// 0x00000148 System.Void Mirror.NetworkConnectionToClient::AddToObserving(Mirror.NetworkIdentity)
extern void NetworkConnectionToClient_AddToObserving_m8FE2E80120C2DDA4922C7D9F2452DDC2D4B61B1E (void);
// 0x00000149 System.Void Mirror.NetworkConnectionToClient::RemoveFromObserving(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkConnectionToClient_RemoveFromObserving_m147BAC8819C87D0E7C967417351F73F59D5D3914 (void);
// 0x0000014A System.Void Mirror.NetworkConnectionToClient::RemoveFromObservingsObservers()
extern void NetworkConnectionToClient_RemoveFromObservingsObservers_m85C96591B6A2B711620EFCFA85744852AD1BD081 (void);
// 0x0000014B System.Void Mirror.NetworkConnectionToClient::AddOwnedObject(Mirror.NetworkIdentity)
extern void NetworkConnectionToClient_AddOwnedObject_m8B35737E33A7C95D361138D6B5C3C12EC399B2D8 (void);
// 0x0000014C System.Void Mirror.NetworkConnectionToClient::RemoveOwnedObject(Mirror.NetworkIdentity)
extern void NetworkConnectionToClient_RemoveOwnedObject_m483A0546623A94C76D75B2005147CE00E75B96C1 (void);
// 0x0000014D System.Void Mirror.NetworkConnectionToClient::DestroyOwnedObjects()
extern void NetworkConnectionToClient_DestroyOwnedObjects_mE55FE0D8D78D31C26A6F3A1489B515A6B76BD0B8 (void);
// 0x0000014E System.String Mirror.NetworkConnectionToServer::get_address()
extern void NetworkConnectionToServer_get_address_m8B37BCF6C31A40567AE58CA9A644E3432F46DC06 (void);
// 0x0000014F System.Void Mirror.NetworkConnectionToServer::SendToTransport(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkConnectionToServer_SendToTransport_mE4BD3DEE8AA367C7A4BA432325082665F1F372A0 (void);
// 0x00000150 System.Void Mirror.NetworkConnectionToServer::Disconnect()
extern void NetworkConnectionToServer_Disconnect_m92ED30CAAE84B3FAEEAFA120D03C66378D38D0A7 (void);
// 0x00000151 System.Void Mirror.NetworkConnectionToServer::.ctor()
extern void NetworkConnectionToServer__ctor_m982DBB4E1EA531A16CC61CEF4B54F913CA097097 (void);
// 0x00000152 System.Void Mirror.NetworkDiagnostics::add_OutMessageEvent(System.Action`1<Mirror.NetworkDiagnostics/MessageInfo>)
extern void NetworkDiagnostics_add_OutMessageEvent_m06B3033E18E9C2811FF72DB202554A4BF09759E1 (void);
// 0x00000153 System.Void Mirror.NetworkDiagnostics::remove_OutMessageEvent(System.Action`1<Mirror.NetworkDiagnostics/MessageInfo>)
extern void NetworkDiagnostics_remove_OutMessageEvent_m7316455882B3370FF74F491EE02FD464B9B73322 (void);
// 0x00000154 System.Void Mirror.NetworkDiagnostics::add_InMessageEvent(System.Action`1<Mirror.NetworkDiagnostics/MessageInfo>)
extern void NetworkDiagnostics_add_InMessageEvent_mFEBE1B764A7ADF6E838AE259D4B63D624C3C20A3 (void);
// 0x00000155 System.Void Mirror.NetworkDiagnostics::remove_InMessageEvent(System.Action`1<Mirror.NetworkDiagnostics/MessageInfo>)
extern void NetworkDiagnostics_remove_InMessageEvent_mD1209731C202D8C3F619CC398B1F1D8B390DBD90 (void);
// 0x00000156 System.Void Mirror.NetworkDiagnostics::ResetStatics()
extern void NetworkDiagnostics_ResetStatics_mA451C96B23DF641867953F3380CDD828E63E5007 (void);
// 0x00000157 System.Void Mirror.NetworkDiagnostics::OnSend(T,System.Int32,System.Int32,System.Int32)
// 0x00000158 System.Void Mirror.NetworkDiagnostics::OnReceive(T,System.Int32,System.Int32)
// 0x00000159 System.Void Mirror.NetworkDiagnostics/MessageInfo::.ctor(Mirror.NetworkMessage,System.Int32,System.Int32,System.Int32)
extern void MessageInfo__ctor_m544E158986CD48130C1E1E2B68040337F0CA475A (void);
// 0x0000015A System.Boolean Mirror.NetworkIdentity::get_isClient()
extern void NetworkIdentity_get_isClient_mB6F508ADC9D7A1B2954D7823D2F738D30F5B0EDE (void);
// 0x0000015B System.Void Mirror.NetworkIdentity::set_isClient(System.Boolean)
extern void NetworkIdentity_set_isClient_m674CE14E426E5D1BE3FA77E655C4C299C05DCF5B (void);
// 0x0000015C System.Boolean Mirror.NetworkIdentity::get_isServer()
extern void NetworkIdentity_get_isServer_mAA48E62EB023DA2D16BD28AA5D39D379F6F1702B (void);
// 0x0000015D System.Void Mirror.NetworkIdentity::set_isServer(System.Boolean)
extern void NetworkIdentity_set_isServer_mE7E2B9C334AB5CF91002A72B1A8A92AF07E39298 (void);
// 0x0000015E System.Boolean Mirror.NetworkIdentity::get_isLocalPlayer()
extern void NetworkIdentity_get_isLocalPlayer_mB8532B381D7C538BC46B0D00BCE2B452D4A5E193 (void);
// 0x0000015F System.Void Mirror.NetworkIdentity::set_isLocalPlayer(System.Boolean)
extern void NetworkIdentity_set_isLocalPlayer_m7980945B063603AA269C0402D578EE7C5186E7F4 (void);
// 0x00000160 System.Boolean Mirror.NetworkIdentity::get_isServerOnly()
extern void NetworkIdentity_get_isServerOnly_m6B6B74787FF82A2B113AF18513BFAB9673B17934 (void);
// 0x00000161 System.Boolean Mirror.NetworkIdentity::get_isClientOnly()
extern void NetworkIdentity_get_isClientOnly_mBA7207D11B2CAF0966781C0103C06FD48F416C44 (void);
// 0x00000162 System.Boolean Mirror.NetworkIdentity::get_hasAuthority()
extern void NetworkIdentity_get_hasAuthority_mA4E58E41DC978348D08F453B3AB6CAA9224A4C3D (void);
// 0x00000163 System.Void Mirror.NetworkIdentity::set_hasAuthority(System.Boolean)
extern void NetworkIdentity_set_hasAuthority_mCF0C9F9DA1852CC2A0A1CE9CCC3EDF661444B7DC (void);
// 0x00000164 System.UInt32 Mirror.NetworkIdentity::get_netId()
extern void NetworkIdentity_get_netId_m2F3E61E85597FE7EBAB83C1E461F42BE772AE352 (void);
// 0x00000165 System.Void Mirror.NetworkIdentity::set_netId(System.UInt32)
extern void NetworkIdentity_set_netId_m9155E92077AFC1FC51932238D983D5270664794F (void);
// 0x00000166 Mirror.NetworkConnection Mirror.NetworkIdentity::get_connectionToServer()
extern void NetworkIdentity_get_connectionToServer_m518AA8B59164E33A77BC72F3539BD5E43631C161 (void);
// 0x00000167 System.Void Mirror.NetworkIdentity::set_connectionToServer(Mirror.NetworkConnection)
extern void NetworkIdentity_set_connectionToServer_m144247A9C92582B4B0AC47E955CD8BC50653E2A5 (void);
// 0x00000168 Mirror.NetworkConnectionToClient Mirror.NetworkIdentity::get_connectionToClient()
extern void NetworkIdentity_get_connectionToClient_m268D25CA55CD77C44173982E559901D9D59CAFC1 (void);
// 0x00000169 System.Void Mirror.NetworkIdentity::set_connectionToClient(Mirror.NetworkConnectionToClient)
extern void NetworkIdentity_set_connectionToClient_m4A52B8CC08863B11F8DA4F6C6B35233618BD1E6D (void);
// 0x0000016A System.Collections.Generic.Dictionary`2<System.UInt32,Mirror.NetworkIdentity> Mirror.NetworkIdentity::get_spawned()
extern void NetworkIdentity_get_spawned_m6A36A3E6B0CA084CA1F1C0220DD3D7BF1C0006E8 (void);
// 0x0000016B Mirror.NetworkBehaviour[] Mirror.NetworkIdentity::get_NetworkBehaviours()
extern void NetworkIdentity_get_NetworkBehaviours_mEFEAE71AE3EE53192B6BB9E431CBEA375B47E448 (void);
// 0x0000016C System.Void Mirror.NetworkIdentity::set_NetworkBehaviours(Mirror.NetworkBehaviour[])
extern void NetworkIdentity_set_NetworkBehaviours_m9AF9E5FDE4252B906D34A4F4F51CAC0A841BFD21 (void);
// 0x0000016D System.Guid Mirror.NetworkIdentity::get_assetId()
extern void NetworkIdentity_get_assetId_m734370E8D5AE546B5F844EF99D03EE59268404F2 (void);
// 0x0000016E System.Void Mirror.NetworkIdentity::set_assetId(System.Guid)
extern void NetworkIdentity_set_assetId_m7811E6284395457BAB4F89021D0911E5E445487A (void);
// 0x0000016F System.Void Mirror.NetworkIdentity::ResetClientStatics()
extern void NetworkIdentity_ResetClientStatics_m594EA3D8DC8356C8EE2AECC843F64D3B04CDD089 (void);
// 0x00000170 System.Void Mirror.NetworkIdentity::ResetServerStatics()
extern void NetworkIdentity_ResetServerStatics_m540B166F756FB767942580CB9CA340C3C5CFA1F6 (void);
// 0x00000171 System.Void Mirror.NetworkIdentity::ResetStatics()
extern void NetworkIdentity_ResetStatics_m67230A5AE6B41D81EBF8E751517FC358A577D8E4 (void);
// 0x00000172 Mirror.NetworkIdentity Mirror.NetworkIdentity::GetSceneIdentity(System.UInt64)
extern void NetworkIdentity_GetSceneIdentity_m59D1B25F0D230035472F73A0A082E270ED751484 (void);
// 0x00000173 System.Void Mirror.NetworkIdentity::SetClientOwner(Mirror.NetworkConnectionToClient)
extern void NetworkIdentity_SetClientOwner_mD63B6C15BB1923625CAC0F7BE62D5D5B55FB792A (void);
// 0x00000174 System.UInt32 Mirror.NetworkIdentity::GetNextNetworkId()
extern void NetworkIdentity_GetNextNetworkId_m64DBB63473A18B06504EAE89B5B390CAF1E0E473 (void);
// 0x00000175 System.Void Mirror.NetworkIdentity::ResetNextNetworkId()
extern void NetworkIdentity_ResetNextNetworkId_m4123AC38348103BC9785EB4660E1A014A591AA70 (void);
// 0x00000176 System.Void Mirror.NetworkIdentity::add_clientAuthorityCallback(Mirror.NetworkIdentity/ClientAuthorityCallback)
extern void NetworkIdentity_add_clientAuthorityCallback_m17469E78F3DE39097879AB6B245AC029E7B3257B (void);
// 0x00000177 System.Void Mirror.NetworkIdentity::remove_clientAuthorityCallback(Mirror.NetworkIdentity/ClientAuthorityCallback)
extern void NetworkIdentity_remove_clientAuthorityCallback_m53C01E7C73C884A6BAF29B9064E2DB14A68ECF48 (void);
// 0x00000178 System.Boolean Mirror.NetworkIdentity::get_SpawnedFromInstantiate()
extern void NetworkIdentity_get_SpawnedFromInstantiate_m985CAC4CA4FB90555EF34953D578375F79FD0B57 (void);
// 0x00000179 System.Void Mirror.NetworkIdentity::set_SpawnedFromInstantiate(System.Boolean)
extern void NetworkIdentity_set_SpawnedFromInstantiate_m57DB19FA8CCEF2C96367CD84D8F547F30FC6E746 (void);
// 0x0000017A System.Void Mirror.NetworkIdentity::InitializeNetworkBehaviours()
extern void NetworkIdentity_InitializeNetworkBehaviours_m0A4BB07C231B34591F0BA5AF6EB4D62772431B91 (void);
// 0x0000017B System.Void Mirror.NetworkIdentity::Awake()
extern void NetworkIdentity_Awake_m61323B6C0742418D53F23F9934BD7E2194724354 (void);
// 0x0000017C System.Void Mirror.NetworkIdentity::OnValidate()
extern void NetworkIdentity_OnValidate_m843D1663189D3313DA2FAA92ABFA7A9AF178FFC6 (void);
// 0x0000017D System.Void Mirror.NetworkIdentity::OnDestroy()
extern void NetworkIdentity_OnDestroy_m716EA63BBDD4355E95A3D30AA1B633B0C4C3316A (void);
// 0x0000017E System.Void Mirror.NetworkIdentity::OnStartServer()
extern void NetworkIdentity_OnStartServer_mD44DEEFF99FC21B78458E24E8F120DE511B84A30 (void);
// 0x0000017F System.Void Mirror.NetworkIdentity::OnStopServer()
extern void NetworkIdentity_OnStopServer_m21FB3585CAEC3597F29D44883755D9C491E70668 (void);
// 0x00000180 System.Void Mirror.NetworkIdentity::OnStartClient()
extern void NetworkIdentity_OnStartClient_m5CACC914D370F9C9381EBCE89A79BF8F5D01A00D (void);
// 0x00000181 System.Void Mirror.NetworkIdentity::OnStopClient()
extern void NetworkIdentity_OnStopClient_m97A59849415C95C7DCB211004E49CB4C990A5E26 (void);
// 0x00000182 System.Void Mirror.NetworkIdentity::OnStartLocalPlayer()
extern void NetworkIdentity_OnStartLocalPlayer_mBDC795F80A5113A0035B70BFA8428294C98A828B (void);
// 0x00000183 System.Void Mirror.NetworkIdentity::OnStopLocalPlayer()
extern void NetworkIdentity_OnStopLocalPlayer_m5D6E1E03F39A8CB01B92D037ADDFDC2AAF3CAB26 (void);
// 0x00000184 System.Void Mirror.NetworkIdentity::NotifyAuthority()
extern void NetworkIdentity_NotifyAuthority_mEB7883212FEECFB8BDAB85A33802CA369E38D5F2 (void);
// 0x00000185 System.Void Mirror.NetworkIdentity::OnStartAuthority()
extern void NetworkIdentity_OnStartAuthority_m461122B7AA8C1E2D4311089750D5CB4C895D5713 (void);
// 0x00000186 System.Void Mirror.NetworkIdentity::OnStopAuthority()
extern void NetworkIdentity_OnStopAuthority_mCF547D01F942E2A31431078C255FA7B734CF120D (void);
// 0x00000187 System.Boolean Mirror.NetworkIdentity::OnSerializeSafely(Mirror.NetworkBehaviour,Mirror.NetworkWriter,System.Boolean)
extern void NetworkIdentity_OnSerializeSafely_m5863548E550C8918AF8C731CE6D0EAB9E1F71244 (void);
// 0x00000188 System.Void Mirror.NetworkIdentity::OnSerializeAllSafely(System.Boolean,Mirror.NetworkWriter,Mirror.NetworkWriter)
extern void NetworkIdentity_OnSerializeAllSafely_m87914DB9FA0100FF860E6B99F7833F5ECD5D1CBD (void);
// 0x00000189 Mirror.NetworkIdentitySerialization Mirror.NetworkIdentity::GetSerializationAtTick(System.Int32)
extern void NetworkIdentity_GetSerializationAtTick_m9495CFBBA40226D903D651B64255302A6AEC79E6 (void);
// 0x0000018A System.Void Mirror.NetworkIdentity::OnDeserializeSafely(Mirror.NetworkBehaviour,Mirror.NetworkReader,System.Boolean)
extern void NetworkIdentity_OnDeserializeSafely_mB210EB25553E357D0D303C01C90AE3CF846360EE (void);
// 0x0000018B System.Void Mirror.NetworkIdentity::OnDeserializeAllSafely(Mirror.NetworkReader,System.Boolean)
extern void NetworkIdentity_OnDeserializeAllSafely_m7F6FA11EEDE3378A69F9DA0507B8655BA2886EBF (void);
// 0x0000018C System.Void Mirror.NetworkIdentity::HandleRemoteCall(System.Byte,System.Int32,Mirror.RemoteCalls.RemoteCallType,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkIdentity_HandleRemoteCall_m77F21A52606C6F956261E0C1E75FAA09A647BBEC (void);
// 0x0000018D System.Void Mirror.NetworkIdentity::AddObserver(Mirror.NetworkConnectionToClient)
extern void NetworkIdentity_AddObserver_m9428D53857802CA726B1C75788327EFCC4EC95F7 (void);
// 0x0000018E System.Void Mirror.NetworkIdentity::RemoveObserver(Mirror.NetworkConnection)
extern void NetworkIdentity_RemoveObserver_mA99E3F9D4E836D8A343886F0D30F254A81FFF440 (void);
// 0x0000018F System.Void Mirror.NetworkIdentity::ClearObservers()
extern void NetworkIdentity_ClearObservers_m44CCE981F8D984092165263D485857CED1BF8488 (void);
// 0x00000190 System.Boolean Mirror.NetworkIdentity::AssignClientAuthority(Mirror.NetworkConnectionToClient)
extern void NetworkIdentity_AssignClientAuthority_m4C4D7E0B6C030144F75A0C10551FB19906FFEA1E (void);
// 0x00000191 System.Void Mirror.NetworkIdentity::RemoveClientAuthority()
extern void NetworkIdentity_RemoveClientAuthority_m3C57C692868A72586E80B532B2AB07E2911F9936 (void);
// 0x00000192 System.Void Mirror.NetworkIdentity::Reset()
extern void NetworkIdentity_Reset_m6DA85920C025150079F7E5CBE09AA8AA10E0FF98 (void);
// 0x00000193 System.Void Mirror.NetworkIdentity::ClearAllComponentsDirtyBits()
extern void NetworkIdentity_ClearAllComponentsDirtyBits_m31F5D1D6622E8A2950963310B6437828BC5671BC (void);
// 0x00000194 System.Void Mirror.NetworkIdentity::ClearDirtyComponentsDirtyBits()
extern void NetworkIdentity_ClearDirtyComponentsDirtyBits_m4771E50B6E4A43B907A47AC6499D985B67139ACA (void);
// 0x00000195 System.Void Mirror.NetworkIdentity::ResetSyncObjects()
extern void NetworkIdentity_ResetSyncObjects_m863724056E602514683FC107518CA8882DD41249 (void);
// 0x00000196 System.Void Mirror.NetworkIdentity::.ctor()
extern void NetworkIdentity__ctor_m1DADAE2A93FFF224D07F8BCAA13FBF044B3606BF (void);
// 0x00000197 System.Void Mirror.NetworkIdentity::.cctor()
extern void NetworkIdentity__cctor_mCFBBCD563E259413802B2783DE795FB6B8FF29DA (void);
// 0x00000198 System.Void Mirror.NetworkIdentity/ClientAuthorityCallback::.ctor(System.Object,System.IntPtr)
extern void ClientAuthorityCallback__ctor_m9C3EF950A999A09BF206ADEF93C35FD75FC29373 (void);
// 0x00000199 System.Void Mirror.NetworkIdentity/ClientAuthorityCallback::Invoke(Mirror.NetworkConnectionToClient,Mirror.NetworkIdentity,System.Boolean)
extern void ClientAuthorityCallback_Invoke_m2ACF61C8B1BB8BE3007906B17B3EE0F5286575F5 (void);
// 0x0000019A System.IAsyncResult Mirror.NetworkIdentity/ClientAuthorityCallback::BeginInvoke(Mirror.NetworkConnectionToClient,Mirror.NetworkIdentity,System.Boolean,System.AsyncCallback,System.Object)
extern void ClientAuthorityCallback_BeginInvoke_m35E7EAF31A4E4C48F8B96CCB32212C07FF38A037 (void);
// 0x0000019B System.Void Mirror.NetworkIdentity/ClientAuthorityCallback::EndInvoke(System.IAsyncResult)
extern void ClientAuthorityCallback_EndInvoke_m5A059C6427D19D10CC8036CB3FFD67F3F681C91D (void);
// 0x0000019C System.Void Mirror.NetworkLoop::ResetStatics()
extern void NetworkLoop_ResetStatics_m862994F7557A01209BF1B6B0B3C586CCD3997216 (void);
// 0x0000019D System.Int32 Mirror.NetworkLoop::FindPlayerLoopEntryIndex(UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction,UnityEngine.LowLevel.PlayerLoopSystem,System.Type)
extern void NetworkLoop_FindPlayerLoopEntryIndex_mAAAF022FCD5597108F2D8D29F2D18459082721EA (void);
// 0x0000019E System.Boolean Mirror.NetworkLoop::AddToPlayerLoop(UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction,System.Type,UnityEngine.LowLevel.PlayerLoopSystem&,System.Type,Mirror.NetworkLoop/AddMode)
extern void NetworkLoop_AddToPlayerLoop_m17DCFB9E363C3F44B05FFB25AC4606059F66E0CD (void);
// 0x0000019F System.Void Mirror.NetworkLoop::RuntimeInitializeOnLoad()
extern void NetworkLoop_RuntimeInitializeOnLoad_mC3BCA1C148F005EFF4F3C240BA148476EB69718D (void);
// 0x000001A0 System.Void Mirror.NetworkLoop::NetworkEarlyUpdate()
extern void NetworkLoop_NetworkEarlyUpdate_mC237C06D4A13EA3D4250DE8D0E4D363AA866A35D (void);
// 0x000001A1 System.Void Mirror.NetworkLoop::NetworkLateUpdate()
extern void NetworkLoop_NetworkLateUpdate_m1DFB608737B3D9FF90519C97FCF4920565CBE6FC (void);
// 0x000001A2 System.Void Mirror.NetworkLoop/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m84BCD7C90A38298F65EFB790577EA9D5CEBECC74 (void);
// 0x000001A3 System.Boolean Mirror.NetworkLoop/<>c__DisplayClass4_0::<FindPlayerLoopEntryIndex>b__0(UnityEngine.LowLevel.PlayerLoopSystem)
extern void U3CU3Ec__DisplayClass4_0_U3CFindPlayerLoopEntryIndexU3Eb__0_m84783107DDF53C5C039EA30F605DA2D63F2F0358 (void);
// 0x000001A4 Mirror.NetworkManager Mirror.NetworkManager::get_singleton()
extern void NetworkManager_get_singleton_m2A1745C1F66546C421D379CBC41C0A1255160348 (void);
// 0x000001A5 System.Void Mirror.NetworkManager::set_singleton(Mirror.NetworkManager)
extern void NetworkManager_set_singleton_m1AFBF26DD89AD4BC74A2B804952A5D556356A4BE (void);
// 0x000001A6 System.Int32 Mirror.NetworkManager::get_numPlayers()
extern void NetworkManager_get_numPlayers_m4AC2699A1F3A8AF82B937ECF114B6E5990A5C949 (void);
// 0x000001A7 System.Boolean Mirror.NetworkManager::get_isNetworkActive()
extern void NetworkManager_get_isNetworkActive_mE7A4388ED7E88A3FD416BCF9C378D2E806B2D2B0 (void);
// 0x000001A8 Mirror.NetworkManagerMode Mirror.NetworkManager::get_mode()
extern void NetworkManager_get_mode_mA83D561FE77A5425807F4DC2250C72925A30F340 (void);
// 0x000001A9 System.Void Mirror.NetworkManager::set_mode(Mirror.NetworkManagerMode)
extern void NetworkManager_set_mode_m84F7E022E6FD142079747F97912C1AD095D0CF45 (void);
// 0x000001AA System.Void Mirror.NetworkManager::OnValidate()
extern void NetworkManager_OnValidate_m91EBF8AB55BCE52DBEC6489F45447F8E0C4E8467 (void);
// 0x000001AB System.Void Mirror.NetworkManager::Reset()
extern void NetworkManager_Reset_m9E15822E644D5D183EEAC2F42217308DFE4B410C (void);
// 0x000001AC System.Void Mirror.NetworkManager::Awake()
extern void NetworkManager_Awake_mD005B3299BBA31F93D2F4C51E0706B15BFBEB02D (void);
// 0x000001AD System.Void Mirror.NetworkManager::Start()
extern void NetworkManager_Start_m73C42FA0BF878D979F7744B28F064BD6F3287A66 (void);
// 0x000001AE System.Void Mirror.NetworkManager::LateUpdate()
extern void NetworkManager_LateUpdate_m9DBDABB3A5C269F5BC64C4EE0D8DA16D35CCBDD5 (void);
// 0x000001AF System.Boolean Mirror.NetworkManager::IsServerOnlineSceneChangeNeeded()
extern void NetworkManager_IsServerOnlineSceneChangeNeeded_mE5F741C4F6B785A5B60952E57C668A216EB73742 (void);
// 0x000001B0 System.Boolean Mirror.NetworkManager::IsSceneActive(System.String)
extern void NetworkManager_IsSceneActive_m67196E967B8A3A488FAB7C7C2E39AE97E19C70A0 (void);
// 0x000001B1 System.Void Mirror.NetworkManager::SetupServer()
extern void NetworkManager_SetupServer_mF7C3000C3996BCD4FE0804E2AA7B54405E15A8EB (void);
// 0x000001B2 System.Void Mirror.NetworkManager::StartServer()
extern void NetworkManager_StartServer_mCBB645B1BFDB7BA46BF56B9A992FAAF703D86689 (void);
// 0x000001B3 System.Void Mirror.NetworkManager::StartClient()
extern void NetworkManager_StartClient_m3DD3AB5F061A45E1BD40A87227320B53D888A051 (void);
// 0x000001B4 System.Void Mirror.NetworkManager::StartClient(System.Uri)
extern void NetworkManager_StartClient_mC5F5EA047B43C8EEA63B99305D46BA7DFC12001C (void);
// 0x000001B5 System.Void Mirror.NetworkManager::StartHost()
extern void NetworkManager_StartHost_m20C95C6A3364A01B9ADBBD4338EC8C9C4DCA889C (void);
// 0x000001B6 System.Void Mirror.NetworkManager::FinishStartHost()
extern void NetworkManager_FinishStartHost_m4F5C8E0A069837D29BD19CB14A464D51B676EF82 (void);
// 0x000001B7 System.Void Mirror.NetworkManager::StartHostClient()
extern void NetworkManager_StartHostClient_mABE10784393569FC24BD894A9D94C062ADF9BFB3 (void);
// 0x000001B8 System.Void Mirror.NetworkManager::StopHost()
extern void NetworkManager_StopHost_m06C5629970EA21BFA8A1A2CC5B7A8D7C17CD6082 (void);
// 0x000001B9 System.Void Mirror.NetworkManager::StopServer()
extern void NetworkManager_StopServer_m646AEC8CCF261658BCC0320F80A194B35F62AB0D (void);
// 0x000001BA System.Void Mirror.NetworkManager::StopClient()
extern void NetworkManager_StopClient_m2B73201A7EF96AD8B41234F3990DEC3258C6781F (void);
// 0x000001BB System.Void Mirror.NetworkManager::OnApplicationQuit()
extern void NetworkManager_OnApplicationQuit_m5D21B806CC3873E8951C5FDA91A387AD26378283 (void);
// 0x000001BC System.Void Mirror.NetworkManager::ConfigureHeadlessFrameRate()
extern void NetworkManager_ConfigureHeadlessFrameRate_mFC486B3E7E6B7B0E763CC1DC6507354C9D58E58C (void);
// 0x000001BD System.Boolean Mirror.NetworkManager::InitializeSingleton()
extern void NetworkManager_InitializeSingleton_m26E1A31DDB6FA09FC4541EF874395D61E0150458 (void);
// 0x000001BE System.Void Mirror.NetworkManager::RegisterServerMessages()
extern void NetworkManager_RegisterServerMessages_mF0AB1F03C0E4DD124828B7E0D340ED9ED49CB605 (void);
// 0x000001BF System.Void Mirror.NetworkManager::RegisterClientMessages()
extern void NetworkManager_RegisterClientMessages_m1E34786A270EADB091629B7FF4D89B08AEF99D1E (void);
// 0x000001C0 System.Void Mirror.NetworkManager::ResetStatics()
extern void NetworkManager_ResetStatics_mEB380F62F910B847D08610A95D5867F8390069D2 (void);
// 0x000001C1 System.Void Mirror.NetworkManager::OnDestroy()
extern void NetworkManager_OnDestroy_mB7648E2344ECB4AA952BEA32F30EC244DB2FBBEF (void);
// 0x000001C2 System.String Mirror.NetworkManager::get_networkSceneName()
extern void NetworkManager_get_networkSceneName_mF3C3AB5E05B2D3EA39F50DC7ADAECBEBD3F933AB (void);
// 0x000001C3 System.Void Mirror.NetworkManager::set_networkSceneName(System.String)
extern void NetworkManager_set_networkSceneName_m61A206B02FF89A2CA92B3B04D0F8CBC7A5EA3D2E (void);
// 0x000001C4 System.Void Mirror.NetworkManager::ServerChangeScene(System.String)
extern void NetworkManager_ServerChangeScene_mF6130AF5DE927E0DDA7891B610BD56EA5E918023 (void);
// 0x000001C5 System.Void Mirror.NetworkManager::ClientChangeScene(System.String,Mirror.SceneOperation,System.Boolean)
extern void NetworkManager_ClientChangeScene_mF9DD7AE323CECDD9CD3C0065DD1AE3A0DA2D350D (void);
// 0x000001C6 System.Void Mirror.NetworkManager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void NetworkManager_OnSceneLoaded_mED7327F232D62A779C71353138D2F60653510CB5 (void);
// 0x000001C7 System.Void Mirror.NetworkManager::UpdateScene()
extern void NetworkManager_UpdateScene_mCFAAFCA2EA58A5FECB79AD418690C2F4065302C5 (void);
// 0x000001C8 System.Void Mirror.NetworkManager::FinishLoadScene()
extern void NetworkManager_FinishLoadScene_m4AC39BE4A23054791DC543694A56DE05AB6026A1 (void);
// 0x000001C9 System.Void Mirror.NetworkManager::FinishLoadSceneHost()
extern void NetworkManager_FinishLoadSceneHost_mBF9C53A75ED7C2BD872A1C129D2D821814AD6E5E (void);
// 0x000001CA System.Void Mirror.NetworkManager::FinishLoadSceneServerOnly()
extern void NetworkManager_FinishLoadSceneServerOnly_m33E7670B395B6B4A8A29B3D67933631E1415A91C (void);
// 0x000001CB System.Void Mirror.NetworkManager::FinishLoadSceneClientOnly()
extern void NetworkManager_FinishLoadSceneClientOnly_m4ED332F9E2E9220A706BC3AE94A2D8F10CCE92A4 (void);
// 0x000001CC System.Void Mirror.NetworkManager::RegisterStartPosition(UnityEngine.Transform)
extern void NetworkManager_RegisterStartPosition_m9326262335C4C75B5B7ACF7CF63A91490D6900F0 (void);
// 0x000001CD System.Void Mirror.NetworkManager::UnRegisterStartPosition(UnityEngine.Transform)
extern void NetworkManager_UnRegisterStartPosition_mA8835D4807A46C7DC634B6BC24B2424F5FC1C8AA (void);
// 0x000001CE UnityEngine.Transform Mirror.NetworkManager::GetStartPosition()
extern void NetworkManager_GetStartPosition_mF46E36E0D5A05DA2A52DAE8BA292F50A06D97BD7 (void);
// 0x000001CF System.Void Mirror.NetworkManager::OnServerConnectInternal(Mirror.NetworkConnectionToClient)
extern void NetworkManager_OnServerConnectInternal_mB269461DE8AAE99F0937E4BD83D40AF2F60D0DE7 (void);
// 0x000001D0 System.Void Mirror.NetworkManager::OnServerAuthenticated(Mirror.NetworkConnectionToClient)
extern void NetworkManager_OnServerAuthenticated_m5E39CEC9895897382E72F4D25D1E38746E0CBB45 (void);
// 0x000001D1 System.Void Mirror.NetworkManager::OnServerReadyMessageInternal(Mirror.NetworkConnectionToClient,Mirror.ReadyMessage)
extern void NetworkManager_OnServerReadyMessageInternal_m5D8FDA9D021DD79BC63C5421855561C66A915599 (void);
// 0x000001D2 System.Void Mirror.NetworkManager::OnServerAddPlayerInternal(Mirror.NetworkConnectionToClient,Mirror.AddPlayerMessage)
extern void NetworkManager_OnServerAddPlayerInternal_m5C28B1719C3CD4C49BA3345E6CD98361017FFA0C (void);
// 0x000001D3 System.Void Mirror.NetworkManager::OnClientConnectInternal()
extern void NetworkManager_OnClientConnectInternal_mF49127056D1A8A296BE62A4A355101FF182B456A (void);
// 0x000001D4 System.Void Mirror.NetworkManager::OnClientAuthenticated()
extern void NetworkManager_OnClientAuthenticated_m64C2FC1218F44EB09319E6B6BA8E87B89470E87F (void);
// 0x000001D5 System.Void Mirror.NetworkManager::OnClientDisconnectInternal()
extern void NetworkManager_OnClientDisconnectInternal_mF6A5A6A589CE7A443FF056B89269E5897C1FBDBD (void);
// 0x000001D6 System.Void Mirror.NetworkManager::OnClientNotReadyMessageInternal(Mirror.NotReadyMessage)
extern void NetworkManager_OnClientNotReadyMessageInternal_m0E4F365D2B6AA4B8FAC6DE4CED8111B403091772 (void);
// 0x000001D7 System.Void Mirror.NetworkManager::OnClientSceneInternal(Mirror.SceneMessage)
extern void NetworkManager_OnClientSceneInternal_m960E2F97F4864ED48D912126067F958576D01F08 (void);
// 0x000001D8 System.Void Mirror.NetworkManager::OnServerConnect(Mirror.NetworkConnectionToClient)
extern void NetworkManager_OnServerConnect_m5C8ADFA71EE15F4CE18C0C18F51D3EAB7277E95A (void);
// 0x000001D9 System.Void Mirror.NetworkManager::OnServerDisconnect(Mirror.NetworkConnectionToClient)
extern void NetworkManager_OnServerDisconnect_m4A144E9989D86B923899A6A551A2B7BCD7DE58C2 (void);
// 0x000001DA System.Void Mirror.NetworkManager::OnServerReady(Mirror.NetworkConnectionToClient)
extern void NetworkManager_OnServerReady_m4FD66B15BD939D64C0F3977017F08AB0DA2D5880 (void);
// 0x000001DB System.Void Mirror.NetworkManager::OnServerAddPlayer(Mirror.NetworkConnectionToClient)
extern void NetworkManager_OnServerAddPlayer_m0B70F7456F2DF3302E71EE010C9D8B23C1EE40BB (void);
// 0x000001DC System.Void Mirror.NetworkManager::OnServerError(Mirror.NetworkConnectionToClient,System.Exception)
extern void NetworkManager_OnServerError_mEE4ECC7016A2FC76DD590D1BD0662A27177E2974 (void);
// 0x000001DD System.Void Mirror.NetworkManager::OnServerChangeScene(System.String)
extern void NetworkManager_OnServerChangeScene_m7D722E01B19C1ED87D3744EDB367A99496D4E3EE (void);
// 0x000001DE System.Void Mirror.NetworkManager::OnServerSceneChanged(System.String)
extern void NetworkManager_OnServerSceneChanged_m9B00AC9CA81A59334723A67CA6B87E2E3654D61F (void);
// 0x000001DF System.Void Mirror.NetworkManager::OnClientConnect()
extern void NetworkManager_OnClientConnect_mB978DE1B531BF33DD91A02DFC98EC57D58FB6FDD (void);
// 0x000001E0 System.Void Mirror.NetworkManager::OnClientConnect(Mirror.NetworkConnection)
extern void NetworkManager_OnClientConnect_m0226ED8B8BD1F62C19E0840E6DBB40325DEF6592 (void);
// 0x000001E1 System.Void Mirror.NetworkManager::OnClientDisconnect()
extern void NetworkManager_OnClientDisconnect_m2291970371EAF8E4FD5697CA21EB1F4EC339E65A (void);
// 0x000001E2 System.Void Mirror.NetworkManager::OnClientDisconnect(Mirror.NetworkConnection)
extern void NetworkManager_OnClientDisconnect_mCD27E8993079E4C7EBA6BEA32D8B5D482A693C74 (void);
// 0x000001E3 System.Void Mirror.NetworkManager::OnClientError(System.Exception)
extern void NetworkManager_OnClientError_mCA52D0701AC90E34C118AF2E49FEF6CCAA29E112 (void);
// 0x000001E4 System.Void Mirror.NetworkManager::OnClientNotReady()
extern void NetworkManager_OnClientNotReady_m73D5E06347AFC345875B5104D19A45EEFE55C252 (void);
// 0x000001E5 System.Void Mirror.NetworkManager::OnClientNotReady(Mirror.NetworkConnection)
extern void NetworkManager_OnClientNotReady_mD7CD726A874E101C7FCA4BC02A329743374977C3 (void);
// 0x000001E6 System.Void Mirror.NetworkManager::OnClientChangeScene(System.String,Mirror.SceneOperation,System.Boolean)
extern void NetworkManager_OnClientChangeScene_mCD3DF009A1A1BC983B32F3E667589D9FE937382D (void);
// 0x000001E7 System.Void Mirror.NetworkManager::OnClientSceneChanged()
extern void NetworkManager_OnClientSceneChanged_mA612D8F7F93490EB508C78140D0F8DDBF36E2F20 (void);
// 0x000001E8 System.Void Mirror.NetworkManager::OnClientSceneChanged(Mirror.NetworkConnection)
extern void NetworkManager_OnClientSceneChanged_mAB98FE5B7CEDD22199EC46566450DF31AB937F35 (void);
// 0x000001E9 System.Void Mirror.NetworkManager::OnStartHost()
extern void NetworkManager_OnStartHost_m45761C08246E28887D9EC025A18170524D048081 (void);
// 0x000001EA System.Void Mirror.NetworkManager::OnStartServer()
extern void NetworkManager_OnStartServer_m1EC40BD3874A0246CE2B9B633BE902DB18F84CB0 (void);
// 0x000001EB System.Void Mirror.NetworkManager::OnStartClient()
extern void NetworkManager_OnStartClient_mD4C0CF1C14BBFBAB489EC1260684BC97343A6F8E (void);
// 0x000001EC System.Void Mirror.NetworkManager::OnStopServer()
extern void NetworkManager_OnStopServer_m25279DB69D43E622F92ECBE53FD53A459846DD29 (void);
// 0x000001ED System.Void Mirror.NetworkManager::OnStopClient()
extern void NetworkManager_OnStopClient_m4CF7ED3468D32E66AA28C8B16BE9F32BBFDBC74B (void);
// 0x000001EE System.Void Mirror.NetworkManager::OnStopHost()
extern void NetworkManager_OnStopHost_mEC7AD99ADFEC2B2F77D0DC531E5B94CE2A5C7F5A (void);
// 0x000001EF System.Void Mirror.NetworkManager::.ctor()
extern void NetworkManager__ctor_m12FFC2DC0BAB6F279513E0089C6772A115662B39 (void);
// 0x000001F0 System.Void Mirror.NetworkManager::.cctor()
extern void NetworkManager__cctor_mDFBBFAD7B474F3C676BEE84B4B737160A772C585 (void);
// 0x000001F1 System.Void Mirror.NetworkManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m8127F0AA36FF769697910076EFFF52A9027B4D37 (void);
// 0x000001F2 System.Void Mirror.NetworkManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mF7E023142E316D2D992981606A8C32F1F6913513 (void);
// 0x000001F3 System.Boolean Mirror.NetworkManager/<>c::<get_numPlayers>b__21_0(System.Collections.Generic.KeyValuePair`2<System.Int32,Mirror.NetworkConnectionToClient>)
extern void U3CU3Ec_U3Cget_numPlayersU3Eb__21_0_m36E3A5EF4600AC3A0E5776725D929D6AE0320723 (void);
// 0x000001F4 System.Boolean Mirror.NetworkManager/<>c::<RegisterClientMessages>b__52_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3CRegisterClientMessagesU3Eb__52_0_mD1A5A74DFD4326E03C1F9BA6E4C2667B22559B5A (void);
// 0x000001F5 System.Int32 Mirror.NetworkManager/<>c::<RegisterStartPosition>b__69_0(UnityEngine.Transform)
extern void U3CU3Ec_U3CRegisterStartPositionU3Eb__69_0_m90BD1E7DE6ADFFC4AB245B2BB151AAB0057F7856 (void);
// 0x000001F6 System.Boolean Mirror.NetworkManager/<>c::<GetStartPosition>b__71_0(UnityEngine.Transform)
extern void U3CU3Ec_U3CGetStartPositionU3Eb__71_0_mDE1748D7CA1C2E23862F3C7198456B4BBD417A9C (void);
// 0x000001F7 System.Void Mirror.NetworkManagerHUD::Awake()
extern void NetworkManagerHUD_Awake_mFFFA4148B42612E4B6E17276D99A52599E1E97D8 (void);
// 0x000001F8 System.Void Mirror.NetworkManagerHUD::OnGUI()
extern void NetworkManagerHUD_OnGUI_mB1FD5FA33A2154F7998B5D4A3BAFC5E438863DA8 (void);
// 0x000001F9 System.Void Mirror.NetworkManagerHUD::StartButtons()
extern void NetworkManagerHUD_StartButtons_mBAAD89CEFCB346EE68E673CC3994C2285E305998 (void);
// 0x000001FA System.Void Mirror.NetworkManagerHUD::StatusLabels()
extern void NetworkManagerHUD_StatusLabels_m9232A28A7AAB84B7171F8B390DBA29B51E15E5E4 (void);
// 0x000001FB System.Void Mirror.NetworkManagerHUD::StopButtons()
extern void NetworkManagerHUD_StopButtons_m2FEA798C6591A67761B8689C0407E8A81E209390 (void);
// 0x000001FC System.Void Mirror.NetworkManagerHUD::.ctor()
extern void NetworkManagerHUD__ctor_mFD8AF8171DC20F54AAFD59DBAA2066F62DD81429 (void);
// 0x000001FD System.Int32 Mirror.NetworkReader::get_Length()
extern void NetworkReader_get_Length_m23094EDDFE84816D0846ED68F1EFEA79220B93EE (void);
// 0x000001FE System.Int32 Mirror.NetworkReader::get_Remaining()
extern void NetworkReader_get_Remaining_m5375279B452A2CB1DC8DBF0E7237AF09CFDB75DA (void);
// 0x000001FF System.Void Mirror.NetworkReader::.ctor(System.Byte[])
extern void NetworkReader__ctor_m13108C432FBACE92C884F450773916200C245F81 (void);
// 0x00000200 System.Void Mirror.NetworkReader::.ctor(System.ArraySegment`1<System.Byte>)
extern void NetworkReader__ctor_m37DA1A425E58E602C5B129E3F8D18AE52C8C7759 (void);
// 0x00000201 System.Void Mirror.NetworkReader::SetBuffer(System.Byte[])
extern void NetworkReader_SetBuffer_m34DAEF19500AD7DF5A7263E89CAB36D300B111B8 (void);
// 0x00000202 System.Void Mirror.NetworkReader::SetBuffer(System.ArraySegment`1<System.Byte>)
extern void NetworkReader_SetBuffer_mB0A7BA6CBD97D646A9EE54873664EB6E105C560F (void);
// 0x00000203 T Mirror.NetworkReader::ReadBlittable()
// 0x00000204 System.Nullable`1<T> Mirror.NetworkReader::ReadBlittableNullable()
// 0x00000205 System.Byte Mirror.NetworkReader::ReadByte()
extern void NetworkReader_ReadByte_mB1431799AB014070DABB99C9A8E50C6F20720A9D (void);
// 0x00000206 System.Byte[] Mirror.NetworkReader::ReadBytes(System.Byte[],System.Int32)
extern void NetworkReader_ReadBytes_mA1E53425AAD4AD3038C9759F6971533248347130 (void);
// 0x00000207 System.ArraySegment`1<System.Byte> Mirror.NetworkReader::ReadBytesSegment(System.Int32)
extern void NetworkReader_ReadBytesSegment_mA17220D13799B7AA2EFF9B49C6F1F98B486A330E (void);
// 0x00000208 System.String Mirror.NetworkReader::ToString()
extern void NetworkReader_ToString_m910E83BEEDE5F1F1FC9B20C1CA0A5906253A6F5A (void);
// 0x00000209 T Mirror.NetworkReader::Read()
// 0x0000020A System.Byte Mirror.NetworkReaderExtensions::ReadByte(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED (void);
// 0x0000020B System.Nullable`1<System.Byte> Mirror.NetworkReaderExtensions::ReadByteNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadByteNullable_mB622478495C2AE927128F9F196A47DCFEB666E4E (void);
// 0x0000020C System.SByte Mirror.NetworkReaderExtensions::ReadSByte(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadSByte_m1B3975CC87DD10621C8A369EA7D053AFE57E958B (void);
// 0x0000020D System.Nullable`1<System.SByte> Mirror.NetworkReaderExtensions::ReadSByteNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadSByteNullable_mEAB105DEC52D7789AEE2A6E110B66A3C2EB8785E (void);
// 0x0000020E System.Char Mirror.NetworkReaderExtensions::ReadChar(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadChar_mCCA8829AD9CA54D8510AE4C3E3D1CA0F6F6E8966 (void);
// 0x0000020F System.Nullable`1<System.Char> Mirror.NetworkReaderExtensions::ReadCharNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadCharNullable_m728E4E8F336F06A0BDB8BDEE69842C707ED4540A (void);
// 0x00000210 System.Boolean Mirror.NetworkReaderExtensions::ReadBool(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06 (void);
// 0x00000211 System.Nullable`1<System.Boolean> Mirror.NetworkReaderExtensions::ReadBoolNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadBoolNullable_m49866827FF66A52CA4E36AAED2D3ACC8766F8B38 (void);
// 0x00000212 System.Int16 Mirror.NetworkReaderExtensions::ReadShort(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadShort_m5FFC8A9D90AE04D1D0AD681F1D2C32564DBC8677 (void);
// 0x00000213 System.Nullable`1<System.Int16> Mirror.NetworkReaderExtensions::ReadShortNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadShortNullable_m6B7ED0AC2C951C2461A34D3DEB05E6055399B896 (void);
// 0x00000214 System.UInt16 Mirror.NetworkReaderExtensions::ReadUShort(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUShort_mA98395DD1B1DA249096858B171B8BC23D95DF765 (void);
// 0x00000215 System.Nullable`1<System.UInt16> Mirror.NetworkReaderExtensions::ReadUShortNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUShortNullable_mEDDEE70BF7A15DC1503C4BDF580F54A26C82DCA4 (void);
// 0x00000216 System.Int32 Mirror.NetworkReaderExtensions::ReadInt(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A (void);
// 0x00000217 System.Nullable`1<System.Int32> Mirror.NetworkReaderExtensions::ReadIntNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadIntNullable_m9F68CD73D47D10DE2E1C6934DE14234E19D02E71 (void);
// 0x00000218 System.UInt32 Mirror.NetworkReaderExtensions::ReadUInt(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4 (void);
// 0x00000219 System.Nullable`1<System.UInt32> Mirror.NetworkReaderExtensions::ReadUIntNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUIntNullable_m3D4906C1707F48E5439F4EA0E9DE4B8860AB2E73 (void);
// 0x0000021A System.Int64 Mirror.NetworkReaderExtensions::ReadLong(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadLong_m67D408F9D8D9FB04A0101AAE2AB9B01120E34435 (void);
// 0x0000021B System.Nullable`1<System.Int64> Mirror.NetworkReaderExtensions::ReadLongNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadLongNullable_m747B938C128B0CAD7E22D0909E1AEE9DFDB54F67 (void);
// 0x0000021C System.UInt64 Mirror.NetworkReaderExtensions::ReadULong(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadULong_mAC6B83521EBA7FFEDFC72A6AAE1BF5D87221A5F5 (void);
// 0x0000021D System.Nullable`1<System.UInt64> Mirror.NetworkReaderExtensions::ReadULongNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadULongNullable_mE853C2A40E3E7F9FD1BB49D5E16BCB9310B0752E (void);
// 0x0000021E System.Single Mirror.NetworkReaderExtensions::ReadFloat(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadFloat_mF3D9834531FC09112A506971638FB9682A231D97 (void);
// 0x0000021F System.Nullable`1<System.Single> Mirror.NetworkReaderExtensions::ReadFloatNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadFloatNullable_m1EB56AA1F1CDB7981728CACF5941EB0B6B4275BD (void);
// 0x00000220 System.Double Mirror.NetworkReaderExtensions::ReadDouble(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadDouble_m949A60A21C6EB3B9952A43355903F08B3A7E0EF9 (void);
// 0x00000221 System.Nullable`1<System.Double> Mirror.NetworkReaderExtensions::ReadDoubleNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadDoubleNullable_mE52DB83CB818F30F912FD40175B39731A2FBD33B (void);
// 0x00000222 System.Decimal Mirror.NetworkReaderExtensions::ReadDecimal(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadDecimal_m79DE6589996D493A3A95BAD98036B09FF9CB144E (void);
// 0x00000223 System.Nullable`1<System.Decimal> Mirror.NetworkReaderExtensions::ReadDecimalNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadDecimalNullable_m18D27D0176D98F043EC804A512EB8B55856229E9 (void);
// 0x00000224 System.String Mirror.NetworkReaderExtensions::ReadString(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadString_m5A56179D2C1CB509EC9C762F97BBD4133A5AD394 (void);
// 0x00000225 System.Byte[] Mirror.NetworkReaderExtensions::ReadBytesAndSize(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadBytesAndSize_mB707572AAF6CBDE9E6FAC190629882468EAFAD8E (void);
// 0x00000226 System.Byte[] Mirror.NetworkReaderExtensions::ReadBytes(Mirror.NetworkReader,System.Int32)
extern void NetworkReaderExtensions_ReadBytes_mF2B3E392F976B37C12A9BB81DBEB98726813730D (void);
// 0x00000227 System.ArraySegment`1<System.Byte> Mirror.NetworkReaderExtensions::ReadBytesAndSizeSegment(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F (void);
// 0x00000228 UnityEngine.Vector2 Mirror.NetworkReaderExtensions::ReadVector2(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector2_m673B821E39E194BA5E2B7E5F444D6CCD76812811 (void);
// 0x00000229 System.Nullable`1<UnityEngine.Vector2> Mirror.NetworkReaderExtensions::ReadVector2Nullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector2Nullable_mCDCE58B581701AC12499A36355838E45F298C817 (void);
// 0x0000022A UnityEngine.Vector3 Mirror.NetworkReaderExtensions::ReadVector3(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector3_mD35BF8B14DD5F75688AB9C360D138D1BAB432637 (void);
// 0x0000022B System.Nullable`1<UnityEngine.Vector3> Mirror.NetworkReaderExtensions::ReadVector3Nullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector3Nullable_m17D39303F570FAB53014718C07327F9DAAD8DB18 (void);
// 0x0000022C UnityEngine.Vector4 Mirror.NetworkReaderExtensions::ReadVector4(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector4_m7870D12D4D86684F68719E7F040A33A085C2F1D4 (void);
// 0x0000022D System.Nullable`1<UnityEngine.Vector4> Mirror.NetworkReaderExtensions::ReadVector4Nullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector4Nullable_m1303DE93C2EB13F32622A8B868B17610B8C4AD09 (void);
// 0x0000022E UnityEngine.Vector2Int Mirror.NetworkReaderExtensions::ReadVector2Int(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector2Int_mC9CEB6A103CD7C5DBCD8A944A57A59C0D1311F25 (void);
// 0x0000022F System.Nullable`1<UnityEngine.Vector2Int> Mirror.NetworkReaderExtensions::ReadVector2IntNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector2IntNullable_m698F001AADBF901CE9571E3AA5687DFC1DD65701 (void);
// 0x00000230 UnityEngine.Vector3Int Mirror.NetworkReaderExtensions::ReadVector3Int(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector3Int_m59BAA3EBC52DB1635EA840D23B9D4A011E480E3F (void);
// 0x00000231 System.Nullable`1<UnityEngine.Vector3Int> Mirror.NetworkReaderExtensions::ReadVector3IntNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector3IntNullable_mB88012F753982406CA6C49E8440318BFF784AF97 (void);
// 0x00000232 UnityEngine.Color Mirror.NetworkReaderExtensions::ReadColor(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadColor_mC5D200708B20F2ADC42224245960E2ED7E5DD27A (void);
// 0x00000233 System.Nullable`1<UnityEngine.Color> Mirror.NetworkReaderExtensions::ReadColorNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadColorNullable_mDA6AADFE45C4CE1364429EACA43199CB319C9065 (void);
// 0x00000234 UnityEngine.Color32 Mirror.NetworkReaderExtensions::ReadColor32(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadColor32_m0F0066C51CACC736B893D9F3C1D4324F87641BEF (void);
// 0x00000235 System.Nullable`1<UnityEngine.Color32> Mirror.NetworkReaderExtensions::ReadColor32Nullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadColor32Nullable_m95A7EDB77042A0B8D6D00D2C96E9A530DEA6AF8C (void);
// 0x00000236 UnityEngine.Quaternion Mirror.NetworkReaderExtensions::ReadQuaternion(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadQuaternion_m135F5C523703C700E6A266DA9718E44D160BB567 (void);
// 0x00000237 System.Nullable`1<UnityEngine.Quaternion> Mirror.NetworkReaderExtensions::ReadQuaternionNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadQuaternionNullable_mE4E31E56C486837C0EC9C6047B276C9452D02C9D (void);
// 0x00000238 UnityEngine.Rect Mirror.NetworkReaderExtensions::ReadRect(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadRect_mA4B7FDD8840C7E3A299614815C36EFB27232AB3C (void);
// 0x00000239 System.Nullable`1<UnityEngine.Rect> Mirror.NetworkReaderExtensions::ReadRectNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadRectNullable_m58AF30FAB6E523648BA18026AD02B220FBDDBC85 (void);
// 0x0000023A UnityEngine.Plane Mirror.NetworkReaderExtensions::ReadPlane(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadPlane_m2DA9573A8252F9B24A10E9E1AB448976D9963B96 (void);
// 0x0000023B System.Nullable`1<UnityEngine.Plane> Mirror.NetworkReaderExtensions::ReadPlaneNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadPlaneNullable_m252E55444808DDA4A5CEBCBE440E34728ECA5120 (void);
// 0x0000023C UnityEngine.Ray Mirror.NetworkReaderExtensions::ReadRay(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadRay_mBE12F756FAAA9395B88F69C6A43F8576921AB20C (void);
// 0x0000023D System.Nullable`1<UnityEngine.Ray> Mirror.NetworkReaderExtensions::ReadRayNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadRayNullable_mEE7ECB615AEFA818E73B366F681EA86595CA8F19 (void);
// 0x0000023E UnityEngine.Matrix4x4 Mirror.NetworkReaderExtensions::ReadMatrix4x4(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadMatrix4x4_mBB21ACB1A8610F3813CE4A37DBF1608CA31A0E2C (void);
// 0x0000023F System.Nullable`1<UnityEngine.Matrix4x4> Mirror.NetworkReaderExtensions::ReadMatrix4x4Nullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadMatrix4x4Nullable_m508241752BEC24CCF4BD45230613444685553D06 (void);
// 0x00000240 System.Guid Mirror.NetworkReaderExtensions::ReadGuid(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadGuid_mCFFAB7379132286F7C9CC70EC291F8B28EA08B0E (void);
// 0x00000241 System.Nullable`1<System.Guid> Mirror.NetworkReaderExtensions::ReadGuidNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadGuidNullable_m884FD11E39BB14010073AB443D46779317340927 (void);
// 0x00000242 Mirror.NetworkIdentity Mirror.NetworkReaderExtensions::ReadNetworkIdentity(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadNetworkIdentity_mFDB6779F9A77F88F9760FD9902EFFDF3331E62AE (void);
// 0x00000243 Mirror.NetworkBehaviour Mirror.NetworkReaderExtensions::ReadNetworkBehaviour(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadNetworkBehaviour_m6D724C97DE822B84C3FF75E80DA169D7C44E5E0B (void);
// 0x00000244 T Mirror.NetworkReaderExtensions::ReadNetworkBehaviour(Mirror.NetworkReader)
// 0x00000245 Mirror.NetworkBehaviour/NetworkBehaviourSyncVar Mirror.NetworkReaderExtensions::ReadNetworkBehaviourSyncVar(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadNetworkBehaviourSyncVar_mD3DCF91C73BB12C70E487EA4C4C85EAC62FE8A1A (void);
// 0x00000246 UnityEngine.Transform Mirror.NetworkReaderExtensions::ReadTransform(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadTransform_m56C2AB03C3891F0A72C1FC7153655E7AE4DCD6E8 (void);
// 0x00000247 UnityEngine.GameObject Mirror.NetworkReaderExtensions::ReadGameObject(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadGameObject_m037E8EDDA39F95DA70EE3226939F677F9E3A2EBD (void);
// 0x00000248 System.Collections.Generic.List`1<T> Mirror.NetworkReaderExtensions::ReadList(Mirror.NetworkReader)
// 0x00000249 T[] Mirror.NetworkReaderExtensions::ReadArray(Mirror.NetworkReader)
// 0x0000024A System.Uri Mirror.NetworkReaderExtensions::ReadUri(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUri_m9CB721F84C66F0749E586B02C4CE8E472F266C06 (void);
// 0x0000024B UnityEngine.Texture2D Mirror.NetworkReaderExtensions::ReadTexture2D(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadTexture2D_m5795D8D017B66A5ED4BDE243E306BD2B77A35EDC (void);
// 0x0000024C UnityEngine.Sprite Mirror.NetworkReaderExtensions::ReadSprite(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadSprite_mA5B19DCF570BA845B63AA79858FCFDF27DEAE040 (void);
// 0x0000024D System.Void Mirror.NetworkReaderExtensions::.cctor()
extern void NetworkReaderExtensions__cctor_mB413B3E2CD6EC4E8C1AF169D00937F17CD8CE6A2 (void);
// 0x0000024E Mirror.NetworkReaderPooled Mirror.NetworkReaderPool::GetReader(System.Byte[])
extern void NetworkReaderPool_GetReader_m284C1E11AA341F1B8BDEE1C4D401AD1154F33027 (void);
// 0x0000024F Mirror.NetworkReaderPooled Mirror.NetworkReaderPool::Get(System.Byte[])
extern void NetworkReaderPool_Get_m38A5E8CACDC8BE64A6EAA54168354FA381604EB2 (void);
// 0x00000250 Mirror.NetworkReaderPooled Mirror.NetworkReaderPool::GetReader(System.ArraySegment`1<System.Byte>)
extern void NetworkReaderPool_GetReader_m3B088EE21A28E6836FA65A6DF78F4FA60DC241C9 (void);
// 0x00000251 Mirror.NetworkReaderPooled Mirror.NetworkReaderPool::Get(System.ArraySegment`1<System.Byte>)
extern void NetworkReaderPool_Get_m0541C01FF0A785443A15550421E8F602494268E2 (void);
// 0x00000252 System.Void Mirror.NetworkReaderPool::Recycle(Mirror.NetworkReaderPooled)
extern void NetworkReaderPool_Recycle_mF73407194562635C193A6734F0BB3F974FA342C8 (void);
// 0x00000253 System.Void Mirror.NetworkReaderPool::Return(Mirror.NetworkReaderPooled)
extern void NetworkReaderPool_Return_mE2123C0BCBD7AAFD0AACC205825CB4186E337943 (void);
// 0x00000254 System.Void Mirror.NetworkReaderPool::.cctor()
extern void NetworkReaderPool__cctor_m2F327E4A2664A358E0B9055711495B2C9258D059 (void);
// 0x00000255 System.Void Mirror.NetworkReaderPool/<>c::.cctor()
extern void U3CU3Ec__cctor_mBA59E1F5D553F890D5D4F59546BE86F0939C1315 (void);
// 0x00000256 System.Void Mirror.NetworkReaderPool/<>c::.ctor()
extern void U3CU3Ec__ctor_m916C5368F0019DD3772A7BC5E54198159022F6D1 (void);
// 0x00000257 Mirror.NetworkReaderPooled Mirror.NetworkReaderPool/<>c::<.cctor>b__7_0()
extern void U3CU3Ec_U3C_cctorU3Eb__7_0_m9C3B1B12EE7DA18B70B08EEE869E0447C48B16E2 (void);
// 0x00000258 System.Void Mirror.PooledNetworkReader::.ctor(System.Byte[])
extern void PooledNetworkReader__ctor_mB96ECB000E753BCEEE9FE0919886A00B195163A4 (void);
// 0x00000259 System.Void Mirror.PooledNetworkReader::.ctor(System.ArraySegment`1<System.Byte>)
extern void PooledNetworkReader__ctor_m83AEA1199CEFDE5150BC6B7A76E1C2A55E53B166 (void);
// 0x0000025A System.Void Mirror.NetworkReaderPooled::.ctor(System.Byte[])
extern void NetworkReaderPooled__ctor_mF52AEE8ECFAFCDEB7925D9AE2304237047D72580 (void);
// 0x0000025B System.Void Mirror.NetworkReaderPooled::.ctor(System.ArraySegment`1<System.Byte>)
extern void NetworkReaderPooled__ctor_mD3C141EEC531CFB236165C76C1A7588A12A7B40E (void);
// 0x0000025C System.Void Mirror.NetworkReaderPooled::Dispose()
extern void NetworkReaderPooled_Dispose_m21DF73835F65C8F21C45A19CFE6EC69878155904 (void);
// 0x0000025D Mirror.NetworkConnectionToClient Mirror.NetworkServer::get_localConnection()
extern void NetworkServer_get_localConnection_m40B5A3EEB31B11F8CB674641AB821E15174DCBE8 (void);
// 0x0000025E System.Void Mirror.NetworkServer::set_localConnection(Mirror.NetworkConnectionToClient)
extern void NetworkServer_set_localConnection_m709B9720203BA015FD94BA59BE74D2A98EDD45AA (void);
// 0x0000025F System.Boolean Mirror.NetworkServer::get_localClientActive()
extern void NetworkServer_get_localClientActive_mF819059FAC02CCA8DCAEDEE3D7D9A0F4EB077846 (void);
// 0x00000260 System.Boolean Mirror.NetworkServer::get_active()
extern void NetworkServer_get_active_m02AA11D3F948B9BFA050605CD3E87E63E6550366 (void);
// 0x00000261 System.Void Mirror.NetworkServer::set_active(System.Boolean)
extern void NetworkServer_set_active_m4596680F3A5532993D5D3C6CE5B5FFC12E4813D2 (void);
// 0x00000262 System.Void Mirror.NetworkServer::Initialize()
extern void NetworkServer_Initialize_m6871B65AFA8F4F610FE036F189207FC915EC5A85 (void);
// 0x00000263 System.Void Mirror.NetworkServer::AddTransportHandlers()
extern void NetworkServer_AddTransportHandlers_m9765DEA51A841B7E0A4ABC03B946419331D170DA (void);
// 0x00000264 System.Void Mirror.NetworkServer::RemoveTransportHandlers()
extern void NetworkServer_RemoveTransportHandlers_m0D20717DD65BF9B4E41188613C50BE814D50060C (void);
// 0x00000265 System.Void Mirror.NetworkServer::ActivateHostScene()
extern void NetworkServer_ActivateHostScene_mF852F057E875C8833FE2FD26ED3C2F3A47A0D20F (void);
// 0x00000266 System.Void Mirror.NetworkServer::RegisterMessageHandlers()
extern void NetworkServer_RegisterMessageHandlers_mAF8745D6832E7EF985180BEC5B13613BFAE218D4 (void);
// 0x00000267 System.Void Mirror.NetworkServer::Listen(System.Int32)
extern void NetworkServer_Listen_m82139C5B724653F4EAD3496B1794C0242D4A8A7E (void);
// 0x00000268 System.Void Mirror.NetworkServer::CleanupSpawned()
extern void NetworkServer_CleanupSpawned_m73DDC30ED37098FEDC46FCA35694291263F8CE88 (void);
// 0x00000269 System.Void Mirror.NetworkServer::Shutdown()
extern void NetworkServer_Shutdown_m30813A8EE5A551909EF61EACA4D372749E5EB039 (void);
// 0x0000026A System.Boolean Mirror.NetworkServer::AddConnection(Mirror.NetworkConnectionToClient)
extern void NetworkServer_AddConnection_mD6EF1BF4690B0AC740924593054229238F49781F (void);
// 0x0000026B System.Boolean Mirror.NetworkServer::RemoveConnection(System.Int32)
extern void NetworkServer_RemoveConnection_mE06195F4F9FE6351A40444E5D137829B16E0E6F2 (void);
// 0x0000026C System.Void Mirror.NetworkServer::SetLocalConnection(Mirror.LocalConnectionToClient)
extern void NetworkServer_SetLocalConnection_mB56448E297F019C2A2399C1C375CDA4C4A1D4061 (void);
// 0x0000026D System.Void Mirror.NetworkServer::RemoveLocalConnection()
extern void NetworkServer_RemoveLocalConnection_m455A66246FA698E48D4D43F2B3EFE3BE9EC7A9C5 (void);
// 0x0000026E System.Boolean Mirror.NetworkServer::NoExternalConnections()
extern void NetworkServer_NoExternalConnections_m0ED59D7BC18A1ED70361C76C7D7F2DE111A87EB3 (void);
// 0x0000026F System.Boolean Mirror.NetworkServer::HasExternalConnections()
extern void NetworkServer_HasExternalConnections_m6468C27F72B226564A0EDC6A9A4D1F51E92541D4 (void);
// 0x00000270 System.Void Mirror.NetworkServer::SendToAll(T,System.Int32,System.Boolean)
// 0x00000271 System.Void Mirror.NetworkServer::SendToReady(T,System.Int32)
// 0x00000272 System.Void Mirror.NetworkServer::SendToObservers(Mirror.NetworkIdentity,T,System.Int32)
// 0x00000273 System.Void Mirror.NetworkServer::SendToReadyObservers(Mirror.NetworkIdentity,T,System.Boolean,System.Int32)
// 0x00000274 System.Void Mirror.NetworkServer::SendToReady(Mirror.NetworkIdentity,T,System.Boolean,System.Int32)
// 0x00000275 System.Void Mirror.NetworkServer::SendToReadyObservers(Mirror.NetworkIdentity,T,System.Int32)
// 0x00000276 System.Void Mirror.NetworkServer::SendToReady(Mirror.NetworkIdentity,T,System.Int32)
// 0x00000277 System.Void Mirror.NetworkServer::OnTransportConnected(System.Int32)
extern void NetworkServer_OnTransportConnected_m94F2EAABC0F07A1F36FD505605324AC1CC56DB94 (void);
// 0x00000278 System.Void Mirror.NetworkServer::OnConnected(Mirror.NetworkConnectionToClient)
extern void NetworkServer_OnConnected_m9C4A85871A17A3A86E365A39AF957C7AFBEA273E (void);
// 0x00000279 System.Boolean Mirror.NetworkServer::UnpackAndInvoke(Mirror.NetworkConnectionToClient,Mirror.NetworkReader,System.Int32)
extern void NetworkServer_UnpackAndInvoke_m4B6E7FEB9A01011B2AAFBF0E486E9D55FB543DC1 (void);
// 0x0000027A System.Void Mirror.NetworkServer::OnTransportData(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkServer_OnTransportData_m23B9C654356FC9103C7F57BD6EC4039D74255410 (void);
// 0x0000027B System.Void Mirror.NetworkServer::OnTransportDisconnected(System.Int32)
extern void NetworkServer_OnTransportDisconnected_m3F5D43F44545761D433E6BDE2FDAC6D6BEBC73B5 (void);
// 0x0000027C System.Void Mirror.NetworkServer::OnError(System.Int32,System.Exception)
extern void NetworkServer_OnError_mDA567F37A36B4CBA1E4B039407DBF31ECBEA4AA5 (void);
// 0x0000027D System.Void Mirror.NetworkServer::RegisterHandler(System.Action`2<Mirror.NetworkConnectionToClient,T>,System.Boolean)
// 0x0000027E System.Void Mirror.NetworkServer::RegisterHandler(System.Action`3<Mirror.NetworkConnectionToClient,T,System.Int32>,System.Boolean)
// 0x0000027F System.Void Mirror.NetworkServer::ReplaceHandler(System.Action`2<Mirror.NetworkConnectionToClient,T>,System.Boolean)
// 0x00000280 System.Void Mirror.NetworkServer::ReplaceHandler(System.Action`1<T>,System.Boolean)
// 0x00000281 System.Void Mirror.NetworkServer::UnregisterHandler()
// 0x00000282 System.Void Mirror.NetworkServer::ClearHandlers()
extern void NetworkServer_ClearHandlers_m0C0F8BFF5E1284DD5B61F28393FD5C5C4E03E5AF (void);
// 0x00000283 System.Boolean Mirror.NetworkServer::GetNetworkIdentity(UnityEngine.GameObject,Mirror.NetworkIdentity&)
extern void NetworkServer_GetNetworkIdentity_mCBE394DAD21D016B63C30823867ECF523D0C0FB1 (void);
// 0x00000284 System.Void Mirror.NetworkServer::DisconnectAll()
extern void NetworkServer_DisconnectAll_mDDA6465FF641ADDA7AD023B5A9D21519AB3398AA (void);
// 0x00000285 System.Boolean Mirror.NetworkServer::AddPlayerForConnection(Mirror.NetworkConnectionToClient,UnityEngine.GameObject)
extern void NetworkServer_AddPlayerForConnection_mEDEF1DD18D9C456FCAE4E3E2EE9619EFBFCD4BBD (void);
// 0x00000286 System.Boolean Mirror.NetworkServer::AddPlayerForConnection(Mirror.NetworkConnectionToClient,UnityEngine.GameObject,System.Guid)
extern void NetworkServer_AddPlayerForConnection_mD565274C11AF7B36CCFEFAA8587ABC39F938ECFB (void);
// 0x00000287 System.Boolean Mirror.NetworkServer::ReplacePlayerForConnection(Mirror.NetworkConnectionToClient,UnityEngine.GameObject,System.Boolean)
extern void NetworkServer_ReplacePlayerForConnection_mB7648580A5E9A9C61007F281014225D02A1097F6 (void);
// 0x00000288 System.Boolean Mirror.NetworkServer::ReplacePlayerForConnection(Mirror.NetworkConnectionToClient,UnityEngine.GameObject,System.Guid,System.Boolean)
extern void NetworkServer_ReplacePlayerForConnection_m20E3D0682EBB6F52E314884534942A55CF2BBD31 (void);
// 0x00000289 System.Void Mirror.NetworkServer::SetClientReady(Mirror.NetworkConnectionToClient)
extern void NetworkServer_SetClientReady_mFE7E42EDC31007D6C9568A7F61A4711B0CA3D0AA (void);
// 0x0000028A System.Void Mirror.NetworkServer::SetClientNotReady(Mirror.NetworkConnectionToClient)
extern void NetworkServer_SetClientNotReady_m11D97BC7786F9B8681B8CC26669B81C242A9B8F0 (void);
// 0x0000028B System.Void Mirror.NetworkServer::SetAllClientsNotReady()
extern void NetworkServer_SetAllClientsNotReady_m07D99564B0F1F79F8772EBACDC9AACC020075B0F (void);
// 0x0000028C System.Void Mirror.NetworkServer::OnClientReadyMessage(Mirror.NetworkConnectionToClient,Mirror.ReadyMessage)
extern void NetworkServer_OnClientReadyMessage_m3F3511144416E643FA74ECDB280764DF9771979A (void);
// 0x0000028D System.Void Mirror.NetworkServer::ShowForConnection(Mirror.NetworkIdentity,Mirror.NetworkConnection)
extern void NetworkServer_ShowForConnection_mD80C701D4A442A1522FD00CF32B43D7AD09A58A0 (void);
// 0x0000028E System.Void Mirror.NetworkServer::HideForConnection(Mirror.NetworkIdentity,Mirror.NetworkConnection)
extern void NetworkServer_HideForConnection_mB8F94AB7B27FAD53D442340EB3D234D840BBA876 (void);
// 0x0000028F System.Void Mirror.NetworkServer::RemovePlayerForConnection(Mirror.NetworkConnection,System.Boolean)
extern void NetworkServer_RemovePlayerForConnection_mDBCB11BCF28386DF483FA2E5D1C44CAD4AF70855 (void);
// 0x00000290 System.Void Mirror.NetworkServer::OnCommandMessage(Mirror.NetworkConnectionToClient,Mirror.CommandMessage,System.Int32)
extern void NetworkServer_OnCommandMessage_m7D5C5D2861CE44343722A2E62450668DF28AFDF6 (void);
// 0x00000291 System.ArraySegment`1<System.Byte> Mirror.NetworkServer::CreateSpawnMessagePayload(System.Boolean,Mirror.NetworkIdentity,Mirror.NetworkWriterPooled,Mirror.NetworkWriterPooled)
extern void NetworkServer_CreateSpawnMessagePayload_mAA41C9557E0403B56221B1AAFA86208DC58B959B (void);
// 0x00000292 System.Void Mirror.NetworkServer::SendSpawnMessage(Mirror.NetworkIdentity,Mirror.NetworkConnection)
extern void NetworkServer_SendSpawnMessage_m981BFA48A52392FA34606B46FB0E99D916F5C0C9 (void);
// 0x00000293 System.Void Mirror.NetworkServer::SendChangeOwnerMessage(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void NetworkServer_SendChangeOwnerMessage_mD12DFD8BCA1C742961402DD3FC0FE8AFDFE8F640 (void);
// 0x00000294 System.Void Mirror.NetworkServer::SpawnObject(UnityEngine.GameObject,Mirror.NetworkConnection)
extern void NetworkServer_SpawnObject_mAE23DBB87E27E4BA0FDA61F7E359A2EC2437EFB2 (void);
// 0x00000295 System.Void Mirror.NetworkServer::Spawn(UnityEngine.GameObject,Mirror.NetworkConnection)
extern void NetworkServer_Spawn_mD8A2B40ED6ED7B29F042AEB364DE9B42A0B7B4D5 (void);
// 0x00000296 System.Void Mirror.NetworkServer::Spawn(UnityEngine.GameObject,UnityEngine.GameObject)
extern void NetworkServer_Spawn_m97D3AE543876CBF3E01B42A97A2B6D17828AB794 (void);
// 0x00000297 System.Void Mirror.NetworkServer::Spawn(UnityEngine.GameObject,System.Guid,Mirror.NetworkConnection)
extern void NetworkServer_Spawn_mB086C140CE33E27ACE4B1B89A76533B0F9D60A22 (void);
// 0x00000298 System.Boolean Mirror.NetworkServer::ValidateSceneObject(Mirror.NetworkIdentity)
extern void NetworkServer_ValidateSceneObject_m8CDD7FC8B1ED711DDB402FB9E26673E2CF565367 (void);
// 0x00000299 System.Boolean Mirror.NetworkServer::SpawnObjects()
extern void NetworkServer_SpawnObjects_m42C2CDF65867CA85100C1C290E532C167B8F45E2 (void);
// 0x0000029A System.Void Mirror.NetworkServer::Respawn(Mirror.NetworkIdentity)
extern void NetworkServer_Respawn_m9B3E4BE2F4A7B2B91E527F26E40F0FF63605D5CB (void);
// 0x0000029B System.Void Mirror.NetworkServer::SpawnObserversForConnection(Mirror.NetworkConnectionToClient)
extern void NetworkServer_SpawnObserversForConnection_m46F922FAE51EA2161AAC8338B727C5B61E798D9C (void);
// 0x0000029C System.Void Mirror.NetworkServer::UnSpawn(UnityEngine.GameObject)
extern void NetworkServer_UnSpawn_m37AA67C164C614C83BFA7ED3E8804B0EEE676FC6 (void);
// 0x0000029D System.Void Mirror.NetworkServer::DestroyPlayerForConnection(Mirror.NetworkConnectionToClient)
extern void NetworkServer_DestroyPlayerForConnection_mEA5F898E778D7A91B6023D6DD07AC379D491D50B (void);
// 0x0000029E System.Void Mirror.NetworkServer::DestroyObject(Mirror.NetworkIdentity,Mirror.NetworkServer/DestroyMode)
extern void NetworkServer_DestroyObject_m85D899225F139DD18E09E82E1FCB966E94E4216C (void);
// 0x0000029F System.Void Mirror.NetworkServer::DestroyObject(UnityEngine.GameObject,Mirror.NetworkServer/DestroyMode)
extern void NetworkServer_DestroyObject_m8B9D77FFA8A27D82DFE1BC7726E9C2305905A676 (void);
// 0x000002A0 System.Void Mirror.NetworkServer::Destroy(UnityEngine.GameObject)
extern void NetworkServer_Destroy_m8F0C7AAD5958C88297A46B039C4E0EF3CE99043D (void);
// 0x000002A1 System.Void Mirror.NetworkServer::AddAllReadyServerConnectionsToObservers(Mirror.NetworkIdentity)
extern void NetworkServer_AddAllReadyServerConnectionsToObservers_m65E19A5724975F1302178308915BFBA8FB6BF7BB (void);
// 0x000002A2 System.Void Mirror.NetworkServer::RebuildObserversDefault(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkServer_RebuildObserversDefault_m5C50544D7EF5C55ED9DBF35F1BEB5671E28D590A (void);
// 0x000002A3 System.Void Mirror.NetworkServer::RebuildObserversCustom(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkServer_RebuildObserversCustom_mDF2B659ED84D57D583758EA8845179C4CFDABA13 (void);
// 0x000002A4 System.Void Mirror.NetworkServer::RebuildObservers(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkServer_RebuildObservers_m6B7D242A0FDDBAD4CD37E9D2FBB169DDF91E9875 (void);
// 0x000002A5 Mirror.NetworkWriter Mirror.NetworkServer::GetEntitySerializationForConnection(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void NetworkServer_GetEntitySerializationForConnection_mDD77A126FB1E080BECADC02FA40BD7895A5A2EEF (void);
// 0x000002A6 System.Void Mirror.NetworkServer::BroadcastToConnection(Mirror.NetworkConnectionToClient)
extern void NetworkServer_BroadcastToConnection_mB9BC1439CFBD326724CF8A92B0FC961896BC145A (void);
// 0x000002A7 System.Void Mirror.NetworkServer::Broadcast()
extern void NetworkServer_Broadcast_mBCF5790CDB8DFE1A4154CE642C6DA40A4384B186 (void);
// 0x000002A8 System.Void Mirror.NetworkServer::NetworkEarlyUpdate()
extern void NetworkServer_NetworkEarlyUpdate_mC91E0A4BF9B7CD237F98D58408FE16BCEA771680 (void);
// 0x000002A9 System.Void Mirror.NetworkServer::NetworkLateUpdate()
extern void NetworkServer_NetworkLateUpdate_m718B5D1E6624E050F39EC8C919B414DF133AB2E4 (void);
// 0x000002AA System.Void Mirror.NetworkServer::.cctor()
extern void NetworkServer__cctor_m77C0CD1A02A7F1E5EA424823ACEB853BE0E5F74D (void);
// 0x000002AB System.Void Mirror.NetworkServer/<>c__DisplayClass51_0`1::.ctor()
// 0x000002AC System.Void Mirror.NetworkServer/<>c__DisplayClass51_0`1::<ReplaceHandler>b__0(Mirror.NetworkConnectionToClient,T)
// 0x000002AD System.Void Mirror.NetworkStartPosition::Awake()
extern void NetworkStartPosition_Awake_mC9554712794D10914CB8E0595B8854B990ACD7A5 (void);
// 0x000002AE System.Void Mirror.NetworkStartPosition::OnDestroy()
extern void NetworkStartPosition_OnDestroy_mF14AB964069CAD898B311772B863D99534262741 (void);
// 0x000002AF System.Void Mirror.NetworkStartPosition::.ctor()
extern void NetworkStartPosition__ctor_m783B168BD36FAA13CC631E4897FB5C5ACC783BF4 (void);
// 0x000002B0 System.Double Mirror.NetworkTime::get_localTime()
extern void NetworkTime_get_localTime_m48FFA7BD2A87D9EC965782C7484F66D9A042FB89 (void);
// 0x000002B1 System.Double Mirror.NetworkTime::get_time()
extern void NetworkTime_get_time_m44D7378ABC9CB592F924B900BD9E0F65643EEE3E (void);
// 0x000002B2 System.Double Mirror.NetworkTime::get_timeVariance()
extern void NetworkTime_get_timeVariance_mA61BCBE156273D70A378DCA86D5B83CC9C3A403E (void);
// 0x000002B3 System.Double Mirror.NetworkTime::get_timeStandardDeviation()
extern void NetworkTime_get_timeStandardDeviation_m47FEBDA6C53D5DB7C3FDCDFAC137FBA2467C7768 (void);
// 0x000002B4 System.Double Mirror.NetworkTime::get_offset()
extern void NetworkTime_get_offset_m41902185659B32B40F184129F46845A70F54F4D2 (void);
// 0x000002B5 System.Double Mirror.NetworkTime::get_rtt()
extern void NetworkTime_get_rtt_m63EB7A06EADCDE66B98BB27386ED0E3DD4A26E5D (void);
// 0x000002B6 System.Double Mirror.NetworkTime::get_rttVariance()
extern void NetworkTime_get_rttVariance_m9BCF4BD5EB001DFC67EBF45ADF98F3234F5FD0A3 (void);
// 0x000002B7 System.Double Mirror.NetworkTime::get_rttStandardDeviation()
extern void NetworkTime_get_rttStandardDeviation_m0D0E0219C4A261AF5688955085AA18630B2BE9EA (void);
// 0x000002B8 System.Void Mirror.NetworkTime::ResetStatics()
extern void NetworkTime_ResetStatics_m2DC91BF81E155310FA18829B21D1DD388AE9BC43 (void);
// 0x000002B9 System.Void Mirror.NetworkTime::UpdateClient()
extern void NetworkTime_UpdateClient_m5011BE5B67EB82E7725B546CE327C36B0608EA1C (void);
// 0x000002BA System.Void Mirror.NetworkTime::OnServerPing(Mirror.NetworkConnectionToClient,Mirror.NetworkPingMessage)
extern void NetworkTime_OnServerPing_m3C4C1E1D94B3B73294A49CD5A632E38AD8082631 (void);
// 0x000002BB System.Void Mirror.NetworkTime::OnClientPong(Mirror.NetworkPongMessage)
extern void NetworkTime_OnClientPong_mF053F60935DBE9188E5D117C898A273B16229447 (void);
// 0x000002BC System.Void Mirror.NetworkTime::.cctor()
extern void NetworkTime__cctor_mFD9C036DE68DFDC4CA95FCD62E83D51315407AFC (void);
// 0x000002BD System.Void Mirror.NetworkWriter::Reset()
extern void NetworkWriter_Reset_mAAB03983E0061CB0AAC0EEEFBDB8D777CB1B191D (void);
// 0x000002BE System.Void Mirror.NetworkWriter::EnsureCapacity(System.Int32)
extern void NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3 (void);
// 0x000002BF System.Byte[] Mirror.NetworkWriter::ToArray()
extern void NetworkWriter_ToArray_m3E85E9234DA5E83A2574340D0F8AE06AEB6E4184 (void);
// 0x000002C0 System.ArraySegment`1<System.Byte> Mirror.NetworkWriter::ToArraySegment()
extern void NetworkWriter_ToArraySegment_m35FBADDD2990B92709B5B3277F7A982140189135 (void);
// 0x000002C1 System.Void Mirror.NetworkWriter::WriteBlittable(T)
// 0x000002C2 System.Void Mirror.NetworkWriter::WriteBlittableNullable(System.Nullable`1<T>)
// 0x000002C3 System.Void Mirror.NetworkWriter::WriteByte(System.Byte)
extern void NetworkWriter_WriteByte_m7F7BB1B62F8DCB5BE680355A4BCAFA3BD44E54C0 (void);
// 0x000002C4 System.Void Mirror.NetworkWriter::WriteBytes(System.Byte[],System.Int32,System.Int32)
extern void NetworkWriter_WriteBytes_m0F3058BA3B1B973C3D99B647DA231D9E82AFEDEC (void);
// 0x000002C5 System.Void Mirror.NetworkWriter::Write(T)
// 0x000002C6 System.Void Mirror.NetworkWriter::.ctor()
extern void NetworkWriter__ctor_mD67C3954391867E0C776DCF1ED04C711AFEAAC5E (void);
// 0x000002C7 System.Void Mirror.NetworkWriterExtensions::WriteByte(Mirror.NetworkWriter,System.Byte)
extern void NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4 (void);
// 0x000002C8 System.Void Mirror.NetworkWriterExtensions::WriteByteNullable(Mirror.NetworkWriter,System.Nullable`1<System.Byte>)
extern void NetworkWriterExtensions_WriteByteNullable_mE34CDDB5354D7536941F1AB0DB0EFA4E3269C5A2 (void);
// 0x000002C9 System.Void Mirror.NetworkWriterExtensions::WriteSByte(Mirror.NetworkWriter,System.SByte)
extern void NetworkWriterExtensions_WriteSByte_m777D700EE0D8256617BE1128DE65C2DEBF674EB3 (void);
// 0x000002CA System.Void Mirror.NetworkWriterExtensions::WriteSByteNullable(Mirror.NetworkWriter,System.Nullable`1<System.SByte>)
extern void NetworkWriterExtensions_WriteSByteNullable_m3330A77E2E4D2AC1B90BF53BC8150063BB6F8B30 (void);
// 0x000002CB System.Void Mirror.NetworkWriterExtensions::WriteChar(Mirror.NetworkWriter,System.Char)
extern void NetworkWriterExtensions_WriteChar_m79E8B11FA260E5C83FAAB385A039B9B73F4E15E7 (void);
// 0x000002CC System.Void Mirror.NetworkWriterExtensions::WriteCharNullable(Mirror.NetworkWriter,System.Nullable`1<System.Char>)
extern void NetworkWriterExtensions_WriteCharNullable_m3D59A08FC508A0A1469115A766844A3DC3F1E420 (void);
// 0x000002CD System.Void Mirror.NetworkWriterExtensions::WriteBool(Mirror.NetworkWriter,System.Boolean)
extern void NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45 (void);
// 0x000002CE System.Void Mirror.NetworkWriterExtensions::WriteBoolNullable(Mirror.NetworkWriter,System.Nullable`1<System.Boolean>)
extern void NetworkWriterExtensions_WriteBoolNullable_mFB1ADF7E798F7991680382003FA5584DA972EBBE (void);
// 0x000002CF System.Void Mirror.NetworkWriterExtensions::WriteShort(Mirror.NetworkWriter,System.Int16)
extern void NetworkWriterExtensions_WriteShort_m8593C0C47C9EADF1A65AA97BCBA9C15BF3739089 (void);
// 0x000002D0 System.Void Mirror.NetworkWriterExtensions::WriteShortNullable(Mirror.NetworkWriter,System.Nullable`1<System.Int16>)
extern void NetworkWriterExtensions_WriteShortNullable_mB84CEDD1AF6243DABA5C235B4013AD5A1801BE6B (void);
// 0x000002D1 System.Void Mirror.NetworkWriterExtensions::WriteUShort(Mirror.NetworkWriter,System.UInt16)
extern void NetworkWriterExtensions_WriteUShort_m4AEC8147034117F9EB131043089577CD2DB42DB4 (void);
// 0x000002D2 System.Void Mirror.NetworkWriterExtensions::WriteUShortNullable(Mirror.NetworkWriter,System.Nullable`1<System.UInt16>)
extern void NetworkWriterExtensions_WriteUShortNullable_mE77F289B55D295E545826AAEA6CAAFCD26FA11A2 (void);
// 0x000002D3 System.Void Mirror.NetworkWriterExtensions::WriteInt(Mirror.NetworkWriter,System.Int32)
extern void NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12 (void);
// 0x000002D4 System.Void Mirror.NetworkWriterExtensions::WriteIntNullable(Mirror.NetworkWriter,System.Nullable`1<System.Int32>)
extern void NetworkWriterExtensions_WriteIntNullable_mD476FA9C8F66E723E823733776EC840B4DCE6FAF (void);
// 0x000002D5 System.Void Mirror.NetworkWriterExtensions::WriteUInt(Mirror.NetworkWriter,System.UInt32)
extern void NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1 (void);
// 0x000002D6 System.Void Mirror.NetworkWriterExtensions::WriteUIntNullable(Mirror.NetworkWriter,System.Nullable`1<System.UInt32>)
extern void NetworkWriterExtensions_WriteUIntNullable_m5C618D0B2F565D4C20CACAE51E3D8A9AAA7EC3A4 (void);
// 0x000002D7 System.Void Mirror.NetworkWriterExtensions::WriteLong(Mirror.NetworkWriter,System.Int64)
extern void NetworkWriterExtensions_WriteLong_m631751934892884B4E8B0FAF18BC616ADBAE1E90 (void);
// 0x000002D8 System.Void Mirror.NetworkWriterExtensions::WriteLongNullable(Mirror.NetworkWriter,System.Nullable`1<System.Int64>)
extern void NetworkWriterExtensions_WriteLongNullable_mE9A8A93B2C853063398E244A3FA8342BB51D0C5C (void);
// 0x000002D9 System.Void Mirror.NetworkWriterExtensions::WriteULong(Mirror.NetworkWriter,System.UInt64)
extern void NetworkWriterExtensions_WriteULong_mC0AE4801C58209EF02B73E3B353100B3AB95D28C (void);
// 0x000002DA System.Void Mirror.NetworkWriterExtensions::WriteULongNullable(Mirror.NetworkWriter,System.Nullable`1<System.UInt64>)
extern void NetworkWriterExtensions_WriteULongNullable_m5CD68A058B65F0F3B8729DEE05D43A94E222055E (void);
// 0x000002DB System.Void Mirror.NetworkWriterExtensions::WriteFloat(Mirror.NetworkWriter,System.Single)
extern void NetworkWriterExtensions_WriteFloat_mA3AEF60E8288F55D5A3365AA0E4730AFFC231050 (void);
// 0x000002DC System.Void Mirror.NetworkWriterExtensions::WriteFloatNullable(Mirror.NetworkWriter,System.Nullable`1<System.Single>)
extern void NetworkWriterExtensions_WriteFloatNullable_m0F2D06A7FA1A84F3F3C54537D6A3ABAB3206585F (void);
// 0x000002DD System.Void Mirror.NetworkWriterExtensions::WriteDouble(Mirror.NetworkWriter,System.Double)
extern void NetworkWriterExtensions_WriteDouble_m0475F5FB9E1D69D60501C7158AD3431680BC1BDB (void);
// 0x000002DE System.Void Mirror.NetworkWriterExtensions::WriteDoubleNullable(Mirror.NetworkWriter,System.Nullable`1<System.Double>)
extern void NetworkWriterExtensions_WriteDoubleNullable_m91EB95539CA35FAA383E01BEFA894A346A218ACA (void);
// 0x000002DF System.Void Mirror.NetworkWriterExtensions::WriteDecimal(Mirror.NetworkWriter,System.Decimal)
extern void NetworkWriterExtensions_WriteDecimal_mFDD008D98CD77D9B4E63EF9AE0421FABAE70F483 (void);
// 0x000002E0 System.Void Mirror.NetworkWriterExtensions::WriteDecimalNullable(Mirror.NetworkWriter,System.Nullable`1<System.Decimal>)
extern void NetworkWriterExtensions_WriteDecimalNullable_mACB5EA0A0661A694C8521C3C245AC9EE3ECAE1DA (void);
// 0x000002E1 System.Void Mirror.NetworkWriterExtensions::WriteString(Mirror.NetworkWriter,System.String)
extern void NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98 (void);
// 0x000002E2 System.Void Mirror.NetworkWriterExtensions::WriteBytesAndSizeSegment(Mirror.NetworkWriter,System.ArraySegment`1<System.Byte>)
extern void NetworkWriterExtensions_WriteBytesAndSizeSegment_mD7FCDC44AB0313C3ED0AFBFEC77366B6947672AC (void);
// 0x000002E3 System.Void Mirror.NetworkWriterExtensions::WriteBytesAndSize(Mirror.NetworkWriter,System.Byte[])
extern void NetworkWriterExtensions_WriteBytesAndSize_mD5E8FA492EACCF5C68D0E76D84C20689CDAA0F27 (void);
// 0x000002E4 System.Void Mirror.NetworkWriterExtensions::WriteBytesAndSize(Mirror.NetworkWriter,System.Byte[],System.Int32,System.Int32)
extern void NetworkWriterExtensions_WriteBytesAndSize_m3A29964C9A4F7D85B49358324A238EADA7DE57BA (void);
// 0x000002E5 System.Void Mirror.NetworkWriterExtensions::WriteArraySegment(Mirror.NetworkWriter,System.ArraySegment`1<T>)
// 0x000002E6 System.Void Mirror.NetworkWriterExtensions::WriteVector2(Mirror.NetworkWriter,UnityEngine.Vector2)
extern void NetworkWriterExtensions_WriteVector2_m5C9C94ECCE2643B670009D710BA8D6A2434F8BA5 (void);
// 0x000002E7 System.Void Mirror.NetworkWriterExtensions::WriteVector2Nullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector2>)
extern void NetworkWriterExtensions_WriteVector2Nullable_mF82E294E5D5AB3D06DEA7404DAD4C9430D89C728 (void);
// 0x000002E8 System.Void Mirror.NetworkWriterExtensions::WriteVector3(Mirror.NetworkWriter,UnityEngine.Vector3)
extern void NetworkWriterExtensions_WriteVector3_mB3C2B2F2D3C9874F883C12813FB20B9D5AABC882 (void);
// 0x000002E9 System.Void Mirror.NetworkWriterExtensions::WriteVector3Nullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkWriterExtensions_WriteVector3Nullable_mD35B4E68313CFC87EF7B80823D9F0502C4D63E62 (void);
// 0x000002EA System.Void Mirror.NetworkWriterExtensions::WriteVector4(Mirror.NetworkWriter,UnityEngine.Vector4)
extern void NetworkWriterExtensions_WriteVector4_m710FEA287EE2C56C2C7DA468B394D23FE2424023 (void);
// 0x000002EB System.Void Mirror.NetworkWriterExtensions::WriteVector4Nullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector4>)
extern void NetworkWriterExtensions_WriteVector4Nullable_m225B849A988CD2861387E8368F780E90E3D956CD (void);
// 0x000002EC System.Void Mirror.NetworkWriterExtensions::WriteVector2Int(Mirror.NetworkWriter,UnityEngine.Vector2Int)
extern void NetworkWriterExtensions_WriteVector2Int_m0099C36CFAF8015034E1CBC4CFCD7623543C758F (void);
// 0x000002ED System.Void Mirror.NetworkWriterExtensions::WriteVector2IntNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector2Int>)
extern void NetworkWriterExtensions_WriteVector2IntNullable_mDAC15DF3BA2A0FDA9705D0D35A3C4F486D2DDFA5 (void);
// 0x000002EE System.Void Mirror.NetworkWriterExtensions::WriteVector3Int(Mirror.NetworkWriter,UnityEngine.Vector3Int)
extern void NetworkWriterExtensions_WriteVector3Int_m2A6D52133117098B0C8A65520CBEFF8C4297B47B (void);
// 0x000002EF System.Void Mirror.NetworkWriterExtensions::WriteVector3IntNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector3Int>)
extern void NetworkWriterExtensions_WriteVector3IntNullable_m6EDE27130713A9C3A4012DCE58D53E488EE7E36A (void);
// 0x000002F0 System.Void Mirror.NetworkWriterExtensions::WriteColor(Mirror.NetworkWriter,UnityEngine.Color)
extern void NetworkWriterExtensions_WriteColor_m142E05754268CB4F297199994A61605D0FF1D9A2 (void);
// 0x000002F1 System.Void Mirror.NetworkWriterExtensions::WriteColorNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Color>)
extern void NetworkWriterExtensions_WriteColorNullable_m0B728B0EF504CC3FB0CE87FAD1505794AB82CDBC (void);
// 0x000002F2 System.Void Mirror.NetworkWriterExtensions::WriteColor32(Mirror.NetworkWriter,UnityEngine.Color32)
extern void NetworkWriterExtensions_WriteColor32_m123810E64991275156516FBB8AD2CFA67A7C3B7B (void);
// 0x000002F3 System.Void Mirror.NetworkWriterExtensions::WriteColor32Nullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Color32>)
extern void NetworkWriterExtensions_WriteColor32Nullable_m3931F587C14E96A05B25E3446B6F7AE6D81115C0 (void);
// 0x000002F4 System.Void Mirror.NetworkWriterExtensions::WriteQuaternion(Mirror.NetworkWriter,UnityEngine.Quaternion)
extern void NetworkWriterExtensions_WriteQuaternion_mFC2E046965F6BA1B694218D5E60E26807974ACBA (void);
// 0x000002F5 System.Void Mirror.NetworkWriterExtensions::WriteQuaternionNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Quaternion>)
extern void NetworkWriterExtensions_WriteQuaternionNullable_m7E68536A12BD33C4E0D063841FCB8B3319CE546A (void);
// 0x000002F6 System.Void Mirror.NetworkWriterExtensions::WriteRect(Mirror.NetworkWriter,UnityEngine.Rect)
extern void NetworkWriterExtensions_WriteRect_m52D47BD93F73E06FB131C75A78127E3CC9073093 (void);
// 0x000002F7 System.Void Mirror.NetworkWriterExtensions::WriteRectNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Rect>)
extern void NetworkWriterExtensions_WriteRectNullable_mD90246AB0237C3D9B3D669CEDFF1548D9DE26364 (void);
// 0x000002F8 System.Void Mirror.NetworkWriterExtensions::WritePlane(Mirror.NetworkWriter,UnityEngine.Plane)
extern void NetworkWriterExtensions_WritePlane_m5BF0BAF633E94AAE16D6D7E44B78E474E601077F (void);
// 0x000002F9 System.Void Mirror.NetworkWriterExtensions::WritePlaneNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Plane>)
extern void NetworkWriterExtensions_WritePlaneNullable_m1D7DE145477804CB9A5F078D51F2C31408A9CA2D (void);
// 0x000002FA System.Void Mirror.NetworkWriterExtensions::WriteRay(Mirror.NetworkWriter,UnityEngine.Ray)
extern void NetworkWriterExtensions_WriteRay_mE3C68E64E43515730710198FF05734D077BDBEBA (void);
// 0x000002FB System.Void Mirror.NetworkWriterExtensions::WriteRayNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Ray>)
extern void NetworkWriterExtensions_WriteRayNullable_m4C9EE3A8F4B5A24523EE0A02827A481F300A7C36 (void);
// 0x000002FC System.Void Mirror.NetworkWriterExtensions::WriteMatrix4x4(Mirror.NetworkWriter,UnityEngine.Matrix4x4)
extern void NetworkWriterExtensions_WriteMatrix4x4_m19A3B92281557AC6E231E3B5C663ACB8366CDFE2 (void);
// 0x000002FD System.Void Mirror.NetworkWriterExtensions::WriteMatrix4x4Nullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Matrix4x4>)
extern void NetworkWriterExtensions_WriteMatrix4x4Nullable_mEBA4E2383B008575F3957933958627DEE30465D7 (void);
// 0x000002FE System.Void Mirror.NetworkWriterExtensions::WriteGuid(Mirror.NetworkWriter,System.Guid)
extern void NetworkWriterExtensions_WriteGuid_mFF6C6A1BC90A9A7BBC9C179FA6FC25753689D3F9 (void);
// 0x000002FF System.Void Mirror.NetworkWriterExtensions::WriteGuidNullable(Mirror.NetworkWriter,System.Nullable`1<System.Guid>)
extern void NetworkWriterExtensions_WriteGuidNullable_m19C9499197D2DEEB57A46D81FC9993D6EBFA14D9 (void);
// 0x00000300 System.Void Mirror.NetworkWriterExtensions::WriteNetworkIdentity(Mirror.NetworkWriter,Mirror.NetworkIdentity)
extern void NetworkWriterExtensions_WriteNetworkIdentity_m670598EE39418EC82E5A35DD60EBDA69D7B8A74A (void);
// 0x00000301 System.Void Mirror.NetworkWriterExtensions::WriteNetworkBehaviour(Mirror.NetworkWriter,Mirror.NetworkBehaviour)
extern void NetworkWriterExtensions_WriteNetworkBehaviour_mDEE6FB11729AF7833D749E1C0573A559113E26A3 (void);
// 0x00000302 System.Void Mirror.NetworkWriterExtensions::WriteTransform(Mirror.NetworkWriter,UnityEngine.Transform)
extern void NetworkWriterExtensions_WriteTransform_m2F65EBB30598661EE20259C40E58691589593CD4 (void);
// 0x00000303 System.Void Mirror.NetworkWriterExtensions::WriteGameObject(Mirror.NetworkWriter,UnityEngine.GameObject)
extern void NetworkWriterExtensions_WriteGameObject_m1B7DE5CB70EE416C894BA361CE421473734456AD (void);
// 0x00000304 System.Void Mirror.NetworkWriterExtensions::WriteList(Mirror.NetworkWriter,System.Collections.Generic.List`1<T>)
// 0x00000305 System.Void Mirror.NetworkWriterExtensions::WriteArray(Mirror.NetworkWriter,T[])
// 0x00000306 System.Void Mirror.NetworkWriterExtensions::WriteUri(Mirror.NetworkWriter,System.Uri)
extern void NetworkWriterExtensions_WriteUri_mFB6E40C094D853A44F750835EF778B567546D775 (void);
// 0x00000307 System.Void Mirror.NetworkWriterExtensions::WriteTexture2D(Mirror.NetworkWriter,UnityEngine.Texture2D)
extern void NetworkWriterExtensions_WriteTexture2D_mFA5FE217BF0E9D2F1CBF3A50C7A0B9C689B79782 (void);
// 0x00000308 System.Void Mirror.NetworkWriterExtensions::WriteSprite(Mirror.NetworkWriter,UnityEngine.Sprite)
extern void NetworkWriterExtensions_WriteSprite_mC849B7B1044D0DC1989BC8F5A77DD93CDB7B0C82 (void);
// 0x00000309 System.Void Mirror.NetworkWriterExtensions::.cctor()
extern void NetworkWriterExtensions__cctor_m4DEF722CAC01E27861468F3E7DF75E30DA6053DF (void);
// 0x0000030A Mirror.NetworkWriterPooled Mirror.NetworkWriterPool::GetWriter()
extern void NetworkWriterPool_GetWriter_mF0156A907C671F91742AB03A6B28F429ECB98450 (void);
// 0x0000030B Mirror.NetworkWriterPooled Mirror.NetworkWriterPool::Get()
extern void NetworkWriterPool_Get_m646E4F6C23D26C7972C41F3896E77BEB6B63D3FA (void);
// 0x0000030C System.Void Mirror.NetworkWriterPool::Recycle(Mirror.NetworkWriterPooled)
extern void NetworkWriterPool_Recycle_mEABD0D229387A34AD7AC58CE838FAF8C765DCB62 (void);
// 0x0000030D System.Void Mirror.NetworkWriterPool::Return(Mirror.NetworkWriterPooled)
extern void NetworkWriterPool_Return_mFA32A7C1E0D86429F3B60364445BC6867894D837 (void);
// 0x0000030E System.Void Mirror.NetworkWriterPool::.cctor()
extern void NetworkWriterPool__cctor_mADD04829DDD342A3B2F45599DB6F9E24F3714A0A (void);
// 0x0000030F System.Void Mirror.NetworkWriterPool/<>c::.cctor()
extern void U3CU3Ec__cctor_m8E31B109427F127868DC4A9CC80423289F8F7925 (void);
// 0x00000310 System.Void Mirror.NetworkWriterPool/<>c::.ctor()
extern void U3CU3Ec__ctor_m156EE2936091B7F9B9C4755D7689C6AC5A15B908 (void);
// 0x00000311 Mirror.NetworkWriterPooled Mirror.NetworkWriterPool/<>c::<.cctor>b__5_0()
extern void U3CU3Ec_U3C_cctorU3Eb__5_0_mE87A5D1CFBABE41A31D4C9C569B739484ACE7A1D (void);
// 0x00000312 System.Void Mirror.PooledNetworkWriter::.ctor()
extern void PooledNetworkWriter__ctor_m03300CFF65FD836ECF916D67D9E9EC307AAC4B5E (void);
// 0x00000313 System.Void Mirror.NetworkWriterPooled::Dispose()
extern void NetworkWriterPooled_Dispose_m8514B01116425746A2736F81802E0D8388C34EA6 (void);
// 0x00000314 System.Void Mirror.NetworkWriterPooled::.ctor()
extern void NetworkWriterPooled__ctor_m282C66453866D943B7D2988B2FFA6D5DF1E5C2F4 (void);
// 0x00000315 System.Void Mirror.Pool`1::.ctor(System.Func`1<T>,System.Int32)
// 0x00000316 T Mirror.Pool`1::Take()
// 0x00000317 T Mirror.Pool`1::Get()
// 0x00000318 System.Void Mirror.Pool`1::Return(T)
// 0x00000319 System.Int32 Mirror.Pool`1::get_Count()
// 0x0000031A System.Double Mirror.Snapshot::get_remoteTimestamp()
// 0x0000031B System.Void Mirror.Snapshot::set_remoteTimestamp(System.Double)
// 0x0000031C System.Double Mirror.Snapshot::get_localTimestamp()
// 0x0000031D System.Void Mirror.Snapshot::set_localTimestamp(System.Double)
// 0x0000031E System.Void Mirror.SnapshotInterpolation::InsertIfNewEnough(T,System.Collections.Generic.SortedList`2<System.Double,T>)
// 0x0000031F System.Boolean Mirror.SnapshotInterpolation::HasAmountOlderThan(System.Collections.Generic.SortedList`2<System.Double,T>,System.Double,System.Int32)
// 0x00000320 System.Boolean Mirror.SnapshotInterpolation::HasEnough(System.Collections.Generic.SortedList`2<System.Double,T>,System.Double,System.Double)
// 0x00000321 System.Boolean Mirror.SnapshotInterpolation::HasEnoughWithoutFirst(System.Collections.Generic.SortedList`2<System.Double,T>,System.Double,System.Double)
// 0x00000322 System.Double Mirror.SnapshotInterpolation::CalculateCatchup(System.Collections.Generic.SortedList`2<System.Double,T>,System.Int32,System.Double)
// 0x00000323 System.Void Mirror.SnapshotInterpolation::GetFirstSecondAndDelta(System.Collections.Generic.SortedList`2<System.Double,T>,T&,T&,System.Double&)
// 0x00000324 System.Boolean Mirror.SnapshotInterpolation::Compute(System.Double,System.Double,System.Double&,System.Double,System.Collections.Generic.SortedList`2<System.Double,T>,System.Int32,System.Single,System.Func`4<T,T,System.Double,T>,T&)
// 0x00000325 System.Int32 Mirror.SyncIDictionary`2::get_Count()
// 0x00000326 System.Boolean Mirror.SyncIDictionary`2::get_IsReadOnly()
// 0x00000327 System.Void Mirror.SyncIDictionary`2::set_IsReadOnly(System.Boolean)
// 0x00000328 System.Void Mirror.SyncIDictionary`2::add_Callback(Mirror.SyncIDictionary`2/SyncDictionaryChanged<TKey,TValue>)
// 0x00000329 System.Void Mirror.SyncIDictionary`2::remove_Callback(Mirror.SyncIDictionary`2/SyncDictionaryChanged<TKey,TValue>)
// 0x0000032A System.Void Mirror.SyncIDictionary`2::Reset()
// 0x0000032B System.Collections.Generic.ICollection`1<TKey> Mirror.SyncIDictionary`2::get_Keys()
// 0x0000032C System.Collections.Generic.ICollection`1<TValue> Mirror.SyncIDictionary`2::get_Values()
// 0x0000032D System.Collections.Generic.IEnumerable`1<TKey> Mirror.SyncIDictionary`2::System.Collections.Generic.IReadOnlyDictionary<TKey,TValue>.get_Keys()
// 0x0000032E System.Collections.Generic.IEnumerable`1<TValue> Mirror.SyncIDictionary`2::System.Collections.Generic.IReadOnlyDictionary<TKey,TValue>.get_Values()
// 0x0000032F System.Void Mirror.SyncIDictionary`2::ClearChanges()
// 0x00000330 System.Void Mirror.SyncIDictionary`2::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
// 0x00000331 System.Void Mirror.SyncIDictionary`2::AddOperation(Mirror.SyncIDictionary`2/Operation<TKey,TValue>,TKey,TValue)
// 0x00000332 System.Void Mirror.SyncIDictionary`2::OnSerializeAll(Mirror.NetworkWriter)
// 0x00000333 System.Void Mirror.SyncIDictionary`2::OnSerializeDelta(Mirror.NetworkWriter)
// 0x00000334 System.Void Mirror.SyncIDictionary`2::OnDeserializeAll(Mirror.NetworkReader)
// 0x00000335 System.Void Mirror.SyncIDictionary`2::OnDeserializeDelta(Mirror.NetworkReader)
// 0x00000336 System.Void Mirror.SyncIDictionary`2::Clear()
// 0x00000337 System.Boolean Mirror.SyncIDictionary`2::ContainsKey(TKey)
// 0x00000338 System.Boolean Mirror.SyncIDictionary`2::Remove(TKey)
// 0x00000339 TValue Mirror.SyncIDictionary`2::get_Item(TKey)
// 0x0000033A System.Void Mirror.SyncIDictionary`2::set_Item(TKey,TValue)
// 0x0000033B System.Boolean Mirror.SyncIDictionary`2::TryGetValue(TKey,TValue&)
// 0x0000033C System.Void Mirror.SyncIDictionary`2::Add(TKey,TValue)
// 0x0000033D System.Void Mirror.SyncIDictionary`2::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x0000033E System.Boolean Mirror.SyncIDictionary`2::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x0000033F System.Void Mirror.SyncIDictionary`2::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
// 0x00000340 System.Boolean Mirror.SyncIDictionary`2::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000341 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Mirror.SyncIDictionary`2::GetEnumerator()
// 0x00000342 System.Collections.IEnumerator Mirror.SyncIDictionary`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000343 System.Void Mirror.SyncIDictionary`2/SyncDictionaryChanged::.ctor(System.Object,System.IntPtr)
// 0x00000344 System.Void Mirror.SyncIDictionary`2/SyncDictionaryChanged::Invoke(Mirror.SyncIDictionary`2/Operation<TKey,TValue>,TKey,TValue)
// 0x00000345 System.IAsyncResult Mirror.SyncIDictionary`2/SyncDictionaryChanged::BeginInvoke(Mirror.SyncIDictionary`2/Operation<TKey,TValue>,TKey,TValue,System.AsyncCallback,System.Object)
// 0x00000346 System.Void Mirror.SyncIDictionary`2/SyncDictionaryChanged::EndInvoke(System.IAsyncResult)
// 0x00000347 System.Void Mirror.SyncDictionary`2::.ctor()
// 0x00000348 System.Void Mirror.SyncDictionary`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000349 System.Void Mirror.SyncDictionary`2::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
// 0x0000034A System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> Mirror.SyncDictionary`2::get_Values()
// 0x0000034B System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> Mirror.SyncDictionary`2::get_Keys()
// 0x0000034C System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> Mirror.SyncDictionary`2::GetEnumerator()
// 0x0000034D System.Int32 Mirror.SyncList`1::get_Count()
// 0x0000034E System.Boolean Mirror.SyncList`1::get_IsReadOnly()
// 0x0000034F System.Void Mirror.SyncList`1::set_IsReadOnly(System.Boolean)
// 0x00000350 System.Void Mirror.SyncList`1::add_Callback(Mirror.SyncList`1/SyncListChanged<T>)
// 0x00000351 System.Void Mirror.SyncList`1::remove_Callback(Mirror.SyncList`1/SyncListChanged<T>)
// 0x00000352 System.Void Mirror.SyncList`1::.ctor()
// 0x00000353 System.Void Mirror.SyncList`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000354 System.Void Mirror.SyncList`1::.ctor(System.Collections.Generic.IList`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000355 System.Void Mirror.SyncList`1::ClearChanges()
// 0x00000356 System.Void Mirror.SyncList`1::Reset()
// 0x00000357 System.Void Mirror.SyncList`1::AddOperation(Mirror.SyncList`1/Operation<T>,System.Int32,T,T)
// 0x00000358 System.Void Mirror.SyncList`1::OnSerializeAll(Mirror.NetworkWriter)
// 0x00000359 System.Void Mirror.SyncList`1::OnSerializeDelta(Mirror.NetworkWriter)
// 0x0000035A System.Void Mirror.SyncList`1::OnDeserializeAll(Mirror.NetworkReader)
// 0x0000035B System.Void Mirror.SyncList`1::OnDeserializeDelta(Mirror.NetworkReader)
// 0x0000035C System.Void Mirror.SyncList`1::Add(T)
// 0x0000035D System.Void Mirror.SyncList`1::AddRange(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000035E System.Void Mirror.SyncList`1::Clear()
// 0x0000035F System.Boolean Mirror.SyncList`1::Contains(T)
// 0x00000360 System.Void Mirror.SyncList`1::CopyTo(T[],System.Int32)
// 0x00000361 System.Int32 Mirror.SyncList`1::IndexOf(T)
// 0x00000362 System.Int32 Mirror.SyncList`1::FindIndex(System.Predicate`1<T>)
// 0x00000363 T Mirror.SyncList`1::Find(System.Predicate`1<T>)
// 0x00000364 System.Collections.Generic.List`1<T> Mirror.SyncList`1::FindAll(System.Predicate`1<T>)
// 0x00000365 System.Void Mirror.SyncList`1::Insert(System.Int32,T)
// 0x00000366 System.Void Mirror.SyncList`1::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000367 System.Boolean Mirror.SyncList`1::Remove(T)
// 0x00000368 System.Void Mirror.SyncList`1::RemoveAt(System.Int32)
// 0x00000369 System.Int32 Mirror.SyncList`1::RemoveAll(System.Predicate`1<T>)
// 0x0000036A T Mirror.SyncList`1::get_Item(System.Int32)
// 0x0000036B System.Void Mirror.SyncList`1::set_Item(System.Int32,T)
// 0x0000036C Mirror.SyncList`1/Enumerator<T> Mirror.SyncList`1::GetEnumerator()
// 0x0000036D System.Collections.Generic.IEnumerator`1<T> Mirror.SyncList`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000036E System.Collections.IEnumerator Mirror.SyncList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000036F System.Void Mirror.SyncList`1/SyncListChanged::.ctor(System.Object,System.IntPtr)
// 0x00000370 System.Void Mirror.SyncList`1/SyncListChanged::Invoke(Mirror.SyncList`1/Operation<T>,System.Int32,T,T)
// 0x00000371 System.IAsyncResult Mirror.SyncList`1/SyncListChanged::BeginInvoke(Mirror.SyncList`1/Operation<T>,System.Int32,T,T,System.AsyncCallback,System.Object)
// 0x00000372 System.Void Mirror.SyncList`1/SyncListChanged::EndInvoke(System.IAsyncResult)
// 0x00000373 T Mirror.SyncList`1/Enumerator::get_Current()
// 0x00000374 System.Void Mirror.SyncList`1/Enumerator::set_Current(T)
// 0x00000375 System.Void Mirror.SyncList`1/Enumerator::.ctor(Mirror.SyncList`1<T>)
// 0x00000376 System.Boolean Mirror.SyncList`1/Enumerator::MoveNext()
// 0x00000377 System.Void Mirror.SyncList`1/Enumerator::Reset()
// 0x00000378 System.Object Mirror.SyncList`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000379 System.Void Mirror.SyncList`1/Enumerator::Dispose()
// 0x0000037A System.Void Mirror.SyncObject::ClearChanges()
// 0x0000037B System.Void Mirror.SyncObject::Flush()
extern void SyncObject_Flush_m5C0BD574AC5AB2581F7934A964C5B180AC0A99CF (void);
// 0x0000037C System.Void Mirror.SyncObject::OnSerializeAll(Mirror.NetworkWriter)
// 0x0000037D System.Void Mirror.SyncObject::OnSerializeDelta(Mirror.NetworkWriter)
// 0x0000037E System.Void Mirror.SyncObject::OnDeserializeAll(Mirror.NetworkReader)
// 0x0000037F System.Void Mirror.SyncObject::OnDeserializeDelta(Mirror.NetworkReader)
// 0x00000380 System.Void Mirror.SyncObject::Reset()
// 0x00000381 System.Void Mirror.SyncObject::.ctor()
extern void SyncObject__ctor_mA548CB7517A8DD0802BDD878A1828C692F610258 (void);
// 0x00000382 System.Void Mirror.SyncObject/<>c::.cctor()
extern void U3CU3Ec__cctor_m64DD28DA9E9B5A885C12A60328F3648805DA4382 (void);
// 0x00000383 System.Void Mirror.SyncObject/<>c::.ctor()
extern void U3CU3Ec__ctor_mA38180FC1A2E2D4C8B450FDCF4DE75C2C290CDFD (void);
// 0x00000384 System.Boolean Mirror.SyncObject/<>c::<.ctor>b__9_0()
extern void U3CU3Ec_U3C_ctorU3Eb__9_0_m8E16FB94F6784CB289430D8550213C98972127F5 (void);
// 0x00000385 System.Int32 Mirror.SyncSet`1::get_Count()
// 0x00000386 System.Boolean Mirror.SyncSet`1::get_IsReadOnly()
// 0x00000387 System.Void Mirror.SyncSet`1::set_IsReadOnly(System.Boolean)
// 0x00000388 System.Void Mirror.SyncSet`1::add_Callback(Mirror.SyncSet`1/SyncSetChanged<T>)
// 0x00000389 System.Void Mirror.SyncSet`1::remove_Callback(Mirror.SyncSet`1/SyncSetChanged<T>)
// 0x0000038A System.Void Mirror.SyncSet`1::.ctor(System.Collections.Generic.ISet`1<T>)
// 0x0000038B System.Void Mirror.SyncSet`1::Reset()
// 0x0000038C System.Void Mirror.SyncSet`1::ClearChanges()
// 0x0000038D System.Void Mirror.SyncSet`1::AddOperation(Mirror.SyncSet`1/Operation<T>,T)
// 0x0000038E System.Void Mirror.SyncSet`1::AddOperation(Mirror.SyncSet`1/Operation<T>)
// 0x0000038F System.Void Mirror.SyncSet`1::OnSerializeAll(Mirror.NetworkWriter)
// 0x00000390 System.Void Mirror.SyncSet`1::OnSerializeDelta(Mirror.NetworkWriter)
// 0x00000391 System.Void Mirror.SyncSet`1::OnDeserializeAll(Mirror.NetworkReader)
// 0x00000392 System.Void Mirror.SyncSet`1::OnDeserializeDelta(Mirror.NetworkReader)
// 0x00000393 System.Boolean Mirror.SyncSet`1::Add(T)
// 0x00000394 System.Void Mirror.SyncSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000395 System.Void Mirror.SyncSet`1::Clear()
// 0x00000396 System.Boolean Mirror.SyncSet`1::Contains(T)
// 0x00000397 System.Void Mirror.SyncSet`1::CopyTo(T[],System.Int32)
// 0x00000398 System.Boolean Mirror.SyncSet`1::Remove(T)
// 0x00000399 System.Collections.Generic.IEnumerator`1<T> Mirror.SyncSet`1::GetEnumerator()
// 0x0000039A System.Collections.IEnumerator Mirror.SyncSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000039B System.Void Mirror.SyncSet`1::ExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000039C System.Void Mirror.SyncSet`1::IntersectWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000039D System.Void Mirror.SyncSet`1::IntersectWithSet(System.Collections.Generic.ISet`1<T>)
// 0x0000039E System.Boolean Mirror.SyncSet`1::IsProperSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000039F System.Boolean Mirror.SyncSet`1::IsProperSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003A0 System.Boolean Mirror.SyncSet`1::IsSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003A1 System.Boolean Mirror.SyncSet`1::IsSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003A2 System.Boolean Mirror.SyncSet`1::Overlaps(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003A3 System.Boolean Mirror.SyncSet`1::SetEquals(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003A4 System.Void Mirror.SyncSet`1::SymmetricExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003A5 System.Void Mirror.SyncSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003A6 System.Void Mirror.SyncSet`1/SyncSetChanged::.ctor(System.Object,System.IntPtr)
// 0x000003A7 System.Void Mirror.SyncSet`1/SyncSetChanged::Invoke(Mirror.SyncSet`1/Operation<T>,T)
// 0x000003A8 System.IAsyncResult Mirror.SyncSet`1/SyncSetChanged::BeginInvoke(Mirror.SyncSet`1/Operation<T>,T,System.AsyncCallback,System.Object)
// 0x000003A9 System.Void Mirror.SyncSet`1/SyncSetChanged::EndInvoke(System.IAsyncResult)
// 0x000003AA System.Void Mirror.SyncHashSet`1::.ctor()
// 0x000003AB System.Void Mirror.SyncHashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000003AC System.Collections.Generic.HashSet`1/Enumerator<T> Mirror.SyncHashSet`1::GetEnumerator()
// 0x000003AD System.Void Mirror.SyncSortedSet`1::.ctor()
// 0x000003AE System.Void Mirror.SyncSortedSet`1::.ctor(System.Collections.Generic.IComparer`1<T>)
// 0x000003AF System.Collections.Generic.SortedSet`1/Enumerator<T> Mirror.SyncSortedSet`1::GetEnumerator()
// 0x000003B0 T Mirror.SyncVar`1::get_Value()
// 0x000003B1 System.Void Mirror.SyncVar`1::set_Value(T)
// 0x000003B2 System.Void Mirror.SyncVar`1::add_Callback(System.Action`2<T,T>)
// 0x000003B3 System.Void Mirror.SyncVar`1::remove_Callback(System.Action`2<T,T>)
// 0x000003B4 System.Void Mirror.SyncVar`1::InvokeCallback(T,T)
// 0x000003B5 System.Void Mirror.SyncVar`1::ClearChanges()
// 0x000003B6 System.Void Mirror.SyncVar`1::Reset()
// 0x000003B7 System.Void Mirror.SyncVar`1::.ctor(T)
// 0x000003B8 T Mirror.SyncVar`1::op_Implicit(Mirror.SyncVar`1<T>)
// 0x000003B9 Mirror.SyncVar`1<T> Mirror.SyncVar`1::op_Implicit(T)
// 0x000003BA System.Void Mirror.SyncVar`1::OnSerializeAll(Mirror.NetworkWriter)
// 0x000003BB System.Void Mirror.SyncVar`1::OnSerializeDelta(Mirror.NetworkWriter)
// 0x000003BC System.Void Mirror.SyncVar`1::OnDeserializeAll(Mirror.NetworkReader)
// 0x000003BD System.Void Mirror.SyncVar`1::OnDeserializeDelta(Mirror.NetworkReader)
// 0x000003BE System.Boolean Mirror.SyncVar`1::Equals(T)
// 0x000003BF System.String Mirror.SyncVar`1::ToString()
// 0x000003C0 UnityEngine.GameObject Mirror.SyncVarGameObject::get_Value()
extern void SyncVarGameObject_get_Value_mCE863D617F089FC2272FE5AB3D1A43317A9BC11C (void);
// 0x000003C1 System.Void Mirror.SyncVarGameObject::set_Value(UnityEngine.GameObject)
extern void SyncVarGameObject_set_Value_mA55A0618A702B53CBB03308DA01ECFBD6F8A534B (void);
// 0x000003C2 System.Void Mirror.SyncVarGameObject::add_Callback(System.Action`2<UnityEngine.GameObject,UnityEngine.GameObject>)
extern void SyncVarGameObject_add_Callback_m5BEC6532748DAFFF8F5C9EF28F12D8F671BDD484 (void);
// 0x000003C3 System.Void Mirror.SyncVarGameObject::remove_Callback(System.Action`2<UnityEngine.GameObject,UnityEngine.GameObject>)
extern void SyncVarGameObject_remove_Callback_m71688C4CBE55498AB5091A04961B3F6101499610 (void);
// 0x000003C4 System.Void Mirror.SyncVarGameObject::InvokeCallback(System.UInt32,System.UInt32)
extern void SyncVarGameObject_InvokeCallback_m0B64C81B474E4F2E8321B0E99701CC7201B0C68E (void);
// 0x000003C5 System.Void Mirror.SyncVarGameObject::.ctor(UnityEngine.GameObject)
extern void SyncVarGameObject__ctor_m8377609DC058D8D57CE2D5E6A391A3A4501944D1 (void);
// 0x000003C6 System.UInt32 Mirror.SyncVarGameObject::GetNetId(UnityEngine.GameObject)
extern void SyncVarGameObject_GetNetId_mDDFDB2CFFDFEAF59FE6E45139D34A469E63C4EBC (void);
// 0x000003C7 UnityEngine.GameObject Mirror.SyncVarGameObject::GetGameObject(System.UInt32)
extern void SyncVarGameObject_GetGameObject_m574C3C357CDC5CCDE2CB6B9BBCC0314452A72749 (void);
// 0x000003C8 UnityEngine.GameObject Mirror.SyncVarGameObject::op_Implicit(Mirror.SyncVarGameObject)
extern void SyncVarGameObject_op_Implicit_mE0FEEE46FAEA6EC1B32E6523D2A391CC6BAC585E (void);
// 0x000003C9 Mirror.SyncVarGameObject Mirror.SyncVarGameObject::op_Implicit(UnityEngine.GameObject)
extern void SyncVarGameObject_op_Implicit_m695B8ABF1927DAC3FED0B7F01F000FA0575906E1 (void);
// 0x000003CA System.Boolean Mirror.SyncVarGameObject::op_Equality(Mirror.SyncVarGameObject,Mirror.SyncVarGameObject)
extern void SyncVarGameObject_op_Equality_m066B432FAFE578B1017E263417D885833D16E673 (void);
// 0x000003CB System.Boolean Mirror.SyncVarGameObject::op_Inequality(Mirror.SyncVarGameObject,Mirror.SyncVarGameObject)
extern void SyncVarGameObject_op_Inequality_mA2FC837AD1E9481E8FA0E40ABE587E2FF01A8F9F (void);
// 0x000003CC System.Boolean Mirror.SyncVarGameObject::op_Equality(Mirror.SyncVarGameObject,UnityEngine.GameObject)
extern void SyncVarGameObject_op_Equality_m24C28828F846C575A2452784DADC64E5F066E5D5 (void);
// 0x000003CD System.Boolean Mirror.SyncVarGameObject::op_Inequality(Mirror.SyncVarGameObject,UnityEngine.GameObject)
extern void SyncVarGameObject_op_Inequality_m55E79A1F6FB893A0DDC2388D0E2D1A5C53A725D9 (void);
// 0x000003CE System.Boolean Mirror.SyncVarGameObject::op_Equality(UnityEngine.GameObject,Mirror.SyncVarGameObject)
extern void SyncVarGameObject_op_Equality_mCA9386853FECA9CEA6CA94DF0AF1375A0EFFAA4C (void);
// 0x000003CF System.Boolean Mirror.SyncVarGameObject::op_Inequality(UnityEngine.GameObject,Mirror.SyncVarGameObject)
extern void SyncVarGameObject_op_Inequality_mC2AF9F047A7BBA6E212D2EE32343963CD5FE712C (void);
// 0x000003D0 System.Boolean Mirror.SyncVarGameObject::Equals(System.Object)
extern void SyncVarGameObject_Equals_mAFDB094F055CED564D553EC1D925A38085E749FF (void);
// 0x000003D1 System.Int32 Mirror.SyncVarGameObject::GetHashCode()
extern void SyncVarGameObject_GetHashCode_mEA0C031E08FDC724791D53B09D662D50433A2ADB (void);
// 0x000003D2 T Mirror.SyncVarNetworkBehaviour`1::get_Value()
// 0x000003D3 System.Void Mirror.SyncVarNetworkBehaviour`1::set_Value(T)
// 0x000003D4 System.Void Mirror.SyncVarNetworkBehaviour`1::add_Callback(System.Action`2<T,T>)
// 0x000003D5 System.Void Mirror.SyncVarNetworkBehaviour`1::remove_Callback(System.Action`2<T,T>)
// 0x000003D6 System.Void Mirror.SyncVarNetworkBehaviour`1::InvokeCallback(System.UInt64,System.UInt64)
// 0x000003D7 System.Void Mirror.SyncVarNetworkBehaviour`1::.ctor(T)
// 0x000003D8 T Mirror.SyncVarNetworkBehaviour`1::op_Implicit(Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003D9 Mirror.SyncVarNetworkBehaviour`1<T> Mirror.SyncVarNetworkBehaviour`1::op_Implicit(T)
// 0x000003DA System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Equality(Mirror.SyncVarNetworkBehaviour`1<T>,Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003DB System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Inequality(Mirror.SyncVarNetworkBehaviour`1<T>,Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003DC System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Equality(Mirror.SyncVarNetworkBehaviour`1<T>,Mirror.NetworkBehaviour)
// 0x000003DD System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Inequality(Mirror.SyncVarNetworkBehaviour`1<T>,Mirror.NetworkBehaviour)
// 0x000003DE System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Equality(Mirror.SyncVarNetworkBehaviour`1<T>,T)
// 0x000003DF System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Inequality(Mirror.SyncVarNetworkBehaviour`1<T>,T)
// 0x000003E0 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Equality(Mirror.NetworkBehaviour,Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003E1 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Inequality(Mirror.NetworkBehaviour,Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003E2 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Equality(T,Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003E3 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Inequality(T,Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003E4 System.Boolean Mirror.SyncVarNetworkBehaviour`1::Equals(System.Object)
// 0x000003E5 System.Int32 Mirror.SyncVarNetworkBehaviour`1::GetHashCode()
// 0x000003E6 System.UInt64 Mirror.SyncVarNetworkBehaviour`1::Pack(System.UInt32,System.Byte)
// 0x000003E7 System.Void Mirror.SyncVarNetworkBehaviour`1::Unpack(System.UInt64,System.UInt32&,System.Byte&)
// 0x000003E8 T Mirror.SyncVarNetworkBehaviour`1::ULongToNetworkBehaviour(System.UInt64)
// 0x000003E9 System.UInt64 Mirror.SyncVarNetworkBehaviour`1::NetworkBehaviourToULong(T)
// 0x000003EA System.Void Mirror.SyncVarNetworkBehaviour`1::OnSerializeAll(Mirror.NetworkWriter)
// 0x000003EB System.Void Mirror.SyncVarNetworkBehaviour`1::OnSerializeDelta(Mirror.NetworkWriter)
// 0x000003EC System.Void Mirror.SyncVarNetworkBehaviour`1::OnDeserializeAll(Mirror.NetworkReader)
// 0x000003ED System.Void Mirror.SyncVarNetworkBehaviour`1::OnDeserializeDelta(Mirror.NetworkReader)
// 0x000003EE Mirror.NetworkIdentity Mirror.SyncVarNetworkIdentity::get_Value()
extern void SyncVarNetworkIdentity_get_Value_m4A5B6029A660F63E83BC9CD1FD156790D4362819 (void);
// 0x000003EF System.Void Mirror.SyncVarNetworkIdentity::set_Value(Mirror.NetworkIdentity)
extern void SyncVarNetworkIdentity_set_Value_m58E2BB65D54E259F0F9F30C288FAF8A34AF53045 (void);
// 0x000003F0 System.Void Mirror.SyncVarNetworkIdentity::add_Callback(System.Action`2<Mirror.NetworkIdentity,Mirror.NetworkIdentity>)
extern void SyncVarNetworkIdentity_add_Callback_m2DE9BE87A8455420C11AA999274F5D226735DCDA (void);
// 0x000003F1 System.Void Mirror.SyncVarNetworkIdentity::remove_Callback(System.Action`2<Mirror.NetworkIdentity,Mirror.NetworkIdentity>)
extern void SyncVarNetworkIdentity_remove_Callback_m28AD6AD811BDFBD270559002550355EE1C06CD27 (void);
// 0x000003F2 System.Void Mirror.SyncVarNetworkIdentity::InvokeCallback(System.UInt32,System.UInt32)
extern void SyncVarNetworkIdentity_InvokeCallback_m6D000155BFB12EEED8A2D46C2F05EB225667E9B5 (void);
// 0x000003F3 System.Void Mirror.SyncVarNetworkIdentity::.ctor(Mirror.NetworkIdentity)
extern void SyncVarNetworkIdentity__ctor_mBAC777A21EC23B1907A8458C0DFFB2E7DA6F7D90 (void);
// 0x000003F4 Mirror.NetworkIdentity Mirror.SyncVarNetworkIdentity::op_Implicit(Mirror.SyncVarNetworkIdentity)
extern void SyncVarNetworkIdentity_op_Implicit_mC090F6DABD968B07E42E88BE67D7AD8104C28BF3 (void);
// 0x000003F5 Mirror.SyncVarNetworkIdentity Mirror.SyncVarNetworkIdentity::op_Implicit(Mirror.NetworkIdentity)
extern void SyncVarNetworkIdentity_op_Implicit_m8F267E2B15BA1F663B8D05AB2B26585A79AA2B6E (void);
// 0x000003F6 System.Boolean Mirror.SyncVarNetworkIdentity::op_Equality(Mirror.SyncVarNetworkIdentity,Mirror.SyncVarNetworkIdentity)
extern void SyncVarNetworkIdentity_op_Equality_m9C151063AF8154BCF536CCDE1A1D97CB1D5F7CF4 (void);
// 0x000003F7 System.Boolean Mirror.SyncVarNetworkIdentity::op_Inequality(Mirror.SyncVarNetworkIdentity,Mirror.SyncVarNetworkIdentity)
extern void SyncVarNetworkIdentity_op_Inequality_m73E5C7EE416394B035810F45C4DFB979EC7DB0B7 (void);
// 0x000003F8 System.Boolean Mirror.SyncVarNetworkIdentity::op_Equality(Mirror.SyncVarNetworkIdentity,Mirror.NetworkIdentity)
extern void SyncVarNetworkIdentity_op_Equality_mEA3911DF99BCF9620BC2B58CC097CAF22F4A2F22 (void);
// 0x000003F9 System.Boolean Mirror.SyncVarNetworkIdentity::op_Inequality(Mirror.SyncVarNetworkIdentity,Mirror.NetworkIdentity)
extern void SyncVarNetworkIdentity_op_Inequality_m3A8A9CD1B021422607F52C8D27A3F6FD62DF80E6 (void);
// 0x000003FA System.Boolean Mirror.SyncVarNetworkIdentity::op_Equality(Mirror.NetworkIdentity,Mirror.SyncVarNetworkIdentity)
extern void SyncVarNetworkIdentity_op_Equality_m3EF4EBF0C92E8EBB64BF873ECCC08103C6F97572 (void);
// 0x000003FB System.Boolean Mirror.SyncVarNetworkIdentity::op_Inequality(Mirror.NetworkIdentity,Mirror.SyncVarNetworkIdentity)
extern void SyncVarNetworkIdentity_op_Inequality_m6F9178AAD975B9E4B57079BF4B8BF13311C3BA10 (void);
// 0x000003FC System.Boolean Mirror.SyncVarNetworkIdentity::Equals(System.Object)
extern void SyncVarNetworkIdentity_Equals_m65605646E2B00333D97FCEF8705FC1C724F0EA7E (void);
// 0x000003FD System.Int32 Mirror.SyncVarNetworkIdentity::GetHashCode()
extern void SyncVarNetworkIdentity_GetHashCode_mFAF16D42E2955CB08324D620546E9383E1B907F3 (void);
// 0x000003FE System.Void Mirror.LatencySimulation::Awake()
extern void LatencySimulation_Awake_mD0CB059F008E153CE8AC551F9249116213F045B2 (void);
// 0x000003FF System.Void Mirror.LatencySimulation::OnEnable()
extern void LatencySimulation_OnEnable_m5192A6777F253107059F1FB3EAAF0F4B5FF4F185 (void);
// 0x00000400 System.Void Mirror.LatencySimulation::OnDisable()
extern void LatencySimulation_OnDisable_mF655C8C3F028CC294460A6A957C33C103891EF53 (void);
// 0x00000401 System.Single Mirror.LatencySimulation::Noise(System.Single)
extern void LatencySimulation_Noise_m1F3843829053E0C0D23E0666D2A728C8C501EE72 (void);
// 0x00000402 System.Single Mirror.LatencySimulation::SimulateLatency(System.Int32)
extern void LatencySimulation_SimulateLatency_m96CBDEEB67E6CA1C913E7E2FF537E7DE5E1965E2 (void);
// 0x00000403 System.Void Mirror.LatencySimulation::SimulateSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32,System.Single,System.Collections.Generic.List`1<Mirror.QueuedMessage>,System.Collections.Generic.List`1<Mirror.QueuedMessage>)
extern void LatencySimulation_SimulateSend_m10A2B28AEE6273662AE3BDA26D889568EA1B3E86 (void);
// 0x00000404 System.Boolean Mirror.LatencySimulation::Available()
extern void LatencySimulation_Available_m58166DC8F9D783C9EAC3ADE7844CE4DDF94122D1 (void);
// 0x00000405 System.Void Mirror.LatencySimulation::ClientConnect(System.String)
extern void LatencySimulation_ClientConnect_mA421FAA285DC9F7A11B71F9A12FB0A18F4EF70F6 (void);
// 0x00000406 System.Void Mirror.LatencySimulation::ClientConnect(System.Uri)
extern void LatencySimulation_ClientConnect_mF4F7A11F7FE9D9B8F6BFE2B59B9EEB8A1D31BB79 (void);
// 0x00000407 System.Boolean Mirror.LatencySimulation::ClientConnected()
extern void LatencySimulation_ClientConnected_m3C83DA084C9C5363E08818FB6B2C5AE8E4D40006 (void);
// 0x00000408 System.Void Mirror.LatencySimulation::ClientDisconnect()
extern void LatencySimulation_ClientDisconnect_mB4DC3F81C0E255CFF948367D2144D6164BCDEE7B (void);
// 0x00000409 System.Void Mirror.LatencySimulation::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
extern void LatencySimulation_ClientSend_mCFDCAEF8FE11104F02BC80EA16F8993F6FAF0974 (void);
// 0x0000040A System.Uri Mirror.LatencySimulation::ServerUri()
extern void LatencySimulation_ServerUri_m2F83819935A33F356172017EC6FBC0028CF7D4B8 (void);
// 0x0000040B System.Boolean Mirror.LatencySimulation::ServerActive()
extern void LatencySimulation_ServerActive_m0D5C3290B13A754C5E82AD0658F5CDF03803C234 (void);
// 0x0000040C System.String Mirror.LatencySimulation::ServerGetClientAddress(System.Int32)
extern void LatencySimulation_ServerGetClientAddress_mF04BFDB290D97FAB20F9765CE9BAF73FB6122B83 (void);
// 0x0000040D System.Void Mirror.LatencySimulation::ServerDisconnect(System.Int32)
extern void LatencySimulation_ServerDisconnect_mA1EEBC5605FDD6960D2D26F25FDACF1014F39562 (void);
// 0x0000040E System.Void Mirror.LatencySimulation::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void LatencySimulation_ServerSend_m5CDF4C4070790964E697DBF25DD6328B38087AAC (void);
// 0x0000040F System.Void Mirror.LatencySimulation::ServerStart()
extern void LatencySimulation_ServerStart_m9A02F23068A6CDC45B8996CB18D0F81A0CE22117 (void);
// 0x00000410 System.Void Mirror.LatencySimulation::ServerStop()
extern void LatencySimulation_ServerStop_m0D316CBA8805BE026057689A8F096D35FBB50CC7 (void);
// 0x00000411 System.Void Mirror.LatencySimulation::ClientEarlyUpdate()
extern void LatencySimulation_ClientEarlyUpdate_mB435C18B756AF92EDE89339FE98BE3D5F12F7CB2 (void);
// 0x00000412 System.Void Mirror.LatencySimulation::ServerEarlyUpdate()
extern void LatencySimulation_ServerEarlyUpdate_mF35156F7650DADF4FB394DEBB1945BC1896184E9 (void);
// 0x00000413 System.Void Mirror.LatencySimulation::ClientLateUpdate()
extern void LatencySimulation_ClientLateUpdate_mE23BD9FE950168B1C0072413F5D78C25DE4213D7 (void);
// 0x00000414 System.Void Mirror.LatencySimulation::ServerLateUpdate()
extern void LatencySimulation_ServerLateUpdate_m4D4209A62E2E89D4D2D9FE9C12BE8284DD52BC5D (void);
// 0x00000415 System.Int32 Mirror.LatencySimulation::GetBatchThreshold(System.Int32)
extern void LatencySimulation_GetBatchThreshold_m37F2AF8CBF3DBFA4A22AE8FC0B233355148971E7 (void);
// 0x00000416 System.Int32 Mirror.LatencySimulation::GetMaxPacketSize(System.Int32)
extern void LatencySimulation_GetMaxPacketSize_mB59E4EB5BE11F496BE1511C989B12BE3B61A3CF3 (void);
// 0x00000417 System.Void Mirror.LatencySimulation::Shutdown()
extern void LatencySimulation_Shutdown_mC3046E3ADA6BF4D6C1BE4A62C477485C10B3B3B9 (void);
// 0x00000418 System.String Mirror.LatencySimulation::ToString()
extern void LatencySimulation_ToString_mE852B81FC6869A65D634D054CC96C97A12998092 (void);
// 0x00000419 System.Void Mirror.LatencySimulation::.ctor()
extern void LatencySimulation__ctor_m03A63EBE9022826CE2FDBB06FBBB34126F0B293B (void);
// 0x0000041A System.Boolean Mirror.MiddlewareTransport::Available()
extern void MiddlewareTransport_Available_m411D7B26DF15DFC79722E919B00CBEC3F610E5DA (void);
// 0x0000041B System.Int32 Mirror.MiddlewareTransport::GetMaxPacketSize(System.Int32)
extern void MiddlewareTransport_GetMaxPacketSize_m28AC40A0440620D670700A64EF2A061E47F3168D (void);
// 0x0000041C System.Int32 Mirror.MiddlewareTransport::GetBatchThreshold(System.Int32)
extern void MiddlewareTransport_GetBatchThreshold_mED304EB53A66D5287A1A21CF41C60ADBCB354086 (void);
// 0x0000041D System.Void Mirror.MiddlewareTransport::Shutdown()
extern void MiddlewareTransport_Shutdown_m8B4F981C42A2329708768E00A385FDBE4019C447 (void);
// 0x0000041E System.Void Mirror.MiddlewareTransport::ClientConnect(System.String)
extern void MiddlewareTransport_ClientConnect_mCAF0426A1FB08C9922E6CF9149709B95BAB06A8D (void);
// 0x0000041F System.Boolean Mirror.MiddlewareTransport::ClientConnected()
extern void MiddlewareTransport_ClientConnected_m1643CFBFD2BBDAF375CAD2651AE6C7F1710E45E3 (void);
// 0x00000420 System.Void Mirror.MiddlewareTransport::ClientDisconnect()
extern void MiddlewareTransport_ClientDisconnect_m3F6ADA34C9ED111E4ECF342D1F90846E88774EC4 (void);
// 0x00000421 System.Void Mirror.MiddlewareTransport::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
extern void MiddlewareTransport_ClientSend_m4C2C38CC8E2DBC85F1D0468737C5582337EF97AF (void);
// 0x00000422 System.Void Mirror.MiddlewareTransport::ClientEarlyUpdate()
extern void MiddlewareTransport_ClientEarlyUpdate_m02AB943881C5268777F72CCBEB21B44E39498B7E (void);
// 0x00000423 System.Void Mirror.MiddlewareTransport::ClientLateUpdate()
extern void MiddlewareTransport_ClientLateUpdate_mBE2E413DBAB919C972DD3F589D10DB0075E211D4 (void);
// 0x00000424 System.Boolean Mirror.MiddlewareTransport::ServerActive()
extern void MiddlewareTransport_ServerActive_mDDBE6B2F53D2AD7625198DA5728EE35C575A3417 (void);
// 0x00000425 System.Void Mirror.MiddlewareTransport::ServerStart()
extern void MiddlewareTransport_ServerStart_m83D72FAFD4A1F5E4D096003D6723B23906709AF9 (void);
// 0x00000426 System.Void Mirror.MiddlewareTransport::ServerStop()
extern void MiddlewareTransport_ServerStop_m60C91D39F2046F046B8C3664CEC5E66ED0D0F6ED (void);
// 0x00000427 System.Void Mirror.MiddlewareTransport::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void MiddlewareTransport_ServerSend_mB7D84E1825413A452DC30BB35081509A892BD3E2 (void);
// 0x00000428 System.Void Mirror.MiddlewareTransport::ServerDisconnect(System.Int32)
extern void MiddlewareTransport_ServerDisconnect_m23D93C972A453065ED9295EE4EA09DEAB06C64B4 (void);
// 0x00000429 System.String Mirror.MiddlewareTransport::ServerGetClientAddress(System.Int32)
extern void MiddlewareTransport_ServerGetClientAddress_m16C622F42B04C08193A7FBA9F8BC2AEDF1EEDA7E (void);
// 0x0000042A System.Uri Mirror.MiddlewareTransport::ServerUri()
extern void MiddlewareTransport_ServerUri_mA0673C40718ABD0F7A6AE094451DA1DCF3B8A326 (void);
// 0x0000042B System.Void Mirror.MiddlewareTransport::ServerEarlyUpdate()
extern void MiddlewareTransport_ServerEarlyUpdate_m41937D34B366DCD18500B17FECD0324214BF064A (void);
// 0x0000042C System.Void Mirror.MiddlewareTransport::ServerLateUpdate()
extern void MiddlewareTransport_ServerLateUpdate_m30BD5C9986FCC69528C0EB40376F94507AA7DE4F (void);
// 0x0000042D System.Void Mirror.MiddlewareTransport::.ctor()
extern void MiddlewareTransport__ctor_mCC955E33DC37D37D65ED774014D51051C5758B65 (void);
// 0x0000042E System.Void Mirror.MultiplexTransport::Awake()
extern void MultiplexTransport_Awake_m93C0AEFF0ACDF70E4537AB84EEB04CE4613A0576 (void);
// 0x0000042F System.Void Mirror.MultiplexTransport::ClientEarlyUpdate()
extern void MultiplexTransport_ClientEarlyUpdate_m25C31409D47DC43E6F8E2D02A3C6FFCC905639A3 (void);
// 0x00000430 System.Void Mirror.MultiplexTransport::ServerEarlyUpdate()
extern void MultiplexTransport_ServerEarlyUpdate_m216824203A20B670BABEBBAF65A4C5EB6B150F35 (void);
// 0x00000431 System.Void Mirror.MultiplexTransport::ClientLateUpdate()
extern void MultiplexTransport_ClientLateUpdate_m50169C7227888430E7B02C74B45BFD75E22D2B87 (void);
// 0x00000432 System.Void Mirror.MultiplexTransport::ServerLateUpdate()
extern void MultiplexTransport_ServerLateUpdate_mA516AD160FD303F59E80FA1FB614CB1653437228 (void);
// 0x00000433 System.Void Mirror.MultiplexTransport::OnEnable()
extern void MultiplexTransport_OnEnable_m644F1B5846E1B5024743D4CD270CE40CAE16E263 (void);
// 0x00000434 System.Void Mirror.MultiplexTransport::OnDisable()
extern void MultiplexTransport_OnDisable_m3818B03C8CCA40A39008126187CDBC97C2B31932 (void);
// 0x00000435 System.Boolean Mirror.MultiplexTransport::Available()
extern void MultiplexTransport_Available_m5E68041189FA6D45F8B0BE192906FBF50745093B (void);
// 0x00000436 System.Void Mirror.MultiplexTransport::ClientConnect(System.String)
extern void MultiplexTransport_ClientConnect_mAAC8096A852A42DE9C53CA2AAF2DB0EB429BC5F2 (void);
// 0x00000437 System.Void Mirror.MultiplexTransport::ClientConnect(System.Uri)
extern void MultiplexTransport_ClientConnect_m2640BA041A88EFF1CC37398A23813966448A3572 (void);
// 0x00000438 System.Boolean Mirror.MultiplexTransport::ClientConnected()
extern void MultiplexTransport_ClientConnected_m59A4C147D9E4D0AA495A8C9F9FC73B8E6B014151 (void);
// 0x00000439 System.Void Mirror.MultiplexTransport::ClientDisconnect()
extern void MultiplexTransport_ClientDisconnect_m11CB4676F1CAE0A596B8918DCB10CEBE5092E60D (void);
// 0x0000043A System.Void Mirror.MultiplexTransport::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
extern void MultiplexTransport_ClientSend_mB899809A718125E3F0704C1E07102C7EE0887E6B (void);
// 0x0000043B System.Int32 Mirror.MultiplexTransport::FromBaseId(System.Int32,System.Int32)
extern void MultiplexTransport_FromBaseId_mF710FA98E9A2336422CB2DFBF7CC4BF54B41B284 (void);
// 0x0000043C System.Int32 Mirror.MultiplexTransport::ToBaseId(System.Int32)
extern void MultiplexTransport_ToBaseId_mEDEA4469539AD4C6ECE88F194B420484F1AF8CB7 (void);
// 0x0000043D System.Int32 Mirror.MultiplexTransport::ToTransportId(System.Int32)
extern void MultiplexTransport_ToTransportId_mAE2AA047364BDDC681A3B2A9520A288162EAC4B7 (void);
// 0x0000043E System.Void Mirror.MultiplexTransport::AddServerCallbacks()
extern void MultiplexTransport_AddServerCallbacks_mAB8919C10049912E067D0C10146E4289146A939A (void);
// 0x0000043F System.Uri Mirror.MultiplexTransport::ServerUri()
extern void MultiplexTransport_ServerUri_mB25D6F90CCBAAADBB30AA514E55B7615D3563C0F (void);
// 0x00000440 System.Boolean Mirror.MultiplexTransport::ServerActive()
extern void MultiplexTransport_ServerActive_mE6AAD0BE67D9B6F7A28F9AA77C848F737067A9A3 (void);
// 0x00000441 System.String Mirror.MultiplexTransport::ServerGetClientAddress(System.Int32)
extern void MultiplexTransport_ServerGetClientAddress_mEC33B8AFBFE17800379569DF67FDB1A95AC53DCF (void);
// 0x00000442 System.Void Mirror.MultiplexTransport::ServerDisconnect(System.Int32)
extern void MultiplexTransport_ServerDisconnect_m3B7A7CEE9970254DCFA47959A99F0A46E697ADF9 (void);
// 0x00000443 System.Void Mirror.MultiplexTransport::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void MultiplexTransport_ServerSend_m760A6519B38A5902633030538CC01A91B1371A4A (void);
// 0x00000444 System.Void Mirror.MultiplexTransport::ServerStart()
extern void MultiplexTransport_ServerStart_m997711F482577343746A59B07BAC0C153AF02B03 (void);
// 0x00000445 System.Void Mirror.MultiplexTransport::ServerStop()
extern void MultiplexTransport_ServerStop_m786191B4B7724BA90BB982C026F1C9E707C36381 (void);
// 0x00000446 System.Int32 Mirror.MultiplexTransport::GetMaxPacketSize(System.Int32)
extern void MultiplexTransport_GetMaxPacketSize_m780CFDCCF070CFE1F9CD3F326245478C5E73EDA0 (void);
// 0x00000447 System.Void Mirror.MultiplexTransport::Shutdown()
extern void MultiplexTransport_Shutdown_mC14719523179B4A1E439ADCDCC313F6E31A972A4 (void);
// 0x00000448 System.String Mirror.MultiplexTransport::ToString()
extern void MultiplexTransport_ToString_mAF2A3B96C367D4F229AF97569896D8B71612E59F (void);
// 0x00000449 System.Void Mirror.MultiplexTransport::.ctor()
extern void MultiplexTransport__ctor_m1AA74D3D57E4FAF657EB99DE735A1BFDB7C38495 (void);
// 0x0000044A System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m054DFE8D50F877AD63841E813CB93811ABBDC1E7 (void);
// 0x0000044B System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::<AddServerCallbacks>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__0_m3F7F308DCC08E8E7F4B38ADB1E0FE84EC27B5299 (void);
// 0x0000044C System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::<AddServerCallbacks>b__1(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__1_m7880630DC1D0F893844AE14EF6D7E437C89C18ED (void);
// 0x0000044D System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::<AddServerCallbacks>b__2(System.Int32,System.Exception)
extern void U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__2_m2F6380738F2DE4BA3B2E0ED90268B7C5718084F3 (void);
// 0x0000044E System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::<AddServerCallbacks>b__3(System.Int32)
extern void U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__3_m1A02C0762E603F8DBB6ABC53042A95AA06729818 (void);
// 0x0000044F System.Void Mirror.TelepathyTransport::Awake()
extern void TelepathyTransport_Awake_m3EB200590EE12064D70C7A0DD027E40C2D5D00E5 (void);
// 0x00000450 System.Boolean Mirror.TelepathyTransport::Available()
extern void TelepathyTransport_Available_m529F410884695E2DBAF3F6C52C4F3F5D831C060A (void);
// 0x00000451 System.Void Mirror.TelepathyTransport::CreateClient()
extern void TelepathyTransport_CreateClient_m5A3E215CA3BC5E1DDA7037F0B46096559ABE3CE6 (void);
// 0x00000452 System.Boolean Mirror.TelepathyTransport::ClientConnected()
extern void TelepathyTransport_ClientConnected_m634791BEEE06F18CB86AADF7B53DE8DF52823152 (void);
// 0x00000453 System.Void Mirror.TelepathyTransport::ClientConnect(System.String)
extern void TelepathyTransport_ClientConnect_m1FBCA148622546F990ED50164D82DB0D8AE6A228 (void);
// 0x00000454 System.Void Mirror.TelepathyTransport::ClientConnect(System.Uri)
extern void TelepathyTransport_ClientConnect_mAB3658761DFA72046AF0564674A15929F626FFB7 (void);
// 0x00000455 System.Void Mirror.TelepathyTransport::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
extern void TelepathyTransport_ClientSend_m5720B529399E32E698EBFC6203097C81371D8795 (void);
// 0x00000456 System.Void Mirror.TelepathyTransport::ClientDisconnect()
extern void TelepathyTransport_ClientDisconnect_mE1F09DD72AAE2D2EABC7B8C89B5D0B300F681BFD (void);
// 0x00000457 System.Void Mirror.TelepathyTransport::ClientEarlyUpdate()
extern void TelepathyTransport_ClientEarlyUpdate_mDC030022D1B606BB0B9C75C94849E0F34A358ACD (void);
// 0x00000458 System.Uri Mirror.TelepathyTransport::ServerUri()
extern void TelepathyTransport_ServerUri_mCC65867B56A7D0B01DC9E7C09847BD8FA607B1DA (void);
// 0x00000459 System.Boolean Mirror.TelepathyTransport::ServerActive()
extern void TelepathyTransport_ServerActive_m1A932812542164DEB969EA1990CF03812FA6B0F6 (void);
// 0x0000045A System.Void Mirror.TelepathyTransport::ServerStart()
extern void TelepathyTransport_ServerStart_m444B6B84A925BEA37A9686610DC36FD7E2E5E7FD (void);
// 0x0000045B System.Void Mirror.TelepathyTransport::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void TelepathyTransport_ServerSend_mBA012641BD35DD6D8150F265D5A5301E03EE4F42 (void);
// 0x0000045C System.Void Mirror.TelepathyTransport::ServerDisconnect(System.Int32)
extern void TelepathyTransport_ServerDisconnect_m828880FA831B11E4F63DF32A684E26CEC912B4E1 (void);
// 0x0000045D System.String Mirror.TelepathyTransport::ServerGetClientAddress(System.Int32)
extern void TelepathyTransport_ServerGetClientAddress_m4322423E96F902AFD444525D1E6CE68E178FCA1D (void);
// 0x0000045E System.Void Mirror.TelepathyTransport::ServerStop()
extern void TelepathyTransport_ServerStop_m1C3B479ED47A722888E04C3F27DB9A8BE0896FEF (void);
// 0x0000045F System.Void Mirror.TelepathyTransport::ServerEarlyUpdate()
extern void TelepathyTransport_ServerEarlyUpdate_mF1E5E6749863D2E9712EB0FFEDB4941698526E77 (void);
// 0x00000460 System.Void Mirror.TelepathyTransport::Shutdown()
extern void TelepathyTransport_Shutdown_m15CFA2417884E989101A3259BCDFBD089B2E9C3D (void);
// 0x00000461 System.Int32 Mirror.TelepathyTransport::GetMaxPacketSize(System.Int32)
extern void TelepathyTransport_GetMaxPacketSize_m1952E7D852788A11E9002C6D9FA31DEE8D7EC336 (void);
// 0x00000462 System.String Mirror.TelepathyTransport::ToString()
extern void TelepathyTransport_ToString_m84B9527990704286FA00487EC8F70897DF684D11 (void);
// 0x00000463 System.Void Mirror.TelepathyTransport::.ctor()
extern void TelepathyTransport__ctor_m29EC658D39383FD39ECB4535F7402767AD209535 (void);
// 0x00000464 System.Boolean Mirror.TelepathyTransport::<Awake>b__16_0()
extern void TelepathyTransport_U3CAwakeU3Eb__16_0_mEBEFF9D7CEC38DF1E1132764A3A1AA21923DD176 (void);
// 0x00000465 System.Void Mirror.TelepathyTransport::<CreateClient>b__18_0()
extern void TelepathyTransport_U3CCreateClientU3Eb__18_0_m80C3EC25F7C28C37EC4AC9C695967C85317D1399 (void);
// 0x00000466 System.Void Mirror.TelepathyTransport::<CreateClient>b__18_1(System.ArraySegment`1<System.Byte>)
extern void TelepathyTransport_U3CCreateClientU3Eb__18_1_m66BA55843D5BD71116C5CEAF402B822801029090 (void);
// 0x00000467 System.Void Mirror.TelepathyTransport::<CreateClient>b__18_2()
extern void TelepathyTransport_U3CCreateClientU3Eb__18_2_m402733E536B10F311D068B8A738D2602F456A7CB (void);
// 0x00000468 System.Void Mirror.TelepathyTransport::<ServerStart>b__27_0(System.Int32)
extern void TelepathyTransport_U3CServerStartU3Eb__27_0_m1F9C5421E9BB2ACEC6D9279B81ED9467C91A8C2E (void);
// 0x00000469 System.Void Mirror.TelepathyTransport::<ServerStart>b__27_1(System.Int32,System.ArraySegment`1<System.Byte>)
extern void TelepathyTransport_U3CServerStartU3Eb__27_1_m0EE491D075C22B93931F221BF2BBDAA3D0A436CC (void);
// 0x0000046A System.Void Mirror.TelepathyTransport::<ServerStart>b__27_2(System.Int32)
extern void TelepathyTransport_U3CServerStartU3Eb__27_2_mF41F1BB9824AA491058579171DD1F314E12B588D (void);
// 0x0000046B System.Boolean Mirror.Transport::Available()
// 0x0000046C System.Boolean Mirror.Transport::ClientConnected()
// 0x0000046D System.Void Mirror.Transport::ClientConnect(System.String)
// 0x0000046E System.Void Mirror.Transport::ClientConnect(System.Uri)
extern void Transport_ClientConnect_mCE84ED183F7A7A3650AD2C693AA98A9CC28DC516 (void);
// 0x0000046F System.Void Mirror.Transport::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
// 0x00000470 System.Void Mirror.Transport::ClientDisconnect()
// 0x00000471 System.Uri Mirror.Transport::ServerUri()
// 0x00000472 System.Boolean Mirror.Transport::ServerActive()
// 0x00000473 System.Void Mirror.Transport::ServerStart()
// 0x00000474 System.Void Mirror.Transport::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
// 0x00000475 System.Void Mirror.Transport::ServerDisconnect(System.Int32)
// 0x00000476 System.String Mirror.Transport::ServerGetClientAddress(System.Int32)
// 0x00000477 System.Void Mirror.Transport::ServerStop()
// 0x00000478 System.Int32 Mirror.Transport::GetMaxPacketSize(System.Int32)
// 0x00000479 System.Int32 Mirror.Transport::GetBatchThreshold(System.Int32)
extern void Transport_GetBatchThreshold_m19AFF384DB542EBDB9362C3E94458F9918D8F472 (void);
// 0x0000047A System.Void Mirror.Transport::Update()
extern void Transport_Update_m805E65D9CFAE9907EA3FF5ED8C4367E8416748A1 (void);
// 0x0000047B System.Void Mirror.Transport::LateUpdate()
extern void Transport_LateUpdate_m93EE7407CC6DBC79D9104171CDC68D2C0D98CC5E (void);
// 0x0000047C System.Void Mirror.Transport::ClientEarlyUpdate()
extern void Transport_ClientEarlyUpdate_m161A0CE6D881CF8F7BBCD41F84C7D137C1B85DC3 (void);
// 0x0000047D System.Void Mirror.Transport::ServerEarlyUpdate()
extern void Transport_ServerEarlyUpdate_mBD9978F98BF9B5F34A97B3FEDCA8C69545AC2F10 (void);
// 0x0000047E System.Void Mirror.Transport::ClientLateUpdate()
extern void Transport_ClientLateUpdate_m483122FA63F280BEC27BAA42EE468A3FD2F7E9F8 (void);
// 0x0000047F System.Void Mirror.Transport::ServerLateUpdate()
extern void Transport_ServerLateUpdate_m8BC41B0A6B1F799D812276E85C6F21F648F0CAE0 (void);
// 0x00000480 System.Void Mirror.Transport::Shutdown()
// 0x00000481 System.Void Mirror.Transport::OnApplicationQuit()
extern void Transport_OnApplicationQuit_m35ACCD0AD1874108989519C9309C03C42A47C283 (void);
// 0x00000482 System.Void Mirror.Transport::.ctor()
extern void Transport__ctor_m951E466BF3C4D441259ACE340863D31DC828B3AF (void);
// 0x00000483 System.Void Mirror.NetworkMessageDelegate::.ctor(System.Object,System.IntPtr)
extern void NetworkMessageDelegate__ctor_mD6E8A0B315CE6D4AF6C72D7B38F8FB7D21006960 (void);
// 0x00000484 System.Void Mirror.NetworkMessageDelegate::Invoke(Mirror.NetworkConnection,Mirror.NetworkReader,System.Int32)
extern void NetworkMessageDelegate_Invoke_m98C548118D37B52A595A6D21FC60C7CBED50A564 (void);
// 0x00000485 System.IAsyncResult Mirror.NetworkMessageDelegate::BeginInvoke(Mirror.NetworkConnection,Mirror.NetworkReader,System.Int32,System.AsyncCallback,System.Object)
extern void NetworkMessageDelegate_BeginInvoke_mAD4A78DFFF75B688D3265665E4556C536E98A977 (void);
// 0x00000486 System.Void Mirror.NetworkMessageDelegate::EndInvoke(System.IAsyncResult)
extern void NetworkMessageDelegate_EndInvoke_m5232A8A85073AD476E6F264D324E0B7F3E0D5AAC (void);
// 0x00000487 System.Void Mirror.SpawnDelegate::.ctor(System.Object,System.IntPtr)
extern void SpawnDelegate__ctor_m8F9AE1A91FE0B260D9D1077D256628CD01E9E850 (void);
// 0x00000488 UnityEngine.GameObject Mirror.SpawnDelegate::Invoke(UnityEngine.Vector3,System.Guid)
extern void SpawnDelegate_Invoke_m1B47F6656BF440993D298D38E84DDDB57155B910 (void);
// 0x00000489 System.IAsyncResult Mirror.SpawnDelegate::BeginInvoke(UnityEngine.Vector3,System.Guid,System.AsyncCallback,System.Object)
extern void SpawnDelegate_BeginInvoke_mF10AB69AA9AA9FBB471564195BD8AA50A04E95AD (void);
// 0x0000048A UnityEngine.GameObject Mirror.SpawnDelegate::EndInvoke(System.IAsyncResult)
extern void SpawnDelegate_EndInvoke_m644B2556D0CF22425613500117FF37F57C214B5E (void);
// 0x0000048B System.Void Mirror.SpawnHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void SpawnHandlerDelegate__ctor_m4EA3BC557AC3E8C422F149086D9D0DB7735CFBC9 (void);
// 0x0000048C UnityEngine.GameObject Mirror.SpawnHandlerDelegate::Invoke(Mirror.SpawnMessage)
extern void SpawnHandlerDelegate_Invoke_m0556616B7CAAF06679FC504B941CA3BB3F3A217A (void);
// 0x0000048D System.IAsyncResult Mirror.SpawnHandlerDelegate::BeginInvoke(Mirror.SpawnMessage,System.AsyncCallback,System.Object)
extern void SpawnHandlerDelegate_BeginInvoke_m93E307D23418E444F7BE2CE62134D183FBF1382D (void);
// 0x0000048E UnityEngine.GameObject Mirror.SpawnHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void SpawnHandlerDelegate_EndInvoke_m2D904E75331BEB2E1D7959472603220B2EA3E719 (void);
// 0x0000048F System.Void Mirror.UnSpawnDelegate::.ctor(System.Object,System.IntPtr)
extern void UnSpawnDelegate__ctor_mA4DC94E5B22576C43190651F3788F13C1A25175B (void);
// 0x00000490 System.Void Mirror.UnSpawnDelegate::Invoke(UnityEngine.GameObject)
extern void UnSpawnDelegate_Invoke_m943692C898825098A05610FFA71F1A432A26F0BF (void);
// 0x00000491 System.IAsyncResult Mirror.UnSpawnDelegate::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern void UnSpawnDelegate_BeginInvoke_m9273D27CD0369BEC9D931651F139D84660F38C10 (void);
// 0x00000492 System.Void Mirror.UnSpawnDelegate::EndInvoke(System.IAsyncResult)
extern void UnSpawnDelegate_EndInvoke_m5233DF02E97F47E53CD4F21A1993DB518A1A5744 (void);
// 0x00000493 System.UInt32 Mirror.Utils::GetTrueRandomUInt()
extern void Utils_GetTrueRandomUInt_m18CEE51EB1A1DF72867E31B9168610AF0D92EAEB (void);
// 0x00000494 System.Boolean Mirror.Utils::IsPrefab(UnityEngine.GameObject)
extern void Utils_IsPrefab_mC214A12F48646B4F34DA550DB27CD33C5CFEE71A (void);
// 0x00000495 System.Boolean Mirror.Utils::IsSceneObjectWithPrefabParent(UnityEngine.GameObject,UnityEngine.GameObject&)
extern void Utils_IsSceneObjectWithPrefabParent_m8B241F8EA3BF330478CC5A837331E948D97C04E2 (void);
// 0x00000496 System.Boolean Mirror.Utils::IsPointInScreen(UnityEngine.Vector2)
extern void Utils_IsPointInScreen_m742BEEAF9B1F44C20CC42003E5818F3BF499B729 (void);
// 0x00000497 System.String Mirror.Utils::PrettyBytes(System.Int64)
extern void Utils_PrettyBytes_m7AD7804F170D3AD120320CF8F4432F58D176ADA2 (void);
// 0x00000498 Mirror.NetworkIdentity Mirror.Utils::GetSpawnedInServerOrClient(System.UInt32)
extern void Utils_GetSpawnedInServerOrClient_m8A1D9AB4EB7672B53355A6900A89E26FFA60A574 (void);
// 0x00000499 System.Void Mirror.RemoteCalls.RemoteCallDelegate::.ctor(System.Object,System.IntPtr)
extern void RemoteCallDelegate__ctor_m252DBEC3816F583FF0270DFFAE713E29C914D65C (void);
// 0x0000049A System.Void Mirror.RemoteCalls.RemoteCallDelegate::Invoke(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void RemoteCallDelegate_Invoke_mE2D0B4E12822433608A1571DBEC43F1EABEE42AB (void);
// 0x0000049B System.IAsyncResult Mirror.RemoteCalls.RemoteCallDelegate::BeginInvoke(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient,System.AsyncCallback,System.Object)
extern void RemoteCallDelegate_BeginInvoke_mB66DBE1857F9A5853CE5FD064120EA1BE703CBE2 (void);
// 0x0000049C System.Void Mirror.RemoteCalls.RemoteCallDelegate::EndInvoke(System.IAsyncResult)
extern void RemoteCallDelegate_EndInvoke_m54C56F11205C67A3B5E3F193790486BC6F8A657D (void);
// 0x0000049D System.Boolean Mirror.RemoteCalls.Invoker::AreEqual(System.Type,Mirror.RemoteCalls.RemoteCallType,Mirror.RemoteCalls.RemoteCallDelegate)
extern void Invoker_AreEqual_mF68D11E6046548EE698E7D455C865AE2A8C28C9C (void);
// 0x0000049E System.Void Mirror.RemoteCalls.Invoker::.ctor()
extern void Invoker__ctor_m8DE43B22C4BFF2DB9E2208F81BC5D31FCFBF37B3 (void);
// 0x0000049F System.Boolean Mirror.RemoteCalls.RemoteProcedureCalls::CheckIfDelegateExists(System.Type,Mirror.RemoteCalls.RemoteCallType,Mirror.RemoteCalls.RemoteCallDelegate,System.Int32)
extern void RemoteProcedureCalls_CheckIfDelegateExists_mAB0E55F6FE91536834E37050438635AE77B3331F (void);
// 0x000004A0 System.Int32 Mirror.RemoteCalls.RemoteProcedureCalls::RegisterDelegate(System.Type,System.String,Mirror.RemoteCalls.RemoteCallType,Mirror.RemoteCalls.RemoteCallDelegate,System.Boolean)
extern void RemoteProcedureCalls_RegisterDelegate_mB78312F428BACF8CF708DD90157F582681898EED (void);
// 0x000004A1 System.Void Mirror.RemoteCalls.RemoteProcedureCalls::RegisterCommand(System.Type,System.String,Mirror.RemoteCalls.RemoteCallDelegate,System.Boolean)
extern void RemoteProcedureCalls_RegisterCommand_mEE58861E157F6AB47C8B8CEE3AD0EEA15ECF4F60 (void);
// 0x000004A2 System.Void Mirror.RemoteCalls.RemoteProcedureCalls::RegisterRpc(System.Type,System.String,Mirror.RemoteCalls.RemoteCallDelegate)
extern void RemoteProcedureCalls_RegisterRpc_mCFB5293951307DDDF042FA2FA881E2FBD1361622 (void);
// 0x000004A3 System.Void Mirror.RemoteCalls.RemoteProcedureCalls::RemoveDelegate(System.Int32)
extern void RemoteProcedureCalls_RemoveDelegate_mEC2E4E8AC9D9FA016D25008B0F82FEA994907AE4 (void);
// 0x000004A4 System.Boolean Mirror.RemoteCalls.RemoteProcedureCalls::GetInvokerForHash(System.Int32,Mirror.RemoteCalls.RemoteCallType,Mirror.RemoteCalls.Invoker&)
extern void RemoteProcedureCalls_GetInvokerForHash_m411FC36099D735D21CE477925761905BC1AE544A (void);
// 0x000004A5 System.Boolean Mirror.RemoteCalls.RemoteProcedureCalls::Invoke(System.Int32,Mirror.RemoteCalls.RemoteCallType,Mirror.NetworkReader,Mirror.NetworkBehaviour,Mirror.NetworkConnectionToClient)
extern void RemoteProcedureCalls_Invoke_mA531F7621D985754257828272313590C314918CA (void);
// 0x000004A6 System.Boolean Mirror.RemoteCalls.RemoteProcedureCalls::CommandRequiresAuthority(System.Int32)
extern void RemoteProcedureCalls_CommandRequiresAuthority_mC8951D65C229D7543154334F9525C265493E23E5 (void);
// 0x000004A7 Mirror.RemoteCalls.RemoteCallDelegate Mirror.RemoteCalls.RemoteProcedureCalls::GetDelegate(System.Int32)
extern void RemoteProcedureCalls_GetDelegate_mB9FD0206C949F270579E058D4F073E7C285A8E73 (void);
// 0x000004A8 System.Void Mirror.RemoteCalls.RemoteProcedureCalls::.cctor()
extern void RemoteProcedureCalls__cctor_m1C19D578EC8EE8467DE532845B32E491702EEF01 (void);
// 0x000004A9 Mirror.ReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ReadyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ReadyMessage_m075F08A19B82E3386470E4682FE8049E1FE466F1 (void);
// 0x000004AA System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ReadyMessage(Mirror.NetworkWriter,Mirror.ReadyMessage)
extern void GeneratedNetworkCode__Write_Mirror_ReadyMessage_m5321F6E73EBC204A535B4117FBFD56B3B6F1DF3E (void);
// 0x000004AB Mirror.NotReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NotReadyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m1B5ECFC3082B00559E38081D4AF150BD7507E528 (void);
// 0x000004AC System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NotReadyMessage(Mirror.NetworkWriter,Mirror.NotReadyMessage)
extern void GeneratedNetworkCode__Write_Mirror_NotReadyMessage_m9FAD954526FEBA8ADE0B3FB19DD79FAE3F75B830 (void);
// 0x000004AD Mirror.AddPlayerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.AddPlayerMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_mD594AE97DACC696857EDABA65071147E6AAA74BF (void);
// 0x000004AE System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.AddPlayerMessage(Mirror.NetworkWriter,Mirror.AddPlayerMessage)
extern void GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_m21FDF6F83887B85B7642727DEE97599057D88142 (void);
// 0x000004AF Mirror.SceneMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SceneMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SceneMessage_m98DC05F3545048FF14872D6F69C883854CBBF6F8 (void);
// 0x000004B0 Mirror.SceneOperation Mirror.GeneratedNetworkCode::_Read_Mirror.SceneOperation(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SceneOperation_mF5CB0A94AE859CF19C5CBE2FB30C751589B9DD31 (void);
// 0x000004B1 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneMessage(Mirror.NetworkWriter,Mirror.SceneMessage)
extern void GeneratedNetworkCode__Write_Mirror_SceneMessage_mE675705095C6D09C5AD43E1176E29E3FD49BCBA1 (void);
// 0x000004B2 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneOperation(Mirror.NetworkWriter,Mirror.SceneOperation)
extern void GeneratedNetworkCode__Write_Mirror_SceneOperation_mCD5706E35424CFD0386B102B9F52BC3FDF3362E2 (void);
// 0x000004B3 Mirror.CommandMessage Mirror.GeneratedNetworkCode::_Read_Mirror.CommandMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_CommandMessage_m6A83891534C74B42DE59F10E61D59F3817F600A0 (void);
// 0x000004B4 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.CommandMessage(Mirror.NetworkWriter,Mirror.CommandMessage)
extern void GeneratedNetworkCode__Write_Mirror_CommandMessage_m46C843E1FEE1A686C21FD996E15D8F88935E2A11 (void);
// 0x000004B5 Mirror.RpcMessage Mirror.GeneratedNetworkCode::_Read_Mirror.RpcMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_RpcMessage_m86AE78A791D6711381C1BCBAECDD632318094BD0 (void);
// 0x000004B6 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.RpcMessage(Mirror.NetworkWriter,Mirror.RpcMessage)
extern void GeneratedNetworkCode__Write_Mirror_RpcMessage_mAF266DA1E1F721E6DE66DC247FE8A2BFD63C492C (void);
// 0x000004B7 Mirror.SpawnMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SpawnMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SpawnMessage_mA50F8DA79BBA354AD085010B076D4E85AB186EFB (void);
// 0x000004B8 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SpawnMessage(Mirror.NetworkWriter,Mirror.SpawnMessage)
extern void GeneratedNetworkCode__Write_Mirror_SpawnMessage_m98EBEFEF724118AB8490FA73F6EAEEF41D7892FF (void);
// 0x000004B9 Mirror.ChangeOwnerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ChangeOwnerMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m63CA80B6E5756675ADA6AF60511E0B30F63311FA (void);
// 0x000004BA System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ChangeOwnerMessage(Mirror.NetworkWriter,Mirror.ChangeOwnerMessage)
extern void GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_mAB793B697671AC8B955B396FC1CC27A58DBF1065 (void);
// 0x000004BB Mirror.ObjectSpawnStartedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_mB8E5941E4475B65FA4DAEACBD74C1F9655199527 (void);
// 0x000004BC System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnStartedMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_mF95BF9BD312E1D208F8C77EC165FD41CF4CB0281 (void);
// 0x000004BD Mirror.ObjectSpawnFinishedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_mE74235E357CB63EEDD22B8E0445B72DCF09C336E (void);
// 0x000004BE System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnFinishedMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_mDE8C681049E25055D2E42DD8853CB06656051246 (void);
// 0x000004BF Mirror.ObjectDestroyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectDestroyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_mF44BEDB706BC3DDE113D3BA95A5868EADEF717BC (void);
// 0x000004C0 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectDestroyMessage(Mirror.NetworkWriter,Mirror.ObjectDestroyMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_mC6BAB97AF90EB548D0A9310CF7D3953796920E36 (void);
// 0x000004C1 Mirror.ObjectHideMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectHideMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_mC93A013D4AF4482FD03EEF3BD5097A2397AD4CC7 (void);
// 0x000004C2 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectHideMessage(Mirror.NetworkWriter,Mirror.ObjectHideMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_m33E4C3748C204940ECFF9AEA1E7ED7F97C05E5B1 (void);
// 0x000004C3 Mirror.EntityStateMessage Mirror.GeneratedNetworkCode::_Read_Mirror.EntityStateMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_EntityStateMessage_m4B23E10BC7E307C7ECD22CC75BEECDBE62E91BF1 (void);
// 0x000004C4 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.EntityStateMessage(Mirror.NetworkWriter,Mirror.EntityStateMessage)
extern void GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m727648D11A6EC5BB7A19141B8902B6055A87241E (void);
// 0x000004C5 Mirror.NetworkPingMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPingMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m10E4A9A1FD5CBAF1001823F67E58E749F91DF115 (void);
// 0x000004C6 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPingMessage(Mirror.NetworkWriter,Mirror.NetworkPingMessage)
extern void GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_m2B4B615BF9725BC828F1547703AF35AC2BA07415 (void);
// 0x000004C7 Mirror.NetworkPongMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPongMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_m5FC87DE505EE48D9CF04A41874B89AB79088B0A7 (void);
// 0x000004C8 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPongMessage(Mirror.NetworkWriter,Mirror.NetworkPongMessage)
extern void GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_mC4B6E0A4CE3AA683CFB3D459621EC25C9CB6D074 (void);
// 0x000004C9 System.Void Mirror.GeneratedNetworkCode::InitReadWriters()
extern void GeneratedNetworkCode_InitReadWriters_m3EF6B3771311173939C3C88B6507664844858949 (void);
static Il2CppMethodPointer s_methodPointers[1225] = 
{
	EmbeddedAttribute__ctor_m94AFFD8C6D8833094CF3E792FD0961D61B08B3BE,
	IsUnmanagedAttribute__ctor_m675B7DAF4C1DADC62DFCCD061BFA40123307934C,
	KcpTransport_FromKcpChannel_mE1CCEFD2A9E964A0C89560F6DB0172C75A6EC716,
	KcpTransport_ToKcpChannel_mE432B8EE0BD4D3B41362274122B3147227DE2922,
	KcpTransport_Awake_mC4B5B95AB1E9B9D1EB9E5557B183742D567F34EA,
	KcpTransport_OnValidate_m5993AC5DC6ABB89891C7B4F4A4514C00F2BA865E,
	KcpTransport_Available_mDA3BAC62C809A798A547268E68121B3ABA72DED6,
	KcpTransport_ClientConnected_mBAA27FD04F9936DC343A4042E6C5DEE69AA4D3D3,
	KcpTransport_ClientConnect_m4A3672DC09D87F271CE7B7A979E764CB02A06C0E,
	KcpTransport_ClientConnect_m11819F34D781BA55F83B239305B3446CF0825D56,
	KcpTransport_ClientSend_m50CAEF627B64AAB4D113CF4DC2C0AADDA53CE6D1,
	KcpTransport_ClientDisconnect_m3D121AB3095CC6D39760FD49E9EBBC9002E9B59E,
	KcpTransport_ClientEarlyUpdate_mA9295A15B66875839B43C7A3BA7385D44F0A6199,
	KcpTransport_ClientLateUpdate_m1409F5F97A36C7C18AE060A64F9C881AEE644DF1,
	KcpTransport_ServerUri_mFFCC8D110A35287BBA75768008686CCAE89ADBE8,
	KcpTransport_ServerActive_m43207C3745E18B193E2571E41E42CDB5FE1A4AB4,
	KcpTransport_ServerStart_m88DDC0773E40D1C73B26320BF5F8441A4FC875A1,
	KcpTransport_ServerSend_m9172730670C1FF5FF4AD7648C0B94402899ABF8F,
	KcpTransport_ServerDisconnect_mC5FDB541625FD58044BB1C90738A0CF2B13E5FC4,
	KcpTransport_ServerGetClientAddress_m6BF2BCAEDA25D0883D3FF388BF9FFD0BE1E2DA12,
	KcpTransport_ServerStop_mEB5DBFC594B11EE22DD8D46ED1218B8554F56514,
	KcpTransport_ServerEarlyUpdate_mEB6EDEEFC58471EAD1EF599183EE2306B127F03E,
	KcpTransport_ServerLateUpdate_m144C8EE30B4BBB0CE6A7CB14B7307293BE35AD10,
	KcpTransport_Shutdown_m26F8F3058772178601B6AD00F2DB83023730A327,
	KcpTransport_GetMaxPacketSize_mA70F8AD2CE6700EC34B8C363D3F2426C23B31769,
	KcpTransport_GetBatchThreshold_m3AB86C5A22DC4CE2A82026E6FFEEC1A47FA4F69C,
	KcpTransport_GetAverageMaxSendRate_mA295A0CCFFEF490F4E77E2CB2DC87D38F08168BD,
	KcpTransport_GetAverageMaxReceiveRate_m8BFF62372AE77AE085E2F4D266C48230BBE886DE,
	KcpTransport_GetTotalSendQueue_mE75D614D3C322855765441DFF18922F94C6245DA,
	KcpTransport_GetTotalReceiveQueue_m4D8318B0BA1D346A16805124225070F0662C2322,
	KcpTransport_GetTotalSendBuffer_m2DDE8C525CD7FC321E21929BF7F76520DB17002E,
	KcpTransport_GetTotalReceiveBuffer_m929E3D0CC26E6B54611C361AFBFAB97DCB405672,
	KcpTransport_PrettyBytes_m3222A9BD856CB6FD635F9CE4E558E38DCB20E8F0,
	KcpTransport_OnLogStatistics_mA13F25110EAAE2C422C00750D7BE5C3A1D5B0E78,
	KcpTransport_ToString_mF10DB3787CA152B727862297A2B4F506C3AA2ACD,
	KcpTransport__ctor_m260525AD3CA58577B7BBA0A6B86A37316D38B31F,
	KcpTransport_U3CAwakeU3Eb__22_1_mD585625EEEEFDA459E21C79E171EAD641219A73E,
	KcpTransport_U3CAwakeU3Eb__22_2_m8F37F800819FF5F4B6FECA1B9A245473C5C32F2B,
	KcpTransport_U3CAwakeU3Eb__22_3_m9A3F481236C3E9DD6A06BAC1B1443E21A4044B8D,
	KcpTransport_U3CAwakeU3Eb__22_4_mD594B1C327335DD24937F880886741D585E53D89,
	KcpTransport_U3CAwakeU3Eb__22_5_m1E28C66BA0C7467B4CAB6F8D5E877DF6800995D7,
	KcpTransport_U3CAwakeU3Eb__22_6_m54A1094FEEFD3B9B1B19ABA976AAFD7577E8F16C,
	KcpTransport_U3CAwakeU3Eb__22_7_mAE5A7AFB6F41D61019B01A537D72FE5CDF06DA59,
	KcpTransport_U3CAwakeU3Eb__22_8_m54B4233D6D2ADC62534CAA719263563E1D8CD27B,
	KcpTransport_U3CAwakeU3Eb__22_9_m6FD62175783CFB309030E6F9725D821C0983454E,
	KcpTransport_U3CAwakeU3Eb__22_10_m8069602520BA0FEE777D94EC9BDEA0F710ADBE22,
	KcpTransport_U3CAwakeU3Eb__22_11_m55D6B3F6AEC961923BAA4DDC249A46A017706E47,
	KcpTransport_U3CAwakeU3Eb__22_12_mFAEF2E49C32F3BB992B2051112F437416C61E3FD,
	U3CU3Ec__cctor_m8D151DACD678CCE4BC5B001A5BCC16FE7939CC73,
	U3CU3Ec__ctor_m06E38D24631445FEA971806DD448CB15039C8295,
	U3CU3Ec_U3CAwakeU3Eb__22_0_m72C38CD0649A005181240E31C60F306C5427522F,
	U3CU3Ec_U3CGetAverageMaxSendRateU3Eb__44_0_m80F7D97E7C065E18BBABA10C388C11A810777E6B,
	U3CU3Ec_U3CGetAverageMaxReceiveRateU3Eb__45_0_mB02934C9AE268E5D93E48C23EDDA7E74D771168B,
	U3CU3Ec_U3CGetTotalSendQueueU3Eb__46_0_m16161ECB4AC750F816D29258431FD9DAD2E28942,
	U3CU3Ec_U3CGetTotalReceiveQueueU3Eb__47_0_mB5BE6853C1A138D9B621A526353E6DAE22D7AA0B,
	U3CU3Ec_U3CGetTotalSendBufferU3Eb__48_0_m632B118EA360946EA0D67972F5C4D6B6D3F95F8D,
	U3CU3Ec_U3CGetTotalReceiveBufferU3Eb__49_0_m793D399CF36BC5221DC7DC73B859F3E8927ED419,
	SyncVarAttribute__ctor_m5460E3D2EBFF0BE62504E2CE8B80DA021DAAC367,
	CommandAttribute__ctor_mE831DDD68AD14E7103B6CC0566A6E623EEAB281D,
	ClientRpcAttribute__ctor_mE29EAD24EC877A67BDF0DD6FF83AE936005137AB,
	TargetRpcAttribute__ctor_m523EA49BB6BC4A1C2C83DAE1CE196F995F03DC29,
	ServerAttribute__ctor_mAE57EB8632202A70EF0F82B640F31076E266EC61,
	ServerCallbackAttribute__ctor_m9E363D7193F18FD1E1F4D2EA92D23C88FD80D80D,
	ClientAttribute__ctor_m0715C84E926C9EEF43D5777AC41925C0F95CF684,
	ClientCallbackAttribute__ctor_m78743AE405A644C18FA74E531C91042224D26CBD,
	SceneAttribute__ctor_mBDDD93EDE68594FD69DF69052B2CC2719704B89A,
	ShowInInspectorAttribute__ctor_mF58724049FE25823AC9D97426459CBD05532E7F7,
	Batcher__ctor_mD8E8A83534FC660326FB00ED14FFF66629E4C15F,
	Batcher_AddMessage_m7A8F80DC9DBF64B02DAEDE2394B47385C7DF8BEE,
	Batcher_CopyAndReturn_mB3EDA280C4935D70A7BA91C1E95D0CFEE2C0B3CD,
	Batcher_GetBatch_m94CF4CEF213738205C87FFC3EFB00F599E0DE7A3,
	Unbatcher_get_BatchesCount_m986752974D3465F3C92C5A49B854B9314322C9B7,
	Unbatcher_StartReadingBatch_m0DE0FE72F1B8CC6A5BD96E26425AA3DB37700F8D,
	Unbatcher_AddBatch_mB625670EC3127D387396C42073052F23EDCB37CB,
	Unbatcher_GetNextMessage_mF551255B8E429C16F4586881905E28C80157B7F4,
	Unbatcher__ctor_m332FE2E909BC6429FA7260BBD51D60CB90364E20,
	Compression_LargestAbsoluteComponentIndex_mF93CA9F88C5C144E50D5BC9AFE5BB3870C7D81F0,
	Compression_ScaleFloatToUShort_m3D013F6AFA08E1E5967E1FFFC5835F9F5C0020FA,
	Compression_ScaleUShortToFloat_m0D1581C586B11177DB5BA934B5E86F505308E61A,
	Compression_QuaternionElement_m34FB4EBB300E5464CD5BF88B4EEDB51E0D0351A2,
	Compression_CompressQuaternion_m17FF0335074A75F1FBB4A013CB320A6DF4A4EB4B,
	Compression_QuaternionNormalizeSafe_m55ED0BD8698C6A081E433A9E00075CB5FFC225E6,
	Compression_DecompressQuaternion_m97E0C9DC786CB565358A8F4476BAE340667A2385,
	Compression_CompressVarUInt_mBE4774C4312609FE845F178152E3D189F0836A9F,
	Compression_CompressVarInt_m8C2BC3E814B3C8E983CAE1FBC46010661E9D552F,
	Compression_DecompressVarUInt_m6FFA961BEE08C48253EE0519959843A1D9587C0A,
	Compression_DecompressVarInt_m2C488DA0B3176C21314EA3D70504D608D2FA6DFD,
	ExponentialMovingAverage__ctor_m1CD28C151EEBED3094D29E53321BD3A98E6CE053,
	ExponentialMovingAverage_Add_m0817D656C4AC628729650F2DB1D1B0B5AF90E828,
	Extensions_GetStableHashCode_mDE19358D2CA9C78D4686D92B6602E2B5B936556C,
	Extensions_GetMethodName_mD053FC30E67EDED20AE40682988FA349619AD670,
	NULL,
	NULL,
	InterestManagement_Awake_m91C60669B89E7D5E72820D3EBC7DD96E0EA86A81,
	InterestManagement_Reset_mB675D8CABBE15E1896C60920B1DCC7714A07FD2B,
	NULL,
	NULL,
	InterestManagement_RebuildAll_mBEB1072D6C2780155E9CD552768EF75EA5E6619C,
	InterestManagement_SetHostVisibility_m6F968317C97072717125B0B25764B70AC21DFD40,
	InterestManagement_OnSpawned_m0DF5418E719C14D340B48F4FC9B59F72A22FD622,
	InterestManagement_OnDestroyed_m1AEB8C56EC644EEC2267A193DA3F36E68FEDCBB9,
	InterestManagement__ctor_mA085F758D3CB060F5066F098F0CFFDE02B91A830,
	LocalConnectionToClient__ctor_mD682323192A4EAA8B206EC7867E64225404374A6,
	LocalConnectionToClient_get_address_m928E5645D6837B8D8AE39376E806F1D0622C033A,
	LocalConnectionToClient_Send_mD23691121DF107DA2A3565F2A32F963D1349F612,
	LocalConnectionToClient_IsAlive_mE163AADEDCCDE354600472EF978A16E8858BAF70,
	LocalConnectionToClient_DisconnectInternal_mF193A3FAB258E180AB1AC139C632745C22147006,
	LocalConnectionToClient_Disconnect_mE9F4063A5E2B473E1AF5D677B4ED910BA8330495,
	LocalConnectionToServer_get_address_m7EE79DD4773F7D66AB2874733A70B353D392742B,
	LocalConnectionToServer_QueueConnectedEvent_m80C26FFC38800B089CD32BFAF1BA9122D572CEFD,
	LocalConnectionToServer_QueueDisconnectedEvent_m67EA5F1FA05D1A7D5101D44280D75CB27CC5945B,
	LocalConnectionToServer_Send_mC02C10C84FE7F2826F067E426F73FD45800E57C3,
	LocalConnectionToServer_Update_m953199C45763C2D110C0CB1720F08C9190AC5F3E,
	LocalConnectionToServer_DisconnectInternal_m2444BD1D95B854D1F09D0D6FF3E527DC6010415D,
	LocalConnectionToServer_Disconnect_mDBD38307FF977677532B6C27AEBD1770556CEC19,
	LocalConnectionToServer_IsAlive_mA20298DE00228363AB3FE09B24A83A803848D618,
	LocalConnectionToServer__ctor_m51AC250892A8ADC06A6DF45B961DAD1D8930E00E,
	Mathd_LerpUnclamped_m2B1C40B47CF48A868E627DACCEACDE0C9111DBD7,
	Mathd_Clamp01_m7FA273B332D04AAB267D225B0C4D080E3E7E31F6,
	Mathd_InverseLerp_m010B2A1EBFAF5D0B560066C0E65E7243932D79E3,
	MessagePacking_get_MaxContentSize_m08A3CAEC36C4FC63995F0C631A43F58E4D3761DC,
	NULL,
	NULL,
	MessagePacking_Unpack_mFFDC4731C89B68D3A80FC425A290457C210899C7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkPingMessage__ctor_m54E9D94B3A2C74849855189A2580FCBDD1C86D94,
	UnityEventNetworkConnection__ctor_mE5A0CD8B31569300790A92DD74132151435BB336,
	NetworkAuthenticator_OnStartServer_mE4093F0CABA168B50ABA5CAC8C5458B02C8BA67C,
	NetworkAuthenticator_OnStopServer_m30FE34BAAACA29F6EF2D6D85B3D9D78D4B5924A7,
	NetworkAuthenticator_OnServerAuthenticate_m5BD7A2C0666CF51DC6A1552FC246AC5C934087BE,
	NetworkAuthenticator_ServerAccept_mACF91D447AFB44C815ECBC7C79295C23AB6D22A5,
	NetworkAuthenticator_ServerReject_m7A761D9E757E5426A8B9EAE9699AB6EA473AB1BE,
	NetworkAuthenticator_OnStartClient_m0CD8BF9259DED666B6C93AD5F1C925B4ABA7390A,
	NetworkAuthenticator_OnStopClient_mE32A1242E920E1F70FC26D8AFA6C5EE1EEB4763C,
	NetworkAuthenticator_OnClientAuthenticate_m698B490B070DB24853E82ABAA3014DD7FA333FED,
	NetworkAuthenticator_ClientAccept_m35557AF9301C3105E4E1DDA474A8972141179DF4,
	NetworkAuthenticator_ClientReject_m97E6B85AC941084DE35435FD35DE280804D64C66,
	NetworkAuthenticator_Reset_m937A72247EEFE2F232123658A2C1D384C065F8B7,
	NetworkAuthenticator__ctor_mB7C72F9607CD3EE26A45F1702FA61FC1E226B855,
	NetworkBehaviour_get_isServer_m1E9842F452B42CDFCCF64673D52142E9B1270B9C,
	NetworkBehaviour_get_isClient_mA90574E13E8163389D82C8DF66C0AC070E091A68,
	NetworkBehaviour_get_isLocalPlayer_m957C0F05035CEAA69E6B06E8E94C7143AB51305F,
	NetworkBehaviour_get_isServerOnly_m6F68A5CCB47E8527E22CC4385A17D6B3D90F742D,
	NetworkBehaviour_get_isClientOnly_m4E9CB13F7BD31E5312168A155CC169BBD9AE6F4F,
	NetworkBehaviour_get_hasAuthority_m9974B1143E13AE7F01DDEBE4D151AD383CCD17F0,
	NetworkBehaviour_get_netId_mB3920F9E26854072F59CA87B5E428C54AE49694E,
	NetworkBehaviour_get_connectionToServer_mF8A6B1E6340A8902BFC3A352B0422CBFD9D772C0,
	NetworkBehaviour_get_connectionToClient_mB8F2C6152FACA59FD1E16517EC227E631159D5A7,
	NetworkBehaviour_HasSyncObjects_m0AAB904D0F7BCEF9CC643D7475F63B24CCA0E3BC,
	NetworkBehaviour_get_netIdentity_mDAF7D6C8D3DE522EEB2BBA08486446A011BE875D,
	NetworkBehaviour_set_netIdentity_m5DA4B47B0A8A8B3FFE8F5A768E06151CA61EE170,
	NetworkBehaviour_get_ComponentIndex_mB490AE2622BC8169CFC0B49818D6F06C73EDA76B,
	NetworkBehaviour_set_ComponentIndex_mCD0B3BDD40102F8DFBF2B44A866C570258E3D72C,
	NetworkBehaviour_get_syncVarDirtyBits_m96E01E08568BF6521B3B69F8F8F36884CAABEEB4,
	NetworkBehaviour_set_syncVarDirtyBits_m521AED68C76A47BA3C2758D50A28869C340DA6DF,
	NetworkBehaviour_GetSyncVarHookGuard_mF9DBFE6D19AC4402C44FDD0BEF11C1C99B694ED8,
	NetworkBehaviour_getSyncVarHookGuard_m9324239619976E41D7BA80ED159EA975DA157B95,
	NetworkBehaviour_SetSyncVarHookGuard_m8098794BA748ACFFE6B68C69B9D5609E43F71334,
	NetworkBehaviour_setSyncVarHookGuard_m703119E70E8190716761F9318611A0A3CA999454,
	NetworkBehaviour_SetSyncVarDirtyBit_mDCA45CAE44ADE6477E858536153376996FE61E40,
	NetworkBehaviour_SetDirtyBit_m18202DFE2962FA3082E2BE3A02D1FDBEA00295CC,
	NetworkBehaviour_IsDirty_mF99D908ECDA3514F74B9C807729EE470A4573E5B,
	NetworkBehaviour_ClearAllDirtyBits_m5E3672905E1E1BD43993C840A2866BB31067F66C,
	NetworkBehaviour_InitSyncObject_mD7B4B335A69AB45D931DB842B8E47C722EE3621C,
	NetworkBehaviour_SendCommandInternal_m7F39ACA5AF256C2595C6D81CF19AF0333C8BDDC7,
	NetworkBehaviour_SendRPCInternal_m570E5B73F726DF4EE80A97483215E244D6C1C9DD,
	NetworkBehaviour_SendTargetRPCInternal_m8E4B839063E4B5CCB1F2EAC9DA83EED74F3BD91E,
	NULL,
	NetworkBehaviour_GeneratedSyncVarSetter_GameObject_mCF8A10F08EAF57737B52D161DD83312B6BF47105,
	NetworkBehaviour_GeneratedSyncVarSetter_NetworkIdentity_mACE2BBEECB92E6DFDB873ECC775A6C64BEB79929,
	NULL,
	NetworkBehaviour_SyncVarGameObjectEqual_m6989106B1F820A05D898018C355C2B37EA50AAE2,
	NetworkBehaviour_SetSyncVarGameObject_mF35ADD89B1A28955C7956DBEF8B1A33193963E9E,
	NetworkBehaviour_GetSyncVarGameObject_m605D12A70F5E6EF9919CDB4D550FFFBE4C466FE3,
	NetworkBehaviour_SyncVarNetworkIdentityEqual_mE6D21F6C6812F37FC6B45E8A5909300C646A2782,
	NULL,
	NetworkBehaviour_GeneratedSyncVarDeserialize_GameObject_m0814F9B2296DB7492B5F2C9317B406A2E69AED64,
	NetworkBehaviour_GeneratedSyncVarDeserialize_NetworkIdentity_mD5FB29C1EDBB9E8095A30BFFE1C49BF6BF5D7501,
	NULL,
	NetworkBehaviour_SetSyncVarNetworkIdentity_mE032B81D8FB0D7C3A779F7A9BF5BB610F1851E75,
	NetworkBehaviour_GetSyncVarNetworkIdentity_mDACFF6689ACAF87156C56F1367C7449B2CE3467E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkBehaviour_OnSerialize_m360FE2CC92D9C705A9E0598FDF4C8527E7778FCD,
	NetworkBehaviour_OnDeserialize_mEB77EDB6C3C748AAE3AB4225FB48D47146D11AEF,
	NetworkBehaviour_SerializeSyncVars_mF60B2EAAE4B8BBE7044C528F6B3D34CBEB09EA65,
	NetworkBehaviour_DeserializeSyncVars_m4EECC3AAB3CAAD098D0B6056C590BBD98AEAF68C,
	NetworkBehaviour_SerializeObjectsAll_m81592519E358E82E1CD8358A873C0AF075D16B5D,
	NetworkBehaviour_SerializeObjectsDelta_mC9A5A1082FC83204986F5B87A0806B45CA3BBBB7,
	NetworkBehaviour_DeSerializeObjectsAll_m0958114631F640903075897341A5C3C55A01D62A,
	NetworkBehaviour_DeSerializeObjectsDelta_m1A05FFFBC8FA68C2302BE9FDA87450446A87CB2D,
	NetworkBehaviour_ResetSyncObjects_m8F15F614664E0F74352049BC18BC91EE5304AA24,
	NetworkBehaviour_OnStartServer_m3419FB42E919414317B3F207B92C54C416787074,
	NetworkBehaviour_OnStopServer_m7E9BDD1C4E29A1FCB0C7D1C902F04E18394D1FD4,
	NetworkBehaviour_OnStartClient_m0621500FCDF7735D0F305FAA4AF2D7A6EE108925,
	NetworkBehaviour_OnStopClient_m1C27F8D943A29DA3DD88A21264F72CCDE2352AB5,
	NetworkBehaviour_OnStartLocalPlayer_mA165E1998DCDA1A953210549EE45E077C9A9728B,
	NetworkBehaviour_OnStopLocalPlayer_m876B5D778E9377FAF0DB9D3D24F148A612F15041,
	NetworkBehaviour_OnStartAuthority_mB5BADB2A156DEFCC66B2EA093C80CCABB8DB2753,
	NetworkBehaviour_OnStopAuthority_m77D01714689FE908D5E840E4474C6F0D93B700F8,
	NetworkBehaviour__ctor_m63A1588EA5424B45068E2E6F74CDF520BF889765,
	NetworkBehaviourSyncVar__ctor_mFD3CF614B6356621B5CC741500D4FAD1E77A447B,
	NetworkBehaviourSyncVar_Equals_m94E677024C09599396FF1AEB14DA042D2FB8B485,
	NetworkBehaviourSyncVar_Equals_mB5F55C22E07441A6816A3D82647F0E3097CEBC06,
	NetworkBehaviourSyncVar_ToString_mBCD8FA34F95F8B2E52F282476D543394C3006FDD,
	U3CU3Ec__DisplayClass45_0__ctor_m76FB1D32E6319FAC696081BC46A87BC4475CE116,
	U3CU3Ec__DisplayClass45_0_U3CInitSyncObjectU3Eb__0_mF868C5D54DA19C10366FF54C752618E6F4F7DC36,
	U3CU3Ec__DisplayClass45_0_U3CInitSyncObjectU3Eb__1_mA65D00456E668E2C3500B3FBCA5E9698E53C2E41,
	NetworkClient_get_connection_m2B3FE54C58837E91419E19A6CC8BE1E94CDECA8E,
	NetworkClient_set_connection_m2FDFE8A03D75526959C9C6925F2D9D61C36D96F1,
	NetworkClient_get_localPlayer_m2EF038BD98B1533642CDDD88FE3A2E857ECC7E42,
	NetworkClient_set_localPlayer_mCF07ABA4A5E90FBA8EEE4B941D96F1E000962557,
	NetworkClient_get_serverIp_m7ECA1AC413B2F98A8E32B3DA46405933029159A1,
	NetworkClient_get_active_m505597A136FC73CF3075CFA8E6B8A75DC686E1DC,
	NetworkClient_get_isConnecting_m31B917869BCEA13334BC92D06B610362A8F60B80,
	NetworkClient_get_isConnected_m44CC2B9852A93C107983648935F9FD570630E6FF,
	NetworkClient_get_isHostClient_mC27E5DF67807AEC44EF107117794F5A68530C0E4,
	NetworkClient_AddTransportHandlers_m73563AC8BB32F2D6635099855330637FB854E712,
	NetworkClient_RemoveTransportHandlers_mD98C3A9A35E41319BC78616293409C3CD6CEC87C,
	NetworkClient_RegisterSystemHandlers_mA88AD3AE61C023D0415C7FE149196CE902143059,
	NetworkClient_Connect_mFE768C43186239AC9730A35D498906E6A9E3EFBF,
	NetworkClient_Connect_mC7A782B0FB7327ED6D5D90C2D159E3BDE7A7B159,
	NetworkClient_ConnectHost_m72D26C20B6BE60DB32760056A4B41A78BE2A50A3,
	NetworkClient_ConnectLocalServer_m454D1654FF382948EA1E53DBCDB03002EFA505EE,
	NetworkClient_Disconnect_m21A0E9A0BFB3E5C2A6579668E8D2DBB9B6B2DBE9,
	NetworkClient_OnTransportConnected_m5616C2719EEEB72494EE2CB0A58D52C5171BFE0A,
	NetworkClient_UnpackAndInvoke_mEE7ECFE5D434F8152EF9EFD9BEDB8B21F188B580,
	NetworkClient_OnTransportData_m0DAC58D9B39B189EF721143A8C5BB3154C79F7EB,
	NetworkClient_OnTransportDisconnected_mCB848CF3AEB421CC0C225A49E8D0571FC6315EC9,
	NetworkClient_OnError_mCEBB3E46CA7D1075109F85D04F1E963BDDEDB384,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkClient_GetPrefab_m678989575DB12B4AF7E5EB6D7E093F97ED13A6C5,
	NetworkClient_RegisterPrefabIdentity_mE60ADF6ED57BC9DE082E2477E11A0560E1AAE529,
	NetworkClient_RegisterPrefab_m7B4E7F40011057CB98595AFC4B8FB45036BC27EB,
	NetworkClient_RegisterPrefab_m3A96053F28D0C61055AA9E94C3281B552E7DACC8,
	NetworkClient_RegisterPrefab_m369DB14A7C16BD210B56709ACFA8E3D63CDFCFFC,
	NetworkClient_RegisterPrefab_mD121E8EAFF1469CECF448B19061C848599F6CB9A,
	NetworkClient_RegisterPrefab_mF0893802FA981D769F7FB441BB70F474396066A4,
	NetworkClient_RegisterPrefab_m19D9EB2C5EF81464F30419447C4E11B3DFD1497F,
	NetworkClient_UnregisterPrefab_m23F13011D72D521245371ADD22951C9FD5A6BF95,
	NetworkClient_RegisterSpawnHandler_m4EA8FE3694A36DC3EC8500F5EA85E77F17902CE1,
	NetworkClient_RegisterSpawnHandler_m0E8EA0177F3FDECE2826F66A561364F51C3061AD,
	NetworkClient_UnregisterSpawnHandler_m02F713EB933B35F96D397A0B3F689266FC07748F,
	NetworkClient_ClearSpawners_m18C672B489B9C1B823DA85AE1ABFC69ADE33DF27,
	NetworkClient_InvokeUnSpawnHandler_m08A37C52CC165663C4A2AC204F42751E3BCE5E35,
	NetworkClient_Ready_mBBB945097A673E4EFE7DDE934C210D2A9EE4B6A7,
	NetworkClient_InternalAddPlayer_m00C082E62196CCE3F7A85F85B1A553D716CB525C,
	NetworkClient_AddPlayer_m78B4C74A788202FFA45C0EB1AD74BE2AC746C3D7,
	NetworkClient_ApplySpawnPayload_mD8F72DBDD33209F4B37A3781FC2ADF8F0F2EA3D5,
	NetworkClient_FindOrSpawnObject_mBFAEAB7A2B6119C4EC9E7678A987E1DD4425819A,
	NetworkClient_GetExistingObject_m13DC5B6A58A798E70761E38214A6478633080986,
	NetworkClient_SpawnPrefab_m3B3377464433A00377FB27423863AEF7008D8A62,
	NetworkClient_SpawnSceneObject_m2FBF0C72F74A5494F100B6B9ABEC66C41D7AEEAA,
	NetworkClient_GetAndRemoveSceneObject_m9BA68CB7047443A10B501CBE36462443DE67BBF0,
	NetworkClient_ConsiderForSpawning_m225F825B55ECA8B916CBA0A6AE60C0ACF404153C,
	NetworkClient_PrepareToSpawnSceneObjects_mD342375563CBA39EF02992FF712207E957041702,
	NetworkClient_OnObjectSpawnStarted_m5526C66CE4E5B24261003BB6D09D34F06488F833,
	NetworkClient_OnObjectSpawnFinished_m6C0835A836B4E69E9D7785F3CFE04EBBF8814036,
	NetworkClient_ClearNullFromSpawned_m8B7DF7CD5B305ACA67B440171A3F84F5634D7C81,
	NetworkClient_OnHostClientObjectDestroy_m464DAEC93D5A115810546D68E91D26EE5B1E69CB,
	NetworkClient_OnHostClientObjectHide_mAF1056993D91A711735DED6B50F7BA1A7D0EE5B2,
	NetworkClient_OnHostClientSpawn_mCE0BB50FA56A108FF5495B2927ACF908BD14E175,
	NetworkClient_OnEntityStateMessage_m56AB050D55735359FAC9CA4632B6BC27EB224577,
	NetworkClient_OnRPCMessage_m7D4C5E49AED3F41FDBCDA1A820F19043D0E229E6,
	NetworkClient_OnObjectHide_m6F77C650A6B5543BC3E4890C79230DBC81186A2E,
	NetworkClient_OnObjectDestroy_mF55A4E10FF6A53CCEE452EEC40AB24F35AB34B9C,
	NetworkClient_OnSpawn_m9137AB0B4880B88ECBE13ADAE18D4F1A798978C9,
	NetworkClient_OnChangeOwner_m0E7ABCA9E0E783B18C0EE4BF5B999D3CECDF8447,
	NetworkClient_ChangeOwner_m598BD6CDF65A61C6BE1308819183028ADF29B9DC,
	NetworkClient_CheckForLocalPlayer_m3C7EE54D70BAEACF185150DA524A9F6B363B0745,
	NetworkClient_DestroyObject_mA2CF724C2D5C63742526D4764D6F0A784EA176CF,
	NetworkClient_NetworkEarlyUpdate_m0AA4A20D1F75DD16082ABC5071C15EAEF7B9FA1A,
	NetworkClient_NetworkLateUpdate_mC4885568D5F12AC2B253E7B28A9ED634F1A5E0D4,
	NetworkClient_DestroyAllClientObjects_mBE6F479C200E10833F9FC134478B3377AE1E4F15,
	NetworkClient_Shutdown_mB8DE5EAF29857E25ECAE1808AEC2E174121B280C,
	NetworkClient__cctor_mB9642212474ADA1517DDF308D7B7017FB2FC68C7,
	U3CU3Ec__cctor_mF5B8CFBF1BC1295E5E9CC72B812966F97F5E3939,
	U3CU3Ec__ctor_m775B14A08D98B75CB79315A8FC616EAFF41E20BA,
	U3CU3Ec_U3CRegisterSystemHandlersU3Eb__35_0_m639849CBAACBF44DD414FBB7B6ECA35809B1F6D2,
	U3CU3Ec_U3CRegisterSystemHandlersU3Eb__35_1_mBC891D7B3EF593243DFFEA3E5EB5515C1E149848,
	U3CU3Ec_U3CRegisterSystemHandlersU3Eb__35_2_m8B2FFF3053405F86FD6F9F7B52F217FAED9192BE,
	U3CU3Ec_U3CRegisterSystemHandlersU3Eb__35_3_m7F600632F8FFE6760AF1395A04F488CA0F639E3E,
	U3CU3Ec_U3COnObjectSpawnFinishedU3Eb__77_0_mC5F002B53A92AA2C5419A6417B270B70A0875017,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass55_0__ctor_m7BD53CFF3662760A31EA20BC3F8E7999D7146F1E,
	U3CU3Ec__DisplayClass55_0_U3CRegisterPrefabU3Eb__0_m7232C5199884C33F5570D35F7BA6822726FDBC14,
	U3CU3Ec__DisplayClass56_0__ctor_mE9EDCF65F817A4400C5691DB28712B2A6BFC3E30,
	U3CU3Ec__DisplayClass56_0_U3CRegisterPrefabU3Eb__0_m4F4A14C3BB286ACD33F093AD93AC16A1460E284D,
	U3CU3Ec__DisplayClass60_0__ctor_m2BC6035ADA1F23F80ED2FDF81540A2E7BE138ECF,
	U3CU3Ec__DisplayClass60_0_U3CRegisterSpawnHandlerU3Eb__0_m9AA0ED1CF7F40F1F508B63BF276976A9B1F4C6CD,
	NetworkConnection_get_observing_m76F1265AC9C153CD56B967C995F48DBE790A279B,
	NULL,
	NetworkConnection_get_identity_m47B5BA14E5FA41A648F2E98231B8FB4E6BC3693D,
	NetworkConnection_set_identity_m7058218EAFA085B7D92C16E38E2ECAE6B5C2360D,
	NetworkConnection_get_clientOwnedObjects_mE2F8EEF302DA963516E0856BE8606ACB842AE74A,
	NetworkConnection_get_remoteTimeStamp_m78C1EC9507C1E7BFA574B37371B199C1F4C9DE5D,
	NetworkConnection_set_remoteTimeStamp_m4B0FB9D21E7497854E55A6A5C25E1FEED72005A4,
	NetworkConnection__ctor_mF9507734AD718690090796907B2C6E5B9B182636,
	NetworkConnection__ctor_m1A1F427C7F07409461CBBAC865E6F3B97BE23C8E,
	NetworkConnection_GetBatchForChannelId_mFB551DBCCF6DB50B41F1A01E7C8012D25C781F1B,
	NetworkConnection_ValidatePacketSize_mF48FE6D020DDE39177F34A7D29A3A1A9294686FD,
	NULL,
	NetworkConnection_Send_m17F96D002C2EB4DEA0187D7E03AD01D8171ED346,
	NULL,
	NetworkConnection_Update_m689B01F67CA54136724EC3FFAADF8443F50DF267,
	NetworkConnection_IsAlive_mE03A9D79115AC77D8B030E010E9E3D1C96346FF3,
	NULL,
	NetworkConnection_ToString_m7E1111EE75534CAA3E357A53F6F26CCA5853ECAB,
	NetworkConnectionToClient_get_address_m8570CE0FFF752E1CE4F8FB6162C4A7967E47D6C5,
	NetworkConnectionToClient__ctor_m5705661C93CCE87C4BFDDDA27B81B805E3A36FB2,
	NetworkConnectionToClient_SendToTransport_mF9447C215A986A1C69C2F39DD722A6D5ADD09621,
	NetworkConnectionToClient_Disconnect_m7CBA0B3910013ECA4202640E695D075087C92105,
	NetworkConnectionToClient_AddToObserving_m8FE2E80120C2DDA4922C7D9F2452DDC2D4B61B1E,
	NetworkConnectionToClient_RemoveFromObserving_m147BAC8819C87D0E7C967417351F73F59D5D3914,
	NetworkConnectionToClient_RemoveFromObservingsObservers_m85C96591B6A2B711620EFCFA85744852AD1BD081,
	NetworkConnectionToClient_AddOwnedObject_m8B35737E33A7C95D361138D6B5C3C12EC399B2D8,
	NetworkConnectionToClient_RemoveOwnedObject_m483A0546623A94C76D75B2005147CE00E75B96C1,
	NetworkConnectionToClient_DestroyOwnedObjects_mE55FE0D8D78D31C26A6F3A1489B515A6B76BD0B8,
	NetworkConnectionToServer_get_address_m8B37BCF6C31A40567AE58CA9A644E3432F46DC06,
	NetworkConnectionToServer_SendToTransport_mE4BD3DEE8AA367C7A4BA432325082665F1F372A0,
	NetworkConnectionToServer_Disconnect_m92ED30CAAE84B3FAEEAFA120D03C66378D38D0A7,
	NetworkConnectionToServer__ctor_m982DBB4E1EA531A16CC61CEF4B54F913CA097097,
	NetworkDiagnostics_add_OutMessageEvent_m06B3033E18E9C2811FF72DB202554A4BF09759E1,
	NetworkDiagnostics_remove_OutMessageEvent_m7316455882B3370FF74F491EE02FD464B9B73322,
	NetworkDiagnostics_add_InMessageEvent_mFEBE1B764A7ADF6E838AE259D4B63D624C3C20A3,
	NetworkDiagnostics_remove_InMessageEvent_mD1209731C202D8C3F619CC398B1F1D8B390DBD90,
	NetworkDiagnostics_ResetStatics_mA451C96B23DF641867953F3380CDD828E63E5007,
	NULL,
	NULL,
	MessageInfo__ctor_m544E158986CD48130C1E1E2B68040337F0CA475A,
	NetworkIdentity_get_isClient_mB6F508ADC9D7A1B2954D7823D2F738D30F5B0EDE,
	NetworkIdentity_set_isClient_m674CE14E426E5D1BE3FA77E655C4C299C05DCF5B,
	NetworkIdentity_get_isServer_mAA48E62EB023DA2D16BD28AA5D39D379F6F1702B,
	NetworkIdentity_set_isServer_mE7E2B9C334AB5CF91002A72B1A8A92AF07E39298,
	NetworkIdentity_get_isLocalPlayer_mB8532B381D7C538BC46B0D00BCE2B452D4A5E193,
	NetworkIdentity_set_isLocalPlayer_m7980945B063603AA269C0402D578EE7C5186E7F4,
	NetworkIdentity_get_isServerOnly_m6B6B74787FF82A2B113AF18513BFAB9673B17934,
	NetworkIdentity_get_isClientOnly_mBA7207D11B2CAF0966781C0103C06FD48F416C44,
	NetworkIdentity_get_hasAuthority_mA4E58E41DC978348D08F453B3AB6CAA9224A4C3D,
	NetworkIdentity_set_hasAuthority_mCF0C9F9DA1852CC2A0A1CE9CCC3EDF661444B7DC,
	NetworkIdentity_get_netId_m2F3E61E85597FE7EBAB83C1E461F42BE772AE352,
	NetworkIdentity_set_netId_m9155E92077AFC1FC51932238D983D5270664794F,
	NetworkIdentity_get_connectionToServer_m518AA8B59164E33A77BC72F3539BD5E43631C161,
	NetworkIdentity_set_connectionToServer_m144247A9C92582B4B0AC47E955CD8BC50653E2A5,
	NetworkIdentity_get_connectionToClient_m268D25CA55CD77C44173982E559901D9D59CAFC1,
	NetworkIdentity_set_connectionToClient_m4A52B8CC08863B11F8DA4F6C6B35233618BD1E6D,
	NetworkIdentity_get_spawned_m6A36A3E6B0CA084CA1F1C0220DD3D7BF1C0006E8,
	NetworkIdentity_get_NetworkBehaviours_mEFEAE71AE3EE53192B6BB9E431CBEA375B47E448,
	NetworkIdentity_set_NetworkBehaviours_m9AF9E5FDE4252B906D34A4F4F51CAC0A841BFD21,
	NetworkIdentity_get_assetId_m734370E8D5AE546B5F844EF99D03EE59268404F2,
	NetworkIdentity_set_assetId_m7811E6284395457BAB4F89021D0911E5E445487A,
	NetworkIdentity_ResetClientStatics_m594EA3D8DC8356C8EE2AECC843F64D3B04CDD089,
	NetworkIdentity_ResetServerStatics_m540B166F756FB767942580CB9CA340C3C5CFA1F6,
	NetworkIdentity_ResetStatics_m67230A5AE6B41D81EBF8E751517FC358A577D8E4,
	NetworkIdentity_GetSceneIdentity_m59D1B25F0D230035472F73A0A082E270ED751484,
	NetworkIdentity_SetClientOwner_mD63B6C15BB1923625CAC0F7BE62D5D5B55FB792A,
	NetworkIdentity_GetNextNetworkId_m64DBB63473A18B06504EAE89B5B390CAF1E0E473,
	NetworkIdentity_ResetNextNetworkId_m4123AC38348103BC9785EB4660E1A014A591AA70,
	NetworkIdentity_add_clientAuthorityCallback_m17469E78F3DE39097879AB6B245AC029E7B3257B,
	NetworkIdentity_remove_clientAuthorityCallback_m53C01E7C73C884A6BAF29B9064E2DB14A68ECF48,
	NetworkIdentity_get_SpawnedFromInstantiate_m985CAC4CA4FB90555EF34953D578375F79FD0B57,
	NetworkIdentity_set_SpawnedFromInstantiate_m57DB19FA8CCEF2C96367CD84D8F547F30FC6E746,
	NetworkIdentity_InitializeNetworkBehaviours_m0A4BB07C231B34591F0BA5AF6EB4D62772431B91,
	NetworkIdentity_Awake_m61323B6C0742418D53F23F9934BD7E2194724354,
	NetworkIdentity_OnValidate_m843D1663189D3313DA2FAA92ABFA7A9AF178FFC6,
	NetworkIdentity_OnDestroy_m716EA63BBDD4355E95A3D30AA1B633B0C4C3316A,
	NetworkIdentity_OnStartServer_mD44DEEFF99FC21B78458E24E8F120DE511B84A30,
	NetworkIdentity_OnStopServer_m21FB3585CAEC3597F29D44883755D9C491E70668,
	NetworkIdentity_OnStartClient_m5CACC914D370F9C9381EBCE89A79BF8F5D01A00D,
	NetworkIdentity_OnStopClient_m97A59849415C95C7DCB211004E49CB4C990A5E26,
	NetworkIdentity_OnStartLocalPlayer_mBDC795F80A5113A0035B70BFA8428294C98A828B,
	NetworkIdentity_OnStopLocalPlayer_m5D6E1E03F39A8CB01B92D037ADDFDC2AAF3CAB26,
	NetworkIdentity_NotifyAuthority_mEB7883212FEECFB8BDAB85A33802CA369E38D5F2,
	NetworkIdentity_OnStartAuthority_m461122B7AA8C1E2D4311089750D5CB4C895D5713,
	NetworkIdentity_OnStopAuthority_mCF547D01F942E2A31431078C255FA7B734CF120D,
	NetworkIdentity_OnSerializeSafely_m5863548E550C8918AF8C731CE6D0EAB9E1F71244,
	NetworkIdentity_OnSerializeAllSafely_m87914DB9FA0100FF860E6B99F7833F5ECD5D1CBD,
	NetworkIdentity_GetSerializationAtTick_m9495CFBBA40226D903D651B64255302A6AEC79E6,
	NetworkIdentity_OnDeserializeSafely_mB210EB25553E357D0D303C01C90AE3CF846360EE,
	NetworkIdentity_OnDeserializeAllSafely_m7F6FA11EEDE3378A69F9DA0507B8655BA2886EBF,
	NetworkIdentity_HandleRemoteCall_m77F21A52606C6F956261E0C1E75FAA09A647BBEC,
	NetworkIdentity_AddObserver_m9428D53857802CA726B1C75788327EFCC4EC95F7,
	NetworkIdentity_RemoveObserver_mA99E3F9D4E836D8A343886F0D30F254A81FFF440,
	NetworkIdentity_ClearObservers_m44CCE981F8D984092165263D485857CED1BF8488,
	NetworkIdentity_AssignClientAuthority_m4C4D7E0B6C030144F75A0C10551FB19906FFEA1E,
	NetworkIdentity_RemoveClientAuthority_m3C57C692868A72586E80B532B2AB07E2911F9936,
	NetworkIdentity_Reset_m6DA85920C025150079F7E5CBE09AA8AA10E0FF98,
	NetworkIdentity_ClearAllComponentsDirtyBits_m31F5D1D6622E8A2950963310B6437828BC5671BC,
	NetworkIdentity_ClearDirtyComponentsDirtyBits_m4771E50B6E4A43B907A47AC6499D985B67139ACA,
	NetworkIdentity_ResetSyncObjects_m863724056E602514683FC107518CA8882DD41249,
	NetworkIdentity__ctor_m1DADAE2A93FFF224D07F8BCAA13FBF044B3606BF,
	NetworkIdentity__cctor_mCFBBCD563E259413802B2783DE795FB6B8FF29DA,
	ClientAuthorityCallback__ctor_m9C3EF950A999A09BF206ADEF93C35FD75FC29373,
	ClientAuthorityCallback_Invoke_m2ACF61C8B1BB8BE3007906B17B3EE0F5286575F5,
	ClientAuthorityCallback_BeginInvoke_m35E7EAF31A4E4C48F8B96CCB32212C07FF38A037,
	ClientAuthorityCallback_EndInvoke_m5A059C6427D19D10CC8036CB3FFD67F3F681C91D,
	NetworkLoop_ResetStatics_m862994F7557A01209BF1B6B0B3C586CCD3997216,
	NetworkLoop_FindPlayerLoopEntryIndex_mAAAF022FCD5597108F2D8D29F2D18459082721EA,
	NetworkLoop_AddToPlayerLoop_m17DCFB9E363C3F44B05FFB25AC4606059F66E0CD,
	NetworkLoop_RuntimeInitializeOnLoad_mC3BCA1C148F005EFF4F3C240BA148476EB69718D,
	NetworkLoop_NetworkEarlyUpdate_mC237C06D4A13EA3D4250DE8D0E4D363AA866A35D,
	NetworkLoop_NetworkLateUpdate_m1DFB608737B3D9FF90519C97FCF4920565CBE6FC,
	U3CU3Ec__DisplayClass4_0__ctor_m84BCD7C90A38298F65EFB790577EA9D5CEBECC74,
	U3CU3Ec__DisplayClass4_0_U3CFindPlayerLoopEntryIndexU3Eb__0_m84783107DDF53C5C039EA30F605DA2D63F2F0358,
	NetworkManager_get_singleton_m2A1745C1F66546C421D379CBC41C0A1255160348,
	NetworkManager_set_singleton_m1AFBF26DD89AD4BC74A2B804952A5D556356A4BE,
	NetworkManager_get_numPlayers_m4AC2699A1F3A8AF82B937ECF114B6E5990A5C949,
	NetworkManager_get_isNetworkActive_mE7A4388ED7E88A3FD416BCF9C378D2E806B2D2B0,
	NetworkManager_get_mode_mA83D561FE77A5425807F4DC2250C72925A30F340,
	NetworkManager_set_mode_m84F7E022E6FD142079747F97912C1AD095D0CF45,
	NetworkManager_OnValidate_m91EBF8AB55BCE52DBEC6489F45447F8E0C4E8467,
	NetworkManager_Reset_m9E15822E644D5D183EEAC2F42217308DFE4B410C,
	NetworkManager_Awake_mD005B3299BBA31F93D2F4C51E0706B15BFBEB02D,
	NetworkManager_Start_m73C42FA0BF878D979F7744B28F064BD6F3287A66,
	NetworkManager_LateUpdate_m9DBDABB3A5C269F5BC64C4EE0D8DA16D35CCBDD5,
	NetworkManager_IsServerOnlineSceneChangeNeeded_mE5F741C4F6B785A5B60952E57C668A216EB73742,
	NetworkManager_IsSceneActive_m67196E967B8A3A488FAB7C7C2E39AE97E19C70A0,
	NetworkManager_SetupServer_mF7C3000C3996BCD4FE0804E2AA7B54405E15A8EB,
	NetworkManager_StartServer_mCBB645B1BFDB7BA46BF56B9A992FAAF703D86689,
	NetworkManager_StartClient_m3DD3AB5F061A45E1BD40A87227320B53D888A051,
	NetworkManager_StartClient_mC5F5EA047B43C8EEA63B99305D46BA7DFC12001C,
	NetworkManager_StartHost_m20C95C6A3364A01B9ADBBD4338EC8C9C4DCA889C,
	NetworkManager_FinishStartHost_m4F5C8E0A069837D29BD19CB14A464D51B676EF82,
	NetworkManager_StartHostClient_mABE10784393569FC24BD894A9D94C062ADF9BFB3,
	NetworkManager_StopHost_m06C5629970EA21BFA8A1A2CC5B7A8D7C17CD6082,
	NetworkManager_StopServer_m646AEC8CCF261658BCC0320F80A194B35F62AB0D,
	NetworkManager_StopClient_m2B73201A7EF96AD8B41234F3990DEC3258C6781F,
	NetworkManager_OnApplicationQuit_m5D21B806CC3873E8951C5FDA91A387AD26378283,
	NetworkManager_ConfigureHeadlessFrameRate_mFC486B3E7E6B7B0E763CC1DC6507354C9D58E58C,
	NetworkManager_InitializeSingleton_m26E1A31DDB6FA09FC4541EF874395D61E0150458,
	NetworkManager_RegisterServerMessages_mF0AB1F03C0E4DD124828B7E0D340ED9ED49CB605,
	NetworkManager_RegisterClientMessages_m1E34786A270EADB091629B7FF4D89B08AEF99D1E,
	NetworkManager_ResetStatics_mEB380F62F910B847D08610A95D5867F8390069D2,
	NetworkManager_OnDestroy_mB7648E2344ECB4AA952BEA32F30EC244DB2FBBEF,
	NetworkManager_get_networkSceneName_mF3C3AB5E05B2D3EA39F50DC7ADAECBEBD3F933AB,
	NetworkManager_set_networkSceneName_m61A206B02FF89A2CA92B3B04D0F8CBC7A5EA3D2E,
	NetworkManager_ServerChangeScene_mF6130AF5DE927E0DDA7891B610BD56EA5E918023,
	NetworkManager_ClientChangeScene_mF9DD7AE323CECDD9CD3C0065DD1AE3A0DA2D350D,
	NetworkManager_OnSceneLoaded_mED7327F232D62A779C71353138D2F60653510CB5,
	NetworkManager_UpdateScene_mCFAAFCA2EA58A5FECB79AD418690C2F4065302C5,
	NetworkManager_FinishLoadScene_m4AC39BE4A23054791DC543694A56DE05AB6026A1,
	NetworkManager_FinishLoadSceneHost_mBF9C53A75ED7C2BD872A1C129D2D821814AD6E5E,
	NetworkManager_FinishLoadSceneServerOnly_m33E7670B395B6B4A8A29B3D67933631E1415A91C,
	NetworkManager_FinishLoadSceneClientOnly_m4ED332F9E2E9220A706BC3AE94A2D8F10CCE92A4,
	NetworkManager_RegisterStartPosition_m9326262335C4C75B5B7ACF7CF63A91490D6900F0,
	NetworkManager_UnRegisterStartPosition_mA8835D4807A46C7DC634B6BC24B2424F5FC1C8AA,
	NetworkManager_GetStartPosition_mF46E36E0D5A05DA2A52DAE8BA292F50A06D97BD7,
	NetworkManager_OnServerConnectInternal_mB269461DE8AAE99F0937E4BD83D40AF2F60D0DE7,
	NetworkManager_OnServerAuthenticated_m5E39CEC9895897382E72F4D25D1E38746E0CBB45,
	NetworkManager_OnServerReadyMessageInternal_m5D8FDA9D021DD79BC63C5421855561C66A915599,
	NetworkManager_OnServerAddPlayerInternal_m5C28B1719C3CD4C49BA3345E6CD98361017FFA0C,
	NetworkManager_OnClientConnectInternal_mF49127056D1A8A296BE62A4A355101FF182B456A,
	NetworkManager_OnClientAuthenticated_m64C2FC1218F44EB09319E6B6BA8E87B89470E87F,
	NetworkManager_OnClientDisconnectInternal_mF6A5A6A589CE7A443FF056B89269E5897C1FBDBD,
	NetworkManager_OnClientNotReadyMessageInternal_m0E4F365D2B6AA4B8FAC6DE4CED8111B403091772,
	NetworkManager_OnClientSceneInternal_m960E2F97F4864ED48D912126067F958576D01F08,
	NetworkManager_OnServerConnect_m5C8ADFA71EE15F4CE18C0C18F51D3EAB7277E95A,
	NetworkManager_OnServerDisconnect_m4A144E9989D86B923899A6A551A2B7BCD7DE58C2,
	NetworkManager_OnServerReady_m4FD66B15BD939D64C0F3977017F08AB0DA2D5880,
	NetworkManager_OnServerAddPlayer_m0B70F7456F2DF3302E71EE010C9D8B23C1EE40BB,
	NetworkManager_OnServerError_mEE4ECC7016A2FC76DD590D1BD0662A27177E2974,
	NetworkManager_OnServerChangeScene_m7D722E01B19C1ED87D3744EDB367A99496D4E3EE,
	NetworkManager_OnServerSceneChanged_m9B00AC9CA81A59334723A67CA6B87E2E3654D61F,
	NetworkManager_OnClientConnect_mB978DE1B531BF33DD91A02DFC98EC57D58FB6FDD,
	NetworkManager_OnClientConnect_m0226ED8B8BD1F62C19E0840E6DBB40325DEF6592,
	NetworkManager_OnClientDisconnect_m2291970371EAF8E4FD5697CA21EB1F4EC339E65A,
	NetworkManager_OnClientDisconnect_mCD27E8993079E4C7EBA6BEA32D8B5D482A693C74,
	NetworkManager_OnClientError_mCA52D0701AC90E34C118AF2E49FEF6CCAA29E112,
	NetworkManager_OnClientNotReady_m73D5E06347AFC345875B5104D19A45EEFE55C252,
	NetworkManager_OnClientNotReady_mD7CD726A874E101C7FCA4BC02A329743374977C3,
	NetworkManager_OnClientChangeScene_mCD3DF009A1A1BC983B32F3E667589D9FE937382D,
	NetworkManager_OnClientSceneChanged_mA612D8F7F93490EB508C78140D0F8DDBF36E2F20,
	NetworkManager_OnClientSceneChanged_mAB98FE5B7CEDD22199EC46566450DF31AB937F35,
	NetworkManager_OnStartHost_m45761C08246E28887D9EC025A18170524D048081,
	NetworkManager_OnStartServer_m1EC40BD3874A0246CE2B9B633BE902DB18F84CB0,
	NetworkManager_OnStartClient_mD4C0CF1C14BBFBAB489EC1260684BC97343A6F8E,
	NetworkManager_OnStopServer_m25279DB69D43E622F92ECBE53FD53A459846DD29,
	NetworkManager_OnStopClient_m4CF7ED3468D32E66AA28C8B16BE9F32BBFDBC74B,
	NetworkManager_OnStopHost_mEC7AD99ADFEC2B2F77D0DC531E5B94CE2A5C7F5A,
	NetworkManager__ctor_m12FFC2DC0BAB6F279513E0089C6772A115662B39,
	NetworkManager__cctor_mDFBBFAD7B474F3C676BEE84B4B737160A772C585,
	U3CU3Ec__cctor_m8127F0AA36FF769697910076EFFF52A9027B4D37,
	U3CU3Ec__ctor_mF7E023142E316D2D992981606A8C32F1F6913513,
	U3CU3Ec_U3Cget_numPlayersU3Eb__21_0_m36E3A5EF4600AC3A0E5776725D929D6AE0320723,
	U3CU3Ec_U3CRegisterClientMessagesU3Eb__52_0_mD1A5A74DFD4326E03C1F9BA6E4C2667B22559B5A,
	U3CU3Ec_U3CRegisterStartPositionU3Eb__69_0_m90BD1E7DE6ADFFC4AB245B2BB151AAB0057F7856,
	U3CU3Ec_U3CGetStartPositionU3Eb__71_0_mDE1748D7CA1C2E23862F3C7198456B4BBD417A9C,
	NetworkManagerHUD_Awake_mFFFA4148B42612E4B6E17276D99A52599E1E97D8,
	NetworkManagerHUD_OnGUI_mB1FD5FA33A2154F7998B5D4A3BAFC5E438863DA8,
	NetworkManagerHUD_StartButtons_mBAAD89CEFCB346EE68E673CC3994C2285E305998,
	NetworkManagerHUD_StatusLabels_m9232A28A7AAB84B7171F8B390DBA29B51E15E5E4,
	NetworkManagerHUD_StopButtons_m2FEA798C6591A67761B8689C0407E8A81E209390,
	NetworkManagerHUD__ctor_mFD8AF8171DC20F54AAFD59DBAA2066F62DD81429,
	NetworkReader_get_Length_m23094EDDFE84816D0846ED68F1EFEA79220B93EE,
	NetworkReader_get_Remaining_m5375279B452A2CB1DC8DBF0E7237AF09CFDB75DA,
	NetworkReader__ctor_m13108C432FBACE92C884F450773916200C245F81,
	NetworkReader__ctor_m37DA1A425E58E602C5B129E3F8D18AE52C8C7759,
	NetworkReader_SetBuffer_m34DAEF19500AD7DF5A7263E89CAB36D300B111B8,
	NetworkReader_SetBuffer_mB0A7BA6CBD97D646A9EE54873664EB6E105C560F,
	NULL,
	NULL,
	NetworkReader_ReadByte_mB1431799AB014070DABB99C9A8E50C6F20720A9D,
	NetworkReader_ReadBytes_mA1E53425AAD4AD3038C9759F6971533248347130,
	NetworkReader_ReadBytesSegment_mA17220D13799B7AA2EFF9B49C6F1F98B486A330E,
	NetworkReader_ToString_m910E83BEEDE5F1F1FC9B20C1CA0A5906253A6F5A,
	NULL,
	NetworkReaderExtensions_ReadByte_m641FCF4CDF53C5E920C7BE5594421C57AC5A8FED,
	NetworkReaderExtensions_ReadByteNullable_mB622478495C2AE927128F9F196A47DCFEB666E4E,
	NetworkReaderExtensions_ReadSByte_m1B3975CC87DD10621C8A369EA7D053AFE57E958B,
	NetworkReaderExtensions_ReadSByteNullable_mEAB105DEC52D7789AEE2A6E110B66A3C2EB8785E,
	NetworkReaderExtensions_ReadChar_mCCA8829AD9CA54D8510AE4C3E3D1CA0F6F6E8966,
	NetworkReaderExtensions_ReadCharNullable_m728E4E8F336F06A0BDB8BDEE69842C707ED4540A,
	NetworkReaderExtensions_ReadBool_m7D03AC08FE76C37F5B589F938D160E350A0B1D06,
	NetworkReaderExtensions_ReadBoolNullable_m49866827FF66A52CA4E36AAED2D3ACC8766F8B38,
	NetworkReaderExtensions_ReadShort_m5FFC8A9D90AE04D1D0AD681F1D2C32564DBC8677,
	NetworkReaderExtensions_ReadShortNullable_m6B7ED0AC2C951C2461A34D3DEB05E6055399B896,
	NetworkReaderExtensions_ReadUShort_mA98395DD1B1DA249096858B171B8BC23D95DF765,
	NetworkReaderExtensions_ReadUShortNullable_mEDDEE70BF7A15DC1503C4BDF580F54A26C82DCA4,
	NetworkReaderExtensions_ReadInt_m406611BCB16DBEFF29DFC581343BB533C103309A,
	NetworkReaderExtensions_ReadIntNullable_m9F68CD73D47D10DE2E1C6934DE14234E19D02E71,
	NetworkReaderExtensions_ReadUInt_mD2C8C8222D61D79A08D3F9C333BD74DA46CB02C4,
	NetworkReaderExtensions_ReadUIntNullable_m3D4906C1707F48E5439F4EA0E9DE4B8860AB2E73,
	NetworkReaderExtensions_ReadLong_m67D408F9D8D9FB04A0101AAE2AB9B01120E34435,
	NetworkReaderExtensions_ReadLongNullable_m747B938C128B0CAD7E22D0909E1AEE9DFDB54F67,
	NetworkReaderExtensions_ReadULong_mAC6B83521EBA7FFEDFC72A6AAE1BF5D87221A5F5,
	NetworkReaderExtensions_ReadULongNullable_mE853C2A40E3E7F9FD1BB49D5E16BCB9310B0752E,
	NetworkReaderExtensions_ReadFloat_mF3D9834531FC09112A506971638FB9682A231D97,
	NetworkReaderExtensions_ReadFloatNullable_m1EB56AA1F1CDB7981728CACF5941EB0B6B4275BD,
	NetworkReaderExtensions_ReadDouble_m949A60A21C6EB3B9952A43355903F08B3A7E0EF9,
	NetworkReaderExtensions_ReadDoubleNullable_mE52DB83CB818F30F912FD40175B39731A2FBD33B,
	NetworkReaderExtensions_ReadDecimal_m79DE6589996D493A3A95BAD98036B09FF9CB144E,
	NetworkReaderExtensions_ReadDecimalNullable_m18D27D0176D98F043EC804A512EB8B55856229E9,
	NetworkReaderExtensions_ReadString_m5A56179D2C1CB509EC9C762F97BBD4133A5AD394,
	NetworkReaderExtensions_ReadBytesAndSize_mB707572AAF6CBDE9E6FAC190629882468EAFAD8E,
	NetworkReaderExtensions_ReadBytes_mF2B3E392F976B37C12A9BB81DBEB98726813730D,
	NetworkReaderExtensions_ReadBytesAndSizeSegment_m2A5CBBE40FFF54ABD370AAD5BE5C37990C56738F,
	NetworkReaderExtensions_ReadVector2_m673B821E39E194BA5E2B7E5F444D6CCD76812811,
	NetworkReaderExtensions_ReadVector2Nullable_mCDCE58B581701AC12499A36355838E45F298C817,
	NetworkReaderExtensions_ReadVector3_mD35BF8B14DD5F75688AB9C360D138D1BAB432637,
	NetworkReaderExtensions_ReadVector3Nullable_m17D39303F570FAB53014718C07327F9DAAD8DB18,
	NetworkReaderExtensions_ReadVector4_m7870D12D4D86684F68719E7F040A33A085C2F1D4,
	NetworkReaderExtensions_ReadVector4Nullable_m1303DE93C2EB13F32622A8B868B17610B8C4AD09,
	NetworkReaderExtensions_ReadVector2Int_mC9CEB6A103CD7C5DBCD8A944A57A59C0D1311F25,
	NetworkReaderExtensions_ReadVector2IntNullable_m698F001AADBF901CE9571E3AA5687DFC1DD65701,
	NetworkReaderExtensions_ReadVector3Int_m59BAA3EBC52DB1635EA840D23B9D4A011E480E3F,
	NetworkReaderExtensions_ReadVector3IntNullable_mB88012F753982406CA6C49E8440318BFF784AF97,
	NetworkReaderExtensions_ReadColor_mC5D200708B20F2ADC42224245960E2ED7E5DD27A,
	NetworkReaderExtensions_ReadColorNullable_mDA6AADFE45C4CE1364429EACA43199CB319C9065,
	NetworkReaderExtensions_ReadColor32_m0F0066C51CACC736B893D9F3C1D4324F87641BEF,
	NetworkReaderExtensions_ReadColor32Nullable_m95A7EDB77042A0B8D6D00D2C96E9A530DEA6AF8C,
	NetworkReaderExtensions_ReadQuaternion_m135F5C523703C700E6A266DA9718E44D160BB567,
	NetworkReaderExtensions_ReadQuaternionNullable_mE4E31E56C486837C0EC9C6047B276C9452D02C9D,
	NetworkReaderExtensions_ReadRect_mA4B7FDD8840C7E3A299614815C36EFB27232AB3C,
	NetworkReaderExtensions_ReadRectNullable_m58AF30FAB6E523648BA18026AD02B220FBDDBC85,
	NetworkReaderExtensions_ReadPlane_m2DA9573A8252F9B24A10E9E1AB448976D9963B96,
	NetworkReaderExtensions_ReadPlaneNullable_m252E55444808DDA4A5CEBCBE440E34728ECA5120,
	NetworkReaderExtensions_ReadRay_mBE12F756FAAA9395B88F69C6A43F8576921AB20C,
	NetworkReaderExtensions_ReadRayNullable_mEE7ECB615AEFA818E73B366F681EA86595CA8F19,
	NetworkReaderExtensions_ReadMatrix4x4_mBB21ACB1A8610F3813CE4A37DBF1608CA31A0E2C,
	NetworkReaderExtensions_ReadMatrix4x4Nullable_m508241752BEC24CCF4BD45230613444685553D06,
	NetworkReaderExtensions_ReadGuid_mCFFAB7379132286F7C9CC70EC291F8B28EA08B0E,
	NetworkReaderExtensions_ReadGuidNullable_m884FD11E39BB14010073AB443D46779317340927,
	NetworkReaderExtensions_ReadNetworkIdentity_mFDB6779F9A77F88F9760FD9902EFFDF3331E62AE,
	NetworkReaderExtensions_ReadNetworkBehaviour_m6D724C97DE822B84C3FF75E80DA169D7C44E5E0B,
	NULL,
	NetworkReaderExtensions_ReadNetworkBehaviourSyncVar_mD3DCF91C73BB12C70E487EA4C4C85EAC62FE8A1A,
	NetworkReaderExtensions_ReadTransform_m56C2AB03C3891F0A72C1FC7153655E7AE4DCD6E8,
	NetworkReaderExtensions_ReadGameObject_m037E8EDDA39F95DA70EE3226939F677F9E3A2EBD,
	NULL,
	NULL,
	NetworkReaderExtensions_ReadUri_m9CB721F84C66F0749E586B02C4CE8E472F266C06,
	NetworkReaderExtensions_ReadTexture2D_m5795D8D017B66A5ED4BDE243E306BD2B77A35EDC,
	NetworkReaderExtensions_ReadSprite_mA5B19DCF570BA845B63AA79858FCFDF27DEAE040,
	NetworkReaderExtensions__cctor_mB413B3E2CD6EC4E8C1AF169D00937F17CD8CE6A2,
	NetworkReaderPool_GetReader_m284C1E11AA341F1B8BDEE1C4D401AD1154F33027,
	NetworkReaderPool_Get_m38A5E8CACDC8BE64A6EAA54168354FA381604EB2,
	NetworkReaderPool_GetReader_m3B088EE21A28E6836FA65A6DF78F4FA60DC241C9,
	NetworkReaderPool_Get_m0541C01FF0A785443A15550421E8F602494268E2,
	NetworkReaderPool_Recycle_mF73407194562635C193A6734F0BB3F974FA342C8,
	NetworkReaderPool_Return_mE2123C0BCBD7AAFD0AACC205825CB4186E337943,
	NetworkReaderPool__cctor_m2F327E4A2664A358E0B9055711495B2C9258D059,
	U3CU3Ec__cctor_mBA59E1F5D553F890D5D4F59546BE86F0939C1315,
	U3CU3Ec__ctor_m916C5368F0019DD3772A7BC5E54198159022F6D1,
	U3CU3Ec_U3C_cctorU3Eb__7_0_m9C3B1B12EE7DA18B70B08EEE869E0447C48B16E2,
	PooledNetworkReader__ctor_mB96ECB000E753BCEEE9FE0919886A00B195163A4,
	PooledNetworkReader__ctor_m83AEA1199CEFDE5150BC6B7A76E1C2A55E53B166,
	NetworkReaderPooled__ctor_mF52AEE8ECFAFCDEB7925D9AE2304237047D72580,
	NetworkReaderPooled__ctor_mD3C141EEC531CFB236165C76C1A7588A12A7B40E,
	NetworkReaderPooled_Dispose_m21DF73835F65C8F21C45A19CFE6EC69878155904,
	NetworkServer_get_localConnection_m40B5A3EEB31B11F8CB674641AB821E15174DCBE8,
	NetworkServer_set_localConnection_m709B9720203BA015FD94BA59BE74D2A98EDD45AA,
	NetworkServer_get_localClientActive_mF819059FAC02CCA8DCAEDEE3D7D9A0F4EB077846,
	NetworkServer_get_active_m02AA11D3F948B9BFA050605CD3E87E63E6550366,
	NetworkServer_set_active_m4596680F3A5532993D5D3C6CE5B5FFC12E4813D2,
	NetworkServer_Initialize_m6871B65AFA8F4F610FE036F189207FC915EC5A85,
	NetworkServer_AddTransportHandlers_m9765DEA51A841B7E0A4ABC03B946419331D170DA,
	NetworkServer_RemoveTransportHandlers_m0D20717DD65BF9B4E41188613C50BE814D50060C,
	NetworkServer_ActivateHostScene_mF852F057E875C8833FE2FD26ED3C2F3A47A0D20F,
	NetworkServer_RegisterMessageHandlers_mAF8745D6832E7EF985180BEC5B13613BFAE218D4,
	NetworkServer_Listen_m82139C5B724653F4EAD3496B1794C0242D4A8A7E,
	NetworkServer_CleanupSpawned_m73DDC30ED37098FEDC46FCA35694291263F8CE88,
	NetworkServer_Shutdown_m30813A8EE5A551909EF61EACA4D372749E5EB039,
	NetworkServer_AddConnection_mD6EF1BF4690B0AC740924593054229238F49781F,
	NetworkServer_RemoveConnection_mE06195F4F9FE6351A40444E5D137829B16E0E6F2,
	NetworkServer_SetLocalConnection_mB56448E297F019C2A2399C1C375CDA4C4A1D4061,
	NetworkServer_RemoveLocalConnection_m455A66246FA698E48D4D43F2B3EFE3BE9EC7A9C5,
	NetworkServer_NoExternalConnections_m0ED59D7BC18A1ED70361C76C7D7F2DE111A87EB3,
	NetworkServer_HasExternalConnections_m6468C27F72B226564A0EDC6A9A4D1F51E92541D4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkServer_OnTransportConnected_m94F2EAABC0F07A1F36FD505605324AC1CC56DB94,
	NetworkServer_OnConnected_m9C4A85871A17A3A86E365A39AF957C7AFBEA273E,
	NetworkServer_UnpackAndInvoke_m4B6E7FEB9A01011B2AAFBF0E486E9D55FB543DC1,
	NetworkServer_OnTransportData_m23B9C654356FC9103C7F57BD6EC4039D74255410,
	NetworkServer_OnTransportDisconnected_m3F5D43F44545761D433E6BDE2FDAC6D6BEBC73B5,
	NetworkServer_OnError_mDA567F37A36B4CBA1E4B039407DBF31ECBEA4AA5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkServer_ClearHandlers_m0C0F8BFF5E1284DD5B61F28393FD5C5C4E03E5AF,
	NetworkServer_GetNetworkIdentity_mCBE394DAD21D016B63C30823867ECF523D0C0FB1,
	NetworkServer_DisconnectAll_mDDA6465FF641ADDA7AD023B5A9D21519AB3398AA,
	NetworkServer_AddPlayerForConnection_mEDEF1DD18D9C456FCAE4E3E2EE9619EFBFCD4BBD,
	NetworkServer_AddPlayerForConnection_mD565274C11AF7B36CCFEFAA8587ABC39F938ECFB,
	NetworkServer_ReplacePlayerForConnection_mB7648580A5E9A9C61007F281014225D02A1097F6,
	NetworkServer_ReplacePlayerForConnection_m20E3D0682EBB6F52E314884534942A55CF2BBD31,
	NetworkServer_SetClientReady_mFE7E42EDC31007D6C9568A7F61A4711B0CA3D0AA,
	NetworkServer_SetClientNotReady_m11D97BC7786F9B8681B8CC26669B81C242A9B8F0,
	NetworkServer_SetAllClientsNotReady_m07D99564B0F1F79F8772EBACDC9AACC020075B0F,
	NetworkServer_OnClientReadyMessage_m3F3511144416E643FA74ECDB280764DF9771979A,
	NetworkServer_ShowForConnection_mD80C701D4A442A1522FD00CF32B43D7AD09A58A0,
	NetworkServer_HideForConnection_mB8F94AB7B27FAD53D442340EB3D234D840BBA876,
	NetworkServer_RemovePlayerForConnection_mDBCB11BCF28386DF483FA2E5D1C44CAD4AF70855,
	NetworkServer_OnCommandMessage_m7D5C5D2861CE44343722A2E62450668DF28AFDF6,
	NetworkServer_CreateSpawnMessagePayload_mAA41C9557E0403B56221B1AAFA86208DC58B959B,
	NetworkServer_SendSpawnMessage_m981BFA48A52392FA34606B46FB0E99D916F5C0C9,
	NetworkServer_SendChangeOwnerMessage_mD12DFD8BCA1C742961402DD3FC0FE8AFDFE8F640,
	NetworkServer_SpawnObject_mAE23DBB87E27E4BA0FDA61F7E359A2EC2437EFB2,
	NetworkServer_Spawn_mD8A2B40ED6ED7B29F042AEB364DE9B42A0B7B4D5,
	NetworkServer_Spawn_m97D3AE543876CBF3E01B42A97A2B6D17828AB794,
	NetworkServer_Spawn_mB086C140CE33E27ACE4B1B89A76533B0F9D60A22,
	NetworkServer_ValidateSceneObject_m8CDD7FC8B1ED711DDB402FB9E26673E2CF565367,
	NetworkServer_SpawnObjects_m42C2CDF65867CA85100C1C290E532C167B8F45E2,
	NetworkServer_Respawn_m9B3E4BE2F4A7B2B91E527F26E40F0FF63605D5CB,
	NetworkServer_SpawnObserversForConnection_m46F922FAE51EA2161AAC8338B727C5B61E798D9C,
	NetworkServer_UnSpawn_m37AA67C164C614C83BFA7ED3E8804B0EEE676FC6,
	NetworkServer_DestroyPlayerForConnection_mEA5F898E778D7A91B6023D6DD07AC379D491D50B,
	NetworkServer_DestroyObject_m85D899225F139DD18E09E82E1FCB966E94E4216C,
	NetworkServer_DestroyObject_m8B9D77FFA8A27D82DFE1BC7726E9C2305905A676,
	NetworkServer_Destroy_m8F0C7AAD5958C88297A46B039C4E0EF3CE99043D,
	NetworkServer_AddAllReadyServerConnectionsToObservers_m65E19A5724975F1302178308915BFBA8FB6BF7BB,
	NetworkServer_RebuildObserversDefault_m5C50544D7EF5C55ED9DBF35F1BEB5671E28D590A,
	NetworkServer_RebuildObserversCustom_mDF2B659ED84D57D583758EA8845179C4CFDABA13,
	NetworkServer_RebuildObservers_m6B7D242A0FDDBAD4CD37E9D2FBB169DDF91E9875,
	NetworkServer_GetEntitySerializationForConnection_mDD77A126FB1E080BECADC02FA40BD7895A5A2EEF,
	NetworkServer_BroadcastToConnection_mB9BC1439CFBD326724CF8A92B0FC961896BC145A,
	NetworkServer_Broadcast_mBCF5790CDB8DFE1A4154CE642C6DA40A4384B186,
	NetworkServer_NetworkEarlyUpdate_mC91E0A4BF9B7CD237F98D58408FE16BCEA771680,
	NetworkServer_NetworkLateUpdate_m718B5D1E6624E050F39EC8C919B414DF133AB2E4,
	NetworkServer__cctor_m77C0CD1A02A7F1E5EA424823ACEB853BE0E5F74D,
	NULL,
	NULL,
	NetworkStartPosition_Awake_mC9554712794D10914CB8E0595B8854B990ACD7A5,
	NetworkStartPosition_OnDestroy_mF14AB964069CAD898B311772B863D99534262741,
	NetworkStartPosition__ctor_m783B168BD36FAA13CC631E4897FB5C5ACC783BF4,
	NetworkTime_get_localTime_m48FFA7BD2A87D9EC965782C7484F66D9A042FB89,
	NetworkTime_get_time_m44D7378ABC9CB592F924B900BD9E0F65643EEE3E,
	NetworkTime_get_timeVariance_mA61BCBE156273D70A378DCA86D5B83CC9C3A403E,
	NetworkTime_get_timeStandardDeviation_m47FEBDA6C53D5DB7C3FDCDFAC137FBA2467C7768,
	NetworkTime_get_offset_m41902185659B32B40F184129F46845A70F54F4D2,
	NetworkTime_get_rtt_m63EB7A06EADCDE66B98BB27386ED0E3DD4A26E5D,
	NetworkTime_get_rttVariance_m9BCF4BD5EB001DFC67EBF45ADF98F3234F5FD0A3,
	NetworkTime_get_rttStandardDeviation_m0D0E0219C4A261AF5688955085AA18630B2BE9EA,
	NetworkTime_ResetStatics_m2DC91BF81E155310FA18829B21D1DD388AE9BC43,
	NetworkTime_UpdateClient_m5011BE5B67EB82E7725B546CE327C36B0608EA1C,
	NetworkTime_OnServerPing_m3C4C1E1D94B3B73294A49CD5A632E38AD8082631,
	NetworkTime_OnClientPong_mF053F60935DBE9188E5D117C898A273B16229447,
	NetworkTime__cctor_mFD9C036DE68DFDC4CA95FCD62E83D51315407AFC,
	NetworkWriter_Reset_mAAB03983E0061CB0AAC0EEEFBDB8D777CB1B191D,
	NetworkWriter_EnsureCapacity_mCA41CB950C5E89DCBAFCE7D00E588D21858911D3,
	NetworkWriter_ToArray_m3E85E9234DA5E83A2574340D0F8AE06AEB6E4184,
	NetworkWriter_ToArraySegment_m35FBADDD2990B92709B5B3277F7A982140189135,
	NULL,
	NULL,
	NetworkWriter_WriteByte_m7F7BB1B62F8DCB5BE680355A4BCAFA3BD44E54C0,
	NetworkWriter_WriteBytes_m0F3058BA3B1B973C3D99B647DA231D9E82AFEDEC,
	NULL,
	NetworkWriter__ctor_mD67C3954391867E0C776DCF1ED04C711AFEAAC5E,
	NetworkWriterExtensions_WriteByte_m237D7B1A984B9FE51BDD8E0C814AA8E55A50A5F4,
	NetworkWriterExtensions_WriteByteNullable_mE34CDDB5354D7536941F1AB0DB0EFA4E3269C5A2,
	NetworkWriterExtensions_WriteSByte_m777D700EE0D8256617BE1128DE65C2DEBF674EB3,
	NetworkWriterExtensions_WriteSByteNullable_m3330A77E2E4D2AC1B90BF53BC8150063BB6F8B30,
	NetworkWriterExtensions_WriteChar_m79E8B11FA260E5C83FAAB385A039B9B73F4E15E7,
	NetworkWriterExtensions_WriteCharNullable_m3D59A08FC508A0A1469115A766844A3DC3F1E420,
	NetworkWriterExtensions_WriteBool_mF5C2F44E715D3E40D9CD29CFC6922287E802AB45,
	NetworkWriterExtensions_WriteBoolNullable_mFB1ADF7E798F7991680382003FA5584DA972EBBE,
	NetworkWriterExtensions_WriteShort_m8593C0C47C9EADF1A65AA97BCBA9C15BF3739089,
	NetworkWriterExtensions_WriteShortNullable_mB84CEDD1AF6243DABA5C235B4013AD5A1801BE6B,
	NetworkWriterExtensions_WriteUShort_m4AEC8147034117F9EB131043089577CD2DB42DB4,
	NetworkWriterExtensions_WriteUShortNullable_mE77F289B55D295E545826AAEA6CAAFCD26FA11A2,
	NetworkWriterExtensions_WriteInt_m4DA80E8C672B3E1891FF1A8A921C6EB94C14EB12,
	NetworkWriterExtensions_WriteIntNullable_mD476FA9C8F66E723E823733776EC840B4DCE6FAF,
	NetworkWriterExtensions_WriteUInt_mD3BFEDAC70C800B5517A81C01CEA93BD83B9F4D1,
	NetworkWriterExtensions_WriteUIntNullable_m5C618D0B2F565D4C20CACAE51E3D8A9AAA7EC3A4,
	NetworkWriterExtensions_WriteLong_m631751934892884B4E8B0FAF18BC616ADBAE1E90,
	NetworkWriterExtensions_WriteLongNullable_mE9A8A93B2C853063398E244A3FA8342BB51D0C5C,
	NetworkWriterExtensions_WriteULong_mC0AE4801C58209EF02B73E3B353100B3AB95D28C,
	NetworkWriterExtensions_WriteULongNullable_m5CD68A058B65F0F3B8729DEE05D43A94E222055E,
	NetworkWriterExtensions_WriteFloat_mA3AEF60E8288F55D5A3365AA0E4730AFFC231050,
	NetworkWriterExtensions_WriteFloatNullable_m0F2D06A7FA1A84F3F3C54537D6A3ABAB3206585F,
	NetworkWriterExtensions_WriteDouble_m0475F5FB9E1D69D60501C7158AD3431680BC1BDB,
	NetworkWriterExtensions_WriteDoubleNullable_m91EB95539CA35FAA383E01BEFA894A346A218ACA,
	NetworkWriterExtensions_WriteDecimal_mFDD008D98CD77D9B4E63EF9AE0421FABAE70F483,
	NetworkWriterExtensions_WriteDecimalNullable_mACB5EA0A0661A694C8521C3C245AC9EE3ECAE1DA,
	NetworkWriterExtensions_WriteString_m3EAD86845E4C7F8503AE059DFB4862D1159EBC98,
	NetworkWriterExtensions_WriteBytesAndSizeSegment_mD7FCDC44AB0313C3ED0AFBFEC77366B6947672AC,
	NetworkWriterExtensions_WriteBytesAndSize_mD5E8FA492EACCF5C68D0E76D84C20689CDAA0F27,
	NetworkWriterExtensions_WriteBytesAndSize_m3A29964C9A4F7D85B49358324A238EADA7DE57BA,
	NULL,
	NetworkWriterExtensions_WriteVector2_m5C9C94ECCE2643B670009D710BA8D6A2434F8BA5,
	NetworkWriterExtensions_WriteVector2Nullable_mF82E294E5D5AB3D06DEA7404DAD4C9430D89C728,
	NetworkWriterExtensions_WriteVector3_mB3C2B2F2D3C9874F883C12813FB20B9D5AABC882,
	NetworkWriterExtensions_WriteVector3Nullable_mD35B4E68313CFC87EF7B80823D9F0502C4D63E62,
	NetworkWriterExtensions_WriteVector4_m710FEA287EE2C56C2C7DA468B394D23FE2424023,
	NetworkWriterExtensions_WriteVector4Nullable_m225B849A988CD2861387E8368F780E90E3D956CD,
	NetworkWriterExtensions_WriteVector2Int_m0099C36CFAF8015034E1CBC4CFCD7623543C758F,
	NetworkWriterExtensions_WriteVector2IntNullable_mDAC15DF3BA2A0FDA9705D0D35A3C4F486D2DDFA5,
	NetworkWriterExtensions_WriteVector3Int_m2A6D52133117098B0C8A65520CBEFF8C4297B47B,
	NetworkWriterExtensions_WriteVector3IntNullable_m6EDE27130713A9C3A4012DCE58D53E488EE7E36A,
	NetworkWriterExtensions_WriteColor_m142E05754268CB4F297199994A61605D0FF1D9A2,
	NetworkWriterExtensions_WriteColorNullable_m0B728B0EF504CC3FB0CE87FAD1505794AB82CDBC,
	NetworkWriterExtensions_WriteColor32_m123810E64991275156516FBB8AD2CFA67A7C3B7B,
	NetworkWriterExtensions_WriteColor32Nullable_m3931F587C14E96A05B25E3446B6F7AE6D81115C0,
	NetworkWriterExtensions_WriteQuaternion_mFC2E046965F6BA1B694218D5E60E26807974ACBA,
	NetworkWriterExtensions_WriteQuaternionNullable_m7E68536A12BD33C4E0D063841FCB8B3319CE546A,
	NetworkWriterExtensions_WriteRect_m52D47BD93F73E06FB131C75A78127E3CC9073093,
	NetworkWriterExtensions_WriteRectNullable_mD90246AB0237C3D9B3D669CEDFF1548D9DE26364,
	NetworkWriterExtensions_WritePlane_m5BF0BAF633E94AAE16D6D7E44B78E474E601077F,
	NetworkWriterExtensions_WritePlaneNullable_m1D7DE145477804CB9A5F078D51F2C31408A9CA2D,
	NetworkWriterExtensions_WriteRay_mE3C68E64E43515730710198FF05734D077BDBEBA,
	NetworkWriterExtensions_WriteRayNullable_m4C9EE3A8F4B5A24523EE0A02827A481F300A7C36,
	NetworkWriterExtensions_WriteMatrix4x4_m19A3B92281557AC6E231E3B5C663ACB8366CDFE2,
	NetworkWriterExtensions_WriteMatrix4x4Nullable_mEBA4E2383B008575F3957933958627DEE30465D7,
	NetworkWriterExtensions_WriteGuid_mFF6C6A1BC90A9A7BBC9C179FA6FC25753689D3F9,
	NetworkWriterExtensions_WriteGuidNullable_m19C9499197D2DEEB57A46D81FC9993D6EBFA14D9,
	NetworkWriterExtensions_WriteNetworkIdentity_m670598EE39418EC82E5A35DD60EBDA69D7B8A74A,
	NetworkWriterExtensions_WriteNetworkBehaviour_mDEE6FB11729AF7833D749E1C0573A559113E26A3,
	NetworkWriterExtensions_WriteTransform_m2F65EBB30598661EE20259C40E58691589593CD4,
	NetworkWriterExtensions_WriteGameObject_m1B7DE5CB70EE416C894BA361CE421473734456AD,
	NULL,
	NULL,
	NetworkWriterExtensions_WriteUri_mFB6E40C094D853A44F750835EF778B567546D775,
	NetworkWriterExtensions_WriteTexture2D_mFA5FE217BF0E9D2F1CBF3A50C7A0B9C689B79782,
	NetworkWriterExtensions_WriteSprite_mC849B7B1044D0DC1989BC8F5A77DD93CDB7B0C82,
	NetworkWriterExtensions__cctor_m4DEF722CAC01E27861468F3E7DF75E30DA6053DF,
	NetworkWriterPool_GetWriter_mF0156A907C671F91742AB03A6B28F429ECB98450,
	NetworkWriterPool_Get_m646E4F6C23D26C7972C41F3896E77BEB6B63D3FA,
	NetworkWriterPool_Recycle_mEABD0D229387A34AD7AC58CE838FAF8C765DCB62,
	NetworkWriterPool_Return_mFA32A7C1E0D86429F3B60364445BC6867894D837,
	NetworkWriterPool__cctor_mADD04829DDD342A3B2F45599DB6F9E24F3714A0A,
	U3CU3Ec__cctor_m8E31B109427F127868DC4A9CC80423289F8F7925,
	U3CU3Ec__ctor_m156EE2936091B7F9B9C4755D7689C6AC5A15B908,
	U3CU3Ec_U3C_cctorU3Eb__5_0_mE87A5D1CFBABE41A31D4C9C569B739484ACE7A1D,
	PooledNetworkWriter__ctor_m03300CFF65FD836ECF916D67D9E9EC307AAC4B5E,
	NetworkWriterPooled_Dispose_m8514B01116425746A2736F81802E0D8388C34EA6,
	NetworkWriterPooled__ctor_m282C66453866D943B7D2988B2FFA6D5DF1E5C2F4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SyncObject_Flush_m5C0BD574AC5AB2581F7934A964C5B180AC0A99CF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SyncObject__ctor_mA548CB7517A8DD0802BDD878A1828C692F610258,
	U3CU3Ec__cctor_m64DD28DA9E9B5A885C12A60328F3648805DA4382,
	U3CU3Ec__ctor_mA38180FC1A2E2D4C8B450FDCF4DE75C2C290CDFD,
	U3CU3Ec_U3C_ctorU3Eb__9_0_m8E16FB94F6784CB289430D8550213C98972127F5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SyncVarGameObject_get_Value_mCE863D617F089FC2272FE5AB3D1A43317A9BC11C,
	SyncVarGameObject_set_Value_mA55A0618A702B53CBB03308DA01ECFBD6F8A534B,
	SyncVarGameObject_add_Callback_m5BEC6532748DAFFF8F5C9EF28F12D8F671BDD484,
	SyncVarGameObject_remove_Callback_m71688C4CBE55498AB5091A04961B3F6101499610,
	SyncVarGameObject_InvokeCallback_m0B64C81B474E4F2E8321B0E99701CC7201B0C68E,
	SyncVarGameObject__ctor_m8377609DC058D8D57CE2D5E6A391A3A4501944D1,
	SyncVarGameObject_GetNetId_mDDFDB2CFFDFEAF59FE6E45139D34A469E63C4EBC,
	SyncVarGameObject_GetGameObject_m574C3C357CDC5CCDE2CB6B9BBCC0314452A72749,
	SyncVarGameObject_op_Implicit_mE0FEEE46FAEA6EC1B32E6523D2A391CC6BAC585E,
	SyncVarGameObject_op_Implicit_m695B8ABF1927DAC3FED0B7F01F000FA0575906E1,
	SyncVarGameObject_op_Equality_m066B432FAFE578B1017E263417D885833D16E673,
	SyncVarGameObject_op_Inequality_mA2FC837AD1E9481E8FA0E40ABE587E2FF01A8F9F,
	SyncVarGameObject_op_Equality_m24C28828F846C575A2452784DADC64E5F066E5D5,
	SyncVarGameObject_op_Inequality_m55E79A1F6FB893A0DDC2388D0E2D1A5C53A725D9,
	SyncVarGameObject_op_Equality_mCA9386853FECA9CEA6CA94DF0AF1375A0EFFAA4C,
	SyncVarGameObject_op_Inequality_mC2AF9F047A7BBA6E212D2EE32343963CD5FE712C,
	SyncVarGameObject_Equals_mAFDB094F055CED564D553EC1D925A38085E749FF,
	SyncVarGameObject_GetHashCode_mEA0C031E08FDC724791D53B09D662D50433A2ADB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SyncVarNetworkIdentity_get_Value_m4A5B6029A660F63E83BC9CD1FD156790D4362819,
	SyncVarNetworkIdentity_set_Value_m58E2BB65D54E259F0F9F30C288FAF8A34AF53045,
	SyncVarNetworkIdentity_add_Callback_m2DE9BE87A8455420C11AA999274F5D226735DCDA,
	SyncVarNetworkIdentity_remove_Callback_m28AD6AD811BDFBD270559002550355EE1C06CD27,
	SyncVarNetworkIdentity_InvokeCallback_m6D000155BFB12EEED8A2D46C2F05EB225667E9B5,
	SyncVarNetworkIdentity__ctor_mBAC777A21EC23B1907A8458C0DFFB2E7DA6F7D90,
	SyncVarNetworkIdentity_op_Implicit_mC090F6DABD968B07E42E88BE67D7AD8104C28BF3,
	SyncVarNetworkIdentity_op_Implicit_m8F267E2B15BA1F663B8D05AB2B26585A79AA2B6E,
	SyncVarNetworkIdentity_op_Equality_m9C151063AF8154BCF536CCDE1A1D97CB1D5F7CF4,
	SyncVarNetworkIdentity_op_Inequality_m73E5C7EE416394B035810F45C4DFB979EC7DB0B7,
	SyncVarNetworkIdentity_op_Equality_mEA3911DF99BCF9620BC2B58CC097CAF22F4A2F22,
	SyncVarNetworkIdentity_op_Inequality_m3A8A9CD1B021422607F52C8D27A3F6FD62DF80E6,
	SyncVarNetworkIdentity_op_Equality_m3EF4EBF0C92E8EBB64BF873ECCC08103C6F97572,
	SyncVarNetworkIdentity_op_Inequality_m6F9178AAD975B9E4B57079BF4B8BF13311C3BA10,
	SyncVarNetworkIdentity_Equals_m65605646E2B00333D97FCEF8705FC1C724F0EA7E,
	SyncVarNetworkIdentity_GetHashCode_mFAF16D42E2955CB08324D620546E9383E1B907F3,
	LatencySimulation_Awake_mD0CB059F008E153CE8AC551F9249116213F045B2,
	LatencySimulation_OnEnable_m5192A6777F253107059F1FB3EAAF0F4B5FF4F185,
	LatencySimulation_OnDisable_mF655C8C3F028CC294460A6A957C33C103891EF53,
	LatencySimulation_Noise_m1F3843829053E0C0D23E0666D2A728C8C501EE72,
	LatencySimulation_SimulateLatency_m96CBDEEB67E6CA1C913E7E2FF537E7DE5E1965E2,
	LatencySimulation_SimulateSend_m10A2B28AEE6273662AE3BDA26D889568EA1B3E86,
	LatencySimulation_Available_m58166DC8F9D783C9EAC3ADE7844CE4DDF94122D1,
	LatencySimulation_ClientConnect_mA421FAA285DC9F7A11B71F9A12FB0A18F4EF70F6,
	LatencySimulation_ClientConnect_mF4F7A11F7FE9D9B8F6BFE2B59B9EEB8A1D31BB79,
	LatencySimulation_ClientConnected_m3C83DA084C9C5363E08818FB6B2C5AE8E4D40006,
	LatencySimulation_ClientDisconnect_mB4DC3F81C0E255CFF948367D2144D6164BCDEE7B,
	LatencySimulation_ClientSend_mCFDCAEF8FE11104F02BC80EA16F8993F6FAF0974,
	LatencySimulation_ServerUri_m2F83819935A33F356172017EC6FBC0028CF7D4B8,
	LatencySimulation_ServerActive_m0D5C3290B13A754C5E82AD0658F5CDF03803C234,
	LatencySimulation_ServerGetClientAddress_mF04BFDB290D97FAB20F9765CE9BAF73FB6122B83,
	LatencySimulation_ServerDisconnect_mA1EEBC5605FDD6960D2D26F25FDACF1014F39562,
	LatencySimulation_ServerSend_m5CDF4C4070790964E697DBF25DD6328B38087AAC,
	LatencySimulation_ServerStart_m9A02F23068A6CDC45B8996CB18D0F81A0CE22117,
	LatencySimulation_ServerStop_m0D316CBA8805BE026057689A8F096D35FBB50CC7,
	LatencySimulation_ClientEarlyUpdate_mB435C18B756AF92EDE89339FE98BE3D5F12F7CB2,
	LatencySimulation_ServerEarlyUpdate_mF35156F7650DADF4FB394DEBB1945BC1896184E9,
	LatencySimulation_ClientLateUpdate_mE23BD9FE950168B1C0072413F5D78C25DE4213D7,
	LatencySimulation_ServerLateUpdate_m4D4209A62E2E89D4D2D9FE9C12BE8284DD52BC5D,
	LatencySimulation_GetBatchThreshold_m37F2AF8CBF3DBFA4A22AE8FC0B233355148971E7,
	LatencySimulation_GetMaxPacketSize_mB59E4EB5BE11F496BE1511C989B12BE3B61A3CF3,
	LatencySimulation_Shutdown_mC3046E3ADA6BF4D6C1BE4A62C477485C10B3B3B9,
	LatencySimulation_ToString_mE852B81FC6869A65D634D054CC96C97A12998092,
	LatencySimulation__ctor_m03A63EBE9022826CE2FDBB06FBBB34126F0B293B,
	MiddlewareTransport_Available_m411D7B26DF15DFC79722E919B00CBEC3F610E5DA,
	MiddlewareTransport_GetMaxPacketSize_m28AC40A0440620D670700A64EF2A061E47F3168D,
	MiddlewareTransport_GetBatchThreshold_mED304EB53A66D5287A1A21CF41C60ADBCB354086,
	MiddlewareTransport_Shutdown_m8B4F981C42A2329708768E00A385FDBE4019C447,
	MiddlewareTransport_ClientConnect_mCAF0426A1FB08C9922E6CF9149709B95BAB06A8D,
	MiddlewareTransport_ClientConnected_m1643CFBFD2BBDAF375CAD2651AE6C7F1710E45E3,
	MiddlewareTransport_ClientDisconnect_m3F6ADA34C9ED111E4ECF342D1F90846E88774EC4,
	MiddlewareTransport_ClientSend_m4C2C38CC8E2DBC85F1D0468737C5582337EF97AF,
	MiddlewareTransport_ClientEarlyUpdate_m02AB943881C5268777F72CCBEB21B44E39498B7E,
	MiddlewareTransport_ClientLateUpdate_mBE2E413DBAB919C972DD3F589D10DB0075E211D4,
	MiddlewareTransport_ServerActive_mDDBE6B2F53D2AD7625198DA5728EE35C575A3417,
	MiddlewareTransport_ServerStart_m83D72FAFD4A1F5E4D096003D6723B23906709AF9,
	MiddlewareTransport_ServerStop_m60C91D39F2046F046B8C3664CEC5E66ED0D0F6ED,
	MiddlewareTransport_ServerSend_mB7D84E1825413A452DC30BB35081509A892BD3E2,
	MiddlewareTransport_ServerDisconnect_m23D93C972A453065ED9295EE4EA09DEAB06C64B4,
	MiddlewareTransport_ServerGetClientAddress_m16C622F42B04C08193A7FBA9F8BC2AEDF1EEDA7E,
	MiddlewareTransport_ServerUri_mA0673C40718ABD0F7A6AE094451DA1DCF3B8A326,
	MiddlewareTransport_ServerEarlyUpdate_m41937D34B366DCD18500B17FECD0324214BF064A,
	MiddlewareTransport_ServerLateUpdate_m30BD5C9986FCC69528C0EB40376F94507AA7DE4F,
	MiddlewareTransport__ctor_mCC955E33DC37D37D65ED774014D51051C5758B65,
	MultiplexTransport_Awake_m93C0AEFF0ACDF70E4537AB84EEB04CE4613A0576,
	MultiplexTransport_ClientEarlyUpdate_m25C31409D47DC43E6F8E2D02A3C6FFCC905639A3,
	MultiplexTransport_ServerEarlyUpdate_m216824203A20B670BABEBBAF65A4C5EB6B150F35,
	MultiplexTransport_ClientLateUpdate_m50169C7227888430E7B02C74B45BFD75E22D2B87,
	MultiplexTransport_ServerLateUpdate_mA516AD160FD303F59E80FA1FB614CB1653437228,
	MultiplexTransport_OnEnable_m644F1B5846E1B5024743D4CD270CE40CAE16E263,
	MultiplexTransport_OnDisable_m3818B03C8CCA40A39008126187CDBC97C2B31932,
	MultiplexTransport_Available_m5E68041189FA6D45F8B0BE192906FBF50745093B,
	MultiplexTransport_ClientConnect_mAAC8096A852A42DE9C53CA2AAF2DB0EB429BC5F2,
	MultiplexTransport_ClientConnect_m2640BA041A88EFF1CC37398A23813966448A3572,
	MultiplexTransport_ClientConnected_m59A4C147D9E4D0AA495A8C9F9FC73B8E6B014151,
	MultiplexTransport_ClientDisconnect_m11CB4676F1CAE0A596B8918DCB10CEBE5092E60D,
	MultiplexTransport_ClientSend_mB899809A718125E3F0704C1E07102C7EE0887E6B,
	MultiplexTransport_FromBaseId_mF710FA98E9A2336422CB2DFBF7CC4BF54B41B284,
	MultiplexTransport_ToBaseId_mEDEA4469539AD4C6ECE88F194B420484F1AF8CB7,
	MultiplexTransport_ToTransportId_mAE2AA047364BDDC681A3B2A9520A288162EAC4B7,
	MultiplexTransport_AddServerCallbacks_mAB8919C10049912E067D0C10146E4289146A939A,
	MultiplexTransport_ServerUri_mB25D6F90CCBAAADBB30AA514E55B7615D3563C0F,
	MultiplexTransport_ServerActive_mE6AAD0BE67D9B6F7A28F9AA77C848F737067A9A3,
	MultiplexTransport_ServerGetClientAddress_mEC33B8AFBFE17800379569DF67FDB1A95AC53DCF,
	MultiplexTransport_ServerDisconnect_m3B7A7CEE9970254DCFA47959A99F0A46E697ADF9,
	MultiplexTransport_ServerSend_m760A6519B38A5902633030538CC01A91B1371A4A,
	MultiplexTransport_ServerStart_m997711F482577343746A59B07BAC0C153AF02B03,
	MultiplexTransport_ServerStop_m786191B4B7724BA90BB982C026F1C9E707C36381,
	MultiplexTransport_GetMaxPacketSize_m780CFDCCF070CFE1F9CD3F326245478C5E73EDA0,
	MultiplexTransport_Shutdown_mC14719523179B4A1E439ADCDCC313F6E31A972A4,
	MultiplexTransport_ToString_mAF2A3B96C367D4F229AF97569896D8B71612E59F,
	MultiplexTransport__ctor_m1AA74D3D57E4FAF657EB99DE735A1BFDB7C38495,
	U3CU3Ec__DisplayClass18_0__ctor_m054DFE8D50F877AD63841E813CB93811ABBDC1E7,
	U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__0_m3F7F308DCC08E8E7F4B38ADB1E0FE84EC27B5299,
	U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__1_m7880630DC1D0F893844AE14EF6D7E437C89C18ED,
	U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__2_m2F6380738F2DE4BA3B2E0ED90268B7C5718084F3,
	U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__3_m1A02C0762E603F8DBB6ABC53042A95AA06729818,
	TelepathyTransport_Awake_m3EB200590EE12064D70C7A0DD027E40C2D5D00E5,
	TelepathyTransport_Available_m529F410884695E2DBAF3F6C52C4F3F5D831C060A,
	TelepathyTransport_CreateClient_m5A3E215CA3BC5E1DDA7037F0B46096559ABE3CE6,
	TelepathyTransport_ClientConnected_m634791BEEE06F18CB86AADF7B53DE8DF52823152,
	TelepathyTransport_ClientConnect_m1FBCA148622546F990ED50164D82DB0D8AE6A228,
	TelepathyTransport_ClientConnect_mAB3658761DFA72046AF0564674A15929F626FFB7,
	TelepathyTransport_ClientSend_m5720B529399E32E698EBFC6203097C81371D8795,
	TelepathyTransport_ClientDisconnect_mE1F09DD72AAE2D2EABC7B8C89B5D0B300F681BFD,
	TelepathyTransport_ClientEarlyUpdate_mDC030022D1B606BB0B9C75C94849E0F34A358ACD,
	TelepathyTransport_ServerUri_mCC65867B56A7D0B01DC9E7C09847BD8FA607B1DA,
	TelepathyTransport_ServerActive_m1A932812542164DEB969EA1990CF03812FA6B0F6,
	TelepathyTransport_ServerStart_m444B6B84A925BEA37A9686610DC36FD7E2E5E7FD,
	TelepathyTransport_ServerSend_mBA012641BD35DD6D8150F265D5A5301E03EE4F42,
	TelepathyTransport_ServerDisconnect_m828880FA831B11E4F63DF32A684E26CEC912B4E1,
	TelepathyTransport_ServerGetClientAddress_m4322423E96F902AFD444525D1E6CE68E178FCA1D,
	TelepathyTransport_ServerStop_m1C3B479ED47A722888E04C3F27DB9A8BE0896FEF,
	TelepathyTransport_ServerEarlyUpdate_mF1E5E6749863D2E9712EB0FFEDB4941698526E77,
	TelepathyTransport_Shutdown_m15CFA2417884E989101A3259BCDFBD089B2E9C3D,
	TelepathyTransport_GetMaxPacketSize_m1952E7D852788A11E9002C6D9FA31DEE8D7EC336,
	TelepathyTransport_ToString_m84B9527990704286FA00487EC8F70897DF684D11,
	TelepathyTransport__ctor_m29EC658D39383FD39ECB4535F7402767AD209535,
	TelepathyTransport_U3CAwakeU3Eb__16_0_mEBEFF9D7CEC38DF1E1132764A3A1AA21923DD176,
	TelepathyTransport_U3CCreateClientU3Eb__18_0_m80C3EC25F7C28C37EC4AC9C695967C85317D1399,
	TelepathyTransport_U3CCreateClientU3Eb__18_1_m66BA55843D5BD71116C5CEAF402B822801029090,
	TelepathyTransport_U3CCreateClientU3Eb__18_2_m402733E536B10F311D068B8A738D2602F456A7CB,
	TelepathyTransport_U3CServerStartU3Eb__27_0_m1F9C5421E9BB2ACEC6D9279B81ED9467C91A8C2E,
	TelepathyTransport_U3CServerStartU3Eb__27_1_m0EE491D075C22B93931F221BF2BBDAA3D0A436CC,
	TelepathyTransport_U3CServerStartU3Eb__27_2_mF41F1BB9824AA491058579171DD1F314E12B588D,
	NULL,
	NULL,
	NULL,
	Transport_ClientConnect_mCE84ED183F7A7A3650AD2C693AA98A9CC28DC516,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Transport_GetBatchThreshold_m19AFF384DB542EBDB9362C3E94458F9918D8F472,
	Transport_Update_m805E65D9CFAE9907EA3FF5ED8C4367E8416748A1,
	Transport_LateUpdate_m93EE7407CC6DBC79D9104171CDC68D2C0D98CC5E,
	Transport_ClientEarlyUpdate_m161A0CE6D881CF8F7BBCD41F84C7D137C1B85DC3,
	Transport_ServerEarlyUpdate_mBD9978F98BF9B5F34A97B3FEDCA8C69545AC2F10,
	Transport_ClientLateUpdate_m483122FA63F280BEC27BAA42EE468A3FD2F7E9F8,
	Transport_ServerLateUpdate_m8BC41B0A6B1F799D812276E85C6F21F648F0CAE0,
	NULL,
	Transport_OnApplicationQuit_m35ACCD0AD1874108989519C9309C03C42A47C283,
	Transport__ctor_m951E466BF3C4D441259ACE340863D31DC828B3AF,
	NetworkMessageDelegate__ctor_mD6E8A0B315CE6D4AF6C72D7B38F8FB7D21006960,
	NetworkMessageDelegate_Invoke_m98C548118D37B52A595A6D21FC60C7CBED50A564,
	NetworkMessageDelegate_BeginInvoke_mAD4A78DFFF75B688D3265665E4556C536E98A977,
	NetworkMessageDelegate_EndInvoke_m5232A8A85073AD476E6F264D324E0B7F3E0D5AAC,
	SpawnDelegate__ctor_m8F9AE1A91FE0B260D9D1077D256628CD01E9E850,
	SpawnDelegate_Invoke_m1B47F6656BF440993D298D38E84DDDB57155B910,
	SpawnDelegate_BeginInvoke_mF10AB69AA9AA9FBB471564195BD8AA50A04E95AD,
	SpawnDelegate_EndInvoke_m644B2556D0CF22425613500117FF37F57C214B5E,
	SpawnHandlerDelegate__ctor_m4EA3BC557AC3E8C422F149086D9D0DB7735CFBC9,
	SpawnHandlerDelegate_Invoke_m0556616B7CAAF06679FC504B941CA3BB3F3A217A,
	SpawnHandlerDelegate_BeginInvoke_m93E307D23418E444F7BE2CE62134D183FBF1382D,
	SpawnHandlerDelegate_EndInvoke_m2D904E75331BEB2E1D7959472603220B2EA3E719,
	UnSpawnDelegate__ctor_mA4DC94E5B22576C43190651F3788F13C1A25175B,
	UnSpawnDelegate_Invoke_m943692C898825098A05610FFA71F1A432A26F0BF,
	UnSpawnDelegate_BeginInvoke_m9273D27CD0369BEC9D931651F139D84660F38C10,
	UnSpawnDelegate_EndInvoke_m5233DF02E97F47E53CD4F21A1993DB518A1A5744,
	Utils_GetTrueRandomUInt_m18CEE51EB1A1DF72867E31B9168610AF0D92EAEB,
	Utils_IsPrefab_mC214A12F48646B4F34DA550DB27CD33C5CFEE71A,
	Utils_IsSceneObjectWithPrefabParent_m8B241F8EA3BF330478CC5A837331E948D97C04E2,
	Utils_IsPointInScreen_m742BEEAF9B1F44C20CC42003E5818F3BF499B729,
	Utils_PrettyBytes_m7AD7804F170D3AD120320CF8F4432F58D176ADA2,
	Utils_GetSpawnedInServerOrClient_m8A1D9AB4EB7672B53355A6900A89E26FFA60A574,
	RemoteCallDelegate__ctor_m252DBEC3816F583FF0270DFFAE713E29C914D65C,
	RemoteCallDelegate_Invoke_mE2D0B4E12822433608A1571DBEC43F1EABEE42AB,
	RemoteCallDelegate_BeginInvoke_mB66DBE1857F9A5853CE5FD064120EA1BE703CBE2,
	RemoteCallDelegate_EndInvoke_m54C56F11205C67A3B5E3F193790486BC6F8A657D,
	Invoker_AreEqual_mF68D11E6046548EE698E7D455C865AE2A8C28C9C,
	Invoker__ctor_m8DE43B22C4BFF2DB9E2208F81BC5D31FCFBF37B3,
	RemoteProcedureCalls_CheckIfDelegateExists_mAB0E55F6FE91536834E37050438635AE77B3331F,
	RemoteProcedureCalls_RegisterDelegate_mB78312F428BACF8CF708DD90157F582681898EED,
	RemoteProcedureCalls_RegisterCommand_mEE58861E157F6AB47C8B8CEE3AD0EEA15ECF4F60,
	RemoteProcedureCalls_RegisterRpc_mCFB5293951307DDDF042FA2FA881E2FBD1361622,
	RemoteProcedureCalls_RemoveDelegate_mEC2E4E8AC9D9FA016D25008B0F82FEA994907AE4,
	RemoteProcedureCalls_GetInvokerForHash_m411FC36099D735D21CE477925761905BC1AE544A,
	RemoteProcedureCalls_Invoke_mA531F7621D985754257828272313590C314918CA,
	RemoteProcedureCalls_CommandRequiresAuthority_mC8951D65C229D7543154334F9525C265493E23E5,
	RemoteProcedureCalls_GetDelegate_mB9FD0206C949F270579E058D4F073E7C285A8E73,
	RemoteProcedureCalls__cctor_m1C19D578EC8EE8467DE532845B32E491702EEF01,
	GeneratedNetworkCode__Read_Mirror_ReadyMessage_m075F08A19B82E3386470E4682FE8049E1FE466F1,
	GeneratedNetworkCode__Write_Mirror_ReadyMessage_m5321F6E73EBC204A535B4117FBFD56B3B6F1DF3E,
	GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m1B5ECFC3082B00559E38081D4AF150BD7507E528,
	GeneratedNetworkCode__Write_Mirror_NotReadyMessage_m9FAD954526FEBA8ADE0B3FB19DD79FAE3F75B830,
	GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_mD594AE97DACC696857EDABA65071147E6AAA74BF,
	GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_m21FDF6F83887B85B7642727DEE97599057D88142,
	GeneratedNetworkCode__Read_Mirror_SceneMessage_m98DC05F3545048FF14872D6F69C883854CBBF6F8,
	GeneratedNetworkCode__Read_Mirror_SceneOperation_mF5CB0A94AE859CF19C5CBE2FB30C751589B9DD31,
	GeneratedNetworkCode__Write_Mirror_SceneMessage_mE675705095C6D09C5AD43E1176E29E3FD49BCBA1,
	GeneratedNetworkCode__Write_Mirror_SceneOperation_mCD5706E35424CFD0386B102B9F52BC3FDF3362E2,
	GeneratedNetworkCode__Read_Mirror_CommandMessage_m6A83891534C74B42DE59F10E61D59F3817F600A0,
	GeneratedNetworkCode__Write_Mirror_CommandMessage_m46C843E1FEE1A686C21FD996E15D8F88935E2A11,
	GeneratedNetworkCode__Read_Mirror_RpcMessage_m86AE78A791D6711381C1BCBAECDD632318094BD0,
	GeneratedNetworkCode__Write_Mirror_RpcMessage_mAF266DA1E1F721E6DE66DC247FE8A2BFD63C492C,
	GeneratedNetworkCode__Read_Mirror_SpawnMessage_mA50F8DA79BBA354AD085010B076D4E85AB186EFB,
	GeneratedNetworkCode__Write_Mirror_SpawnMessage_m98EBEFEF724118AB8490FA73F6EAEEF41D7892FF,
	GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m63CA80B6E5756675ADA6AF60511E0B30F63311FA,
	GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_mAB793B697671AC8B955B396FC1CC27A58DBF1065,
	GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_mB8E5941E4475B65FA4DAEACBD74C1F9655199527,
	GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_mF95BF9BD312E1D208F8C77EC165FD41CF4CB0281,
	GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_mE74235E357CB63EEDD22B8E0445B72DCF09C336E,
	GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_mDE8C681049E25055D2E42DD8853CB06656051246,
	GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_mF44BEDB706BC3DDE113D3BA95A5868EADEF717BC,
	GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_mC6BAB97AF90EB548D0A9310CF7D3953796920E36,
	GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_mC93A013D4AF4482FD03EEF3BD5097A2397AD4CC7,
	GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_m33E4C3748C204940ECFF9AEA1E7ED7F97C05E5B1,
	GeneratedNetworkCode__Read_Mirror_EntityStateMessage_m4B23E10BC7E307C7ECD22CC75BEECDBE62E91BF1,
	GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m727648D11A6EC5BB7A19141B8902B6055A87241E,
	GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m10E4A9A1FD5CBAF1001823F67E58E749F91DF115,
	GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_m2B4B615BF9725BC828F1547703AF35AC2BA07415,
	GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_m5FC87DE505EE48D9CF04A41874B89AB79088B0A7,
	GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_mC4B6E0A4CE3AA683CFB3D459621EC25C9CB6D074,
	GeneratedNetworkCode_InitReadWriters_m3EF6B3771311173939C3C88B6507664844858949,
};
extern void NetworkPingMessage__ctor_m54E9D94B3A2C74849855189A2580FCBDD1C86D94_AdjustorThunk (void);
extern void NetworkBehaviourSyncVar__ctor_mFD3CF614B6356621B5CC741500D4FAD1E77A447B_AdjustorThunk (void);
extern void NetworkBehaviourSyncVar_Equals_m94E677024C09599396FF1AEB14DA042D2FB8B485_AdjustorThunk (void);
extern void NetworkBehaviourSyncVar_Equals_mB5F55C22E07441A6816A3D82647F0E3097CEBC06_AdjustorThunk (void);
extern void NetworkBehaviourSyncVar_ToString_mBCD8FA34F95F8B2E52F282476D543394C3006FDD_AdjustorThunk (void);
extern void MessageInfo__ctor_m544E158986CD48130C1E1E2B68040337F0CA475A_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[6] = 
{
	{ 0x06000083, NetworkPingMessage__ctor_m54E9D94B3A2C74849855189A2580FCBDD1C86D94_AdjustorThunk },
	{ 0x060000D2, NetworkBehaviourSyncVar__ctor_mFD3CF614B6356621B5CC741500D4FAD1E77A447B_AdjustorThunk },
	{ 0x060000D3, NetworkBehaviourSyncVar_Equals_m94E677024C09599396FF1AEB14DA042D2FB8B485_AdjustorThunk },
	{ 0x060000D4, NetworkBehaviourSyncVar_Equals_mB5F55C22E07441A6816A3D82647F0E3097CEBC06_AdjustorThunk },
	{ 0x060000D5, NetworkBehaviourSyncVar_ToString_mBCD8FA34F95F8B2E52F282476D543394C3006FDD_AdjustorThunk },
	{ 0x06000159, MessageInfo__ctor_m544E158986CD48130C1E1E2B68040337F0CA475A_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1225] = 
{
	6908,
	6908,
	11082,
	10962,
	6908,
	6908,
	6666,
	6666,
	5521,
	5521,
	2451,
	6908,
	6908,
	6908,
	6765,
	6666,
	6908,
	1486,
	5485,
	4899,
	6908,
	6908,
	6908,
	6908,
	4618,
	4618,
	6728,
	6728,
	6728,
	6728,
	6728,
	6728,
	11217,
	6908,
	6765,
	6908,
	6908,
	2449,
	6908,
	6908,
	2449,
	6908,
	5485,
	1485,
	5485,
	5485,
	1485,
	5485,
	12362,
	6908,
	5521,
	4834,
	4834,
	4648,
	4648,
	4648,
	4648,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	5485,
	2450,
	10106,
	3982,
	6727,
	5521,
	3567,
	1839,
	6908,
	8667,
	7646,
	7644,
	9775,
	11385,
	11255,
	11256,
	10131,
	10096,
	11466,
	11121,
	5485,
	5449,
	11092,
	11219,
	0,
	0,
	6908,
	6908,
	0,
	0,
	6908,
	3041,
	5521,
	5521,
	6908,
	6908,
	6765,
	2451,
	4040,
	6908,
	6908,
	6765,
	6908,
	6908,
	2451,
	6908,
	6908,
	6908,
	4040,
	6908,
	8582,
	11029,
	8582,
	12315,
	0,
	0,
	9413,
	0,
	0,
	0,
	0,
	0,
	0,
	5449,
	6908,
	6908,
	6908,
	5521,
	5521,
	5521,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6666,
	6666,
	6666,
	6666,
	6666,
	6666,
	6891,
	6765,
	6765,
	6666,
	6765,
	5521,
	6727,
	5485,
	6892,
	5643,
	4097,
	4097,
	3176,
	3176,
	5643,
	5643,
	6666,
	6908,
	5521,
	1096,
	1096,
	1103,
	0,
	542,
	542,
	0,
	9423,
	1042,
	2391,
	9423,
	0,
	977,
	977,
	0,
	1042,
	2391,
	0,
	0,
	0,
	0,
	0,
	1915,
	3041,
	1915,
	3041,
	3982,
	3982,
	5521,
	5521,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	3170,
	4216,
	1995,
	6765,
	6908,
	6908,
	6666,
	12322,
	11516,
	12322,
	11516,
	12322,
	12297,
	12297,
	12297,
	12297,
	12362,
	12362,
	11504,
	11516,
	11516,
	12362,
	12362,
	12362,
	12362,
	9418,
	9903,
	12362,
	11516,
	0,
	0,
	0,
	0,
	0,
	9388,
	11516,
	10092,
	11516,
	8294,
	9086,
	8294,
	9086,
	11516,
	9024,
	9024,
	11509,
	12362,
	9390,
	12297,
	11516,
	12297,
	10127,
	9450,
	11232,
	11229,
	11233,
	11233,
	10965,
	12362,
	11520,
	11519,
	12362,
	11517,
	11518,
	11529,
	11508,
	11526,
	11518,
	11517,
	11529,
	11505,
	10084,
	11516,
	11531,
	12362,
	12362,
	12362,
	12362,
	12362,
	12362,
	6908,
	5519,
	5525,
	5524,
	5454,
	5100,
	0,
	0,
	0,
	0,
	6908,
	4909,
	6908,
	4909,
	6908,
	4909,
	6765,
	0,
	6765,
	5521,
	6765,
	6692,
	5449,
	6908,
	5485,
	4899,
	9322,
	0,
	2451,
	0,
	6908,
	4040,
	0,
	6765,
	6765,
	5485,
	2451,
	6908,
	5521,
	3041,
	6908,
	5521,
	5521,
	6908,
	6765,
	2451,
	6908,
	6908,
	11516,
	11516,
	11516,
	11516,
	12362,
	0,
	0,
	1076,
	6666,
	5418,
	6666,
	5418,
	6666,
	5418,
	6666,
	6666,
	6666,
	5418,
	6891,
	5642,
	6765,
	5521,
	6765,
	5521,
	12322,
	6765,
	5521,
	6722,
	5479,
	12362,
	12362,
	12362,
	11233,
	5521,
	12355,
	12362,
	11516,
	11516,
	6666,
	5418,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	1248,
	1471,
	4880,
	1607,
	3041,
	512,
	5521,
	5521,
	6908,
	3982,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	12362,
	3059,
	1607,
	487,
	5521,
	12362,
	8653,
	7457,
	12362,
	12362,
	12362,
	6908,
	3994,
	12322,
	11516,
	6727,
	6666,
	6727,
	5485,
	6908,
	6908,
	6908,
	6908,
	6908,
	6666,
	10965,
	6908,
	6908,
	6908,
	5521,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6666,
	6908,
	6908,
	12362,
	6908,
	12322,
	11516,
	5521,
	1570,
	3136,
	6908,
	6908,
	6908,
	6908,
	6908,
	11516,
	11516,
	6765,
	5521,
	5521,
	3079,
	3039,
	6908,
	6908,
	6908,
	5520,
	5579,
	5521,
	5521,
	5521,
	5521,
	3068,
	5521,
	5521,
	6908,
	5521,
	6908,
	5521,
	5521,
	6908,
	5521,
	1570,
	6908,
	5521,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	12362,
	12362,
	6908,
	3670,
	3982,
	4648,
	3982,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6727,
	6727,
	5521,
	5134,
	5521,
	5134,
	0,
	0,
	6666,
	2381,
	3271,
	6765,
	0,
	10965,
	10836,
	11290,
	10861,
	11367,
	10837,
	10965,
	10835,
	11069,
	10846,
	11367,
	10863,
	11092,
	10848,
	11384,
	10864,
	11121,
	10850,
	11466,
	10865,
	11314,
	10862,
	11036,
	10842,
	11020,
	10841,
	11219,
	11219,
	9698,
	10819,
	11478,
	10866,
	11486,
	10868,
	11496,
	10870,
	11484,
	10867,
	11494,
	10869,
	11003,
	10838,
	11008,
	10839,
	11254,
	10853,
	11264,
	10855,
	11249,
	10852,
	11260,
	10854,
	11155,
	10851,
	11058,
	10845,
	11219,
	11219,
	0,
	12233,
	11219,
	11219,
	0,
	0,
	11219,
	11219,
	11219,
	12362,
	11219,
	11219,
	11166,
	11166,
	11516,
	11516,
	12362,
	12362,
	6908,
	6765,
	5521,
	5134,
	5521,
	5134,
	6908,
	12322,
	11516,
	12297,
	12297,
	11504,
	12362,
	12362,
	12362,
	12362,
	12362,
	11511,
	12362,
	12362,
	10965,
	10962,
	11516,
	12362,
	12297,
	12297,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	11511,
	11516,
	8545,
	9031,
	11511,
	9987,
	0,
	0,
	0,
	0,
	0,
	12362,
	9413,
	12362,
	9420,
	8544,
	8543,
	7946,
	11516,
	11516,
	12362,
	10115,
	10106,
	10106,
	10083,
	9067,
	7897,
	10106,
	10106,
	10106,
	10106,
	10106,
	9068,
	10965,
	12297,
	11516,
	11516,
	11516,
	11516,
	10095,
	10095,
	11516,
	11516,
	10083,
	10083,
	10083,
	9704,
	11516,
	12362,
	12362,
	12362,
	12362,
	0,
	0,
	6908,
	6908,
	6908,
	12308,
	12308,
	12308,
	12308,
	12308,
	12308,
	12308,
	12308,
	12362,
	12362,
	10103,
	11515,
	12362,
	6908,
	5485,
	6765,
	6001,
	0,
	0,
	5418,
	1587,
	0,
	6908,
	10083,
	10054,
	10120,
	10069,
	10129,
	10055,
	10083,
	10053,
	10094,
	10061,
	10129,
	10071,
	10095,
	10062,
	10130,
	10072,
	10096,
	10063,
	10131,
	10073,
	10126,
	10070,
	10090,
	10059,
	10089,
	10058,
	10106,
	10006,
	10106,
	8311,
	0,
	10132,
	10074,
	10134,
	10076,
	10136,
	10078,
	10133,
	10075,
	10135,
	10077,
	10086,
	10056,
	10087,
	10057,
	10113,
	10066,
	10116,
	10068,
	10111,
	10065,
	10114,
	10067,
	10102,
	10064,
	10092,
	10060,
	10106,
	10106,
	10106,
	10106,
	0,
	0,
	10106,
	10106,
	10106,
	12362,
	12322,
	12322,
	11516,
	11516,
	12362,
	12362,
	6908,
	6765,
	6908,
	6908,
	6908,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	6908,
	0,
	0,
	0,
	0,
	0,
	6908,
	12362,
	6908,
	6666,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	6765,
	5521,
	5521,
	5521,
	3173,
	5521,
	11384,
	11232,
	11219,
	11219,
	9420,
	9420,
	9420,
	9420,
	9420,
	9420,
	3982,
	6727,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	6765,
	5521,
	5521,
	5521,
	3173,
	5521,
	11219,
	11219,
	9420,
	9420,
	9420,
	9420,
	9420,
	9420,
	3982,
	6727,
	6908,
	6908,
	6908,
	5025,
	5023,
	267,
	6666,
	5521,
	5521,
	6666,
	6908,
	2451,
	6765,
	6666,
	4899,
	5485,
	1486,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	4618,
	4618,
	6908,
	6765,
	6908,
	6666,
	4618,
	4618,
	6908,
	5521,
	6666,
	6908,
	2451,
	6908,
	6908,
	6666,
	6908,
	6908,
	1486,
	5485,
	4899,
	6765,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	6666,
	5521,
	5521,
	6666,
	6908,
	2451,
	2156,
	4618,
	4618,
	6908,
	6765,
	6666,
	4899,
	5485,
	1486,
	6908,
	6908,
	4618,
	6908,
	6765,
	6908,
	6908,
	5485,
	1486,
	2799,
	5485,
	6908,
	6666,
	6908,
	6666,
	5521,
	5521,
	2451,
	6908,
	6908,
	6765,
	6666,
	6908,
	1486,
	5485,
	4899,
	6908,
	6908,
	6908,
	4618,
	6765,
	6908,
	6666,
	6908,
	5134,
	6908,
	5485,
	2522,
	5485,
	0,
	0,
	0,
	5521,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4618,
	6908,
	6908,
	6908,
	6908,
	6908,
	6908,
	0,
	6908,
	6908,
	3059,
	1610,
	489,
	5521,
	3059,
	2395,
	945,
	4903,
	3059,
	4909,
	1376,
	4903,
	3059,
	5521,
	1368,
	5521,
	12355,
	10965,
	9413,
	10979,
	11217,
	11232,
	3059,
	1613,
	491,
	5521,
	1246,
	6908,
	7940,
	7595,
	8314,
	9086,
	11511,
	8521,
	7444,
	10962,
	11216,
	12362,
	11262,
	10115,
	11165,
	10105,
	10939,
	10082,
	11302,
	10965,
	10122,
	10083,
	11010,
	10088,
	11281,
	10118,
	11334,
	10127,
	10999,
	10084,
	11245,
	10110,
	11244,
	10109,
	11242,
	10107,
	11243,
	10108,
	11052,
	10091,
	11163,
	10103,
	11164,
	10104,
	12362,
};
static const Il2CppTokenRangePair s_rgctxIndices[68] = 
{
	{ 0x0200001A, { 17, 6 } },
	{ 0x0200001B, { 23, 2 } },
	{ 0x02000035, { 64, 2 } },
	{ 0x02000036, { 66, 2 } },
	{ 0x02000055, { 113, 2 } },
	{ 0x02000060, { 132, 8 } },
	{ 0x02000063, { 176, 41 } },
	{ 0x02000067, { 217, 9 } },
	{ 0x02000068, { 226, 51 } },
	{ 0x0200006C, { 277, 6 } },
	{ 0x0200006F, { 283, 46 } },
	{ 0x02000073, { 329, 8 } },
	{ 0x02000074, { 337, 8 } },
	{ 0x02000075, { 345, 16 } },
	{ 0x02000077, { 361, 16 } },
	{ 0x0600005C, { 0, 2 } },
	{ 0x0600005D, { 2, 3 } },
	{ 0x0600007A, { 5, 1 } },
	{ 0x0600007B, { 6, 2 } },
	{ 0x0600007D, { 8, 3 } },
	{ 0x0600007E, { 11, 6 } },
	{ 0x060000AD, { 25, 4 } },
	{ 0x060000B0, { 29, 4 } },
	{ 0x060000B5, { 33, 3 } },
	{ 0x060000B8, { 36, 3 } },
	{ 0x060000BB, { 39, 1 } },
	{ 0x060000BC, { 40, 1 } },
	{ 0x060000BD, { 41, 1 } },
	{ 0x060000BE, { 42, 4 } },
	{ 0x060000EF, { 46, 1 } },
	{ 0x060000F0, { 47, 8 } },
	{ 0x060000F1, { 55, 2 } },
	{ 0x060000F2, { 57, 6 } },
	{ 0x060000F3, { 63, 1 } },
	{ 0x0600013D, { 68, 2 } },
	{ 0x06000157, { 70, 1 } },
	{ 0x06000158, { 71, 1 } },
	{ 0x06000203, { 72, 2 } },
	{ 0x06000204, { 74, 3 } },
	{ 0x06000209, { 77, 4 } },
	{ 0x06000244, { 81, 1 } },
	{ 0x06000248, { 82, 4 } },
	{ 0x06000249, { 86, 2 } },
	{ 0x06000270, { 88, 2 } },
	{ 0x06000271, { 90, 1 } },
	{ 0x06000272, { 91, 2 } },
	{ 0x06000273, { 93, 2 } },
	{ 0x06000274, { 95, 1 } },
	{ 0x06000275, { 96, 1 } },
	{ 0x06000276, { 97, 1 } },
	{ 0x0600027D, { 98, 3 } },
	{ 0x0600027E, { 101, 3 } },
	{ 0x0600027F, { 104, 2 } },
	{ 0x06000280, { 106, 6 } },
	{ 0x06000281, { 112, 1 } },
	{ 0x060002C1, { 115, 1 } },
	{ 0x060002C2, { 116, 3 } },
	{ 0x060002C5, { 119, 4 } },
	{ 0x060002E5, { 123, 4 } },
	{ 0x06000304, { 127, 4 } },
	{ 0x06000305, { 131, 1 } },
	{ 0x0600031E, { 140, 9 } },
	{ 0x0600031F, { 149, 7 } },
	{ 0x06000320, { 156, 1 } },
	{ 0x06000321, { 157, 1 } },
	{ 0x06000322, { 158, 2 } },
	{ 0x06000323, { 160, 6 } },
	{ 0x06000324, { 166, 10 } },
};
extern const uint32_t g_rgctx_List_1_t4CA52616DFE66A79AE64E18ED2DEFDED1D06AC08;
extern const uint32_t g_rgctx_List_1_AddRange_mCAB149E67880F7E6C49772EB583F2BD28B516954;
extern const uint32_t g_rgctx_Queue_1_tED902440A083AE00C849EADBD14D5F5DAE12D248;
extern const uint32_t g_rgctx_Queue_1_get_Count_mB00F0E3E6FB74E393CD567125A234A9408C8234F;
extern const uint32_t g_rgctx_Queue_1_Dequeue_mD57861CC36B03F129094EB312FB728DE942B2EA7;
extern const uint32_t g_rgctx_T_t139E2FFCA147116E6CF748BF98F26ED3A9611344;
extern const uint32_t g_rgctx_MessagePacking_GetId_TisT_tD993192AF87795F9BA5433E2B10AA2C3B4C00DD3_m214184541FE39CF8FBFDDF4DD385E68D3DA452B6;
extern const uint32_t g_rgctx_NetworkWriter_Write_TisT_tD993192AF87795F9BA5433E2B10AA2C3B4C00DD3_m694E34068ED34009C994A7580E91E796F3BD755F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass6_0_2_tADD677C365101662255983047576ECE0268C1312;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass6_0_2__ctor_mD783306844A3DAC78CE97842E4D9BE3B5B6EABF7;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass6_0_2_U3CWrapHandlerU3Eb__0_mD4EB5732E784E10616632C15886DC522962E274A;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass7_0_2_tEA91EC9A1646A09BCD2513BE6301ECC533945531;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass7_0_2__ctor_mFB29C39E66ABF923837913475E4B0439350ABC77;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass7_0_2_U3CWrapHandlerU3Eg__WrappedU7C0_m2CFE9CED289E0325F5190E303769FFF6DD60F36C;
extern const uint32_t g_rgctx_Action_3_t8E00C012E66E048E9A45E2AD154FD57DC2F4621A;
extern const uint32_t g_rgctx_Action_3__ctor_m05B01B08C036872A57D3B84FFA827971180E1F92;
extern const uint32_t g_rgctx_MessagePacking_WrapHandler_TisT_t992A357F26FC11442F70E1DCBF47243F7BB60F55_TisC_t8CEA10D551CC20AD4EB8499C2F3119D329F9A991_m3941EB643DE753F111D1697D9FA0201C50A60C20;
extern const uint32_t g_rgctx_T_tB06C3C539FACC7058FC27BECA7D4CCD87E985E8E;
extern const uint32_t g_rgctx_NetworkReader_Read_TisT_tB06C3C539FACC7058FC27BECA7D4CCD87E985E8E_m3435199B6851481C9CF66BA7511D3C1A6BF5BED1;
extern const uint32_t g_rgctx_NetworkDiagnostics_OnReceive_TisT_tB06C3C539FACC7058FC27BECA7D4CCD87E985E8E_m66B6B007FB13DC105A622540F408947BE7D62BD7;
extern const uint32_t g_rgctx_C_tC912A05CEA1C58E52F8332328BF453410E19C273;
extern const uint32_t g_rgctx_Action_3_tC5E8D80068D66ECDB8B18C976EDAD16F65F6123F;
extern const uint32_t g_rgctx_Action_3_Invoke_m04BDE07043EBE830B1267C2DC90C5F5F7FBE915B;
extern const uint32_t g_rgctx_Action_2_tC68F49354E98A890E2B426546A36096D3B61E7C9;
extern const uint32_t g_rgctx_Action_2_Invoke_mD5B895D9156FBCC15499FF2EA84D40091C361578;
extern const uint32_t g_rgctx_NetworkBehaviour_SyncVarEqual_TisT_t0FC425C7561A7D79480FB394A57F27D090966EFF_m3A20BA13AF29F0D9750F7959E632F23DC7CE64B1;
extern const uint32_t g_rgctx_NetworkBehaviour_SetSyncVar_TisT_t0FC425C7561A7D79480FB394A57F27D090966EFF_mB66CE7E55BA58EA23FE218C4C2922A0EA6D1359F;
extern const uint32_t g_rgctx_Action_2_t12D11ECE8A8CA086CDBF7B653214EE0E018A247D;
extern const uint32_t g_rgctx_Action_2_Invoke_m1D2A87F32F5DF69E8917B501E72E7617CA6B3CBD;
extern const uint32_t g_rgctx_NetworkBehaviour_SyncVarNetworkBehaviourEqual_TisT_tD78A1FB7F75A4C347B29FF3DD090278E0EBF9EF6_m546BF5FB26588F444DA5CF6446007F2AB81E2E04;
extern const uint32_t g_rgctx_NetworkBehaviour_SetSyncVarNetworkBehaviour_TisT_tD78A1FB7F75A4C347B29FF3DD090278E0EBF9EF6_mA6D3AAA1F0533E328FB79EE328BD97321AE78D8D;
extern const uint32_t g_rgctx_Action_2_t16DF40D957C7EEE7B116F2EA9B2B083A7BECAEE5;
extern const uint32_t g_rgctx_Action_2_Invoke_m19B27065ECD595C48C169A6CBCFB95DD38D919B1;
extern const uint32_t g_rgctx_NetworkBehaviour_SyncVarEqual_TisT_t4B168B60FD63772E970D7EC343A89ED7337DE4F6_m6DACEAA0963839EDF74AB2CE2C86880C8F3F260B;
extern const uint32_t g_rgctx_Action_2_tFF1BF7B0A56D83B0F39CDF6D53CFE5B714640B24;
extern const uint32_t g_rgctx_Action_2_Invoke_m056D08A018620979E7864DD5F9BAAFEB97436AD1;
extern const uint32_t g_rgctx_NetworkBehaviour_GetSyncVarNetworkBehaviour_TisT_t6C90390DF8C3C92D7976A3BCF11C11923C9D844D_m06A0EEA1756893B7FDC86130624F8F6D0607FAB4;
extern const uint32_t g_rgctx_Action_2_t23A2E1BA6F695D77C98EC81D4233E8959F765D12;
extern const uint32_t g_rgctx_Action_2_Invoke_mA712644655DFD138FB51860406DED075CEF624D1;
extern const uint32_t g_rgctx_T_t725D06876D0D32B8F55B46E5F3F3D500590A67B8;
extern const uint32_t g_rgctx_T_tB887D8DB3A3F05B3BB96F9030712CCFE36A14BA1;
extern const uint32_t g_rgctx_T_t81F157C97222CC86748828CFAC34DC255E809E02;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_mFD1BEA3D52A3F1A1D630C810215B707FD0900127;
extern const uint32_t g_rgctx_EqualityComparer_1_t63338B2EC676F0F05086C85E624FC67A119FB702;
extern const uint32_t g_rgctx_EqualityComparer_1_t63338B2EC676F0F05086C85E624FC67A119FB702;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_mC257157FA4DC168A1F8C474AA8AC016BF61E5356;
extern const uint32_t g_rgctx_NetworkConnection_Send_TisT_t751499DA430BCAA6D3D45679122C8184CBE865C7_m0FF85357206C9A6EB2B71DE4A4197E2E84935465;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass47_0_1_tDDB045A069276E76A895EE36CFD0E48526668D52;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass47_0_1__ctor_m5C2091DD273467D27E241A35FD8ED2DD373DD2B4;
extern const uint32_t g_rgctx_MessagePacking_GetId_TisT_tE6D3EBBBF4D42020D3C8CE8677D8C6D3512DC5F0_m11BD2093EC33763666600A78D7314DB59A1B8F41;
extern const uint32_t g_rgctx_T_tE6D3EBBBF4D42020D3C8CE8677D8C6D3512DC5F0;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass47_0_1_U3CRegisterHandlerU3Eg__HandlerWrappedU7C0_m4DB7B00650148BCA662F6EF7D68DEFA48687E21F;
extern const uint32_t g_rgctx_Action_2_t739E7721748F3BFEC10F86DAA0AADDEE5E9D4BCE;
extern const uint32_t g_rgctx_Action_2__ctor_mCC007523022354BAC79A019FBC58AD5CF984DC37;
extern const uint32_t g_rgctx_MessagePacking_WrapHandler_TisT_tE6D3EBBBF4D42020D3C8CE8677D8C6D3512DC5F0_TisNetworkConnection_t49880296B0FA972023F34582D7A41D7B63383E78_m7D0631DE3288C0F521C248382B4FE8ED243D1629;
extern const uint32_t g_rgctx_MessagePacking_GetId_TisT_t0FD9CF7B61A57CB8D4829EFB4BB189C9EF191436_m5D4D2F436CF76934EC36E52B06B10D700DFCE0A9;
extern const uint32_t g_rgctx_MessagePacking_WrapHandler_TisT_t0FD9CF7B61A57CB8D4829EFB4BB189C9EF191436_TisNetworkConnection_t49880296B0FA972023F34582D7A41D7B63383E78_m3C300EC8A6C8FFA0D3A2F212BB0B5BD63AB8DB06;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass49_0_1_tBEFBF8E2E4AD20296AC44FE153A516AEBEDD71FD;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass49_0_1__ctor_m587FF032616DA74790714FCC437BE42437B32749;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass49_0_1_U3CReplaceHandlerU3Eb__0_mB6161603898969F033DD69DBC649A7468903197A;
extern const uint32_t g_rgctx_Action_2_tB55CB934B7A94948D7CEBFF6202825AD5208C7A2;
extern const uint32_t g_rgctx_Action_2__ctor_m4EA3C9D5D7401D33CBD03DDEB61FDDE051A0B680;
extern const uint32_t g_rgctx_NetworkClient_ReplaceHandler_TisT_t61617DEA6A58347D20E7298F4BBFE2DF49A53B0A_m9A9EB8865654C48FC1D6C8F761254A526B2D5B19;
extern const uint32_t g_rgctx_MessagePacking_GetId_TisT_tF2F8426C5ACDC965F8FA1692FFBAEE84EEE86C21_m22EBA7763D869D47BE13B1EFDE2118CC16DFBC35;
extern const uint32_t g_rgctx_Action_1_t8936590B41FBAC422CC9312A2B3352E5FD2C70F0;
extern const uint32_t g_rgctx_Action_1_Invoke_mC4323688AC6DED649E7930A83958C03074CC4131;
extern const uint32_t g_rgctx_Action_1_t28A9E13798DA254B2052F7D0CAB55AD4AE2DA55F;
extern const uint32_t g_rgctx_Action_1_Invoke_m61D8B5F87FC0AF1514AA71DEED084757903A4869;
extern const uint32_t g_rgctx_MessagePacking_Pack_TisT_t3E4807A7ED5A5B7D2D9379E5FE928C5F7969C9E8_mA827C8DA0B04B9B62EB54D3805AEEC600FDBD726;
extern const uint32_t g_rgctx_NetworkDiagnostics_OnSend_TisT_t3E4807A7ED5A5B7D2D9379E5FE928C5F7969C9E8_m69022AD3E4CA3053A2E704D03583FB8B85534B06;
extern const uint32_t g_rgctx_T_t00E0F792EFBBB7CEB35858347DEB6F4F78EE84AC;
extern const uint32_t g_rgctx_T_t9EBE1E3BF279F72BFABFE776C07EE27175DB179B;
extern const uint32_t g_rgctx_T_tDF6FA0132A0DAF62F1062D93307A7A7CB8BF41CC;
extern const uint32_t g_rgctx_T_tDF6FA0132A0DAF62F1062D93307A7A7CB8BF41CC;
extern const uint32_t g_rgctx_NetworkReader_ReadBlittable_TisT_t1CD5BEBCA831A04806D9C686E19FA8A88022EDD2_m8CE5FFFE9824C81792739EED460709055796A301;
extern const uint32_t g_rgctx_Nullable_1_tED1B6437BDA108A568BA016B66B69A206AF6D7A9;
extern const uint32_t g_rgctx_Nullable_1__ctor_m694DD22142018DBBFC38F5F868A52745373197D3;
extern const uint32_t g_rgctx_Reader_1_t7C061865EF93A94AFC3AD12A9DB49AF0F904A05A;
extern const uint32_t g_rgctx_T_t7B529C29664E0228E6271C873F4987647FBBC3C2;
extern const uint32_t g_rgctx_Func_2_t84CD5734738E60A9DD2B70B270D28F83DB66BA79;
extern const uint32_t g_rgctx_Func_2_Invoke_mE1113A9BE951A31EC8C52A72AD594E9A7EA3B655;
extern const uint32_t g_rgctx_T_t1F9CCFC4150283498C64C236C3DB7FD7E67FD775;
extern const uint32_t g_rgctx_List_1_t6A47AA30C555AF25EB2CF8638D641534C1DD6177;
extern const uint32_t g_rgctx_List_1__ctor_mE4DFB3808BB6BE8C86B5C13EA84608AAF85C5D98;
extern const uint32_t g_rgctx_NetworkReader_Read_TisT_tD5AE035A1E6217C76CC767E1727D6C5E4A018AC4_m0F35CAED283D70F94B470A07F81DFD93812D3FDE;
extern const uint32_t g_rgctx_List_1_Add_mBBCC509A4B4A5204A87D7B2C1EC7E84F9D19ACD8;
extern const uint32_t g_rgctx_TU5BU5D_t496DFB72EF11B2A89E296E87E8F1FFF8F1F626CB;
extern const uint32_t g_rgctx_NetworkReader_Read_TisT_tC3618DE086915E6BB25FA6D90192B0B2DDD23B5F_mC8C8630493CF3FC406B3B40AD24183E4647D7FA3;
extern const uint32_t g_rgctx_MessagePacking_Pack_TisT_t7CD2EC9C62A1AD97F6FD8DA162FC91C04E2DB65B_mE7531320CB7DCD359E448A8A3BBB11DBF8BCAF1B;
extern const uint32_t g_rgctx_NetworkDiagnostics_OnSend_TisT_t7CD2EC9C62A1AD97F6FD8DA162FC91C04E2DB65B_mFB70BDF16326A34451A082B13F41FBF44213AF5D;
extern const uint32_t g_rgctx_NetworkServer_SendToAll_TisT_t5FF8A8EAC7FDAD40D9293234906C66BE00EA744D_m49F1E4B1FF418B3D9076B7467AA458455FDCBEEE;
extern const uint32_t g_rgctx_MessagePacking_Pack_TisT_t01C7B96EBBDE6AC2D090BBC248548149F724D458_m78D7D9E5484BA8E6EFA1A9C3C43253AD3DF49F6E;
extern const uint32_t g_rgctx_NetworkDiagnostics_OnSend_TisT_t01C7B96EBBDE6AC2D090BBC248548149F724D458_m012E887888DF6EB1595C732DE3DB1B3DD27F27A3;
extern const uint32_t g_rgctx_MessagePacking_Pack_TisT_t91F0CC010FB9DEFEA6763E3B991BEDDD4F99F17F_mC5D14A60B7FC15DA6D71B01F1E059D2B7A37CD23;
extern const uint32_t g_rgctx_NetworkDiagnostics_OnSend_TisT_t91F0CC010FB9DEFEA6763E3B991BEDDD4F99F17F_m78D613A2830660559C5335D74C44A2D86338C191;
extern const uint32_t g_rgctx_NetworkServer_SendToReadyObservers_TisT_t6B75A0BFA1E98EB2B28AFC7CF2C2B0785E240E6B_mEDEB0D25D31A21049FC91DD84F7C5A5CA0C2B2B2;
extern const uint32_t g_rgctx_NetworkServer_SendToReadyObservers_TisT_tD9129F7563C97C96A3D505545119AB4E57E64E42_mB0413FFD8A2CDAA857F9D12C0876A231C436712B;
extern const uint32_t g_rgctx_NetworkServer_SendToReadyObservers_TisT_tD3652448C0A0BEF945E00871DA9441A7F397D2EC_mAD9DC2CD43E7EF8E353BCD77A037CB5182EE4CB6;
extern const uint32_t g_rgctx_MessagePacking_GetId_TisT_tE7D431B3102F446D782816319519B7584447E4BA_mCE8A5C459208FA1F1C3C3018D0B793BB16DAD619;
extern const uint32_t g_rgctx_T_tE7D431B3102F446D782816319519B7584447E4BA;
extern const uint32_t g_rgctx_MessagePacking_WrapHandler_TisT_tE7D431B3102F446D782816319519B7584447E4BA_TisNetworkConnectionToClient_t80F9FBDD786601CB93A63585D05BCAA1050C406A_m68C33C7A0126CEC8E8C9E5BB6876D45947C8B217;
extern const uint32_t g_rgctx_MessagePacking_GetId_TisT_t25912D0832FFFE8C56A0018C8066528334489777_m14E0E817279617225CE25EBCAEBA85EC1442F82D;
extern const uint32_t g_rgctx_T_t25912D0832FFFE8C56A0018C8066528334489777;
extern const uint32_t g_rgctx_MessagePacking_WrapHandler_TisT_t25912D0832FFFE8C56A0018C8066528334489777_TisNetworkConnectionToClient_t80F9FBDD786601CB93A63585D05BCAA1050C406A_m2ACA41504DCD7895A3635515F5448AE99AAFBBAB;
extern const uint32_t g_rgctx_MessagePacking_GetId_TisT_t8B2A57F699BB1E21D1B75FAEDB781A6AEA4E8764_m28745DDF1309F0DED828E7E1773A83C35CF6C85C;
extern const uint32_t g_rgctx_MessagePacking_WrapHandler_TisT_t8B2A57F699BB1E21D1B75FAEDB781A6AEA4E8764_TisNetworkConnectionToClient_t80F9FBDD786601CB93A63585D05BCAA1050C406A_mCCC845510B433329ADD19A65F1BF61DEFF72264A;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_t1483A0B6F1CB637FBEE79DC6E144DFFB46EC9C6D;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1__ctor_m8CE3DD2B7277F600712064DCB3F6764C4AE5F7B3;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CReplaceHandlerU3Eb__0_m50FEE2C5133D1B7DED20E8FECF72F31DDCE39226;
extern const uint32_t g_rgctx_Action_2_t2A8DC00416466F11084CAED8728A8B97C94349F0;
extern const uint32_t g_rgctx_Action_2__ctor_m82D1B9C817560E27A556474F3B1D69C4343A7B60;
extern const uint32_t g_rgctx_NetworkServer_ReplaceHandler_TisT_t4B186920175327428B661168FC0851B1058F13BE_m328A98B02D0563A28EA383DA3F3DB3705228FCCF;
extern const uint32_t g_rgctx_MessagePacking_GetId_TisT_t5AE160A4F8AF9ADBA0C1FA9E9AA83892ACE0CB14_m3466B6676CC857E2D91A167D1E905234E4BB8B9E;
extern const uint32_t g_rgctx_Action_1_tCFC5FA47B86CD5941A824DB382D423097A4C5A6D;
extern const uint32_t g_rgctx_Action_1_Invoke_m5E07A731F22FE5B88190EAE05D1CB4502C13675A;
extern const uint32_t g_rgctx_T_t1474E8BC1A3D6DD02EB7B843DAFFDC10268BB884;
extern const uint32_t g_rgctx_Nullable_1_get_HasValue_m2D65FF808D0C72EC1E1B242BE2811CE724535E80;
extern const uint32_t g_rgctx_Nullable_1_get_Value_mDF5A97CC0ED0040355013FA1EFB15DAF7BC64AFD;
extern const uint32_t g_rgctx_NetworkWriter_WriteBlittable_TisT_t4B6C9ED0CB1F5476267FED3F41849EB6A432E817_mF1A74AE5E716D49DC93FC0F2A7F74B826C47EDFE;
extern const uint32_t g_rgctx_Writer_1_t4FC723C83C9530FABDFFE5BF7993E398C738A848;
extern const uint32_t g_rgctx_T_t0C38038C1399EF21FD04A08E1FCD09EBD0C7DED1;
extern const uint32_t g_rgctx_Action_2_t7F35554DFCA69963A4B9CE151051D59FD537ABB3;
extern const uint32_t g_rgctx_Action_2_Invoke_mCD65377EF0978BCF302AC268C958EF1F09EB71BD;
extern const uint32_t g_rgctx_ArraySegment_1_get_Count_m6DCE9944A1DAA2674CEF74FA0CA947CCE2BD9244;
extern const uint32_t g_rgctx_ArraySegment_1_get_Array_m684FFD03E76DA63DC60C749F591729FB88937D8A;
extern const uint32_t g_rgctx_ArraySegment_1_get_Offset_mECAB0D45215538EB5B33248C4DC54FFE2E54A265;
extern const uint32_t g_rgctx_NetworkWriter_Write_TisT_t7FAEFCAFCEADAF391B8327AF09C8443C27538F98_m56862B533175C98CD53888DEC837D44669888F68;
extern const uint32_t g_rgctx_List_1_t894F0E88EE44835A3895B1D80DC1BB3EC00EB7D0;
extern const uint32_t g_rgctx_List_1_get_Count_m1BBB6A544D8908ABEC587BF5EF69EA888A3C2B09;
extern const uint32_t g_rgctx_List_1_get_Item_m20FDDB9BF6C41A70C89C0BCC67A9A5F7001BAC0D;
extern const uint32_t g_rgctx_NetworkWriter_Write_TisT_t66B8EEC92D05245F9C899E27DD855A719E932D90_m618E399545056F28773351F268A6940194CD8D52;
extern const uint32_t g_rgctx_NetworkWriter_Write_TisT_t901E69ACDF2AA0BB82ECB44F41C09E2855E2E9BF_m48C1134EB2AD8E0D1E346F8F83979F7C7E993C11;
extern const uint32_t g_rgctx_Stack_1_tE41867904A4BD9B9AD9F9D7DB12D1E33B179AE92;
extern const uint32_t g_rgctx_Stack_1__ctor_m4D5F53F90ACD6774ACB49DACCE8ED069D0B66197;
extern const uint32_t g_rgctx_Func_1_t99F9FD808F6F6B9EFCEB8ADAAE5153CBDEE087DF;
extern const uint32_t g_rgctx_Func_1_Invoke_mCAF774BB0296060606AAB9EF8A91BC4EA7EE6CFC;
extern const uint32_t g_rgctx_Stack_1_Push_m2CC3848E065F7900E657FB882750C21C37FFC09C;
extern const uint32_t g_rgctx_Pool_1_Get_m38E0B03A58B9C071B059958EF2AD0E640B3953F2;
extern const uint32_t g_rgctx_Stack_1_get_Count_m6DDA2F53B317B6000A1457FE744509D93222954C;
extern const uint32_t g_rgctx_Stack_1_Pop_mFFFCD1C6F63A53FBA15248EE2FB5784FFA707221;
extern const uint32_t g_rgctx_T_t89528DA5AA7EEA0062FD8D0EE7E86949BEAFEBB9;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_t89528DA5AA7EEA0062FD8D0EE7E86949BEAFEBB9_Snapshot_get_remoteTimestamp_m0527AF79D8BECBCC0ED78DE5CF959E3CA13A68F1;
extern const uint32_t g_rgctx_SortedList_2_t8726DEF09A8522DB60D10ED00E0F72939986448F;
extern const uint32_t g_rgctx_SortedList_2_get_Count_m2CEFEEF6CD800BAC0B7F7540BDAAEDBE99816060;
extern const uint32_t g_rgctx_SortedList_2_get_Values_m99499492796D9218BB9E7CABEA26F6981114758C;
extern const uint32_t g_rgctx_IList_1_t0993FF494379228DEAA664AA99FA495109F16294;
extern const uint32_t g_rgctx_IList_1_get_Item_m773A3DC0BD74D0F969819F56EB30CCB52D99269E;
extern const uint32_t g_rgctx_SortedList_2_ContainsKey_m54C418782B97E53D2A814D4916101FCC712533E7;
extern const uint32_t g_rgctx_SortedList_2_Add_m2B59F8AAF324B909439D3A1C74CEDAEA6168327A;
extern const uint32_t g_rgctx_SortedList_2_tB587957E14014BCFAF69C93124442411EE097962;
extern const uint32_t g_rgctx_SortedList_2_get_Count_mC9BDD3B925DC39CFF78497B62594AD81EBBBCB03;
extern const uint32_t g_rgctx_SortedList_2_get_Values_mDDD9DBEF35EF3207000D6E16F38AABF4C11C0904;
extern const uint32_t g_rgctx_IList_1_t59CD2D14FCD76B158B9FD7B73BC04255FAAB0035;
extern const uint32_t g_rgctx_IList_1_get_Item_mDA1683CD9ED4CE20406F39A813919C06588812C3;
extern const uint32_t g_rgctx_T_tA1E2C962DC5AB3B076A2B3B434E37468C4E0784D;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_tA1E2C962DC5AB3B076A2B3B434E37468C4E0784D_Snapshot_get_localTimestamp_m297E9654A2BE6D19A72C09500B81AF147167617E;
extern const uint32_t g_rgctx_SnapshotInterpolation_HasAmountOlderThan_TisT_t294A9678E9F39BF583F821F1F80CC8E3FC96EB86_m28F6DEB0AA5D79D69E8E000864042447C823149A;
extern const uint32_t g_rgctx_SnapshotInterpolation_HasAmountOlderThan_TisT_tF69855E1E692FF54C69A1373BE8BF3533019711D_m176D41CA10DD7288A8C70863C775F0EC532345C0;
extern const uint32_t g_rgctx_SortedList_2_t3186D968F22573D8A79EAEE04A3A677136FAEB7A;
extern const uint32_t g_rgctx_SortedList_2_get_Count_m21520658084EC20E636312DA9687ECC0DC347011;
extern const uint32_t g_rgctx_SortedList_2_t38403A078EDCDA5E39767C70FD861DB07021EB88;
extern const uint32_t g_rgctx_SortedList_2_get_Values_m808D6AB02812F273A3D2BE18A226D9FC85D93758;
extern const uint32_t g_rgctx_IList_1_tE31C108874BCF50AE5078833C477538B1D04AF81;
extern const uint32_t g_rgctx_IList_1_get_Item_m4FD0A3FA52F4518F0D11170CCCDADF40CCC32986;
extern const uint32_t g_rgctx_T_tE7866AC3F882BBCDBFB17F87C95F35A6667763E7;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_tE7866AC3F882BBCDBFB17F87C95F35A6667763E7_Snapshot_get_remoteTimestamp_m0527AF79D8BECBCC0ED78DE5CF959E3CA13A68F1;
extern const uint32_t g_rgctx_SnapshotInterpolation_HasEnough_TisT_t7000DAC998F9F689E7EB30E3F9DB59CBAF33E0FB_m84641562AD7FD13CF41B71B5A84DA8D5B7CDFDE3;
extern const uint32_t g_rgctx_SnapshotInterpolation_CalculateCatchup_TisT_t7000DAC998F9F689E7EB30E3F9DB59CBAF33E0FB_m1A2A715B4A00B3296C5FA952AA415B96F620EF80;
extern const uint32_t g_rgctx_SnapshotInterpolation_GetFirstSecondAndDelta_TisT_t7000DAC998F9F689E7EB30E3F9DB59CBAF33E0FB_m051C810B4FDA17BCA52A7D13027ADC913DD35939;
extern const uint32_t g_rgctx_SortedList_2_tDF34A61256DC9FCB808105E2C7BF84FB6198D6F3;
extern const uint32_t g_rgctx_SortedList_2_RemoveAt_mFAA1E454747D63B21DC460E90D41DEF1EBDC226E;
extern const uint32_t g_rgctx_SnapshotInterpolation_HasEnoughWithoutFirst_TisT_t7000DAC998F9F689E7EB30E3F9DB59CBAF33E0FB_m9C2C7E7DB29F42E50F653D39B4B41464599FE8C8;
extern const uint32_t g_rgctx_T_t7000DAC998F9F689E7EB30E3F9DB59CBAF33E0FB;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_t7000DAC998F9F689E7EB30E3F9DB59CBAF33E0FB_Snapshot_get_remoteTimestamp_m0527AF79D8BECBCC0ED78DE5CF959E3CA13A68F1;
extern const uint32_t g_rgctx_Func_4_t9BF327645D974653FBC1AF101BF2D8B05E8880B2;
extern const uint32_t g_rgctx_Func_4_Invoke_m3A3F664AC42AFE8E844D81C6F0EC06A3EBA4CC83;
extern const uint32_t g_rgctx_ICollection_1_t8B20EE12C1B93DB25DA6AEC5AFDFEAE76230528E;
extern const uint32_t g_rgctx_ICollection_1_get_Count_mFA85478E0D60FA0997C1587DBB620997076542FA;
extern const uint32_t g_rgctx_SyncDictionaryChanged_tED96A0B6B1CDA35728415B07893EB74513AD7571;
extern const uint32_t g_rgctx_SyncIDictionary_2_set_IsReadOnly_mA87550DA7C2E1357432CF3095356F159EE5D7FB1;
extern const uint32_t g_rgctx_List_1_tE4BC89115BE946EAD0A028C07FF5717C0CCA051A;
extern const uint32_t g_rgctx_List_1_Clear_mBF3E04999DB852556A74B4C54370D7B49B2CEB82;
extern const uint32_t g_rgctx_ICollection_1_Clear_m5EA7F97512D657210E70B27A200B63D7C388547A;
extern const uint32_t g_rgctx_IDictionary_2_t7B061B92288B8CC6CC49EAB0D87B562F070C312A;
extern const uint32_t g_rgctx_IDictionary_2_get_Keys_m124811A3449C391BD72F761912CBDA15E17D6D8B;
extern const uint32_t g_rgctx_IDictionary_2_get_Values_m465C38DF45FFE24B27EE11102A7128DF9E264C70;
extern const uint32_t g_rgctx_List_1__ctor_mF34EE1E0AA6398F1205D9B78F9DC073430219F99;
extern const uint32_t g_rgctx_SyncIDictionary_2_get_IsReadOnly_mF59318B25739E6DDCB916894C4ADD81004AA76AC;
extern const uint32_t g_rgctx_List_1_Add_mE44D4B55B3789411E06F74CA6D9E0FFBD348469C;
extern const uint32_t g_rgctx_SyncDictionaryChanged_Invoke_mE9945090EF6D1F9C3383664630E6359E7D560EB8;
extern const uint32_t g_rgctx_IEnumerable_1_t28F2C7D4D508C5CA96EABE29304789D95B5C4A11;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_m1A5ECD7541DA526D96781846FB9E311C603FE9C0;
extern const uint32_t g_rgctx_IEnumerator_1_tE55705B3E2F022DDAA83183612595EF0156D337F;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m11F923DD1FC14F0B7B1266A1800DFE193AEB5890;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_m0B91A1C1CD9A3DF740D2B31645FC1DBEA29A9CD3;
extern const uint32_t g_rgctx_NetworkWriter_Write_TisTKey_tAD9F9054FC32C5D15971CF2C8B1B95E8C3C3EF08_m9EAB8A3B3AB542EF3CD422BAD4FC10D586370FAB;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Value_m3181856732170751C1595A95E9D2ED4B53E4BCAE;
extern const uint32_t g_rgctx_NetworkWriter_Write_TisTValue_tCF3048D07B8CF615D3DE43668A25538C440B9721_mC1AD9308BF42AEDE75D16D2ACF8E171D88B78523;
extern const uint32_t g_rgctx_List_1_get_Count_m0594E2A359407D80E21CE8F5D78F56C7776C3952;
extern const uint32_t g_rgctx_List_1_get_Item_mF3231EADCBA89FE15E3249D0B6F5F542C5D5AE59;
extern const uint32_t g_rgctx_NetworkReader_Read_TisTKey_tAD9F9054FC32C5D15971CF2C8B1B95E8C3C3EF08_mF5AC515AEC01BB2CC5422ECA18973D4706A5E1A1;
extern const uint32_t g_rgctx_NetworkReader_Read_TisTValue_tCF3048D07B8CF615D3DE43668A25538C440B9721_m053468B60AB6B44DDC15BED189B07630EF77AF4F;
extern const uint32_t g_rgctx_IDictionary_2_Add_m8B9CD4E9952F7679BB4738B92BC3E525F5F2C8DA;
extern const uint32_t g_rgctx_IDictionary_2_set_Item_mE68FAB37ECD6426B8972D992D7CA401A49AB7BF1;
extern const uint32_t g_rgctx_IDictionary_2_Remove_m7B229A5996D91CF9194598879FFDC0B22D2F21E4;
extern const uint32_t g_rgctx_SyncIDictionary_2_AddOperation_m4C5E4D2AAA4C518497DEEE7C5ECA8FCD957496A7;
extern const uint32_t g_rgctx_IDictionary_2_ContainsKey_m42EC3DF9DDB51C43226D8A76E794715A6EFEB071;
extern const uint32_t g_rgctx_IDictionary_2_TryGetValue_mA74A43A4936AC069517AA517EA28B15B9555ED0D;
extern const uint32_t g_rgctx_IDictionary_2_get_Item_m576B27E3F6AE9A0BA085C43A3423FDFB635D602F;
extern const uint32_t g_rgctx_SyncIDictionary_2_ContainsKey_m99561BFE9CDA02A703EA2E4F3CB267BEDC29568B;
extern const uint32_t g_rgctx_SyncIDictionary_2_Add_m6DF39E9C6E3531DBAD8CDA72D4176B41956AF72D;
extern const uint32_t g_rgctx_SyncIDictionary_2_TryGetValue_mAEE7B2C1D9B793C012FF9EF1C4EC827437B012DC;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_mFB4ACB2FCA8D788D430A6EBE2D60DD57E5B7B559;
extern const uint32_t g_rgctx_EqualityComparer_1_t51BE012734ED54BB143E320B1E3D77734DE5AF50;
extern const uint32_t g_rgctx_EqualityComparer_1_t51BE012734ED54BB143E320B1E3D77734DE5AF50;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m6B560F9A0B1B14C77837B651B8EA8BCFBD17BA5A;
extern const uint32_t g_rgctx_SyncIDictionary_2_get_Count_m8E3EE8F737806147D63FC6C3619963D7B8863C8E;
extern const uint32_t g_rgctx_Dictionary_2_tCBCE1356C517DAB1A630E9193FBD27ADFDA63340;
extern const uint32_t g_rgctx_Dictionary_2__ctor_mBF272A1F9BC74EB6134C58F20BAD788BAF47EB68;
extern const uint32_t g_rgctx_SyncIDictionary_2__ctor_m5AC12BEFE5090C47353042F233C342428EC6DF96;
extern const uint32_t g_rgctx_SyncIDictionary_2_tC1FC455805D09111936D8D560F7F457947DDEE6C;
extern const uint32_t g_rgctx_Dictionary_2__ctor_mC6C6B6ABF2A4F5E123191AEF9E0BED7037B53D26;
extern const uint32_t g_rgctx_Dictionary_2__ctor_m9B924957A4CA46FFBEF6ED904F56926990EDFC84;
extern const uint32_t g_rgctx_Dictionary_2_get_Values_m5B3A92DBB61E6A0EA017B8C122533856AB1AFF12;
extern const uint32_t g_rgctx_Dictionary_2_get_Keys_mEF5A4E3ACCACD2A5B18ED7B9C4E031625CBC9904;
extern const uint32_t g_rgctx_Dictionary_2_GetEnumerator_m7B526D9DAC4ED78CA2BC31FE1E715C3DDF5692BE;
extern const uint32_t g_rgctx_ICollection_1_t1A87B3B16F46C0F31DA1EEC90585E1AE5828CF45;
extern const uint32_t g_rgctx_ICollection_1_get_Count_mD12961EFAE0A16D7AE327D6BFA5F6AB2D7AA8FDE;
extern const uint32_t g_rgctx_SyncListChanged_t68F3612B6BCA7C4B6EE16D9CBA68F343AD8E1FAB;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_m179CCC5C325E588E195BBC0FC9839922C471BD4E;
extern const uint32_t g_rgctx_EqualityComparer_1_t71BB6716BFBD9404DD4D157B5A4739681DF4A798;
extern const uint32_t g_rgctx_SyncList_1__ctor_m49B74E63A7B8D7582ED6EAE7D3CFEF9BB96BC468;
extern const uint32_t g_rgctx_List_1_tF31653A4543288F92927F24626567C642ACAB086;
extern const uint32_t g_rgctx_List_1__ctor_mDBBEB85C0F922B32F4BC6E28224F7062728421EF;
extern const uint32_t g_rgctx_List_1_tA7030C63CE1885AA91EA066335F14B7F09F0C54D;
extern const uint32_t g_rgctx_List_1__ctor_mA8D3D32B098981208C6DF25B3BBEF6C96D08C011;
extern const uint32_t g_rgctx_List_1_Clear_mCB63A1FC250B09FFF27524FDD74E58D315D0457D;
extern const uint32_t g_rgctx_SyncList_1_set_IsReadOnly_m0D6B500FE04895F6D671EFC433CEB60E38060B6F;
extern const uint32_t g_rgctx_ICollection_1_Clear_m38CC4C9A099D6F2B4B928D2C1DBF3B6DA425F01D;
extern const uint32_t g_rgctx_SyncList_1_get_IsReadOnly_m0850F414A948523C68F425649448F70E132EE733;
extern const uint32_t g_rgctx_List_1_Add_mA620B6FB6C21BE99BE5726AEA68FE1B0ED6677DF;
extern const uint32_t g_rgctx_SyncListChanged_Invoke_m02948903A940B9D4924858BF8EABDF2BBB135F5E;
extern const uint32_t g_rgctx_IList_1_t124C48A37312A8F8F3F3D9F10F74B5F0FF611EA9;
extern const uint32_t g_rgctx_IList_1_get_Item_mDCDDEED97123871B8FC41070396B269D69C9EA1C;
extern const uint32_t g_rgctx_NetworkWriter_Write_TisT_tA3C92BC449FE896282C9547B2A522013F31854A6_m327B80BADD2EAA1388E43657F8A6F10A7383535D;
extern const uint32_t g_rgctx_List_1_get_Count_m2F133DBB94E4EBFCE522DACD395B861A7089DA8A;
extern const uint32_t g_rgctx_List_1_get_Item_mE7ABC2C93478C194BDF7E82960C568FB401E79B6;
extern const uint32_t g_rgctx_NetworkReader_Read_TisT_tA3C92BC449FE896282C9547B2A522013F31854A6_m463C44B4B3917F1DE8A08ADD5A1FC0374F3B2427;
extern const uint32_t g_rgctx_ICollection_1_Add_m4DC9D2CCEED77DDD4FA541021DA81DDB9E0CC70E;
extern const uint32_t g_rgctx_IList_1_Insert_m387ABF4ADA60FF59EE06B212FB9AD50AE2EBB12B;
extern const uint32_t g_rgctx_IList_1_RemoveAt_m6D4427AA33A91A6DAD57BBD09EE9CA1ABAE4FEC7;
extern const uint32_t g_rgctx_IList_1_set_Item_mE5998087F8C26940BE5674FF4308C5935F47F725;
extern const uint32_t g_rgctx_SyncList_1_AddOperation_m41C01C36907674D255D83A1EA83273D16EBD0A18;
extern const uint32_t g_rgctx_IEnumerable_1_tD2A0775CFB034B23DC5017071CABB4C5CEA465F9;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_m7A29DAECD0C84F14709B1C55B858BA3858B1DE47;
extern const uint32_t g_rgctx_IEnumerator_1_t31833133BBDEC7A83A069355BB14B0254057793F;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m3002EA7067110E5EA52263D16B0B33A3282D4B67;
extern const uint32_t g_rgctx_SyncList_1_Add_mC0F138AE99A0B29A84E717946811FC19A804FC15;
extern const uint32_t g_rgctx_SyncList_1_IndexOf_m6D15C126B5A5D88A5B71746032BA100D8AE09501;
extern const uint32_t g_rgctx_ICollection_1_CopyTo_mC431F4E294A5D87586AF35AB04F85D3125D8DA9C;
extern const uint32_t g_rgctx_IEqualityComparer_1_t7F71CB094D169E43C61B44C32EB1431AF59091F0;
extern const uint32_t g_rgctx_IEqualityComparer_1_Equals_m4E0CC461D19E9FA17A42C90A5C36723D402F7353;
extern const uint32_t g_rgctx_Predicate_1_tAF28D2C4874540F3DDAB2B888E508B0B3A69C016;
extern const uint32_t g_rgctx_Predicate_1_Invoke_mBA0251F4F4EDF82378E9687CE1FF0C7C2B7B96CB;
extern const uint32_t g_rgctx_SyncList_1_FindIndex_mCDE85258011536F8B60983275F780CCEA157363E;
extern const uint32_t g_rgctx_List_1_Add_m845FAD005CEBB8856D8EF501DF6CB379E7C94C58;
extern const uint32_t g_rgctx_SyncList_1_Insert_m106CFB9A6974C3312AF62FE7D85BFD1F30E3756E;
extern const uint32_t g_rgctx_SyncList_1_RemoveAt_m012E35A398E22EF8E3256F94B0FDA648A6C67C9B;
extern const uint32_t g_rgctx_List_1_GetEnumerator_mD6EEDA07A83493D212CDEAEFA25C0EA7FE80C8D6;
extern const uint32_t g_rgctx_Enumerator_get_Current_m8D3C62EED65871DF3059FB30E87FDD4A1B14090E;
extern const uint32_t g_rgctx_SyncList_1_Remove_m9D10CE3B1E70544DC9285CCFA5096F32A23E940B;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m84DCFF287C0FADBA94320B80C5A394225999ECF0;
extern const uint32_t g_rgctx_Enumerator_t426E049849334D0037E1AE6AA8493E2B55F160A6;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_t426E049849334D0037E1AE6AA8493E2B55F160A6_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_List_1_get_Count_mFC6406FF44895409FA04DE67BC1F66ABA4FB7513;
extern const uint32_t g_rgctx_Enumerator_t17E12781F75C3235F022AFBA1081D7F57C6D3C97;
extern const uint32_t g_rgctx_Enumerator__ctor_mF74B750E64033549CD4D904E50A2D046C8A1A437;
extern const uint32_t g_rgctx_Enumerator_set_Current_mF18D3C72C5C8D8D421E610B85F014E9FF832F899;
extern const uint32_t g_rgctx_SyncList_1_t7279AF626D1A3CD625FB0ABEDAFCF8CC4F433E49;
extern const uint32_t g_rgctx_SyncList_1_get_Count_m4FA71676637044EC010A8A6935BAB2D686B5D92C;
extern const uint32_t g_rgctx_SyncList_1_get_Item_mFA19DF0CF2C29727C0BD3CAB7D818A251FFA5D58;
extern const uint32_t g_rgctx_Enumerator_get_Current_mB086E7C3F9D232DCBD2E47B2BCD5C0C3FCC1F5F4;
extern const uint32_t g_rgctx_T_t4835924C45A5161E8B294F4CBE509C72C0A26889;
extern const uint32_t g_rgctx_ICollection_1_tE89C0BEA80162FB48A74AB195985FAFCD2259338;
extern const uint32_t g_rgctx_ICollection_1_get_Count_m593FD9351E224DFF2C35EDE969C4F174FE41BB50;
extern const uint32_t g_rgctx_SyncSetChanged_t4C348DC44724F96D1D750B07F9E00E1B78D6CEF7;
extern const uint32_t g_rgctx_List_1_t96BFD07477D5F7C11580BFCB4ACE9DE3EB9E4599;
extern const uint32_t g_rgctx_List_1__ctor_m345233E8698434BAF539A18FAD2C31C648653F23;
extern const uint32_t g_rgctx_SyncSet_1_set_IsReadOnly_mB33D6514B528A1C80BDACB6878E0771BF969638C;
extern const uint32_t g_rgctx_List_1_Clear_m095D8D83AC1E8A0CA376CB37CD26FABD4C0AE56E;
extern const uint32_t g_rgctx_ICollection_1_Clear_mCB6E77FE90FD125625C8B0E3B6CEC80160C70B14;
extern const uint32_t g_rgctx_SyncSet_1_get_IsReadOnly_m24958F98ABF53BA531E4FA321C4E7F751342A500;
extern const uint32_t g_rgctx_List_1_Add_mB1D189C43660F7996273789FB72D313CA9E81709;
extern const uint32_t g_rgctx_SyncSetChanged_Invoke_m57284E4E9F8309D98F94318E3E043C255DE6A109;
extern const uint32_t g_rgctx_SyncSet_1_AddOperation_m61F65CFFE22814B8B509BE1CAFA945C801698ABE;
extern const uint32_t g_rgctx_IEnumerable_1_tA9CCF72BF11D17764D2F188D37F86B1679752D2E;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_m922D58C38CF48D24AD9B2550AA0EED9024790370;
extern const uint32_t g_rgctx_IEnumerator_1_t5DBA2302BB200F2B13268E63716BA2B9EE7DD8C2;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_mD10A1D2986136754DAA533D4218D2D2D362E1B13;
extern const uint32_t g_rgctx_NetworkWriter_Write_TisT_t871D28ED157FDD035D66411B650D93EBCF0663E5_m19D2B3ACA9AB8A082CDD55A81E9629CEC42FAEF8;
extern const uint32_t g_rgctx_List_1_get_Count_m4DFC4CFC501467DF325B5139B04C68B9E973BA45;
extern const uint32_t g_rgctx_List_1_get_Item_m13A6B93AA344EB1C646062AB66A69155D41B217C;
extern const uint32_t g_rgctx_NetworkReader_Read_TisT_t871D28ED157FDD035D66411B650D93EBCF0663E5_m79F74BCBF8CC4BEE0AF774294D04435B51A16E4D;
extern const uint32_t g_rgctx_ISet_1_tA2115719BF049635CF3C9008F87E8A0D4A02A7F7;
extern const uint32_t g_rgctx_ISet_1_Add_mA5044DDFA40AC4D5C619E8347F075626FE18FCE3;
extern const uint32_t g_rgctx_ICollection_1_Remove_m695BA01620751D2FBEB16E15AAD5FDB767948199;
extern const uint32_t g_rgctx_SyncSet_1_AddOperation_m117E5FA8F2DB33B66012FC155E1A423EDE38AD9C;
extern const uint32_t g_rgctx_ICollection_1_Contains_mD1D03DD5C9D97E1A0564A52356728BE7F79B27E9;
extern const uint32_t g_rgctx_ICollection_1_CopyTo_m53481449415F609558CB9F6262D09883410EAD22;
extern const uint32_t g_rgctx_SyncSet_1_GetEnumerator_m87943530388CF1FD37168FBB54DF96623ADFEEEC;
extern const uint32_t g_rgctx_SyncSet_1_Clear_mE335789C54EDBEEDD78C43D78AD4C33FDBEE110C;
extern const uint32_t g_rgctx_SyncSet_1_Remove_m6E50E7FF4AA91DD2E05582C0C721D0A1E4E43F32;
extern const uint32_t g_rgctx_SyncSet_1_IntersectWithSet_mC2B8D63B29963B1567B11D6A9F4D1422739F1EB8;
extern const uint32_t g_rgctx_HashSet_1_t4D392AF75C81099D0F904E47166CD88355F6313A;
extern const uint32_t g_rgctx_HashSet_1__ctor_m53541564B69F712E08FAFAB9BF88906D99548509;
extern const uint32_t g_rgctx_List_1_t4758659D07234AAF31E8B051493D4513796EB905;
extern const uint32_t g_rgctx_List_1__ctor_mEC97FEA0C8FC02CF25787C97B964C83595511FA7;
extern const uint32_t g_rgctx_List_1_GetEnumerator_m49CE0AF7BB8DC2E5C24725858D781F6489622CC2;
extern const uint32_t g_rgctx_Enumerator_get_Current_mAFF6074687538FBDFF9C58CD215FF95FB2B0523A;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m0B51D9A3738FF739BDD29FF813628CC208136DAB;
extern const uint32_t g_rgctx_Enumerator_t9DAF482D8206366B9EA16C04930FF3659AFFA953;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_t9DAF482D8206366B9EA16C04930FF3659AFFA953_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_ISet_1_IsProperSubsetOf_m9169483187C1293F2675FE78ED751E389C1B62E3;
extern const uint32_t g_rgctx_ISet_1_IsProperSupersetOf_m3D8254C9E4F446050118162801D0EA7018C07C6F;
extern const uint32_t g_rgctx_ISet_1_IsSubsetOf_m2ED6F7417BEEBF10B210999BA6754EEF4E519560;
extern const uint32_t g_rgctx_ISet_1_IsSupersetOf_m8CE58AB1BB5F4685F61860C85020701C917A4173;
extern const uint32_t g_rgctx_ISet_1_Overlaps_m02E1DD4B13CC9F714ADA620E7A396776ED59E876;
extern const uint32_t g_rgctx_ISet_1_SetEquals_mA641DEF01686A78712A743389D26640757F0CCFE;
extern const uint32_t g_rgctx_SyncSet_1_Add_mE565C7345A1B79B308C863C728B201F1ED8F927E;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_m5C7562C58A913CA11FE8FD79BCDB0E848D216A24;
extern const uint32_t g_rgctx_EqualityComparer_1_tC991FBF1A784E528D63C9330A38BBB19EE4CB015;
extern const uint32_t g_rgctx_SyncHashSet_1__ctor_m117C1D65525A35811AFD50405CA9F0A3902B225B;
extern const uint32_t g_rgctx_HashSet_1_t80CEA056BD275FC5A091EF3C9545E0C6C3F18B06;
extern const uint32_t g_rgctx_HashSet_1__ctor_mAA4B80BA49CF68DCDD1C8F46DD02807364366237;
extern const uint32_t g_rgctx_SyncSet_1__ctor_m6B1B96249878AFD48946BCCD3F3611BC6CC17669;
extern const uint32_t g_rgctx_SyncSet_1_t0AF185D3B61C3E6360E19EF504916ABA87975535;
extern const uint32_t g_rgctx_HashSet_1_GetEnumerator_mD42F43AA5CDBD10AB7F9FE4A5253B30531FF9430;
extern const uint32_t g_rgctx_Comparer_1_get_Default_mF1BD84689F815FB0C606C551B6542DB3048BD625;
extern const uint32_t g_rgctx_Comparer_1_t9077364B39202C626E6BDE2E7315A27000C996F2;
extern const uint32_t g_rgctx_SyncSortedSet_1__ctor_mDA68FEEEC46BEF0D025477B6C080401F2B5C49B9;
extern const uint32_t g_rgctx_SortedSet_1_t1ADEA36D2C0EE0A41D5238C702C86BFDB39A7AB4;
extern const uint32_t g_rgctx_SortedSet_1__ctor_mCE3E5234C2D16BC64E7BFE13E45094F93F2E658F;
extern const uint32_t g_rgctx_SyncSet_1__ctor_m1920ED1BED2D3F068941C9C7622691537B59B473;
extern const uint32_t g_rgctx_SyncSet_1_tE85EB7FB3CB5B27AF25EC41670953B775E4507A9;
extern const uint32_t g_rgctx_SortedSet_1_GetEnumerator_m7E08D2146548CE3DA5984B363614D6A7FAC565D8;
extern const uint32_t g_rgctx_SyncVar_1_Equals_m4E3A519CF68BF913C17A3E66CEB07BC4478FB674;
extern const uint32_t g_rgctx_SyncVar_1_tD752B0CEAA283A19E377923C2BEE5E7DB7625B8D;
extern const uint32_t g_rgctx_SyncVar_1_InvokeCallback_m58BD5BDB344BE7B6012D205484D932837EAFA475;
extern const uint32_t g_rgctx_Action_2_tEF31C578116007F6EFFC484DBF34276BA1B12BF4;
extern const uint32_t g_rgctx_Action_2_Invoke_mAE32EC9AD6B0A3E3C470A3E5C24B6F4004735725;
extern const uint32_t g_rgctx_SyncVar_1_get_Value_m0647D2DA89F25CF1AEA6021AFF9A77B03B7DCB20;
extern const uint32_t g_rgctx_SyncVar_1__ctor_m52169D87AFEA0E500725E4867A6381BB9B8DC064;
extern const uint32_t g_rgctx_NetworkWriter_Write_TisT_tC9302E34ECDEDDB943B1E80227CAD3B673F45C27_mDE5007FC1D34D2046578AEA67F1BAA726AEE6573;
extern const uint32_t g_rgctx_NetworkReader_Read_TisT_tC9302E34ECDEDDB943B1E80227CAD3B673F45C27_m30857DEB77C931F4754AAE786CCF6C96EE5C5D3D;
extern const uint32_t g_rgctx_SyncVar_1_set_Value_m8F199B7B77DE2089199122619DED69ED8C547CD6;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_mE105B9EDC94B3CB8609859AF116FB782323E5AA5;
extern const uint32_t g_rgctx_EqualityComparer_1_t0DA6D423D973B0B28A383FA1A8912D8EA2C77834;
extern const uint32_t g_rgctx_EqualityComparer_1_t0DA6D423D973B0B28A383FA1A8912D8EA2C77834;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_mADCEA575E635DA68F52E448711325B63A3D5F113;
extern const uint32_t g_rgctx_T_tC9302E34ECDEDDB943B1E80227CAD3B673F45C27;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_tC9302E34ECDEDDB943B1E80227CAD3B673F45C27_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1_ULongToNetworkBehaviour_m561A663EDC94C1DFCB29A742282ECF4CAC5523C8;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1_tEDDEE78090428574CB02AD42E467607137E5E4B6;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1_NetworkBehaviourToULong_m7BE5AD4C419CEE57776EC4EB15B9B5FA7D4567AE;
extern const uint32_t g_rgctx_Action_2_t0DF03FC689FD55D5FF28BCB45B42F96BFC9BE0CD;
extern const uint32_t g_rgctx_Action_2_Invoke_m0B9CDBADA38B434D4284D87A65E08EA353C72C1E;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1_tEDDEE78090428574CB02AD42E467607137E5E4B6;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1_get_Value_m4F724D74501A78801CE979331397870CD16624E8;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1__ctor_m56D492557339B323EE65D69A09C95D334FEF3184;
extern const uint32_t g_rgctx_T_t51C5F3FDA6F7FA1DA0076ABC9AA41C30B1D4233F;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1_op_Equality_m0B6763DC0255AD105DF9C3EF0478936EFD5E5D0E;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1_op_Equality_m9DDEB369D0F44C9F9B5743564A1148C38BE4F35E;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1_op_Equality_m1B2FDDEF81C56C5F1FFB34DE522AD8E53D38F692;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1_op_Equality_m8C26E6CED07D1C64FEEAD90C9E0667E61562960A;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1_op_Equality_m4212A73A4AD4778F9B9835025892A28BCDB59EFC;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1_Unpack_mBB1F5A4AB649F32ECF428E14FA04C910937DA785;
extern const uint32_t g_rgctx_SyncVarNetworkBehaviour_1_Pack_m53DDCA8A7D5B5224168400575BCF6CDE35CCAE0F;
static const Il2CppRGCTXDefinition s_rgctxValues[377] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t4CA52616DFE66A79AE64E18ED2DEFDED1D06AC08 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_AddRange_mCAB149E67880F7E6C49772EB583F2BD28B516954 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Queue_1_tED902440A083AE00C849EADBD14D5F5DAE12D248 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Queue_1_get_Count_mB00F0E3E6FB74E393CD567125A234A9408C8234F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Queue_1_Dequeue_mD57861CC36B03F129094EB312FB728DE942B2EA7 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t139E2FFCA147116E6CF748BF98F26ED3A9611344 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_GetId_TisT_tD993192AF87795F9BA5433E2B10AA2C3B4C00DD3_m214184541FE39CF8FBFDDF4DD385E68D3DA452B6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkWriter_Write_TisT_tD993192AF87795F9BA5433E2B10AA2C3B4C00DD3_m694E34068ED34009C994A7580E91E796F3BD755F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass6_0_2_tADD677C365101662255983047576ECE0268C1312 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass6_0_2__ctor_mD783306844A3DAC78CE97842E4D9BE3B5B6EABF7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass6_0_2_U3CWrapHandlerU3Eb__0_mD4EB5732E784E10616632C15886DC522962E274A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass7_0_2_tEA91EC9A1646A09BCD2513BE6301ECC533945531 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass7_0_2__ctor_mFB29C39E66ABF923837913475E4B0439350ABC77 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass7_0_2_U3CWrapHandlerU3Eg__WrappedU7C0_m2CFE9CED289E0325F5190E303769FFF6DD60F36C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_3_t8E00C012E66E048E9A45E2AD154FD57DC2F4621A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_3__ctor_m05B01B08C036872A57D3B84FFA827971180E1F92 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_WrapHandler_TisT_t992A357F26FC11442F70E1DCBF47243F7BB60F55_TisC_t8CEA10D551CC20AD4EB8499C2F3119D329F9A991_m3941EB643DE753F111D1697D9FA0201C50A60C20 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tB06C3C539FACC7058FC27BECA7D4CCD87E985E8E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkReader_Read_TisT_tB06C3C539FACC7058FC27BECA7D4CCD87E985E8E_m3435199B6851481C9CF66BA7511D3C1A6BF5BED1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiagnostics_OnReceive_TisT_tB06C3C539FACC7058FC27BECA7D4CCD87E985E8E_m66B6B007FB13DC105A622540F408947BE7D62BD7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_C_tC912A05CEA1C58E52F8332328BF453410E19C273 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_3_tC5E8D80068D66ECDB8B18C976EDAD16F65F6123F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_3_Invoke_m04BDE07043EBE830B1267C2DC90C5F5F7FBE915B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_tC68F49354E98A890E2B426546A36096D3B61E7C9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_mD5B895D9156FBCC15499FF2EA84D40091C361578 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkBehaviour_SyncVarEqual_TisT_t0FC425C7561A7D79480FB394A57F27D090966EFF_m3A20BA13AF29F0D9750F7959E632F23DC7CE64B1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkBehaviour_SetSyncVar_TisT_t0FC425C7561A7D79480FB394A57F27D090966EFF_mB66CE7E55BA58EA23FE218C4C2922A0EA6D1359F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_t12D11ECE8A8CA086CDBF7B653214EE0E018A247D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_m1D2A87F32F5DF69E8917B501E72E7617CA6B3CBD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkBehaviour_SyncVarNetworkBehaviourEqual_TisT_tD78A1FB7F75A4C347B29FF3DD090278E0EBF9EF6_m546BF5FB26588F444DA5CF6446007F2AB81E2E04 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkBehaviour_SetSyncVarNetworkBehaviour_TisT_tD78A1FB7F75A4C347B29FF3DD090278E0EBF9EF6_mA6D3AAA1F0533E328FB79EE328BD97321AE78D8D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_t16DF40D957C7EEE7B116F2EA9B2B083A7BECAEE5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_m19B27065ECD595C48C169A6CBCFB95DD38D919B1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkBehaviour_SyncVarEqual_TisT_t4B168B60FD63772E970D7EC343A89ED7337DE4F6_m6DACEAA0963839EDF74AB2CE2C86880C8F3F260B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_tFF1BF7B0A56D83B0F39CDF6D53CFE5B714640B24 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_m056D08A018620979E7864DD5F9BAAFEB97436AD1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkBehaviour_GetSyncVarNetworkBehaviour_TisT_t6C90390DF8C3C92D7976A3BCF11C11923C9D844D_m06A0EEA1756893B7FDC86130624F8F6D0607FAB4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_t23A2E1BA6F695D77C98EC81D4233E8959F765D12 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_mA712644655DFD138FB51860406DED075CEF624D1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t725D06876D0D32B8F55B46E5F3F3D500590A67B8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tB887D8DB3A3F05B3BB96F9030712CCFE36A14BA1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t81F157C97222CC86748828CFAC34DC255E809E02 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_mFD1BEA3D52A3F1A1D630C810215B707FD0900127 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t63338B2EC676F0F05086C85E624FC67A119FB702 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t63338B2EC676F0F05086C85E624FC67A119FB702 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_mC257157FA4DC168A1F8C474AA8AC016BF61E5356 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkConnection_Send_TisT_t751499DA430BCAA6D3D45679122C8184CBE865C7_m0FF85357206C9A6EB2B71DE4A4197E2E84935465 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass47_0_1_tDDB045A069276E76A895EE36CFD0E48526668D52 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass47_0_1__ctor_m5C2091DD273467D27E241A35FD8ED2DD373DD2B4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_GetId_TisT_tE6D3EBBBF4D42020D3C8CE8677D8C6D3512DC5F0_m11BD2093EC33763666600A78D7314DB59A1B8F41 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tE6D3EBBBF4D42020D3C8CE8677D8C6D3512DC5F0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass47_0_1_U3CRegisterHandlerU3Eg__HandlerWrappedU7C0_m4DB7B00650148BCA662F6EF7D68DEFA48687E21F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_t739E7721748F3BFEC10F86DAA0AADDEE5E9D4BCE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2__ctor_mCC007523022354BAC79A019FBC58AD5CF984DC37 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_WrapHandler_TisT_tE6D3EBBBF4D42020D3C8CE8677D8C6D3512DC5F0_TisNetworkConnection_t49880296B0FA972023F34582D7A41D7B63383E78_m7D0631DE3288C0F521C248382B4FE8ED243D1629 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_GetId_TisT_t0FD9CF7B61A57CB8D4829EFB4BB189C9EF191436_m5D4D2F436CF76934EC36E52B06B10D700DFCE0A9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_WrapHandler_TisT_t0FD9CF7B61A57CB8D4829EFB4BB189C9EF191436_TisNetworkConnection_t49880296B0FA972023F34582D7A41D7B63383E78_m3C300EC8A6C8FFA0D3A2F212BB0B5BD63AB8DB06 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass49_0_1_tBEFBF8E2E4AD20296AC44FE153A516AEBEDD71FD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass49_0_1__ctor_m587FF032616DA74790714FCC437BE42437B32749 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass49_0_1_U3CReplaceHandlerU3Eb__0_mB6161603898969F033DD69DBC649A7468903197A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_tB55CB934B7A94948D7CEBFF6202825AD5208C7A2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2__ctor_m4EA3C9D5D7401D33CBD03DDEB61FDDE051A0B680 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkClient_ReplaceHandler_TisT_t61617DEA6A58347D20E7298F4BBFE2DF49A53B0A_m9A9EB8865654C48FC1D6C8F761254A526B2D5B19 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_GetId_TisT_tF2F8426C5ACDC965F8FA1692FFBAEE84EEE86C21_m22EBA7763D869D47BE13B1EFDE2118CC16DFBC35 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t8936590B41FBAC422CC9312A2B3352E5FD2C70F0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_mC4323688AC6DED649E7930A83958C03074CC4131 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t28A9E13798DA254B2052F7D0CAB55AD4AE2DA55F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m61D8B5F87FC0AF1514AA71DEED084757903A4869 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_Pack_TisT_t3E4807A7ED5A5B7D2D9379E5FE928C5F7969C9E8_mA827C8DA0B04B9B62EB54D3805AEEC600FDBD726 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiagnostics_OnSend_TisT_t3E4807A7ED5A5B7D2D9379E5FE928C5F7969C9E8_m69022AD3E4CA3053A2E704D03583FB8B85534B06 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t00E0F792EFBBB7CEB35858347DEB6F4F78EE84AC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t9EBE1E3BF279F72BFABFE776C07EE27175DB179B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tDF6FA0132A0DAF62F1062D93307A7A7CB8BF41CC },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tDF6FA0132A0DAF62F1062D93307A7A7CB8BF41CC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkReader_ReadBlittable_TisT_t1CD5BEBCA831A04806D9C686E19FA8A88022EDD2_m8CE5FFFE9824C81792739EED460709055796A301 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Nullable_1_tED1B6437BDA108A568BA016B66B69A206AF6D7A9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Nullable_1__ctor_m694DD22142018DBBFC38F5F868A52745373197D3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Reader_1_t7C061865EF93A94AFC3AD12A9DB49AF0F904A05A },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t7B529C29664E0228E6271C873F4987647FBBC3C2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t84CD5734738E60A9DD2B70B270D28F83DB66BA79 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_mE1113A9BE951A31EC8C52A72AD594E9A7EA3B655 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t1F9CCFC4150283498C64C236C3DB7FD7E67FD775 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t6A47AA30C555AF25EB2CF8638D641534C1DD6177 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mE4DFB3808BB6BE8C86B5C13EA84608AAF85C5D98 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkReader_Read_TisT_tD5AE035A1E6217C76CC767E1727D6C5E4A018AC4_m0F35CAED283D70F94B470A07F81DFD93812D3FDE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_mBBCC509A4B4A5204A87D7B2C1EC7E84F9D19ACD8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_t496DFB72EF11B2A89E296E87E8F1FFF8F1F626CB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkReader_Read_TisT_tC3618DE086915E6BB25FA6D90192B0B2DDD23B5F_mC8C8630493CF3FC406B3B40AD24183E4647D7FA3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_Pack_TisT_t7CD2EC9C62A1AD97F6FD8DA162FC91C04E2DB65B_mE7531320CB7DCD359E448A8A3BBB11DBF8BCAF1B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiagnostics_OnSend_TisT_t7CD2EC9C62A1AD97F6FD8DA162FC91C04E2DB65B_mFB70BDF16326A34451A082B13F41FBF44213AF5D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkServer_SendToAll_TisT_t5FF8A8EAC7FDAD40D9293234906C66BE00EA744D_m49F1E4B1FF418B3D9076B7467AA458455FDCBEEE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_Pack_TisT_t01C7B96EBBDE6AC2D090BBC248548149F724D458_m78D7D9E5484BA8E6EFA1A9C3C43253AD3DF49F6E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiagnostics_OnSend_TisT_t01C7B96EBBDE6AC2D090BBC248548149F724D458_m012E887888DF6EB1595C732DE3DB1B3DD27F27A3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_Pack_TisT_t91F0CC010FB9DEFEA6763E3B991BEDDD4F99F17F_mC5D14A60B7FC15DA6D71B01F1E059D2B7A37CD23 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkDiagnostics_OnSend_TisT_t91F0CC010FB9DEFEA6763E3B991BEDDD4F99F17F_m78D613A2830660559C5335D74C44A2D86338C191 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkServer_SendToReadyObservers_TisT_t6B75A0BFA1E98EB2B28AFC7CF2C2B0785E240E6B_mEDEB0D25D31A21049FC91DD84F7C5A5CA0C2B2B2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkServer_SendToReadyObservers_TisT_tD9129F7563C97C96A3D505545119AB4E57E64E42_mB0413FFD8A2CDAA857F9D12C0876A231C436712B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkServer_SendToReadyObservers_TisT_tD3652448C0A0BEF945E00871DA9441A7F397D2EC_mAD9DC2CD43E7EF8E353BCD77A037CB5182EE4CB6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_GetId_TisT_tE7D431B3102F446D782816319519B7584447E4BA_mCE8A5C459208FA1F1C3C3018D0B793BB16DAD619 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tE7D431B3102F446D782816319519B7584447E4BA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_WrapHandler_TisT_tE7D431B3102F446D782816319519B7584447E4BA_TisNetworkConnectionToClient_t80F9FBDD786601CB93A63585D05BCAA1050C406A_m68C33C7A0126CEC8E8C9E5BB6876D45947C8B217 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_GetId_TisT_t25912D0832FFFE8C56A0018C8066528334489777_m14E0E817279617225CE25EBCAEBA85EC1442F82D },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t25912D0832FFFE8C56A0018C8066528334489777 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_WrapHandler_TisT_t25912D0832FFFE8C56A0018C8066528334489777_TisNetworkConnectionToClient_t80F9FBDD786601CB93A63585D05BCAA1050C406A_m2ACA41504DCD7895A3635515F5448AE99AAFBBAB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_GetId_TisT_t8B2A57F699BB1E21D1B75FAEDB781A6AEA4E8764_m28745DDF1309F0DED828E7E1773A83C35CF6C85C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_WrapHandler_TisT_t8B2A57F699BB1E21D1B75FAEDB781A6AEA4E8764_TisNetworkConnectionToClient_t80F9FBDD786601CB93A63585D05BCAA1050C406A_mCCC845510B433329ADD19A65F1BF61DEFF72264A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_t1483A0B6F1CB637FBEE79DC6E144DFFB46EC9C6D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1__ctor_m8CE3DD2B7277F600712064DCB3F6764C4AE5F7B3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass51_0_1_U3CReplaceHandlerU3Eb__0_m50FEE2C5133D1B7DED20E8FECF72F31DDCE39226 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_t2A8DC00416466F11084CAED8728A8B97C94349F0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2__ctor_m82D1B9C817560E27A556474F3B1D69C4343A7B60 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkServer_ReplaceHandler_TisT_t4B186920175327428B661168FC0851B1058F13BE_m328A98B02D0563A28EA383DA3F3DB3705228FCCF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MessagePacking_GetId_TisT_t5AE160A4F8AF9ADBA0C1FA9E9AA83892ACE0CB14_m3466B6676CC857E2D91A167D1E905234E4BB8B9E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_tCFC5FA47B86CD5941A824DB382D423097A4C5A6D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m5E07A731F22FE5B88190EAE05D1CB4502C13675A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t1474E8BC1A3D6DD02EB7B843DAFFDC10268BB884 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Nullable_1_get_HasValue_m2D65FF808D0C72EC1E1B242BE2811CE724535E80 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Nullable_1_get_Value_mDF5A97CC0ED0040355013FA1EFB15DAF7BC64AFD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkWriter_WriteBlittable_TisT_t4B6C9ED0CB1F5476267FED3F41849EB6A432E817_mF1A74AE5E716D49DC93FC0F2A7F74B826C47EDFE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Writer_1_t4FC723C83C9530FABDFFE5BF7993E398C738A848 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t0C38038C1399EF21FD04A08E1FCD09EBD0C7DED1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_t7F35554DFCA69963A4B9CE151051D59FD537ABB3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_mCD65377EF0978BCF302AC268C958EF1F09EB71BD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ArraySegment_1_get_Count_m6DCE9944A1DAA2674CEF74FA0CA947CCE2BD9244 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ArraySegment_1_get_Array_m684FFD03E76DA63DC60C749F591729FB88937D8A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ArraySegment_1_get_Offset_mECAB0D45215538EB5B33248C4DC54FFE2E54A265 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkWriter_Write_TisT_t7FAEFCAFCEADAF391B8327AF09C8443C27538F98_m56862B533175C98CD53888DEC837D44669888F68 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t894F0E88EE44835A3895B1D80DC1BB3EC00EB7D0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m1BBB6A544D8908ABEC587BF5EF69EA888A3C2B09 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_m20FDDB9BF6C41A70C89C0BCC67A9A5F7001BAC0D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkWriter_Write_TisT_t66B8EEC92D05245F9C899E27DD855A719E932D90_m618E399545056F28773351F268A6940194CD8D52 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkWriter_Write_TisT_t901E69ACDF2AA0BB82ECB44F41C09E2855E2E9BF_m48C1134EB2AD8E0D1E346F8F83979F7C7E993C11 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Stack_1_tE41867904A4BD9B9AD9F9D7DB12D1E33B179AE92 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1__ctor_m4D5F53F90ACD6774ACB49DACCE8ED069D0B66197 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t99F9FD808F6F6B9EFCEB8ADAAE5153CBDEE087DF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_mCAF774BB0296060606AAB9EF8A91BC4EA7EE6CFC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_Push_m2CC3848E065F7900E657FB882750C21C37FFC09C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Pool_1_Get_m38E0B03A58B9C071B059958EF2AD0E640B3953F2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_get_Count_m6DDA2F53B317B6000A1457FE744509D93222954C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_Pop_mFFFCD1C6F63A53FBA15248EE2FB5784FFA707221 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t89528DA5AA7EEA0062FD8D0EE7E86949BEAFEBB9 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_t89528DA5AA7EEA0062FD8D0EE7E86949BEAFEBB9_Snapshot_get_remoteTimestamp_m0527AF79D8BECBCC0ED78DE5CF959E3CA13A68F1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedList_2_t8726DEF09A8522DB60D10ED00E0F72939986448F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_get_Count_m2CEFEEF6CD800BAC0B7F7540BDAAEDBE99816060 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_get_Values_m99499492796D9218BB9E7CABEA26F6981114758C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IList_1_t0993FF494379228DEAA664AA99FA495109F16294 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IList_1_get_Item_m773A3DC0BD74D0F969819F56EB30CCB52D99269E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_ContainsKey_m54C418782B97E53D2A814D4916101FCC712533E7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_Add_m2B59F8AAF324B909439D3A1C74CEDAEA6168327A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedList_2_tB587957E14014BCFAF69C93124442411EE097962 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_get_Count_mC9BDD3B925DC39CFF78497B62594AD81EBBBCB03 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_get_Values_mDDD9DBEF35EF3207000D6E16F38AABF4C11C0904 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IList_1_t59CD2D14FCD76B158B9FD7B73BC04255FAAB0035 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IList_1_get_Item_mDA1683CD9ED4CE20406F39A813919C06588812C3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tA1E2C962DC5AB3B076A2B3B434E37468C4E0784D },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_tA1E2C962DC5AB3B076A2B3B434E37468C4E0784D_Snapshot_get_localTimestamp_m297E9654A2BE6D19A72C09500B81AF147167617E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SnapshotInterpolation_HasAmountOlderThan_TisT_t294A9678E9F39BF583F821F1F80CC8E3FC96EB86_m28F6DEB0AA5D79D69E8E000864042447C823149A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SnapshotInterpolation_HasAmountOlderThan_TisT_tF69855E1E692FF54C69A1373BE8BF3533019711D_m176D41CA10DD7288A8C70863C775F0EC532345C0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedList_2_t3186D968F22573D8A79EAEE04A3A677136FAEB7A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_get_Count_m21520658084EC20E636312DA9687ECC0DC347011 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedList_2_t38403A078EDCDA5E39767C70FD861DB07021EB88 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_get_Values_m808D6AB02812F273A3D2BE18A226D9FC85D93758 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IList_1_tE31C108874BCF50AE5078833C477538B1D04AF81 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IList_1_get_Item_m4FD0A3FA52F4518F0D11170CCCDADF40CCC32986 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tE7866AC3F882BBCDBFB17F87C95F35A6667763E7 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_tE7866AC3F882BBCDBFB17F87C95F35A6667763E7_Snapshot_get_remoteTimestamp_m0527AF79D8BECBCC0ED78DE5CF959E3CA13A68F1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SnapshotInterpolation_HasEnough_TisT_t7000DAC998F9F689E7EB30E3F9DB59CBAF33E0FB_m84641562AD7FD13CF41B71B5A84DA8D5B7CDFDE3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SnapshotInterpolation_CalculateCatchup_TisT_t7000DAC998F9F689E7EB30E3F9DB59CBAF33E0FB_m1A2A715B4A00B3296C5FA952AA415B96F620EF80 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SnapshotInterpolation_GetFirstSecondAndDelta_TisT_t7000DAC998F9F689E7EB30E3F9DB59CBAF33E0FB_m051C810B4FDA17BCA52A7D13027ADC913DD35939 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedList_2_tDF34A61256DC9FCB808105E2C7BF84FB6198D6F3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedList_2_RemoveAt_mFAA1E454747D63B21DC460E90D41DEF1EBDC226E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SnapshotInterpolation_HasEnoughWithoutFirst_TisT_t7000DAC998F9F689E7EB30E3F9DB59CBAF33E0FB_m9C2C7E7DB29F42E50F653D39B4B41464599FE8C8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t7000DAC998F9F689E7EB30E3F9DB59CBAF33E0FB },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_t7000DAC998F9F689E7EB30E3F9DB59CBAF33E0FB_Snapshot_get_remoteTimestamp_m0527AF79D8BECBCC0ED78DE5CF959E3CA13A68F1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_4_t9BF327645D974653FBC1AF101BF2D8B05E8880B2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_4_Invoke_m3A3F664AC42AFE8E844D81C6F0EC06A3EBA4CC83 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ICollection_1_t8B20EE12C1B93DB25DA6AEC5AFDFEAE76230528E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_get_Count_mFA85478E0D60FA0997C1587DBB620997076542FA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SyncDictionaryChanged_tED96A0B6B1CDA35728415B07893EB74513AD7571 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncIDictionary_2_set_IsReadOnly_mA87550DA7C2E1357432CF3095356F159EE5D7FB1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tE4BC89115BE946EAD0A028C07FF5717C0CCA051A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Clear_mBF3E04999DB852556A74B4C54370D7B49B2CEB82 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_Clear_m5EA7F97512D657210E70B27A200B63D7C388547A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IDictionary_2_t7B061B92288B8CC6CC49EAB0D87B562F070C312A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_get_Keys_m124811A3449C391BD72F761912CBDA15E17D6D8B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_get_Values_m465C38DF45FFE24B27EE11102A7128DF9E264C70 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mF34EE1E0AA6398F1205D9B78F9DC073430219F99 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncIDictionary_2_get_IsReadOnly_mF59318B25739E6DDCB916894C4ADD81004AA76AC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_mE44D4B55B3789411E06F74CA6D9E0FFBD348469C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncDictionaryChanged_Invoke_mE9945090EF6D1F9C3383664630E6359E7D560EB8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_t28F2C7D4D508C5CA96EABE29304789D95B5C4A11 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerable_1_GetEnumerator_m1A5ECD7541DA526D96781846FB9E311C603FE9C0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_tE55705B3E2F022DDAA83183612595EF0156D337F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m11F923DD1FC14F0B7B1266A1800DFE193AEB5890 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Key_m0B91A1C1CD9A3DF740D2B31645FC1DBEA29A9CD3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkWriter_Write_TisTKey_tAD9F9054FC32C5D15971CF2C8B1B95E8C3C3EF08_m9EAB8A3B3AB542EF3CD422BAD4FC10D586370FAB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Value_m3181856732170751C1595A95E9D2ED4B53E4BCAE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkWriter_Write_TisTValue_tCF3048D07B8CF615D3DE43668A25538C440B9721_mC1AD9308BF42AEDE75D16D2ACF8E171D88B78523 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m0594E2A359407D80E21CE8F5D78F56C7776C3952 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_mF3231EADCBA89FE15E3249D0B6F5F542C5D5AE59 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkReader_Read_TisTKey_tAD9F9054FC32C5D15971CF2C8B1B95E8C3C3EF08_mF5AC515AEC01BB2CC5422ECA18973D4706A5E1A1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkReader_Read_TisTValue_tCF3048D07B8CF615D3DE43668A25538C440B9721_m053468B60AB6B44DDC15BED189B07630EF77AF4F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_Add_m8B9CD4E9952F7679BB4738B92BC3E525F5F2C8DA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_set_Item_mE68FAB37ECD6426B8972D992D7CA401A49AB7BF1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_Remove_m7B229A5996D91CF9194598879FFDC0B22D2F21E4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncIDictionary_2_AddOperation_m4C5E4D2AAA4C518497DEEE7C5ECA8FCD957496A7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_ContainsKey_m42EC3DF9DDB51C43226D8A76E794715A6EFEB071 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_TryGetValue_mA74A43A4936AC069517AA517EA28B15B9555ED0D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_get_Item_m576B27E3F6AE9A0BA085C43A3423FDFB635D602F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncIDictionary_2_ContainsKey_m99561BFE9CDA02A703EA2E4F3CB267BEDC29568B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncIDictionary_2_Add_m6DF39E9C6E3531DBAD8CDA72D4176B41956AF72D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncIDictionary_2_TryGetValue_mAEE7B2C1D9B793C012FF9EF1C4EC827437B012DC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_mFB4ACB2FCA8D788D430A6EBE2D60DD57E5B7B559 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t51BE012734ED54BB143E320B1E3D77734DE5AF50 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t51BE012734ED54BB143E320B1E3D77734DE5AF50 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m6B560F9A0B1B14C77837B651B8EA8BCFBD17BA5A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncIDictionary_2_get_Count_m8E3EE8F737806147D63FC6C3619963D7B8863C8E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_tCBCE1356C517DAB1A630E9193FBD27ADFDA63340 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2__ctor_mBF272A1F9BC74EB6134C58F20BAD788BAF47EB68 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncIDictionary_2__ctor_m5AC12BEFE5090C47353042F233C342428EC6DF96 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SyncIDictionary_2_tC1FC455805D09111936D8D560F7F457947DDEE6C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2__ctor_mC6C6B6ABF2A4F5E123191AEF9E0BED7037B53D26 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2__ctor_m9B924957A4CA46FFBEF6ED904F56926990EDFC84 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_get_Values_m5B3A92DBB61E6A0EA017B8C122533856AB1AFF12 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_get_Keys_mEF5A4E3ACCACD2A5B18ED7B9C4E031625CBC9904 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_GetEnumerator_m7B526D9DAC4ED78CA2BC31FE1E715C3DDF5692BE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ICollection_1_t1A87B3B16F46C0F31DA1EEC90585E1AE5828CF45 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_get_Count_mD12961EFAE0A16D7AE327D6BFA5F6AB2D7AA8FDE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SyncListChanged_t68F3612B6BCA7C4B6EE16D9CBA68F343AD8E1FAB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_m179CCC5C325E588E195BBC0FC9839922C471BD4E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t71BB6716BFBD9404DD4D157B5A4739681DF4A798 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncList_1__ctor_m49B74E63A7B8D7582ED6EAE7D3CFEF9BB96BC468 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tF31653A4543288F92927F24626567C642ACAB086 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mDBBEB85C0F922B32F4BC6E28224F7062728421EF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tA7030C63CE1885AA91EA066335F14B7F09F0C54D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mA8D3D32B098981208C6DF25B3BBEF6C96D08C011 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Clear_mCB63A1FC250B09FFF27524FDD74E58D315D0457D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncList_1_set_IsReadOnly_m0D6B500FE04895F6D671EFC433CEB60E38060B6F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_Clear_m38CC4C9A099D6F2B4B928D2C1DBF3B6DA425F01D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncList_1_get_IsReadOnly_m0850F414A948523C68F425649448F70E132EE733 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_mA620B6FB6C21BE99BE5726AEA68FE1B0ED6677DF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncListChanged_Invoke_m02948903A940B9D4924858BF8EABDF2BBB135F5E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IList_1_t124C48A37312A8F8F3F3D9F10F74B5F0FF611EA9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IList_1_get_Item_mDCDDEED97123871B8FC41070396B269D69C9EA1C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkWriter_Write_TisT_tA3C92BC449FE896282C9547B2A522013F31854A6_m327B80BADD2EAA1388E43657F8A6F10A7383535D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m2F133DBB94E4EBFCE522DACD395B861A7089DA8A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_mE7ABC2C93478C194BDF7E82960C568FB401E79B6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkReader_Read_TisT_tA3C92BC449FE896282C9547B2A522013F31854A6_m463C44B4B3917F1DE8A08ADD5A1FC0374F3B2427 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_Add_m4DC9D2CCEED77DDD4FA541021DA81DDB9E0CC70E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IList_1_Insert_m387ABF4ADA60FF59EE06B212FB9AD50AE2EBB12B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IList_1_RemoveAt_m6D4427AA33A91A6DAD57BBD09EE9CA1ABAE4FEC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IList_1_set_Item_mE5998087F8C26940BE5674FF4308C5935F47F725 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncList_1_AddOperation_m41C01C36907674D255D83A1EA83273D16EBD0A18 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_tD2A0775CFB034B23DC5017071CABB4C5CEA465F9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerable_1_GetEnumerator_m7A29DAECD0C84F14709B1C55B858BA3858B1DE47 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t31833133BBDEC7A83A069355BB14B0254057793F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m3002EA7067110E5EA52263D16B0B33A3282D4B67 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncList_1_Add_mC0F138AE99A0B29A84E717946811FC19A804FC15 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncList_1_IndexOf_m6D15C126B5A5D88A5B71746032BA100D8AE09501 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_CopyTo_mC431F4E294A5D87586AF35AB04F85D3125D8DA9C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEqualityComparer_1_t7F71CB094D169E43C61B44C32EB1431AF59091F0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEqualityComparer_1_Equals_m4E0CC461D19E9FA17A42C90A5C36723D402F7353 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Predicate_1_tAF28D2C4874540F3DDAB2B888E508B0B3A69C016 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Predicate_1_Invoke_mBA0251F4F4EDF82378E9687CE1FF0C7C2B7B96CB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncList_1_FindIndex_mCDE85258011536F8B60983275F780CCEA157363E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_m845FAD005CEBB8856D8EF501DF6CB379E7C94C58 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncList_1_Insert_m106CFB9A6974C3312AF62FE7D85BFD1F30E3756E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncList_1_RemoveAt_m012E35A398E22EF8E3256F94B0FDA648A6C67C9B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_GetEnumerator_mD6EEDA07A83493D212CDEAEFA25C0EA7FE80C8D6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_m8D3C62EED65871DF3059FB30E87FDD4A1B14090E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncList_1_Remove_m9D10CE3B1E70544DC9285CCFA5096F32A23E940B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m84DCFF287C0FADBA94320B80C5A394225999ECF0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t426E049849334D0037E1AE6AA8493E2B55F160A6 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_t426E049849334D0037E1AE6AA8493E2B55F160A6_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_mFC6406FF44895409FA04DE67BC1F66ABA4FB7513 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t17E12781F75C3235F022AFBA1081D7F57C6D3C97 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator__ctor_mF74B750E64033549CD4D904E50A2D046C8A1A437 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_set_Current_mF18D3C72C5C8D8D421E610B85F014E9FF832F899 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SyncList_1_t7279AF626D1A3CD625FB0ABEDAFCF8CC4F433E49 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncList_1_get_Count_m4FA71676637044EC010A8A6935BAB2D686B5D92C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncList_1_get_Item_mFA19DF0CF2C29727C0BD3CAB7D818A251FFA5D58 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_mB086E7C3F9D232DCBD2E47B2BCD5C0C3FCC1F5F4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t4835924C45A5161E8B294F4CBE509C72C0A26889 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ICollection_1_tE89C0BEA80162FB48A74AB195985FAFCD2259338 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_get_Count_m593FD9351E224DFF2C35EDE969C4F174FE41BB50 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SyncSetChanged_t4C348DC44724F96D1D750B07F9E00E1B78D6CEF7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t96BFD07477D5F7C11580BFCB4ACE9DE3EB9E4599 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_m345233E8698434BAF539A18FAD2C31C648653F23 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSet_1_set_IsReadOnly_mB33D6514B528A1C80BDACB6878E0771BF969638C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Clear_m095D8D83AC1E8A0CA376CB37CD26FABD4C0AE56E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_Clear_mCB6E77FE90FD125625C8B0E3B6CEC80160C70B14 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSet_1_get_IsReadOnly_m24958F98ABF53BA531E4FA321C4E7F751342A500 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_mB1D189C43660F7996273789FB72D313CA9E81709 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSetChanged_Invoke_m57284E4E9F8309D98F94318E3E043C255DE6A109 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSet_1_AddOperation_m61F65CFFE22814B8B509BE1CAFA945C801698ABE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_tA9CCF72BF11D17764D2F188D37F86B1679752D2E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerable_1_GetEnumerator_m922D58C38CF48D24AD9B2550AA0EED9024790370 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t5DBA2302BB200F2B13268E63716BA2B9EE7DD8C2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_mD10A1D2986136754DAA533D4218D2D2D362E1B13 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkWriter_Write_TisT_t871D28ED157FDD035D66411B650D93EBCF0663E5_m19D2B3ACA9AB8A082CDD55A81E9629CEC42FAEF8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m4DFC4CFC501467DF325B5139B04C68B9E973BA45 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_m13A6B93AA344EB1C646062AB66A69155D41B217C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkReader_Read_TisT_t871D28ED157FDD035D66411B650D93EBCF0663E5_m79F74BCBF8CC4BEE0AF774294D04435B51A16E4D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ISet_1_tA2115719BF049635CF3C9008F87E8A0D4A02A7F7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ISet_1_Add_mA5044DDFA40AC4D5C619E8347F075626FE18FCE3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_Remove_m695BA01620751D2FBEB16E15AAD5FDB767948199 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSet_1_AddOperation_m117E5FA8F2DB33B66012FC155E1A423EDE38AD9C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_Contains_mD1D03DD5C9D97E1A0564A52356728BE7F79B27E9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_CopyTo_m53481449415F609558CB9F6262D09883410EAD22 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSet_1_GetEnumerator_m87943530388CF1FD37168FBB54DF96623ADFEEEC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSet_1_Clear_mE335789C54EDBEEDD78C43D78AD4C33FDBEE110C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSet_1_Remove_m6E50E7FF4AA91DD2E05582C0C721D0A1E4E43F32 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSet_1_IntersectWithSet_mC2B8D63B29963B1567B11D6A9F4D1422739F1EB8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_HashSet_1_t4D392AF75C81099D0F904E47166CD88355F6313A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HashSet_1__ctor_m53541564B69F712E08FAFAB9BF88906D99548509 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t4758659D07234AAF31E8B051493D4513796EB905 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mEC97FEA0C8FC02CF25787C97B964C83595511FA7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_GetEnumerator_m49CE0AF7BB8DC2E5C24725858D781F6489622CC2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_mAFF6074687538FBDFF9C58CD215FF95FB2B0523A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m0B51D9A3738FF739BDD29FF813628CC208136DAB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t9DAF482D8206366B9EA16C04930FF3659AFFA953 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_t9DAF482D8206366B9EA16C04930FF3659AFFA953_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ISet_1_IsProperSubsetOf_m9169483187C1293F2675FE78ED751E389C1B62E3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ISet_1_IsProperSupersetOf_m3D8254C9E4F446050118162801D0EA7018C07C6F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ISet_1_IsSubsetOf_m2ED6F7417BEEBF10B210999BA6754EEF4E519560 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ISet_1_IsSupersetOf_m8CE58AB1BB5F4685F61860C85020701C917A4173 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ISet_1_Overlaps_m02E1DD4B13CC9F714ADA620E7A396776ED59E876 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ISet_1_SetEquals_mA641DEF01686A78712A743389D26640757F0CCFE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSet_1_Add_mE565C7345A1B79B308C863C728B201F1ED8F927E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_m5C7562C58A913CA11FE8FD79BCDB0E848D216A24 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tC991FBF1A784E528D63C9330A38BBB19EE4CB015 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncHashSet_1__ctor_m117C1D65525A35811AFD50405CA9F0A3902B225B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_HashSet_1_t80CEA056BD275FC5A091EF3C9545E0C6C3F18B06 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HashSet_1__ctor_mAA4B80BA49CF68DCDD1C8F46DD02807364366237 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSet_1__ctor_m6B1B96249878AFD48946BCCD3F3611BC6CC17669 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SyncSet_1_t0AF185D3B61C3E6360E19EF504916ABA87975535 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HashSet_1_GetEnumerator_mD42F43AA5CDBD10AB7F9FE4A5253B30531FF9430 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Comparer_1_get_Default_mF1BD84689F815FB0C606C551B6542DB3048BD625 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Comparer_1_t9077364B39202C626E6BDE2E7315A27000C996F2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSortedSet_1__ctor_mDA68FEEEC46BEF0D025477B6C080401F2B5C49B9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SortedSet_1_t1ADEA36D2C0EE0A41D5238C702C86BFDB39A7AB4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1__ctor_mCE3E5234C2D16BC64E7BFE13E45094F93F2E658F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncSet_1__ctor_m1920ED1BED2D3F068941C9C7622691537B59B473 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SyncSet_1_tE85EB7FB3CB5B27AF25EC41670953B775E4507A9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SortedSet_1_GetEnumerator_m7E08D2146548CE3DA5984B363614D6A7FAC565D8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVar_1_Equals_m4E3A519CF68BF913C17A3E66CEB07BC4478FB674 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SyncVar_1_tD752B0CEAA283A19E377923C2BEE5E7DB7625B8D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVar_1_InvokeCallback_m58BD5BDB344BE7B6012D205484D932837EAFA475 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_tEF31C578116007F6EFFC484DBF34276BA1B12BF4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_mAE32EC9AD6B0A3E3C470A3E5C24B6F4004735725 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVar_1_get_Value_m0647D2DA89F25CF1AEA6021AFF9A77B03B7DCB20 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVar_1__ctor_m52169D87AFEA0E500725E4867A6381BB9B8DC064 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkWriter_Write_TisT_tC9302E34ECDEDDB943B1E80227CAD3B673F45C27_mDE5007FC1D34D2046578AEA67F1BAA726AEE6573 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NetworkReader_Read_TisT_tC9302E34ECDEDDB943B1E80227CAD3B673F45C27_m30857DEB77C931F4754AAE786CCF6C96EE5C5D3D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVar_1_set_Value_m8F199B7B77DE2089199122619DED69ED8C547CD6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_mE105B9EDC94B3CB8609859AF116FB782323E5AA5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t0DA6D423D973B0B28A383FA1A8912D8EA2C77834 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t0DA6D423D973B0B28A383FA1A8912D8EA2C77834 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_mADCEA575E635DA68F52E448711325B63A3D5F113 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tC9302E34ECDEDDB943B1E80227CAD3B673F45C27 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_tC9302E34ECDEDDB943B1E80227CAD3B673F45C27_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1_ULongToNetworkBehaviour_m561A663EDC94C1DFCB29A742282ECF4CAC5523C8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1_tEDDEE78090428574CB02AD42E467607137E5E4B6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1_NetworkBehaviourToULong_m7BE5AD4C419CEE57776EC4EB15B9B5FA7D4567AE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_t0DF03FC689FD55D5FF28BCB45B42F96BFC9BE0CD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_m0B9CDBADA38B434D4284D87A65E08EA353C72C1E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1_tEDDEE78090428574CB02AD42E467607137E5E4B6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1_get_Value_m4F724D74501A78801CE979331397870CD16624E8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1__ctor_m56D492557339B323EE65D69A09C95D334FEF3184 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t51C5F3FDA6F7FA1DA0076ABC9AA41C30B1D4233F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1_op_Equality_m0B6763DC0255AD105DF9C3EF0478936EFD5E5D0E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1_op_Equality_m9DDEB369D0F44C9F9B5743564A1148C38BE4F35E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1_op_Equality_m1B2FDDEF81C56C5F1FFB34DE522AD8E53D38F692 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1_op_Equality_m8C26E6CED07D1C64FEEAD90C9E0667E61562960A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1_op_Equality_m4212A73A4AD4778F9B9835025892A28BCDB59EFC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1_Unpack_mBB1F5A4AB649F32ECF428E14FA04C910937DA785 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SyncVarNetworkBehaviour_1_Pack_m53DDCA8A7D5B5224168400575BCF6CDE35CCAE0F },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Mirror_CodeGenModule;
const Il2CppCodeGenModule g_Mirror_CodeGenModule = 
{
	"Mirror.dll",
	1225,
	s_methodPointers,
	6,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	68,
	s_rgctxIndices,
	377,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

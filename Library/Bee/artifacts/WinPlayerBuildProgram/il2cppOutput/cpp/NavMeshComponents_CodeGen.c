﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.AI.NavMeshBuilder2dWrapper::.ctor()
extern void NavMeshBuilder2dWrapper__ctor_mE0604240721E895B7E60290F43DF2584D52E9434 (void);
// 0x00000002 UnityEngine.Mesh UnityEngine.AI.NavMeshBuilder2dWrapper::GetMesh(UnityEngine.Sprite)
extern void NavMeshBuilder2dWrapper_GetMesh_mDD18BF75BA00E11A8D646F55C88AF5931DAE4D60 (void);
// 0x00000003 UnityEngine.Mesh UnityEngine.AI.NavMeshBuilder2dWrapper::GetMesh(UnityEngine.Collider2D)
extern void NavMeshBuilder2dWrapper_GetMesh_m3325665DB61483C0FE9C07F1B639A7D87B80271C (void);
// 0x00000004 System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject> UnityEngine.AI.NavMeshBuilder2dWrapper::GetRoot()
extern void NavMeshBuilder2dWrapper_GetRoot_m87906A051C38EFDC8680992CF98CD543611A7D51 (void);
// 0x00000005 System.Void UnityEngine.AI.NavMeshBuilder2d::CollectSources(System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>,UnityEngine.AI.NavMeshBuilder2dWrapper)
extern void NavMeshBuilder2d_CollectSources_mA2BA7E899BC025BBF017DC7E7F251DF3CE87B7C5 (void);
// 0x00000006 System.Void UnityEngine.AI.NavMeshBuilder2d::CollectSources(UnityEngine.GameObject,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>,UnityEngine.AI.NavMeshBuilder2dWrapper)
extern void NavMeshBuilder2d_CollectSources_mD6FEEA1EA9A203A8BB209DA5B316070E0BBDF1BC (void);
// 0x00000007 System.Void UnityEngine.AI.NavMeshBuilder2d::CollectSources(System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>,UnityEngine.SpriteRenderer,System.Int32,UnityEngine.AI.NavMeshBuilder2dWrapper)
extern void NavMeshBuilder2d_CollectSources_m4FA440D582BC0885E14E9ECE7AC5E96183FED6A8 (void);
// 0x00000008 System.Void UnityEngine.AI.NavMeshBuilder2d::CollectSources(System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>,UnityEngine.AI.NavMeshModifier,System.Int32,UnityEngine.AI.NavMeshBuilder2dWrapper)
extern void NavMeshBuilder2d_CollectSources_m55C2BD2842480F24EE4CFCBC3EF9103E1A4219A7 (void);
// 0x00000009 System.Void UnityEngine.AI.NavMeshBuilder2d::CollectTileSources(System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>,UnityEngine.Tilemaps.Tilemap,System.Int32,UnityEngine.AI.NavMeshBuilder2dWrapper)
extern void NavMeshBuilder2d_CollectTileSources_m94B1FA7CB9322EBB24DFCBA81DFA126CBFAECAD3 (void);
// 0x0000000A System.Void UnityEngine.AI.NavMeshBuilder2d::sprite2mesh(UnityEngine.Sprite,UnityEngine.Mesh)
extern void NavMeshBuilder2d_sprite2mesh_mE406993897C80E0268931FA92009D75048D5A24F (void);
// 0x0000000B UnityEngine.AI.NavMeshBuildSource UnityEngine.AI.NavMeshBuilder2d::BoxBoundSource(UnityEngine.Bounds)
extern void NavMeshBuilder2d_BoxBoundSource_m348739F23A2C4E063BE9C3A1B303AD77BDC30CDC (void);
// 0x0000000C System.Void UnityEngine.AI.NavMeshBuilder2d::.ctor()
extern void NavMeshBuilder2d__ctor_m8A3B7D0BBBAD7BEADEFE42E64EC2995DE8515778 (void);
// 0x0000000D System.Int32 UnityEngine.AI.NavMeshLink::get_agentTypeID()
extern void NavMeshLink_get_agentTypeID_mBA512E28C98F7271A4EC58863BC5507FABC1565F (void);
// 0x0000000E System.Void UnityEngine.AI.NavMeshLink::set_agentTypeID(System.Int32)
extern void NavMeshLink_set_agentTypeID_m9FBF19AA686D99FAC90A86273240403DE35D1171 (void);
// 0x0000000F UnityEngine.Vector3 UnityEngine.AI.NavMeshLink::get_startPoint()
extern void NavMeshLink_get_startPoint_mF070CE559A173AE0ED1AE64B66B6FC9D41CFB211 (void);
// 0x00000010 System.Void UnityEngine.AI.NavMeshLink::set_startPoint(UnityEngine.Vector3)
extern void NavMeshLink_set_startPoint_mE3F4B2CEEE6E3E58AE16E887B8EC1A5563DB1D00 (void);
// 0x00000011 UnityEngine.Vector3 UnityEngine.AI.NavMeshLink::get_endPoint()
extern void NavMeshLink_get_endPoint_m6D83EDB204BABC7C6F57BC30FE0CC9D4A77EAE50 (void);
// 0x00000012 System.Void UnityEngine.AI.NavMeshLink::set_endPoint(UnityEngine.Vector3)
extern void NavMeshLink_set_endPoint_mDE94DB8EA84FF53BA3A836B8F3E9ECD58E821408 (void);
// 0x00000013 System.Single UnityEngine.AI.NavMeshLink::get_width()
extern void NavMeshLink_get_width_mCE612FE2683527D823B6F8F2A4294063CB3A567D (void);
// 0x00000014 System.Void UnityEngine.AI.NavMeshLink::set_width(System.Single)
extern void NavMeshLink_set_width_mDBC807667A0351A719CE95945A9823C393AFDE1A (void);
// 0x00000015 System.Int32 UnityEngine.AI.NavMeshLink::get_costModifier()
extern void NavMeshLink_get_costModifier_m3FFEF19C169C808526B2426931C6830AD69F0E86 (void);
// 0x00000016 System.Void UnityEngine.AI.NavMeshLink::set_costModifier(System.Int32)
extern void NavMeshLink_set_costModifier_m5F1026E616A4014AEA86D37E9B74C0BA21F84CEA (void);
// 0x00000017 System.Boolean UnityEngine.AI.NavMeshLink::get_bidirectional()
extern void NavMeshLink_get_bidirectional_m55F07C5D29C159113CBD013656FCEEE9C42D8EDF (void);
// 0x00000018 System.Void UnityEngine.AI.NavMeshLink::set_bidirectional(System.Boolean)
extern void NavMeshLink_set_bidirectional_m11000F7EEE034182321107E0CFD206B93E2F0DB5 (void);
// 0x00000019 System.Boolean UnityEngine.AI.NavMeshLink::get_autoUpdate()
extern void NavMeshLink_get_autoUpdate_m990A747E3A283DBD7E7966B53539A6109F4006B8 (void);
// 0x0000001A System.Void UnityEngine.AI.NavMeshLink::set_autoUpdate(System.Boolean)
extern void NavMeshLink_set_autoUpdate_m435F36291F066510A7F5CA74858F2355306DD268 (void);
// 0x0000001B System.Int32 UnityEngine.AI.NavMeshLink::get_area()
extern void NavMeshLink_get_area_m0FD4060A62220399AA17DA1F4A0C92EF19E60E58 (void);
// 0x0000001C System.Void UnityEngine.AI.NavMeshLink::set_area(System.Int32)
extern void NavMeshLink_set_area_mF7AA5AB09937CF2C6C3C1A6AC15252903B16A44B (void);
// 0x0000001D System.Void UnityEngine.AI.NavMeshLink::OnEnable()
extern void NavMeshLink_OnEnable_m51D292D990683E1B33BC9667BC8B3CC726A45834 (void);
// 0x0000001E System.Void UnityEngine.AI.NavMeshLink::OnDisable()
extern void NavMeshLink_OnDisable_m4438490649F048480A36E505D4373DC963BECCF2 (void);
// 0x0000001F System.Void UnityEngine.AI.NavMeshLink::UpdateLink()
extern void NavMeshLink_UpdateLink_m209BE1B56461901F4DF40419F69052F0C9CBDA32 (void);
// 0x00000020 System.Void UnityEngine.AI.NavMeshLink::AddTracking(UnityEngine.AI.NavMeshLink)
extern void NavMeshLink_AddTracking_mE9E5F99B1112B73FE978CF9B4A2BDF9375EDEAB9 (void);
// 0x00000021 System.Void UnityEngine.AI.NavMeshLink::RemoveTracking(UnityEngine.AI.NavMeshLink)
extern void NavMeshLink_RemoveTracking_m5DBA4C7EDE2B4B075EE6AF0631B5E7F8B21B966B (void);
// 0x00000022 System.Void UnityEngine.AI.NavMeshLink::SetAutoUpdate(System.Boolean)
extern void NavMeshLink_SetAutoUpdate_mF68515C154A408E6E8BD7189E40E4F00E524CC72 (void);
// 0x00000023 System.Void UnityEngine.AI.NavMeshLink::AddLink()
extern void NavMeshLink_AddLink_m0025D8D10612A3D2043426453C98930817A84315 (void);
// 0x00000024 System.Boolean UnityEngine.AI.NavMeshLink::HasTransformChanged()
extern void NavMeshLink_HasTransformChanged_mD0C7C31886FBF76EA12C223497BB1E5B16C57DB9 (void);
// 0x00000025 System.Void UnityEngine.AI.NavMeshLink::OnDidApplyAnimationProperties()
extern void NavMeshLink_OnDidApplyAnimationProperties_m0E9E42780F1C0F3E20A2C195FCA29248C60A9A21 (void);
// 0x00000026 System.Void UnityEngine.AI.NavMeshLink::UpdateTrackedInstances()
extern void NavMeshLink_UpdateTrackedInstances_mE43058846B333900F1E7E096E8FE96057A4820EC (void);
// 0x00000027 System.Void UnityEngine.AI.NavMeshLink::.ctor()
extern void NavMeshLink__ctor_m25087DBD118A26C9C0F0F2C7840411837E1DD254 (void);
// 0x00000028 System.Void UnityEngine.AI.NavMeshLink::.cctor()
extern void NavMeshLink__cctor_mDA9752C99F9BD750B4CCAEF76D909DAB6B54877D (void);
// 0x00000029 System.Boolean UnityEngine.AI.NavMeshModifier::get_overrideArea()
extern void NavMeshModifier_get_overrideArea_mEBDF7BA1A5E02732EC58C0719E55C497E7AC8588 (void);
// 0x0000002A System.Void UnityEngine.AI.NavMeshModifier::set_overrideArea(System.Boolean)
extern void NavMeshModifier_set_overrideArea_mBB378398BA37AF6DF4B01DC251E8E0BB3B38C4D6 (void);
// 0x0000002B System.Int32 UnityEngine.AI.NavMeshModifier::get_area()
extern void NavMeshModifier_get_area_mFB0A525E06D5301CD8ACFD0E0DC5E8A6CC014456 (void);
// 0x0000002C System.Void UnityEngine.AI.NavMeshModifier::set_area(System.Int32)
extern void NavMeshModifier_set_area_m4A0F009842B714B7A5925733E142401BC3E7EC44 (void);
// 0x0000002D System.Boolean UnityEngine.AI.NavMeshModifier::get_ignoreFromBuild()
extern void NavMeshModifier_get_ignoreFromBuild_m3430FE1AC3498D6231D5CED7E66678EAE0846D69 (void);
// 0x0000002E System.Void UnityEngine.AI.NavMeshModifier::set_ignoreFromBuild(System.Boolean)
extern void NavMeshModifier_set_ignoreFromBuild_mAEB916ABF0C8770E96B687E41211B7DABB14BC0B (void);
// 0x0000002F System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier> UnityEngine.AI.NavMeshModifier::get_activeModifiers()
extern void NavMeshModifier_get_activeModifiers_mA80EBEDFD00FB8094130BACB291024C637E4DA1C (void);
// 0x00000030 System.Void UnityEngine.AI.NavMeshModifier::OnEnable()
extern void NavMeshModifier_OnEnable_mA7E3D7443E1420FFFD74711D68561D18D75C2543 (void);
// 0x00000031 System.Void UnityEngine.AI.NavMeshModifier::OnDisable()
extern void NavMeshModifier_OnDisable_mE5AE357A2D5966422AE7D122CE0528A932F8B63F (void);
// 0x00000032 System.Boolean UnityEngine.AI.NavMeshModifier::AffectsAgentType(System.Int32)
extern void NavMeshModifier_AffectsAgentType_m6E35A4A98A6465EE310DA56D3E364E43384916D1 (void);
// 0x00000033 System.Void UnityEngine.AI.NavMeshModifier::.ctor()
extern void NavMeshModifier__ctor_m50E7379A7135AA826CC86706FFC7CE03D52C3791 (void);
// 0x00000034 System.Void UnityEngine.AI.NavMeshModifier::.cctor()
extern void NavMeshModifier__cctor_m56C631511ADB28A8A18AE149FF8E3610BFD12E8A (void);
// 0x00000035 UnityEngine.Vector3 UnityEngine.AI.NavMeshModifierVolume::get_size()
extern void NavMeshModifierVolume_get_size_m321A424ABBDC0BD9E863CC5F04DBBBBD124E857C (void);
// 0x00000036 System.Void UnityEngine.AI.NavMeshModifierVolume::set_size(UnityEngine.Vector3)
extern void NavMeshModifierVolume_set_size_m046BA321411814CD871C2F294E856B8056F39879 (void);
// 0x00000037 UnityEngine.Vector3 UnityEngine.AI.NavMeshModifierVolume::get_center()
extern void NavMeshModifierVolume_get_center_m680C062269D60BD22DEC8DE535627C68C0867122 (void);
// 0x00000038 System.Void UnityEngine.AI.NavMeshModifierVolume::set_center(UnityEngine.Vector3)
extern void NavMeshModifierVolume_set_center_mEA63562D97D51F46D0E1B2CEACCAC4A15C22E7E1 (void);
// 0x00000039 System.Int32 UnityEngine.AI.NavMeshModifierVolume::get_area()
extern void NavMeshModifierVolume_get_area_m7FFDB165FCE530ACCE64932C2F623A1E040FD37B (void);
// 0x0000003A System.Void UnityEngine.AI.NavMeshModifierVolume::set_area(System.Int32)
extern void NavMeshModifierVolume_set_area_m6CE9C2E002305B27C0EB4824AA2259B7BB9056B8 (void);
// 0x0000003B System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume> UnityEngine.AI.NavMeshModifierVolume::get_activeModifiers()
extern void NavMeshModifierVolume_get_activeModifiers_m15594BB3E469B272A98BF5B66D14C82F77E6046A (void);
// 0x0000003C System.Void UnityEngine.AI.NavMeshModifierVolume::OnEnable()
extern void NavMeshModifierVolume_OnEnable_m4F14D96EB3294A96D71B179300DB2483F2FA6180 (void);
// 0x0000003D System.Void UnityEngine.AI.NavMeshModifierVolume::OnDisable()
extern void NavMeshModifierVolume_OnDisable_mA21F77B955F1C82D0E88D09A346BF016BC8F0B84 (void);
// 0x0000003E System.Boolean UnityEngine.AI.NavMeshModifierVolume::AffectsAgentType(System.Int32)
extern void NavMeshModifierVolume_AffectsAgentType_m181399154BFAA77A6F48D6C0281C37F088F9E98B (void);
// 0x0000003F System.Void UnityEngine.AI.NavMeshModifierVolume::.ctor()
extern void NavMeshModifierVolume__ctor_mCFA57767A4A82943453EA212E465784E612C6EC2 (void);
// 0x00000040 System.Void UnityEngine.AI.NavMeshModifierVolume::.cctor()
extern void NavMeshModifierVolume__cctor_mEFC3F16CD27DDC34A56A2A3BFF05436968F35DDE (void);
// 0x00000041 System.Int32 UnityEngine.AI.NavMeshSurface::get_agentTypeID()
extern void NavMeshSurface_get_agentTypeID_m817A837C523AD133FAE8D159DAA4C568BA431FA3 (void);
// 0x00000042 System.Void UnityEngine.AI.NavMeshSurface::set_agentTypeID(System.Int32)
extern void NavMeshSurface_set_agentTypeID_m75B82A12C316BA7F1BC1DAB6D9DD832C2A13D690 (void);
// 0x00000043 UnityEngine.AI.CollectObjects UnityEngine.AI.NavMeshSurface::get_collectObjects()
extern void NavMeshSurface_get_collectObjects_m70B41C466C0E6CB9B8D216E15AFD446D667B7A48 (void);
// 0x00000044 System.Void UnityEngine.AI.NavMeshSurface::set_collectObjects(UnityEngine.AI.CollectObjects)
extern void NavMeshSurface_set_collectObjects_mAE4B9BEF06AF02D80159BB2B02DDD4DDB11C90B8 (void);
// 0x00000045 UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::get_size()
extern void NavMeshSurface_get_size_mD238C28C636DAA2C4942544223D8BE33501083AA (void);
// 0x00000046 System.Void UnityEngine.AI.NavMeshSurface::set_size(UnityEngine.Vector3)
extern void NavMeshSurface_set_size_m25C813BC47FC1FC9FD99613FEC6E7764A6712E51 (void);
// 0x00000047 UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::get_center()
extern void NavMeshSurface_get_center_m84044AA44C1246C0C82AB10215FFB288CFC75280 (void);
// 0x00000048 System.Void UnityEngine.AI.NavMeshSurface::set_center(UnityEngine.Vector3)
extern void NavMeshSurface_set_center_m7ADBC113E3E0D11F7D9463C77D7F78F141877473 (void);
// 0x00000049 UnityEngine.LayerMask UnityEngine.AI.NavMeshSurface::get_layerMask()
extern void NavMeshSurface_get_layerMask_mE85E4356346FBD202B24DC58457E0E7DF8C13C7B (void);
// 0x0000004A System.Void UnityEngine.AI.NavMeshSurface::set_layerMask(UnityEngine.LayerMask)
extern void NavMeshSurface_set_layerMask_mBA70CAC4BAA7A9DD70F333E42DB20246D967F4A3 (void);
// 0x0000004B UnityEngine.AI.NavMeshCollectGeometry UnityEngine.AI.NavMeshSurface::get_useGeometry()
extern void NavMeshSurface_get_useGeometry_m88DEE508B8229A62B19F4355405F56BCC07E6F34 (void);
// 0x0000004C System.Void UnityEngine.AI.NavMeshSurface::set_useGeometry(UnityEngine.AI.NavMeshCollectGeometry)
extern void NavMeshSurface_set_useGeometry_m226D0AD24ABD7189CDFB11120E841EF2F26FD171 (void);
// 0x0000004D System.Int32 UnityEngine.AI.NavMeshSurface::get_defaultArea()
extern void NavMeshSurface_get_defaultArea_mC48E96F3ACAA2F701620D43CD7D860F30395CF30 (void);
// 0x0000004E System.Void UnityEngine.AI.NavMeshSurface::set_defaultArea(System.Int32)
extern void NavMeshSurface_set_defaultArea_mFE8E7487806747A5BA23AF04B518101019BD0526 (void);
// 0x0000004F System.Boolean UnityEngine.AI.NavMeshSurface::get_ignoreNavMeshAgent()
extern void NavMeshSurface_get_ignoreNavMeshAgent_m3974A81143726E7738955EB1E66746BDD4DFEB79 (void);
// 0x00000050 System.Void UnityEngine.AI.NavMeshSurface::set_ignoreNavMeshAgent(System.Boolean)
extern void NavMeshSurface_set_ignoreNavMeshAgent_m3BB95A33F5E5A0F637147E29C8B94D6934C22A6E (void);
// 0x00000051 System.Boolean UnityEngine.AI.NavMeshSurface::get_ignoreNavMeshObstacle()
extern void NavMeshSurface_get_ignoreNavMeshObstacle_mBE7EB23C662C3D8963EFAF40DC48E34934514459 (void);
// 0x00000052 System.Void UnityEngine.AI.NavMeshSurface::set_ignoreNavMeshObstacle(System.Boolean)
extern void NavMeshSurface_set_ignoreNavMeshObstacle_mE48F13378BCB6BC698B42DD410078B0F2BC2C15E (void);
// 0x00000053 System.Boolean UnityEngine.AI.NavMeshSurface::get_overrideTileSize()
extern void NavMeshSurface_get_overrideTileSize_m498945E257ABA5479D2EF46CC115E83C30F9CD39 (void);
// 0x00000054 System.Void UnityEngine.AI.NavMeshSurface::set_overrideTileSize(System.Boolean)
extern void NavMeshSurface_set_overrideTileSize_m0DCB54AA760A43F4F0A028A9C7F23CFF0E80D6F0 (void);
// 0x00000055 System.Int32 UnityEngine.AI.NavMeshSurface::get_tileSize()
extern void NavMeshSurface_get_tileSize_m547CCF2E478DD4F6949CE4790F1666DC811FDA4B (void);
// 0x00000056 System.Void UnityEngine.AI.NavMeshSurface::set_tileSize(System.Int32)
extern void NavMeshSurface_set_tileSize_m87B5E72A2739EDF7E42C2CBBCFC2D63CD30A7904 (void);
// 0x00000057 System.Boolean UnityEngine.AI.NavMeshSurface::get_overrideVoxelSize()
extern void NavMeshSurface_get_overrideVoxelSize_m7C0420C2A9F451680139E2590171379605670DBA (void);
// 0x00000058 System.Void UnityEngine.AI.NavMeshSurface::set_overrideVoxelSize(System.Boolean)
extern void NavMeshSurface_set_overrideVoxelSize_mF186072B354E8BF4FF6546B8C29C709B7A395CB7 (void);
// 0x00000059 System.Single UnityEngine.AI.NavMeshSurface::get_voxelSize()
extern void NavMeshSurface_get_voxelSize_m3FCE294C0933A0C459F34E1BF0E6F6F513F75C1A (void);
// 0x0000005A System.Void UnityEngine.AI.NavMeshSurface::set_voxelSize(System.Single)
extern void NavMeshSurface_set_voxelSize_m4556E474F8A9831CE1173E1FA9F7B20AA7F8A702 (void);
// 0x0000005B System.Boolean UnityEngine.AI.NavMeshSurface::get_buildHeightMesh()
extern void NavMeshSurface_get_buildHeightMesh_mFE7AE3812BF637CAA070B5E4AE37FD274DCB175B (void);
// 0x0000005C System.Void UnityEngine.AI.NavMeshSurface::set_buildHeightMesh(System.Boolean)
extern void NavMeshSurface_set_buildHeightMesh_mFD446C2E53118C25FC2B006283A0D632281E5CF9 (void);
// 0x0000005D UnityEngine.AI.NavMeshData UnityEngine.AI.NavMeshSurface::get_navMeshData()
extern void NavMeshSurface_get_navMeshData_m944D83922EAC0768700E6BF3B41F7AB6D457BC8F (void);
// 0x0000005E System.Void UnityEngine.AI.NavMeshSurface::set_navMeshData(UnityEngine.AI.NavMeshData)
extern void NavMeshSurface_set_navMeshData_m8C2E4D6D6E50DEE4F8E6D4882703857C5CCCCD52 (void);
// 0x0000005F System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface> UnityEngine.AI.NavMeshSurface::get_activeSurfaces()
extern void NavMeshSurface_get_activeSurfaces_mF1F8A900FF35EC6DDA6A3CD9C9EE53A2D4ADD4AA (void);
// 0x00000060 System.Void UnityEngine.AI.NavMeshSurface::OnEnable()
extern void NavMeshSurface_OnEnable_m308F744EB55A7443F56CEBE857C898171A59AA69 (void);
// 0x00000061 System.Void UnityEngine.AI.NavMeshSurface::OnDisable()
extern void NavMeshSurface_OnDisable_m99AB28410CF2481C04DA1DA05CADE73AF089101B (void);
// 0x00000062 System.Void UnityEngine.AI.NavMeshSurface::AddData()
extern void NavMeshSurface_AddData_m3B92D495D85795E8AAEB5BF782A86576DBF7206F (void);
// 0x00000063 System.Void UnityEngine.AI.NavMeshSurface::RemoveData()
extern void NavMeshSurface_RemoveData_m244AFF7183FD627F16B91BAD59DC32131738BB3B (void);
// 0x00000064 UnityEngine.AI.NavMeshBuildSettings UnityEngine.AI.NavMeshSurface::GetBuildSettings()
extern void NavMeshSurface_GetBuildSettings_mD1046164C39A191D78217E4E7A0329F40122760A (void);
// 0x00000065 System.Void UnityEngine.AI.NavMeshSurface::BuildNavMesh()
extern void NavMeshSurface_BuildNavMesh_m593B302D342E36F0C4FF9F9C3EB195B9A0E03AB5 (void);
// 0x00000066 UnityEngine.AsyncOperation UnityEngine.AI.NavMeshSurface::UpdateNavMesh(UnityEngine.AI.NavMeshData)
extern void NavMeshSurface_UpdateNavMesh_m29A795A61811599F6BD6BFF45C0EAC66010BF64A (void);
// 0x00000067 System.Void UnityEngine.AI.NavMeshSurface::Register(UnityEngine.AI.NavMeshSurface)
extern void NavMeshSurface_Register_m6658C2249E6B1E7573C06FE21C3D33D5FB809453 (void);
// 0x00000068 System.Void UnityEngine.AI.NavMeshSurface::Unregister(UnityEngine.AI.NavMeshSurface)
extern void NavMeshSurface_Unregister_m954186C642CB8A05D45985BBACCF873D58E01737 (void);
// 0x00000069 System.Void UnityEngine.AI.NavMeshSurface::UpdateActive()
extern void NavMeshSurface_UpdateActive_m64DE882138713A5E3B6650706665164F3780EC88 (void);
// 0x0000006A System.Void UnityEngine.AI.NavMeshSurface::AppendModifierVolumes(System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>&)
extern void NavMeshSurface_AppendModifierVolumes_mEFF654357C4853A17D06DBF634DB122839122550 (void);
// 0x0000006B System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource> UnityEngine.AI.NavMeshSurface::CollectSources()
extern void NavMeshSurface_CollectSources_mD13E1B734C6EC5650E652F7F693B53AF714D4B11 (void);
// 0x0000006C UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::Abs(UnityEngine.Vector3)
extern void NavMeshSurface_Abs_m6FDEC6511AC40FD4E1C76F8BEB31A60A5BA5C293 (void);
// 0x0000006D UnityEngine.Bounds UnityEngine.AI.NavMeshSurface::GetWorldBounds(UnityEngine.Matrix4x4,UnityEngine.Bounds)
extern void NavMeshSurface_GetWorldBounds_m53366C6EC28A95EB84515C2572B976C1EF5F0D77 (void);
// 0x0000006E UnityEngine.Bounds UnityEngine.AI.NavMeshSurface::CalculateWorldBounds(System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>)
extern void NavMeshSurface_CalculateWorldBounds_m6E01402D5A3CD76865FD4A23FAFBFA9FB9A5F713 (void);
// 0x0000006F System.Boolean UnityEngine.AI.NavMeshSurface::HasTransformChanged()
extern void NavMeshSurface_HasTransformChanged_m5766AE0E589120789DE1EAB7AE22AA206AE74E42 (void);
// 0x00000070 System.Void UnityEngine.AI.NavMeshSurface::UpdateDataIfTransformChanged()
extern void NavMeshSurface_UpdateDataIfTransformChanged_mEAF82FD02A70516374534495BC57E62A08238423 (void);
// 0x00000071 System.Void UnityEngine.AI.NavMeshSurface::.ctor()
extern void NavMeshSurface__ctor_m79043E9325067CF57A0685A4085E85F8947587D4 (void);
// 0x00000072 System.Void UnityEngine.AI.NavMeshSurface::.cctor()
extern void NavMeshSurface__cctor_mA4BC6AC522609D76F252DBA917DB451153FA506F (void);
// 0x00000073 System.Void UnityEngine.AI.NavMeshSurface/<>c::.cctor()
extern void U3CU3Ec__cctor_mEBDA56AE0FA98A2660170758B63D2C55175EC3D2 (void);
// 0x00000074 System.Void UnityEngine.AI.NavMeshSurface/<>c::.ctor()
extern void U3CU3Ec__ctor_mC124ECF3C8BCA1F364397BCDA11234EAF60C1648 (void);
// 0x00000075 System.Boolean UnityEngine.AI.NavMeshSurface/<>c::<AppendModifierVolumes>b__76_0(UnityEngine.AI.NavMeshModifierVolume)
extern void U3CU3Ec_U3CAppendModifierVolumesU3Eb__76_0_mFA187EB36C602A178C49CA72A2B982E5311016ED (void);
// 0x00000076 System.Boolean UnityEngine.AI.NavMeshSurface/<>c::<CollectSources>b__77_0(UnityEngine.AI.NavMeshModifier)
extern void U3CU3Ec_U3CCollectSourcesU3Eb__77_0_m8F7050C648EFFB8533ACF3CA1BA6C5CBDB6DF999 (void);
// 0x00000077 System.Boolean UnityEngine.AI.NavMeshSurface/<>c::<CollectSources>b__77_1(UnityEngine.AI.NavMeshBuildSource)
extern void U3CU3Ec_U3CCollectSourcesU3Eb__77_1_m5BF192B9E3C50B44808654213B9A6DB4B5938D8C (void);
// 0x00000078 System.Boolean UnityEngine.AI.NavMeshSurface/<>c::<CollectSources>b__77_2(UnityEngine.AI.NavMeshBuildSource)
extern void U3CU3Ec_U3CCollectSourcesU3Eb__77_2_m533C32C15CF6C2F6E273BFB5DD04D776AA3CF04D (void);
// 0x00000079 System.Int32 UnityEngine.AI.NavMeshSurface2d::get_agentTypeID()
extern void NavMeshSurface2d_get_agentTypeID_mBB1CB5B47E7868994C9BB2495EB08F2D23B32C34 (void);
// 0x0000007A System.Void UnityEngine.AI.NavMeshSurface2d::set_agentTypeID(System.Int32)
extern void NavMeshSurface2d_set_agentTypeID_m9C298B6186807E4ED16AD1997412E957373B73A7 (void);
// 0x0000007B UnityEngine.AI.CollectObjects2d UnityEngine.AI.NavMeshSurface2d::get_collectObjects()
extern void NavMeshSurface2d_get_collectObjects_m3B7DE460DF0816F803B264FCA035FC154FA12103 (void);
// 0x0000007C System.Void UnityEngine.AI.NavMeshSurface2d::set_collectObjects(UnityEngine.AI.CollectObjects2d)
extern void NavMeshSurface2d_set_collectObjects_m455EF8C631748B70DD72489190C7BDEB39325F1A (void);
// 0x0000007D UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface2d::get_size()
extern void NavMeshSurface2d_get_size_m5B92BB1C6C52715724A44144FE114D55D151DD3C (void);
// 0x0000007E System.Void UnityEngine.AI.NavMeshSurface2d::set_size(UnityEngine.Vector3)
extern void NavMeshSurface2d_set_size_mC3357FABEF20078866029AB7ECF61529D9DAC990 (void);
// 0x0000007F UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface2d::get_center()
extern void NavMeshSurface2d_get_center_m7F52FBF7F31D5E8A941602D8DD473E8F7BFC42B2 (void);
// 0x00000080 System.Void UnityEngine.AI.NavMeshSurface2d::set_center(UnityEngine.Vector3)
extern void NavMeshSurface2d_set_center_mD08B2B85279141FD6D23DD35B63CED4F1C80975A (void);
// 0x00000081 UnityEngine.LayerMask UnityEngine.AI.NavMeshSurface2d::get_layerMask()
extern void NavMeshSurface2d_get_layerMask_m67A239369D5B7B54ED3124D70167659866AF0BEA (void);
// 0x00000082 System.Void UnityEngine.AI.NavMeshSurface2d::set_layerMask(UnityEngine.LayerMask)
extern void NavMeshSurface2d_set_layerMask_m0C8C885CC76560B80776073CE11080BFAE7E80CB (void);
// 0x00000083 UnityEngine.AI.NavMeshCollectGeometry UnityEngine.AI.NavMeshSurface2d::get_useGeometry()
extern void NavMeshSurface2d_get_useGeometry_mAC674708212AD945152A392A291C3BAE9F1DE538 (void);
// 0x00000084 System.Void UnityEngine.AI.NavMeshSurface2d::set_useGeometry(UnityEngine.AI.NavMeshCollectGeometry)
extern void NavMeshSurface2d_set_useGeometry_m16CE11A1EC8166569C50942362BBC963CE4BC6D1 (void);
// 0x00000085 System.Boolean UnityEngine.AI.NavMeshSurface2d::get_overrideByGrid()
extern void NavMeshSurface2d_get_overrideByGrid_m3EA3E3ACF3A55B4E44570BA9940B56A6911069D2 (void);
// 0x00000086 System.Void UnityEngine.AI.NavMeshSurface2d::set_overrideByGrid(System.Boolean)
extern void NavMeshSurface2d_set_overrideByGrid_m1262A9D0EC451A80E01A9931E407C377612C27DC (void);
// 0x00000087 UnityEngine.GameObject UnityEngine.AI.NavMeshSurface2d::get_useMeshPrefab()
extern void NavMeshSurface2d_get_useMeshPrefab_mD2DECC874AE35383DCD2E870FBBEC5B62C50C153 (void);
// 0x00000088 System.Void UnityEngine.AI.NavMeshSurface2d::set_useMeshPrefab(UnityEngine.GameObject)
extern void NavMeshSurface2d_set_useMeshPrefab_m699557EF6160D68CC03B76ED11070FB47C50DA18 (void);
// 0x00000089 System.Boolean UnityEngine.AI.NavMeshSurface2d::get_compressBounds()
extern void NavMeshSurface2d_get_compressBounds_m452A027FB3102878642D048C9B3E3A89F913FB8A (void);
// 0x0000008A System.Void UnityEngine.AI.NavMeshSurface2d::set_compressBounds(System.Boolean)
extern void NavMeshSurface2d_set_compressBounds_m3B6948D3AAD099F1BDCCD186997AA7B1E0730080 (void);
// 0x0000008B UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface2d::get_overrideVector()
extern void NavMeshSurface2d_get_overrideVector_m338B5656EF72D1CA4D03F28E552EF40884A5048B (void);
// 0x0000008C System.Void UnityEngine.AI.NavMeshSurface2d::set_overrideVector(UnityEngine.Vector3)
extern void NavMeshSurface2d_set_overrideVector_mEA06BDD1BBF5FB94D62C435BD519CB7B767E29DD (void);
// 0x0000008D System.Int32 UnityEngine.AI.NavMeshSurface2d::get_defaultArea()
extern void NavMeshSurface2d_get_defaultArea_mD0B11DF562EE8D388B193623AB23D4CB1AF20DB4 (void);
// 0x0000008E System.Void UnityEngine.AI.NavMeshSurface2d::set_defaultArea(System.Int32)
extern void NavMeshSurface2d_set_defaultArea_m093C89F0B4B0CFB215F4D3470D23DD759038C75A (void);
// 0x0000008F System.Boolean UnityEngine.AI.NavMeshSurface2d::get_ignoreNavMeshAgent()
extern void NavMeshSurface2d_get_ignoreNavMeshAgent_m37711799CC6E0BEDEE6E895C4441E0E6559BB9A4 (void);
// 0x00000090 System.Void UnityEngine.AI.NavMeshSurface2d::set_ignoreNavMeshAgent(System.Boolean)
extern void NavMeshSurface2d_set_ignoreNavMeshAgent_m127DCF407993171E5692F97328128295FCEDC555 (void);
// 0x00000091 System.Boolean UnityEngine.AI.NavMeshSurface2d::get_ignoreNavMeshObstacle()
extern void NavMeshSurface2d_get_ignoreNavMeshObstacle_m3DF17F7B512C10A502ADFD4B6D504423F82DC880 (void);
// 0x00000092 System.Void UnityEngine.AI.NavMeshSurface2d::set_ignoreNavMeshObstacle(System.Boolean)
extern void NavMeshSurface2d_set_ignoreNavMeshObstacle_mD4378B92F615D8F09C1DB640AF56FE686CBEDBFA (void);
// 0x00000093 System.Boolean UnityEngine.AI.NavMeshSurface2d::get_overrideTileSize()
extern void NavMeshSurface2d_get_overrideTileSize_mAEC6D7F272B6B186FE43ED357853E3B47DF0ECB4 (void);
// 0x00000094 System.Void UnityEngine.AI.NavMeshSurface2d::set_overrideTileSize(System.Boolean)
extern void NavMeshSurface2d_set_overrideTileSize_m2B053D9BEC47F27D0D36F20A9AD8743950391008 (void);
// 0x00000095 System.Int32 UnityEngine.AI.NavMeshSurface2d::get_tileSize()
extern void NavMeshSurface2d_get_tileSize_m0B17267D260283F8BCEAC17069C587C9B9AF4D78 (void);
// 0x00000096 System.Void UnityEngine.AI.NavMeshSurface2d::set_tileSize(System.Int32)
extern void NavMeshSurface2d_set_tileSize_m9D36C7A8FBFDB2FA71B6FFC941C58F291561B6BD (void);
// 0x00000097 System.Boolean UnityEngine.AI.NavMeshSurface2d::get_overrideVoxelSize()
extern void NavMeshSurface2d_get_overrideVoxelSize_mD997D4ADD4018898E7A4DE66C2EC2DF9A57FB3BE (void);
// 0x00000098 System.Void UnityEngine.AI.NavMeshSurface2d::set_overrideVoxelSize(System.Boolean)
extern void NavMeshSurface2d_set_overrideVoxelSize_m73AE2BDAD36CA38BF5E2FE7FA9070D118A51A272 (void);
// 0x00000099 System.Single UnityEngine.AI.NavMeshSurface2d::get_voxelSize()
extern void NavMeshSurface2d_get_voxelSize_mB797865B80F6F7A4502D919EB4E250128DBEDB68 (void);
// 0x0000009A System.Void UnityEngine.AI.NavMeshSurface2d::set_voxelSize(System.Single)
extern void NavMeshSurface2d_set_voxelSize_m87966F102916FBAE3DC2516A0687D31F5E2AF803 (void);
// 0x0000009B System.Boolean UnityEngine.AI.NavMeshSurface2d::get_buildHeightMesh()
extern void NavMeshSurface2d_get_buildHeightMesh_m4E906AFBA3EC431C60BBBD8BBCC78714B6DBD381 (void);
// 0x0000009C System.Void UnityEngine.AI.NavMeshSurface2d::set_buildHeightMesh(System.Boolean)
extern void NavMeshSurface2d_set_buildHeightMesh_mE59973F052730AB36EBCFAD0E62A388EE7CFC8D7 (void);
// 0x0000009D UnityEngine.AI.NavMeshData UnityEngine.AI.NavMeshSurface2d::get_navMeshData()
extern void NavMeshSurface2d_get_navMeshData_mB95AF6262EE3CE34080A10BF7B6352E9A18BCA35 (void);
// 0x0000009E System.Void UnityEngine.AI.NavMeshSurface2d::set_navMeshData(UnityEngine.AI.NavMeshData)
extern void NavMeshSurface2d_set_navMeshData_mE704451EB97F03E4A8AE4A999812EAF78F95273D (void);
// 0x0000009F System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface2d> UnityEngine.AI.NavMeshSurface2d::get_activeSurfaces()
extern void NavMeshSurface2d_get_activeSurfaces_m4FB9955072D676355E83FBB77BA1DA66E0E7822F (void);
// 0x000000A0 System.Void UnityEngine.AI.NavMeshSurface2d::OnEnable()
extern void NavMeshSurface2d_OnEnable_mE492F8892AE9D644EED009BA4694659089B16B07 (void);
// 0x000000A1 System.Void UnityEngine.AI.NavMeshSurface2d::OnDisable()
extern void NavMeshSurface2d_OnDisable_m1B3B81A9382DEA6A3E26C4FA6A905821705CD785 (void);
// 0x000000A2 System.Void UnityEngine.AI.NavMeshSurface2d::AddData()
extern void NavMeshSurface2d_AddData_m5A159869514B72786B077F87BF90860C17331651 (void);
// 0x000000A3 System.Void UnityEngine.AI.NavMeshSurface2d::RemoveData()
extern void NavMeshSurface2d_RemoveData_mCFB8ECD4B4C7342D41A40830899C40538A53F174 (void);
// 0x000000A4 UnityEngine.AI.NavMeshBuildSettings UnityEngine.AI.NavMeshSurface2d::GetBuildSettings()
extern void NavMeshSurface2d_GetBuildSettings_m0A3F6F5A3703DF3623BE7031D9E831BC1A64A3A6 (void);
// 0x000000A5 System.Void UnityEngine.AI.NavMeshSurface2d::BuildNavMesh()
extern void NavMeshSurface2d_BuildNavMesh_mD546A0EEA12D8B97AF264D9367898AB2F27A5AC0 (void);
// 0x000000A6 UnityEngine.AsyncOperation UnityEngine.AI.NavMeshSurface2d::BuildNavMeshAsync()
extern void NavMeshSurface2d_BuildNavMeshAsync_m399AAD6583BCEC4EE027C35A9489760104D3503F (void);
// 0x000000A7 UnityEngine.AsyncOperation UnityEngine.AI.NavMeshSurface2d::UpdateNavMesh(UnityEngine.AI.NavMeshData)
extern void NavMeshSurface2d_UpdateNavMesh_mFA61071748EBE2ADF9507013D77F714A0E1EC9D4 (void);
// 0x000000A8 System.Void UnityEngine.AI.NavMeshSurface2d::Register(UnityEngine.AI.NavMeshSurface2d)
extern void NavMeshSurface2d_Register_m2C4BE30EBDB969A0CE56D820DA3B9CE4A9B0BC09 (void);
// 0x000000A9 System.Void UnityEngine.AI.NavMeshSurface2d::Unregister(UnityEngine.AI.NavMeshSurface2d)
extern void NavMeshSurface2d_Unregister_m13BC7E14D33A52A97CFC7BFD7AC682F9B68188B5 (void);
// 0x000000AA System.Void UnityEngine.AI.NavMeshSurface2d::UpdateActive()
extern void NavMeshSurface2d_UpdateActive_mA7E40282FE68FB4AF115A0C926762B7E1B992CAB (void);
// 0x000000AB System.Void UnityEngine.AI.NavMeshSurface2d::AppendModifierVolumes(System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>&)
extern void NavMeshSurface2d_AppendModifierVolumes_mF8623E33A19BA16DB790A4BD156E87D6EB11B07B (void);
// 0x000000AC System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource> UnityEngine.AI.NavMeshSurface2d::CollectSources()
extern void NavMeshSurface2d_CollectSources_m2AF42C3A2EADA6AE761A72A5CD39BA9611294A86 (void);
// 0x000000AD UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface2d::Abs(UnityEngine.Vector3)
extern void NavMeshSurface2d_Abs_m93ACE4AB9A0B128A56DB0EF705832259BD438AD4 (void);
// 0x000000AE UnityEngine.Bounds UnityEngine.AI.NavMeshSurface2d::GetWorldBounds(UnityEngine.Matrix4x4,UnityEngine.Bounds)
extern void NavMeshSurface2d_GetWorldBounds_m19310580DD329AA9F2D2157C8D99A6955D0180B0 (void);
// 0x000000AF UnityEngine.Bounds UnityEngine.AI.NavMeshSurface2d::CalculateWorldBounds(System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>)
extern void NavMeshSurface2d_CalculateWorldBounds_m8444ECC8CFA16BE978C0BF9963B71A73DA5FCC62 (void);
// 0x000000B0 UnityEngine.Bounds UnityEngine.AI.NavMeshSurface2d::CalculateGridWorldBounds(UnityEngine.Matrix4x4)
extern void NavMeshSurface2d_CalculateGridWorldBounds_m0DAB250D4DF2DD43035D392DC7E45B869E8E6282 (void);
// 0x000000B1 System.Boolean UnityEngine.AI.NavMeshSurface2d::HasTransformChanged()
extern void NavMeshSurface2d_HasTransformChanged_m3BDDD1E4784451CE7CED9D206E70401864D5C443 (void);
// 0x000000B2 System.Void UnityEngine.AI.NavMeshSurface2d::UpdateDataIfTransformChanged()
extern void NavMeshSurface2d_UpdateDataIfTransformChanged_m3367FA661EDFCFAAB82E830F02306EB2EF9602C1 (void);
// 0x000000B3 System.Void UnityEngine.AI.NavMeshSurface2d::.ctor()
extern void NavMeshSurface2d__ctor_m652509810BFC1FDC84A33F856BEAB0D20057EC53 (void);
// 0x000000B4 System.Void UnityEngine.AI.NavMeshSurface2d::.cctor()
extern void NavMeshSurface2d__cctor_m7CBC813ECA4BD28B56B7D06670C0E06A1E471500 (void);
// 0x000000B5 System.Void UnityEngine.AI.NavMeshSurface2d/<>c::.cctor()
extern void U3CU3Ec__cctor_mB534DF09C3EF248D17C7AB487F643D3F29EFDD60 (void);
// 0x000000B6 System.Void UnityEngine.AI.NavMeshSurface2d/<>c::.ctor()
extern void U3CU3Ec__ctor_m0B0E3E0360E4786541071127CD394606E512BFBE (void);
// 0x000000B7 System.Boolean UnityEngine.AI.NavMeshSurface2d/<>c::<AppendModifierVolumes>b__93_0(UnityEngine.AI.NavMeshModifierVolume)
extern void U3CU3Ec_U3CAppendModifierVolumesU3Eb__93_0_mC01A94D6E7899DEB8079B26EF4EB278DF6DDB092 (void);
// 0x000000B8 System.Boolean UnityEngine.AI.NavMeshSurface2d/<>c::<CollectSources>b__94_0(UnityEngine.AI.NavMeshModifier)
extern void U3CU3Ec_U3CCollectSourcesU3Eb__94_0_m58F9EB3197CE0E95D0DF6B323F26F001370C45BA (void);
// 0x000000B9 System.Boolean UnityEngine.AI.NavMeshSurface2d/<>c::<CollectSources>b__94_1(UnityEngine.AI.NavMeshBuildSource)
extern void U3CU3Ec_U3CCollectSourcesU3Eb__94_1_m819F97AF66B0746B51661726FBD6F07926288D59 (void);
// 0x000000BA System.Boolean UnityEngine.AI.NavMeshSurface2d/<>c::<CollectSources>b__94_2(UnityEngine.AI.NavMeshBuildSource)
extern void U3CU3Ec_U3CCollectSourcesU3Eb__94_2_m4404DE300ABDAA8665AD90E12098A4229B20410F (void);
static Il2CppMethodPointer s_methodPointers[186] = 
{
	NavMeshBuilder2dWrapper__ctor_mE0604240721E895B7E60290F43DF2584D52E9434,
	NavMeshBuilder2dWrapper_GetMesh_mDD18BF75BA00E11A8D646F55C88AF5931DAE4D60,
	NavMeshBuilder2dWrapper_GetMesh_m3325665DB61483C0FE9C07F1B639A7D87B80271C,
	NavMeshBuilder2dWrapper_GetRoot_m87906A051C38EFDC8680992CF98CD543611A7D51,
	NavMeshBuilder2d_CollectSources_mA2BA7E899BC025BBF017DC7E7F251DF3CE87B7C5,
	NavMeshBuilder2d_CollectSources_mD6FEEA1EA9A203A8BB209DA5B316070E0BBDF1BC,
	NavMeshBuilder2d_CollectSources_m4FA440D582BC0885E14E9ECE7AC5E96183FED6A8,
	NavMeshBuilder2d_CollectSources_m55C2BD2842480F24EE4CFCBC3EF9103E1A4219A7,
	NavMeshBuilder2d_CollectTileSources_m94B1FA7CB9322EBB24DFCBA81DFA126CBFAECAD3,
	NavMeshBuilder2d_sprite2mesh_mE406993897C80E0268931FA92009D75048D5A24F,
	NavMeshBuilder2d_BoxBoundSource_m348739F23A2C4E063BE9C3A1B303AD77BDC30CDC,
	NavMeshBuilder2d__ctor_m8A3B7D0BBBAD7BEADEFE42E64EC2995DE8515778,
	NavMeshLink_get_agentTypeID_mBA512E28C98F7271A4EC58863BC5507FABC1565F,
	NavMeshLink_set_agentTypeID_m9FBF19AA686D99FAC90A86273240403DE35D1171,
	NavMeshLink_get_startPoint_mF070CE559A173AE0ED1AE64B66B6FC9D41CFB211,
	NavMeshLink_set_startPoint_mE3F4B2CEEE6E3E58AE16E887B8EC1A5563DB1D00,
	NavMeshLink_get_endPoint_m6D83EDB204BABC7C6F57BC30FE0CC9D4A77EAE50,
	NavMeshLink_set_endPoint_mDE94DB8EA84FF53BA3A836B8F3E9ECD58E821408,
	NavMeshLink_get_width_mCE612FE2683527D823B6F8F2A4294063CB3A567D,
	NavMeshLink_set_width_mDBC807667A0351A719CE95945A9823C393AFDE1A,
	NavMeshLink_get_costModifier_m3FFEF19C169C808526B2426931C6830AD69F0E86,
	NavMeshLink_set_costModifier_m5F1026E616A4014AEA86D37E9B74C0BA21F84CEA,
	NavMeshLink_get_bidirectional_m55F07C5D29C159113CBD013656FCEEE9C42D8EDF,
	NavMeshLink_set_bidirectional_m11000F7EEE034182321107E0CFD206B93E2F0DB5,
	NavMeshLink_get_autoUpdate_m990A747E3A283DBD7E7966B53539A6109F4006B8,
	NavMeshLink_set_autoUpdate_m435F36291F066510A7F5CA74858F2355306DD268,
	NavMeshLink_get_area_m0FD4060A62220399AA17DA1F4A0C92EF19E60E58,
	NavMeshLink_set_area_mF7AA5AB09937CF2C6C3C1A6AC15252903B16A44B,
	NavMeshLink_OnEnable_m51D292D990683E1B33BC9667BC8B3CC726A45834,
	NavMeshLink_OnDisable_m4438490649F048480A36E505D4373DC963BECCF2,
	NavMeshLink_UpdateLink_m209BE1B56461901F4DF40419F69052F0C9CBDA32,
	NavMeshLink_AddTracking_mE9E5F99B1112B73FE978CF9B4A2BDF9375EDEAB9,
	NavMeshLink_RemoveTracking_m5DBA4C7EDE2B4B075EE6AF0631B5E7F8B21B966B,
	NavMeshLink_SetAutoUpdate_mF68515C154A408E6E8BD7189E40E4F00E524CC72,
	NavMeshLink_AddLink_m0025D8D10612A3D2043426453C98930817A84315,
	NavMeshLink_HasTransformChanged_mD0C7C31886FBF76EA12C223497BB1E5B16C57DB9,
	NavMeshLink_OnDidApplyAnimationProperties_m0E9E42780F1C0F3E20A2C195FCA29248C60A9A21,
	NavMeshLink_UpdateTrackedInstances_mE43058846B333900F1E7E096E8FE96057A4820EC,
	NavMeshLink__ctor_m25087DBD118A26C9C0F0F2C7840411837E1DD254,
	NavMeshLink__cctor_mDA9752C99F9BD750B4CCAEF76D909DAB6B54877D,
	NavMeshModifier_get_overrideArea_mEBDF7BA1A5E02732EC58C0719E55C497E7AC8588,
	NavMeshModifier_set_overrideArea_mBB378398BA37AF6DF4B01DC251E8E0BB3B38C4D6,
	NavMeshModifier_get_area_mFB0A525E06D5301CD8ACFD0E0DC5E8A6CC014456,
	NavMeshModifier_set_area_m4A0F009842B714B7A5925733E142401BC3E7EC44,
	NavMeshModifier_get_ignoreFromBuild_m3430FE1AC3498D6231D5CED7E66678EAE0846D69,
	NavMeshModifier_set_ignoreFromBuild_mAEB916ABF0C8770E96B687E41211B7DABB14BC0B,
	NavMeshModifier_get_activeModifiers_mA80EBEDFD00FB8094130BACB291024C637E4DA1C,
	NavMeshModifier_OnEnable_mA7E3D7443E1420FFFD74711D68561D18D75C2543,
	NavMeshModifier_OnDisable_mE5AE357A2D5966422AE7D122CE0528A932F8B63F,
	NavMeshModifier_AffectsAgentType_m6E35A4A98A6465EE310DA56D3E364E43384916D1,
	NavMeshModifier__ctor_m50E7379A7135AA826CC86706FFC7CE03D52C3791,
	NavMeshModifier__cctor_m56C631511ADB28A8A18AE149FF8E3610BFD12E8A,
	NavMeshModifierVolume_get_size_m321A424ABBDC0BD9E863CC5F04DBBBBD124E857C,
	NavMeshModifierVolume_set_size_m046BA321411814CD871C2F294E856B8056F39879,
	NavMeshModifierVolume_get_center_m680C062269D60BD22DEC8DE535627C68C0867122,
	NavMeshModifierVolume_set_center_mEA63562D97D51F46D0E1B2CEACCAC4A15C22E7E1,
	NavMeshModifierVolume_get_area_m7FFDB165FCE530ACCE64932C2F623A1E040FD37B,
	NavMeshModifierVolume_set_area_m6CE9C2E002305B27C0EB4824AA2259B7BB9056B8,
	NavMeshModifierVolume_get_activeModifiers_m15594BB3E469B272A98BF5B66D14C82F77E6046A,
	NavMeshModifierVolume_OnEnable_m4F14D96EB3294A96D71B179300DB2483F2FA6180,
	NavMeshModifierVolume_OnDisable_mA21F77B955F1C82D0E88D09A346BF016BC8F0B84,
	NavMeshModifierVolume_AffectsAgentType_m181399154BFAA77A6F48D6C0281C37F088F9E98B,
	NavMeshModifierVolume__ctor_mCFA57767A4A82943453EA212E465784E612C6EC2,
	NavMeshModifierVolume__cctor_mEFC3F16CD27DDC34A56A2A3BFF05436968F35DDE,
	NavMeshSurface_get_agentTypeID_m817A837C523AD133FAE8D159DAA4C568BA431FA3,
	NavMeshSurface_set_agentTypeID_m75B82A12C316BA7F1BC1DAB6D9DD832C2A13D690,
	NavMeshSurface_get_collectObjects_m70B41C466C0E6CB9B8D216E15AFD446D667B7A48,
	NavMeshSurface_set_collectObjects_mAE4B9BEF06AF02D80159BB2B02DDD4DDB11C90B8,
	NavMeshSurface_get_size_mD238C28C636DAA2C4942544223D8BE33501083AA,
	NavMeshSurface_set_size_m25C813BC47FC1FC9FD99613FEC6E7764A6712E51,
	NavMeshSurface_get_center_m84044AA44C1246C0C82AB10215FFB288CFC75280,
	NavMeshSurface_set_center_m7ADBC113E3E0D11F7D9463C77D7F78F141877473,
	NavMeshSurface_get_layerMask_mE85E4356346FBD202B24DC58457E0E7DF8C13C7B,
	NavMeshSurface_set_layerMask_mBA70CAC4BAA7A9DD70F333E42DB20246D967F4A3,
	NavMeshSurface_get_useGeometry_m88DEE508B8229A62B19F4355405F56BCC07E6F34,
	NavMeshSurface_set_useGeometry_m226D0AD24ABD7189CDFB11120E841EF2F26FD171,
	NavMeshSurface_get_defaultArea_mC48E96F3ACAA2F701620D43CD7D860F30395CF30,
	NavMeshSurface_set_defaultArea_mFE8E7487806747A5BA23AF04B518101019BD0526,
	NavMeshSurface_get_ignoreNavMeshAgent_m3974A81143726E7738955EB1E66746BDD4DFEB79,
	NavMeshSurface_set_ignoreNavMeshAgent_m3BB95A33F5E5A0F637147E29C8B94D6934C22A6E,
	NavMeshSurface_get_ignoreNavMeshObstacle_mBE7EB23C662C3D8963EFAF40DC48E34934514459,
	NavMeshSurface_set_ignoreNavMeshObstacle_mE48F13378BCB6BC698B42DD410078B0F2BC2C15E,
	NavMeshSurface_get_overrideTileSize_m498945E257ABA5479D2EF46CC115E83C30F9CD39,
	NavMeshSurface_set_overrideTileSize_m0DCB54AA760A43F4F0A028A9C7F23CFF0E80D6F0,
	NavMeshSurface_get_tileSize_m547CCF2E478DD4F6949CE4790F1666DC811FDA4B,
	NavMeshSurface_set_tileSize_m87B5E72A2739EDF7E42C2CBBCFC2D63CD30A7904,
	NavMeshSurface_get_overrideVoxelSize_m7C0420C2A9F451680139E2590171379605670DBA,
	NavMeshSurface_set_overrideVoxelSize_mF186072B354E8BF4FF6546B8C29C709B7A395CB7,
	NavMeshSurface_get_voxelSize_m3FCE294C0933A0C459F34E1BF0E6F6F513F75C1A,
	NavMeshSurface_set_voxelSize_m4556E474F8A9831CE1173E1FA9F7B20AA7F8A702,
	NavMeshSurface_get_buildHeightMesh_mFE7AE3812BF637CAA070B5E4AE37FD274DCB175B,
	NavMeshSurface_set_buildHeightMesh_mFD446C2E53118C25FC2B006283A0D632281E5CF9,
	NavMeshSurface_get_navMeshData_m944D83922EAC0768700E6BF3B41F7AB6D457BC8F,
	NavMeshSurface_set_navMeshData_m8C2E4D6D6E50DEE4F8E6D4882703857C5CCCCD52,
	NavMeshSurface_get_activeSurfaces_mF1F8A900FF35EC6DDA6A3CD9C9EE53A2D4ADD4AA,
	NavMeshSurface_OnEnable_m308F744EB55A7443F56CEBE857C898171A59AA69,
	NavMeshSurface_OnDisable_m99AB28410CF2481C04DA1DA05CADE73AF089101B,
	NavMeshSurface_AddData_m3B92D495D85795E8AAEB5BF782A86576DBF7206F,
	NavMeshSurface_RemoveData_m244AFF7183FD627F16B91BAD59DC32131738BB3B,
	NavMeshSurface_GetBuildSettings_mD1046164C39A191D78217E4E7A0329F40122760A,
	NavMeshSurface_BuildNavMesh_m593B302D342E36F0C4FF9F9C3EB195B9A0E03AB5,
	NavMeshSurface_UpdateNavMesh_m29A795A61811599F6BD6BFF45C0EAC66010BF64A,
	NavMeshSurface_Register_m6658C2249E6B1E7573C06FE21C3D33D5FB809453,
	NavMeshSurface_Unregister_m954186C642CB8A05D45985BBACCF873D58E01737,
	NavMeshSurface_UpdateActive_m64DE882138713A5E3B6650706665164F3780EC88,
	NavMeshSurface_AppendModifierVolumes_mEFF654357C4853A17D06DBF634DB122839122550,
	NavMeshSurface_CollectSources_mD13E1B734C6EC5650E652F7F693B53AF714D4B11,
	NavMeshSurface_Abs_m6FDEC6511AC40FD4E1C76F8BEB31A60A5BA5C293,
	NavMeshSurface_GetWorldBounds_m53366C6EC28A95EB84515C2572B976C1EF5F0D77,
	NavMeshSurface_CalculateWorldBounds_m6E01402D5A3CD76865FD4A23FAFBFA9FB9A5F713,
	NavMeshSurface_HasTransformChanged_m5766AE0E589120789DE1EAB7AE22AA206AE74E42,
	NavMeshSurface_UpdateDataIfTransformChanged_mEAF82FD02A70516374534495BC57E62A08238423,
	NavMeshSurface__ctor_m79043E9325067CF57A0685A4085E85F8947587D4,
	NavMeshSurface__cctor_mA4BC6AC522609D76F252DBA917DB451153FA506F,
	U3CU3Ec__cctor_mEBDA56AE0FA98A2660170758B63D2C55175EC3D2,
	U3CU3Ec__ctor_mC124ECF3C8BCA1F364397BCDA11234EAF60C1648,
	U3CU3Ec_U3CAppendModifierVolumesU3Eb__76_0_mFA187EB36C602A178C49CA72A2B982E5311016ED,
	U3CU3Ec_U3CCollectSourcesU3Eb__77_0_m8F7050C648EFFB8533ACF3CA1BA6C5CBDB6DF999,
	U3CU3Ec_U3CCollectSourcesU3Eb__77_1_m5BF192B9E3C50B44808654213B9A6DB4B5938D8C,
	U3CU3Ec_U3CCollectSourcesU3Eb__77_2_m533C32C15CF6C2F6E273BFB5DD04D776AA3CF04D,
	NavMeshSurface2d_get_agentTypeID_mBB1CB5B47E7868994C9BB2495EB08F2D23B32C34,
	NavMeshSurface2d_set_agentTypeID_m9C298B6186807E4ED16AD1997412E957373B73A7,
	NavMeshSurface2d_get_collectObjects_m3B7DE460DF0816F803B264FCA035FC154FA12103,
	NavMeshSurface2d_set_collectObjects_m455EF8C631748B70DD72489190C7BDEB39325F1A,
	NavMeshSurface2d_get_size_m5B92BB1C6C52715724A44144FE114D55D151DD3C,
	NavMeshSurface2d_set_size_mC3357FABEF20078866029AB7ECF61529D9DAC990,
	NavMeshSurface2d_get_center_m7F52FBF7F31D5E8A941602D8DD473E8F7BFC42B2,
	NavMeshSurface2d_set_center_mD08B2B85279141FD6D23DD35B63CED4F1C80975A,
	NavMeshSurface2d_get_layerMask_m67A239369D5B7B54ED3124D70167659866AF0BEA,
	NavMeshSurface2d_set_layerMask_m0C8C885CC76560B80776073CE11080BFAE7E80CB,
	NavMeshSurface2d_get_useGeometry_mAC674708212AD945152A392A291C3BAE9F1DE538,
	NavMeshSurface2d_set_useGeometry_m16CE11A1EC8166569C50942362BBC963CE4BC6D1,
	NavMeshSurface2d_get_overrideByGrid_m3EA3E3ACF3A55B4E44570BA9940B56A6911069D2,
	NavMeshSurface2d_set_overrideByGrid_m1262A9D0EC451A80E01A9931E407C377612C27DC,
	NavMeshSurface2d_get_useMeshPrefab_mD2DECC874AE35383DCD2E870FBBEC5B62C50C153,
	NavMeshSurface2d_set_useMeshPrefab_m699557EF6160D68CC03B76ED11070FB47C50DA18,
	NavMeshSurface2d_get_compressBounds_m452A027FB3102878642D048C9B3E3A89F913FB8A,
	NavMeshSurface2d_set_compressBounds_m3B6948D3AAD099F1BDCCD186997AA7B1E0730080,
	NavMeshSurface2d_get_overrideVector_m338B5656EF72D1CA4D03F28E552EF40884A5048B,
	NavMeshSurface2d_set_overrideVector_mEA06BDD1BBF5FB94D62C435BD519CB7B767E29DD,
	NavMeshSurface2d_get_defaultArea_mD0B11DF562EE8D388B193623AB23D4CB1AF20DB4,
	NavMeshSurface2d_set_defaultArea_m093C89F0B4B0CFB215F4D3470D23DD759038C75A,
	NavMeshSurface2d_get_ignoreNavMeshAgent_m37711799CC6E0BEDEE6E895C4441E0E6559BB9A4,
	NavMeshSurface2d_set_ignoreNavMeshAgent_m127DCF407993171E5692F97328128295FCEDC555,
	NavMeshSurface2d_get_ignoreNavMeshObstacle_m3DF17F7B512C10A502ADFD4B6D504423F82DC880,
	NavMeshSurface2d_set_ignoreNavMeshObstacle_mD4378B92F615D8F09C1DB640AF56FE686CBEDBFA,
	NavMeshSurface2d_get_overrideTileSize_mAEC6D7F272B6B186FE43ED357853E3B47DF0ECB4,
	NavMeshSurface2d_set_overrideTileSize_m2B053D9BEC47F27D0D36F20A9AD8743950391008,
	NavMeshSurface2d_get_tileSize_m0B17267D260283F8BCEAC17069C587C9B9AF4D78,
	NavMeshSurface2d_set_tileSize_m9D36C7A8FBFDB2FA71B6FFC941C58F291561B6BD,
	NavMeshSurface2d_get_overrideVoxelSize_mD997D4ADD4018898E7A4DE66C2EC2DF9A57FB3BE,
	NavMeshSurface2d_set_overrideVoxelSize_m73AE2BDAD36CA38BF5E2FE7FA9070D118A51A272,
	NavMeshSurface2d_get_voxelSize_mB797865B80F6F7A4502D919EB4E250128DBEDB68,
	NavMeshSurface2d_set_voxelSize_m87966F102916FBAE3DC2516A0687D31F5E2AF803,
	NavMeshSurface2d_get_buildHeightMesh_m4E906AFBA3EC431C60BBBD8BBCC78714B6DBD381,
	NavMeshSurface2d_set_buildHeightMesh_mE59973F052730AB36EBCFAD0E62A388EE7CFC8D7,
	NavMeshSurface2d_get_navMeshData_mB95AF6262EE3CE34080A10BF7B6352E9A18BCA35,
	NavMeshSurface2d_set_navMeshData_mE704451EB97F03E4A8AE4A999812EAF78F95273D,
	NavMeshSurface2d_get_activeSurfaces_m4FB9955072D676355E83FBB77BA1DA66E0E7822F,
	NavMeshSurface2d_OnEnable_mE492F8892AE9D644EED009BA4694659089B16B07,
	NavMeshSurface2d_OnDisable_m1B3B81A9382DEA6A3E26C4FA6A905821705CD785,
	NavMeshSurface2d_AddData_m5A159869514B72786B077F87BF90860C17331651,
	NavMeshSurface2d_RemoveData_mCFB8ECD4B4C7342D41A40830899C40538A53F174,
	NavMeshSurface2d_GetBuildSettings_m0A3F6F5A3703DF3623BE7031D9E831BC1A64A3A6,
	NavMeshSurface2d_BuildNavMesh_mD546A0EEA12D8B97AF264D9367898AB2F27A5AC0,
	NavMeshSurface2d_BuildNavMeshAsync_m399AAD6583BCEC4EE027C35A9489760104D3503F,
	NavMeshSurface2d_UpdateNavMesh_mFA61071748EBE2ADF9507013D77F714A0E1EC9D4,
	NavMeshSurface2d_Register_m2C4BE30EBDB969A0CE56D820DA3B9CE4A9B0BC09,
	NavMeshSurface2d_Unregister_m13BC7E14D33A52A97CFC7BFD7AC682F9B68188B5,
	NavMeshSurface2d_UpdateActive_mA7E40282FE68FB4AF115A0C926762B7E1B992CAB,
	NavMeshSurface2d_AppendModifierVolumes_mF8623E33A19BA16DB790A4BD156E87D6EB11B07B,
	NavMeshSurface2d_CollectSources_m2AF42C3A2EADA6AE761A72A5CD39BA9611294A86,
	NavMeshSurface2d_Abs_m93ACE4AB9A0B128A56DB0EF705832259BD438AD4,
	NavMeshSurface2d_GetWorldBounds_m19310580DD329AA9F2D2157C8D99A6955D0180B0,
	NavMeshSurface2d_CalculateWorldBounds_m8444ECC8CFA16BE978C0BF9963B71A73DA5FCC62,
	NavMeshSurface2d_CalculateGridWorldBounds_m0DAB250D4DF2DD43035D392DC7E45B869E8E6282,
	NavMeshSurface2d_HasTransformChanged_m3BDDD1E4784451CE7CED9D206E70401864D5C443,
	NavMeshSurface2d_UpdateDataIfTransformChanged_m3367FA661EDFCFAAB82E830F02306EB2EF9602C1,
	NavMeshSurface2d__ctor_m652509810BFC1FDC84A33F856BEAB0D20057EC53,
	NavMeshSurface2d__cctor_m7CBC813ECA4BD28B56B7D06670C0E06A1E471500,
	U3CU3Ec__cctor_mB534DF09C3EF248D17C7AB487F643D3F29EFDD60,
	U3CU3Ec__ctor_m0B0E3E0360E4786541071127CD394606E512BFBE,
	U3CU3Ec_U3CAppendModifierVolumesU3Eb__93_0_mC01A94D6E7899DEB8079B26EF4EB278DF6DDB092,
	U3CU3Ec_U3CCollectSourcesU3Eb__94_0_m58F9EB3197CE0E95D0DF6B323F26F001370C45BA,
	U3CU3Ec_U3CCollectSourcesU3Eb__94_1_m819F97AF66B0746B51661726FBD6F07926288D59,
	U3CU3Ec_U3CCollectSourcesU3Eb__94_2_m4404DE300ABDAA8665AD90E12098A4229B20410F,
};
static const int32_t s_InvokerIndices[186] = 
{
	6908,
	4903,
	4903,
	6765,
	10106,
	9086,
	8312,
	8312,
	8312,
	10106,
	11162,
	6908,
	6727,
	5485,
	6898,
	5650,
	6898,
	5650,
	6836,
	5591,
	6727,
	5485,
	6666,
	5418,
	6666,
	5418,
	6727,
	5485,
	6908,
	6908,
	6908,
	11516,
	11516,
	5418,
	6908,
	6666,
	6908,
	12362,
	6908,
	12362,
	6666,
	5418,
	6727,
	5485,
	6666,
	5418,
	12322,
	6908,
	6908,
	3946,
	6908,
	12362,
	6898,
	5650,
	6898,
	5650,
	6727,
	5485,
	12322,
	6908,
	6908,
	3946,
	6908,
	12362,
	6727,
	5485,
	6727,
	5485,
	6898,
	5650,
	6898,
	5650,
	6736,
	5495,
	6727,
	5485,
	6727,
	5485,
	6666,
	5418,
	6666,
	5418,
	6666,
	5418,
	6727,
	5485,
	6666,
	5418,
	6836,
	5591,
	6666,
	5418,
	6765,
	5521,
	12322,
	6908,
	6908,
	6908,
	6908,
	6759,
	6908,
	4903,
	11516,
	11516,
	12362,
	5406,
	6765,
	11489,
	9318,
	3566,
	6666,
	6908,
	6908,
	12362,
	12362,
	6908,
	3982,
	3982,
	3980,
	3980,
	6727,
	5485,
	6727,
	5485,
	6898,
	5650,
	6898,
	5650,
	6736,
	5495,
	6727,
	5485,
	6666,
	5418,
	6765,
	5521,
	6666,
	5418,
	6898,
	5650,
	6727,
	5485,
	6666,
	5418,
	6666,
	5418,
	6666,
	5418,
	6727,
	5485,
	6666,
	5418,
	6836,
	5591,
	6666,
	5418,
	6765,
	5521,
	12322,
	6908,
	6908,
	6908,
	6908,
	6759,
	6908,
	6765,
	4903,
	11516,
	11516,
	12362,
	5406,
	6765,
	11489,
	9318,
	3566,
	10952,
	6666,
	6908,
	6908,
	12362,
	12362,
	6908,
	3982,
	3982,
	3980,
	3980,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_NavMeshComponents_CodeGenModule;
const Il2CppCodeGenModule g_NavMeshComponents_CodeGenModule = 
{
	"NavMeshComponents.dll",
	186,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
